﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class CancellationPolicy
    {
        public bool  nonRefundable { get; set; }
        public string dayMin { get; set; }
        public string dayMax { get; set; }
        public float deduction { get; set; }
        public string unit { get; set; }
        public  ChargeRate objCharges { get; set; }
        public float CancellationAmount { get; set; }
        public float CUTCancellationAmount { get; set; }
        public float AgentCancellationMarkup { get; set; }
        public float StaffCancellationMarkup { get; set; }
        public float CutRoomAmount { get; set; }
      
        public string CancellationDateTime { get; set; }
        public bool AmendRestricted { get; set; }
        public bool CancelRestricted { get; set; }
        public string NoShowPolicy { get; set; }
        public string ToolTip { get; set; }
        public List<date> arrChargeDate { get; set; }


        public float AgentRoomMarkup { get; set; }
        public float SupplierMarkup { get; set; }
        public float S2SMarkup { get; set; }

    }
    public class ChargeRate
    {
        public float BaseRate { get; set; }
        public float TotalRate { get; set; }
        public List<TaxRate> Charge { get; set; }
    }
}
