﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class HotelOccupancy
    {
        public List<int> RoomNo { get; set; }
        public int RoomCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public string ChildAges { get; set; }
        public List<Customers> GuestList { get; set; }
        public List<RoomType> Rooms { get; set; }

        public float MinPrice { get; set; }
        public float MaxPrice { get; set; }

    }
}
