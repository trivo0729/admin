﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
   public  class RateBreakup
    {
        public string  RoomID { get; set; }

        public string  RoomDescpID { get; set; }

        public string RoomName { get; set; }

        public string MealPlan { get; set; }

        public float RoomRate { get; set; }

        //public List<GSTdetails> GST { get; set; }

        public ServiceCharge charges { get; set; }

        public float CutRoomRate { get; set; }

        public float ActuatlRate { get; set; }

        public List<date> dates { get; set; }

        public string Bedding { get; set; }

        public string Smooking { get; set; }

        public string Note { get; set; }

        public List<string> Cancelation { get; set; }


    }
}
