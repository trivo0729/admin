﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class RateGroup
    {
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string AvailToken { get; set; }
        public ServiceCharge Charge { get; set; }
        public float CutPrice { get; set; }
        public float AgentMarkup { get; set; }
        public List<HotelOccupancy> RoomOccupancy { get; set; }
        public float Total { get; set; }
    }
}
