﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class RoomType
    {
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public string ChildAges { get; set; }

        //public Int64 RoomTypeId { get; set; }
        public string RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomDescription { get; set; }
        public string RoomDescriptionId { get; set; }
        public Int64 AvailCount { get; set; }
        public List<special> special { get; set; }
        public Int64 specialsApplied { get; set; }
        public List<CancellationPolicy> CancellationPolicy { get; set; }
        //public rateBasis rateBasis_s { get; set; }
        //public List<rateBasis> rateBasis { get; set; }
        public string RoomRateType { get; set; }
        public string RoomRateTypeCurrency { get; set; }
        public Int64 RoomRateTypeCurrencyId { get; set; }
        public string RoomAllocationDetails { get; set; }
        public string minStay { get; set; }
        public string dateApplyMinStay { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public string tariffNotes { get; set; }
        public float Total { get; set; }
        public int LeftToSell { get; set; }
        public string status { get; set; }
        public int passengerNamesRequiredForBooking { get; set; }
        public validForOccupancy validForOccupancy { get; set; }
        public string Nationality { get; set; }
        public string changedOccupancy { get; set; }
        public List<date> Dates { get; set; }
        
        public List<TaxRate> OtherRates { get; set; }
        public List<string> ListCancel { get; set; }

        
    }
}
