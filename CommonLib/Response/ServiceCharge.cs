﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
   public class ServiceCharge
    {
        /* Markup & Charge Component */

        public string Type { get; set; }

        public float Rate { get; set; }             /*Converted into Agent Currency*/

        public float ServiceTax { get; set; }

        public float CUTMarkup { get; set; }

        public bool IsOnMarkup { get; set; }

        public float AgentMarkup { get; set; }

        public float Discount { get; set; }

        public float Commission { get; set; }

        public float TDS { get; set; }

        public float CUTPrice { get; set; }

        public float TotalPrice { get; set; }              /* Supplier Amount*/

        //public List<GSTdetails> GSTdetails { get; set; }


        public float FranchiseeMarkup { get; set; }
        //public List<GSTdetails> FranchiseGSTdetails { get; set; }

        public float RoomRate { get; set; }
        public List<TaxRate> OtherRates { get; set; }
    }
}
