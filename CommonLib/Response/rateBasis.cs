﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
    public class rateBasis
    {
        public string RoomDescription { get; set; }
        public Int64 RoomDescriptionId { get; set; }
        public Int64 specialsApplied { get; set; }
        public string RoomRateType { get; set; }
        public string RoomRateTypeCurrency { get; set; }
        public Int64 RoomRateTypeCurrencyId { get; set; }
        public string RoomAllocationDetails { get; set; }
        public string minStay { get; set; }
        public string dateApplyMinStay { get; set; }
        public List<CancellationPolicy> cancellationRules { get; set; }
        public float CUTPrice { get; set; }
        public float AgentMarkup { get; set; }
        public string tariffNotes { get; set; }
        public float Total { get; set; }
        public int LeftToSell { get; set; }
        public string status { get; set; }
    }
}
