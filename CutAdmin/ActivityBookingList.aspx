﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ActivityBookingList.aspx.cs" Inherits="CutAdmin.ActivityBookingList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ActivityBookingList.js"></script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <link href="css/dynamicDiv.css" rel="stylesheet" />

    <script src="Scripts/moments.js"></script>

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
        ul li {
            list-style: none;
            cursor: pointer;
        }

        .deleteMe {
            float: right;
            background: #bb3131;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <section role="main" id="main">
        <div class="with-padding">

            <%-- <div class="with-padding " id="div_BookingList">
                   </div>--%>

            <hgroup id="main-title" class="thin">
                <h3>Activity Booking List</h3>
                <hr>
            </hgroup>
            <div class="with-padding" style="width:99%">
               <%-- <div id="tbl_BookingList_wrapper" class="dataTables_wrapper" role="grid">--%>
                   <%-- <div id="tbl_BookingList_length" class="dataTables_length">
                        <label>Show
                            <select size="1" name="tbl_BookingList_length" aria-controls="tbl_BookingList">
                                <option value="10" selected="selected">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            entries</label></div>--%>
                 <%--   <div class="dataTables_filter" id="tbl_BookingList_filter">
                        <label>Search:
                            <input type="text" aria-controls="tbl_BookingList"></label></div>--%>
                    <table class="simple-table responsive-table" id="tbl_BookingList" >

                        <thead>
                            <tr>
                                <th scope="col">S.N</th>
                                <th scope="col" class="align-center">Booking Date</th>  
                                <th scope="col" class="align-center">Ref.No</th>  
                                <th scope="col" class="align-center">Passenger</th>       
                                <th scope="col" class="align-center">Agency</th>
                                <th scope="col" class="align-center">Activity & Location</th>       
                                <th scope="col" class="align-center">CheckIn Date</th>
                                <th scope="col" class="align-center">No Of Pax</th>                      
                                <th scope="col" class="align-center">Status</th>        
                                <th scope="col" class="align-center">Amount</th>                              
                            </tr>

                        </thead>
                    
                    </table>
         
                </div>
            </div>
    
    </section>
    <!-- JavaScript at the bottom for fast page loading -->
     	<script src="js/libs/jquery.tablesorter.min.js"></script>
    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>
   

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
</asp:Content>
