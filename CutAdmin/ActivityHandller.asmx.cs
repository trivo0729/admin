﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Threading;
using System.ComponentModel;
using System.Reflection;
using CutAdmin.dbml;

namespace CutAdmin
{
    public class Inclusion
    {
        public string Value { get; set; }
        public Int64 id { get; set; }
    }

    public class Exclusion
    {
        public string Value { get; set; }
        public Int64 id { get; set; }
    }

    public class ExclusionSpl
    {
        public string Value { get; set; }
        public Int64 id { get; set; }
    }
    public class InclusionSpl
    {
        public string Value { get; set; }
        public Int64 id { get; set; }
    }
    /// <summary>
    /// Summary description for ActivityHandller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ActivityHandller : System.Web.Services.WebService
    {
        public static List<Ratelistdetails> ListSearch { get; set; }
        public static List<ListRate> ListRatee { get; set; }

        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        //helperDataContext DB = new helperDataContext();

        string json = "";
        private string escapejsondata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Search Vications

        #region AutocompleteCity
        [WebMethod(EnableSession = true)]
        public string autocompleteCity()
        {
            string CityName = "";
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            DataManager.DBReturnCode retcode = ActivityManager.AutocompleteCity(CityName, out dtResult);
            if (retcode == DataManager.DBReturnCode.SUCCESS)
            {
                //dtResult.Columns.Add("CityCountry", typeof(string), "City+','+Country");
                //List<AutoComplete> list_autocomplete = new List<AutoComplete>();
                //list_autocomplete = dtResult.AsEnumerable()
                //.Select(data => new  AutoComplete
                //{
                //    id = data.Field<String>("City"),
                //    value = data.Field<String>("CityCountry")
                //}).ToList();
                List<Dictionary<string, object>> sCity = new List<Dictionary<string, object>>();
                sCity = DataManager.ConvertDataTable(dtResult);
                jsonString = jsSerializer.Serialize(new { retCode = 1, Session = 1, arrCountry = sCity });
                dtResult.Dispose();
            }
            else
            {
                jsonString = jsSerializer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;

        }

        #endregion

        #region Get TripType
        [WebMethod(EnableSession = true)]
        public string GetTripType(string City, string Country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            DataManager.DBReturnCode retcode = ActivityManager.GetTripType(City, Country, out dtResult);
            if (retcode == DataManager.DBReturnCode.SUCCESS)
            {
                List<string> TourType = new List<string>();
                for (int i = 0; i < dtResult.Rows.Count; i++)
                {
                    string Type = dtResult.Rows[i]["Tour_Type"].ToString();
                    string[] Types = Type.Split(';');
                    for (int j = 0; j < Types.Length; j++)
                    {
                        TourType.Add(Types[j]);
                    }

                }
                List<Dictionary<string, object>> sTrips = new List<Dictionary<string, object>>();
                sTrips = DataManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, arrsTrips = TourType.Distinct() });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region Search Act
        [WebMethod(EnableSession = true)]
        public string SearchAct(string City, string Country, string TripType, string dtTour)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            Session["TravelDate"] = dtTour;
            Session["SearchResult"] = null;
            DataManager.DBReturnCode retcode = ActivityManager.SearchAct(City, TripType.Replace("-All Type-", " "), out dtResult);
            if (retcode == DataManager.DBReturnCode.SUCCESS)
            {
                if (dtTour != "")
                {
                    dtResult.Columns.Add("From", typeof(DateTime));
                    dtResult.Columns.Add("To", typeof(DateTime));
                    DateTime From = new DateTime();
                    DateTime To = new DateTime();
                    DateTime TravelDate = new DateTime();
                    TravelDate = DateTime.ParseExact(dtTour, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        //  From = DateTime.ParseExact(dtResult.Rows[i]["Valid_from"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        From = GetAppDate(dtResult.Rows[i]["Valid_from"].ToString());
                        //  To = DateTime.ParseExact(dtResult.Rows[i]["Valid_to"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        To = GetAppDate(dtResult.Rows[i]["Valid_to"].ToString());
                        dtResult.Rows[i]["From"] = From;
                        dtResult.Rows[i]["To"] = To;
                    }
                    DataRow[] rows = null;
                    rows = dtResult.Select("(To  >= '" + TravelDate + "' AND From <= '" + TravelDate + "')");
                    if (rows.Length != 0)
                    {
                        dtResult = rows.CopyToDataTable();
                        Session["SearchResult"] = dtResult;
                        List<Dictionary<string, object>> sTrips = new List<Dictionary<string, object>>();
                        sTrips = DataManager.ConvertDataTable(dtResult);
                        dtResult.Dispose();
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });

                    }
                    else
                    {
                        return jsSerializer.Serialize(new { retCode = 2, Session = 1 });
                    }
                }
                else
                {
                    List<Dictionary<string, object>> sTrips = new List<Dictionary<string, object>>();
                    sTrips = DataManager.ConvertDataTable(dtResult);
                    Session["SearchResult"] = dtResult;
                    dtResult.Dispose();
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                }



            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        public static DateTime GetAppDate(string Date)
        {
            //DateTime AppDate = new DateTime();
            //try
            //{
            //    //AppDate = ConvertDateTime(Row["AppliedDate"].ToString());
            //    AppDate = DateTime.ParseExact(Row["AppliedDate"].ToString().Split(' ')[0].Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);

            //}
            //catch
            //{

            //}
            //return AppDate;
            //  string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {
                                 "d-M-yy", "dd-MM-yy",
                                "M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;

                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }

        #endregion

        #endregion



        #region Activity List
        [WebMethod(EnableSession = true)]
        public string ActList()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<DataLayer.Activity> listActivity;
            DBHelper.DBReturnCode retcode = ActivityManager.GetGenralDetails(out listActivity);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                //if (dtResult.Columns.Contains("MinPrice") == false)
                //{
                //    dtResult.Columns.Add("MinPrice", typeof(string));
                //    dtResult.Columns.Add("ActTypes", typeof(string));
                //}
                //string UniqeId = "";
                //List<float> MinPrice = new List<float>(dtResult.Rows.Count);
                //List<float> PriceRange = new List<float>();
                //List<string> ActType = new List<string>();
                //List<string> Tour = new List<string>();
                //List<string> TourType = new List<string>();
                //float AdultPrice = 0;
                //float Child = 0;
                //for (int i = 0; i < dtResult.Rows.Count; i++)
                //{
                //    TourType.Add(dtResult.Rows[i]["Tour_Type"].ToString());
                //    UniqeId = dtResult.Rows[i]["Sr_No"].ToString();
                //    retcode = ActivityManager.GetActivityPrice(UniqeId, out dtPrice);
                //    MinPrice = new List<float>(dtPrice.Rows.Count);
                //    ActType = new List<string>(dtPrice.Rows.Count);
                //    for (int J = 0; J < dtPrice.Rows.Count; J++)
                //    {
                //        if (dtPrice.Rows[J]["Adult_Price"].ToString() != "")
                //            AdultPrice = Convert.ToSingle(dtPrice.Rows[J]["Adult_Price"]);
                //        else
                //            AdultPrice = 0;
                //        //Child = Convert.ToSingle(dtPrice.Rows[J]["Child_Price"]);
                //        MinPrice.Add(AdultPrice + Child);
                //        //PriceRange.Add(AdultPrice + Child);
                //        ActType.Add(dtPrice.Rows[J]["Act_Type"].ToString());
                //        Tour.Add(dtPrice.Rows[J]["Act_Type"].ToString());
                //    }
                //    var result = string.Join(",", ActType.Distinct().ToArray());
                //    dtResult.Rows[i]["MinPrice"] = MinPrice.Min();
                //    dtResult.Rows[i]["ActTypes"] = result.ToString();
                //    PriceRange.Add(MinPrice.Min());
                //}
                //List<Dictionary<string, object>> sActivitylist = new List<Dictionary<string, object>>();
                //sActivitylist = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, arrActivity = listActivity });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string ActTeriiff(string id)
        {
            string jsonString = "";

            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retcode = ActivityManager.ActTeriiff(id, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                parentRow = JsonStringManager.ConvertDataTable(dtResult);
                //jsonString = "";
                //foreach (DataRow dr in dtResult.Rows)
                //{
                //    jsonString += "{";
                //    foreach (DataColumn dc in dtResult.Columns)
                //    {
                //        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejsondata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                //    }
                //    jsonString = jsonString.Trim(',') + "},";
                //}

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, dttable = parentRow });
                //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
                // dtResult.Dispose();
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
                //jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            //return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetActivities(int PageNo)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable(), dtPrice = new DataTable();
            DataManager.DBReturnCode retcode = DataManager.DBReturnCode.EXCEPTION;
            if (Session["SearchResult"] != null)
            {
                dtResult = (DataTable)Session["SearchResult"];
                retcode = DataManager.DBReturnCode.SUCCESS;
            }
            else
            {
                retcode = ActivityManager.GetActivity(out dtResult);
            }


            if (retcode == DataManager.DBReturnCode.SUCCESS)
            {
                if (dtResult.Columns.Contains("MinPrice") == false)
                {
                    dtResult.Columns.Add("MinPrice", typeof(string));
                    dtResult.Columns.Add("ActTypes", typeof(string));
                }

                string UniqeId = "";
                List<float> MinPrice = new List<float>(dtPrice.Rows.Count);
                List<float> PriceRange = new List<float>();
                List<string> ActType = new List<string>();
                List<string> Tour = new List<string>();
                List<string> TourType = new List<string>();
                float AdultPrice = 0;
                float Child = 0;
                int pageNum = PageNo;
                int pageSize = 1 * 12;
                for (int i = 0; i < dtResult.Rows.Count; i++)
                {
                    string Type = dtResult.Rows[i]["Tour_Type"].ToString();
                    string[] Types = Type.Split(';');
                    for (int j = 0; j < Types.Length; j++)
                    {
                        TourType.Add(Types[j].Replace(" ", ""));
                    }

                    UniqeId = dtResult.Rows[i]["Sr_No"].ToString();
                    retcode = ActivityManager.GetActivityPrice(UniqeId, out dtPrice);
                    MinPrice = new List<float>(dtPrice.Rows.Count);
                    ActType = new List<string>(dtPrice.Rows.Count);
                    for (int J = 0; J < dtPrice.Rows.Count; J++)
                    {
                        if (dtPrice.Rows[J]["Adult_Price"].ToString() != "")
                            AdultPrice = Convert.ToSingle(dtPrice.Rows[J]["Adult_Price"]);
                        else
                            AdultPrice = 0;
                        //Child = Convert.ToSingle(dtPrice.Rows[J]["Child_Price"]);
                        MinPrice.Add(AdultPrice + Child);
                        //PriceRange.Add(AdultPrice + Child);
                        ActType.Add(dtPrice.Rows[J]["Act_Type"].ToString());
                        Tour.Add(dtPrice.Rows[J]["Act_Type"].ToString());
                    }
                    var result = string.Join(",", ActType.Distinct().ToArray());
                    dtResult.Rows[i]["MinPrice"] = MinPrice.Min();
                    dtResult.Rows[i]["ActTypes"] = result.ToString();
                    PriceRange.Add(MinPrice.Min());
                }

                DataTable dtPage = dtResult.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
                Session["dtActivityList"] = dtResult;
                //jsonString = "";
                decimal No = Convert.ToDecimal(dtResult.Rows.Count);
                decimal Pagging = Convert.ToDecimal(No / pageSize);
                string NoOfPage = Convert.ToString(Pagging);
                string[] splitPagging = NoOfPage.Split('.');
                if (splitPagging.Length > 2)
                {
                    if (Convert.ToDecimal(splitPagging[1]) > Convert.ToDecimal(0.1))
                    {
                        splitPagging[1] = "1";
                        No = Convert.ToDecimal(splitPagging[0]);
                        decimal Roundof = Convert.ToDecimal(splitPagging[1]);
                        Pagging = No + Roundof;
                    }
                }
                string Price = Convert.ToString(PriceRange.Min() + "," + PriceRange.Max());
                List<Dictionary<string, object>> Activity = new List<Dictionary<string, object>>();
                Activity = DataManager.ConvertDataTable(dtPage);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, arrActivity = Activity, PriceRange = Price, MinPrice = PriceRange.Min(), MaxPrice = PriceRange.Max(), Tour = Tour.Distinct(), TourType = TourType.Distinct(), Pagging = Pagging, noActivity = dtResult.Rows.Count });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }



        #region Search Activity
        [WebMethod(EnableSession = true)]
        public string SearchActivities(int PageNo, List<string> TourMode, List<string> TourTypes)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable(), dtPrice = new DataTable();
            DataTable dtTourType = new DataTable();
            DataManager.DBReturnCode retcode = DataManager.DBReturnCode.EXCEPTION;

            if (Session["dtActivityList"] != null)
            {
                dtResult = (DataTable)Session["dtActivityList"];
                DataRow[] rows = null;
                DataRow newRow = null;
                if (TourMode.Count == 1)
                {
                    rows = dtResult.Select("(ActTypes ='" + TourMode[0] + "')");
                    if (rows.Length != 0)
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                }
                else
                {
                    dtTourType = dtResult.Clone();
                    for (int i = 0; i < TourMode.Count; i++)
                    {
                        if (TourMode[i] == "")
                            continue;
                        rows = null;
                        rows = dtResult.Select("(ActTypes  like '%" + TourMode[i] + "%')");
                        if (rows.Length != 0)
                        {
                            // dtResult = rows.CopyToDataTable();
                            foreach (DataRow d in rows)
                            {

                                dtTourType.ImportRow(d);
                            }
                        }
                    }
                    dtResult.Rows.Clear();
                    dtResult = dtTourType.Copy();
                }
                dtTourType.Rows.Clear();
                dtTourType = dtResult.Clone();
                for (int i = 0; i < TourTypes.Count; i++)
                {
                    string Type = TourTypes[i].Replace("ThemePark", "Theme Park").Replace("FamilyTour", "Family Tour")
                                .Replace("YouthTour", "Youth Tour").Replace("YouthTour", "Youth Tour")
                                .Replace("Shows&Event", "Shows & Event").Replace("WaterPark", "Water Park")
                                .Replace("SkyTour", "Sky Tour");
                    rows = dtResult.Select("(Tour_Type  like '" + Type + "%')");
                    if (rows.Length != 0)
                    {
                        //Import the Rows
                        foreach (DataRow d in rows)
                        {

                            dtTourType.ImportRow(d);
                        }

                        //dtTourType = rows.CopyToDataTable();
                    }
                }
                dtResult.Rows.Clear();
                dtResult = dtTourType.Copy();
                float AdultPrice = 0;
                float Child = 0;
                int pageNum = PageNo;
                int pageSize = 1 * 12;
                DataTable dtPage = dtResult.Rows.Cast<System.Data.DataRow>().Skip((pageNum - 1) * pageSize).Take(pageSize).CopyToDataTable();
                Session["dtSearchList"] = dtResult;
                //jsonString = "";
                decimal No = Convert.ToDecimal(dtResult.Rows.Count);
                decimal Pagging = Convert.ToDecimal(No / pageSize);
                string NoOfPage = Convert.ToString(Pagging);
                string[] splitPagging = NoOfPage.Split('.');
                if (splitPagging.Length > 2)
                {
                    if (Convert.ToDecimal(splitPagging[1]) > Convert.ToDecimal(0.1))
                    {
                        splitPagging[1] = "1";
                        No = Convert.ToDecimal(splitPagging[0]);
                        decimal Roundof = Convert.ToDecimal(splitPagging[1]);
                        Pagging = No + Roundof;
                    }
                }
                List<Dictionary<string, object>> Activity = new List<Dictionary<string, object>>();
                Activity = DataManager.ConvertDataTable(dtPage);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, arrActivity = Activity, Pagging = Pagging, noActivity = dtResult.Rows.Count });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion
        #endregion



        //[WebMethod(true)]
        //public string GetMode()
        //{
        //    /*if (!Utils.ValidateSession())
        //    {
        //        return "{\"Session\":\"0\"}";
        //    }*/
        //    DataTable dtResult = new DataTable();
        //    StringBuilder jsonStringR = new StringBuilder();
        //    StringBuilder jsonString = new StringBuilder();
        //    DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        //    retCode = ActivityManager.GetMODE(out dtResult);
        //    //DataManager.DBReturnCode retCode = DataManager.MatchDetails(sCreatedBy, out dtResult);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        //var plantype = objGlobalDefault.sPlanType;
        //        if (dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                jsonStringR.Append("{");
        //                jsonStringR.Append("\"TourType\":\"" + escapejsondata(dr["TourType"].ToString().Replace("\r\n", " ")) + "\",");
        //                if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
        //                jsonStringR.Append("},");
        //            }
        //            if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
        //            jsonString.Append("{\"Session\":\"1\",\"retCode\":\"1\",\"MatchSummary\":[" + jsonStringR + "]}");
        //        }
        //    }
        //    else
        //    {
        //        jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
        //    }

        //    return jsonString.ToString();
        //}



        [WebMethod(true)]
        public string GetMode()
        {
            JavaScriptSerializer obj = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            StringBuilder jsonStringR = new StringBuilder();
            StringBuilder jsonString = new StringBuilder();
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            retCode = ActivityManager.GetMODE(out dtResult);
            //DataManager.DBReturnCode retCode = DataManager.MatchDetails(sCreatedBy, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Mode = new List<Dictionary<string, object>>();
                Mode = DataManager.ConvertDataTable(dtResult);
                return obj.Serialize(new { Session = 1, retCode = 1, dtresult = Mode });

            }
            else
            {
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
            }

            return jsonString.ToString();
        }

        [WebMethod(true)]
        public string Getpriority()
        {
            JavaScriptSerializer obj = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            StringBuilder jsonStringR = new StringBuilder();
            StringBuilder jsonString = new StringBuilder();
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            retCode = ActivityManager.Getpriority(out dtResult);
            //DataManager.DBReturnCode retCode = DataManager.MatchDetails(sCreatedBy, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> priority = new List<Dictionary<string, object>>();
                priority = DataManager.ConvertDataTable(dtResult);
                return obj.Serialize(new { Session = 1, retCode = 1, dtresult = priority });

            }
            else
            {
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
            }

            return jsonString.ToString();
        }


        [WebMethod(true)]
        public string GetSupplier()
        {
            JavaScriptSerializer obj = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            StringBuilder jsonStringR = new StringBuilder();
            StringBuilder jsonString = new StringBuilder();
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            retCode = ActivityManager.GetSupplier(out dtResult);
            //DataManager.DBReturnCode retCode = DataManager.MatchDetails(sCreatedBy, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Supplier = new List<Dictionary<string, object>>();
                Supplier = DataManager.ConvertDataTable(dtResult);
                return obj.Serialize(new { Session = 1, retCode = 1, dtresult = Supplier });

            }
            else
            {
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
            }

            return jsonString.ToString();
        }


        #region Get Activity Details

        [WebMethod(EnableSession = true)]
        public string GetActivityDetails(string Aid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataSet dsResult = new DataSet();
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retcode = ActivityManager.GetDetails(Aid, out dsResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sImages = new List<Dictionary<string, object>>();
                sImages = DataManager.ConvertDataTable(dsResult.Tables[0]);
                List<Dictionary<string, object>> sDetails = new List<Dictionary<string, object>>();
                sDetails = DataManager.ConvertDataTable(dsResult.Tables[1]);
                List<Dictionary<string, object>> sTicketType = new List<Dictionary<string, object>>();
                sTicketType = DataManager.ConvertDataTable(dsResult.Tables[2]);
                List<Dictionary<string, object>> sInclExclu = new List<Dictionary<string, object>>();
                sInclExclu = DataManager.ConvertDataTable(dsResult.Tables[3]);
                List<float> MinPrice = new List<float>(dsResult.Tables[2].Rows.Count);
                float AdultPrice = 0;
                float Child = 0;
                for (int J = 0; J < dsResult.Tables[2].Rows.Count; J++)
                {
                    if (dsResult.Tables[2].Rows[J]["Adult_Price"].ToString() != "")
                        AdultPrice = Convert.ToSingle(dsResult.Tables[2].Rows[J]["Adult_Price"]);
                    else
                        AdultPrice = 0;
                    //Child = Convert.ToSingle(dtPrice.Rows[J]["Child_Price"]);
                    MinPrice.Add(AdultPrice + Child);
                }
                dsResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, arrImages = sImages, arrDetails = sDetails, arrRateTime = sTicketType, arrInclusionExclu = sInclExclu, MinPrice = MinPrice.Min() });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        #endregion

        #region Get Activity By Trip
        [WebMethod(EnableSession = true)]
        public string GetTripDetails(string Aid, string T_Id, string Act_Type)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataSet dsResult = new DataSet();
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retcode = ActivityManager.GetTripDetails(T_Id, Aid, Act_Type, out dsResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sTicketType = new List<Dictionary<string, object>>();
                sTicketType = DataManager.ConvertDataTable(dsResult.Tables[0]);
                dsResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, sTicketType = sTicketType });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region BookActivityDetails
        [WebMethod(EnableSession = true)]
        public string BookActivityDetails(Int64 noAdults, Int64 noChilds, Int64 noChilds2, float AdultPrice, float ChildPrice, float ChildPrice2, string ActivityType, string T_id, string Act_Id, Int64 MaxPax, string BookDate)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Session["TravelDate"] = BookDate;
            float TotalPrice = 0;
            try
            {
                TotalPrice = (noAdults * AdultPrice) + (noChilds * ChildPrice) + (noChilds2 * ChildPrice2);
                Session["TripTotal"] = TotalPrice;
                Session["noAdults"] = noAdults;
                Session["noChilds"] = noChilds;
                Session["noChilds2"] = noChilds2;
                Session["AdultPrice"] = AdultPrice;
                Session["ChildPrice"] = ChildPrice;
                Session["ChildPrice2"] = ChildPrice2;
                Session["T_id"] = T_id;
                Session["Act_Id"] = Act_Id;
                Session["ActivityType"] = ActivityType;
                Session["MaxPax"] = MaxPax;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region GenrateBookingDetails
        [WebMethod(EnableSession = true)]
        public string GenrateBookingDetails()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            float TotalPrice = 0;

            string T_Id = Session["T_id"].ToString(), Aid = Session["Act_Id"].ToString(), Act_Type = Session["ActivityType"].ToString();
            try
            {
                DataSet dsResult = new DataSet();
                DataTable dtResult = new DataTable();
                DataManager.DBReturnCode retcode = ActivityManager.GetActivityByType(T_Id, Aid, Act_Type, out dtResult);
                List<Dictionary<string, object>> sTicketType = new List<Dictionary<string, object>>();
                sTicketType = DataManager.ConvertDataTable(dtResult);
                dsResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, TotalPrice = Session["TripTotal"].ToString(), BookDate = Session["TravelDate"].ToString(), noAdults = Session["noAdults"].ToString(), noChilds = Session["noChilds"].ToString(), noChilds2 = Session["noChilds2"].ToString(), AdultPrice = Session["AdultPrice"].ToString(), ChildPrice = Session["ChildPrice"].ToString(), T_id = Session["T_id"].ToString(), Act_Id = Session["Act_Id"].ToString(), ActivityType = Session["ActivityType"].ToString(), MaxPax = Session["MaxPax"].ToString(), arrActivity = sTicketType });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        //#region Book
        // [WebMethod(EnableSession = true)]
        //public string Book(List<string> Gender, List<string> FirstName,List<string> LastName,List<string> GenderOfChild,List<string>Child ,List<string> ChildAges,string LeadingPax,string MobileNo,string Email,string Address)
        //{
        //    DataSet dsResult = new DataSet();
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    string Aid = Session["Act_Id"].ToString(), T_Id = Session["T_id"].ToString(), Act_Type = Session["ActivityType"].ToString();
        //   DBHelper.DBReturnCode retcode = ActivityManager.GetTripDetails(T_Id ,Aid,Act_Type ,out dsResult);
        //   if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //   {
        //       string StartDate = dsResult.Tables[0].Rows[0]["Valid_from"].ToString();
        //       string Till = dsResult.Tables[0].Rows[0]["Valid_to"].ToString();
        //       //string CutMail = ActivityManager.Book( dsResult.Tables[0].Rows[0]["F1"].ToString(),Act_Type,Session["TripTotal"].ToString(), LeadingPax, Email, MobileNo, StartDate, Till, Gender, FirstName, LastName, GenderOfChild, Child);
        //       string CutMail = ActivityManager.BookActivity(dsResult.Tables[0], Session["TripTotal"].ToString(), LeadingPax, Email, MobileNo, StartDate, Till, Gender, FirstName, LastName, GenderOfChild, Child);
        //       DataManager.DBReturnCode retCode = DataManager.BookingMails("tausifqazi005@gmail.com", "Please Conform Booking", CutMail.Replace("Your Booking Request","Please Conform Request").ToString());
        //       if (retCode == DataManager.DBReturnCode.SUCCESS)
        //       {
        //           retCode = DataManager.BookingMails(Email, "Conform Booking", CutMail.Replace("Please Conform Request","Your Booking Request").ToString());
        //           return jsSerializer.Serialize(new { Session = 1, retCode = 1 });
        //       }
        //       else
        //       {
        //           return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
        //       }
        //   }
        //    else
        //   {
        //       return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //   }
        //}
        //#endregion

        [WebMethod(EnableSession = true)]
        public string ChangeDate(string NewDate)
        {
            Session["TravelDate"] = NewDate;
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #region Book
        [WebMethod(EnableSession = true)]
        public string Book(string FirstName, string LastName, string NoOfAdult, string Child, string Child2, string AdultPrice, string Kids1TotalPrice, string Kids2TotalPrice, string MobileNo, string Email)
        {
            DataSet dsResult = new DataSet();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string Aid = Session["Act_Id"].ToString(), T_Id = Session["T_id"].ToString(), Act_Type = Session["ActivityType"].ToString(), Total = Session["TripTotal"].ToString(), BookDate = Session["TravelDate"].ToString();
            DBHelper.DBReturnCode retcode = ActivityManager.InsertInvoiceVoucher(T_Id, Aid, FirstName, LastName, NoOfAdult, Child, Child2, AdultPrice, Kids1TotalPrice, Kids2TotalPrice, Total, MobileNo, Email, BookDate);
            retcode = ActivityManager.GetTripDetails(T_Id, Aid, Act_Type, out dsResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string StartDate = dsResult.Tables[0].Rows[0]["Valid_from"].ToString();
                string Till = dsResult.Tables[0].Rows[0]["Valid_to"].ToString();
                //string CutMail = ActivityManager.Book( dsResult.Tables[0].Rows[0]["F1"].ToString(),Act_Type,Session["TripTotal"].ToString(), LeadingPax, Email, MobileNo, StartDate, Till, Gender, FirstName, LastName, GenderOfChild, Child);
                string CutMail = ActivityManager.BookActivity(dsResult.Tables[0], Session["TripTotal"].ToString(), FirstName, LastName, NoOfAdult, Child, Child2, Kids1TotalPrice, Kids2TotalPrice, MobileNo, Email);
                DataManager.DBReturnCode retCode = DataManager.BookingMails("tausifqazi005@gmail.com", "Please Conform Booking", CutMail.Replace("Your Booking Request", "Please Conform Request").ToString());
                if (retCode == DataManager.DBReturnCode.SUCCESS)
                {
                    retCode = DataManager.BookingMails(Email, "Conform Booking", CutMail.Replace("Please Conform Request", "Your Booking Request").ToString());
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1 });
                }
                else
                {
                    return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
                }
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region DeleteFile
        [WebMethod(EnableSession = true)]
        public string DeleteFile(string FileNo, string noFiles, string sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                string NewFileName = "";
                string Path = System.Web.HttpContext.Current.Server.MapPath("~/activityImages/" + sid);
                string FileName = FileNo;
                DirectoryInfo dir = new DirectoryInfo(Path);
                FileInfo[] files = null;

                files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    if (file.Name == FileName)
                    {
                        file.Delete();
                        String[] NoFil = noFiles.Split('^');
                        for (int i = 0; i < NoFil.Length - 1; i++)
                        {
                            if (FileNo != NoFil[i])
                            {
                                NewFileName += NoFil[i] + "^";
                            }
                        }
                        break;
                    }
                    else
                    {
                        continue;
                    }

                }
                DataManager.DBReturnCode retCode = ActivityManager.UpdateImages(NewFileName, sid);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }
        #endregion

        #region SetFiles
        [WebMethod(EnableSession = true)]
        public string SetFiles(string sImage, string sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                DataManager.DBReturnCode retCode = ActivityManager.UpdateImages(sImage, sid);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }

        #endregion

        #region Mode&Priority

        //[WebMethod(EnableSession = true)]
        //public string AddMode(string fName)
        //{

        //    //string json = "";
        //    //DateTime sDob = new DateTime(2000, 2, 29);
        //    //DateTime now = new DateTime(2009, 2, 28);
        //    //Int16 Age = CalculateAgeCorrect(sDob, now);
        //    DBHelper.DBReturnCode retCode = ActivityManager.AddMode(fName);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        //if (UserType == "Admin")
        //        //{
        //        //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
        //        //}
        //        //else if (UserType == "Staff")
        //        //{
        //        //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Staff\"}";
        //    }
        //    return json;
        //}

        [WebMethod(EnableSession = true)]
        public string Update_Mode(Int64 Pid, string fName)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.Update_Mode(Pid, fName);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(true)]
        public string DeleteMode(Int64 sid)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.DeleteMode(sid);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetModeActiveDeactive(Int64 Sid, string Status)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.GetModeActiveDeactive(Sid, Status);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string AddPriority(string fName)
        {
            //string json = "";
            //DateTime sDob = new DateTime(2000, 2, 29);
            //DateTime now = new DateTime(2009, 2, 28);
            //Int16 Age = CalculateAgeCorrect(sDob, now);
            DBHelper.DBReturnCode retCode = ActivityManager.AddPriority(fName);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                //if (UserType == "Admin")
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                //}
                //else if (UserType == "Staff")
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Staff\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Update_Priority(Int64 Pid, string fName)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.Update_Priority(Pid, fName);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(true)]
        public string DeletePriority(Int64 sid)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.DeletePriority(sid);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPriorityActiveDeactive(Int64 Sid, string Status)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.GetPriorityActiveDeactive(Sid, Status);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        #endregion

        #region ActivityDetailsAddUpdate

        [WebMethod(EnableSession = true)]
        public string GetActivity(string id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.GetActivity(id, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = sType });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetAllActivity()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.GetAllActivity(out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Allact = new List<Dictionary<string, object>>();
                Allact = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = Allact });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetChildPolicyDetails(string id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.GetChildPolicyDetails(id, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = sType });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string SetOperatingDate(string id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.SetOperatingDate(id, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = sType });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string SetSlots(string id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.SetSlots(id, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = sType });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string AddActivity(string Country, string City, string activityname, string subtitle, string Description, string Attraction, string longitude, string lattitude, string TourNote, string TourType, string Kid1Range, string Kid2Range)
        {
            //string json = "";
            //DateTime sDob = new DateTime(2000, 2, 29);
            //DateTime now = new DateTime(2009, 2, 28);
            //Int16 Age = CalculateAgeCorrect(sDob, now); 
            DataTable dt = new DataTable();

            DBHelper.DBReturnCode retCode = ActivityManager.AddActivity(Country, City, activityname, subtitle, Description, Attraction, longitude, lattitude, TourNote, TourType, Kid1Range, Kid2Range, out dt);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
                //Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
                var Sr_No = dt.Rows[0]["Sr_No"].ToString();
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Sr_No\":\"" + Sr_No + "\"}";
                // json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                //if (UserType == "Admin")
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                //}
                //else if (UserType == "Staff")
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Staff\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateActivityDetails(string id, string Country, string City, string activityname, string subtitle, string Description, string Attraction, string longitude, string lattitude, string TourNote, string TourType)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.UpdateActivityDetails(id, Country, City, activityname, subtitle, Description, Attraction, longitude, lattitude, TourNote, TourType);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(true)]
        public string DeleteActivity(string id)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.DeleteActivity(id);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string GetTourTypes()
        {


            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            StringBuilder jsonStringR = new StringBuilder();
            StringBuilder jsonString = new StringBuilder();
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            retCode = ActivityManager.GetFormList(out dtResult);
            //DataManager.DBReturnCode retCode = DataManager.MatchDetails(sCreatedBy, out dtResult);


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, tblType = sType });

            }
            else
            {
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
            }

            return jsonString.ToString();
        }

        #endregion

        #region Tarrif AddUpdate

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }







        public int MyProperty { get; set; }
        [WebMethod(EnableSession = true)]
        public string AddActivityTariff(string Type, string id, string TeriffID, List<string> SlotID, string Supplier, string ChildageFrm,
            string ChildageUpto, string ChildMinHeight, string SeasonDate1, string SeasonDate2, string Currencytkt, string Nationality, string Adult,
            string Child11, string Child4, string Infant, string Policy, string Offer, string Note, string Inclusion,
            string Exclusion, string Picfrm, string drpfrm, string PickuptHr, string PickuptMin, string DropoftHr, string DropoftMin,
            string Picfrmspl, string drpfrmspl, string PickuptHrspl, string PickuptMinspl, string DropoftHrspl, string DropoftMinspl,
            string Spldate1, string Spldate2, string Currencyspl, string Nationalityspl, string Adultspl, string Childspl11,
            string Childspl4, string Infantspl, string Cancelspl, string Offerspl, string incspl, string excspl, string Terrifspl,
            string ATimeSlot, string SplDate, string SplDateSlot,
            List<string> SplSlotID, List<Inclusion> objInclusion, List<Exclusion> objExclusion, List<ExclusionSpl> objExclusionSpl, List<InclusionSpl> objInclusionSpl)
        {

            string uniqueid = GenerateRandomString(3);
            string[] curren = Currencytkt.Split('^');
            string[] nation = Nationality.Split('^');
            string[] adul = Adult.Split('^');
            string[] chil11 = Child11.Split('^');
            string[] chil4 = Child4.Split('^');
            string[] infa = Infant.Split('^');
            string[] polic = Policy.Split('^');
            string[] offe = Offer.Split('^');
            string[] not = Note.Split('^');
            string[] Pickupfrm = Picfrm.Split('^');
            string[] picktimeHr = PickuptHr.Split('^');
            string[] picktimeMn = PickuptMin.Split('^');
            string[] Dropat = drpfrm.Split('^');
            string[] droptHr = DropoftHr.Split('^');
            string[] droptMn = DropoftMin.Split('^');

            string[] currenspl = Currencyspl.Split('^');
            string[] nationalspl = Nationalityspl.Split('^');
            string[] adulspl = Adultspl.Split('^');
            string[] chil11spl = Childspl11.Split('^');
            string[] chil4spl = Childspl4.Split('^');
            string[] infaspl = Infantspl.Split('^');
            string[] cancelspl = Cancelspl.Split('^');
            string[] offespl = Offerspl.Split('^');
            string[] terrispl = Terrifspl.Split('^');

            string[] Pickupfrmspl = Picfrmspl.Split('^');
            string[] picktimeHrspl = PickuptHrspl.Split('^');
            string[] picktimeMnspl = PickuptMinspl.Split('^');
            string[] Dropatspl = drpfrmspl.Split('^');
            string[] droptHrspl = DropoftHrspl.Split('^');
            string[] droptMnspl = DropoftMinspl.Split('^');



            try
            {
                 helperDataContext db = new helperDataContext();
                 tbl_aeActivityTariff Teriff = db.tbl_aeActivityTariffs.Single(x => x.Act_Id == id && x.T_Id == TeriffID);

                //Teriff.Act_Id = id;
                //Teriff.T_Id = uniqueid;
                //Teriff.Act_Type = Type;
                Teriff.Supplier = Supplier;
                Teriff.Valid_from = SeasonDate1;
                Teriff.Valid_to = SeasonDate2;
                Teriff.Child_Age_Start = ChildageFrm;
                Teriff.Child_Age_Upto = ChildageUpto;
                Teriff.Child_Min_Height = ChildMinHeight;
                //Teriff.Pickup_from = Pickupfrm;
                //Teriff.Pickup_Time = Pickuptime;
                //Teriff.Drop_to = Dropofat;
                //Teriff.Drop_time = Dropoftime;
                if (SplDate == "Yes")
                {

                    Teriff.Spl_Valid_from = Spldate1;
                    Teriff.Spl_Valid_to = Spldate2;


                }
                //DB.tbl_aeActivityTariffs.InsertOnSubmit(Teriff);
                db.SubmitChanges();

                //if (ATimeSlot=="Yes")
                //{
                int i = 0;
                if (SlotID.Count == 0)
                {
                    for (i = 0; i == SlotID.Count; i++)
                    {
                        tbl_aeTimingSlot TSlott = new tbl_aeTimingSlot();
                        TSlott.T_Id = TeriffID;
                        TSlott.Act_Id = id;
                        TSlott.Slot_Id = 0;
                        TSlott.Timing_Type = "Normal";
                        TSlott.Currency = curren[i];
                        TSlott.Nationality = nation[i];
                        TSlott.Adult = adul[i];

                        if (Child11 != "")
                        {
                            TSlott.Child_11_5 = chil11[i];
                        }
                        if (Child4 != "")
                        {
                            TSlott.Child_4_2 = chil4[i];
                        }
                        if (Infant != "")
                        {
                            TSlott.Infant = infa[i];
                        }

                        //TSlott.Child_11_5 = chil11[i];
                        //TSlott.Child_4_2 = chil4[i];
                        //TSlott.Infant = infa[i];

                        TSlott.Cacellation_Policy = polic[i];
                        TSlott.Offer = offe[i];
                        TSlott.Terrif_Note = not[i];

                        TSlott.Pickup_From = Pickupfrm[i];
                        TSlott.Pickup_Time_Hours = picktimeHr[i];
                        TSlott.Pickup_Time_Min = picktimeMn[i];
                        TSlott.Drop_Off_At = Dropat[i];
                        TSlott.Drop_Off_Time_Hours = droptHr[i];
                        TSlott.Drop_Off_Time_Min = droptMn[i];

                        string inclusnn = "";
                        foreach (Inclusion Inclusionn in objInclusion)
                        {
                            // if (Inclusionn.id == Convert.ToInt64(SlotID[i]))
                            inclusnn += Inclusionn.Value;
                        }
                        string exclusnn = "";
                        foreach (Exclusion Exclusionn in objExclusion)
                        {
                            //if (Exclusionn.id == Convert.ToInt64(SlotID[i]))
                            exclusnn += Exclusionn.Value;
                        }
                        TSlott.Inclusion = inclusnn;
                        TSlott.Exclusion = exclusnn;
                        db.tbl_aeTimingSlots.InsertOnSubmit(TSlott);
                        db.SubmitChanges();
                        // continue;
                    }

                    //i++;
                }
                else
                {
                    for (i = 0; i < SlotID.Count; i++)
                    {

                        tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                        TSlot.T_Id = TeriffID;
                        TSlot.Act_Id = id;
                        TSlot.Slot_Id = Convert.ToInt64(SlotID[i]);
                        TSlot.Timing_Type = "Normal";
                        TSlot.Currency = curren[i];
                        TSlot.Nationality = nation[i];
                        TSlot.Adult = adul[i];
                        if (Child11 != "")
                        {
                            TSlot.Child_11_5 = chil11[i];
                        }
                        if (Child4 != "")
                        {
                            TSlot.Child_4_2 = chil4[i];
                        }
                        if (Infant != "")
                        {
                            TSlot.Infant = infa[i];
                        }

                        TSlot.Cacellation_Policy = polic[i];
                        TSlot.Offer = offe[i];
                        TSlot.Terrif_Note = not[i];
                        if (Type != "TKT")
                        {
                            TSlot.Pickup_From = Pickupfrm[i];
                            TSlot.Pickup_Time_Hours = picktimeHr[i];
                            TSlot.Pickup_Time_Min = picktimeMn[i];
                            TSlot.Drop_Off_At = Dropat[i];
                            TSlot.Drop_Off_Time_Hours = droptHr[i];
                            TSlot.Drop_Off_Time_Min = droptMn[i];
                        }


                        string inclusn = "";
                        foreach (Inclusion Inclusionn in objInclusion)
                        {
                            if (Inclusionn.id == Convert.ToInt64(SlotID[i]))
                                inclusn += Inclusionn.Value;
                        }
                        string exclusn = "";
                        foreach (Exclusion Exclusionn in objExclusion)
                        {
                            if (Exclusionn.id == Convert.ToInt64(SlotID[i]))
                                exclusn += Exclusionn.Value;
                        }
                        TSlot.Inclusion = inclusn;
                        TSlot.Exclusion = exclusn;
                        db.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                        db.SubmitChanges();


                    }
                    //i++;
                }


                // }

                if (SplDate == "Yes")
                {
                    int j = 0;

                    if (SplSlotID.Count == 0)
                    {
                        for (j = 0; j == SplSlotID.Count; j++)
                        {
                            tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                            TSlot.T_Id = TeriffID;
                            TSlot.Act_Id = id;
                            TSlot.Slot_Id = 0;
                            TSlot.Timing_Type = "Special";
                            TSlot.Currency = currenspl[j];
                            TSlot.Nationality = nationalspl[j];
                            TSlot.Adult = adulspl[j];

                            if (Childspl11 != "")
                            {
                                TSlot.Child_11_5 = chil11spl[j];
                            }
                            if (Childspl4 != "")
                            {
                                TSlot.Child_4_2 = chil4spl[j];
                            }
                            if (Infantspl != "")
                            {
                                TSlot.Infant = infaspl[j];
                            }
                            //TSlot.Child_11_5 = chil11spl[j];
                            //TSlot.Child_4_2 = chil4spl[j];
                            //TSlot.Infant = infaspl[j];

                            TSlot.Cacellation_Policy = cancelspl[j];
                            TSlot.Offer = offespl[j];
                            TSlot.Terrif_Note = terrispl[j];
                            TSlot.Spl_Valid_from = Spldate1;
                            TSlot.Spl_Valid_to = Spldate2;

                            if (Type != "TKT")
                            {
                                TSlot.Pickup_From = Pickupfrmspl[j];
                                TSlot.Pickup_Time_Hours = picktimeHrspl[j];
                                TSlot.Pickup_Time_Min = picktimeMnspl[j];
                                TSlot.Drop_Off_At = Dropatspl[j];
                                TSlot.Drop_Off_Time_Hours = droptHrspl[j];
                                TSlot.Drop_Off_Time_Min = droptMnspl[j];
                            }
                            string inclusion = "";
                            foreach (InclusionSpl InclusionSpl in objInclusionSpl)
                            {
                                //if (InclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                inclusion += InclusionSpl.Value;
                            }
                            string exclusion = "";
                            foreach (ExclusionSpl exclusionSpl in objExclusionSpl)
                            {
                                // if (exclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                exclusion += exclusionSpl.Value;
                            }
                            TSlot.Inclusion = inclusion;
                            TSlot.Exclusion = exclusion;
                            db.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                            db.SubmitChanges();
                        }

                        // j++;
                    }
                    else
                    {
                        for (j = 0; j < SplSlotID.Count; j++)
                        {


                            tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                            TSlot.T_Id = TeriffID;
                            TSlot.Act_Id = id;
                            TSlot.Slot_Id = Convert.ToInt64(SplSlotID[j]);
                            TSlot.Timing_Type = "Special";
                            TSlot.Currency = currenspl[j];
                            TSlot.Nationality = nationalspl[j];
                            TSlot.Adult = adulspl[j];

                            if (Childspl11 != "")
                            {
                                TSlot.Child_11_5 = chil11spl[j];
                            }
                            if (Childspl4 != "")
                            {
                                TSlot.Child_4_2 = chil4spl[j];
                            }
                            if (Infantspl != "")
                            {
                                TSlot.Infant = infaspl[j];
                            }

                            //TSlot.Child_11_5 = chil11spl[j];
                            //TSlot.Child_4_2 = chil4spl[j];
                            //TSlot.Infant = infaspl[j];

                            TSlot.Cacellation_Policy = cancelspl[j];
                            TSlot.Offer = offespl[j];
                            TSlot.Terrif_Note = terrispl[j];
                            TSlot.Spl_Valid_from = Spldate1;
                            TSlot.Spl_Valid_to = Spldate2;
                            if (Type != "TKT")
                            {
                                TSlot.Pickup_From = Pickupfrmspl[j];
                                TSlot.Pickup_Time_Hours = picktimeHrspl[j];
                                TSlot.Pickup_Time_Min = picktimeMnspl[j];
                                TSlot.Drop_Off_At = Dropatspl[j];
                                TSlot.Drop_Off_Time_Hours = droptHrspl[j];
                                TSlot.Drop_Off_Time_Min = droptMnspl[j];
                            }
                            string inclusion = "";
                            foreach (InclusionSpl InclusionSpl in objInclusionSpl)
                            {
                                if (InclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                    inclusion += InclusionSpl.Value;
                            }
                            string exclusion = "";
                            foreach (ExclusionSpl exclusionSpl in objExclusionSpl)
                            {
                                if (exclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                    exclusion += exclusionSpl.Value;
                            }
                            TSlot.Inclusion = inclusion;
                            TSlot.Exclusion = exclusion;
                            db.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                            db.SubmitChanges();


                        }

                        //j++;
                    }


                }




                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }



            return json;

            ////string json = "";
            ////DateTime sDob = new DateTime(2000, 2, 29);
            ////DateTime now = new DateTime(2009, 2, 28);
            ////Int16 Age = CalculateAgeCorrect(sDob, now);
            //DBHelper.DBReturnCode retCode = ActivityManager.AddActivityTariff(Type, id, SlotID, Supplier, ChildageFrm, ChildageUpto, ChildMinHeight, SeasonDate1, SeasonDate2, Currencytkt, Adult, Child11, Child4, Infant, Policy, Offer, Note, Inclusion, Exclusion, Spldate1, Spldate2, Currencyspl, Adultspl, Childspl11, Childspl4, Infantspl, Cancelspl, Offerspl, incspl, excspl, Terrifspl, ATimeSlot, SplDate, SplDateSlot);
            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            //}

        }



        [WebMethod(EnableSession = true)]
        public string UpdateTariffDetails(string id, string validfrom, string validto, string priority, string currency, string liveselling, Double minpax, Double maxpax, Double adultheight, Double adultprice, Double childprice, Double childprice2, Double infantprice, Double childage, string activitytype, string tariffnote, string starttime, string endtime, string duration, string pickupreport, string pickupfrom, string droptime, string dropto, string days, string inclusion, string exclusion, Double adultagestart, string TrrifId)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.UpdateTariffDetails(id, validfrom, validto, priority, currency, liveselling, minpax, maxpax, adultheight, adultprice, childprice, childprice2, infantprice, childage, activitytype, tariffnote, starttime, endtime, duration, pickupreport, pickupfrom, droptime, dropto, days, inclusion, exclusion, adultagestart, TrrifId);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(true)]
        public string DeleteActivityTariff(string id, string TrrifId)
        {
            DBHelper.DBReturnCode retCode = ActivityManager.DeleteActivityTariff(id, TrrifId);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }



        [WebMethod(EnableSession = true)]
        public string GetActivityTariff(string nid, string type)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string Texts = "";
            retCode = ActivityManager.GetActivityTariff1(nid, type, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> sType = new List<Dictionary<string, object>>();
                sType = DataManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = sType });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion



        #region GetMyActivity
        [WebMethod(EnableSession = true)]
        public string GetMyActivity()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            GlobalDefaultTransfers ObjGlobalDefault = new GlobalDefaultTransfers();
            ObjGlobalDefault = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            try
            {
                if (ObjGlobalDefault != null)
                {
                    DataSet dsresult = new DataSet();
                    retCode = ActivityManager.GetMyActivity(ObjGlobalDefault.sEmail, out dsresult);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        List<Dictionary<string, object>> MyActivity = new List<Dictionary<string, object>>();
                        MyActivity = DataManager.ConvertDataTable(dsresult.Tables[0]);
                        return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtTable = MyActivity });
                    }
                    else
                    {
                        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
                    }
                }
                else
                {
                    return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
                }

                //return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion



        [WebMethod(EnableSession = true)]
        public string SaveActivity(string activityname, string subtitle, string Country, string City, string Location,
            string Description, string ChildrenAllowed, float ChildAgeFrom, float ChildAgeUpTo,
            float SmallChildAgeUpTo, string allowheight, float ChildMinHight, string Infantallowed,
            float MaxNoofInfant, string AllowMaxChild, float MaxChild, string Attraction, string TourType,
            float MinCapacity, float MaximumCapacity, string TourNote, string ActivityType, string PriorityType,
            string OperationDays, string OperationDaysTKT, string OperationDaysSIC, string OperationDaysPVT, List<string> datefrom, List<string> dateTill, string FulYear, List<string> Name, List<string> TourStartH, List<string> TourStartM,
            List<string> TourEndH, List<string> TourEndM, List<string> PickUpFrom, List<string> PickUpTimeH, List<string> PickUpTimeM, List<string> DropOffAt, List<string> DropOffTimeH, List<string> DropOffTimeM, List<string> PriorityTyp, string AllowSlot)
        {

            string ID = "";
            Decimal Balance = 0;
            Decimal withdrawl = 0;
            try
            {
                using (var DB = new helperDataContext())
                {
                    //string DateTimes = DateTime.Now.ToString("dd-MM-yyyy");

                    tbl_aeActivityMaster Prodoct = new tbl_aeActivityMaster();
                    string uniqueid = ActivityManager.GenerateRandomString(3);
                    Prodoct.Sr_No = uniqueid;
                    Prodoct.Act_Name = activityname;
                    Prodoct.Sub_Title = subtitle;
                    Prodoct.Country = Country;
                    Prodoct.City = City;
                    Prodoct.Location = Location;
                    Prodoct.Description = Description;
                    Prodoct.Tour_Type = TourType;
                    Prodoct.Allowed_Child = ChildrenAllowed;
                    Prodoct.Allow_Height = allowheight;
                    Prodoct.ALlow_Infant = Infantallowed;
                    Prodoct.Allow_Maxchild = AllowMaxChild;
                    Prodoct.Attractions = Attraction;
                    Prodoct.Min_Capacity = MinCapacity;
                    Prodoct.Max_Capacity = MaximumCapacity;
                    Prodoct.Tour_Note = TourNote;
                    Prodoct.Full_Year = FulYear;
                    //if (FulYear == "No")
                    //{
                    Prodoct.Operation_Days = OperationDays;
                    //}

                    Prodoct.Slot_Allow = AllowSlot;
                    Prodoct.Status = true;
                    DB.tbl_aeActivityMasters.InsertOnSubmit(Prodoct);
                    DB.SubmitChanges();

                    if (ChildrenAllowed == "Yes")
                    {
                        tbl_aeActivityChildPolicy ChildPolicy = new tbl_aeActivityChildPolicy();
                        ChildPolicy.Act_Id = uniqueid;
                        ChildPolicy.Child_Age_From = ChildAgeFrom;
                        ChildPolicy.Child_Age_Upto = ChildAgeUpTo;
                        ChildPolicy.Small_Child_Age_Upto = SmallChildAgeUpTo;
                        ChildPolicy.Allow_Height = allowheight;

                        ChildPolicy.Child_Min_Height = ChildMinHight;
                        ChildPolicy.Infant_Allow = Infantallowed;

                        ChildPolicy.No_Of_Infant = MaxNoofInfant;
                        ChildPolicy.Max_Child = MaxChild;
                        DB.tbl_aeActivityChildPolicies.InsertOnSubmit(ChildPolicy);
                        DB.SubmitChanges();
                    }

                    String[] actType = ActivityType.Split(';');

                    string teriffidd = "";
                    for (int i = 0; i < actType.Length - 1; i++)
                    {

                        if (actType[i] == "All")
                        {

                            string Acct = "TKT" + ";" + "SIC";

                            String[] AcctType = Acct.Split(';');
                            for (int J = 0; J < AcctType.Length; J++)
                            {

                                string Tid = ActivityManager.GenerateRandomString(3);
                                tbl_aeActivityTariff Teriff = new tbl_aeActivityTariff();
                                Teriff.T_Id = Tid;
                                Teriff.Act_Id = uniqueid;
                                Teriff.Act_Type = AcctType[J];
                                Teriff.Priority = PriorityType;
                                Teriff.Attraction = Attraction;
                                Teriff.Tour_Type = TourType;
                                if (AcctType[J] == "TKT")
                                {
                                    Teriff.OperationsDays = OperationDaysTKT;
                                }
                                else if (AcctType[J] == "SIC")
                                {
                                    Teriff.OperationsDays = OperationDaysSIC;
                                }
                                else
                                {
                                    Teriff.OperationsDays = OperationDaysPVT;
                                }
                                //Teriff.Supplier = Supplier;
                                //Teriff.Status = Status;
                                DB.tbl_aeActivityTariffs.InsertOnSubmit(Teriff);
                                DB.SubmitChanges();
                                teriffidd += Tid + "^";
                            }

                            break;
                        }
                        else
                        {
                            string Tidd = ActivityManager.GenerateRandomString(3);
                            tbl_aeActivityTariff Terifff = new tbl_aeActivityTariff();
                            Terifff.T_Id = Tidd;
                            Terifff.Act_Id = uniqueid;

                            Terifff.Act_Id = uniqueid;
                            Terifff.Act_Type = actType[i];
                            Terifff.Priority = PriorityType;
                            Terifff.Attraction = Attraction;
                            Terifff.Tour_Type = TourType;
                            if (actType[i] == "TKT")
                            {
                                Terifff.OperationsDays = OperationDaysTKT;
                            }
                            else if (actType[i] == "SIC")
                            {
                                Terifff.OperationsDays = OperationDaysSIC;
                            }
                            else
                            {
                                Terifff.OperationsDays = OperationDaysPVT;
                            }
                            DB.tbl_aeActivityTariffs.InsertOnSubmit(Terifff);
                            DB.SubmitChanges();
                            teriffidd += Tidd + "^";
                        }

                    }

                    if (FulYear == "No")
                    {
                        //String[] DateFrm = datefrom.Split(';');
                        //String[] Datetil = dateTill.Split(';');
                        int i = 0;
                        foreach (string Date in datefrom)
                        {

                            tbl_aeActivityOperatingTime OprTime = new tbl_aeActivityOperatingTime();
                            OprTime.Act_Id = uniqueid;
                            OprTime.Operating_From = Date;
                            OprTime.Operating_Till = dateTill[i];
                            DB.tbl_aeActivityOperatingTimes.InsertOnSubmit(OprTime);
                            DB.SubmitChanges();
                            i++;


                        }

                    }
                    else
                    {

                        //DateTime Today = DateTime.Today;
                        //string TodayDate = Today.ToString("dd-MM-yyyy");
                        //string tilldate = TodayDate.AddYears(1);

                        DateTime Today = DateTime.Today;
                        string TodayDate = Today.ToString("dd-MM-yyyy");
                        DateTime todays = Convert.ToDateTime(TodayDate);
                        DateTime tdate = todays.AddYears(1);
                        string tilldate = tdate.ToString("dd-MM-yyyy");


                        tbl_aeActivityOperatingTime OprTime = new tbl_aeActivityOperatingTime();
                        OprTime.Act_Id = uniqueid;
                        OprTime.Operating_From = TodayDate;
                        OprTime.Operating_Till = tilldate;
                        DB.tbl_aeActivityOperatingTimes.InsertOnSubmit(OprTime);
                        DB.SubmitChanges();




                    }
                    if (AllowSlot == "Yes")
                    {
                        int i = 0;
                        foreach (string Slot in Name)
                        {

                            tbl_aeActivitySlot Tslot = new tbl_aeActivitySlot();
                            Tslot.Act_Id = uniqueid;
                            Tslot.Slot_Name = Slot;
                            Tslot.Tour_Start_Hours = TourStartH[i];
                            Tslot.Tour_Start_Min = TourStartM[i];
                            Tslot.Tour_End_Hours = TourEndH[i];
                            Tslot.Tour_End_Min = TourEndM[i];
                            if (PickUpFrom.Count == 0)
                            {
                                Tslot.Pickup_From = null;
                            }
                            else
                            {
                                Tslot.Pickup_From = PickUpFrom[i];
                            }
                            if (PickUpTimeH.Count == 0)
                            {
                                Tslot.Pickup_Time_Hours = null;
                            }
                            else
                            {
                                Tslot.Pickup_Time_Hours = PickUpTimeH[i];
                            }
                            if (PickUpTimeM.Count == 0)
                            {
                                Tslot.Pickup_Time_Min = null;
                            }
                            else
                            {
                                Tslot.Pickup_Time_Min = PickUpTimeM[i];
                            }
                            if (DropOffAt.Count == 0)
                            {
                                Tslot.Drop_Off_At = null;
                            }
                            else
                            {
                                Tslot.Drop_Off_At = DropOffAt[i];
                            }
                            if (DropOffTimeH.Count == 0)
                            {
                                Tslot.Drop_Off_Time_Hours = null;
                            }
                            else
                            {
                                Tslot.Drop_Off_Time_Hours = DropOffTimeH[i];
                            }

                            if (DropOffTimeM.Count == 0)
                            {
                                Tslot.Drop_Off_Time_Min = null;
                            }
                            else
                            {
                                Tslot.Drop_Off_Time_Min = DropOffTimeM[i];
                            }


                            Tslot.Priority_Type = PriorityTyp[i];
                            DB.tbl_aeActivitySlots.InsertOnSubmit(Tslot);
                            DB.SubmitChanges();
                            i++;


                        }
                    }
                    //ID = Prodoct.nID.ToString();
                    //tbl_MainBalance Prodoctt = new tbl_MainBalance();

                    //Prodoctt.Member_ID = Convert.ToInt64(ID);
                    //Prodoctt.Withdrawl = withdrawl;
                    //Prodoctt.Balance = Balance;
                    //DB.tbl_MainBalances.InsertOnSubmit(Prodoctt);
                    //DB.SubmitChanges();

                    //tbl_Register Prodocttt = DB.tbl_Registers.Single(x => (x.nID == Convert.ToInt64(ID)));
                    ////tbl_Register Prodocttt = new tbl_Register();
                    //Prodocttt.Unique_ID = Convert.ToInt64(ID);
                    //DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Sr_No\":\"" + Prodoct.Sr_No + "\",\"teriffidd\":\"" + teriffidd + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }




        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                    //NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }


        [WebMethod(EnableSession = true)]
        public string GetActivityRatelist(string id)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = ((from Time in DB.tbl_aeActivityOperatingTimes
                                 join ActivityDetail in DB.tbl_aeActivityTariffs on Time.Act_Id equals ActivityDetail.Act_Id
                                 where ActivityDetail.Act_Id == id

                                 select new
                                 {
                                     Time.P_Id,
                                     Time.Time_Id,
                                     Time.Act_Id,
                                     Time.Operating_From,
                                     Time.Operating_Till,
                                     ActivityDetail.Act_Type,
                                     ActivityDetail.Adult_Price,
                                     ActivityDetail.Child_Price,
                                     ActivityDetail.Child_Price2,
                                     ActivityDetail.Supplier,
                                     ActivityDetail.Status
                                 }).Distinct()).ToList();

                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrRateList\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(true)]
        public string SearchTeriff(string id, string From, string To, string RatesFor, string Slot, string Status, string Supplier, string SupplierName)
        {
            using (var DB = new helperDataContext())
            {
                var ListChildPolicy = ((from ChildPolicy in DB.tbl_aeActivityChildPolicies
                                        where ChildPolicy.Act_Id == id

                                        select new
                                        {

                                            ChildPolicy.Child_Age_From,
                                            ChildPolicy.Child_Age_Upto,
                                            ChildPolicy.Small_Child_Age_Upto,

                                        }).Distinct()).ToList();


                DataTable dtResult = (DataTable)Session["RateList"];
                Int32 Get = 20;
                string jsonString = "";
                JavaScriptSerializer objSerializer = new JavaScriptSerializer();
                DataTable dtTable = dtResult.Clone();
                foreach (DataRow dr in dtResult.Rows)
                {

                    dtTable.ImportRow(dr);

                }

                DataView myDataView = dtResult.DefaultView;
                DataTable SorteddtResult = myDataView.ToTable();
                DataRow[] rows = null;


                if (From != "" && To == "")
                {
                    DateTime dFrom = ConvertDateTime(From);
                    //DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {
                        string[] splitfdt = dtTable.Rows[i]["Valid_from"].ToString().Split('^');
                        string[] splittdt = dtTable.Rows[i]["Valid_to"].ToString().Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtfrm <= dFrom)
                            {
                                rows = dtTable.Select("(Sid=" + dtTable.Rows[i]["Sid"].ToString() + ") ");
                                SorteddtResult.Rows.Add(rows[0].ItemArray);
                                break;
                            }


                        }

                    }
                    if (rows == null)
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                    else
                    {
                        //SorteddtResult = rows.CopyToDataTable();
                    }

                }

                if (To != "" && From == "")
                {
                    //DateTime dFrom = ConvertDateTime(From);
                    DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {
                        string[] splitfdt = dtTable.Rows[i]["Valid_from"].ToString().Split('^');
                        string[] splittdt = dtTable.Rows[i]["Valid_to"].ToString().Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtto >= dTo)
                            {
                                rows = dtTable.Select("(Sid=" + dtTable.Rows[i]["Sid"].ToString() + ") ");
                                SorteddtResult.Rows.Add(rows[0].ItemArray);
                                break;
                            }


                        }

                    }
                    if (rows == null)
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                    else
                    {
                        //SorteddtResult = rows.CopyToDataTable();
                    }

                }

                if (From != "" && To != "")
                {
                    DateTime dFrom = ConvertDateTime(From);
                    DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {
                        string[] splitfdt = dtTable.Rows[i]["Valid_from"].ToString().Split('^');
                        string[] splittdt = dtTable.Rows[i]["Valid_to"].ToString().Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtfrm <= dFrom && dtto >= dTo)
                            {
                                rows = dtTable.Select("(Sid=" + dtTable.Rows[i]["Sid"].ToString() + ") ");
                                SorteddtResult.Rows.Add(rows[0].ItemArray);
                                break;
                            }


                        }

                    }
                    if (rows == null)
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                    else
                    {
                        //SorteddtResult = rows.CopyToDataTable();
                    }

                }

                if (RatesFor != "")
                {
                    rows = SorteddtResult.Select("(Act_Type like '%" + RatesFor + "%')");
                    if (rows.Length != 0)
                    {
                        SorteddtResult = rows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }

                if (Slot != "")
                {
                    rows = SorteddtResult.Select("(SlotName like '%" + Slot + "%')");
                    if (rows.Length != 0)
                    {
                        SorteddtResult = rows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                //if (Status != "")
                //{
                //    rows = SorteddtResult.Select("(TravelDate like '%" + Status + "%')");
                //    if (rows.Length != 0)
                //    {
                //        SorteddtResult = rows.CopyToDataTable();
                //    }
                //    else
                //    {
                //        SorteddtResult = null;
                //        SorteddtResult = dtResult.Clone();
                //    }
                //}


                if (SupplierName != "")
                {
                    rows = SorteddtResult.Select("(Supplier like '%" + SupplierName + "%')");
                    if (rows.Length != 0)
                    {
                        SorteddtResult = rows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }




                try
                {
                    IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0).Take(Get);
                    SorteddtResult = allButFirst.CopyToDataTable();
                    jsonString = "";
                    Session["dtRateListSearch"] = SorteddtResult;
                    List<Dictionary<string, object>> List_RateList = new List<Dictionary<string, object>>();
                    List_RateList = JsonStringManager.ConvertDataTable(SorteddtResult);

                    dtResult.Dispose();
                    objSerializer.MaxJsonLength = Int32.MaxValue;

                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                    SorteddtResult.Dispose();
                    objSerializer.MaxJsonLength = Int32.MaxValue;
                    return objSerializer.Serialize(new { retCode = 1, Session = 1, Arr = List_RateList, ArrChildPolicy = ListChildPolicy });

                }
                catch
                {
                    SorteddtResult = dtTable.Clone();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";

                }
                return jsonString;
            }
        }


        [WebMethod(true)]
        public string SearchTerifff(string id, string From, string To, string RatesFor, string Slot, string Status, string Supplier, string SupplierName)
        {
            using (var DB = new helperDataContext())
            {
                var ListChildPolicy = ((from ChildPolicy in DB.tbl_aeActivityChildPolicies
                                        where ChildPolicy.Act_Id == id

                                        select new
                                        {

                                            ChildPolicy.Child_Age_From,
                                            ChildPolicy.Child_Age_Upto,
                                            ChildPolicy.Small_Child_Age_Upto,

                                        }).Distinct()).ToList();

                var List = ((from Tarrif in DB.tbl_aeActivityTariffs
                             join Timing in DB.tbl_aeTimingSlots on Tarrif.T_Id equals Timing.T_Id
                             join Supp in DB.tbl_APIDetails on Tarrif.Supplier equals (Supp.sid).ToString()
                             //join ActivityDetail in DB.tbl_aeActivityTariffs on Time.Act_Id equals ActivityDetail.Act_Id
                             where Tarrif.Act_Id == id && Tarrif.Act_Id == Timing.Act_Id


                             select new
                             {

                                 Tarrif.Act_Id,
                                 Tarrif.T_Id,
                                 Tarrif.Valid_from,
                                 Tarrif.Valid_to,
                                 Tarrif.Spl_Valid_from,
                                 Tarrif.Spl_Valid_to,
                                 Tarrif.Act_Type,

                                 Timing.Slot_Id,
                                 Timing.Adult,
                                 Timing.Child_11_5,
                                 Timing.Child_4_2,
                                 Timing.Currency,
                                 Timing.Timing_Type,
                                 Supp.Supplier,
                                 Timing.Sid,
                                 SlotName = DB.tbl_aeActivitySlots.Single(s => s.Slot_Id == Timing.Slot_Id).Slot_Name,



                             }).Distinct()).ToList();


                string jsonString = "";
                JavaScriptSerializer objSerializer = new JavaScriptSerializer();

                ListSearch = new List<Ratelistdetails>();
                ListRatee = new List<ListRate>();
                List<string> Dataa = new List<string>();
                if (From != "" && To != "")
                {
                    DateTime dFrom = ConvertDateTime(From);
                    DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < List.Count; i++)
                    {
                        string[] splitfdt = List[i].Valid_from.Split('^');
                        string[] splittdt = List[i].Valid_to.Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtfrm <= dFrom && dtto >= dTo)
                            {
                                //List = List.Where(s => ConvertDateTime(s.Valid_from) >= dFrom && ConvertDateTime(s.Valid_to) <= dTo).ToList();


                                ListSearch.Add(new Ratelistdetails
                            {
                                ActID = List[i].Act_Id,
                                Tid = List[i].T_Id,
                                ValidFrom = List[i].Valid_from,
                                ValidTo = List[i].Valid_to,
                                SValidFrom = List[i].Spl_Valid_from,
                                SValidTo = List[i].Spl_Valid_to,
                                Actype = List[i].Act_Type,
                                SlotID = Convert.ToInt64(List[i].Slot_Id),
                                Adult = List[i].Adult,
                                Child_11_5 = List[i].Child_11_5,
                                Child_4_2 = List[i].Child_4_2,
                                Currency = List[i].Currency,
                                TimingTpe = List[i].Timing_Type,
                                Supplier = List[i].Supplier,
                                Sid = List[i].Sid,
                                SlotName = List[i].SlotName
                            });

                            }

                        }

                    }

                }

                if (From != "" && To == "")
                {
                    DateTime dFrom = ConvertDateTime(From);
                    // DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < List.Count; i++)
                    {
                        string[] splitfdt = List[i].Valid_from.Split('^');
                        string[] splittdt = List[i].Valid_to.Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtfrm <= dFrom)
                            {
                                //List = List.Where(s => ConvertDateTime(s.Valid_from) >= dFrom && ConvertDateTime(s.Valid_to) <= dTo).ToList();


                                ListSearch.Add(new Ratelistdetails
                                {
                                    ActID = List[i].Act_Id,
                                    Tid = List[i].T_Id,
                                    ValidFrom = List[i].Valid_from,
                                    ValidTo = List[i].Valid_to,
                                    SValidFrom = List[i].Spl_Valid_from,
                                    SValidTo = List[i].Spl_Valid_to,
                                    Actype = List[i].Act_Type,
                                    SlotID = Convert.ToInt64(List[i].Slot_Id),
                                    Adult = List[i].Adult,
                                    Child_11_5 = List[i].Child_11_5,
                                    Child_4_2 = List[i].Child_4_2,
                                    Currency = List[i].Currency,
                                    TimingTpe = List[i].Timing_Type,
                                    Supplier = List[i].Supplier,
                                    Sid = List[i].Sid,
                                    SlotName = List[i].SlotName
                                });

                            }

                        }

                    }

                }

                if (From == "" && To != "")
                {
                    //DateTime dFrom = ConvertDateTime(From);
                    DateTime dTo = ConvertDateTime(To);

                    DateTime Today = DateTime.Today;
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = Convert.ToDateTime(TodayDate);
                    for (int i = 0; i < List.Count; i++)
                    {
                        string[] splitfdt = List[i].Valid_from.Split('^');
                        string[] splittdt = List[i].Valid_to.Split('^');

                        for (int j = 0; j < splitfdt.Length - 1; j++)
                        {
                            DateTime dtfrm = ConvertDateTime(splitfdt[j]);
                            DateTime dtto = ConvertDateTime(splittdt[j]);
                            if (dtto >= dTo)
                            {
                                //List = List.Where(s => ConvertDateTime(s.Valid_from) >= dFrom && ConvertDateTime(s.Valid_to) <= dTo).ToList();


                                ListSearch.Add(new Ratelistdetails
                                {
                                    ActID = List[i].Act_Id,
                                    Tid = List[i].T_Id,
                                    ValidFrom = List[i].Valid_from,
                                    ValidTo = List[i].Valid_to,
                                    SValidFrom = List[i].Spl_Valid_from,
                                    SValidTo = List[i].Spl_Valid_to,
                                    Actype = List[i].Act_Type,
                                    SlotID = Convert.ToInt64(List[i].Slot_Id),
                                    Adult = List[i].Adult,
                                    Child_11_5 = List[i].Child_11_5,
                                    Child_4_2 = List[i].Child_4_2,
                                    Currency = List[i].Currency,
                                    TimingTpe = List[i].Timing_Type,
                                    Supplier = List[i].Supplier,
                                    Sid = List[i].Sid,
                                    SlotName = List[i].SlotName
                                });

                            }

                        }

                    }

                }


                if (RatesFor != "")
                {
                    List = List.Where(s => s.Act_Type == RatesFor).ToList();
                    foreach (var item in List)
                    {
                        ListSearch.Add(new Ratelistdetails
                               {
                                   ActID = item.Act_Id,
                                   Tid = item.T_Id,
                                   ValidFrom = item.Valid_from,
                                   ValidTo = item.Valid_to,
                                   SValidFrom = item.Spl_Valid_from,
                                   SValidTo = item.Spl_Valid_to,
                                   Actype = item.Act_Type,
                                   SlotID = Convert.ToInt64(item.Slot_Id),
                                   Adult = item.Adult,
                                   Child_11_5 = item.Child_11_5,
                                   Child_4_2 = item.Child_4_2,
                                   Currency = item.Currency,
                                   TimingTpe = item.Timing_Type,
                                   Supplier = item.Supplier,
                                   Sid = item.Sid,
                                   SlotName = item.SlotName
                               });
                    }

                }

                if (Slot != "")
                {
                    List = List.Where(s => s.SlotName == Slot).ToList();
                    foreach (var item in List)
                    {
                        ListSearch.Add(new Ratelistdetails
                        {
                            ActID = item.Act_Id,
                            Tid = item.T_Id,
                            ValidFrom = item.Valid_from,
                            ValidTo = item.Valid_to,
                            SValidFrom = item.Spl_Valid_from,
                            SValidTo = item.Spl_Valid_to,
                            Actype = item.Act_Type,
                            SlotID = Convert.ToInt64(item.Slot_Id),
                            Adult = item.Adult,
                            Child_11_5 = item.Child_11_5,
                            Child_4_2 = item.Child_4_2,
                            Currency = item.Currency,
                            TimingTpe = item.Timing_Type,
                            Supplier = item.Supplier,
                            Sid = item.Sid,
                            SlotName = item.SlotName
                        });
                    }
                }
                //if (Status != "")
                //{
                //    rows = SorteddtResult.Select("(TravelDate like '%" + Status + "%')");
                //    if (rows.Length != 0)
                //    {
                //        SorteddtResult = rows.CopyToDataTable();
                //    }
                //    else
                //    {
                //        SorteddtResult = null;
                //        SorteddtResult = dtResult.Clone();
                //    }
                //}


                if (SupplierName != "")
                {
                    List = List.Where(s => s.Supplier == SupplierName).ToList();
                    foreach (var item in List)
                    {
                        ListSearch.Add(new Ratelistdetails
                        {
                            ActID = item.Act_Id,
                            Tid = item.T_Id,
                            ValidFrom = item.Valid_from,
                            ValidTo = item.Valid_to,
                            SValidFrom = item.Spl_Valid_from,
                            SValidTo = item.Spl_Valid_to,
                            Actype = item.Act_Type,
                            SlotID = Convert.ToInt64(item.Slot_Id),
                            Adult = item.Adult,
                            Child_11_5 = item.Child_11_5,
                            Child_4_2 = item.Child_4_2,
                            Currency = item.Currency,
                            TimingTpe = item.Timing_Type,
                            Supplier = item.Supplier,
                            Sid = item.Sid,
                            SlotName = item.SlotName
                        });
                    }
                }

                if (List.Count > 0)
                {
                    json = jsSerializer.Serialize(List);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    return objSerializer.Serialize(new { retCode = 1, Session = 1, Arr = ListSearch, ArrChildPolicy = ListChildPolicy });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;

            }
        }

        public class Ratelistdetails
        {
            public string ActID { get; set; }
            public string Tid { get; set; }
            public string ValidFrom { get; set; }
            public string ValidTo { get; set; }
            public string SValidFrom { get; set; }
            public string SValidTo { get; set; }
            public string Actype { get; set; }
            public Int64 SlotID { get; set; }
            public string Adult { get; set; }
            public string Child_11_5 { get; set; }
            public string Child_4_2 { get; set; }
            public string Currency { get; set; }
            public string TimingTpe { get; set; }
            public string Supplier { get; set; }
            public Int64 Sid { get; set; }
            public string SlotName { get; set; }

        }

        public class ListRate
        {
            public List<Ratelistdetails> List_Rate { get; set; }
        }

        //[WebMethod(EnableSession = true)]
        //public string SearchTeriff(string id, string From, string To, string RatesFor, string Status, string Supplier, string SupplierName)
        //{
        //    try
        //    {
        //        var List = ((from Tarrif in DB.tbl_aeActivityTariffs
        //                     join Timing in DB.tbl_aeTimingSlots on Tarrif.T_Id equals Timing.T_Id
        //                     join ChildPolicy in DB.tbl_aeActivityChildPolicies on Tarrif.Act_Id equals ChildPolicy.Act_Id
        //                     join Supp in DB.tbl_APIDetails on Tarrif.Supplier equals (Supp.sid).ToString()

        //                     where Tarrif.Act_Id == id && Tarrif.Act_Id == Timing.Act_Id //&& Tarrif.Act_Type == RatesFor

        //                     select new
        //                     {

        //                         Tarrif.Act_Id,
        //                         Tarrif.T_Id,
        //                         Tarrif.Valid_from,
        //                         Tarrif.Valid_to,
        //                         Tarrif.Spl_Valid_from,
        //                         Tarrif.Spl_Valid_to,
        //                         Tarrif.Act_Type,

        //                         Timing.Slot_Id,
        //                         Timing.Adult,
        //                         Timing.Child_11_5,
        //                         Timing.Child_4_2,
        //                         Timing.Timing_Type,
        //                         Timing.Currency,
        //                         ChildPolicy.Child_Age_From,
        //                         ChildPolicy.Child_Age_Upto,
        //                         ChildPolicy.Small_Child_Age_Upto,
        //                         Supp.Supplier,
        //                         Timing.Sid
        //                     }).Distinct()).ToList();


        //        DataTable listing = ConvertToDatatable(List);

        //        ////////////////////////
        //        Int32 Get = 20;
        //        string jsonString = "";
        //        JavaScriptSerializer objSerializer = new JavaScriptSerializer();

        //        DataTable dtTable = listing.Clone();
        //        foreach (DataRow dr in listing.Rows)
        //        {
        //            dtTable.ImportRow(dr);
        //        }
        //        DataRow[] rows = null;


        //        if (From != "" && To != "")
        //        {
        //            DateTime dFrom = ConvertDateTime(From);
        //            DateTime dTo = ConvertDateTime(To);

        //            DateTime Today = DateTime.Today;
        //            string TodayDate = Today.ToString("dd-MM-yyyy");
        //            DateTime todays = Convert.ToDateTime(TodayDate);
        //            for (int i = 0; i < dtTable.Rows.Count; i++)
        //            {
        //                string[] splitfdt = dtTable.Rows[i]["Valid_from"].ToString().Split('^');
        //                string[] splittdt = dtTable.Rows[i]["Valid_to"].ToString().Split('^');

        //                for (int j = 0; j < splitfdt.Length - 1; j++)
        //                {
        //                    DateTime dtfrm = ConvertDateTime(splitfdt[j]);
        //                    DateTime dtto = ConvertDateTime(splittdt[j]);
        //                    if (dtfrm <= dFrom && dtto >= dTo)
        //                    {
        //                        rows = dtTable.Select("(Sid=" + dtTable.Rows[i]["Sid"].ToString() + ") ");
        //                        break;
        //                    }

        //                    // List = List.Where(s => ConvertDateTime(s.Valid_from) <= dtfrm && ConvertDateTime(s.Valid_to) <= dtto).ToList();
        //                    //   rows = dtTable.Select("(Valid_from  >='#" + dtfrm + "#' AND Valid_to <='#" + dtto + "#') ");


        //                }

        //            }
        //            if (rows == null)
        //            {
        //                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //                return json;
        //            }
        //            else
        //            {
        //                dtTable = rows.CopyToDataTable();
        //            }

        //        }

        //        if (RatesFor != "")
        //        {
        //            //  rows = dtTable.Select(s => s.Act_Type == RatesFor).ToList();
        //            rows = dtTable.Select("(Act_Type like '%" + RatesFor + "%') ");
        //            if (rows.Length != 0)
        //            {
        //                dtTable = rows.CopyToDataTable();
        //            }
        //        }



        //        if (Supplier != "")
        //        {
        //            // List = List.Where(s => s.Supplier == SupplierName).ToList();
        //            rows = dtTable.Select("(Supplier like '%" + SupplierName + "%') ");
        //        }



        //        //  rows = dtTable.Select("(AgencyName like '%" + Name + "%') AND (AgencyType like '%" + Type + "%') AND (Agentuniquecode like '%" + Code + "%') AND (GroupName like '%" + Group + "%') AND (ActiveFlag = " + Status + ") AND (Country like '%" + Country + "%') AND (Code like '%" + City + "%')  AND (AvailableCredit >= " + MinBalance + ")");





        //        DataView myDataView = listing.DefaultView;
        //        myDataView.Sort = "Sid DESC";
        //        DataTable SorteddtResult = myDataView.ToTable();

        //        if (rows.Length == 0)
        //        {

        //            SorteddtResult = null;

        //            SorteddtResult = dtTable.Clone();
        //        }
        //        else
        //        {

        //            SorteddtResult = rows.CopyToDataTable();

        //        }

        //        //objSerializer.MaxJsonLength = Int32.MaxValue;
        //        try
        //        {
        //            IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0).Take(Get);
        //            SorteddtResult = allButFirst.CopyToDataTable();
        //            jsonString = "";

        //            List<Dictionary<string, object>> List_Agent = new List<Dictionary<string, object>>();

        //            List_Agent = JsonStringManager.ConvertDataTable(SorteddtResult);

        //            listing.Dispose();
        //            objSerializer.MaxJsonLength = Int32.MaxValue;

        //            //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
        //            SorteddtResult.Dispose();
        //            objSerializer.MaxJsonLength = Int32.MaxValue;
        //            return objSerializer.Serialize(new { retCode = 1, Session = 1, Arr = List_Agent });


        //        }
        //        catch
        //        {

        //        }
        //        ////////////////////


        //    }

        //    catch (Exception ex)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }


        //    return json;
        //}


        [WebMethod(EnableSession = true)]
        public string GetActivityRatelistt(string id)
        {
            using (var DB = new helperDataContext())
            {
                string jsonString = "";
                DataTable dtResult;


                var List = ((from Tarrif in DB.tbl_aeActivityTariffs
                             join Timing in DB.tbl_aeTimingSlots on Tarrif.T_Id equals Timing.T_Id
                             join Supp in DB.tbl_APIDetails on Tarrif.Supplier equals (Supp.sid).ToString()
                             //join ActivityDetail in DB.tbl_aeActivityTariffs on Time.Act_Id equals ActivityDetail.Act_Id
                             where Tarrif.Act_Id == id && Tarrif.Act_Id == Timing.Act_Id


                             select new
                             {

                                 Tarrif.Act_Id,
                                 Tarrif.T_Id,
                                 Tarrif.Valid_from,
                                 Tarrif.Valid_to,
                                 Tarrif.Spl_Valid_from,
                                 Tarrif.Spl_Valid_to,
                                 Tarrif.Act_Type,

                                 Timing.Slot_Id,
                                 Timing.Adult,
                                 Timing.Child_11_5,
                                 Timing.Child_4_2,
                                 Timing.Currency,
                                 Timing.Timing_Type,
                                 Supp.Supplier,
                                 Timing.Sid,
                                 SlotName = DB.tbl_aeActivitySlots.Single(s => s.Slot_Id == Timing.Slot_Id).Slot_Name,



                             }).Distinct()).ToList();


                ListtoDataTable lsttodt = new ListtoDataTable();
                DataTable dt = lsttodt.ToDataTable(List);
                Session.Add("RateList", dt);

                var ListChildPolicy = ((from ChildPolicy in DB.tbl_aeActivityChildPolicies
                                        where ChildPolicy.Act_Id == id

                                        select new
                                        {

                                            ChildPolicy.Child_Age_From,
                                            ChildPolicy.Child_Age_Upto,
                                            ChildPolicy.Small_Child_Age_Upto,

                                        }).Distinct()).ToList();

                ListtoDataTable childlist = new ListtoDataTable();
                DataTable dtt = childlist.ToDataTable(ListChildPolicy);
                Session.Add("ChildPolicy", dtt);

                if (List.Count > 0)
                {
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, ArrRateList = List, ArrChildPolicy = ListChildPolicy });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                return jsonString;
            }
        }


        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetTimeSlot(string id)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = ((from Timeslots in DB.tbl_aeActivitySlots

                                 where Timeslots.Act_Id == id

                                 select new
                                 {
                                     Timeslots.Slot_Id,
                                     Timeslots.Slot_Name,
                                     Timeslots.Pickup_From,
                                     Timeslots.Pickup_Time_Hours,
                                     Timeslots.Pickup_Time_Min,
                                     Timeslots.Drop_Off_At,
                                     Timeslots.Drop_Off_Time_Hours,
                                     Timeslots.Drop_Off_Time_Min,
                                     Timeslots.Tour_Start_Hours,
                                     Timeslots.Tour_Start_Min,
                                     Timeslots.Tour_End_Hours,
                                     Timeslots.Tour_End_Min,


                                 }).Distinct()).ToList();



                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrTimeSlot\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }



        [WebMethod(EnableSession = true)]
        public string GetChildPolicy(string id)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = ((from ChildPolicy in DB.tbl_aeActivityChildPolicies

                                 where ChildPolicy.Act_Id == id

                                 select new
                                 {
                                     ChildPolicy.Child_Age_From,
                                     ChildPolicy.Child_Age_Upto,
                                     ChildPolicy.Small_Child_Age_Upto,
                                     ChildPolicy.Child_Min_Height,


                                 }).Distinct()).ToList();



                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrChildPolicy\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }



        [WebMethod(EnableSession = true)]
        public string SaveLocation(string Location, string Country, string City)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            var UserType = objGlobalDefault.UserType;
            Int64 Uid = 0;
            GenralHandler objgnrl = new GenralHandler();
            Uid = objgnrl.GetUserID(UserType);

            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    //string DateTimes = DateTime.Now.ToString("dd-MM-yyyy");
                    var CityList = (from obj in DB.tbl_CommonLocations where obj.LocationName == Location && obj.Country == Country && obj.City == City && obj.SupplierID == Uid select obj).ToList();
                    if (CityList.Count == 0)
                    {
                        tbl_CommonLocation Locat = new tbl_CommonLocation();
                        //string uniqueid = ActivityManager.GenerateRandomString(3);
                        Locat.LocationName = Location;
                        Locat.Country = Country;
                        Locat.City = City;
                        Locat.SupplierID = Uid;
                        //Locat.Longitude = Longitude;
                        //Locat.Latitude = Latitude;

                        DB.tbl_CommonLocations.InsertOnSubmit(Locat);
                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Locat\":\"1\"}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\",\"Locat\":\"1\"}";
                    }
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }



        [WebMethod(EnableSession = true)]
        public string GetLocation()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            var UserType = objGlobalDefault.UserType;
            Int64 Uid = 0;
            GenralHandler objgnrl = new GenralHandler();
            Uid = objgnrl.GetUserID(UserType);


            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var List = ((from Loct in DB.tbl_CommonLocations
                                 where Loct.SupplierID == Uid


                                 select new
                                 {
                                     Loct.Lid,
                                     Loct.LocationName,
                                     Loct.Country,
                                     Loct.City,
                                     //Loct.Longitude,
                                     //Loct.Latitude,



                                 }).Distinct()).ToList();



                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrLocation\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetLocationByCity(string city)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var List = ((from Loct in DB.tbl_CommonLocations
                                 where Loct.City == city


                                 select new
                                 {
                                     Loct.Lid,
                                     Loct.LocationName,
                                     Loct.Country,
                                     Loct.City,
                                     //Loct.Longitude,
                                     //Loct.Latitude,



                                 }).Distinct()).ToList();



                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrLocation\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetDetails(Int64 LocationID)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = ActivityManager.GetDetails(LocationID, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejsondata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateLocation(string Location, string Country, string City, Int64 id)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = ActivityManager.UpdateLocation(Location, Country, City, id);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }

        }


        [WebMethod(EnableSession = true)]
        public string DeleteLocation(Int64 LocationId)
        {
            string jsonString = "";
            DBHelper.DBReturnCode retcode = ActivityManager.DeleteLocation(LocationId);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        //[WebMethod(EnableSession = true)]
        //public string GetLocation(string Location)
        //{
        //    string jsonString = "";
        //    DataTable dtResult;
        //    DBHelper.DBReturnCode retcode = ActivityManager.GetLocation(Location, out dtResult);
        //    JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
        //        list_autocomplete = dtResult.AsEnumerable()
        //        .Select(data => new Models.AutoComplete
        //        {
        //            //id = data.Field<String>("Sid"),
        //            value = data.Field<String>("Location_Name")
        //        }).ToList();

        //        jsonString = objSerlizer.Serialize(list_autocomplete);
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
        //    }
        //    return jsonString;
        //}



        //[WebMethod(EnableSession = true)]
        //public string UpdateRates(Int64 id, string Tid, string TimingType, string adult, string child, string schild, string Currency, string FromDate, string FromTo, string SFromDate, string SFromTo)
        //{
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        // DBHelper.DBReturnCode retcode = ActivityManager.UpdateRates(id, adult, child, schild, Currency);

        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    else
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }

        //}

        [WebMethod(EnableSession = true)]
        public string UpdateRates(Int64 id, string Tid, string TimingType, string adult, string child, string schild, string Currency, string FromDate, string FromTo, string SFromDate, string SFromTo)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    if (TimingType == "Normal")
                    {
                        tbl_aeActivityTariff Update = DB.tbl_aeActivityTariffs.Single(x => x.T_Id == Tid);
                        Update.Valid_from = FromDate;
                        Update.Valid_to = FromTo;

                        DB.SubmitChanges();
                    }
                    else
                    {
                        tbl_aeActivityTariff Update = DB.tbl_aeActivityTariffs.Single(x => x.T_Id == Tid);
                        Update.Spl_Valid_from = SFromDate;
                        Update.Spl_Valid_to = SFromTo;
                        DB.SubmitChanges();

                        tbl_aeTimingSlot updateSlot = DB.tbl_aeTimingSlots.Single(x => x.Sid == id);
                        updateSlot.Spl_Valid_from = SFromDate;
                        updateSlot.Spl_Valid_to = SFromTo;
                        DB.SubmitChanges();
                    }

                    tbl_aeTimingSlot updaterate = DB.tbl_aeTimingSlots.Single(x => x.Sid == id);
                    updaterate.Adult = adult;
                    updaterate.Child_11_5 = child;
                    updaterate.Child_4_2 = schild;
                    updaterate.Currency = Currency;
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;


        }

        [WebMethod(EnableSession = true)]
        public string DeleteRate(Int64 id)
        {
            string jsonString = "";
            DBHelper.DBReturnCode retcode = ActivityManager.DeleteRate(id);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }



        [WebMethod(EnableSession = true)]
        public string TeriffIDForTKT(string id, string TypeActivity)
        {
            string jsonString = "";

            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retcode = ActivityManager.TeriffIDForTKT(id, TypeActivity, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejsondata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateActivity(string id, string UTer_ID, string Upd_OprDates, string Upd_SLots, string UTourType, string activityname, string subtitle, string Country, string City, string Location,
            string Description, string ChildrenAllowed, float ChildAgeFrom, float ChildAgeUpTo,
            float SmallChildAgeUpTo, string allowheight, float ChildMinHight, string Infantallowed,
            float MaxNoofInfant, string AllowMaxChild, float MaxChild, string Attraction, string TourType,
            float MinCapacity, float MaximumCapacity, string TourNote, string ActivityType, string PriorityType,
            string OperationDays, string OperationDaysTKT, string OperationDaysSIC, string OperationDaysPVT, List<string> datefrom, List<string> dateTill, string FulYear, List<string> Name, List<string> SlotValue, List<string> TourStartH, List<string> TourStartM,
            List<string> TourEndH, List<string> TourEndM, List<string> PickUpFrom, List<string> PickUpTimeH, List<string> PickUpTimeM, List<string> DropOffAt, List<string> DropOffTimeH, List<string> DropOffTimeM, List<string> PriorityTyp, string AllowSlot)
        {

            string ID = "";
            Decimal Balance = 0;
            Decimal withdrawl = 0;
            try
            {
                using (var DB = new helperDataContext())
                {
                    //string DateTimes = DateTime.Now.ToString("dd-MM-yyyy");
                    //helperDataContext DB = new helperDataContext();
                    // tbl_aeActivityMaster Prodoct = new tbl_aeActivityMaster();
                    //string uniqueid = ActivityManager.GenerateRandomString(3);


                    tbl_aeActivityMaster Prodoct = DB.tbl_aeActivityMasters.Single(x => x.Sr_No == id);
                    Prodoct.Sr_No = id;
                    Prodoct.Act_Name = activityname;
                    Prodoct.Sub_Title = subtitle;
                    Prodoct.Country = Country;
                    Prodoct.City = City;
                    Prodoct.Location = Location;
                    Prodoct.Description = Description;
                    Prodoct.Tour_Type = TourType;
                    Prodoct.Allowed_Child = ChildrenAllowed;
                    Prodoct.Allow_Height = allowheight;
                    Prodoct.ALlow_Infant = Infantallowed;
                    Prodoct.Allow_Maxchild = AllowMaxChild;
                    Prodoct.Attractions = Attraction;
                    Prodoct.Min_Capacity = MinCapacity;
                    Prodoct.Max_Capacity = MaximumCapacity;
                    Prodoct.Tour_Note = TourNote;
                    Prodoct.Full_Year = FulYear;
                    //if (FulYear == "No")
                    //{
                    Prodoct.Operation_Days = OperationDays;
                    //}

                    Prodoct.Slot_Allow = AllowSlot;
                    //DB.tbl_aeActivityMasters.InsertOnSubmit(Prodoct);
                    DB.SubmitChanges();

                    if (ChildrenAllowed == "Yes")
                    {
                        //tbl_aeActivityChildPolicy ChildPolicy = new tbl_aeActivityChildPolicy();

                        tbl_aeActivityChildPolicy ChildPolicy = DB.tbl_aeActivityChildPolicies.Single(x => x.Act_Id == id);
                        ChildPolicy.Act_Id = id;
                        ChildPolicy.Child_Age_From = ChildAgeFrom;
                        ChildPolicy.Child_Age_Upto = ChildAgeUpTo;
                        ChildPolicy.Small_Child_Age_Upto = SmallChildAgeUpTo;
                        ChildPolicy.Allow_Height = allowheight;

                        ChildPolicy.Child_Min_Height = ChildMinHight;
                        ChildPolicy.Infant_Allow = Infantallowed;

                        ChildPolicy.No_Of_Infant = MaxNoofInfant;
                        ChildPolicy.Max_Child = MaxChild;
                        //DB.tbl_aeActivityChildPolicies.InsertOnSubmit(ChildPolicy);
                        DB.SubmitChanges();
                    }

                    String[] actType = ActivityType.Split(';');
                    string[] OldActType = UTourType.Split(';');
                    string teriffidd = "";
                    bool Chk = true;
                    for (int i = 0; i < actType.Length - 1; i++)
                    {
                        Chk = true;
                        if (actType[i] == "All")
                            continue;
                        for (int j = 0; j < OldActType.Length - 1; j++)
                        {
                            if (actType[i] == OldActType[j])
                            {
                                OldActType[j] = "";
                                Chk = false;
                                //Update

                                tbl_aeActivityTariff Teriff = DB.tbl_aeActivityTariffs.Single(x => x.Act_Id == id && x.Act_Type == actType[i]);

                                Teriff.Priority = PriorityType;
                                Teriff.Attraction = Attraction;
                                Teriff.Tour_Type = TourType;
                                if (actType[i] == "TKT")
                                {
                                    Teriff.OperationsDays = OperationDaysTKT;
                                }
                                else if (actType[i] == "SIC")
                                {
                                    Teriff.OperationsDays = OperationDaysSIC;
                                }
                                else if (actType[i] == "PVT")
                                {
                                    Teriff.OperationsDays = OperationDaysPVT;
                                }
                                DB.SubmitChanges();
                                break;

                            }
                        }
                        if (Chk)
                        {
                            //Add

                            string Tid = ActivityManager.GenerateRandomString(3);
                            tbl_aeActivityTariff Teriff = new tbl_aeActivityTariff();
                            Teriff.T_Id = Tid;
                            Teriff.Act_Id = id;
                            Teriff.Act_Type = actType[i];
                            Teriff.Priority = PriorityType;
                            Teriff.Attraction = Attraction;
                            Teriff.Tour_Type = TourType;
                            if (actType[i] == "TKT")
                            {
                                Teriff.OperationsDays = OperationDaysTKT;
                            }
                            else if (actType[i] == "SIC")
                            {
                                Teriff.OperationsDays = OperationDaysSIC;
                            }
                            else if (actType[i] == "PVT")
                            {
                                Teriff.OperationsDays = OperationDaysPVT;
                            }
                            DB.tbl_aeActivityTariffs.InsertOnSubmit(Teriff);
                            DB.SubmitChanges();

                        }

                    }
                    for (int j = 0; j < OldActType.Length - 1; j++)
                    {
                        if (OldActType[j] == "")
                            continue;
                        else if (OldActType[j] != "")
                        {
                            //delete
                            tbl_aeActivityTariff DelTer = DB.tbl_aeActivityTariffs.Single(x => x.Act_Id == id && x.Act_Type == OldActType[j]);
                            DB.tbl_aeActivityTariffs.DeleteOnSubmit(DelTer);
                            DB.SubmitChanges();

                        }
                    }

                    if (FulYear == "No")
                    {

                        int i = 0;
                        foreach (string Date in datefrom)
                        {
                            tbl_aeActivityOperatingTime OprTime = new tbl_aeActivityOperatingTime();

                            // tbl_aeActivityOperatingTime OprTime = DB.tbl_aeActivityOperatingTimes.Single(x => x.Act_Id == id);                        
                            OprTime.Act_Id = id;
                            OprTime.Operating_From = Date;
                            OprTime.Operating_Till = dateTill[i];
                            DB.tbl_aeActivityOperatingTimes.InsertOnSubmit(OprTime);
                            DB.SubmitChanges();
                            i++;


                        }

                    }
                    else
                    {

                        //DateTime Today = DateTime.Today;
                        //string TodayDate = Today.ToString("dd-MM-yyyy");
                        //string tilldate = TodayDate.AddYears(1);

                        DateTime Today = DateTime.Today;
                        string TodayDate = Today.ToString("dd-MM-yyyy");
                        DateTime todays = Convert.ToDateTime(TodayDate);
                        DateTime tdate = todays.AddYears(1);
                        string tilldate = tdate.ToString("dd-MM-yyyy");

                        tbl_aeActivityOperatingTime OprTime = new tbl_aeActivityOperatingTime();

                        //tbl_aeActivityOperatingTime OprTime = DB.tbl_aeActivityOperatingTimes.Single(x => x.Act_Id == id);
                        OprTime.Act_Id = id;
                        OprTime.Operating_From = TodayDate;
                        OprTime.Operating_Till = tilldate;
                        DB.tbl_aeActivityOperatingTimes.InsertOnSubmit(OprTime);
                        DB.SubmitChanges();

                    }


                    String[] UpOptDates = Upd_OprDates.Split(';');

                    for (int i = 0; i < UpOptDates.Length - 1; i++)
                    {
                        tbl_aeActivityOperatingTime DelOpTime = DB.tbl_aeActivityOperatingTimes.Single(x => x.P_Id.ToString() == UpOptDates[i]);
                        DB.tbl_aeActivityOperatingTimes.DeleteOnSubmit(DelOpTime);
                        DB.SubmitChanges();
                    }

                    if (AllowSlot == "Yes")
                    {
                        String[] OldSlotval = Upd_SLots.Split(';');
                        //String[] NSlotVal = SlotValue.Split(';');

                        for (int j = 0; j < SlotValue.Count; j++)
                        {
                            if (SlotValue[j] == "")
                            {
                                //Add
                                tbl_aeActivitySlot Tslot = new tbl_aeActivitySlot();
                                //tbl_aeActivitySlot Tslot = DB.tbl_aeActivitySlots.Single(x => x.Act_Id == id);
                                Tslot.Act_Id = id;
                                Tslot.Slot_Name = Name[j];
                                Tslot.Tour_Start_Hours = TourStartH[j];
                                Tslot.Tour_Start_Min = TourStartM[j];
                                Tslot.Tour_End_Hours = TourEndH[j];
                                Tslot.Tour_End_Min = TourEndM[j];
                                if (PickUpFrom.Count == 0)
                                {
                                    Tslot.Pickup_From = null;
                                }
                                else
                                {
                                    Tslot.Pickup_From = PickUpFrom[j];
                                }
                                if (PickUpTimeH.Count == 0)
                                {
                                    Tslot.Pickup_Time_Hours = null;
                                }
                                else
                                {
                                    Tslot.Pickup_Time_Hours = PickUpTimeH[j];
                                }
                                if (PickUpTimeM.Count == 0)
                                {
                                    Tslot.Pickup_Time_Min = null;
                                }
                                else
                                {
                                    Tslot.Pickup_Time_Min = PickUpTimeM[j];
                                }
                                if (DropOffAt.Count == 0)
                                {
                                    Tslot.Drop_Off_At = null;
                                }
                                else
                                {
                                    Tslot.Drop_Off_At = DropOffAt[j];
                                }
                                if (DropOffTimeH.Count == 0)
                                {
                                    Tslot.Drop_Off_Time_Hours = null;
                                }
                                else
                                {
                                    Tslot.Drop_Off_Time_Hours = DropOffTimeH[j];
                                }

                                if (DropOffTimeM.Count == 0)
                                {
                                    Tslot.Drop_Off_Time_Min = null;
                                }
                                else
                                {
                                    Tslot.Drop_Off_Time_Min = DropOffTimeM[j];
                                }


                                Tslot.Priority_Type = PriorityTyp[j];
                                DB.tbl_aeActivitySlots.InsertOnSubmit(Tslot);
                                DB.SubmitChanges();
                            }

                            for (int k = 0; k < OldSlotval.Length - 1; k++)
                            {
                                if (OldSlotval[k] == SlotValue[j])
                                {
                                    //Update
                                    //tbl_aeActivitySlot Tslot = new tbl_aeActivitySlot();
                                    tbl_aeActivitySlot Tslot = DB.tbl_aeActivitySlots.Single(x => x.Slot_Id == Convert.ToInt64(OldSlotval[k]));
                                    Tslot.Act_Id = id;
                                    Tslot.Slot_Name = Name[k];
                                    Tslot.Tour_Start_Hours = TourStartH[k];
                                    Tslot.Tour_Start_Min = TourStartM[k];
                                    Tslot.Tour_End_Hours = TourEndH[k];
                                    Tslot.Tour_End_Min = TourEndM[k];
                                    if (PickUpFrom.Count == 0)
                                    {
                                        Tslot.Pickup_From = null;
                                    }
                                    else
                                    {
                                        Tslot.Pickup_From = PickUpFrom[k];
                                    }
                                    if (PickUpTimeH.Count == 0)
                                    {
                                        Tslot.Pickup_Time_Hours = null;
                                    }
                                    else
                                    {
                                        Tslot.Pickup_Time_Hours = PickUpTimeH[k];
                                    }
                                    if (PickUpTimeM.Count == 0)
                                    {
                                        Tslot.Pickup_Time_Min = null;
                                    }
                                    else
                                    {
                                        Tslot.Pickup_Time_Min = PickUpTimeM[k];
                                    }
                                    if (DropOffAt.Count == 0)
                                    {
                                        Tslot.Drop_Off_At = null;
                                    }
                                    else
                                    {
                                        Tslot.Drop_Off_At = DropOffAt[k];
                                    }
                                    if (DropOffTimeH.Count == 0)
                                    {
                                        Tslot.Drop_Off_Time_Hours = null;
                                    }
                                    else
                                    {
                                        Tslot.Drop_Off_Time_Hours = DropOffTimeH[k];
                                    }

                                    if (DropOffTimeM.Count == 0)
                                    {
                                        Tslot.Drop_Off_Time_Min = null;
                                    }
                                    else
                                    {
                                        Tslot.Drop_Off_Time_Min = DropOffTimeM[k];
                                    }


                                    Tslot.Priority_Type = PriorityTyp[k];
                                    //DB.tbl_aeActivitySlots.InsertOnSubmit(Tslot);
                                    DB.SubmitChanges();

                                    SlotValue[j] = "";
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }


                        //int i = 0;
                        //foreach (string Slot in Name)
                        //{

                        //    tbl_aeActivitySlot Tslot = new tbl_aeActivitySlot();
                        //    //tbl_aeActivitySlot Tslot = DB.tbl_aeActivitySlots.Single(x => x.Act_Id == id);
                        //    Tslot.Act_Id = id;
                        //    Tslot.Slot_Name = Slot;
                        //    Tslot.Tour_Start_Hours = TourStartH[i];
                        //    Tslot.Tour_Start_Min = TourStartM[i];
                        //    Tslot.Tour_End_Hours = TourEndH[i];
                        //    Tslot.Tour_End_Min = TourEndM[i];
                        //    if (PickUpFrom.Count == 0)
                        //    {
                        //        Tslot.Pickup_From = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Pickup_From = PickUpFrom[i];
                        //    }
                        //    if (PickUpTimeH.Count == 0)
                        //    {
                        //        Tslot.Pickup_Time_Hours = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Pickup_Time_Hours = PickUpTimeH[i];
                        //    }
                        //    if (PickUpTimeM.Count == 0)
                        //    {
                        //        Tslot.Pickup_Time_Min = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Pickup_Time_Min = PickUpTimeM[i];
                        //    }
                        //    if (DropOffAt.Count == 0)
                        //    {
                        //        Tslot.Drop_Off_At = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Drop_Off_At = DropOffAt[i];
                        //    }
                        //    if (DropOffTimeH.Count == 0)
                        //    {
                        //        Tslot.Drop_Off_Time_Hours = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Drop_Off_Time_Hours = DropOffTimeH[i];
                        //    }

                        //    if (DropOffTimeM.Count == 0)
                        //    {
                        //        Tslot.Drop_Off_Time_Min = null;
                        //    }
                        //    else
                        //    {
                        //        Tslot.Drop_Off_Time_Min = DropOffTimeM[i];
                        //    }


                        //    Tslot.Priority_Type = PriorityTyp[i];
                        //    DB.tbl_aeActivitySlots.InsertOnSubmit(Tslot);
                        //    DB.SubmitChanges();
                        //    i++;


                        //}
                    }

                    //String[] Upd_ActSlots = Upd_SLots.Split(';');
                    //for (int i = 0; i < Upd_ActSlots.Length-1; i++)
                    //{
                    //    tbl_aeActivitySlot DelSlot = DB.tbl_aeActivitySlots.Single(x => x.Slot_Id.ToString() == Upd_ActSlots[i]);
                    //    DB.tbl_aeActivitySlots.DeleteOnSubmit(DelSlot);                   
                    //    DB.SubmitChanges();
                    //}

                    //ID = Prodoct.nID.ToString();
                    //tbl_MainBalance Prodoctt = new tbl_MainBalance();

                    //Prodoctt.Member_ID = Convert.ToInt64(ID);
                    //Prodoctt.Withdrawl = withdrawl;
                    //Prodoctt.Balance = Balance;
                    //DB.tbl_MainBalances.InsertOnSubmit(Prodoctt);
                    //DB.SubmitChanges();

                    //tbl_Register Prodocttt = DB.tbl_Registers.Single(x => (x.nID == Convert.ToInt64(ID)));
                    ////tbl_Register Prodocttt = new tbl_Register();
                    //Prodocttt.Unique_ID = Convert.ToInt64(ID);
                    //DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Sr_No\":\"" + Prodoct.Sr_No + "\",\"teriffidd\":\"" + teriffidd + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(true)]
        public string DeleteSlot(string Act_Id, Int64 Slot_Id)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_aeActivitySlot DelSlot = DB.tbl_aeActivitySlots.Single(x => x.Slot_Id == Slot_Id && x.Act_Id == Act_Id);
                    DB.tbl_aeActivitySlots.DeleteOnSubmit(DelSlot);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        //[WebMethod(EnableSession = true)]
        //public string TeriffIDForSIC(string id, string TypeActivity)
        //{
        //    string jsonString = "";

        //    DataTable dtResult = new DataTable();
        //    DBHelper.DBReturnCode retcode = ActivityManager.TeriffIDForSIC(id, TypeActivity, out dtResult);
        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejsondata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"dttable\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}





        [WebMethod(EnableSession = true)]
        public string GetTariffDetails(string id)
        {
            string jsonn = "";
            try
            {
                using (var DB = new helperDataContext())
                {

                    var List = ((from ActivityTer in DB.tbl_aeActivityMasters
                                 join meta in DB.tbl_aeActivityTariffs on ActivityTer.Sr_No equals meta.Act_Id
                                 where ActivityTer.Sr_No == id

                                 select new
                                 {
                                     meta.T_Id,
                                     meta.Act_Type,
                                     meta.Supplier,
                                     meta.Valid_from,
                                     meta.Valid_to,
                                     meta.Child_Age_Start,
                                     meta.Child_Age_Upto,
                                     meta.Child_Min_Height,
                                     meta.Spl_Valid_from,
                                     meta.Spl_Valid_to,
                                     ActivityTer.Allowed_Child,
                                     ActivityTer.Allow_Height,
                                     ActivityTer.Slot_Allow,

                                 }).Distinct()).ToList();


                    var TimingList = ((from ActivityTiming in DB.tbl_aeTimingSlots
                                       where ActivityTiming.Act_Id == id

                                       select new
                                       {
                                           ActivityTiming.Sid,
                                           ActivityTiming.T_Id,
                                           ActivityTiming.Act_Id,
                                           ActivityTiming.Slot_Id,
                                           ActivityTiming.Timing_Type,
                                           ActivityTiming.Currency,
                                           ActivityTiming.Nationality,
                                           ActivityTiming.Adult,
                                           ActivityTiming.Child_11_5,
                                           ActivityTiming.Child_4_2,
                                           ActivityTiming.Infant,
                                           ActivityTiming.Cacellation_Policy,
                                           ActivityTiming.Offer,
                                           ActivityTiming.Terrif_Note,
                                           ActivityTiming.Pickup_From,
                                           ActivityTiming.Pickup_Time_Hours,
                                           ActivityTiming.Pickup_Time_Min,
                                           ActivityTiming.Drop_Off_At,
                                           ActivityTiming.Drop_Off_Time_Hours,
                                           ActivityTiming.Drop_Off_Time_Min,
                                           ActivityTiming.Inclusion,
                                           ActivityTiming.Exclusion,


                                       }).Distinct()).ToList();

                    if (List.Count > 0)
                    {
                        json = jsSerializer.Serialize(List);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        jsonn = jsSerializer.Serialize(TimingList);
                        jsonn = jsonn.TrimEnd(']');
                        jsonn = jsonn.TrimStart('[');

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ArrActTariff\":[" + json + "],\"ArrActTiming\":[" + jsonn + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }

                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }



        [WebMethod(EnableSession = true)]
        public string UpdateActivityTariff(string Type, string id, string TeriffID, List<string> SlotID, string Supplier, string ChildageFrm,
            string ChildageUpto, string ChildMinHeight, string SeasonDate1, string SeasonDate2, string Currencytkt, string Nationality, string Adult,
            string Child11, string Child4, string Infant, string Policy, string Offer, string Note, string Inclusion,
            string Exclusion, string Picfrm, string drpfrm, string PickuptHr, string PickuptMin, string DropoftHr, string DropoftMin,
            string Picfrmspl, string drpfrmspl, string PickuptHrspl, string PickuptMinspl, string DropoftHrspl, string DropoftMinspl,
            string Spldate1, string Spldate2, string Currencyspl, string Nationalityspl, string Adultspl, string Childspl11,
            string Childspl4, string Infantspl, string Cancelspl, string Offerspl, string incspl, string excspl, string Terrifspl,
            string ATimeSlot, string SplDate, string SplDateSlot,
            List<string> SplSlotID, List<Inclusion> objInclusion, List<Exclusion> objExclusion, List<ExclusionSpl> objExclusionSpl, List<InclusionSpl> objInclusionSpl,
            string UpdateTimingID, string SplDateVal, string SplDateValSIC)
        {

            string uniqueid = GenerateRandomString(3);
            string[] curren = Currencytkt.Split('^');
            string[] nation = Nationality.Split('^');
            string[] adul = Adult.Split('^');
            string[] chil11 = Child11.Split('^');
            string[] chil4 = Child4.Split('^');
            string[] infa = Infant.Split('^');
            string[] polic = Policy.Split('^');
            string[] offe = Offer.Split('^');
            string[] not = Note.Split('^');
            string[] Pickupfrm = Picfrm.Split('^');
            string[] picktimeHr = PickuptHr.Split('^');
            string[] picktimeMn = PickuptMin.Split('^');
            string[] Dropat = drpfrm.Split('^');
            string[] droptHr = DropoftHr.Split('^');
            string[] droptMn = DropoftMin.Split('^');

            string[] currenspl = Currencyspl.Split('^');
            string[] nationalspl = Nationalityspl.Split('^');
            string[] adulspl = Adultspl.Split('^');
            string[] chil11spl = Childspl11.Split('^');
            string[] chil4spl = Childspl4.Split('^');
            string[] infaspl = Infantspl.Split('^');
            string[] cancelspl = Cancelspl.Split('^');
            string[] offespl = Offerspl.Split('^');
            string[] terrispl = Terrifspl.Split('^');

            string[] Pickupfrmspl = Picfrmspl.Split('^');
            string[] picktimeHrspl = PickuptHrspl.Split('^');
            string[] picktimeMnspl = PickuptMinspl.Split('^');
            string[] Dropatspl = drpfrmspl.Split('^');
            string[] droptHrspl = DropoftHrspl.Split('^');
            string[] droptMnspl = DropoftMinspl.Split('^');

            string[] TimingSid = UpdateTimingID.Split('^');

            if (Type == "TKT")
            {
                SplDate = SplDateVal;
            }
            else if (Type == "SIC")
            {
                SplDate = SplDateValSIC;
            }

            try
            {
                //helperDataContext DB = new helperDataContext();
                using (var DB = new helperDataContext())
                {
                    tbl_aeActivityTariff Teriff = DB.tbl_aeActivityTariffs.Single(x => x.Act_Id == id && x.T_Id == TeriffID);


                    Teriff.Supplier = Supplier;
                    Teriff.Valid_from = SeasonDate1;
                    Teriff.Valid_to = SeasonDate2;
                    Teriff.Child_Age_Start = ChildageFrm;
                    Teriff.Child_Age_Upto = ChildageUpto;
                    Teriff.Child_Min_Height = ChildMinHeight;

                    if (SplDate == "Yes")
                    {

                        Teriff.Spl_Valid_from = Spldate1;
                        Teriff.Spl_Valid_to = Spldate2;


                    }
                    else
                    {
                        Teriff.Spl_Valid_from = null;
                        Teriff.Spl_Valid_to = null;
                    }

                    DB.SubmitChanges();


                    int i = 0;
                    if (SlotID.Count == 0)
                    {
                        for (i = 0; i == SlotID.Count; i++)
                        {
                            tbl_aeTimingSlot TSlott = new tbl_aeTimingSlot();
                            TSlott.T_Id = TeriffID;
                            TSlott.Act_Id = id;
                            TSlott.Slot_Id = 0;
                            TSlott.Timing_Type = "Normal";
                            TSlott.Currency = curren[i];
                            TSlott.Nationality = nation[i];
                            TSlott.Adult = adul[i];

                            if (Child11 != "")
                            {
                                TSlott.Child_11_5 = chil11[i];
                            }
                            if (Child4 != "")
                            {
                                TSlott.Child_4_2 = chil4[i];
                            }
                            if (Infant != "")
                            {
                                TSlott.Infant = infa[i];
                            }


                            TSlott.Cacellation_Policy = polic[i];
                            TSlott.Offer = offe[i];
                            TSlott.Terrif_Note = not[i];

                            TSlott.Pickup_From = Pickupfrm[i];
                            TSlott.Pickup_Time_Hours = picktimeHr[i];
                            TSlott.Pickup_Time_Min = picktimeMn[i];
                            TSlott.Drop_Off_At = Dropat[i];
                            TSlott.Drop_Off_Time_Hours = droptHr[i];
                            TSlott.Drop_Off_Time_Min = droptMn[i];

                            string inclusnn = "";
                            foreach (Inclusion Inclusionn in objInclusion)
                            {

                                inclusnn += Inclusionn.Value;
                            }
                            string exclusnn = "";
                            foreach (Exclusion Exclusionn in objExclusion)
                            {

                                exclusnn += Exclusionn.Value;
                            }
                            TSlott.Inclusion = inclusnn;
                            TSlott.Exclusion = exclusnn;
                            DB.tbl_aeTimingSlots.InsertOnSubmit(TSlott);
                            DB.SubmitChanges();
                            // continue;
                        }


                    }
                    else
                    {
                        for (i = 0; i < SlotID.Count; i++)
                        {

                            tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                            TSlot.T_Id = TeriffID;
                            TSlot.Act_Id = id;
                            TSlot.Slot_Id = Convert.ToInt64(SlotID[i]);
                            TSlot.Timing_Type = "Normal";
                            TSlot.Currency = curren[i];
                            TSlot.Nationality = nation[i];
                            TSlot.Adult = adul[i];
                            if (Child11 != "")
                            {
                                TSlot.Child_11_5 = chil11[i];
                            }
                            if (Child4 != "")
                            {
                                TSlot.Child_4_2 = chil4[i];
                            }
                            if (Infant != "")
                            {
                                TSlot.Infant = infa[i];
                            }

                            TSlot.Cacellation_Policy = polic[i];
                            TSlot.Offer = offe[i];
                            TSlot.Terrif_Note = not[i];
                            if (Type != "TKT")
                            {
                                TSlot.Pickup_From = Pickupfrm[i];
                                TSlot.Pickup_Time_Hours = picktimeHr[i];
                                TSlot.Pickup_Time_Min = picktimeMn[i];
                                TSlot.Drop_Off_At = Dropat[i];
                                TSlot.Drop_Off_Time_Hours = droptHr[i];
                                TSlot.Drop_Off_Time_Min = droptMn[i];
                            }


                            string inclusn = "";
                            foreach (Inclusion Inclusionn in objInclusion)
                            {
                                if (Inclusionn.id == Convert.ToInt64(SlotID[i]))
                                    inclusn += Inclusionn.Value;
                            }
                            string exclusn = "";
                            foreach (Exclusion Exclusionn in objExclusion)
                            {
                                if (Exclusionn.id == Convert.ToInt64(SlotID[i]))
                                    exclusn += Exclusionn.Value;
                            }
                            TSlot.Inclusion = inclusn;
                            TSlot.Exclusion = exclusn;
                            DB.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                            DB.SubmitChanges();


                        }

                    }


                    // }

                    if (SplDate == "Yes")
                    {
                        int j = 0;

                        if (SplSlotID.Count == 0)
                        {
                            for (j = 0; j == SplSlotID.Count; j++)
                            {
                                tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                                TSlot.T_Id = TeriffID;
                                TSlot.Act_Id = id;
                                TSlot.Slot_Id = 0;
                                TSlot.Timing_Type = "Special";
                                TSlot.Currency = currenspl[j];
                                TSlot.Nationality = nationalspl[j];
                                TSlot.Adult = adulspl[j];

                                if (Childspl11 != "")
                                {
                                    TSlot.Child_11_5 = chil11spl[j];
                                }
                                if (Childspl4 != "")
                                {
                                    TSlot.Child_4_2 = chil4spl[j];
                                }
                                if (Infantspl != "")
                                {
                                    TSlot.Infant = infaspl[j];
                                }


                                TSlot.Cacellation_Policy = cancelspl[j];
                                TSlot.Offer = offespl[j];
                                TSlot.Terrif_Note = terrispl[j];
                                TSlot.Spl_Valid_from = Spldate1;
                                TSlot.Spl_Valid_to = Spldate2;

                                if (Type != "TKT")
                                {
                                    TSlot.Pickup_From = Pickupfrmspl[j];
                                    TSlot.Pickup_Time_Hours = picktimeHrspl[j];
                                    TSlot.Pickup_Time_Min = picktimeMnspl[j];
                                    TSlot.Drop_Off_At = Dropatspl[j];
                                    TSlot.Drop_Off_Time_Hours = droptHrspl[j];
                                    TSlot.Drop_Off_Time_Min = droptMnspl[j];
                                }
                                string inclusion = "";
                                foreach (InclusionSpl InclusionSpl in objInclusionSpl)
                                {

                                    inclusion += InclusionSpl.Value;
                                }
                                string exclusion = "";
                                foreach (ExclusionSpl exclusionSpl in objExclusionSpl)
                                {

                                    exclusion += exclusionSpl.Value;
                                }
                                TSlot.Inclusion = inclusion;
                                TSlot.Exclusion = exclusion;
                                DB.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                                DB.SubmitChanges();
                            }

                            // j++;
                        }
                        else
                        {
                            for (j = 0; j < SplSlotID.Count; j++)
                            {


                                tbl_aeTimingSlot TSlot = new tbl_aeTimingSlot();
                                TSlot.T_Id = TeriffID;
                                TSlot.Act_Id = id;
                                TSlot.Slot_Id = Convert.ToInt64(SplSlotID[j]);
                                TSlot.Timing_Type = "Special";
                                TSlot.Currency = currenspl[j];
                                TSlot.Nationality = nationalspl[j];
                                TSlot.Adult = adulspl[j];

                                if (Childspl11 != "")
                                {
                                    TSlot.Child_11_5 = chil11spl[j];
                                }
                                if (Childspl4 != "")
                                {
                                    TSlot.Child_4_2 = chil4spl[j];
                                }
                                if (Infantspl != "")
                                {
                                    TSlot.Infant = infaspl[j];
                                }


                                TSlot.Cacellation_Policy = cancelspl[j];
                                TSlot.Offer = offespl[j];
                                TSlot.Terrif_Note = terrispl[j];
                                TSlot.Spl_Valid_from = Spldate1;
                                TSlot.Spl_Valid_to = Spldate2;
                                if (Type != "TKT")
                                {
                                    TSlot.Pickup_From = Pickupfrmspl[j];
                                    TSlot.Pickup_Time_Hours = picktimeHrspl[j];
                                    TSlot.Pickup_Time_Min = picktimeMnspl[j];
                                    TSlot.Drop_Off_At = Dropatspl[j];
                                    TSlot.Drop_Off_Time_Hours = droptHrspl[j];
                                    TSlot.Drop_Off_Time_Min = droptMnspl[j];
                                }
                                string inclusion = "";
                                foreach (InclusionSpl InclusionSpl in objInclusionSpl)
                                {
                                    if (InclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                        inclusion += InclusionSpl.Value;
                                }
                                string exclusion = "";
                                foreach (ExclusionSpl exclusionSpl in objExclusionSpl)
                                {
                                    if (exclusionSpl.id == Convert.ToInt64(SplSlotID[j]))
                                        exclusion += exclusionSpl.Value;
                                }
                                TSlot.Inclusion = inclusion;
                                TSlot.Exclusion = exclusion;
                                DB.tbl_aeTimingSlots.InsertOnSubmit(TSlot);
                                DB.SubmitChanges();


                            }

                            //j++;
                        }


                    }

                    for (int j = 0; j < TimingSid.Length - 1; j++)
                    {
                        var ListSid = (from obj in DB.tbl_aeTimingSlots
                                       where obj.Sid == Convert.ToInt64(TimingSid[j])
                                       select new
                                       {
                                           obj.Sid,
                                           obj.Act_Id,
                                           obj.T_Id

                                       }).ToList();

                        for (int l = 0; l < ListSid.Count; l++)
                        {
                            if (ListSid[l].T_Id == TeriffID)
                            {
                                tbl_aeTimingSlot Delete = DB.tbl_aeTimingSlots.Single(x => x.Sid == Convert.ToInt64(TimingSid[j]));
                                DB.tbl_aeTimingSlots.DeleteOnSubmit(Delete);
                                DB.SubmitChanges();
                            }
                        }




                    }


                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }



            return json;

            ////string json = "";
            ////DateTime sDob = new DateTime(2000, 2, 29);
            ////DateTime now = new DateTime(2009, 2, 28);
            ////Int16 Age = CalculateAgeCorrect(sDob, now);
            //DBHelper.DBReturnCode retCode = ActivityManager.AddActivityTariff(Type, id, SlotID, Supplier, ChildageFrm, ChildageUpto, ChildMinHeight, SeasonDate1, SeasonDate2, Currencytkt, Adult, Child11, Child4, Infant, Policy, Offer, Note, Inclusion, Exclusion, Spldate1, Spldate2, Currencyspl, Adultspl, Childspl11, Childspl4, Infantspl, Cancelspl, Offerspl, incspl, excspl, Terrifspl, ATimeSlot, SplDate, SplDateSlot);
            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            //}

        }

        [WebMethod(EnableSession = true)]
        public string ActivityBookingList()
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    //var ActivityBookingList = (from obj in DB.tbl_aeActivityVouchers select obj).ToList();
                    var ActivityBookingList = (from obj in DB.tbl_aeActivityMasters
                                               join objAct in DB.tbl_aeActivityVouchers on obj.Sr_No equals objAct.Act_Id
                                               join obju in DB.tbl_AdminLogins on objAct.User_Id equals obju.sid.ToString()

                                               select new
                                               {
                                                   obj.Act_Name,
                                                   obj.City,
                                                   obj.Country,
                                                   objAct.Sr_No,
                                                   objAct.Request_Id,
                                                   objAct.Date,
                                                   objAct.Status,
                                                   objAct.BookDate,
                                                   objAct.Passenger_Name,
                                                   objAct.Mobile_No,
                                                   objAct.PaymentTransaction_Id,
                                                   objAct.Pickup_Location,
                                                   objAct.Priority_Type,
                                                   objAct.Slot_Name,
                                                   objAct.Total,
                                                   objAct.Voucher_No,
                                                   objAct.Invoice_No,
                                                   objAct.E_Mail,
                                                   objAct.Drop_Location,
                                                   objAct.BookingType,
                                                   objAct.Adults,
                                                   objAct.Childs1,
                                                   objAct.Childs2,
                                                   obju.AgencyName,

                                               }).ToList();
                    // var ActivityBookingListNew = ActivityBookingList.OrderByDescending(s => s.Sr_No);
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ActivityBookingList });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string RedirectStatus(string Actid, string Status)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    if (Status == "On")
                        Status = "True";
                    else
                        Status = "False";


                    tbl_aeActivityMaster update = DB.tbl_aeActivityMasters.Single(x => x.Sr_No == Actid);
                    update.Status = Convert.ToBoolean(Status);
                    DB.SubmitChanges();
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                }
            }
            catch
            {

                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

    }
}


