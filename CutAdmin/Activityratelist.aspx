﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Activityratelist.aspx.cs" Inherits="CutAdmin.Activityratelist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/ActivityRateList.js?v=1.0"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });

            
        });


    </script>
    <%-- <style>
        .collapse {
            display: none;
        }

            .collapse.in {
                display: block;
            }
    </style>--%>

    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <%-- <h2 class="hd2" id="ActName"> <a class="right size13 white mt5" style="cursor:pointer" data-toggle="collapse" data-target="#filter">Filter</a> </h2>--%>
        <div class="columns" style="margin-bottom:-3%">
            <div class="eight-columns">
                <hgroup id="main-title" class="thin margin-left">
                    <h3 id="ActName"></h3>
                    <hr />
                </hgroup>

            </div>
            <div class="four-columns">

                <a class="" id="btn_AddRates" style="cursor: pointer; color: blue; float: left; font-size: large; margin-top: 40px" data-toggle="collapse" data-target="#filter" onclick="AddTariffRates()">Add Rates</a>
                <a class="" id="btn_UpdateRates" style="cursor: pointer;display:none; color: blue; float: left; font-size: large; margin-top: 40px" data-toggle="collapse" data-target="#filter" onclick="UpdateRates()">Update Rates</a>

            </div>
        </div>

        <div class="with-padding">

            <details class="details margin-bottom" onclose>
                <summary>Filter</summary>
                <div class="columns" style="min-height: 130px; margin-top: 20px; margin-left: 14px">

                    <div class="two-columns">
                        <h6>From:</h6>                        
                        <input type="text" id="datepicker_From" placeholder="From Date" class="input ui-autocomplete-input full-width">                       
                    </div>

                    <div class="two-columns">
                        <h6>To :</h6>
                        <input type="text" id="datepicker_To" placeholder="To Date" class="input ui-autocomplete-input full-width">     
                    </div>

                    <div class="two-columns">
                        <h6>Rates For :</h6>
                        <select id="ddl_Rates" name="validation-select" class="select validate[required]" style="width: 145px;">
                            <option value="">Please select</option>
                            <option value="TKT">Ticket Only</option>
                            <option value="SIC">SIC</option>
                        </select>
                    </div>

                    <div class="two-columns">
                        <h6>Slot</h6>
                        <input type="text" id="txt_Slot" class="input full-width[required]" value="" style="width: 85%">                 
                    </div>

                    <%--<div class="new-row three-columns">
                        <h6>Status :</h6>
                        <select id="ddl_Status" name="validation-select" class="select validate[required]" style="width: 185px;">
                            <option value="">Please select</option>
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>

                        </select>
                    </div>--%>

                    <div class="two-columns">
                        <h6>Supplier</h6>
                        <input type="text" id="txt_Supplier" class="input full-width[required]" list="Select_Supplier" value="" style="width: 85%">                 
                         <datalist id="Select_Supplier"></datalist>

                    </div>
                    <div class="two-columns">
                        <button type="button" style="float: left; margin-top: 15px; width: 35%" class="button glossy blue-gradient" onclick="SearchTeriff()">Search</button>
                        <%--<button type="button" style="float:left;margin-top:15px;width:35%" class="button glossy blue-gradient" onclick="">Search</button>--%>
                    </div>
                </div>
            </details>

            <table class="simple-table responsive-table" id="tbl_Rates" width="100%" style="/*display:none*/">

                <thead>
                    <tr>

                        <th scope="col" align="center">Sr No.</th>
                        <th scope="col" align="center">From</th>
                        <th scope="col" align="center">To</th>
                        <%--<th scope="col" align="center">Slots</th>--%>
                        <th scope="col" align="center">Rates For</th>
                        <th scope="col" align="center" id="Slots" style="display:none">Slot</th>
                        <th scope="col" align="center">Adult Rates</th>
                        <th scope="col" align="center" id="Childage" style="display:none"></th>
                        <th scope="col" align="center" id="SChildage" style="display:none"></th>
                         <th scope="col" align="center">Currency</th>
                        <th scope="col" align="center">Supplier</th>
                        <th scope="col" align="center">Status</th>
                        <%--<th scope="col" align="center">Update</th>--%>
                    </tr>
                </thead>


            </table>

        </div>


        <table class="simple-table responsive-table" id="tbl_ActRatelist" width="100%" style="display:none">

                <thead>
                    <tr>

                        <th align="center" scope="col">S.No. </th>
                        <th align="center" scope="col">From </th>
                        <th align="center" scope="col">To </th>
                        <th align="center" scope="col">Rates For</th>
                        <th align="center" scope="col">Adult Rates</th>
                        <th align="center" scope="col">Small Child Rates </th>

                        <th align="center" scope="col">Supplier</th>
                        <th align="center" scope="col">Status</th>
                        <th align="center" scope="col">Action</th>

                    </tr>
                </thead>

            </table>


    </section>

    <!-- End main content -->



    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
    <script src="js/developr.modal.js"></script>


    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Table sort - DataTables
        var table = $('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
                { 'bSortable': false, 'aTargets': [0, 5] }
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnInitComplete': function (oSettings) {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        $('#sorting-example1').tablesorter({
            headers: {
                0: { sorter: false },
                5: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
                row = tr.next('.row-drop'),
                rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');

            // Add fake row
            $('<tr class="row-drop">' +
                '<td colspan="' + tr.children().length + '">' +
                    '<div class="float-right">' +
                        '<button type="submit" class="button glossy mid-margin-right">' +
                            '<span class="button-icon"><span class="icon-mail"></span></span>' +
                            'Send mail' +
                        '</button>' +
                        '<button type="submit" class="button glossy">' +
                            '<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
                            'Remove' +
                        '</button>' +
                    '</div>' +
                    '<strong>Name:</strong> John Doe<br>' +
                    '<strong>Account:</strong> admin<br>' +
                    '<strong>Last connect:</strong> 05-07-2011<br>' +
                    '<strong>Email:</strong> john@doe.com' +
                '</td>' +
            '</tr>').insertAfter(tr);

        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });

        // Table sort - simple
        $('#sorting-example2').tablesorter({
            headers: {
                5: { sorter: false }
            }
        });

    </script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>


</asp:Content>
