﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddTaxces.aspx.cs" Inherits="CutAdmin.AddTaxces" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
    <script src="Scripts/taxces.js?v=1.1"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Update -><a href="ExchangeMarkup.aspx" title="Exchange Rate List">Tax Details</a></h1>
            <%--<h3>Tax Details</h3>--%>
        </hgroup>
        <div class="with-padding">


            <div class="side-tabs same-height margin-bottom">
                <ul class="tabs" id="ul_Service">
                    <%--<li class="active"><a href="#Activity">Activity</a></li>
                    <li><a href="#Airlines">Airlines</a></li>
                    <li><a href="#Canteen">Canteen</a></li>
                    <li><a href="#Car">Car</a></li>
                    <li><a href="#Hotel">Hotel</a></li>
                    <li><a href="#OTB">OTB</a></li>
                    <li><a href="#Packages">Packages</a></li>
                    <li><a href="#shahzad">Shahzad</a></li>
                    <li><a href="#Visa">Visa</a></li>
                    <li><a href="#AddMore">AddMore</a></li>--%>
                </ul>

                <div class="tabs-content" id="Tab_Service">
                    "<div class="sideTabsys">  
                    
                                                                                                                                                                                                   <div class="columns">                                                                                                                                                                                <div class="four-columns twelve-columns-mobile">                                                                                                                                                     <label>Service Name:</label><div class="input full-width">                                                                                                                                           <input value="" id="txt_Service_4" class="input-unstyled full-width" placeholder="Activity" type="text">                                                                                    </div>                                                                                                                                                                                               </div>                                                                                                                                                                                               <div class="four-columns twelve-columns-mobile">                                                                                                                                                     <label>GST on Markup %</label><div class="input full-width">                                                                                                                                         <input value="" id="txtOnMarkup_4" class="input-unstyled full-width" placeholder="20" type="text">                                                                                          </div>                                                                                                                                                                                               </div>                                                                                                                                                                                               <div class="four-columns twelve-columns-mobile">                                                                                                                                                     <label>GST on Main %</label><div class="input full-width">                                                                                                                                           <input value="" id="txtOnMain_4" class="input-unstyled full-width" placeholder="0" type="text">                                                                                             </div>                                                                                                                                                                                               </div>                                                                                                                                                                                                                                                                                                                                                                                                    </div>                                                                                                                                                                                               <p class="button-height">                                                                                                                                                                            <input type="checkbox" name="switch-medium-3" id="chk_onMarkup_4" class="switch medium wider mid-margin-right" value="1" checked data-text-on="GST on Amount" data-text-off="GST on Markup"></p>                                                                                                                                                                                                                                                                                                                                                                                                      <div class="columns">                                                                                                                                                                                <div class="four-columns twelve-columns-mobile">                                                                                                                                                     <label>VAT %</label><div class="input full-width">                                                                                                                                                   <input id="txtVAT_4" value="" class="input-unstyled full-width" placeholder="5" type="text">                                                                                                </div>                                                                                                                                                                                               </div>                                                                                                                                                                                               <div class="four-columns twelve-columns-mobile">                                                                                                                                                     <label>TDS %</label><div class="input full-width">                                                                                                                                                   <input value="" id="txtTDS_4" class="input-unstyled full-width" placeholder="5" type="text">                                                                                                </div>                                                                                                                                                                                               </div>                                                                                                                                                                                                                                                                                                                                                                                                    </div>                                                                                                                                                                                               </div>                                                                                                                                                                                               <p class="text-alignright">                                                                                                                                                                          <button type="button" onclick="Save(4)" class="button anthracite-gradient">Save</button></p>                                                                                                [object Object]"

<%--                    <div id="Activity" class="with-padding">
                        <div class="sideTabsys">
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Service Name:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="Activity" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Markup %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="20" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Main %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0" type="text">
                                    </div>
                                </div>

                            </div>
                            <p class="button-height">
                                <input type="checkbox" name="switch-medium-3" id="switch-medium-3" class="switch medium wider mid-margin-right" value="1" checked data-text-on="GST on Amount" data-text-off="GST on Markup">
                            </p>

                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>VAT %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>TDS %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="text-alignright">
                            <button type="button" class="button anthracite-gradient">Save</button></p>
                    </div>

                    <div id="Airlines" class="with-padding">
                        <div class="sideTabsys">
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Service Name:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="Airlines" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Markup %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="25" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Main %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0" type="text">
                                    </div>
                                </div>

                            </div>
                            <p class="button-height">
                                <input type="checkbox" name="switch-medium-3" id="switch-medium-3" class="switch medium wider mid-margin-right" value="1" checked data-text-on="GST on Amount" data-text-off="GST on Markup">
                            </p>

                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>VAT %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>TDS %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="text-alignright">
                            <button type="button" class="button anthracite-gradient">Save</button></p>
                    </div>

                    <div id="Canteen" class="with-padding">
                        Canteen
                    </div>

                    <div id="Car" class="with-padding">
                        <div class="sideTabsys">
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Service Name:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="Car" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Markup %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="20" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Main %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0" type="text">
                                    </div>
                                </div>

                            </div>
                            <p class="button-height">
                                <input type="checkbox" name="switch-medium-3" id="switch-medium-3" class="switch medium wider mid-margin-right" value="1" checked data-text-on="GST on Amount" data-text-off="GST on Markup">
                            </p>

                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>VAT %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>TDS %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="text-alignright">
                            <button type="button" class="button anthracite-gradient">Save</button></p>
                    </div>
                    <div id="Hotel" class="with-padding">Hotel</div>
                    <div id="OTB" class="with-padding">OTB</div>
                    <div id="Packages" class="with-padding">Packages</div>
                    <div id="shahzad" class="with-padding">shahzad</div>
                    <div id="Visa" class="with-padding">Visa</div>
                    <div id="AddMore" class="with-padding">
                        <div class="sideTabsys">
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>Service Name:</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="Service Name" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Markup %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="20" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>GST on Main %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="0" type="text">
                                    </div>
                                </div>

                            </div>
                            <p class="button-height">
                                <input type="checkbox" name="switch-medium-3" id="switch-medium-3" class="switch medium wider mid-margin-right" value="1" checked data-text-on="GST on Amount" data-text-off="GST on Markup">
                            </p>

                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile">
                                    <label>VAT %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile">
                                    <label>TDS %</label><div class="input full-width">
                                        <input value="" class="input-unstyled full-width" placeholder="5" type="text">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="text-alignright">
                            <button type="button" class="button anthracite-gradient">Save</button></p>
                    </div>--%>
                </div>
            </div>

        </div>


    </section>
    <!-- End main content -->
</asp:Content>
