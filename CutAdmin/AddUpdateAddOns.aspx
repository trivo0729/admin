﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddUpdateAddOns.aspx.cs" Inherits="CutAdmin.AddUpdateAddOns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <link href="css/dynamicDiv.css" rel="stylesheet" />
    <script src="Scripts/AddOns.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
           <h1>Addons Details </h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <form>
                <div class="columns">
                    <%--<div class="new-row-mobile three-columns five-columns-tablet twelve-columns-mobile">--%>

                    <%--<p class="block-label button-height">--%>
                    <div class="three-columns two-columns-tablet twelve-columns-mobile">
                        <label for="block-label-1" class="label">Name <span class="required red" id="lbl_Name">*</span></label>
                        <input type="text" name="block-label-1" id="txt_Name" class="input full-width" value="">
                        <%--</p>--%>
                    </div>
                    <%--<p class="block-label button-height">--%>
                    <div class="three-columns two-columns-tablet twelve-columns-mobile">
                        <label for="block-label-1" class="label">Discription</label>
                        <input type="text" name="block-label-1" id="txt_Discription" class="input full-width" value="">
                        <%--</p>--%>
                    </div>
                    <%--<p class="block-label button-height">--%>
                    <div class="three-columns two-columns-tablet twelve-columns-mobile">
                        <label for="block-label-1" class="label">Type <span class="required red" id="lbl_Type">*</span></label>
                        <select class="select full-width" id="Sel_Type">
                             <option value="false"  selected="selected" >Genral</option>
                              <option value="true">Meal </option>
                        </select>
                        <%--</p>--%>
                    </div>
                    <%--<p class="button-height">--%>
                    <div class="three-columns two-columns-tablet twelve-columns-mobile" style="padding-top:0.5%">
                        <br />
                        <input type="button" class="button anthracite-gradient" onclick="SaveAddOns()" id="btn_Add" value="Save">
                        <input type="reset" class="button anthracite-gradient" id="btn_reset" value="Reset" />
                        <%--</p>--%>
                    </div>
                    <%--</div>--%>
                </div>
            </form>
            <%--<div class="new-row-tablet new-row-mobile nine-columns twelve-columns-tablet">--%>
                <table class="table responsive-table" id="tbl_AddOns">
                    <thead>
                        <tr>
                            <th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
                            <th scope="col" class="align-center hide-on-mobile">Name</th>
                            <th scope="col" class="align-center hide-on-mobile">Description</th>
                            <th scope="col" class="align-center hide-on-mobile">Type</th>
                            <th scope="col" class="align-center hide-on-mobile">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            <%--</div>--%>
            <%--</div>--%>
        </div>
    </section>
</asp:Content>
