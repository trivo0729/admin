﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddVisaDetails.aspx.cs" Inherits="CutAdmin.AddVisaDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddVisaDetails.js?v=1.1"></script>
    <script src="Scripts/EdnrdLoginDetails.js?v=1.1"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();
            //GetGroups();
            $('[data-toggle="popover"]').popover();

        });
        $(function () {
            var oldValue;
            var unsaved = false;
            $(":input").change(function () {
                unsaved = true;
            });
            $('input[type="text"]').keyup(function () {
                if (this.id != "txt_Username") {
                    // = this.value()
                    if (/^[A-Z0 -9]+$/i.test(this.value)) {

                    }
                    else {
                        this.value = '';
                    }
                }

            });

        });
        function AddVisaNo(id) {
            if (id == "VisaNo_Cut") {
                $("#txt_Application").css("display", "none")
                $("#tbl_VisaNo").css("display", "")
            }
            else {
                $("#txt_Application").css("display", "")
                $("#tbl_VisaNo").css("display", "none")
            }

        }
        function SelectType(Id) {
            if (Id == "rdb_Cut") {
                $("#Application").css('display', 'none')
                $("#lblNopix").css('display', 'none')
                $('#div_Supplier').css('display', 'none')
            }
            else {
                $("#Application").css('display', '')
                $("#lblNopix").css('display', '')
                $('#div_Supplier').css('display', '')
            }

        }
        function InsertVisaNo() {
            $("#txt_Application").val($("#visa_no1").val() + $("#visa_no2").val() + $("#visa_no3").val());
            $("#VisaNo_Model").modal("hide")
        }
    </script>
    <style type="text/css">
        div.scrollingDiv {
            width: auto;
            height: 600PX;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            debugger;

            $("#txt_Agent").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../Handler/VisaHandler.asmx/GetAgentList",
                        data: "{'name':'" + document.getElementById('txt_Agent').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    GetAgentDetails(ui.item.id)
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txtPassport").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../Handler/VisaHandler.asmx/GetPassportNo",
                        data: "{'PassportNo':'" + document.getElementById('txtPassport').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    GetPassportDetails(ui.item.id)
                    //$('#hdnDCode').val(ui.item.id);
                }
            });
        });
        function SelectType(Id) {
            if (Id == "rdb_Cut") {
                $("#Application").css('display', 'none')
                $("#lblNopix").css('display', 'none')
                $('#div_Supplier').css('display', 'none')
            }
            else {
                $("#Application").css('display', '')
                $("#lblNopix").css('display', '')
                $('#div_Supplier').css('display', '')
            }

        }
        function GetPassportDetails(sid) {
            $.ajax({
                type: "POST",
                url: "../Handler/VisaHandler.asmx/GetPassportDetails",
                data: '{"Sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retcode == 1) {
                        var arrDetails = result.Visa;
                        if (result.Valid == false) {
                            // Passport Details //
                            $("#txtIssuing").val(arrDetails[0].IssuingGovernment);
                            $("#txtDoi").val(arrDetails[0].IssuingDate);
                            $("#txtED").val(arrDetails[0].ExpDate);
                            $("#txtAddress1").val(arrDetails[0].AddressLine1);
                            $("#txtAddress2").val(arrDetails[0].AddressLine2);
                            $("#txtCity").val(arrDetails[0].City);
                            $("#selCountry").val(arrDetails[0].Country);
                            $("#txtTelephone").val(arrDetails[0].Telephone);

                            // Passenger Details //
                            $('#txtFirst').val(arrDetails[0].FirstName);
                            $('#txtMiddle').val(arrDetails[0].MiddleName);
                            $('#txtLast').val(arrDetails[0].LastName);
                            $('#txtFather').val(arrDetails[0].FatherName);
                            $('#txtMother').val(arrDetails[0].MotherName);
                            $('#txtHusband').val(arrDetails[0].HusbandMame)
                            $('#selLanguage').val(arrDetails[0].Language);
                            $('#selGender').val(arrDetails[0].Gender);
                            $('#selMarital').val(arrDetails[0].MaritalStatus);
                            $('#selNationality').val(arrDetails[0].PresentNationality);
                            $('#txtDob').val(arrDetails[0].Birth);
                            $('#selCountry1').val(arrDetails[0].BirthCountry);
                            $('#selDepartment').val();
                            $('#txt_BirthPlace').val(arrDetails[0].BirthPlace);
                            $('#selReligion').val(arrDetails[0].Religion);
                            $("#selProfession option:selected").text(arrDetails[0].Profession)
                        }
                        else {
                            Ok("Visa for Passport Id " + $("#txtPassport").val() + " is already Approved,Please click Ok to See visa Copy..", "VisaCopy", [arrDetails[0].Vcode])
                        }




                    }
                },
                error: function () {
                }
            });
        }
        function VisaCopy(Code) {
            $('#Ticket').empty();
            var tRow = '';
            tRow = '<iframe src="../VisaImages/' + Code + '_6.pdf" width="80%" height="500px"  />';
            $('#Ticket').append(tRow)
            $("#Modal96Hours").modal("show")
            Cancel()
        }
        var Currency;
        function GetCurrency(CurrencyImage) {
            Currency = CurrencyImage
            $(".Currency").addClass(CurrencyImage)

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Visa Application Form</h1>

          
        </hgroup>
        <div class="with-padding">
            <form action="#" class="frmSupplier">
                <%-- <h3>Company Information</h3>--%>
                <hr />
                <div class="columns">
                    <div class="two-columns twelve-columns-mobile">
                        <input type="radio" name="radio" id="rdb_Cut" value="Normal" onchange="SelectType(this.id)" class="radio mid-margin-left" checked> 
                        <label for="rdb_Cut" class="label">Online Visa</label>
                    </div>
                    <div class="two-columns twelve-columns-mobile">
                        <input type="radio" name="radio" id="rdb_Other" value="Urgent" onchange="SelectType(this.id)" class="radio mid-margin-left">
                         <label for="Apply Offline" class="label"> Apply Offline </label>
                    </div>
                    <div class="three-columns twelve-columns-mobile" style="display: none" id="lblNopix">
                        <label>Supplier </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivselSupplier" >
                            <select id="selSupplier" name="validation-select" onchange="Notification()" class="select" tabindex="-1">
                                <%--<option value="-" selected="selected">-Select Any Process-</option>
                                <option value="1">Normal</option>
                                <option value="2">Urgent</option>--%>
                            </select>
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                     <div class="one-columns twelve-columns-mobile" style="display: none" id="div_Supplier">
                        <span class="button-group" style="float: left; cursor: pointer; margin-left: 2px">
                            <a href="#" class="button" title="New Supplier" style="float: right; cursor: pointer" onclick="OpenDilogBox()">
                                <span class="icon-page-list-inverted"></span>
                            </a>
                        </span>
                    </div>
                </div>  
                 <div class="columns" style="display: none" id="Application">
                     <div class="two-columns twelve-columns-mobile">
                        <input type="radio" name="AppNo" onchange="AddVisaNo(this.id)" id="AppNo_Cut" value="" class="radio mid-margin-left" checked> 
                        <label for="rdb_Cut" class="label">Application No</label>
                    </div>
                    <div class="two-columns twelve-columns-mobile">
                        <input type="radio" id="VisaNo_Cut"  name="AppNo" onchange="AddVisaNo(this.id)" value="" class="radio mid-margin-left">
                            <label for="Apply Offline" class="label"> Visa No </label>
                    </div> 
                    <div class="three-columns twelve-columns-mobile">
                        <div class="input full-width">
                        <input type="text"  id="txt_Application" class="input-unstyled full-width" />
                          </div> 
                    </div>
                     <div class="three-columns twelve-columns-mobile">
                         <table id="tbl_VisaNo" style="display: none">
                            <tr>
                                <td style="width: 25%">
                                    <select name="visa_no1" id="visa_no1" class="select" tabindex="-1" >
                                        <option value="212">212</option>
                                        <option value="216">216</option>
                                        <option value="201">201</option>
                                        <option value="204">204</option>
                                        <option value="206">206</option>
                                        <option value="208">208</option>
                                        <option value="210">210</option>
                                        <option value="214">214</option>
                                    </select></td>
                                <td style="width: 25%">
                                    <select name="visa_no2" id="visa_no2" class="select" tabindex="-1">
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>

                                    </select></td>
                                <td>
                                    <div class="input full-width">
                                    <input name="visa_no3" id="visa_no3" type="text" size="24" maxlength="8" class="input-unstyled full-width" >
                                    </div>
                                </td>
                            </tr>
                            </table>
                     </div>
                </div>
               <div class="columns">
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Agency<span class="red">*</span>:</label>
                    <div class="input full-width"><input type="text" id="txt_Agent" title="Select Agent" class="input-unstyled full-width">
                        <input type="hidden" id="hdnDCode" />
                    </div>
                </div>
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Agent Name::<span class="orange" id="spAgent"> - </span></label>
                    <p>Agent Code:<span class="orange" id="spUniqueCode"> - </span> , Available limit:<span class="orange" id="spAvailableLimit">00.00</span></p>
                     <input type="hidden" id="txt_Avail" value="0" />
                </div>
                <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                    <label> Credit Limit:[<span class="orange" id="spCreditLimit">0.00/0.00</span>]</label>

                    <p>One Time Limit:[<span class="orange" id="spOneTimeCreditLimit">00.00</span>]</p>
                </div>



            </div>
             <h3>Passport Details</h3>
                <hr />
                <div class="columns">
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Passport No<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtPassport" onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Passport Number" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Please of Issue (As per Passport)<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtIssuing" onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Issuing Government" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Date Of Issue<span class="red">*</span> :</label>
                        <span class="icon-calendar"></span>
                        <span class="input full-width">
                            <input id="txtDoi" onblur="CheckValidation(this.id)" name="datepicker" class="input-unstyled" placeholder="dd-mm-yyyy" type="text">
                        </span>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Expiry Date<span class="red">*</span> :</label>
                        <span class="icon-calendar"></span>
                        <span class="input full-width">
                            <input id="txtED" onblur="CheckValidation(this.id)"  name="datepicker" class="input-unstyled" placeholder="dd-mm-yyyy" type="text">
                        </span>
                    </div>

                </div>
                 <hr />
                <div class="columns">
                    <%--<div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Supplier<span class="red">*</span>:</label><div class="input full-width">
                            <input placeholder="Supplier" id="txt_Sponsor" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Application No</label><div class="input full-width">
                            <input placeholder="Application No" id="txt_ApplicationNo" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Visa No</label><br />
                        <div class="input full-width">
                            <input placeholder="Visa No" id="txt_VisaNo" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>--%>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Processing </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivProcessing">
                            <select id="selProcessing" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Select Any Process-</option>
                                <option value="1">Normal</option>
                                <option value="2">Urgent</option>
                            </select>
                        </div>
                    </div>

                    <div class="six-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Visa Type </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivService">
                            <select id="selService" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Salect Visa Type-</option>
                                <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                <option value="1">14 days Service VISA</option>
                                <option value="2">30 days Tourist Single Entry</option>
                                <option value="3">30 days Tourist Multiple Entry</option>
                                <option value="4">90 days Tourist Single Entry</option>
                                <option value="5">90 days Tourist Multiple Entry</option>
                                <option value="7">90 Days Tourist Single Entry Convertible</option>

                            </select>
                        </div>
                    </div>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Nationality </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivVisaCountry">
                            <select id="selVisaCountry" name="validation-select" class="select" tabindex="-1" onchange="Notification()">
                                <option value="-" selected="selected">-Salect Visa Country-</option>
                                <option value="Indian">Indian</option>
                                <option value="Pakistani">Pakistani</option>
                                <option value="Sri Lankan">Sri Lankan</option>
                                <option value="South African">South African</option>
                                <option value="Mozambican">Mozambican</option>
                                <option value="Malaysian">Malaysian</option>
                                <option value="Indonesian">Indonesian</option>
                                <option value="Philippines">Philippines</option>
                            </select>
                        </div>
                    </div>
                    <%-- <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>State:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_State" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Country" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Pin / Zip Code </label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Zip" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Pin / Zip Code " type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Email:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Email" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Telephone No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Phone" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Telephone No" type="text">
                        </div>
                    </div>

                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Fax No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Fax" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Fax No." type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Company Mobile No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Mobile" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Company Mobile No" type="text">
                        </div>
                    </div>
                    <div class="six-columns six-columns-tablet twelve-columns-mobile">
                        <label>Address:</label><br />
                        <div class="input full-width">
                            <input id="txt_Address" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Address " type="text">
                        </div>
                    </div>--%>
                </div>
                <h4>Visa Charges</h4>
                <hr />
                <div class="columns">
                    <div class="two-columns twelve-columns-mobile">
                        <label>Visa Fee</label>
                         <label style="color: black" id="VisaFee">0.00</label></div>
                    <div class="two-columns twelve-columns-mobile">
                        <label>Other Fee</label>
                        <label style="color: black" id="OtherFee">0.00</label></div>
                    <div class="two-columns twelve-columns-mobile">
                        <label>Service Tax</label>
                         <label style="color: black" id="ServiceTax">0.00</label></div>
                    <div class="two-columns twelve-columns-mobile">
                        <label>Total Amount</label>
                        <label style="color: black" id="TotalAmount">0.00</label></div>
                </div>
                <h4>Passenger Details</h4>
                <hr />
                <div class="columns">
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>First Name</label>
                         <span class="red">*</span>
                        <div class="input full-width">
                            <input id="txtFirst" placeholder="First Name" onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Middle Name :</label><br />
                        <div class="input full-width">
                            <input id="txtMiddle" placeholder=" Middle Name" name="prompt-value" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Last Name</label>
                        <span class="red">*</span>
                        <div class="input full-width">
                            <input id="txtLast" placeholder=" Last Name"  onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Father's Name</label><span class="red">*</span><br />
                        <div class="input full-width">
                            <input id="txtFather" placeholder="Father Name"  onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Mother's Name</label><span class="red">*</span><br />
                        <div class="input full-width">
                            <input id="txtMother" placeholder="Mother Name"  onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Language Spoken </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivLanguage">
                            <select id="selLanguage" name="validation-select" class="select" tabindex="-1" onblur="CheckValidation(this.id)">
                                <option value="-" selected="selected">-Select Language-</option>
                                <option value="1">ARABIC</option>
                                <option value="2">ENGLISH</option>
                                <option value="3">FRENCH</option>
                                <option value="4">HINDI</option>
                                <option value="5">URDU</option>
                                <option value="6">GERMAN</option>
                                <option value="7">ITALIAN</option>
                                <option value="8">SWAHILI</option>
                                <option value="9">SPANISH</option>
                                <option value="10">RUSSIAN</option>
                                <option value="11">INDONESIAN</option>
                                <option value="12">MALAYSIAN</option>
                                <option value="13">MALAYAN</option>
                                <option value="14">BALOCHI</option>
                                <option value="15">TURKISH</option>
                                <option value="16">JAPANESE</option>
                                <option value="17">CHINESE</option>
                                <option value="18">THAI</option>
                                <option value="19">TAGALOG</option>
                                <option value="20">FARSI</option>
                                <option value="21">SINHALESE</option>
                                <option value="22">BANGLA</option>
                                <option value="23">TELUGU</option>
                                <option value="98">Unknown</option>
                                <option value="99">Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Gender </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivGender">
                            <select id="selGender" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Select Gender-</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Marital Status </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivMarital">
                            <select id="selMarital" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Select Any Process-</option>
                                <option value="1">Single</option>
                                <option value="2">Married</option>
                                <option value="3">Divorced</option>
                                <option value="4">Window</option>
                                <option value="5">Deceased</option>
                                <option value="6">Unspecific</option>
                                <option value="7">Child</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Spouse Name</label><span class="red">*</span><br />
                        <div class="input full-width">
                            <input id="txtHusband" placeholder="Spouse Name"  onblur="CheckValidation(this.id)" name="prompt-value" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Present  Nationality </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivNationality">
                            <select id="selNationality" name="validation-select" class="select" tabindex="-1" onblur="CheckValidation(this.id)">
                                <option value="-">-Select Nationality-</option>
                                <option value="209">AFGHANISTAN</option>
                                <option value="473">ALBANIA</option>
                                <option value="131">ALGERIA</option>
                                <option value="445">ANDORRA</option>
                                <option value="305">ANGOLA</option>
                                <option value="663">ANTIGUA &amp; BARBUDA</option>
                                <option value="605">ARGENTINA</option>
                                <option value="263">ARMENIA</option>
                                <option value="701">AUSTRALIA</option>
                                <option value="417">AUSTRIA</option>
                                <option value="265">AZERBAIJAN</option>
                                <option value="619">BAHAMAS</option>
                                <option value="107">BAHRAIN</option>
                                <option value="207">BANGLADESH</option>
                                <option value="657">BARBADOS</option>
                                <option value="411">BELGIUM</option>
                                <option value="651">BELIZE</option>
                                <option value="369">BENIN</option>
                                <option value="634">BERMUDA</option>
                                <option value="211">BHUTAN</option>
                                <option value="609">BOLIVIA</option>
                                <option value="463">BOSNIA AND HERZEG</option>
                                <option value="375">BOTSWANA</option>
                                <option value="603">BRAZIL</option>
                                <option value="401">BRITAIN</option>
                                <option value="253">BRUNEI</option>
                                <option value="465">BULGARIA</option>
                                <option value="351">BURKINA FASO</option>
                                <option value="307">BURUNDI</option>
                                <option value="393">CABO VERDE</option>
                                <option value="215">CAMBODIA</option>
                                <option value="309">CAMEROON</option>
                                <option value="501">CANADA</option>
                                <option value="363">CENTRAL AFRICA REP</option>
                                <option value="311">CHAD</option>
                                <option value="607">CHILE</option>
                                <option value="219">CHINA</option>
                                <option value="611">COLOMBIA</option>
                                <option value="642">COMONWEALTH DOMINICA</option>
                                <option value="301">COMOROS</option>
                                <option value="313">CONGO Republic</option>
                                <option value="623">COSTARICA</option>
                                <option value="453">CROATIA</option>
                                <option value="621">CUBA</option>
                                <option value="431">CYPRUS</option>
                                <option value="452">CZECH</option>
                                <option value="274">DAGHYSTAN</option>
                                <option value="315">DAHOOMI</option>
                                <option value="361">DEM REP OF CONGO</option>
                                <option value="423">DENMARK</option>
                                <option value="141">DJIBOUTI</option>
                                <option value="625">DOMINICAN</option>
                                <option value="613">ECUADOR</option>
                                <option value="125">EGYPT</option>
                                <option value="635">EL SALVADOR</option>
                                <option value="251">ENTIAGO</option>
                                <option value="303">ERITREN</option>
                                <option value="459">ESTONIA</option>
                                <option value="317">ETHIOPIA</option>
                                <option value="705">FIJI</option>
                                <option value="433">FINLAND</option>
                                <option value="403">FRANCE</option>
                                <option value="659">FRENCH GUIANA</option>
                                <option value="371">GABON</option>
                                <option value="387">GAMBIA</option>
                                <option value="273">GEORGIA</option>
                                <option value="407">GERMANY</option>
                                <option value="381">GHAMBIA</option>
                                <option value="319">GHANA</option>
                                <option value="385">GHINIA BISSAU</option>
                                <option value="429">GREECE</option>
                                <option value="365">GREENLAND</option>
                                <option value="649">GRENADA</option>
                                <option value="627">GUATAMALA</option>
                                <option value="653">GUYANA</option>
                                <option value="639">HAITI</option>
                                <option value="409">HOLLAND</option>
                                <option value="647">HONDURAS</option>
                                <option value="223">HONG KONG</option>
                                <option value="467">HUNGARY</option>
                                <option value="443">ICELAND</option>
                                <option value="205" selected="selected">INDIA</option>
                                <option value="243">INDONESIA</option>
                                <option value="201">IRAN</option>
                                <option value="113">IRAQ</option>
                                <option value="427">IRELAND</option>
                                <option value="405">ITALY</option>
                                <option value="323">IVORY COAST</option>
                                <option value="629">JAMAICA</option>
                                <option value="231">JAPAN</option>
                                <option value="121">JORDAN</option>
                                <option value="503">KAIMAN ISLAN</option>
                                <option value="715">KALDUNIA NEW</option>
                                <option value="261">KAZAKHESTAN</option>
                                <option value="325">KENYA</option>
                                <option value="667">KINGSTONE</option>
                                <option value="391">KIRIBATI</option>
                                <option value="476">KOSOVA</option>
                                <option value="105">KUWAIT</option>
                                <option value="489">KYRGYZ REPUBLIC</option>
                                <option value="117">LABANON</option>
                                <option value="245">LAOS</option>
                                <option value="461">LATVIA</option>
                                <option value="377">LESOTHO</option>
                                <option value="327">LIBERIA</option>
                                <option value="127">LIBYA</option>
                                <option value="449">LIECHTENSTEIN</option>
                                <option value="457">LITHUANIA</option>
                                <option value="413">LUXEMBOURG</option>
                                <option value="259">MACAU</option>
                                <option value="329">MADAGASCAR</option>
                                <option value="333">MALAWI</option>
                                <option value="241">MALAYSIA</option>
                                <option value="257">MALDIVES</option>
                                <option value="335">MALI</option>
                                <option value="435">MALTA</option>
                                <option value="727">MARSHALL ISLAND</option>
                                <option value="661">MARTINIQUE</option>
                                <option value="721">MARYANA ISLAND</option>
                                <option value="135">MAURITANIA</option>
                                <option value="367">MAURITIUS</option>
                                <option value="601">MEXICO</option>
                                <option value="732">MICRONESIA</option>
                                <option value="481">MOLDOVA</option>
                                <option value="439">MONACO</option>
                                <option value="249">MONGOLIA</option>
                                <option value="488">MONTENEGRO</option>
                                <option value="133">MOROCCO</option>
                                <option value="337">MOZAMBIQUE</option>
                                <option value="373">NAMEBIA</option>
                                <option value="737">NAURU</option>
                                <option value="235">NEPAL</option>
                                <option value="707">NEW GHINIA</option>
                                <option value="703">NEW ZEALAND</option>
                                <option value="631">NICARAGUA</option>
                                <option value="339">NIGER</option>
                                <option value="341">NIGERIA</option>
                                <option value="229">NORTH KOREA</option>
                                <option value="421">NORWAY</option>
                                <option value="723">OKINAWA</option>
                                <option value="203">PAKISTAN</option>
                                <option value="669">PALAU</option>
                                <option value="123">PALESTINE</option>
                                <option value="633">PANAMA</option>
                                <option value="731">PAPUA NEW GUINE</option>
                                <option value="645">PARAGUAY</option>
                                <option value="615">PERU</option>
                                <option value="237">PHILIPPINES</option>
                                <option value="471">POLAND</option>
                                <option value="425">PORTUGAL</option>
                                <option value="641">PUERTO RICO</option>
                                <option value="109">QATAR</option>
                                <option value="485">REPUBL. OF MACEDONIA</option>
                                <option value="490">REPUBLIC OF BELARUS</option>
                                <option value="213">REPUBLIC OF MYANMAR</option>
                                <option value="321">REPUPLIC OF GUINEA</option>
                                <option value="469">ROMANIA</option>
                                <option value="343">ROWANDA</option>
                                <option value="477">RUSSIA</option>
                                <option value="491">SAINT LUCIA</option>
                                <option value="665">SAINT VINSENT</option>
                                <option value="447">SAN MARINO</option>
                                <option value="395">SAO TOME</option>
                                <option value="103">SAUDI ARABIA</option>
                                <option value="345">SENEGAL</option>
                                <option value="383">SICHEL</option>
                                <option value="347">SIERRA LEONE</option>
                                <option value="225">SINGAPORE</option>
                                <option value="454">SLOVAKIA</option>
                                <option value="455">SLOVENIA</option>
                                <option value="725">SOLOMON ISLAND</option>
                                <option value="139">SOMALIA</option>
                                <option value="349">SOUTH AFRICA</option>
                                <option value="227">SOUTH KOREA</option>
                                <option value="138">SOUTH SUDAN</option>
                                <option value="437">SPAIN</option>
                                <option value="217">SRI LANKA</option>
                                <option value="606">ST HELENA</option>
                                <option value="487">ST KITTS-NAVIS</option>
                                <option value="137">SUDAN</option>
                                <option value="111">SULTANATE OF OMAN</option>
                                <option value="462">SURBIA</option>
                                <option value="655">SURINAME</option>
                                <option value="379">SWAZILAND</option>
                                <option value="419">SWEDEN</option>
                                <option value="415">SWIZERLAND</option>
                                <option value="119">SYRIA</option>
                                <option value="713">TAHITI</option>
                                <option value="221">TAIWAN</option>
                                <option value="267">TAJIKSTAN</option>
                                <option value="353">TANZANIA</option>
                                <option value="711">TASMANIA</option>
                                <option value="239">THAILAND</option>
                                <option value="483">THE HELLENIC REPBL</option>
                                <option value="709">TIMOR LESTE</option>
                                <option value="255">TONGA</option>
                                <option value="637">TRINIDAD</option>
                                <option value="129">TUNISIA</option>
                                <option value="475">TURKEY</option>
                                <option value="269">TURKMENISTAN</option>
                                <option value="735">TUVALU</option>
                                <option value="502">U S A</option>
                                <option value="357">UGANDA</option>
                                <option value="479">UKRAINE</option>
                                <option value="643">URGWAY</option>
                                <option value="271">UZBAKISTAN</option>
                                <option value="505">United Nations</option>
                                <option value="733">VANVATU</option>
                                <option value="441">VATICAN</option>
                                <option value="617">VENEZUELA</option>
                                <option value="233">VIETNAM</option>
                                <option value="729">W SAMOA</option>
                                <option value="115">YEMEN</option>
                                <option value="464">YUGOSLAVIA</option>
                                <option value="359">ZAMBIA</option>
                                <option value="331">ZIMBABWE</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Birth Date</label><br />
                        <span class="input full-width">
                            <span class="icon-calendar"></span>
                            <input id="txtDob" name="datepicker"  class="input-unstyled" placeholder="dd-mm-yyyy" type="text">
                        </span>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Birth Country</label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivCountry1">
                            <select id="selCountry1" name="validation-select" class="select" tabindex="-1" onblur="CheckValidation(this.id)">
                                <option value="-">-Select Nationality-</option>
                                <option value="209">AFGHANISTAN</option>
                                <option value="473">ALBANIA</option>
                                <option value="131">ALGERIA</option>
                                <option value="445">ANDORRA</option>
                                <option value="305">ANGOLA</option>
                                <option value="663">ANTIGUA &amp; BARBUDA</option>
                                <option value="605">ARGENTINA</option>
                                <option value="263">ARMENIA</option>
                                <option value="701">AUSTRALIA</option>
                                <option value="417">AUSTRIA</option>
                                <option value="265">AZERBAIJAN</option>
                                <option value="619">BAHAMAS</option>
                                <option value="107">BAHRAIN</option>
                                <option value="207">BANGLADESH</option>
                                <option value="657">BARBADOS</option>
                                <option value="411">BELGIUM</option>
                                <option value="651">BELIZE</option>
                                <option value="369">BENIN</option>
                                <option value="634">BERMUDA</option>
                                <option value="211">BHUTAN</option>
                                <option value="609">BOLIVIA</option>
                                <option value="463">BOSNIA AND HERZEG</option>
                                <option value="375">BOTSWANA</option>
                                <option value="603">BRAZIL</option>
                                <option value="401">BRITAIN</option>
                                <option value="253">BRUNEI</option>
                                <option value="465">BULGARIA</option>
                                <option value="351">BURKINA FASO</option>
                                <option value="307">BURUNDI</option>
                                <option value="393">CABO VERDE</option>
                                <option value="215">CAMBODIA</option>
                                <option value="309">CAMEROON</option>
                                <option value="501">CANADA</option>
                                <option value="363">CENTRAL AFRICA REP</option>
                                <option value="311">CHAD</option>
                                <option value="607">CHILE</option>
                                <option value="219">CHINA</option>
                                <option value="611">COLOMBIA</option>
                                <option value="642">COMONWEALTH DOMINICA</option>
                                <option value="301">COMOROS</option>
                                <option value="313">CONGO Republic</option>
                                <option value="623">COSTARICA</option>
                                <option value="453">CROATIA</option>
                                <option value="621">CUBA</option>
                                <option value="431">CYPRUS</option>
                                <option value="452">CZECH</option>
                                <option value="274">DAGHYSTAN</option>
                                <option value="315">DAHOOMI</option>
                                <option value="361">DEM REP OF CONGO</option>
                                <option value="423">DENMARK</option>
                                <option value="141">DJIBOUTI</option>
                                <option value="625">DOMINICAN</option>
                                <option value="613">ECUADOR</option>
                                <option value="125">EGYPT</option>
                                <option value="635">EL SALVADOR</option>
                                <option value="251">ENTIAGO</option>
                                <option value="303">ERITREN</option>
                                <option value="459">ESTONIA</option>
                                <option value="317">ETHIOPIA</option>
                                <option value="705">FIJI</option>
                                <option value="433">FINLAND</option>
                                <option value="403">FRANCE</option>
                                <option value="659">FRENCH GUIANA</option>
                                <option value="371">GABON</option>
                                <option value="387">GAMBIA</option>
                                <option value="273">GEORGIA</option>
                                <option value="407">GERMANY</option>
                                <option value="381">GHAMBIA</option>
                                <option value="319">GHANA</option>
                                <option value="385">GHINIA BISSAU</option>
                                <option value="429">GREECE</option>
                                <option value="365">GREENLAND</option>
                                <option value="649">GRENADA</option>
                                <option value="627">GUATAMALA</option>
                                <option value="653">GUYANA</option>
                                <option value="639">HAITI</option>
                                <option value="409">HOLLAND</option>
                                <option value="647">HONDURAS</option>
                                <option value="223">HONG KONG</option>
                                <option value="467">HUNGARY</option>
                                <option value="443">ICELAND</option>
                                <option value="205" selected="selected">INDIA</option>
                                <option value="243">INDONESIA</option>
                                <option value="201">IRAN</option>
                                <option value="113">IRAQ</option>
                                <option value="427">IRELAND</option>
                                <option value="405">ITALY</option>
                                <option value="323">IVORY COAST</option>
                                <option value="629">JAMAICA</option>
                                <option value="231">JAPAN</option>
                                <option value="121">JORDAN</option>
                                <option value="503">KAIMAN ISLAN</option>
                                <option value="715">KALDUNIA NEW</option>
                                <option value="261">KAZAKHESTAN</option>
                                <option value="325">KENYA</option>
                                <option value="667">KINGSTONE</option>
                                <option value="391">KIRIBATI</option>
                                <option value="476">KOSOVA</option>
                                <option value="105">KUWAIT</option>
                                <option value="489">KYRGYZ REPUBLIC</option>
                                <option value="117">LABANON</option>
                                <option value="245">LAOS</option>
                                <option value="461">LATVIA</option>
                                <option value="377">LESOTHO</option>
                                <option value="327">LIBERIA</option>
                                <option value="127">LIBYA</option>
                                <option value="449">LIECHTENSTEIN</option>
                                <option value="457">LITHUANIA</option>
                                <option value="413">LUXEMBOURG</option>
                                <option value="259">MACAU</option>
                                <option value="329">MADAGASCAR</option>
                                <option value="333">MALAWI</option>
                                <option value="241">MALAYSIA</option>
                                <option value="257">MALDIVES</option>
                                <option value="335">MALI</option>
                                <option value="435">MALTA</option>
                                <option value="727">MARSHALL ISLAND</option>
                                <option value="661">MARTINIQUE</option>
                                <option value="721">MARYANA ISLAND</option>
                                <option value="135">MAURITANIA</option>
                                <option value="367">MAURITIUS</option>
                                <option value="601">MEXICO</option>
                                <option value="732">MICRONESIA</option>
                                <option value="481">MOLDOVA</option>
                                <option value="439">MONACO</option>
                                <option value="249">MONGOLIA</option>
                                <option value="488">MONTENEGRO</option>
                                <option value="133">MOROCCO</option>
                                <option value="337">MOZAMBIQUE</option>
                                <option value="373">NAMEBIA</option>
                                <option value="737">NAURU</option>
                                <option value="235">NEPAL</option>
                                <option value="707">NEW GHINIA</option>
                                <option value="703">NEW ZEALAND</option>
                                <option value="631">NICARAGUA</option>
                                <option value="339">NIGER</option>
                                <option value="341">NIGERIA</option>
                                <option value="229">NORTH KOREA</option>
                                <option value="421">NORWAY</option>
                                <option value="723">OKINAWA</option>
                                <option value="203">PAKISTAN</option>
                                <option value="669">PALAU</option>
                                <option value="123">PALESTINE</option>
                                <option value="633">PANAMA</option>
                                <option value="731">PAPUA NEW GUINE</option>
                                <option value="645">PARAGUAY</option>
                                <option value="615">PERU</option>
                                <option value="237">PHILIPPINES</option>
                                <option value="471">POLAND</option>
                                <option value="425">PORTUGAL</option>
                                <option value="641">PUERTO RICO</option>
                                <option value="109">QATAR</option>
                                <option value="485">REPUBL. OF MACEDONIA</option>
                                <option value="490">REPUBLIC OF BELARUS</option>
                                <option value="213">REPUBLIC OF MYANMAR</option>
                                <option value="321">REPUPLIC OF GUINEA</option>
                                <option value="469">ROMANIA</option>
                                <option value="343">ROWANDA</option>
                                <option value="477">RUSSIA</option>
                                <option value="491">SAINT LUCIA</option>
                                <option value="665">SAINT VINSENT</option>
                                <option value="447">SAN MARINO</option>
                                <option value="395">SAO TOME</option>
                                <option value="103">SAUDI ARABIA</option>
                                <option value="345">SENEGAL</option>
                                <option value="383">SICHEL</option>
                                <option value="347">SIERRA LEONE</option>
                                <option value="225">SINGAPORE</option>
                                <option value="454">SLOVAKIA</option>
                                <option value="455">SLOVENIA</option>
                                <option value="725">SOLOMON ISLAND</option>
                                <option value="139">SOMALIA</option>
                                <option value="349">SOUTH AFRICA</option>
                                <option value="227">SOUTH KOREA</option>
                                <option value="138">SOUTH SUDAN</option>
                                <option value="437">SPAIN</option>
                                <option value="217">SRI LANKA</option>
                                <option value="606">ST HELENA</option>
                                <option value="487">ST KITTS-NAVIS</option>
                                <option value="137">SUDAN</option>
                                <option value="111">SULTANATE OF OMAN</option>
                                <option value="462">SURBIA</option>
                                <option value="655">SURINAME</option>
                                <option value="379">SWAZILAND</option>
                                <option value="419">SWEDEN</option>
                                <option value="415">SWIZERLAND</option>
                                <option value="119">SYRIA</option>
                                <option value="713">TAHITI</option>
                                <option value="221">TAIWAN</option>
                                <option value="267">TAJIKSTAN</option>
                                <option value="353">TANZANIA</option>
                                <option value="711">TASMANIA</option>
                                <option value="239">THAILAND</option>
                                <option value="483">THE HELLENIC REPBL</option>
                                <option value="709">TIMOR LESTE</option>
                                <option value="255">TONGA</option>
                                <option value="637">TRINIDAD</option>
                                <option value="129">TUNISIA</option>
                                <option value="475">TURKEY</option>
                                <option value="269">TURKMENISTAN</option>
                                <option value="735">TUVALU</option>
                                <option value="502">U S A</option>
                                <option value="357">UGANDA</option>
                                <option value="479">UKRAINE</option>
                                <option value="643">URGWAY</option>
                                <option value="271">UZBAKISTAN</option>
                                <option value="505">United Nations</option>
                                <option value="733">VANVATU</option>
                                <option value="441">VATICAN</option>
                                <option value="617">VENEZUELA</option>
                                <option value="233">VIETNAM</option>
                                <option value="729">W SAMOA</option>
                                <option value="115">YEMEN</option>
                                <option value="464">YUGOSLAVIA</option>
                                <option value="359">ZAMBIA</option>
                                <option value="331">ZIMBABWE</option>
                            </select>
                        </div>
                    </div>
                     <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Birth Place</label><br />
                        <div class="input full-width">
                            <input id="txt_BirthPlace" name="prompt-value" value="" onblur="CheckValidation(this.id)" class="input-unstyled full-width" placeholder="Birth Place" type="text">
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Religion </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivReligion">
                            <select id="selReligion" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Select Religion-</option>
                                <option value="0">Unknown</option>
                                <option value="1">MUSLIM</option>
                                <option value="2">CHRISTIAN</option>
                                <option value="3">HINDHU</option>
                                <option value="4">BUDIST</option>
                                <option value="5">SIKH</option>
                                <option value="6">KADIANI</option>
                                <option value="7">BAHAEI</option>
                                <option value="8">JEWISH</option>
                                <option value="9">Zorostrian</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Profession </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivProfession">
                            <select id="selProfession" name="validation-select" class="select" tabindex="-1">
                                <option value="-" selected="selected">-Select Profession-</option>
                                <option value="NONE">NONE</option>
                                <option value="STUDENT">STUDENT</option>
                                <option value="SALES REPRESENTATIVE">SALES REPRESENTATIVE</option>
                                <option value="MARKETING ASSISTANT">MARKETING ASSISTANT</option>
                                <option value="MARKETING EXECUTIVE">MARKETING EXECUTIVE</option>
                                <option value="BUSINESSMAN">BUSINESSMAN</option>
                                <option value="BUSINESSWOMAN">BUSINESSWOMAN</option>
                                <option value="BUSINESS PERSON">BUSINESS PERSON</option>
                                <option value="BUSINESS">BUSINESS</option>
                                <option value="HOUSE WIFE">HOUSE WIFE</option>
                            </select>
                        </div>
                    </div>
                </div>
               

                <h3>Address Details (as Per Passport)</h3>
                <hr />
                <div class="columns">
                    <div class="twelve-columns six-columns-tablet twelve-columns-mobile">
                        <label>Address1<span class="red">*</span></label>
                        <div class="input full-width">
                            <input id="txtAddress1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Address1" type="text">
                        </div>
                    </div>
                    <div class="twelve-columns six-columns-tablet twelve-columns-mobile">
                        <label>Address2<span class="red">*</span></label><br />
                        <div class="input full-width">
                            <input id="txtAddress2" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Address2" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>City<span class="red">*</span></label><br />
                        <div class="input full-width">
                            <input id="selCity" name="prompt-value" value="" class="input-unstyled full-width" placeholder="City" type="text">
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Country </label>
                        <span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivCountry">
                            <select id="selCountry" name="validation-select" class="select" tabindex="-1" ">
                                <option value="-">-Select Nationality-</option>
                                <option value="209">AFGHANISTAN</option>
                                <option value="473">ALBANIA</option>
                                <option value="131">ALGERIA</option>
                                <option value="445">ANDORRA</option>
                                <option value="305">ANGOLA</option>
                                <option value="663">ANTIGUA &amp; BARBUDA</option>
                                <option value="605">ARGENTINA</option>
                                <option value="263">ARMENIA</option>
                                <option value="701">AUSTRALIA</option>
                                <option value="417">AUSTRIA</option>
                                <option value="265">AZERBAIJAN</option>
                                <option value="619">BAHAMAS</option>
                                <option value="107">BAHRAIN</option>
                                <option value="207">BANGLADESH</option>
                                <option value="657">BARBADOS</option>
                                <option value="411">BELGIUM</option>
                                <option value="651">BELIZE</option>
                                <option value="369">BENIN</option>
                                <option value="634">BERMUDA</option>
                                <option value="211">BHUTAN</option>
                                <option value="609">BOLIVIA</option>
                                <option value="463">BOSNIA AND HERZEG</option>
                                <option value="375">BOTSWANA</option>
                                <option value="603">BRAZIL</option>
                                <option value="401">BRITAIN</option>
                                <option value="253">BRUNEI</option>
                                <option value="465">BULGARIA</option>
                                <option value="351">BURKINA FASO</option>
                                <option value="307">BURUNDI</option>
                                <option value="393">CABO VERDE</option>
                                <option value="215">CAMBODIA</option>
                                <option value="309">CAMEROON</option>
                                <option value="501">CANADA</option>
                                <option value="363">CENTRAL AFRICA REP</option>
                                <option value="311">CHAD</option>
                                <option value="607">CHILE</option>
                                <option value="219">CHINA</option>
                                <option value="611">COLOMBIA</option>
                                <option value="642">COMONWEALTH DOMINICA</option>
                                <option value="301">COMOROS</option>
                                <option value="313">CONGO Republic</option>
                                <option value="623">COSTARICA</option>
                                <option value="453">CROATIA</option>
                                <option value="621">CUBA</option>
                                <option value="431">CYPRUS</option>
                                <option value="452">CZECH</option>
                                <option value="274">DAGHYSTAN</option>
                                <option value="315">DAHOOMI</option>
                                <option value="361">DEM REP OF CONGO</option>
                                <option value="423">DENMARK</option>
                                <option value="141">DJIBOUTI</option>
                                <option value="625">DOMINICAN</option>
                                <option value="613">ECUADOR</option>
                                <option value="125">EGYPT</option>
                                <option value="635">EL SALVADOR</option>
                                <option value="251">ENTIAGO</option>
                                <option value="303">ERITREN</option>
                                <option value="459">ESTONIA</option>
                                <option value="317">ETHIOPIA</option>
                                <option value="705">FIJI</option>
                                <option value="433">FINLAND</option>
                                <option value="403">FRANCE</option>
                                <option value="659">FRENCH GUIANA</option>
                                <option value="371">GABON</option>
                                <option value="387">GAMBIA</option>
                                <option value="273">GEORGIA</option>
                                <option value="407">GERMANY</option>
                                <option value="381">GHAMBIA</option>
                                <option value="319">GHANA</option>
                                <option value="385">GHINIA BISSAU</option>
                                <option value="429">GREECE</option>
                                <option value="365">GREENLAND</option>
                                <option value="649">GRENADA</option>
                                <option value="627">GUATAMALA</option>
                                <option value="653">GUYANA</option>
                                <option value="639">HAITI</option>
                                <option value="409">HOLLAND</option>
                                <option value="647">HONDURAS</option>
                                <option value="223">HONG KONG</option>
                                <option value="467">HUNGARY</option>
                                <option value="443">ICELAND</option>
                                <option value="205" selected="selected">INDIA</option>
                                <option value="243">INDONESIA</option>
                                <option value="201">IRAN</option>
                                <option value="113">IRAQ</option>
                                <option value="427">IRELAND</option>
                                <option value="405">ITALY</option>
                                <option value="323">IVORY COAST</option>
                                <option value="629">JAMAICA</option>
                                <option value="231">JAPAN</option>
                                <option value="121">JORDAN</option>
                                <option value="503">KAIMAN ISLAN</option>
                                <option value="715">KALDUNIA NEW</option>
                                <option value="261">KAZAKHESTAN</option>
                                <option value="325">KENYA</option>
                                <option value="667">KINGSTONE</option>
                                <option value="391">KIRIBATI</option>
                                <option value="476">KOSOVA</option>
                                <option value="105">KUWAIT</option>
                                <option value="489">KYRGYZ REPUBLIC</option>
                                <option value="117">LABANON</option>
                                <option value="245">LAOS</option>
                                <option value="461">LATVIA</option>
                                <option value="377">LESOTHO</option>
                                <option value="327">LIBERIA</option>
                                <option value="127">LIBYA</option>
                                <option value="449">LIECHTENSTEIN</option>
                                <option value="457">LITHUANIA</option>
                                <option value="413">LUXEMBOURG</option>
                                <option value="259">MACAU</option>
                                <option value="329">MADAGASCAR</option>
                                <option value="333">MALAWI</option>
                                <option value="241">MALAYSIA</option>
                                <option value="257">MALDIVES</option>
                                <option value="335">MALI</option>
                                <option value="435">MALTA</option>
                                <option value="727">MARSHALL ISLAND</option>
                                <option value="661">MARTINIQUE</option>
                                <option value="721">MARYANA ISLAND</option>
                                <option value="135">MAURITANIA</option>
                                <option value="367">MAURITIUS</option>
                                <option value="601">MEXICO</option>
                                <option value="732">MICRONESIA</option>
                                <option value="481">MOLDOVA</option>
                                <option value="439">MONACO</option>
                                <option value="249">MONGOLIA</option>
                                <option value="488">MONTENEGRO</option>
                                <option value="133">MOROCCO</option>
                                <option value="337">MOZAMBIQUE</option>
                                <option value="373">NAMEBIA</option>
                                <option value="737">NAURU</option>
                                <option value="235">NEPAL</option>
                                <option value="707">NEW GHINIA</option>
                                <option value="703">NEW ZEALAND</option>
                                <option value="631">NICARAGUA</option>
                                <option value="339">NIGER</option>
                                <option value="341">NIGERIA</option>
                                <option value="229">NORTH KOREA</option>
                                <option value="421">NORWAY</option>
                                <option value="723">OKINAWA</option>
                                <option value="203">PAKISTAN</option>
                                <option value="669">PALAU</option>
                                <option value="123">PALESTINE</option>
                                <option value="633">PANAMA</option>
                                <option value="731">PAPUA NEW GUINE</option>
                                <option value="645">PARAGUAY</option>
                                <option value="615">PERU</option>
                                <option value="237">PHILIPPINES</option>
                                <option value="471">POLAND</option>
                                <option value="425">PORTUGAL</option>
                                <option value="641">PUERTO RICO</option>
                                <option value="109">QATAR</option>
                                <option value="485">REPUBL. OF MACEDONIA</option>
                                <option value="490">REPUBLIC OF BELARUS</option>
                                <option value="213">REPUBLIC OF MYANMAR</option>
                                <option value="321">REPUPLIC OF GUINEA</option>
                                <option value="469">ROMANIA</option>
                                <option value="343">ROWANDA</option>
                                <option value="477">RUSSIA</option>
                                <option value="491">SAINT LUCIA</option>
                                <option value="665">SAINT VINSENT</option>
                                <option value="447">SAN MARINO</option>
                                <option value="395">SAO TOME</option>
                                <option value="103">SAUDI ARABIA</option>
                                <option value="345">SENEGAL</option>
                                <option value="383">SICHEL</option>
                                <option value="347">SIERRA LEONE</option>
                                <option value="225">SINGAPORE</option>
                                <option value="454">SLOVAKIA</option>
                                <option value="455">SLOVENIA</option>
                                <option value="725">SOLOMON ISLAND</option>
                                <option value="139">SOMALIA</option>
                                <option value="349">SOUTH AFRICA</option>
                                <option value="227">SOUTH KOREA</option>
                                <option value="138">SOUTH SUDAN</option>
                                <option value="437">SPAIN</option>
                                <option value="217">SRI LANKA</option>
                                <option value="606">ST HELENA</option>
                                <option value="487">ST KITTS-NAVIS</option>
                                <option value="137">SUDAN</option>
                                <option value="111">SULTANATE OF OMAN</option>
                                <option value="462">SURBIA</option>
                                <option value="655">SURINAME</option>
                                <option value="379">SWAZILAND</option>
                                <option value="419">SWEDEN</option>
                                <option value="415">SWIZERLAND</option>
                                <option value="119">SYRIA</option>
                                <option value="713">TAHITI</option>
                                <option value="221">TAIWAN</option>
                                <option value="267">TAJIKSTAN</option>
                                <option value="353">TANZANIA</option>
                                <option value="711">TASMANIA</option>
                                <option value="239">THAILAND</option>
                                <option value="483">THE HELLENIC REPBL</option>
                                <option value="709">TIMOR LESTE</option>
                                <option value="255">TONGA</option>
                                <option value="637">TRINIDAD</option>
                                <option value="129">TUNISIA</option>
                                <option value="475">TURKEY</option>
                                <option value="269">TURKMENISTAN</option>
                                <option value="735">TUVALU</option>
                                <option value="502">U S A</option>
                                <option value="357">UGANDA</option>
                                <option value="479">UKRAINE</option>
                                <option value="643">URGWAY</option>
                                <option value="271">UZBAKISTAN</option>
                                <option value="505">United Nations</option>
                                <option value="733">VANVATU</option>
                                <option value="441">VATICAN</option>
                                <option value="617">VENEZUELA</option>
                                <option value="233">VIETNAM</option>
                                <option value="729">W SAMOA</option>
                                <option value="115">YEMEN</option>
                                <option value="464">YUGOSLAVIA</option>
                                <option value="359">ZAMBIA</option>
                                <option value="331">ZIMBABWE</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label> Contact No:</label><br />
                        <div class="input full-width">
                            <input id="txtTelephone" name="prompt-value" value="" class="input-unstyled full-width"  placeholder="0" type="text">
                        </div>
                    </div>

                </div>
                 <h3>Flight Details </h3>
                <hr />
                <div class="columns">
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Arrival AIRLINE<span class="red">*</span> :</label>
                        <br>
                        <div class="full-width button-height" id="DivselAirLine">
                            <select id="selAirLine" name="validation-select" class="select" tabindex="-1" ">
                                <option value="-">-Select AirLine-</option>
                                <option value="1">ABU DHABI</option>
                                <option value="2">AEROFLOT </option>
                                <option value="3">AEROSVIT</option>
                                <option value="4">AIR ARABIA</option>
                                <option value="5">AIR ASTANA</option>
                                <option value="6">AIR BERLIN</option>
                                <option value="7">AIR INIDA</option>
                                <option value="8">AIR INDIA EXPERSS</option>
                                <option value="9">AIR MAURITIUS</option>
                                <option value="10">AIR SEYCHELLES</option>
                                <option value="11">ARABIAN AIRLINES</option>
                                <option value="12">AUSTRIAN AIRLINES</option>
                                <option value="13">AZERBAI JAN AIR</option>
                                <option value="14">BAHRAIN AIR</option>
                                <option value="15">BIMAN BANGLADESH</option>
                                <option value="16">JAP BRITISH AIRWAYSANESE</option>
                                <option value="17">CATHAY PACIFIC</option>
                                <option value="18">CHINA SOUTHERN</option>
                                <option value="19">CATHY PACIFIC</option>
                                <option value="20">CHINA SOUTHERN</option>
                                <option value="21">CRIMEA AIR</option>
                                <option value="22">DELTA AIR</option>
                                <option value="23">CUTCH AIRLINES</option>
                                <option value="98">EGYPT AIRWAYS</option>
                                <option value="99">EMIRATES</option>
                                <option value="10">ETIHAD</option>
                                <option value="11">FINNAIR</option>
                                <option value="12">FLY DUBAI</option>
                                <option value="13">GULF AIR</option>
                                <option value="14">HAHN AIR</option>
                                <option value="15">INDIGO</option>
                                <option value="16">IRAN AIR</option>
                                <option value="17">IRAQI AIRWAYS</option>
                                <option value="18">JET AIR</option>
                                <option value="19">KENYA AIR</option>
                                <option value="20">KING FISHER</option>
                                <option value="21">KISH AIR</option>
                                <option value="22">KLM ROYAL DUTCH</option>
                                <option value="23">KOREAN AIR</option>
                                <option value="98">KUWAIT AIR</option>
                                <option value="99">LUFTHANSA</option>
                                <option value="10">MAHAN AIR</option>
                                <option value="11">MIHIN LANKA</option>
                                <option value="12">NORDSTAR</option>
                                <option value="13">OMAN AIR ORENBURG AIRLINES</option>
                                <option value="14">PEGASUS AIRLINES</option>
                                <option value="15">QATAR AIRWAYS</option>
                                <option value="16">ROSSIYA</option>
                                <option value="17">ROYAL BRUNEI</option>
                                <option value="18">ROYAL JORDANIAN</option>
                                <option value="19">RWANDAIR EXPRESS</option>
                                <option value="20">S7 (SIBERIA) AIRLINES</option>
                                <option value="21">SAUDI AIRLAINES</option>
                                <option value="22">SINGAPORE  AIRLINES</option>
                                <option value="23">SOMON AIR</option>
                                <option value="98">SOUTH AFRICAN AIRWAY</option>
                                <option value="99">SPICE JET</option>
                                <option value="20">SRILANKAN AIRLINES</option>
                                <option value="21">SWISS AIR</option>
                                <option value="22">THAI AIR</option>
                                <option value="23">TRANSAERO</option>
                                <option value="98">TURKISH AIRLINES</option>
                                <option value="99">UKRAINE AIRLINES</option>
                                <option value="20">UNITED AIRLINES</option>
                                <option value="21">URAL AIRLINES (U6)</option>
                                <option value="22">UZBEKISTAN AIRWAYS</option>
                                <option value="23">VIRGIN ATLANTIC</option>
                                <option value="98">YEMEN AIRWAYS</option>
                                <option value="99">JETLITE</option>
                                <option value="22">ATLANTA</option>
                                <option value="23">CRUISE</option>
                                <option value="98">RUSLINE</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Arrival flight No<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtflightNo"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Arrival flight No" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Arrival from<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtArrivalfrom"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Arrival from" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Arrival Date<span class="red">*</span> :</label>
                        <span class="icon-calendar"></span>
                        <span class="input full-width">
                            <input id="dteArrival" placeholder="dd-mm-yyyy" name="datepicker" class="input-unstyled" type="text">
                        </span>
                    </div>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Deprture  AirLine<span class="red">*</span> :</label>
                        <br>
                        <div class="full-width button-height" id="DivDeprtureAirLine">
                            <select id="selDeprtureAirLine" name="validation-select" class="select" tabindex="-1" ">
                                <option value="-">-Select AirLine-</option>
                                <option value="1">ABU DHABI</option>
                                <option value="2">AEROFLOT </option>
                                <option value="3">AEROSVIT</option>
                                <option value="4">AIR ARABIA</option>
                                <option value="5">AIR ASTANA</option>
                                <option value="6">AIR BERLIN</option>
                                <option value="7">AIR INIDA</option>
                                <option value="8">AIR INDIA EXPERSS</option>
                                <option value="9">AIR MAURITIUS</option>
                                <option value="10">AIR SEYCHELLES</option>
                                <option value="11">ARABIAN AIRLINES</option>
                                <option value="12">AUSTRIAN AIRLINES</option>
                                <option value="13">AZERBAI JAN AIR</option>
                                <option value="14">BAHRAIN AIR</option>
                                <option value="15">BIMAN BANGLADESH</option>
                                <option value="16">JAP BRITISH AIRWAYSANESE</option>
                                <option value="17">CATHAY PACIFIC</option>
                                <option value="18">CHINA SOUTHERN</option>
                                <option value="19">CATHY PACIFIC</option>
                                <option value="20">CHINA SOUTHERN</option>
                                <option value="21">CRIMEA AIR</option>
                                <option value="22">DELTA AIR</option>
                                <option value="23">CUTCH AIRLINES</option>
                                <option value="98">EGYPT AIRWAYS</option>
                                <option value="99">EMIRATES</option>
                                <option value="10">ETIHAD</option>
                                <option value="11">FINNAIR</option>
                                <option value="12">FLY DUBAI</option>
                                <option value="13">GULF AIR</option>
                                <option value="14">HAHN AIR</option>
                                <option value="15">INDIGO</option>
                                <option value="16">IRAN AIR</option>
                                <option value="17">IRAQI AIRWAYS</option>
                                <option value="18">JET AIR</option>
                                <option value="19">KENYA AIR</option>
                                <option value="20">KING FISHER</option>
                                <option value="21">KISH AIR</option>
                                <option value="22">KLM ROYAL DUTCH</option>
                                <option value="23">KOREAN AIR</option>
                                <option value="98">KUWAIT AIR</option>
                                <option value="99">LUFTHANSA</option>
                                <option value="10">MAHAN AIR</option>
                                <option value="11">MIHIN LANKA</option>
                                <option value="12">NORDSTAR</option>
                                <option value="13">OMAN AIR ORENBURG AIRLINES</option>
                                <option value="14">PEGASUS AIRLINES</option>
                                <option value="15">QATAR AIRWAYS</option>
                                <option value="16">ROSSIYA</option>
                                <option value="17">ROYAL BRUNEI</option>
                                <option value="18">ROYAL JORDANIAN</option>
                                <option value="19">RWANDAIR EXPRESS</option>
                                <option value="20">S7 (SIBERIA) AIRLINES</option>
                                <option value="21">SAUDI AIRLAINES</option>
                                <option value="22">SINGAPORE  AIRLINES</option>
                                <option value="23">SOMON AIR</option>
                                <option value="98">SOUTH AFRICAN AIRWAY</option>
                                <option value="99">SPICE JET</option>
                                <option value="20">SRILANKAN AIRLINES</option>
                                <option value="21">SWISS AIR</option>
                                <option value="22">THAI AIR</option>
                                <option value="23">TRANSAERO</option>
                                <option value="98">TURKISH AIRLINES</option>
                                <option value="99">UKRAINE AIRLINES</option>
                                <option value="20">UNITED AIRLINES</option>
                                <option value="21">URAL AIRLINES (U6)</option>
                                <option value="22">UZBEKISTAN AIRWAYS</option>
                                <option value="23">VIRGIN ATLANTIC</option>
                                <option value="98">YEMEN AIRWAYS</option>
                                <option value="99">JETLITE</option>
                                <option value="22">ATLANTA</option>
                                <option value="23">CRUISE</option>
                                <option value="98">RUSLINE</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Deprture flight No<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtDeprtureflightNo"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Deprture flight No" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Deprture from<span class="red">*</span> :</label>
                        <div class="input full-width">
                            <input id="txtDeprturefrom"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Deprture from" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                         <label>Departing Date<span class="red">*</span> :</label>
                         <span class="icon-calendar"></span>
                        <span class="input full-width">
                            <input id="dteDeparting" placeholder="dd-mm-yyyy" name="datepicker" class="input-unstyled" type="text">
                        </span>
                    </div>
                </div>
               <p class="text-right" style="text-align: right;" >
                    <button type="button" class="button anthracite-gradient UpdateMarkup" id="btn_RegiterAgent" onclick="GetVisaCode()" title="Submit Details">Save & Continue</button>
                </p>
                <%--<p class="text-right" style="text-align: right;" id="Update">
                    <button type="button" id="btn_RegiterAgent" class="button anthracite-gradient UpdateMarkup"  onclick="GetVisaCode()" title="Submit Details">Save & Continue</button>
                </p>--%>
            </form>



        </div>
    </section>
    <!-- End main content -->
</asp:Content>
