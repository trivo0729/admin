﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agent/AgentMaster.Master" AutoEventWireup="true" CodeBehind="AgentDashboard.aspx.cs" Inherits="CutAdmin.Agent.AgentDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <!-- Additional styles -->

    <script src="Scripts/Dashboard.js?v=1.0"></script>
   
    <script src="../Scripts/BookingList.js?v=1.1"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
            <h1>Dashboard</h1>
            <hr />
        </hgroup>


        <div class="with-padding">

            <div class="columns">
                <div class="new-row-mobile three-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="GroupRequest">
                    </div>
                </div>
                <div class="new-row-mobile three-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="BookingsReconfirm">
                    </div>
                </div>
                <div class="new-row-mobile three-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="BookingsRequests">
                    </div>
                </div>
                <div class="new-row-mobile three-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="BookingOnHold">
                    </div>
                </div>

            </div>

            <div class="columns datepicker" id="">
            </div>
        </div>
    </section>
</asp:Content>
