﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agent/AgentMaster.Master" AutoEventWireup="true" CodeBehind="Agentdeposite.aspx.cs" Inherits="CutAdmin.Agent.Agentdeposite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script>
        $(function () {
            //debugger
            $("#dteBankDeposite").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });

            $("#dteCashDeposit").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
        })
    </script>
    <script type="text/javascript">
        var CurrencyClass = '<%=Session["CurrencyClass"]%>';
    </script>
   
    <script src="Scripts/BankDeposit.js"></script>
    <script src="Scripts/AgentMaster.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">
       
        <hgroup id="main-title" class="thin">
            <h1>Payment Request</h1>
              <hr />
        </hgroup>
        <div class="with-padding">
            <span class="lato size14 grey" style="float: right; padding-bottom: 10px;">
                <%--Name:<span class="orange" id="spName"></span>,
            Supplier Code:<span class="orange" id="spUniqueCode"></span>, --%>
            Available limit:<span class="orange" id="spAvailableLimit"></span>,
             Credit Limit:[<span class="orange" id="spCreditLimit"></span>/<span class="orange" id="spCreditLimit_Cmp">0</span>]
            One Time Limit:[<span class="orange" id="spOneTimeCreditLimit"></span>/<span class="orange" id="spOneTimeCreditLimit_Cmp"></span>]
            </span>
            <div class="clear"></div>
            <div class="standard-tabs margin-bottom" id="add-tabs">
                <ul class="tabs" id="tabs">
                    <li class="active"><a href="#Bank">Bank</a></li>
                    <li><a href="#Cash">Cash</a></li>

                </ul>
                <div class="tabs-content">
                    <div id="Bank" class="with-padding">
                        <div class="columns">
                            <div class="six-columns  twelve-columns-mobile six-columns-tablet">
                                <label>Type of Transaction</label>
                                <div class="full-width button-height">
                                    <select id="Select_TypeOfCash" name="validation-select" class="select">
                                        <option selected="selected" value="-">Select Transaction</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="NEFT">NEFT</option>
                                        <option value="IMPS">IMPS</option>
                                        <option value="RTGS">RTGS</option>
                                        <option value="NetBanking">Net Banking</option>
                                        <option value="Draft">Draft</option>
                                        <option value="TT">TT</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_TypeOfCash">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="six-columns  twelve-columns-mobile six-columns-tablet">
                                <label>Bank Account</label>
                                <div class="full-width button-height">
                                    <select id="Select_BankAccount" name="validation-select" class="select">
                                        <%-- <option selected="selected" value="-">Select Bank</option>
                                        <option value="ICICI Bank -  023105002994">ICICI Bank -  023105002994</option>
                                        <option value="Axis Bank - 914020021370944">Axis Bank - 914020021370944</option>
                                        <option value="Bank of India - 870120110000456">Bank of India - 870120110000456</option>
                                        <option value="ICICI Bank - 023105500640">ICICI Bank - 023105500640</option>
                                        <option value="EMIRATES ISLAMIC BANK - 370-73941042-01">EMIRATES ISLAMIC BANK - 370-73941042-01</option>--%>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_BankAccount">
                                    <b>* This field is required</b></label>
                            </div>

                            <div class="four-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_AmountGiven" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_AmountGiven">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <label>Deposit Date</label><div class="input full-width">
                                    <%--<input name="prompt-value" id="dteBankDeposite" value="" class="input-unstyled full-width" placeholder="dd/mm/yy" type="text">--%>
                                    <input type="text" name="prompt-value" placeholder="dd/mm/yy" id="dteBankDeposite" class="input-unstyled" value="">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_dteBankDeposite">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <label>Receipt Number/Transaction ID</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_BankTransactionNumber" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_BankTransactionNumber">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_BankRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="saveBack">
                            <input id="btnBankDeposite" type="button" value="Submit" style="float:right" class="button anthracite-gradient" onclick="submit()">
                            <br />
                            <br />
                           <%-- <button id="btnBankDeposite" type="button" onclick="Submit()" class="button anthracite-gradient">Submit</button>--%>
                            <%--<button type="button" onclick="window.location = 'Dashboard.aspx'" class="button glossy">
                                Back
                            </button>--%>
                        </div>
                    </div>

                    <div id="Cash" class="with-padding">
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_Ammount" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_Ammount">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Deposit Date</label><div class="input full-width">
                                    <input name="prompt-value" id="dteCashDeposit" value="" class="input-unstyled" placeholder="dd/mm/yy" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_dteCashDeposit">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Given To</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_Given_To" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_Given_To">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Receipt Number</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_ReceiptNumberCheque" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_ReceiptNumberCheque">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_CashRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="saveBack">
                            <input id="btnSubmitCash" type="button" value="Submit" class="button anthracite-gradient" onclick="submit()">
                            <%--<button id="btnSubmitCash" type="button" onclick="SaveCash()" class="button anthracite-gradient">Submit</button>--%>
                        </div>
                    </div>

                </div>
            </div>
            <div class="respTable">
                <table class="table responsive-table" id="tblCreditDetails">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center hide-on-mobile-portrait">S.N</th>
                            <th scope="col" class="align-center">Amount </th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Transaction Type </th>
                            <th scope="col" class="align-center">Bank </th>
                            <th scope="col" class="align-center">Receipt Number/Transaction ID </th>
                            <th scope="col" class="align-center">Status </th>
                             <th scope="col" class="align-center">Approved Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

</asp:Content>
