﻿

var Nex = 0;
var Pre = 10;
var Limit = 0;
var SearchBit = false;
function GetAgencyStatementAll() {
    debugger;
    SearchBit = false;
    $("#tbl_AgencyStatement").dataTable().fnClearTable();
    $("#tbl_AgencyStatement").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/TransactionHandler.asmx/GetTransactions",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgency = result.AgentStatement;
                var sAgencyName = result.AgencyName;
                var PageCount = result.Count;
                var tRow;
                if (arrAgency && arrAgency != 0) {


                    var tRow = '';
                    for (var i = 0; i < arrAgency.length; i++) {
                        var TD = arrAgency[i].TransactionDate;
                        var TxnDt = TD.split(" ");

                        var Debited;
                        var Credit;
                        var Ref;
                        if ((arrAgency[i].DebitedAmount) == "") {
                            Debited = "-";
                        }
                        else {
                            Debited = arrAgency[i].DebitedAmount;
                        }

                        if ((arrAgency[i].CreditedAmount) == "") {
                            Credit = "-";
                        }
                        else {
                            Credit = arrAgency[i].CreditedAmount;
                        }

                        if ((arrAgency[i].ReferenceNo) == "" || (arrAgency[i].ReferenceNo) == null) {
                            Ref = "-";
                        }
                        else {
                            Ref = arrAgency[i].ReferenceNo;
                        }


                        tRow += '<tr>';
                        tRow += '<td align=center style="width:5%;">' + (i + 1) + '</td>';
                        tRow += '<td align=center>' + TxnDt[0] + '</td>';
                        tRow += '<td align=center>' + Ref + '</td>';
                        tRow += '<td align=center>' + arrAgency[i].Particulars + '</td>';
                        if (Credit != '-') {
                            tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(Credit) + '</span></td>';
                        }
                        else {
                            tRow += '<td align=center>-</td>';
                        }
                        if (Debited != '-') {
                            tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(Debited) + '</span></td>';
                        }
                        else {
                            tRow += '<td align=center>-</td>';
                        }
                        tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(arrAgency[i].Balance) + '</span></td>';
                        tRow += '</tr>';
                    }

                    $("#tbl_AgencyStatement").append(tRow);
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                    $("#tbl_AgencyStatement").removeAttr("style");
                }
                else {
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
            }
            if (result.retCode == 0) {
               
                Success('Something Went Wrong');
            }

            if (result.retCode == 2) {
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });

            }


        },
        error: function () {
        }
    });
}

function GetAgencyStatementByDate() {
    debugger;
    SearchBit = true;
    var TransType;
    var dFrom;
    var dTo;
    var tRow;
    var RefNo;

    dFrom = $('#datepicker3').val();
    dTo = $('#datepicker4').val();
    TransType = $('#TransactionType').val();
    RefNo = $('#txt_Reference').val();

    $("#tbl_AgencyStatement").dataTable().fnClearTable();
    $("#tbl_AgencyStatement").dataTable().fnDestroy();

    $.ajax({
        type: "POST",
        url: "Handler/TransactionHandler.asmx/GetTransactionByDate",
        data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '","TransType":"' + TransType + '","RefNo":"' + RefNo + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgency = result.AgentStatement;
                var sAgencyName = result.AgencyName;
                var PageCount = result.Count;
                var tRow;

                if (arrAgency && arrAgency != 0) {

                    $("#tbl_AgencyStatement tbody").empty();
                    var tRow = '';
                    for (var i = 0; i < arrAgency.length; i++) {
                        var TD = arrAgency[i].TransactionDate;
                        var TxnDt = TD.split(" ");

                        var Debited;
                        var Credit;
                        var Ref;
                        if ((arrAgency[i].DebitedAmount) == "") {
                            Debited = "-";
                        }
                        else {
                            Debited = arrAgency[i].DebitedAmount;
                        }

                        if ((arrAgency[i].CreditedAmount) == "") {
                            Credit = "-";
                        }
                        else {
                            Credit = arrAgency[i].CreditedAmount;
                        }

                        if ((arrAgency[i].ReferenceNo) == "") {
                            Ref = "-";
                        }
                        else {
                            Ref = arrAgency[i].ReferenceNo;
                        }


                        tRow += '<tr>';
                        tRow += '<td align=center style="width:5%;">' + (i + 1) + '</td>';
                        tRow += '<td align=center>' + TxnDt[0] + '</td>';
                        tRow += '<td align=center>' + Ref + '</td>';
                        tRow += '<td align=center>' + arrAgency[i].Particulars + '</td>';
                        if (Credit != '-') {
                            tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(Credit) + '</span></td>';
                        }
                        else {
                            tRow += '<td align=center>-</td>';
                        }
                        if (Debited != '-') {
                            tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(Debited) + '</span></td>';
                        }
                        else {
                            tRow += '<td align=center>-</td>';
                        }
                        tRow += '<td align=center><span><i class="' + CurrencyClass + '"></i>&nbsp' + numberWithCommas(arrAgency[i].Balance) + '</span></td>';
                        tRow += '</tr>';
                        //SN++;
                    }

                    $("#tbl_AgencyStatement tbody").append(tRow);
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });


                }
                else {
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
            }
            if (result.retCode == 0) {
                Success('Something Went Wrong');
            }

            if (result.retCode == 2) {
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }


        },
        error: function () {
        }
    });
}

function ExportAgencyStatement(Document) {
    debugger;

    var Type = $('#TransactionType').val();
    var RefNo = $('#txt_Reference').val();
    var From = $('#datepicker3').val();
    var To = $('#datepicker4').val();

    if (Type == "Both" && From == "" && To == "" && RefNo == "") {
        SearchBit = false;
    }

    if (SearchBit == false) {
        window.location.href = "../HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=AgencyStatement&Type=All&Document=" + Document;
    }
    else {
        window.location.href = "../HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=AgencyStatement&Type=Search&Document=" + Document;
    }

    //   window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&sid=" + sid + "&dFrom=" + dFrom + "&dTo=" + dTo + "&TransType=" + TransType;

}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}