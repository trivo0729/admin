﻿$(document).ready(function () {
    BookingListAll()
    $("#Check-In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Check-Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

var BookingList = new Array();

function BookingListAll() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/BookingList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function htmlGenrator() {
    var trow = '';
    for (var i = 0; i < BookingList.length; i++) {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;
        trow += '<tr>';
        trow += '<td style="width:3%">' + (i + 1) + '</td>';
        trow += '<td style="width:12%">' + BookingList[i].ReservationDate + ' </td>';
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Confirm" onclick="Success(\'This booking is cancelled.\');">' + BookingList[i].ReservationID + '</a></td>';
        } else if (BookingList[i].Status == 'OnRequest') {
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Confirm" >' + BookingList[i].ReservationID + ' </a></td>';
        } else {
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Confirm" >' + BookingList[i].ReservationID + ' </a></td>';
        }
        trow += '<td>' + BookingList[i].AgencyName + ' </td>';
        trow += '<td style="width:10%">' + BookingList[i].bookingname + '</td>';
        trow += '<td style="width:20%">' + BookingList[i].HotelName + ', ' + BookingList[i].City + ' </td>';
        trow += '<td style="width:11%">' + BookingList[i].CheckIn + ' </td>';
        trow += '<td style="width:11%">' + BookingList[i].CheckOut + ' </td>';
        trow += '<td style="width:4%">' + BookingList[i].TotalRooms + ' </td>';
        trow += '<td style="width:6%">' + BookingList[i].Status + ' </td>';
        if (BookingList[i].Status != "GroupRequest")
            trow += '<td><span style="width:10%"><i class="' + GetCurrency(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        else
            trow += '<td><span style="width:10%">n/a </span></td>';
        if (BookingList[i].Status == "GroupRequest")
        {
            trow += '<td style="width:3%" class="align-center">n/a</td>';
            trow += '<td style="width:3%" class="align-center">n/a</td>';
        }
        else
        {
            trow += '<td style="width:3%" class="align-center"><a style="cursor:pointer" title="Invoice" class="button"  onclick="GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-cc-share tracked"></span></a></td>';
            trow += '<td style="width:3%" class="align-center"><a style="cursor:pointer" title="Voucher" class="button"  onclick="GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-page-list-inverted"></span></a></td>';
        }
        trow += '</tr>';
    }
    trow += '</tbody>'
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()
    $("#tbl_BookingList").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $("#tbl_BookingList").removeAttr("style");
}

function Search() {

    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();

    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();

    var data = {
        CheckIn: CheckIn,
        CheckOut: CheckOut,
        Passenger: Passenger,
        BookingDate: BookingDate,
        Reference: Reference,
        HotelName: HotelName,
        Location: Location,
        ReservationStatus: ReservationStatus
    }

    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_BookingList").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });

}

function Reset() {
    $("#Check-In").val('');
    $("#Check-Out").val('');
    $("#txt_Passenger").val('');
    $("#Bookingdate").val('');
    $("#txt_Reference").val('');
    $("#txt_Hotel").val('');
    $("#txt_Location").val('');
    $("#selReservation").val('All');
}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}

function ExportBookingDetailsToExcel(Document) {
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();
    var Type = "All";
    if (CheckIn == "" && CheckOut == "" && Passenger == "" && BookingDate == "" && Reference == "" && HotelName == "" && Location == "" && ReservationStatus == "All") {
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=SupplierBookingDetails&Type=" + Type + "&Document=" + Document;
    }
    else {
        Type = "Search";
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=SupplierBookingDetails&Type=" + Type + "&Document=" + Document;
    }
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {
    //OpenCancellationPopup(ReservationID, Status);
    $("#hdn_Supplier").val(Source);
    //$("#hdn_AffiliateCode").val(AffilateCode);
    OpenHoldPopup(ReservationID, Status);

    //$("#hdn_HoldDate").val(HoldDate);
    //$("#hdn_DeadLineDate").val(DeadLineDate);
    //$('#ConfirmAlertForOnHoldModel').modal('show');
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].Name + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" style="width: 100%">' +
                   '<tr>' +
                  '<h4>Re-Confirmation Detail</h4>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Staff Name :&nbsp;&nbsp; <input type="text" id="StaffName" class="input" > </span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +

                  '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                  '<tr>' +
                    '<td style="border-bottom: none;" >' +
                  '<span class="text-left">Comment :  <input type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                   '' +
                  '</td>' +
                  '</tr>' +
                   '</table>' +

                   '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                  '</div>',
                    title: 'Re-confirm Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true

                });

                $("#ConfirmDate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    //onSelect: insertDepartureDate,
                    //minDate: "dateToday",
                    //maxDate: "+3M +10D"
                });
            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}

function SaveConfirmDetail(ReservationID, status, HotelName) {
    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").val();
    if (ReconfirmThrough == "-") {
        Success('Please Select Reconfirm Through.');
        return false;
    }

    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,
    }

    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/SaveConfirmDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Confirm Detail Save.");
                $("#ConfirmDate").val('');
                $("#HotelConfirmationNo").val('');
                $("#StaffName").val('');
                $("#ReconfirmThrough option:selected").val('-');
                $("#Comment").val('');
                setTimeout(function () {
                    window.location.href = "BookingList.aspx";
                }, 2000);
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });


}