﻿function sendPassword() {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    $("#tbl_ForgotPassword tr td label").html("* This field is required");
    $("#tbl_ForgotPassword tr td label").css("display", "none");

    var bValid = true;
    var sEmail = $("#txt_sendPassword").val();
    if (sEmail == "") {
        bValid = false;
        Success("Please enter Email Address.");
        //$("#lbl_sendPassword").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            Success("Please enter valid Email Address.");
            //$("#lbl_sendPassword").html("* Wrong email format.");
            //$("#lbl_sendPassword").css("display", "");
        }
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/DefaultHandler.asmx/sendPassword",
            data: '{"sEmail":"' + sEmail + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Some error occured, Please try again in some time.");
                    return false;
                }
                if (result.retCode == 1) {
                    Success("Password sent to your registered email, Email has been sent successfully.");
                    setTimeout(function () {
                        window.location.href = "Login.aspx";
                    }, 2000);
             
                    //  window.location.href = "Default.aspx";
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
                else if (result.retCode == 0) {
                    Success("Sorry, No account found with this email id.");
                    setTimeout(function () {
                        window.location.href = "Login.aspx";
                    }, 2000);
                
                    //window.location.href = "Default.aspx";
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
                else if (result.retCode == 2) {
                    Success("Sorry, No account found with this email id.");
                    setTimeout(function () {
                        window.location.href = "Login.aspx";
                    }, 2000);
              
                    // window.location.href = "Default.aspx";
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
            },
            error: function () {
            }

        });
    }
}