﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities = "", selectedFacilities = [];
var arrRoomList = [];
var UserType;
$(document).ready(function () {
    GetCountry();
    getDates();
});

function GetHotelList() {
    var City1 = $("#txt_Destination").val();
    var City = City1.split(',')[0];
    var Country = City1.split(',')[1].trim();
    var data = {
        Country: Country,
        City: City
    }
    $.ajax({
        type: "POST",
        url: "Handler/HotelHandler.asmx/GetMappedHotelstoUpdate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelList = result.HotelListbyCity;
                Div = '';
                for (var i = 0; i < arrHotelList.length; i++) {
                    Div += '<option value="' + arrHotelList[i].sid + '">' + arrHotelList[i].HotelName + '</option>'
                }
                $("#listHotels").append(Div);
            }
            else {
                AlertDanger(result.ex)
            }
        },

    });
}

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}

function getDates() {
    $("#txt_Checkin").datepicker($.extend({
        minDate: 0,
        onSelect: function () {
            var minDate = $(this).datepicker('getDate');
            minDate.setDate(minDate.getDate() + 1); //add One days
            $("#txt_Checkout").datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));

    $("#txt_Checkout").datepicker($.extend({
        onSelect: function () {
            var maxDate = $(this).datepicker('getDate');
            maxDate.setDate(maxDate.getDate());
            ChangeNights();
        }
    }, datepickersOpt));


}

function ChangeNights() {
    var Checkin = moment($("#txt_Checkin").val(), "DD-MM-YYYY");
    var Checkout = moment($("#txt_Checkout").val(), "DD-MM-YYYY");
    var CountDays = Checkout.diff(Checkin, 'days');
    $('#sel_Nights').val(CountDays);
    $("#drp_Nights .select span")[0].textContent = CountDays;
    // document.getElementById('sel_Nights').value = CountDays;
}
function drpChangeNights() {
    days = $('#sel_Nights').val();
    days = parseInt(days);
    var firstDate = new Date($('#txt_Checkin').datepicker("getDate"));
    var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
    $('#txt_Checkout').datepicker('setDate', secondDate);
    insertDepartureDate('', '', '');

}
function insertDepartureDate(value, date, inst) {
    var firstDate = new Date($('#txt_Checkin').datepicker("getDate"));
    var dateAdjust = $('#sel_Nights').val();
    var current_date = new Date();

    current_time = current_date.getTime();
    days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
    if (days < 0) {
        var add_day = 1;
    } else {
        var add_day = 2;
    }
    days = parseInt(days);

    $('#txt_Checkout').datepicker("option", "minDate", days + add_day);
    $('#txt_Checkout').datepicker("option", "maxDate", days + 365);
    dateAdjust = parseInt(dateAdjust);
    var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
    $('#txt_Checkout').datepicker('setDate', secondDate);
}

var arrCountry = new Array();
function GetCountry() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    if (UserType == undefined || UserType != 'Agent')
                        ddlRequest += ' <option class="optCountry" value="AllCountry">For All Nationality</option>';
                       
                    for (i = 0; i < arrCountry.length; i++) {

                        if (arrCountry[i].Country != "AllCountry")
                            ddlRequest += '<option class="optCountry" value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    ddlRequest += ' <option value="Unselect" style="display:inline-block;" disabled>Unselect All</option>';
                    $("#sel_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

var DatesHotel = [];
var RoomsHotel = [];
var arrRoomDetails = [];
var arrCancelations = [];
var arrSuppliers = [];
var arrCurrencyList = [];
var arrMealPlans = [];
var arrRoomTypes = [];
var arrRoomRates = [];
var nationalityCode = [];
var arrRateType = [];
var arrSeasonOfferListt = [];
var arrFilter = [];
var flags = [], Rooms = [], Datewise = [];
var validNationality = new Array();
var nationality; var HotelCode=[]; var HotelName; var Destination;
var session = "";
var SearchValid = "";
function SearchRates() {
    debugger;
    var Message = "";
    SearchValid = "Advance";
    Destination = $('#hdnDCode').val();
    var Checkin = $('#txt_Checkin').val();
    var Checkout = $('#txt_Checkout').val();
    HotelName = [];
    HotelCode = $('#listHotels').val();
    if (HotelCode != null)
    {
        for (var i = 0; i < HotelCode.length; i++) {
            var arrCodes = $.grep(arrHotelList, function (p) { return p.sid == HotelCode[i]; })
                          .map(function (p) { return p.HotelName; });
            HotelName.push(arrCodes);
        }
    }
    nationalityCode = $('#sel_Nationality').val();
    validNationality = new Array();
    if (Destination == "") {
        Success("Enter the city name where you want to go!");
        return false;
    }
    if (Checkin == "") {
        Success('Please Enter Checkin Date');
        return false;
    }
    if (Checkout == "") {
        Success('Please Enter Checkout Date');
        return false;
    }

    if (nationalityCode == "" || nationalityCode == null) {
        Success("Please select Nationality");
        return false;
    }
    nationalityCode = [];
    nationalityCode.push($('#sel_Nationality').val())
    var nationalityName = $('#sel_Nationality option:selected').text();
    var DName = $('#txt_Destination').val();
    var Nights = $('#sel_Nights').val();
    var Adults = $('#Select_Adults1d').val();
    var Childs = $('#Select_Children1').val();
    var room = $('#Select_Rooms').val();

    var ddlRequest = '';

    var Supplier = "";
    if ($('#sel_Supplier').val() != null) {
        Supplier = $('#sel_Supplier').val();
    }
    var MealPlan = "";
    if ($('#sel_MealPlan').val() != null) {
        MealPlan = $('#sel_MealPlan').val();
    }
    var CurrencyCode = "";
    if ($('#sel_CurrencyCode').val() != null) {
        CurrencyCode = $('#sel_CurrencyCode').val();
    }


    if (HotelCode == null) {
        HotelCode = [];
    }
    if (nationalityCode == null) {
        nationalityCode = "";
    }

    if (Nights == null) {
        Nights = 0;
    }
    if (Adults == null) {
        Adults = 0;
    }
    if (Childs == null) {
        Childs = 0;
    }

    else {
        var roomcount = parseInt(room);
        var occupancy = '';
        for (var i = 0; i < roomcount; i++) {
            if (i == 0) {
                occupancy = Room1();
            }
            else if (i == 1) {
                occupancy = occupancy + '$' + Room2();
            }
            else if (i == 2) {
                occupancy = occupancy + '$' + Room3();
            }
            else if (i == 3) {
                occupancy = occupancy + '$' + Room4();
            }
            else if (i == 4) {
                occupancy = occupancy + '$' + Room5();
            }
            else if (i == 5) {
                occupancy = occupancy + '$' + Room6();
            }
            else if (i == 6) {
                occupancy = occupancy + '$' + Room7();
            }
            else if (i == 7) {
                occupancy = occupancy + '$' + Room8();
            }
            else if (i == 8) {
                occupancy = occupancy + '$' + Room9();
            }
        }
        session = Destination + '_' + DName + '_' + Checkin + '_' + Checkout + '_' + room + '_' + occupancy + '_' + HotelCode + '_' + HotelName + '_' + nationalityCode + '_' + nationalityName;
        var trRequest = "";
        var data =
         {
             HotelCodes: HotelCode,
             CheckIn: Checkin,
             CheckOut: Checkout,
             Nationality: nationalityCode,
             SearchValid: SearchValid,
             Search_Params: session,
         }
        $.ajax({
            type: "POST",
            url: "Handler/HotelHandler.asmx/SearchHotel",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    window.location.href = "b2bhotels.aspx?data=" + session;
                }
                else if (result.retCode == 0) {
                    AlertDanger(result.ex)
                }

            }
        });
    }

}



function CheckHotel(currentval) {
    if ($("#listHotels option[value='0']").is(':selected')) {
        selectAll();
    }
    else if ($("#listHotels option[value='Unselect']").is(':selected')) {
        UnselectAll();
    }

}

function selectAll() {
    $('#listHotels option').prop('selected', true);
    $("#listHotels option[value='All']").click();
}

function UnselectAll() {
    $('#listHotels option').prop('selected', false);
    $("#listHotels option[value='All']").click();
}
