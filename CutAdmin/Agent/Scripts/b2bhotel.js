﻿$(function () {
    GetHotels();
    GetSearchParams();
});
var arrHotels = new Array();
var arrFilter = new Array();
var session
function GetHotels() {
    session = GetQueryStringParams("data").replace(/%20/g, ' ');
    $.ajax({
        type: "POST",
        url: "Handler/HotelHandler.asmx/GetSearch",
        data: JSON.stringify({ Search_Params: session }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotels = result.arrHotel.arrHotels;
                arrFilter = result.arrHotel.arrFilter;
                GenrateDetails();
                GenrateFilter();
            }
            else if (result.retCode == 0) {
                AlertDanger(result.ex)
            }

        }
    });
}


function GenrateDetails() {
  
    try {
        for (var i = 0; i < arrHotels.length; i++) {
            var html = "";
            html += '<div class="block margin-bottom">'
            html += '<div class="columns  with-mid-padding" style="opacity: 1;"> '
            html += '<div class="three-columns twelve-columns-mobile">'
            if (arrHotels[i].Image.length != 0)
                html += '<span class="stack rotated" onclick="GetImage(\''+ arrHotels[i].HotelId +'\')"><img src="' + arrHotels[i].Image[0].Url + '" style="width: 200px;height: 120px;cursor:poniter"></span>'
            else
                html += '<span class="stack rotated"><img src="../img/noImage.png" style="width: 200px;height: 130px;"></span>'
            html +='</div>'					 
            html += '<div class="nine-columns twelve-columns-mobile">'
            html += '<div class="twelve-columns twelve-columns-mobile">'
            html += '<h3 class="green underline">' + arrHotels[i].HotelName + ' <span class="fa fa-map-marker float-righ" style="width:30px;cursor:pointer;float: right;"></span>'
            html += '<a data-footer="Google Map" data-title="' + arrHotels[i].HotelName + '" data-gallery="multiimages" data-toggle="lightbox" style="">'
            html += '</a><small class="">' + arrHotels[i].Address + '</small></h3>'
            html += '</div>'

            html += '<div class="columns"> '
            /*Details & Facility*/
            html += '<div class="ten-columns twelve-columns-mobile">'
            html += '<div class="hidden-xs"><p class="grey d-inline-block text-truncate hidden-on-mobile">'
            html += arrHotels[i].Description.substr(0, 250) + "..</p>"
            html += GenrateFacility(arrHotels[i].Facility)
            html +='</div>'										
            html += '</div>'
            /*Rate & Booking*/
            html += '<div class="two-columns twelve-columns-mobile">'
          
            html += ' <p class="SearchBtn" style="padding-top: 0px;">'
            html += '<img src="../img/' + arrHotels[i].Category + '.png">'

            if (arrHotels[i].Total !=0)
                html += '<label class="green thin underline" ><i class="' + GetCurrencyIcon(arrHotels[i].Currency) + '"></i> ' + arrHotels[i].Total + '</label><br/>'
            else
                html += '<br/><label class="green thin underline" >Comming Soon</label><br/>'
            html += '<input type="button" class="button anthracite-gradient buttonmrgTop" onclick="GetRates(\'' + arrHotels[i].HotelId + '\');" value="Details">'
            html += '</p>'
            html += '</div>'
            html += '</div>'

            html += '</div>'
            html += '</div> <div id="div_Tab' + arrHotels[i].HotelId + '" style="display:none" class="with-mid-padding"></div>'
            $("#itemcontent").append(html);
        }
        autoSlides(i);
    } catch (e) {
        AlertDanger(e)
    }
} /*Hotel Details*/

function GenrateFilter() {
    try {
        var arrelemt = new Array();
        /* Category*/
        var opt = '';
        arrelemt = arrFilter.Category;
        for (var i = 0; i < arrFilter.Category.length; i++) {
            opt += '<option value="' + arrelemt[i].Name + '"  selected="selected">' + arrelemt[i].Name + ' Star (' + arrelemt[i].Count + ')</option>'
        }
        $("#sel_Category").html(opt);
        $("#sel_Category").addClass("multiple white-gradient easy-multiple-selection check-list ")

        /* Locations */
        opt = '';
        arrelemt = arrFilter.arrLocation;
        for (var i = 0; i < arrelemt.length; i++) {
            opt = '<option value="' + arrelemt[i].Name + '"  selected="selected">' + arrelemt[i].Name + '(' + arrelemt[i].Count + ')</option>'
        }
        $("#sel_Location").append(opt);

        /* Facility */
        opt = '';
        arrelemt = arrFilter.arrFacility;
        for (var i = 0; i < arrFilter.arrFacility.length; i++) {
            opt += '<option value="' + arrelemt[i].Name + '"  selected="selected">' + arrelemt[i].Name + '(' + arrelemt[i].Count + ')</option>'
        }
        $("#sel_Facility").append(opt);

        /*Price  Range*/
        $("#Price_Filter").empty();
        var htlml = '<span class="demo-slider" data-slider-options="{"size":false,"values":["' + arrFilter.MinPrice + '","' + arrFilter.MaxPrice + '"],"tooltip":["left","right"],"tooltipOnHover":false,"topLabelAlign":"right","barClasses":["red-gradient","glossy"]}"></span>'
        $("#Price_Filter").append(htlml);
        var Remain = arrFilter.MinPrice.toString().split('.')[1];
        $('.demo-slider').slider({
            values: [arrFilter.MinPrice, arrFilter.MaxPrice],
            tooltipOnHover: true,
            round: false,
            stickToRound: true,
            max: arrFilter.MaxPrice,
            min: arrFilter.MinPrice,
            tooltip: ["left", "left"],
            stripesSize: 'small',
            tooltipFormat: null,
            tooltipBiggerOnDrag: true,
            tooltipClass: ['compact', 'black-gradient', 'glossy'],
            barClasses: ["blue-gradient", "glossy"],
        });
        $(".slider").click(function () {
            FilterRate()
        });
    }
    catch(e){
        AlertDanger(e)
    }
} /*Filters*/

function GetSearchParams() {
    try {
        var Session = GetQueryStringParams("data").replace(/%20/g, ' ');
        session = Session.split('_');
        $('#hdnDCode').val(session[0]);
        $('#lbl_Location').text(session[1]);
        $('#txt_Destination').val(session[1]);
        $("#txt_Destination").trigger("change")
        $("#lbl_CheckIn").text(session[2])
        $("#lbl_CheckOut").text(session[3])
        $("#lbl_Room").text(session[4])
        setTimeout(function() {
            GetHotel(session[6], session[7])
        },2000)
        $('#txt_Checkin').val(session[2]);
        $('#txt_Checkout').val(session[3]);
        $('#listHotels').val(session[6]);
        setTimeout(function () {
            GetNation(session[8], session[9])
        }, 2000)
        $('#sel_Nationality').val(session[8]);
        //$("#Select_Rooms").val(session[4])
        debugger
        $("#Select_Rooms").find("option:contains('" + session[4] + "')").each(function () {
            if ($(this).text() == session[4]) {
                $(this).prop("selected", true);
            }
        });
        $("#Select_Rooms").change()
        //$($(".select-value")[0]).text(session[4])

    } catch (e) {
        AlertDanger(e)
    }
  
} /*Get Params*/

function GetHotel(HotelCode, HotelName) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        HotelCode = HotelCode.split(",")
        if (HotelName == "All")
            $(".HotelDiv .select span")[0].textContent = "For All";
        else
            $(".HotelDiv .select span")[0].textContent = HotelName;
        for (var i = 0; i <= HotelCode.length - 1; i++) {
            $('input[value="' + HotelCode[i] + '"]').prop("selected", true);
            $("#listHotels").val(HotelCode);
        }
    }
    catch (ex)
    { }

}
function GetNation(NationCode, NationName) {
    debugger;
    try {
        NationCode = NationCode.split(",")
        if (NationName == "All")
            $(".NationDiv .select span")[0].textContent = "For All";
        else
            $(".NationDiv .select span")[0].textContent = NationName;
        for (var i = 0; i <= NationCode.length - 1; i++) {
            $('input[value="' + NationCode[i] + '"]').prop("selected", true);
            $("#sel_Nationality").val(NationCode);
        }
    }
    catch (ex)
    { }

}

function GetImage(HotelCode) {
    try {
        var arrImages = $.grep(arrHotels, function (h) { return h.HotelId == HotelCode })
                                    .map(function (h) { return h.Image; })[0];
        var title = $.grep(arrHotels, function (h) { return h.HotelId == HotelCode })
                                    .map(function (h) { return h.HotelName + " <small>," + h.Address.substr(0, 50) + "</small>"; })[0];
        $.modal({
            content: '<div id="div_Images"<</div>',
            title: title,
            width: 600,
            scrolling: false,
            buttons: {
                'Close': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
        $("#div_Images").append(GetImages(arrImages, 0))
        autoSlides();
    } catch (e) {
        AlertDanger(e)
    }
}  /*Get Images*/

function GetRates(HotelCode) {
    try {
        $("#div_Tab" + HotelCode).empty();
        var html = '';
        var Tab = '';
        var arrRateGroup = $.grep(arrHotels, function (h) { return h.HotelId == HotelCode })
                                   .map(function (h) { return h.RateList; })[0];

        html += '<div class="swipe-tabs same-height"><ul class="tabs">'
        for (var i = 0; i < arrRateGroup.length; i++) {
            if (i == 0)
                html += '<li class="active"><a href="#swipe-tab-'+i+''+HotelCode+'">Group ' + parseInt(i + 1) + '</a></li>';
            else
                html += '<li><a href="#swipe-tab-' + i + '' + HotelCode + '">Group ' + parseInt(i + 1) + '</a></li>'
        }
        html += '</ul>'
        html += '<div class="tabs-content">'
        for (var i = 0; i < arrRateGroup.length; i++) {
            html += '<div id="swipe-tab-' + i + '' + HotelCode + '" class="with-mid-padding">'
            html += '<div id="div_Rates_' + HotelCode + '_' + i + '" style="overflow:scroll height:300px;"></div>';
            html +='</div>';
        }
        html += '</div>'
        $("#div_Tab" + HotelCode).append(html)
        for (var i = 0; i < arrRateGroup.length; i++) {
            GenrateB2bRates(arrRateGroup[i], 'div_Rates_' + HotelCode + '_' + i + '')
            $("#swipe-tab-" + i + HotelCode).css("height","auto")
        }
        $("#div_Tab" + HotelCode).toggle(500);

    } catch (e) { AlertDanger(e) }
}  /* Get Rates*/

function BookingRates(HotelCode, Rate) {
    debugger
    var arrBookingDetails = new Array();
    var Supplier = "";
    for (var i = 0; i < arrSearchOccupancy.length; i++) {
        var ndRoomOccupancy = $("#"+Rate);
        var ndRoom = $(ndRoomOccupancy).find(".Room_" + i);
        var AdultCount = $($(ndRoomOccupancy).find(".AdultCount" + i)).val();
        var ChildCount = $($(ndRoomOccupancy).find(".ChildCount" + i)).val();
        var ChildAges = $($(ndRoomOccupancy).find(".ChildAges" + i)).val();
        Supplier = $($(ndRoomOccupancy).find(".Supplier" + i)).val();
        var arrOcuupancy = $("#tbl_Occupancy_" + i + "_" + Rate);
        if (ndRoom.length != 0) {
            for (var r = 0; r < ndRoom.length; r++) {
                if (ndRoom[r].checked) {
                    var RoomTypeID = $($(arrOcuupancy).find(".RoomTypeId" + r)).val();
                    var RoomDescriptionId = $($(arrOcuupancy).find(".RoomDescription" + r)).val();
                    var Total = $($(arrOcuupancy).find(".TotalRate" + r)).val()
                    arrBookingDetails.push({
                        RoomTypeID: RoomTypeID,
                        RoomDescriptionId: RoomDescriptionId,
                        Total: Total,
                        noRooms: parseInt(i + 1),
                        AdultCount: AdultCount,
                        ChildCount: ChildCount,
                        ChildAges: ChildAges
                    });
                }

            }

        }
    }
    if (arrBookingDetails.length != 0) {
        var session = GetQueryStringParams("data").replace(/%20/g, ' ');
        $.ajax({
            type: "POST",
            url: "Handler/BookingHandler.asmx/GenrateBookingDetails",
            data: JSON.stringify({ ListRates: arrBookingDetails, Serach: session, HotelCode: HotelCode, Supplier: Supplier }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    window.location.href = "hotelbooking.aspx?data=" + session + "&OnHold=" + result.OnHold + "&OnRequest=" + result.OnRequest;
                }
                else {
                    AlertDanger(result.ex)
                }
            }
        })

    }
    else
        Success("Please Select Rate for Booking!!")
} /*Select Room*/

function GenrateFacility(arrFacility) {
    var html = '';
    try {
        html += '<ul class="columns with-padding  hotelpreferences1">'
        for (var i = 0; i < arrFacility.length; i++) {
            var Code = arrFacility[i];
            var sclass = '', tooltip='';
              
                if (Code.indexOf("Fridge") != -1) {
                tooltip = "fridge";
                sclass = "icohp-fridge";
            }
                else if (Code.indexOf("halal") != -1) {
                tooltip = Code;
                sclass = "icohp-halal";
            }
                else if (Code.indexOf("Microwave") != -1) {
                tooltip = "Microwave";
                sclass = "icohp-microwave";
            }
                else if (Code.indexOf("Washing machine") != -1 || Code.indexOf("Laundry") != -1 || Code.indexOf("Laundry Service") != -1) {
                tooltip = Code;
                sclass = "icohp-washing";
            }
                else if (Code.indexOf("Room") != -1) {
                tooltip = Code;
                sclass = "icohp-roomservice";
            }
                else if (Code.indexOf("Hotel safe") != -1) {
                tooltip = "safe";
                sclass = "icohp-safe";
            }
            else if (Code == "Safe") {
                tooltip = "safe";
                sclass = "icohp-safe";
            }
            else if (Code.indexOf("Business") != -1) {
                tooltip = "Conference room";
                sclass = "icohp-conferenceroom";
            }
            else if (Code.indexOf("Hairdryer") != -1 ) {
                tooltip = "Hairdryer";
                sclass = "icohp-hairdryer";
            }
            else if (Code.indexOf("Garden") != -1 || Code.indexOf("Park") != -1) {
                tooltip = "Garden";
                sclass = "icohp-garden";
            }
            else if (Code == "30")
                sclass = "icohp-grill";
            else if (Code == "Kitchen") {
                tooltip = "Kitchen";
                sclass = "icohp-kitchen";
            }
            else if (Code.indexOf("Bar") != -1) {
                tooltip = Code;
                sclass = "icohp-bar";
            }
            else if (Code.indexOf("Living") != -1) {
                tooltip = Code;
                sclass = "icohp-living";
            }
            else if (Code.indexOf("TV") != -1) {
                tooltip = Code;
                sclass = "icohp-tv";
            }
          
            else if (Code.indexOf("Wi-fi") != -1 || Code.indexOf("Internet") != -1) {
                tooltip = Code;
                sclass = "icohp-internet";
            }
            else if (Code.indexOf("Air") != -1 ) {
                tooltip = Code;
                sclass = "icohp-air";
            }
            else if (Code.indexOf("pool") != -1 ) {
                tooltip = Code;
                sclass = "icohp-pool";
            }
            else if (Code.indexOf("childcare") != -1) {
                tooltip = Code;
                sclass = "icohp-childcare";
            }
            else if (Code.indexOf("gym") != -1) {
                tooltip = Code;
                sclass = "icohp-fitness";
            }
            else if (Code.indexOf("Breakfast") != -1 ) {
                tooltip = "Breakfast";
                sclass = "icohp-breakfast";
            }
            else if (Code.indexOf("car") != -1) {
                tooltip = Code;
                sclass = "icohp-parking";
            }
            else if (Code.indexOf("pets") != -1 ) {
                tooltip = Code;
                sclass = "icohp-pets";
            }
           
            else if (Code.indexOf("spa") != -1) {
                tooltip = Code;
                sclass = "icohp-spa";
            }
          
            else if (Code.indexOf("play") != -1) {
                tooltip = "Playground";
                sclass = "icohp-playground";
            }
            else
                sclass = "";

                if (sclass !="")
                    html += ' <li class="' + sclass + ' blue-gradient glossy  children-tooltip tooltip-bottom" title="' + tooltip + '" ></li>'
        }
        html += '</ul>'

    } catch (e) {
        AlertDanger(e)
    }
    return html;
} /*Facility*/







