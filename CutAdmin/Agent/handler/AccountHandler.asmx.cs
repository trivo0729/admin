﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for AccountHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
   [System.Web.Script.Services.ScriptService]
    public class AccountHandler : System.Web.Services.WebService
    {

        CutAdmin.handler.AccountHandler obj = new CutAdmin.handler.AccountHandler();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string GetCreditInformation()
        {
            return json = obj.GetCreditInformation();
        }
        [WebMethod]
        public string Deleterecord(Int64 Id)
        {
            return json = obj.Deleterecord(Id);
        }
    }
}
