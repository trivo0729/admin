﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
   [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        CutAdmin.HotelHandler obj = new CutAdmin.HotelHandler();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        [WebMethod(EnableSession = true)]
        public string GetCount()
        {
            string json = "";
            return json = obj.GetCount();
        }

        [WebMethod(EnableSession = true)]
        public string BookingRequests()
        {
            string json = "";
            return json = obj.BookingRequests();
        }

        [WebMethod(EnableSession = true)]
        public string BookingOnHold()
        {
            string json = "";
            return json = obj.BookingOnHold();
        }

        [WebMethod(EnableSession = true)]
        public string BookingReconfirmation()
        {
            string json = "";
            return json = obj.BookingReconfirmation();
        }

        [WebMethod(EnableSession = true)]
        public string BookingGroupRequest()
        {
            string json = "";
            return json = obj.BookingGroupRequest();
        }


        public string DeleteHote(Int64 Id)
        {
            string json = "";
            return json = obj.DeleteHote(Id);
        }

        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {
            string json = "";
            return json = obj.GetDestinationCode(name);
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedHotelstoUpdate(string Country, string City)
        {
            string json = "";
            return json = obj.GetMappedHotelstoUpdate(Country, City);
        }

        [WebMethod(EnableSession = true)]
        public string SearchHotel(Int64[] HotelCodes, string CheckIn, string CheckOut, string[] Nationality, string SearchValid, string Search_Params)
        {
            try
            {
                HttpContext.Current.Session["session"] = Search_Params;
                var arrHotels = CutAdmin.DataLayer.SearchManager.GetHotelDetails(HotelCodes, CheckIn, CheckOut, Nationality, SearchValid, Search_Params);
                return jsSerializer.Serialize(new { retCode = 1, arrHotels = arrHotels });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetSearch(string Search_Params)
        {
            try
            {
                var arrHotel = CutAdmin.DataLayer.SearchManager.GetSearchHotel(Search_Params);
                if (arrHotel == null)
                    throw new Exception("Not Valid Search,Please try again.");
                return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotel });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo, Int64 HotelCode, string Supplier)
        {
            string json = "";
            return json = CutAdmin.DataLayer.SearchManager.Availbility(Serach, RoomID, RoomDescID, RoomNo, HotelCode, Supplier);
        }
    }
}
