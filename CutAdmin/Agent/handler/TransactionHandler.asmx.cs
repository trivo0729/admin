﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Agent.handler
{
    /// <summary>
    /// Summary description for TransactionHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class TransactionHandler : System.Web.Services.WebService
    {
        CutAdmin.handler.TransactionHandler obj = new CutAdmin.handler.TransactionHandler();
        [WebMethod(EnableSession = true)]
        public string GetTransactions()
        {
            string json = "";
            return json = obj.GetTransactions();
        }
        [WebMethod(EnableSession = true)]
        public string GetTransactionByDate(string dFrom, string dTo, string TransType, string RefNo)
        {
            string json = "";
            return json = obj.GetTransactionByDate(dFrom, dTo, TransType, RefNo);
        }
    }
}
