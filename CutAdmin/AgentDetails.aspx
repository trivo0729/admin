﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AgentDetails.aspx.cs" Inherits="CutAdmin.AgentDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AgentDetails.js?v=1.2"></script>
    <script src="Scripts/IndividualMarkup.js"></script>
    <script src="Scripts/AgentRoleManagment.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Agent Details</h1>
            <hr />
            <h2><a href="AddAgent.aspx" class="addnew"><i class="fa fa-user-plus"></i></a><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <br />
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Company mail (Agency Mail) </label>
                            <div class="input full-width">
                                <input type="text" id="txt_AgencyList" class="input-unstyled full-width">
                                 <input type="hidden" id="hdnDCode" />
                            </div>
                        </div>
                        <div class="two-columns twelve-columns-mobile four-columns-tablet" style="display: none">
                            <label>Agent Type</label>
                            <div class="full-width button-height">
                                <select id="AgentType" class="select">
                                    <option selected="selected" value="">All</option>
                                    <option value="Agent">Agent</option>
                                    <option value="Franchisee">Franchisee</option>
                                </select>
                            </div>
                        </div>

                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Agent Code</label>
                            <div class="input full-width">
                                <input type="text" id="AgentCode" class="input-unstyled full-width">
                            </div>
                        </div>

                        <div class="two-columns four-columns-tablet twelve-columns-mobile" style="display: none">
                            <label>Group</label>
                            <div class="full-width button-height">
                                <select id="selGroup" class="select">
                                    <option selected="selected" value="-">Select Any Group</option>
                                    <option value="5">Group A</option>
                                    <option value="6">Group B</option>
                                    <option value="15">Group D</option>
                                    <option value="16">Group C</option>
                                    <option value="17">Franchisee</option>
                                    <option value="18">Corporate </option>
                                    <option value="19">B2C</option>
                                    <option value="22">Pakistan</option>
                                    <option value="24">Test</option>
                                    <option value="28">testing2</option>
                                    <option value="29">testing</option>
                                    <option value="30">test GM</option>
                                </select>
                            </div>
                        </div>

                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Min Balance</label>
                            <div class="input full-width">
                                <input type="text" id="MinBalance" class="input-unstyled full-width">
                            </div>
                        </div>

                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <label>Status</label>
                            <div class="full-width button-height">
                                <select id="selStatus" class="select">
                                    <option selected="selected" value="">All</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="columns">
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Country</label>
                            <div class="full-width button-height">
                                <select id="selCountry" class="select" onchange="GetCountryCity()">
                                </select>
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>City</label>
                            <div class="full-width button-height">
                                <select id="selCity" class="select">
                                    <option selected="selected" value="-">Select Any City</option>
                                </select>
                            </div>
                        </div>
                        <div class="two-columns twelve-columns-mobile formBTn">
                            <br />
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="reset()">Reset</button>

                        </div>
                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <br />
                            <span class="icon-pdf right" onclick="ExportAgentDetailsToExcel('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportAgentDetailsToExcel('excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_AgentDetails">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Agency Details</th>
                            <th scope="col" class="align-center hide-on-mobile">Login Details</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Agent Code</th>
                            <th scope="col" class="hide-on-tablet">Balance</th>
                          <%--  <th scope="col" class="hide-on-tablet">Roles</th>--%>
                            <%--<th scope="col" class="align-center">Update Credit</th>--%>
                            <th scope="col" class="align-center">Live</th>
                            <%--<th scope="col" class="align-center">Markup</th>--%>
                            <th scope="col" class="align-center" style="width:30px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
    <script src="Scripts/CountryCityCode.js"></script>
</asp:Content>
