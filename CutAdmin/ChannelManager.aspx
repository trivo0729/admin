﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ChannelManager.aspx.cs" Inherits="CutAdmin.ChannelManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ChannelManager.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
         <input type="hidden" id="hdnDCode" />

        <hgroup id="main-title" class="thin">
           
            <h1>Settings</h1>
            <h2>Name:<span class="orange" id="spName"></span>,</h2>
        </hgroup>

        <div class="with-padding">

            <div class="standard-tabs margin-bottom" id="add-tabs">

                <ul class="tabs">
                    <li class="active"><a href="#Roles">Page Acess</a></li>
                    <li><a href="#API">Channels</a></li>
                </ul>

                <div class="tabs-content">

                    <div id="Roles" class="with-padding">
                        <h2>Hotel</h2>
                        <div class="columns">
                            <div class="three-columns eight-columns-mobile bold">
                                <label>Show Supplier:</label>
                            </div>
                            <div class="three-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="chk_Supplier" class="switch tiny" >
                                </p>
                            </div>
                            <div class="three-columns eight-columns-mobile bold">
                                <label>On Hold:</label></div>
                            <div class="three-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="chkOnHold" class="switch tiny" >
                                </p>
                            </div>
                        </div>
                        <hr />
                        <h2>Visa & OTB</h2>
                        <div id="tblForms"></div>
                        <%--<div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Add Visa Details::</label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Visa Status:</label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>OTB Request:</label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>OTB Status:</label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Package Details:</label>
                            </div>
                            <div class="two-columns">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Package Search: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Activity Details: </label>
                            </div>
                            <div class="two-columns">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>Activity Search: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>


                        </div>--%>
                        <div class="saveBack">

                            <button type="submit" class="button glossy mid-margin-right anthracite-gradient" onclick="SubmitAgentRole('Form')" title="Save Details">
                                Save
                            </button>

                            <button type="submit" class="button glossy anthracite-gradient" title="Back to Agent Details" onclick="window.location = 'AgentDetails.aspx'">
                                Back
                            </button>

                        </div>
                    </div>

                    <div id="API" class="with-padding">

                        <h2>Supplier</h2>
                        <div id="tblAPI" >
                        </div>
                        <%--<div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>HotelBeds: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>GRN Connect: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>GTA: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch medium mid-margin-right checked" value="1">
                                </p>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>DOTW: </label>
                            </div>
                            <div class="two-columns">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>EXPEDIA: </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>MGH:  </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                        </div>

                        <div class="columns">
                            <div class="two-columns eight-columns-mobile bold">
                                <label>ClickUrTrip:  </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                            <div class="two-columns eight-columns-mobile bold">
                                <label>TBO:  </label>
                            </div>
                            <div class="two-columns four-columns-mobile">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-medium-1" id="switch-medium-1" class="switch tiny" value="1">
                                </p>
                            </div>
                        </div>--%>
                        <div class="saveBack">

                            <button type="submit" id="btnSubmitCash" onclick="SubmitAgentRole('Hotel')" class="button glossy mid-margin-right anthracite-gradient">
                                Save
                            </button>


                        </div>

                    </div>


                </div>









            </div>

        </div>

    </section>
    <!-- End main content -->
</asp:Content>
