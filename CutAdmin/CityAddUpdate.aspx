﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CityAddUpdate.aspx.cs" Inherits="CutAdmin.CityAddUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddCity.js"></script>
    <%--  <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Favicon count
        Tinycon.setBubble(2);

        // If the browser support the Notification API, ask user for permission (with a little delay)
        if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet()) {
            setTimeout(function () {
                notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function () {
                    // Confirmation message
                    if (notify.hasNotificationPermission()) {
                        notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
                            icon: 'img/demo/icon.png',
                            system: true
                        });
                    }
                    else {
                        notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
                            icon: 'img/demo/icon.png'
                        });
                    }
                });

            }, 2000);
        }

        /*
		 * Handling of 'other actions' menu
		 */

        var otherActions = $('#otherActions'),
			current = false;

        // Other actions
        $('.list .button-group a:nth-child(2)').menuTooltip('Loading...', {

            classes: ['with-mid-padding'],
            ajax: 'ajax-demo/tooltip-content.html',

            onShow: function (target) {
                // Remove auto-hide class
                target.parent().removeClass('show-on-parent-hover');
            },

            onRemove: function (target) {
                // Restore auto-hide class
                target.parent().addClass('show-on-parent-hover');
            }
        });

        // Delete button
        $('.list .button-group a:last-child').data('confirm-options', {

            onShow: function () {
                // Remove auto-hide class
                $(this).parent().removeClass('show-on-parent-hover');
            },

            onConfirm: function () {
                // Remove element
                $(this).closest('li').fadeAndRemove();

                // Prevent default link behavior
                return false;
            },

            onRemove: function () {
                // Restore auto-hide class
                $(this).parent().addClass('show-on-parent-hover');
            }

        });


       



	</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>City Details</h1>

        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="four-columns eight-columns-mobile bold">
                    <div class="input full-width">
                        <input name="prompt-value" id="txt_SeachCity" value="" class="input-unstyled full-width" placeholder="Search City" onkeyup="SearchCity()" type="text">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <button type="button" class="button compact anthracite-gradient" onclick="newCity()">New City</button>

                </div>

            </div>
            <%--<div class="table-header button-height anthracite-gradient">
				<div class="float-right">
					Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
				</div>

				Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
					<option value="1">10</option>
					<option value="2">20</option>
					<option value="3" selected="selected">40</option>
					<option value="4">100</option>
				</select> entries
			</div>--%>
            <div class="with-padding">
                <div class="respTable">
                    <table class="table responsive-table" id="tbl_MasterCity">

                        <thead>
                            <tr>
                                <th scope="col">S.No</th>
                                <th scope="col">City</th>
                                <th scope="col">Country</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </section>

</asp:Content>
