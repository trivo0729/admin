﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using CutAdmin.BL;
using System.Globalization;
namespace CutAdmin.Common
{
    public class BoookingManger
    {
        public class Group
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }
        public static string BlockRoom(string Serach, List<Group> ListRates, string HotelCode, string Supplier)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                HttpContext.Current.Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                HotelFilter objHotel = new HotelFilter();
                if (HttpContext.Current.Session["ModelHotel" + Serach] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Serach];
                arrHotelDetails = objHotel.arrHotels.Where(d => d.HotelId == HotelCode.ToString()).FirstOrDefault();
                arrHotelDetails.RateList[0].Charge = new ServiceCharge();
                CommonLib.Response.RateGroup objAvailRate = arrHotelDetails.RateList.Where(d => d.Name == Supplier).FirstOrDefault();
                float RoomTotal = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate.RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        arrRate.OtherRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100); }); //  Take Other Rate
                        Other += arrRate.OtherRates.Select(d => d.TotalRate).ToList().Sum();
                        foreach (var objOther in arrRate.OtherRates)
                        {
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                                    other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                            }
                        }

                        RoomTotal += arrRate.Total;
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        #endregion

                        #region Check Inventory
                        foreach (var objDate in arrRate.Dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, Convert.ToDecimal(objDate.RateTypeId), objDate.RoomTypeId.ToString(), objDate.datetime, Convert.ToInt64(objAvailRate.Name));
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }
                if (noInventory.Any(d => d == 0) && bList.Any(d => d == true))
                    throw new Exception("Inventory is not available ,Please Contact Administrator or Change checking Date.");
                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                HttpContext.Current.Session["RateList" + Serach] = Rooms;
                arrHotelDetails.RateList.Where(d=>d.Name ==Supplier).FirstOrDefault().Charge = new ServiceCharge { RoomRate = RoomTotal, OtherRates = other, TotalPrice = RoomTotal + other.Select(d => d.TotalRate).ToList().Sum() };
                HttpContext.Current.Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }


        public static string GetBookingDetails(string Serach)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                List<RoomType> Rooms = (List<RoomType>)HttpContext.Current.Session["RateList" + Serach];
                List<Group> ListRates = (List<Group>)HttpContext.Current.Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, arrHotelDetails.RateList[0].Charge });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }


        public static string ValidateTransaction(List<CutAdmin.BookingHandler.Addons> arrAdons, string Serach)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (HttpContext.Current.Session["AvailDetails" + Serach] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)HttpContext.Current.Session["RatesDetails"];
                float BaseRate = objAvailRate[0].Charge.TotalPrice + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                Int64 Uid = AccountManager.GetUserByLogin();
               
                #endregion
                if (IsValid)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                else
                    throw new Exception("Your Balance is insufficient to Make this booking");
            }

            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }

    }
}