﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Globalization;

namespace CutAdmin.Common
{
    public class Common
    {
        public Facilies MyProperty { get; set; }
        public static string CurrentPageName
        {
            get
            {
                string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string sRet = oInfo.Name.ToLower();
                return sRet;
            }
        }
        public static bool IsCommonPage(string pageName)
        {
            if (!ConfigHelper.SecurePages)
                return true;
            else
            {
                string pageId = pageName.Replace(".aspx", "");
                foreach (string page in ConfigHelper.CommonPages)
                {
                    if (page.CompareTo(pageId) == 0) { return true; }
                }
                return false;
            }
        }
        public static bool IsLoginPage
        {
            get
            {
                string pageId = CurrentPageName.Replace(".aspx", "");
                return pageId.Equals("login") ? true : false;
            }
        }
        public static long ToUnixTimestamp(DateTime target)
        {
            var date = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
            var unixTimestamp = System.Convert.ToInt64((target - date).TotalSeconds);
            return unixTimestamp;
        }
        public static bool Session(out DataLayer.GlobalDefault objGlobal)
        {
            objGlobal = null;
            DataLayer.AgentDetailsOnAdmin objGlobalAgent = null;
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] == null)
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    return false;
                else
                {
                    objGlobal = (DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    return true;
                }
            }
            else
            {
                objGlobalAgent = (DataLayer.AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                objGlobal = (DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                return true;
            }

        }
        public static List<HotelLib.Response.Facility> GetFacility(DataTable dtResult)
        {
            List<HotelLib.Response.Facility> facilities_List = new List<HotelLib.Response.Facility>();
            if (dtResult == null)
                return facilities_List;

            facilities_List = dtResult.AsEnumerable()
            .Select(data => new HotelLib.Response.Facility { Name = data.Field<string>("Name") }).ToList();
            return facilities_List;
        }
        public static List<HotelLib.Response.Accomodation> GetAccomodation(DataTable dtResult)
        {
            List<HotelLib.Response.Accomodation> Accomodation_List = new List<HotelLib.Response.Accomodation>();
            if (dtResult == null)
                return Accomodation_List;

            Accomodation_List = dtResult.AsEnumerable()
            .Select(data => new HotelLib.Response.Accomodation { Code = data.Field<string>("CharacteristicCode"), Name = data.Field<string>("Description") }).ToList();
            return Accomodation_List;
        }
        public static string GetCurrenctSymbol(string currency)
        {
            if (currency == "INR")
                return "<i class=\"fa fa-inr\"></i>";
            else if (currency == "USD")
                return "<i class=\"fa fa-usd\"></i>";
            else if (currency == "GBP")
                return "<i class=\"fa fa-gbp\"></i>";
            else if (currency == "EUR")
                return "<i class=\"fa fa-eur\"></i>";
            else
                return "";
        }
        //public static net.webservicex.www.Currency GetCurrency(string m_currency)
        //{
        //    if (m_currency.ToUpper() == "EUR")
        //        return net.webservicex.www.Currency.EUR;
        //    else if (m_currency.ToUpper() == "AED")
        //        return net.webservicex.www.Currency.AED;
        //    else if (m_currency.ToUpper() == "AFA")
        //        return net.webservicex.www.Currency.AFA;
        //    else if (m_currency.ToUpper() == "ALL")
        //        return net.webservicex.www.Currency.ALL;
        //    else if (m_currency.ToUpper() == "ANG")
        //        return net.webservicex.www.Currency.ANG;
        //    else if (m_currency.ToUpper() == "ARS")
        //        return net.webservicex.www.Currency.ARS;
        //    else if (m_currency.ToUpper() == "AUD")
        //        return net.webservicex.www.Currency.AUD;
        //    else if (m_currency.ToUpper() == "AWG")
        //        return net.webservicex.www.Currency.AWG;
        //    else if (m_currency.ToUpper() == "BBD")
        //        return net.webservicex.www.Currency.BBD;
        //    else if (m_currency.ToUpper() == "BDT")
        //        return net.webservicex.www.Currency.BDT;
        //    else if (m_currency.ToUpper() == "BHD")
        //        return net.webservicex.www.Currency.BHD;
        //    else if (m_currency.ToUpper() == "BIF")
        //        return net.webservicex.www.Currency.BIF;
        //    else if (m_currency.ToUpper() == "BMD")
        //        return net.webservicex.www.Currency.BMD;
        //    else if (m_currency.ToUpper() == "BND")
        //        return net.webservicex.www.Currency.BND;
        //    else if (m_currency.ToUpper() == "BOB")
        //        return net.webservicex.www.Currency.BOB;
        //    else if (m_currency.ToUpper() == "BRL")
        //        return net.webservicex.www.Currency.BRL;
        //    else if (m_currency.ToUpper() == "BSD")
        //        return net.webservicex.www.Currency.BSD;
        //    else if (m_currency.ToUpper() == "BTN")
        //        return net.webservicex.www.Currency.BTN;
        //    else if (m_currency.ToUpper() == "BWP")
        //        return net.webservicex.www.Currency.BWP;
        //    else if (m_currency.ToUpper() == "BZD")
        //        return net.webservicex.www.Currency.BZD;
        //    else if (m_currency.ToUpper() == "CAD")
        //        return net.webservicex.www.Currency.CAD;
        //    else if (m_currency.ToUpper() == "CHF")
        //        return net.webservicex.www.Currency.CHF;
        //    else if (m_currency.ToUpper() == "CLP")
        //        return net.webservicex.www.Currency.CLP;
        //    else if (m_currency.ToUpper() == "CNY")
        //        return net.webservicex.www.Currency.CNY;
        //    else if (m_currency.ToUpper() == "COP")
        //        return net.webservicex.www.Currency.COP;
        //    else if (m_currency.ToUpper() == "CRC")
        //        return net.webservicex.www.Currency.CRC;
        //    else if (m_currency.ToUpper() == "CUP")
        //        return net.webservicex.www.Currency.CUP;
        //    else if (m_currency.ToUpper() == "CVE")
        //        return net.webservicex.www.Currency.CVE;
        //    else if (m_currency.ToUpper() == "CYP")
        //        return net.webservicex.www.Currency.CYP;
        //    else if (m_currency.ToUpper() == "CZK")
        //        return net.webservicex.www.Currency.CZK;
        //    else if (m_currency.ToUpper() == "DJF")
        //        return net.webservicex.www.Currency.DJF;
        //    else if (m_currency.ToUpper() == "DKK")
        //        return net.webservicex.www.Currency.DKK;
        //    else if (m_currency.ToUpper() == "DOP")
        //        return net.webservicex.www.Currency.DOP;
        //    else if (m_currency.ToUpper() == "DZD")
        //        return net.webservicex.www.Currency.DZD;
        //    else if (m_currency.ToUpper() == "EEK")
        //        return net.webservicex.www.Currency.EEK;
        //    else if (m_currency.ToUpper() == "EGP")
        //        return net.webservicex.www.Currency.EGP;
        //    else if (m_currency.ToUpper() == "ETB")
        //        return net.webservicex.www.Currency.ETB;
        //    else if (m_currency.ToUpper() == "FKP")
        //        return net.webservicex.www.Currency.FKP;
        //    else if (m_currency.ToUpper() == "GBP")
        //        return net.webservicex.www.Currency.GBP;
        //    else if (m_currency.ToUpper() == "GHC")
        //        return net.webservicex.www.Currency.GHC;
        //    else if (m_currency.ToUpper() == "GIP")
        //        return net.webservicex.www.Currency.GIP;
        //    else if (m_currency.ToUpper() == "GMD")
        //        return net.webservicex.www.Currency.GMD;
        //    else if (m_currency.ToUpper() == "GNF")
        //        return net.webservicex.www.Currency.GNF;
        //    else if (m_currency.ToUpper() == "GTQ")
        //        return net.webservicex.www.Currency.GTQ;
        //    else if (m_currency.ToUpper() == "GYD")
        //        return net.webservicex.www.Currency.GYD;
        //    else if (m_currency.ToUpper() == "HKD")
        //        return net.webservicex.www.Currency.HKD;
        //    else if (m_currency.ToUpper() == "HNL")
        //        return net.webservicex.www.Currency.HNL;
        //    else if (m_currency.ToUpper() == "MVR")
        //        return net.webservicex.www.Currency.MVR;
        //    else if (m_currency.ToUpper() == "HRK")
        //        return net.webservicex.www.Currency.HRK;
        //    else if (m_currency.ToUpper() == "HTG")
        //        return net.webservicex.www.Currency.HTG;
        //    else if (m_currency.ToUpper() == "HUF")
        //        return net.webservicex.www.Currency.HUF;
        //    else if (m_currency.ToUpper() == "IDR")
        //        return net.webservicex.www.Currency.IDR;
        //    else if (m_currency.ToUpper() == "ILS")
        //        return net.webservicex.www.Currency.ILS;
        //    else if (m_currency.ToUpper() == "INR")
        //        return net.webservicex.www.Currency.INR;
        //    else if (m_currency.ToUpper() == "IQD")
        //        return net.webservicex.www.Currency.IQD;
        //    else if (m_currency.ToUpper() == "ISK")
        //        return net.webservicex.www.Currency.ISK;
        //    else if (m_currency.ToUpper() == "JMD")
        //        return net.webservicex.www.Currency.JMD;
        //    else if (m_currency.ToUpper() == "JOD")
        //        return net.webservicex.www.Currency.JOD;
        //    else if (m_currency.ToUpper() == "JPY")
        //        return net.webservicex.www.Currency.JPY;
        //    else if (m_currency.ToUpper() == "KES")
        //        return net.webservicex.www.Currency.KES;
        //    else if (m_currency.ToUpper() == "KHR")
        //        return net.webservicex.www.Currency.KHR;
        //    else if (m_currency.ToUpper() == "KMF")
        //        return net.webservicex.www.Currency.KMF;
        //    else if (m_currency.ToUpper() == "KPW")
        //        return net.webservicex.www.Currency.KPW;
        //    else if (m_currency.ToUpper() == "KRW")
        //        return net.webservicex.www.Currency.KRW;
        //    else if (m_currency.ToUpper() == "KWD")
        //        return net.webservicex.www.Currency.KWD;
        //    else if (m_currency.ToUpper() == "KYD")
        //        return net.webservicex.www.Currency.KYD;
        //    else if (m_currency.ToUpper() == "KZT")
        //        return net.webservicex.www.Currency.KZT;
        //    else if (m_currency.ToUpper() == "LAK")
        //        return net.webservicex.www.Currency.LAK;
        //    else if (m_currency.ToUpper() == "LBP")
        //        return net.webservicex.www.Currency.LBP;
        //    else if (m_currency.ToUpper() == "LKR")
        //        return net.webservicex.www.Currency.LKR;
        //    else if (m_currency.ToUpper() == "LRD")
        //        return net.webservicex.www.Currency.LRD;
        //    else if (m_currency.ToUpper() == "LSL")
        //        return net.webservicex.www.Currency.LSL;
        //    else if (m_currency.ToUpper() == "LTL")
        //        return net.webservicex.www.Currency.LTL;
        //    else if (m_currency.ToUpper() == "LVL")
        //        return net.webservicex.www.Currency.LVL;
        //    else if (m_currency.ToUpper() == "LYD")
        //        return net.webservicex.www.Currency.LYD;
        //    else if (m_currency.ToUpper() == "MAD")
        //        return net.webservicex.www.Currency.MAD;
        //    else if (m_currency.ToUpper() == "MDL")
        //        return net.webservicex.www.Currency.MDL;
        //    else if (m_currency.ToUpper() == "MGF")
        //        return net.webservicex.www.Currency.MGF;
        //    else if (m_currency.ToUpper() == "MKD")
        //        return net.webservicex.www.Currency.MKD;
        //    else if (m_currency.ToUpper() == "MMK")
        //        return net.webservicex.www.Currency.MMK;
        //    else if (m_currency.ToUpper() == "MNT")
        //        return net.webservicex.www.Currency.MNT;
        //    else if (m_currency.ToUpper() == "MOP")
        //        return net.webservicex.www.Currency.MOP;
        //    else if (m_currency.ToUpper() == "MRO")
        //        return net.webservicex.www.Currency.MRO;
        //    else if (m_currency.ToUpper() == "MTL")
        //        return net.webservicex.www.Currency.MTL;
        //    else if (m_currency.ToUpper() == "MUR")
        //        return net.webservicex.www.Currency.MUR;
        //    else if (m_currency.ToUpper() == "MVR")
        //        return net.webservicex.www.Currency.MVR;
        //    else if (m_currency.ToUpper() == "MWK")
        //        return net.webservicex.www.Currency.MWK;
        //    else if (m_currency.ToUpper() == "MXN")
        //        return net.webservicex.www.Currency.MXN;
        //    else if (m_currency.ToUpper() == "MYR")
        //        return net.webservicex.www.Currency.MYR;
        //    else if (m_currency.ToUpper() == "MZM")
        //        return net.webservicex.www.Currency.MZM;
        //    else if (m_currency.ToUpper() == "NAD")
        //        return net.webservicex.www.Currency.NAD;
        //    else if (m_currency.ToUpper() == "NGN")
        //        return net.webservicex.www.Currency.NGN;
        //    else if (m_currency.ToUpper() == "NIO")
        //        return net.webservicex.www.Currency.NIO;
        //    else if (m_currency.ToUpper() == "NOK")
        //        return net.webservicex.www.Currency.NOK;
        //    else if (m_currency.ToUpper() == "NPR")
        //        return net.webservicex.www.Currency.NPR;
        //    else if (m_currency.ToUpper() == "NZD")
        //        return net.webservicex.www.Currency.NZD;
        //    else if (m_currency.ToUpper() == "OMR")
        //        return net.webservicex.www.Currency.OMR;
        //    else if (m_currency.ToUpper() == "PAB")
        //        return net.webservicex.www.Currency.PAB;
        //    else if (m_currency.ToUpper() == "PEN")
        //        return net.webservicex.www.Currency.PEN;
        //    else if (m_currency.ToUpper() == "PGK")
        //        return net.webservicex.www.Currency.PGK;
        //    else if (m_currency.ToUpper() == "PHP")
        //        return net.webservicex.www.Currency.PHP;
        //    else if (m_currency.ToUpper() == "PKR")
        //        return net.webservicex.www.Currency.PKR;
        //    else if (m_currency.ToUpper() == "PLN")
        //        return net.webservicex.www.Currency.PLN;
        //    else if (m_currency.ToUpper() == "PYG")
        //        return net.webservicex.www.Currency.PYG;
        //    else if (m_currency.ToUpper() == "QAR")
        //        return net.webservicex.www.Currency.QAR;
        //    else if (m_currency.ToUpper() == "ROL")
        //        return net.webservicex.www.Currency.ROL;
        //    else if (m_currency.ToUpper() == "RUB")
        //        return net.webservicex.www.Currency.RUB;
        //    else if (m_currency.ToUpper() == "SAR")
        //        return net.webservicex.www.Currency.SAR;
        //    else if (m_currency.ToUpper() == "SBD")
        //        return net.webservicex.www.Currency.SBD;
        //    else if (m_currency.ToUpper() == "SCR")
        //        return net.webservicex.www.Currency.SCR;
        //    else if (m_currency.ToUpper() == "SDD")
        //        return net.webservicex.www.Currency.SDD;
        //    else if (m_currency.ToUpper() == "SEK")
        //        return net.webservicex.www.Currency.SEK;
        //    else if (m_currency.ToUpper() == "SGD")
        //        return net.webservicex.www.Currency.SGD;
        //    else if (m_currency.ToUpper() == "SHP")
        //        return net.webservicex.www.Currency.SHP;
        //    else if (m_currency.ToUpper() == "SIT")
        //        return net.webservicex.www.Currency.SIT;
        //    else if (m_currency.ToUpper() == "SKK")
        //        return net.webservicex.www.Currency.SKK;
        //    else if (m_currency.ToUpper() == "SLL")
        //        return net.webservicex.www.Currency.SLL;
        //    else if (m_currency.ToUpper() == "SOS")
        //        return net.webservicex.www.Currency.SOS;
        //    else if (m_currency.ToUpper() == "SRG")
        //        return net.webservicex.www.Currency.SRG;
        //    else if (m_currency.ToUpper() == "STD")
        //        return net.webservicex.www.Currency.STD;
        //    else if (m_currency.ToUpper() == "SVC")
        //        return net.webservicex.www.Currency.SVC;
        //    else if (m_currency.ToUpper() == "SYP")
        //        return net.webservicex.www.Currency.SYP;
        //    else if (m_currency.ToUpper() == "SZL")
        //        return net.webservicex.www.Currency.SZL;
        //    else if (m_currency.ToUpper() == "THB")
        //        return net.webservicex.www.Currency.THB;
        //    else if (m_currency.ToUpper() == "TND")
        //        return net.webservicex.www.Currency.TND;
        //    else if (m_currency.ToUpper() == "TOP")
        //        return net.webservicex.www.Currency.TOP;
        //    else if (m_currency.ToUpper() == "TRL")
        //        return net.webservicex.www.Currency.TRL;
        //    else if (m_currency.ToUpper() == "TRY")
        //        return net.webservicex.www.Currency.TRY;
        //    else if (m_currency.ToUpper() == "TTD")
        //        return net.webservicex.www.Currency.TTD;
        //    else if (m_currency.ToUpper() == "TWD")
        //        return net.webservicex.www.Currency.TWD;
        //    else if (m_currency.ToUpper() == "TZS")
        //        return net.webservicex.www.Currency.TZS;
        //    else if (m_currency.ToUpper() == "UAH")
        //        return net.webservicex.www.Currency.UAH;
        //    else if (m_currency.ToUpper() == "UGX")
        //        return net.webservicex.www.Currency.UGX;
        //    else if (m_currency.ToUpper() == "USD")
        //        return net.webservicex.www.Currency.USD;
        //    else if (m_currency.ToUpper() == "UYU")
        //        return net.webservicex.www.Currency.UYU;
        //    else if (m_currency.ToUpper() == "VEB")
        //        return net.webservicex.www.Currency.VEB;
        //    else if (m_currency.ToUpper() == "VND")
        //        return net.webservicex.www.Currency.VND;
        //    else if (m_currency.ToUpper() == "VUV")
        //        return net.webservicex.www.Currency.VUV;
        //    else if (m_currency.ToUpper() == "WST")
        //        return net.webservicex.www.Currency.WST;
        //    else if (m_currency.ToUpper() == "XAF")
        //        return net.webservicex.www.Currency.XAF;
        //    else if (m_currency.ToUpper() == "XAG")
        //        return net.webservicex.www.Currency.XAG;
        //    else if (m_currency.ToUpper() == "XAU")
        //        return net.webservicex.www.Currency.XAU;
        //    else if (m_currency.ToUpper() == "XCD")
        //        return net.webservicex.www.Currency.XCD;
        //    else if (m_currency.ToUpper() == "XOF")
        //        return net.webservicex.www.Currency.XOF;
        //    else if (m_currency.ToUpper() == "XPD")
        //        return net.webservicex.www.Currency.XPD;
        //    else if (m_currency.ToUpper() == "XPF")
        //        return net.webservicex.www.Currency.XPF;
        //    else if (m_currency.ToUpper() == "XPT")
        //        return net.webservicex.www.Currency.XPT;
        //    else if (m_currency.ToUpper() == "YER")
        //        return net.webservicex.www.Currency.YER;
        //    else if (m_currency.ToUpper() == "YUM")
        //        return net.webservicex.www.Currency.YUM;
        //    else if (m_currency.ToUpper() == "ZAR")
        //        return net.webservicex.www.Currency.ZAR;
        //    else if (m_currency.ToUpper() == "ZMK")
        //        return net.webservicex.www.Currency.ZMK;
        //    else if (m_currency.ToUpper() == "ZWD")
        //        return net.webservicex.www.Currency.ZWD;
        //    else
        //        return net.webservicex.www.Currency.USD;
        //}

        public static string Facility(string Code, out string tooltip)
        {
            Code = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Code);
            tooltip = "";
            if (Code == "Fridge")
            {
                tooltip = "fridge";
                return "icohp-fridge";
            }
            else if (Code == "Halal Food Available")
            {
                tooltip = Code;
                return "icohp-halal";
            }
            else if (Code == "Mini fridge")
            {
                tooltip = "fridge";
                return "icohp-fridge";
            }
            else if (Code == "Microwave")
            {
                tooltip = "Microwave";
                return "icohp-microwave";
            }
            else if (Code == "Washing machine" || Code == "Laundry" || Code == "Laundry Service")
            {
                tooltip = Code;
                return "icohp-washing";
            }
            else if (Code == "Room Service" || Code == "Room Service - 24 Hours")
            {
                tooltip = Code;
                return "icohp-roomservice";
            }
            else if (Code == "Hotel safe")
            {
                tooltip = "safe";
                return "icohp-safe";
            }
            else if (Code == "Safe")
            {
                tooltip = "safe";
                return "icohp-safe";
            }
            else if (Code == "Conference room" || Code == "Business Centre")
            {
                tooltip = "Conference room";
                return "icohp-conferenceroom";
            }
            else if (Code == "Hairdressing salon")
            {
                tooltip = "Hairdryer";
                return "icohp-hairdryer";
            }
            else if (Code == "Hairdryer")
            {
                tooltip = "Hairdryer";
                return "icohp-hairdryer";
            }
            else if (Code == "Garden")
            {
                tooltip = "Garden";
                return "icohp-garden";
            }
            else if (Code == "30")
                return "icohp-grill";
            else if (Code == "Kitchen")
            {
                tooltip = "Kitchen";
                return "icohp-kitchen";
            }
            else if (Code == "Bar" || Code == "Free welcome drink")
            {
                tooltip = "Bar";
                return "icohp-bar";
            }
            else if (Code == "Minibar")
            {
                tooltip = "Bar";
                return "icohp-bar";
            }
            else if (Code == "Living room")
            {
                tooltip = "Living room";
                return "icohp-living";
            }
            else if (Code == "TV")
            {
                tooltip = "TV";
                return "icohp-tv";
            }
            else if (Code == "TV lounge")
            {
                tooltip = "TV";
                return "icohp-tv";
            }
            else if (Code == "Satellite TV")
            {
                tooltip = "TV";
                return "icohp-tv";
            }
            else if (Code == "Cable TV")
            {
                tooltip = "TV";
                return "icohp-tv";
            }
            else if (Code == "Wi-fi" || Code == "Complimentary WiFi access" || Code == "Free Wireless Internet" || Code == "Free High-Speed Internet")
            {
                tooltip = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code == "Wired Internet" || Code == "Free Internet")
            {
                tooltip = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code == "Internet access" || Code == "High Speed Internet")
            {
                tooltip = Code;
                return "icohp-internet";
            }
            else if (Code == "Air conditioning in public areas")
            {
                tooltip = "Air conditioning";
                return "icohp-air";
            }
            else if (Code == "Air conditioning in Restaurant" || Code == "Air Conditioning" || Code == "air conditioning in restaurant")
            {
                tooltip = Code;
                return "icohp-air";
            }
            else if (Code == "Individually adjustable air conditioning")
            {
                tooltip = "Air conditioning";
                return "icohp-air";
            }
            else if (Code == "Outdoor freshwater pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Outdoor heated pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Outdoor saltwater pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Indoor freshwater pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Indoor heated pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Indoor saltwater pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Games room")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "Children’s swimming area")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "Fireplace")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "Entertainment programme for children")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "Gym")
            {
                tooltip = "Fitness Equipment";
                return "icohp-fitness";
            }
            else if (Code == "Fitness")
            {
                tooltip = "Fitness Equipment";
                return "icohp-fitness";
            }
            else if (Code == "Breakfast" || Code == "Continental Breakfast")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Breakfast a la carte")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Breakfast and dinner")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Breakfast and lunch")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Breakfast buffet" || Code == "Breakfast Buffet")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Breakfast served to the table")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Early bird breakfast")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Hot breakfast")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "Late riser breakfast")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "YES Car Park")
            {
                tooltip = "Parking";
                return "icohp-parking";
            }
            else if (Code == "Car park" || Code == "Car Parking - Onsite Free" || Code == "Free Parking" || Code == "Free Valet Parking")
            {
                tooltip = "Parking";
                return "icohp-parking";
            }
            else if (Code == "Large pets allowed (over 5 kg)" || Code == "large pets allowed (over 5 kg)")
            {
                tooltip = Code;
                return "icohp-pets";
            }
            else if (Code == "Small pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "YES Large pets allowed (over 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "54YES Small pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "Spa Centre")
            {
                tooltip = Code;
                return "icohp-spa";
            }
            else if (Code == "Spa treatments" || Code == "Spa access")
            {
                tooltip = "Spa";
                return "icohp-spa";
            }
            else if (Code == "Badminton")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Basketball")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Casino")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Golf")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Golf desk")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Golf practice facility")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Table tennis")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Tennis")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Volleyball")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else
                return "";
        }
        public static string UserRating(float rating)
        {
            if (Convert.ToInt32(rating) == 1)
                return "user-rating-1.png";
            else if (Convert.ToInt32(rating) == 2)
                return "user-rating-2.png";
            else if (Convert.ToInt32(rating) == 3)
                return "user-rating-3.png";
            else if (Convert.ToInt32(rating) == 4)
                return "user-rating-4.png";
            else if (Convert.ToInt32(rating) == 5)
                return "user-rating-5.png";
            else
                return "user-rating-0.png";
        }
        public static int HotelBed_HotelCategory(string code)
        {
            if (code.ToUpper() == "1EST")
                return 1;
            else if (code.ToUpper() == "2EST")
                return 2;
            else if (code.ToUpper() == "3EST")
                return 3;
            else if (code.ToUpper() == "4EST")
                return 4;
            else if (code.ToUpper() == "5EST")
                return 5;
            else
                return 0;

        }

        public static int MGH_HotelCategory(string code)
        {
            if (code == "Bed and Breakfast")
                return 40;
            else if (code == "Guest Houses")
                return 41;
            else if (code == "Serviced Apartments")
                return 42;
            else if (code == "Boutique Hotel")
                return 43;
            else if (code == "3 Star Hotel")
                return 3;
            else if (code == "Resort and Houseboats")
                return 6;
            else if (code == "Homestays")
                return 7;
            else if (code == "Budget Hotel")
                return 8;
            else if (code == "4 Star Hotel")
                return 4;
            else if (code == "5 Star Hotel")
                return 5;
            else if (code == "Villas")
                return 11;
            else if (code == "Apartment")
                return 12;
            else if (code == "Business Hotel")
                return 13;
            else if (code == "City Hotel")
                return 14;
            else if (code == "Beach Resort")
                return 15;
            else if (code == "Resort")
                return 16;
            else if (code == "Service Apartment")
                return 17;
            else if (code == "Beach Hotel")
                return 18;
            else if (code == "Airport Hotel")
                return 19;
            else if (code == "Apartment Hotel")
                return 20;
            else if (code == "Heritage")
                return 21;
            else if (code == "Resorts")
                return 22;
            else if (code == "Homestay")
                return 23;
            else if (code == "Spa")
                return 25;
            else if (code == "Tent or Cabanas")
                return 26;
            else if (code == "Houseboat")
                return 27;
            else if (code == "Lodge")
                return 28;
            else if (code == "Restaurant")
                return 29;
            else if (code == "Spa Resort")
                return 30;
            else if (code == "Yoga Retreat")
                return 31;
            else if (code == "Retreat")
                return 32;
            else if (code == "1 Star Hotel")
                return 1;
            else if (code == "2 Star Hotel")
                return 2;
            else if (code == "Keys")
                return 35;
            else if (code == "Rural")
                return 36;
            else if (code == "Hostel")
                return 37;
            else if (code == "Others")
                return 38;
            else if (code == "Bungalow")
                return 39;
            else
                return 0;
        }

        public static int DoTW_HotelCategory(string code)
        {

            if (code == "559")
                return Convert.ToInt32(code);
            else if (code == "560")
                return Convert.ToInt32(code);
            else if (code == "561")
                return Convert.ToInt32(code);
            else if (code == "562")
                return Convert.ToInt32(code);
            else if (code == "563")
                return Convert.ToInt32(code);
            else
                return Convert.ToInt32(code);
        }

        public static int Common_HotelCategory(string code)
        {
            if (code.ToUpper() == "1EST" || code == "1 Star Hotel" || code == "559" || code == "1")
                return 1;
            else if (code.ToUpper() == "2EST" || code == "2 Star Hotel" || code == "560" || code == "2")
                return 2;
            else if (code.ToUpper() == "3EST" || code == "3 Star Hotel" || code == "561" || code == "3")
                return 3;
            else if (code.ToUpper() == "4EST" || code == "4 Star Hotel" || code == "562" || code == "4")
                return 4;
            else if (code.ToUpper() == "5EST" || code == "5 Star Hotel" || code == "563" || code == "5")
                return 5;
            //else if (code == "559")
            //    return Convert.ToInt32(code);
            //else if (code == "560")
            //    return Convert.ToInt32(code);
            //else if (code == "561")
            //    return Convert.ToInt32(code);
            //else if (code == "562")
            //    return Convert.ToInt32(code);
            //else if (code == "563")
            //    return Convert.ToInt32(code);
            else if (code == "48055")
                return Convert.ToInt32(code);
            else if (code == "Bed and Breakfast")
                return 40;
            else if (code == "Guest Houses")
                return 41;
            else if (code == "Serviced Apartments")
                return 42;
            else if (code == "Boutique Hotel")
                return 43;
            else if (code == "Resort and Houseboats")
                return 6;
            else if (code == "Homestays")
                return 7;
            else if (code == "Budget Hotel")
                return 8;
            else if (code == "Villas")
                return 11;
            else if (code == "Apartment")
                return 12;
            else if (code == "Business Hotel")
                return 13;
            else if (code == "City Hotel")
                return 14;
            else if (code == "Beach Resort")
                return 15;
            else if (code == "Resort")
                return 16;
            else if (code == "Service Apartment")
                return 17;
            else if (code == "Beach Hotel")
                return 18;
            else if (code == "Airport Hotel")
                return 19;
            else if (code == "Apartment Hotel")
                return 20;
            else if (code == "Heritage")
                return 21;
            else if (code == "Resorts")
                return 22;
            else if (code == "Homestay")
                return 23;
            else if (code == "Spa")
                return 25;
            else if (code == "Tent or Cabanas")
                return 26;
            else if (code == "Houseboat")
                return 27;
            else if (code == "Lodge")
                return 28;
            else if (code == "Restaurant")
                return 29;
            else if (code == "Spa Resort")
                return 30;
            else if (code == "Yoga Retreat")
                return 31;
            else if (code == "Retreat")
                return 32;
            else if (code == "Keys")
                return 35;
            else if (code == "Rural")
                return 36;
            else if (code == "Hostel")
                return 37;
            else if (code == "Others")
                return 38;
            else if (code == "Bungalow")
                return 39;
            else
                return 0;

        }

        public static string MGHFacility(string Code, out string tooltip)
        {
            tooltip = "";
            if (Code == "Fridge")
            {
                tooltip = "fridge";
                return "icohp-fridge";
            }
            //else if (Code == "Mini fridge")
            //{
            //    tooltip = "fridge";
            //    return "icohp-fridge";
            //}
            //else if (Code == "Microwave")
            //{
            //    tooltip = "Microwave";
            //    return "icohp-microwave";
            //}
            //else if (Code == "Washing machine")
            //{
            //    tooltip = "Washing machine";
            //    return "icohp-washing";
            //}
            else if (Code == "Room Service")
            {
                tooltip = Code;
                return "icohp-roomservice";
            }
            else if (Code == "Hotel safe")
            {
                tooltip = "safe";
                return "icohp-safe";
            }
            else if (Code == "Safe at Reception / Room")
            {
                tooltip = "safe";
                return "icohp-safe";
            }
            else if (Code == "Conference room")
            {
                tooltip = "Conference room";
                return "icohp-conferenceroom";
            }
            else if (Code == "salon")
            {
                tooltip = "salon";
                return "icohp-hairdryer";
            }
            //else if (Code == "Hairdryer")
            //{
            //    tooltip = "Hairdryer";
            //    return "icohp-hairdryer";
            //}
            else if (Code == "Garden")
            {
                tooltip = "Garden";
                return "icohp-garden";
            }
            else if (Code == "Barbecue grill(s)")
                return "icohp-grill";
            //else if (Code == "Kitchen")
            //{
            //    tooltip = "Kitchen";
            //    return "icohp-kitchen";
            //}
            else if (Code == "Bar")
            {
                tooltip = "Bar";
                return "icohp-bar";
            }
            else if (Code == "Bar/lounge")
            {
                tooltip = "Bar/lounge";
                return "icohp-bar";
            }
            //else if (Code == "Living room")
            //{
            //    tooltip = "Living room";
            //    return "icohp-living";
            //}
            //else if (Code == "TV lounge")
            //{
            //    tooltip = "TV";
            //    return "icohp-tv";
            //}
            else if (Code == "TV lounge")
            {
                tooltip = "TV";
                return "icohp-tv";
            }
            else if (Code == "Wi-fi" || Code == "Wi Fi Internet")
            {
                tooltip = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code == "Wired Internet")
            {
                tooltip = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code == "Internet access-complementary" || Code == "Internet access-high-speed")
            {
                tooltip = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code == "Air-conditioned public areas")
            {
                tooltip = "Air conditioning";
                return "icohp-air";
            }
            else if (Code == "Air conditioning in public areas")
            {
                tooltip = "Air conditioning";
                return "icohp-air";
            }
            else if (Code == "Air conditioning in Restaurant")
            {
                tooltip = "Air conditioning";
                return "icohp-air";
            }
            else if (Code == "Outdoor freshwater pool" || Code == "swimming pool" || Code == "Indoor saltwater pool" || Code == "Indoor heated pool" || Code == "Outdoor freshwater pool" || Code == "Outdoor saltwater pool" || Code == "Outdoor heated pool" || Code == "Indoor freshwater pool")
            {
                tooltip = "Swimming Pool";
                return "icohp-pool";
            }
            else if (Code == "Games room" || Code == "Arcade/game room")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "ChildrenÃƒÂ¢Ã¢Â‚Â¬Ã¢Â„Â¢s swimming area" || Code == "Entertainment programme for children" || Code == "ChildrenÃƒÂ¢Ã¢Â‚Â¬Ã¢Â„Â¢s play area")
            {
                tooltip = "Child Care";
                return "icohp-childcare";
            }
            else if (Code == "Gym")
            {
                tooltip = "Fitness Equipment";
                return "icohp-fitness";
            }
            else if (Code == "Fitness" || Code == "Health or fitness facilities")
            {
                tooltip = "Fitness Equipment";
                return "icohp-fitness";
            }
            else if (Code == "Breakfast services" || Code == "Breakfast buffet" || Code == "Breakfast" || Code == "Continental breakfast" || Code == "Breakfast and lunch" || Code == "Early bird breakfast" || Code == "Late riser breakfast" || Code == "Hot breakfast" || Code == "Breakfast and dinner" || Code == "Breakfast served to the table")
            {
                tooltip = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code == "YES Car park")
            {
                tooltip = "Parking";
                return "icohp-parking";
            }
            else if (Code == "Car park")
            {
                tooltip = "Parking";
                return "icohp-parking";
            }
            else if (Code == "Parking" || Code == "Parking(free)" || Code == "Parking(valet)" || Code == "Parking(secure)" || Code == "Secure parking" || Code == "NO Car park")
            {
                tooltip = "Parking";
                return "icohp-parking";
            }
            else if (Code == "Large pets allowed (over 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "Small pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "YES Large pets allowed (over 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "YES Small pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "NO Large pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "NO Small pets allowed (under 5 kg)")
            {
                tooltip = "Pets";
                return "icohp-pets";
            }
            else if (Code == "Spa services")
            {
                tooltip = "Spa";
                return "icohp-spa";
            }
            else if (Code == "Spa services nearby")
            {
                tooltip = "Spa";
                return "icohp-spa";
            }
            else if (Code == "Badminton")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Basketball")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Casino")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Golf" || Code == "Mini golf" || Code == "Golf desk" || Code == "Golf practice facility")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Table tennis" || Code == "Tennis" || Code == "Paddle tennis")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else if (Code == "Volleyball" || Code == "Beach volleyball")
            {
                tooltip = "Playground";
                return "icohp-playground";
            }
            else
                return "";
        }

        public static string MainHotelFacilities(string Code, out string text)
        {
            text = "";
            if (Code.ToLower().Contains("internet"))
            {
                text = "High-speed Internet";
                return "icohp-internet";
            }
            else if (Code.ToLower().Contains("air conditioning"))
            {
                text = "Air conditioning";
                return "icohp-air";
            }
            else if (Code.ToLower().Contains("swimming pool"))
            {
                text = "Swimming pool";
                return "icohp-pool";
            }
            else if (Code.ToLower().Contains("childcare"))
            {
                text = "Childcare";
                return "icohp-childcare";
            }
            else if (Code.ToLower().Contains("fitness equipment"))
            {
                text = "Fitness equipment";
                return "icohp-fitness";
            }
            else if (Code.ToLower().Contains("breakfast"))
            {
                text = "Breakfast";
                return "icohp-breakfast";
            }
            else if (Code.ToLower().Contains("parking"))
            {
                text = "Parking";
                return "icohp-parking";
            }
            else if (Code.ToLower().Contains("pets allowed"))
            {
                text = "Pets allowed";
                return "icohp-pets";
            }
            else if (Code.ToLower().Contains("spa services"))
            {
                text = "Spa services";
                return "icohp-spa";
            }
            else if (Code.ToLower().Contains("hair dryer"))
            {
                text = "Hair dryer";
                return "icohp-hairdryer";
            }
            else if (Code.ToLower().Contains("garden"))
            {
                text = "Courtyard garden";
                return "icohp-garden";
            }
            else if (Code.ToLower().Contains("grill barbecue"))
            {
                text = "Grill / Barbecue";
                return "icohp-grill";
            }
            else if (Code.ToLower().Contains("kitchen"))
            {
                text = "Kitchen";
                return "icohp-kitchen";
            }
            else if (Code.ToLower().Contains("bar"))
            {
                text = "Bar";
                return "icohp-bar";
            }
            else if (Code.ToLower().Contains("living"))
            {
                text = "Living";
                return "icohp-living";
            }
            else if (Code.ToLower().Contains("tv"))
            {
                text = "TV";
                return "icohp-tv";
            }
            else if (Code.ToLower().Contains("fridge"))
            {
                text = "Fridge";
                return "icohp-fridge";
            }
            else if (Code.ToLower().Contains("microwave"))
            {
                text = "Microwave";
                return "icohp-microwave";
            }
            else if (Code.ToLower().Contains("washing maschine"))
            {
                text = "Washing maschine";
                return "icohp-washing";
            }
            else if (Code.ToLower().Contains("room service"))
            {
                text = "Room service";
                return "icohp-roomservice";
            }
            else if (Code.ToLower().Contains("safe"))
            {
                text = "Reception Safe";
                return "icohp-safe";
            }
            else if (Code.ToLower().Contains("playground"))
            {
                text = "Playground";
                return "icohp-playground";
            }
            else if (Code.ToLower().Contains("conference room"))
            {
                text = "Conference room";
                return "icohp-conferenceroom";
            }
            else
                return "none";
        }
    }

    public class Facilies
    {
        public string Name { get; set; }
        public int Priority { get; set; }
        public string icon { get; set; }
        public string toolip { get; set; }
    }


}