﻿using CommonLib.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Common
{
    public class CommonXMLHelper
    {
        public static bool AvailRequest(string session, out Models.CommonStatusCode objStatusCode)
        {

            CommonAvailRQ objCommonAvailRQ = new CommonAvailRQ();
            Models.AvailCommonRequest objAvail = new Models.AvailCommonRequest();
            List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
            string[] array_input = session.Split('_');
            objCommonAvailRQ.EchoToken = Guid.NewGuid().ToString().Replace("-", "");
            objCommonAvailRQ.Language = "ENG";
            objCommonAvailRQ.PageNumber = 1;
            objCommonAvailRQ.SessionID = Common.ToUnixTimestamp(DateTime.Now).ToString();
            objCommonAvailRQ.UserName = System.Configuration.ConfigurationManager.AppSettings["APIUserName"];
            objCommonAvailRQ.Password = System.Configuration.ConfigurationManager.AppSettings["APIPassword"];
            objCommonAvailRQ.DestinationCode = array_input[0].ToString();
            objAvail.Location = array_input[1].ToString();
            objCommonAvailRQ.DestinationType = "SIMPLE";
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            objCommonAvailRQ.CheckInDate = fDate.ToString("yyyyMMdd");
            objAvail.CheckIn = fDate.ToString("dd/MM/yyyy");
            objCommonAvailRQ.CheckOutDate = tDate.ToString("yyyyMMdd");
            objAvail.CheckOut = tDate.ToString("dd/MM/yyyy");
            string occpancy = array_input[5].ToString();
            objAvail.DestinationCode = array_input[0].ToString();
            try
            {
                objAvail.HotelCode = array_input[6].ToString();
                objAvail.HotelName = array_input[7].ToString();
                objAvail.StarRating = array_input[8].ToString();
                objAvail.NationalityCode = array_input[9].ToString();
            }
            catch { }
            string[] array_occupancy = occpancy.Split('$');
            int adult = 0;
            int child = 0;
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Customer> list_Customer = new List<Customer>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        int Age = Convert.ToInt32(n_array[i]);
                        if (Age > 0)
                            list_Customer.Add(new Customer { Age = Age, type = "CH" });
                    }
                }
                list_Occupancy.Add(new HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
            }
            List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
            //foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
            //{
            //    DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
            //}
            foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
            {
                if (!DISTINST_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                    DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
            }
            List<HotelOccupancy> LIST_FINAL_OCCUPANCY = new List<HotelOccupancy>();
            int k = 0;
            foreach (Occupancy objOccupancy in DISTINST_OCCUPANCY)
            {
                var Occupancy_List = list_Occupancy.Where(data => data.AdultCount == objOccupancy.AdultCount && data.ChildCount == objOccupancy.ChildCount).ToList();
                int RoomCount = 0;
                int AdultCount = 0;
                int ChildCount = 0;

                List<Customer> ListCustomer = new List<Customer>();

               
                foreach (HotelOccupancy objHotelOccupancy in Occupancy_List)
                {
                    RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                    ChildCount = objHotelOccupancy.ChildCount;
                    AdultCount = objHotelOccupancy.AdultCount;
                    foreach (Customer objCustomer in objHotelOccupancy.GuestList)
                    {
                        ListCustomer.Add(objCustomer);
                    }
                }
                //k++;

                LIST_FINAL_OCCUPANCY.Add(new HotelOccupancy { RoomCount = RoomCount, AdultCount = AdultCount, ChildCount = ChildCount, GuestList = ListCustomer });
            }
            objAvail.Room = list_Occupancy.Sum(data => data.RoomCount);
            objAvail.Adult = list_Occupancy.Sum(data => data.AdultCount);
            objAvail.Child = list_Occupancy.Sum(data => data.ChildCount);
            objAvail.List_Occupancy = LIST_FINAL_OCCUPANCY;
            objAvail.List_ActualOccupancy = list_Occupancy;
            string HotelCode = array_input[6].ToString();
            objCommonAvailRQ.HotelOccupancy = LIST_FINAL_OCCUPANCY;
            if (!String.IsNullOrEmpty(HotelCode))
                objCommonAvailRQ.sHotelCodeList = new HotelCodeList { withinResults = "Y", ProductCode = HotelCode };

            //string xml = objCommonAvailRQ.GenerateXML();
            string xml = "";
            string m_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            int Status = 0;
            List<Occupancy> m_List_Hotel_Occupancy = new List<Occupancy>();
            foreach (HotelOccupancy objOccupancy in LIST_FINAL_OCCUPANCY)
            {
                m_List_Hotel_Occupancy.Add(new Occupancy { RoomCount = objOccupancy.RoomCount, AdultCount = objOccupancy.AdultCount, ChildCount = objOccupancy.ChildCount });
            }

            /*.... to comment/uncomment ..Start..*/
            //string txt = System.IO.File.ReadAllText(@"E:\_Projects_\test cut\HotelBeds_Mumbai\HotelValuedAvailRS.xml");
            //m_response = txt;
            bool bResponse = true;
            //Status = 1;
            /*.... to comment/uncomment ..End..*/

            /*To be comment/uncomment*/
            //bool bResponse = objHotelValuedAvailRQ.Post(xml, out m_response, out RequestHeader, out ResponseHeader, out Status);
            /*.......................*/
            objAvail.Night = Convert.ToInt32((tDate - fDate).TotalDays);
            objStatusCode = new Models.CommonStatusCode { Request = xml, Response = m_response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
            return bResponse;
        }
    }
}