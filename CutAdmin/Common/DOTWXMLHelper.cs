﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.Models;
using DOTWLib.Common;
using DOTWLib.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Common
{
    public class DOTWXMLHelper
    {
        public bool Response { get; set; }
        public string session { get; set; }
        public Models.DOTWStatusCode objStatusCode { get; set; }
        public HttpContext context { get; set; }
        public GlobalDefault objGlobalDefault { get; set; }
        public AgentDetailsOnAdmin objAgentDetailsOnAdmin { get; set; }
        public Models.DoTWHotel objDoTWHotel { get; set; }
        public static string xmlResponse { get; set; }
        public System.Globalization.CultureInfo CultureInfo { get; set; }
        //public static bool AvailRequest(string session, out Models.DOTWStatusCode objStatusCode)
        public void AvailRequest(bool IsFirstResqest)
        {
            //GlobalDefault objGlobalDefault = null;
            DoTWAvailRequest objDOTWValuedAvailRQ = new DoTWAvailRequest();
            List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
            objDOTWValuedAvailRQ.username = System.Configuration.ConfigurationManager.AppSettings["DOTWUserName"];
            objDOTWValuedAvailRQ.password = System.Configuration.ConfigurationManager.AppSettings["DOTWPassword"];
            objDOTWValuedAvailRQ.id = System.Configuration.ConfigurationManager.AppSettings["DOTWHotelId"];
            objDOTWValuedAvailRQ.source = 1;
            objDOTWValuedAvailRQ.product = "hotel";
            objDOTWValuedAvailRQ.command = "searchhotels";
            string[] array_input = session.Split('_');
            City objCity = new City();
            objCity.name = array_input[1].ToString();
            DataTable dtResult;
            string citycode = "";
            string[] Country = array_input[1].Split(',');
            string cty = Country[0];

            objDOTWValuedAvailRQ.fDate = array_input[2].ToString();
            objDOTWValuedAvailRQ.tDate = array_input[3].ToString();
            objDOTWValuedAvailRQ.hotelname = array_input[7].ToString();

            objDOTWValuedAvailRQ.rating = array_input[6].ToString();
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            Currency objCurrency = new Currency();
            objCurrency.text = "";
            objCurrency.value = 520; //get from data base
            objDOTWValuedAvailRQ.currency = objCurrency;
            string occpancy = array_input[5].ToString();
            string[] array_occupancy = occpancy.Split('$');
            Room[] arrRoom = new Room[Convert.ToInt32(array_input[4])];
            List<Room> lstRoom = new List<Room>();
            int adult = 0;
            int child = 0;
            passengerNationality objPassengerNationality = new passengerNationality();
            //objPassengerNationality.name = array_input[7].ToString();
            //objPassengerNationality.code = 81; //from databse
            passengerCountryOfResidence objpassengerCountryOfResidence = new passengerCountryOfResidence();
            //objpassengerCountryOfResidence.name = "";
            //objpassengerCountryOfResidence.code = 72;

            //.........................................................................................................................................................
            string Nationality = array_input[9].ToString();
            string Residence = array_input[9].ToString();
            DBHelper.DBReturnCode retCodeNationality = DOTWManager.GetDOTWNationalityCode(Nationality, out dtResult);
            string Nation = (dtResult.Rows[0]["name"]).ToString();
            string NationCode = (dtResult.Rows[0]["code"]).ToString();
            objPassengerNationality.name = Nation;
            objPassengerNationality.code = Convert.ToInt16(NationCode);
            DBHelper.DBReturnCode retCodeResidency = DOTWManager.GetDOTWNationalityCode(Residence, out dtResult);
            string Residency = (dtResult.Rows[0]["name"]).ToString();
            string ResidenceCode = (dtResult.Rows[0]["code"]).ToString();
            objpassengerCountryOfResidence.name = Residency;
            objpassengerCountryOfResidence.code = Convert.ToInt16(ResidenceCode);
            //.........................................................................................................................................................

           

            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Child> lstChild = new List<Child>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        //...............................under test by maqsood...........................................//
                        int Age = Convert.ToInt32(n_array[i]);
                        //...............................under test by maqsood...........................................//
                        if (Age > 0)
                            lstChild.Add(new Child { Age = Age });
                    }
                }
                lstRoom.Add(new Room { adults = adult, children = lstChild, rateBasis = -1, passengerNationality = objPassengerNationality, passengerCountryOfResidence = objpassengerCountryOfResidence });
            }

            ////////// ======================== Ocupancy=======================================================////////
            List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
            foreach (Room Check_Duplicate_Occupancy in lstRoom)
            {
                // if (!DISTINST_OCCUPANCY.Exists(data => data.AdultCount == Check_Duplicate_Occupancy.adults && data.ChildCount == Check_Duplicate_Occupancy.children.Count))
                DISTINST_OCCUPANCY.Add(new Occupancy { AdultCount = Check_Duplicate_Occupancy.adults, ChildCount = Check_Duplicate_Occupancy.children.Count });
            }
            //...............................under test by maqsood...........................................//
            // ================================= Hotel Ocupancy ======================================================//
            List<Room> LIST_FINAL_OCCUPANCY = new List<Room>();
            int k = 0;
            foreach (Occupancy objOccupancy in DISTINST_OCCUPANCY)
            {
                //int k = 0;
                var Occupancy_List = lstRoom.Where(data => data.adults == objOccupancy.AdultCount && data.children.Count == objOccupancy.ChildCount).ToList();
                int RoomCount = 0;
                int AdultCount = 0;
                int ChildCount = 0;

                List<Child> ListCustomer = new List<Child>();

                foreach (Room objHotelOccupancy in Occupancy_List)
                {
                    //..............BBBB.................under test by maqsood...........................................//
                    //RoomCount = RoomCount + objHotelOccupancy.;
                    //RoomCount = objHotelOccupancy.RoomCount;
                    //..............EEEE.................under test by maqsood...........................................//
                    ChildCount = objHotelOccupancy.children.Count;
                    AdultCount = objHotelOccupancy.adults;
                    //...............BBBB................under test by maqsood...........................................//

                    //ListCustomer.AddRange(objHotelOccupancy.GuestList);
                    //for (int k = 0; k < objHotelOccupancy.ChildCount; k++)
                    //{
                    //    ListCustomer.Add(objHotelOccupancy.GuestList[k]);
                    //}

                    //................EEEEE...............under test by maqsood...........................................//
                }
                if (Convert.ToInt32(objOccupancy.ChildCount) > 0)
                {
                    if (objOccupancy.ChildCount > 1)
                    {
                        foreach (Child objCustomer in Occupancy_List[k].children)
                        {
                            ListCustomer.Add(objCustomer);

                        }
                        //k++;
                    }
                    else
                    {
                        foreach (Child objCustomer in Occupancy_List[0].children)
                        {
                            ListCustomer.Add(objCustomer);
                        }
                    }

                }

                //k++;

                LIST_FINAL_OCCUPANCY.Add(new Room { adults = AdultCount, children = ListCustomer, rateBasis = -1, passengerNationality = objPassengerNationality, passengerCountryOfResidence = objpassengerCountryOfResidence });
            }
            string m_Response = "";
            //string m_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            int Status = 0;
            int nStatus = 0;
            List<Occupancy> m_List_Hotel_Occupancy = new List<Occupancy>();
            foreach (Room objOccupancy in LIST_FINAL_OCCUPANCY)
            {
                m_List_Hotel_Occupancy.Add(new Occupancy { AdultCount = objOccupancy.adults, ChildCount = objOccupancy.children.Count });
            }
            objDOTWValuedAvailRQ.room = lstRoom;
            DOTWWebClient objDOTWWebClient = new DOTWWebClient();
            string xmlRequest;


            DBHelper.DBReturnCode retCode = DOTWManager.GetDOTWCityCode(cty, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                citycode = dtResult.Rows[0]["code"].ToString();
                //DataTable dtcty = dtResult.Select("name='" + cty + "'").CopyToDataTable();
                //citycode = dtcty.Rows[0]["code"].ToString();

                objCity.code = Convert.ToInt32(citycode);
                objDOTWValuedAvailRQ.city = objCity;
                xmlRequest = objDOTWValuedAvailRQ.GenerateXML();
                //test 5 Feb
                //string txt = System.IO.File.ReadAllText(@"C:\Users\User1\Downloads\DoTW Certification Process\Res_Dubai_7JUN16_3R2A0C1A0C3A1C_NP.xml");
                //m_Response = txt;
                //bool bResponse = true;
                //nStatus = 1;
                //test 5 Feb end

                bool bResponse = objDOTWWebClient.Post(xmlRequest, out m_Response, out RequestHeader, out ResponseHeader, out nStatus);
                xmlResponse = m_Response;
                Response = bResponse;
                //objStatusCode.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                objDOTWValuedAvailRQ.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                objStatusCode = new Models.DOTWStatusCode { Night = objDOTWValuedAvailRQ.Night, Request = xmlRequest, Response = m_Response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objDOTWValuedAvailRQ, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
                if (IsFirstResqest)
                    GetHotellist();

                // CUT.Common.Common.Session(out objGlobalDefault);
                //LogManager.Add(0, objStatusCode.RequestHeader, objStatusCode.Request, objStatusCode.ResponseHeader, objStatusCode.Response, objGlobalDefault.sid, objStatusCode.Status);
                //return bResponse;
            }
            else
            {
                m_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><result command=\"searchhotels\" tID=\"1493105287001170\" ip=\"127.0.0.1\"  date=\"2017-04-25 07:28:07\" version=\"2.0\" elapsedTime=\"0.080264091491699\"><currencyShort>USD</currencyShort><sort order=\"asc\">sortByPrice</sort><hotels count=\"0\"></hotels><successful>TRUE</successful></result>";
                Status = 1;
                objDOTWValuedAvailRQ.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                xmlRequest = "";
                objStatusCode = new Models.DOTWStatusCode { Night = objDOTWValuedAvailRQ.Night, Request = xmlRequest, Response = m_Response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objDOTWValuedAvailRQ, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
                if (IsFirstResqest)
                    GetHotellist();
            }
        }


        public void GetHotellist()
        {
            //AvailRequest(false);
            string m_Response = "";
            Response = AvailRequest(session, out m_Response);
            if (Response)
            {
                int count;
                float min_price;
                float max_price;
                int m_counthotel;
                ParseDOTWResponse.CultureInfos = CultureInfo;
                ParseDOTWResponse objParseDOTWResponse = new ParseDOTWResponse();
                objParseDOTWResponse.Context = context;
                //objParseDOTWResponse.CultureInfos = CultureInfo;
                bool bParse_DoTW = false;
                bParse_DoTW = objParseDOTWResponse.ParseXML(objStatusCode.Response, objStatusCode.DisplayRequest.Night, objStatusCode.DisplayRequest.room.Count, objStatusCode.Occupancy, session);

                bParse_DoTW = objParseDOTWResponse.ParsePriceXML(m_Response, objStatusCode.DisplayRequest.Night, objStatusCode.DisplayRequest.room.Count, objStatusCode.Occupancy);


                //List<DOTWLib.Response.DOTWHotelDetails> List_DOTWHotelDetails = objParseDOTWResponse.GetServiceHotel(objDOTWStatusCode.DisplayRequest.room);
                List<DOTWLib.Response.DOTWHotelDetails> List_DOTWHotelDetails = objParseDOTWResponse.GetServiceHotelPrice(objStatusCode.DisplayRequest.room);
                min_price = List_DOTWHotelDetails.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
                max_price = List_DOTWHotelDetails.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
                m_counthotel = List_DOTWHotelDetails.Where(data => data.CUTPrice == min_price).Count();

                objDoTWHotel = new Models.DoTWHotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseDOTWResponse.Facility, HotelDetails = List_DOTWHotelDetails, CountHotel = m_counthotel, Location = objParseDOTWResponse.Location, Category = objParseDOTWResponse.Category, DisplayRequest = objStatusCode.DisplayRequest };


            }

        }

        public static bool AvailRequest(string session, out string m_Response)
        {
            //var session = city + '_' + fdate + '_' + tdate + '_' + room + '_' + occupancy + '_' + hotel + '_' + rating + '_' + nationality;
            //DOTWValuedAvailRQ objDOTWValuedAvailRQ = new DOTWValuedAvailRQ();
            //SearchRequest objSearchRequest = new SearchRequest();
            Models.DoTWAvailRequest objDOTWValuedAvailRQ = new DoTWAvailRequest();
            List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
            objDOTWValuedAvailRQ.username = System.Configuration.ConfigurationManager.AppSettings["DOTWUserName"];
            objDOTWValuedAvailRQ.password = System.Configuration.ConfigurationManager.AppSettings["DOTWPassword"];
            objDOTWValuedAvailRQ.id = System.Configuration.ConfigurationManager.AppSettings["DOTWHotelId"];
            objDOTWValuedAvailRQ.source = 1;
            objDOTWValuedAvailRQ.product = "hotel";
            objDOTWValuedAvailRQ.command = "searchhotels";
            string[] array_input = session.Split('_');
            City objCity = new City();
            objCity.name = array_input[0].ToString();
            DataTable dtResult;
            string citycode = "";
            string[] Country = array_input[1].Split(',');
            string cty = Country[0];


            objDOTWValuedAvailRQ.fDate = array_input[2].ToString();
            objDOTWValuedAvailRQ.tDate = array_input[3].ToString();
            objDOTWValuedAvailRQ.hotelname = array_input[5].ToString();
            objDOTWValuedAvailRQ.rating = array_input[6].ToString();
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            Currency objCurrency = new Currency();
            objCurrency.text = "";
            objCurrency.value = 520; //get from data base
            objDOTWValuedAvailRQ.currency = objCurrency;
            string occpancy = array_input[5].ToString();
            string[] array_occupancy = occpancy.Split('$');
            Room[] arrRoom = new Room[Convert.ToInt32(array_input[4])];
            List<Room> lstRoom = new List<Room>();
            int adult = 0;
            int child = 0;
            passengerNationality objPassengerNationality = new passengerNationality();
            //objPassengerNationality.name = array_input[7].ToString();
            //objPassengerNationality.code = 81; //from databse
            passengerCountryOfResidence objpassengerCountryOfResidence = new passengerCountryOfResidence();
            //objpassengerCountryOfResidence.name = "";
            //objpassengerCountryOfResidence.code = 72;

            //.........................................................................................................................................................
            string Nationality = array_input[9].ToString();
            string Residence = array_input[9].ToString();
            DBHelper.DBReturnCode retCodeNationality = DOTWManager.GetDOTWNationalityCode(Nationality, out dtResult);
            string Nation = (dtResult.Rows[0]["name"]).ToString();
            string NationCode = (dtResult.Rows[0]["code"]).ToString();
            objPassengerNationality.name = Nation;
            objPassengerNationality.code = Convert.ToInt16(NationCode);
            DBHelper.DBReturnCode retCodeResidency = DOTWManager.GetDOTWNationalityCode(Residence, out dtResult);
            string Residency = (dtResult.Rows[0]["name"]).ToString();
            string ResidenceCode = (dtResult.Rows[0]["code"]).ToString();
            objpassengerCountryOfResidence.name = Residency;
            objpassengerCountryOfResidence.code = Convert.ToInt16(ResidenceCode);
            //.........................................................................................................................................................

           
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Child> lstChild = new List<Child>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        //...............................under test by maqsood...........................................//
                        int Age = Convert.ToInt32(n_array[i]);
                        //...............................under test by maqsood...........................................//
                        if (Age > 0)
                            lstChild.Add(new Child { Age = Age });
                    }
                }
                lstRoom.Add(new Room { adults = adult, children = lstChild, rateBasis = -1, passengerNationality = objPassengerNationality, passengerCountryOfResidence = objpassengerCountryOfResidence });
            }
            ////////// ======================== Ocupancy=======================================================////////
            List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
            foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
            {
                //if (!DISTINST_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
            }
            //...............................under test by maqsood...........................................//
            // ================================= Hotel Ocupancy ======================================================//
            List<HotelOccupancy> LIST_FINAL_OCCUPANCY = new List<HotelOccupancy>();
            int k = 0;
            foreach (Occupancy objOccupancy in DISTINST_OCCUPANCY)
            {
                //int k = 0;
                var Occupancy_List = list_Occupancy.Where(data => data.AdultCount == objOccupancy.AdultCount && data.ChildCount == objOccupancy.ChildCount).ToList();
                int RoomCount = 0;
                int AdultCount = 0;
                int ChildCount = 0;

                List<Customers> ListCustomer = new List<Customers>();
                //..............BBBB.................under test by maqsood...........................................//

                //..............EEEE.................under test by maqsood...........................................//

                foreach (HotelOccupancy objHotelOccupancy in Occupancy_List)
                {
                    //..............BBBB.................under test by maqsood...........................................//
                    RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                    //RoomCount = objHotelOccupancy.RoomCount;
                    //..............EEEE.................under test by maqsood...........................................//
                    ChildCount = objHotelOccupancy.ChildCount;
                    AdultCount = objHotelOccupancy.AdultCount;
                    //...............BBBB................under test by maqsood...........................................//

                    //ListCustomer.AddRange(objHotelOccupancy.GuestList);
                    //for (int k = 0; k < objHotelOccupancy.ChildCount; k++)
                    //{
                    //    ListCustomer.Add(objHotelOccupancy.GuestList[k]);
                    //}

                    //................EEEEE...............under test by maqsood...........................................//
                }
                if (Convert.ToInt32(objOccupancy.ChildCount) > 0)
                {
                    if (objOccupancy.ChildCount > 1)
                    {
                        foreach (Customers objCustomer in Occupancy_List[k].GuestList)
                        {
                            ListCustomer.Add(objCustomer);

                        }
                        k++;
                    }
                    else
                    {
                        foreach (Customers objCustomer in Occupancy_List[0].GuestList)
                        {
                            ListCustomer.Add(objCustomer);
                        }
                    }

                }

                //k++;

                LIST_FINAL_OCCUPANCY.Add(new HotelOccupancy { RoomCount = RoomCount, AdultCount = AdultCount, ChildCount = ChildCount, GuestList = ListCustomer });
            }
            m_Response = "";
            //string m_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            //int Status = 0;
            int nStatus = 0;
            List<Occupancy> m_List_Hotel_Occupancy = new List<Occupancy>();
            foreach (HotelOccupancy objOccupancy in LIST_FINAL_OCCUPANCY)
            {
                m_List_Hotel_Occupancy.Add(new Occupancy { RoomCount = objOccupancy.RoomCount, AdultCount = objOccupancy.AdultCount, ChildCount = objOccupancy.ChildCount });
            }
            objDOTWValuedAvailRQ.room = lstRoom;
            DOTWWebClient objDOTWWebClient = new DOTWWebClient();
            string xmlRequest;


            //test 5 Feb
            //string txt = System.IO.File.ReadAllText(@"C:\Users\User1\Downloads\DoTW Certification Process\Res_Dubai_7JUN16_3R2A0C1A0C3A1C.xml");
            //m_Response = txt;
            //bool bResponse = true;
            //nStatus = 1;
            //test 5 Feb end
            DBHelper.DBReturnCode retCode = DOTWManager.GetDOTWCityCode(cty, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                citycode = dtResult.Rows[0]["code"].ToString();
                objCity.code = Convert.ToInt32(citycode);
                objDOTWValuedAvailRQ.city = objCity;
                xmlRequest = objDOTWValuedAvailRQ.GeneratePriceXML();
                //DataTable dtcty = dtResult.Select("name='" + cty + "'").CopyToDataTable();
                //citycode = dtcty.Rows[0]["code"].ToString();

                bool bResponse = objDOTWWebClient.Post(xmlRequest, out m_Response, out RequestHeader, out ResponseHeader, out nStatus);
                return bResponse;
            }
            else
            {
                m_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><result command=\"searchhotels\" tID=\"1493105287001170\" ip=\"127.0.0.1\"  date=\"2017-04-25 07:28:07\" version=\"2.0\" elapsedTime=\"0.080264091491699\"><currencyShort>USD</currencyShort><sort order=\"asc\">sortByPrice</sort><hotels count=\"0\"></hotels><successful>TRUE</successful></result>";
                bool bResponse = true;
                return bResponse;
            }

        }
    }
}