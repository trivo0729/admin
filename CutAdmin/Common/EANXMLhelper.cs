﻿using CutAdmin.DataLayer;
using EANLib.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;

namespace CutAdmin.Common
{
    public class EANXMLhelper
    {
        public bool bResponse { get; set; }
        public string session { get; set; }
        public CutAdmin.Models.EANStatusCode objStatusCode { get; set; }
        public Models.EANHotels objEANHotels { get; set; }
        public static HttpContext Context { get; set; }
        #region HotelSearch
        public void AvailRequest()
        {
            AvailRequest objAvailRequest = new AvailRequest();
            List<Room> list_Occupancy = new List<Room>();
            string[] array_input = session.Split('_');
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            TimeSpan difference = tDate - fDate;
            double days = difference.TotalDays;
            objAvailRequest.departureDate = tDate.ToString("yyyyMMdd");
            objAvailRequest.departureDate = tDate.ToString("MM/dd/yyyy").Replace("-", "/");
            objAvailRequest.arrivalDate = fDate.ToString("yyyyMMdd");
            objAvailRequest.arrivalDate = fDate.ToString("MM/dd/yyyy").Replace("-", "/");
            objAvailRequest.countryCode = "";
            string occpancy = array_input[5].ToString();
            string[] array_occupancy = occpancy.Split('$');
            objAvailRequest.city = array_input[1];
            int adult = 0;
            int child = 0;
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Room> list_Customer = new List<Room>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        int NoChild = Convert.ToInt32(n_array[i]);
                        if (NoChild > 0)
                            list_Customer.Add(new Room { numberOfChildren = NoChild, numberOfAdults = adult });
                    }
                }
                list_Occupancy.Add(new Room { RoomCount = array_occupancy.Length, numberOfAdults = Convert.ToInt32(m_array[0]), numberOfChildren = Convert.ToInt32(n_array[0]), childAges = Convert.ToInt32(n_array[1]) });
            }
            objAvailRequest.room = list_Occupancy;
            string xmlRequest = objAvailRequest.GenerateXML();
            string m_response = "";
            string RequestHeader = "";
            string ResponseHeader = "";
            int Status;

            // lOCAL RESPONSE // 
            //string txt = System.IO.File.ReadAllText(@"E:\Xpedia\Response&Request\Dubai2'2,21.2,21Response.txt");
            //m_response = txt;
            //bool bResponse = true;
            //Status = 1;
            // Response Ends Here // 
            bResponse = objAvailRequest.Post(xmlRequest, "list", out m_response, out RequestHeader, out ResponseHeader, out Status);
            EANLib.Response.HotelListResponse objHotelListResponse = new EANLib.Response.HotelListResponse();
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objHotelListResponse = objSerlizer.Deserialize<EANLib.Response.HotelListResponse>(m_response);
            objStatusCode = new Models.EANStatusCode { Request = xmlRequest, Response = m_response, Status = Status, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Occupancy = list_Occupancy, noRooms = array_occupancy.Length, FDate = objAvailRequest.arrivalDate, TDate = objAvailRequest.departureDate, noNights = Convert.ToInt64(days) };
            GetHotellist();
            // return bResponse;

        }
        #endregion

        #region Parse list

        public void GetHotellist()
        {
            float min_price, max_price;
            Int32 m_counthotel;
            List<EANLib.Response.HotelSummary> List_EANHotels;
            EANLib.Response.HotelListResponse objHotelListResponse = new EANLib.Response.HotelListResponse();
            ParseEANResponse objEANResponse = new ParseEANResponse();
            objEANResponse.Context = Context;
            bResponse = objEANResponse.ParseXML(objStatusCode.Response, objStatusCode, out objHotelListResponse);
            List_EANHotels = objHotelListResponse.HotelList.HotelSummary;
            min_price = Convert.ToSingle(List_EANHotels.OrderBy(data => data.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total).Select(data => data.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total).FirstOrDefault());
            max_price = Convert.ToSingle(List_EANHotels.OrderByDescending(data => data.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total).Select(data => data.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.total).FirstOrDefault());
            m_counthotel = List_EANHotels.Count;
            List<string> ValueAds = objEANResponse.List_Facility.Distinct().ToList();
            //objEANHotels.Facility = ValueAds;
            objEANHotels = new Models.EANHotels { MinPrice = min_price, MaxPrice = max_price, Facility = ValueAds, CountHotel = m_counthotel, HotelDetail = List_EANHotels, FromDate = objStatusCode.FDate, ToDate = objStatusCode.TDate, Occupancy = objStatusCode.Occupancy, noNights = objStatusCode.noNights, noRoomRequest = objStatusCode.noRooms };
            //Session["EanAvail"] = objEANHotels;
        }
        #endregion

        #region HotelRoom Search
        public static bool AvailRooms(string HotelId, string ToDate, string Fromdate, List<EANLib.Request.Room> Occupancy, out   string m_response)
        {
            // ,out EANStatusCode objStatusCode
            AvailRequest objAvailRequest = new AvailRequest();
            objAvailRequest.arrivalDate = Fromdate;
            objAvailRequest.departureDate = ToDate;
            objAvailRequest.room = Occupancy;
            string xmlRequest = objAvailRequest.GenerateRoomXML(HotelId);
            m_response = "";
            string RequestHeader = "", ResponseHeader = "";

            int Status;

            // lOCAL RESPONSE // 
            //string txt = System.IO.File.ReadAllText(@"E:\Xpedia\Response&Request\Dubai20Response.txt");
            //m_response = txt;
            //bool bResponse = true;
            //Status = 1;
            // Response Ends Here // 
            bool bResponse = objAvailRequest.Post(xmlRequest, "avail", out m_response, out RequestHeader, out ResponseHeader, out Status);
            EANLib.Response.HotelListResponse objHotelListResponse = new EANLib.Response.HotelListResponse();
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objHotelListResponse = objSerlizer.Deserialize<EANLib.Response.HotelListResponse>(m_response);
            // objStatusCode = new Models.EANStatusCode { Request = xmlRequest, Response = m_response, Status = Status, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Occupancy = list_Occupancy, noRooms = array_occupancy.Length, FDate = objAvailRequest.arrivalDate, TDate = objAvailRequest.departureDate };


            return bResponse;

        }
        #endregion HotelRoom Search

        #region Hotel ConformBooking
        public static bool ConformRequest(EANLib.Response.HotelRoomAvailabilityResponse objExpediaRoomAvailability, List<EANLib.Request.Guest> Guest_listEan, string HotelCode, string RoomCode, string RoomTypeCode, out string m_response)
        {
            bool response = false;
            m_response = "";
            List<Room> list_Occupancy = new List<Room>();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            // ,out EANStatusCode objStatusCode
            AvailRequest objAvailRequest = new AvailRequest();
            string[] array_input = HttpContext.Current.Session["session"].ToString().Split('_');
            DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
            DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
            TimeSpan difference = tDate - fDate;
            double days = difference.TotalDays;
            objAvailRequest.departureDate = tDate.ToString("yyyyMMdd");
            objAvailRequest.departureDate = tDate.ToString("MM/dd/yyyy").Replace("-", "/");
            objAvailRequest.arrivalDate = fDate.ToString("yyyyMMdd");
            objAvailRequest.arrivalDate = fDate.ToString("MM/dd/yyyy").Replace("-", "/");
            //objAvailRequest.rateKey  = objExpediaRoomAvailability.HotelRoomResponse.Single(data=>data.roomTypeCode == RoomCode && data.rateCode == RoomTypeCode).RateInfos.RateInfo.RoomGroup.Room
            //objAvailRequest.countryCode = objExpediaRoomAvailability.;
            EANLib.Response.HotelRoomResponse objRoomResponse = objExpediaRoomAvailability.HotelRoomResponse.Single(data => data.roomTypeCode == RoomCode && data.rateCode == RoomTypeCode);
            List<EANLib.Response.Room> objRoom = objRoomResponse.RateInfos.RateInfo.RoomGroup.Room;
            string occpancy = array_input[5].ToString();
            string[] array_occupancy = occpancy.Split('$');
            objAvailRequest.city = array_input[1];
            objAvailRequest.rateKey = objRoom[0].rateKey;
            int adult = 0;
            int child = 0;
            int j = 0;
            foreach (string str in array_occupancy)
            {
                string[] m_array = str.Split('|');
                adult = adult + Convert.ToInt32(m_array[0]);
                string[] n_array = m_array[1].ToString().Split('^');
                child = child + Convert.ToInt32(n_array[0]);
                List<Room> list_Customer = new List<Room>();
                for (int i = 0; i < n_array.Length; i++)
                {
                    if (i > 0)
                    {
                        int NoChild = Convert.ToInt32(n_array[i]);
                        if (NoChild > 0)
                            list_Customer.Add(new Room { numberOfChildren = NoChild, numberOfAdults = adult, FirstName = Guest_listEan[0].sCustomer[i].Name, LastName = Guest_listEan[0].sCustomer[i].LastName, Type = Guest_listEan[0].sCustomer[i].type, ratekey = objRoom[i].rateKey });
                    }
                }
                list_Occupancy.Add(new Room { RoomCount = array_occupancy.Length, numberOfAdults = Convert.ToInt32(m_array[0]), numberOfChildren = Convert.ToInt32(n_array[0]), childAges = Convert.ToInt32(n_array[1]), FirstName = Guest_listEan[0].sCustomer[j].Name, LastName = Guest_listEan[0].sCustomer[j].LastName, Type = Guest_listEan[0].sCustomer[j].type, ratekey = objRoom[j].rateKey });
                j++;
            }
            objAvailRequest.HotelId = HotelCode;
            objAvailRequest.room = list_Occupancy;
            objAvailRequest.roomTypeCode = RoomTypeCode;
            objAvailRequest.bedTypeId = objRoomResponse.BedTypes.BedType[0].Id;
            objAvailRequest.chargeableRate = Convert.ToString(Convert.ToDecimal(objRoomResponse.RateInfos.RateInfo.ChargeableRateInfo.total));
            objAvailRequest.supplierType = objRoomResponse.supplierType;
            objAvailRequest.roomTypeCode = RoomCode;
            objAvailRequest.rateCode = RoomTypeCode;
            objAvailRequest.smokingPreference = objRoomResponse.smokingPreferences;
            m_response = "";
            string RequestHeader = "", ResponseHeader = "";
            string xmlRequest = objAvailRequest.GetBookingDetails();
            int Status;
            // string txt = System.IO.File.ReadAllText(@"D:\Kashif\Xpedia\Doc\BookingRequest.txt");
            // lOCAL RESPONSE // 
            //string txt = System.IO.File.ReadAllText(@"D:\Kashif\Xpedia\Doc\BookingResonse.txt");
            // m_response = txt;
            //bool bResponse = true;
            //Status = 1;
            // Response Ends Here // 
            bool bResponse = objAvailRequest.PostBookingRequest(xmlRequest, "res", out m_response, out RequestHeader, out ResponseHeader, out Status);
            EANLib.Response.HotelListResponse objHotelListResponse = new EANLib.Response.HotelListResponse();
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objHotelListResponse = objSerlizer.Deserialize<EANLib.Response.HotelListResponse>(m_response);
            // objStatusCode = new Models.EANStatusCode { Request = xmlRequest, Response = m_response, Status = Status, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Occupancy = list_Occupancy, noRooms = array_occupancy.Length, FDate = objAvailRequest.arrivalDate, TDate = objAvailRequest.departureDate };



            return response = bResponse;
        }
        #endregion
    }
}