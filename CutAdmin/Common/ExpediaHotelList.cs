﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.dbml;
namespace CutAdmin.Common
{
    public class ExpediaHotelList
    {
        helperDataContext db = new helperDataContext();
        public string Session { get; set; }
        public List<CommonLib.Response.CommonHotelDetails> Hotels { get; set; }
        public void GetExpediaHotelList()
        {
            Hotels = new List<CommonHotelDetails>();
            string[] array_input = Session.Split('_');

            string[] array_inputt = array_input[1].Split(',');

            var ExpediaHotelList = (from obj in db.tbl_ExpActivePropertyLists 
                                    join objLocation in db.Exp_PropertyLocationLists on obj.EANHotelID equals
                                    objLocation.EANHotelID where obj.City == array_inputt[0]
                                    select new
                                    {
                                        obj.Name,
                                        obj.Location,
                                        obj.StarRating,
                                        obj.EANHotelID,
                                        obj.Longitude,
                                        obj.Address1,
                                        obj.Address2,
                                        obj.Latitude,
                                        obj.SupplierType,
                                        
                                    }).ToList();
                                   
                                   
            foreach (var objHotel in ExpediaHotelList)
            {
                //string Facility="";
                string Address = "";
                if (objHotel.Address1!= null)
                    Address += objHotel.Address1;
                if (objHotel.Address2 != null)
                    Address += " ," + objHotel.Address2;
              
                Hotels.Add(new CommonHotelDetails
                {
                    HotelName = objHotel.Name,
                    Category = objHotel.StarRating.ToString(),
                    // Currency = "INR",
                    DateFrom = "",
                    DotwCode = "",
                    HotelBedsCode = objHotel.EANHotelID.ToString(),
                    MGHCode = "",
                    GRNCode = "",
                    ExpediaCode = "",
                    Description = GetDescription(objHotel.EANHotelID.ToString()),
                    HotelId = objHotel.EANHotelID.ToString(),
                    Location = GetLocations(objHotel.EANHotelID.ToString()),
                    Address = Address,
                    Facility = GetFacility(objHotel.EANHotelID.ToString()),
                    Image = GetImage(objHotel.EANHotelID.ToString()),
                    Langitude = objHotel.Longitude.ToString(),
                    Latitude = objHotel.Latitude.ToString(),
                   // GTACode = "GTA",
                    Supplier = "Expedia",
                });
            }
        }
        public List<Location> GetLocations(string EANHotelID)
        {
            //List<tbl_CommonHotelMaster> Locations = new List<tbl_CommonHotelMaster>();
            List<Location> Locations = new List<Location>();
            var ExpLocationList = (from obj in db.Exp_PropertyLocationLists where obj.EANHotelID == (double)Convert.ToDecimal(EANHotelID) select obj).ToList();
            foreach (var objLocation in ExpLocationList)
            {
                Locations.Add(new Location { HotelCode = EANHotelID, Name = objLocation.PropertyLocationDescription, Supplier = "Expedia", Count = 0, });
            }
            return Locations;
        }
        public List<String> GetFacility(string facilities)
        {
            List<String> Facilities = new List<String>();
            //foreach (var objFacility in facilities.Split(','))
            //{
            //    Facilities.Add(objFacility);
            //}
            return Facilities;
        }
        public List<Image> GetImage(string EANHotelID)
        {
            List<Image> Images = new List<Image>();
            var ExpImage = (from obj in db.tbl_ExpHotelImageLists where obj.EANHotelID == (double)Convert.ToDecimal(EANHotelID)  select obj).ToList();
            foreach (var objImage in ExpImage)
            {
                Images.Add(new Image { Title = objImage.Caption, Url = objImage.URL, Count = 0});
            }
            return Images;
        }
        public String GetDescription(string EANHotelID)
        {
            String Descriptions = String.Empty;
            try
            {
                Descriptions = (from obj in db.tbl_ExpPropertyDescriptionLists
                                where (obj.EANHotelID == (double)Convert.ToDecimal(EANHotelID))
                                select obj.PropertyDescription).FirstOrDefault();

                if (Descriptions == null)
                    Descriptions = "";
                return Descriptions;
            }
            catch
            {

            }

            return Descriptions;
        }

    }

}