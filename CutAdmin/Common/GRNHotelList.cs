﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.dbml;
namespace CutAdmin.Common
{
    public class GRNHotelList
    {
        helperDataContext db = new helperDataContext();
        public string Session { get; set; }
        public List<CommonLib.Response.CommonHotelDetails> Hotels { get; set; }
        public void GetGRNHotels()
        {
            Hotels = new List<CommonHotelDetails>();
            string[] array_input = Session.Split('_');
            List<tbl_GRN_Country> CountryList = new List<tbl_GRN_Country>();
            CountryList = (from Country in db.tbl_GRN_Countries select Country).ToList();
            string[] SplitCountry = array_input[1].Split(',');
            string CName = SplitCountry[1].Trim();
            string CountryCode = CountryList.Single(x => x.CountryName == CName).Country2Code;
            var GRNHotelList =
                (from obj in db.tbl_GRN_Hotels
                 join objCity in db.tbl_GRN_Cities on
                 obj.city_code equals objCity.Citycode 
                 join  objCountry in db.tbl_GRN_Countries on
                 objCity.Countrycode equals objCountry.Country2Code
                 where objCountry.Country2Code == CountryCode &&
                 objCity.Cityname.Contains(array_input[1].Split(',')[0])
                 select new {
                     obj.name,
                     obj.category,
                     obj.description,
                     obj.code,
                     obj.facilities,
                     obj.longitude,
                     obj.latitude,
                     obj.address
                 }).ToList();
           // var GRNHotelList = (from obj in db.tbl_GRN_Hotels where obj.city_code == array_input[0] select obj).ToList();
              foreach (var objGRNHotel in GRNHotelList)
            {
                string Facility="";
                string address = "";
                string CategoryChek;
                if (objGRNHotel.address != null)
                    address += objGRNHotel.address;
                if (objGRNHotel.category != "")
                    CategoryChek = objGRNHotel.category;
                else
                    CategoryChek = "0";
                Hotels.Add(new CommonHotelDetails
                {
                    HotelName = objGRNHotel.name,
                    Category = CategoryChek,
                     Currency = "INR",
                    DateFrom = "",
                    DotwCode = "",
                    HotelBedsCode = "",
                    MGHCode = "",
                    GRNCode = "GRN",
                    ExpediaCode = "",
                    Description = objGRNHotel.description,
                    HotelId = objGRNHotel.code,
                    Location = new List<Location>(),
                    Address = objGRNHotel.address,
                    Facility = GetFacility(objGRNHotel.facilities),
                    Image = new List<Image>(),
                    Langitude = objGRNHotel.longitude,
                    Latitude =  objGRNHotel.latitude,
                    Supplier = "GRN",
                });
            }
        }
        public List<String> GetFacility(string facilities)
        {
            List<String> Facilities = new List<String>();
            foreach (var objFacility in facilities.Split(','))
            {
                Facilities.Add(objFacility);
            }
            return Facilities;
        }
        }
        //public List<Location> GetLocations(string HotelId)
        //{
        //    List<tbl_CommonHotelMaster> Locations = new List<tbl_CommonHotelMaster>();
        //    List<Location> Locations = new List<Location>();
        //    var GtaLocationList = (from obj in db.tbl_GTANearByLocations where obj.HotelID == HotelId select obj).ToList();
        //    foreach (var objLocation in GtaLocationList)
        //    {
        //        Locations.Add(new Location { HotelCode = HotelId, Name = objLocation.Location, Supplier = "GTA", Count = 0, });
        //    }
        //    return Locations;
        //}

        //public List<Image> GetImage(string HotelId)
        //{
        //    List<Image> Images = new List<Image>();
        //    var GtaImage = (from obj in db.tbl_GTAHotelImages where obj.HotelID == HotelId select obj).ToList();
        //    foreach (var objImage in GtaImage)
        //    {
        //        Images.Add(new Image { Title = objImage.Title, Url = objImage.Path, Count = 0, });
        //    }
        //    return Images;
        //}
        //public String GetDescription(string HotelId)
        //{
        //    String Descriptions = String.Empty;
        //    try
        //    {
        //        Descriptions = (from obj in db.tbl_GTAReports
        //                        where (obj.HotelID == HotelId && obj.ReportType == "general")
        //                        select obj.ReportText).FirstOrDefault();

        //        if (Descriptions == null)
        //            Descriptions = "";
        //        return Descriptions;
        //    }
        //    catch
        //    {

        //    }

        //    return Descriptions;
        //}

 //   }

}



