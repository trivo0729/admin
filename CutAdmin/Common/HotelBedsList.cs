﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.dbml;
namespace CutAdmin.Common
{
    public class HotelBedsList
    {
        helperDataContext db = new helperDataContext();
        public string Session { get; set; }
        public List<CommonLib.Response.CommonHotelDetails> Hotels { get; set; }
        public void GetHotelBedsList()
        {
            Hotels = new List<CommonHotelDetails>();
            string[] array_input = Session.Split('_');
            var HotelBedsList = (from obj in db.HOTELs
                                 join objContact in db.CONTACTs on obj.HotelCode equals objContact.HotelCode
                                 where obj.DestinationCode == array_input[0]
                                 select new {
                                      obj.Name,
                                    obj.CategoryCode,
                                    HOTEL_DESCRIPTIONs="",
                                    obj.HotelCode,
                                    obj.Longitude,
                                    obj.Latitude,
                                    obj.CONTACT,
                                    obj.HOTEL_IMAGEs,
                                    

                                 }).ToList();
            foreach (var objHotel in HotelBedsList)
            {
               
                Hotels.Add(new CommonHotelDetails
                {
                    HotelName = objHotel.Name,
                    Category = objHotel.CategoryCode,
                    Currency = "INR",
                    DateFrom = "",
                    DotwCode = "",
                    HotelBedsCode = objHotel.HotelCode,
                    MGHCode = "",
                    GRNCode = "",
                    ExpediaCode = "",
                    Description = GetDescription(objHotel.HotelCode).ToString(),
                    HotelId = objHotel.HotelCode,
                    Location = new List<Location>(),
                    Address = objHotel.CONTACT.Address,
                    Facility = GetFacility(objHotel.HotelCode),
                    Image = GetImage(objHotel.HotelCode),
                    Langitude = objHotel.Longitude.ToString(),
                    Latitude = objHotel.Latitude.ToString(),
                   // GTACode = "",
                    Supplier = "HotelBeds",
                });
            }
        }
        //public List<Location> GetLocations(string HotelCode)
        //{
        //    List<tbl_CommonHotelMaster> Locations = new List<tbl_CommonHotelMaster>();
        //    List<Location> Locations = new List<Location>();
        //    var GtaLocationList = (from obj in db.tbl_GTANearByLocations where obj.HotelID == HotelId select obj).ToList();
        //    foreach (var objLocation in GtaLocationList)
        //    {
        //        Locations.Add(new Location { HotelCode = HotelId, Name = objLocation.Location, Supplier = "GTA", Count = 0, });
        //    }
        //    return Locations;
        //}
        public List<String> GetFacility(string facilities)
        {
            List<String> Facilities = new List<String>();
            //foreach (var objFacility in facilities.Split(','))
            //{
            //    Facilities.Add(objFacility);
            //}
            return Facilities;
        }
        public List<Image> GetImage(string HotelCode)
        {
            List<Image> Images = new List<Image>();
            var HotelBedsImage = (from obj in db.HOTEL_IMAGEs where obj.HotelCode == HotelCode select obj).ToList();
            foreach (var objImage in HotelBedsImage)
            {
                Images.Add(new Image {  Url = objImage.ImagePath, Count = 0, });
            }
            return Images;
        }
        public String GetDescription(string HotelCode)
        {
            String Descriptions = String.Empty;
            try
            {
                Descriptions = (from obj in db.HOTEL_DESCRIPTIONs
                                where obj.HotelCode == HotelCode && obj.LanguageCode=="ENG"
                                select obj.HotelFacilities).FirstOrDefault();

                if (Descriptions == null)
                    Descriptions = "";
                return Descriptions;
            }
            catch
            {

            }

            return Descriptions;
        }

    }

}
