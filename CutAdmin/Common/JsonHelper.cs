﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using GRNLib.Request.Availability;
using GRNLib.Request.Booking;
using CutAdmin.Models;
using CutAdmin.DataLayer;
using GRNLib.Request;
using CutAdmin.BL;
using CutAdmin.dbml;
namespace CutAdmin.Common
{
    public class JsonHelper
    {
        public bool isResponse { get; set; }
        public string session { get; set; }
        public GRNStatusCode objStatusCode { get; set; }
        public GlobalDefault objGlobalDefault { get; set; }
        public Models.GRNHotels objHotel { get; set; }
        public static HttpContext Context { get; set; }
        public System.Globalization.CultureInfo CultureInfo { get; set; }
        public AgentDetailsOnAdmin objAgentDetailsOnAdmin { get; set; }

        public void AvailReq()
        {
            try
            {
                //ParseGRNResponse ObjParseGRNResponse = new ParseGRNResponse();

                //List<int> ChildAge = new List<int>();
                //helperDataContext db = new helperDataContext();
                //GRNLib.Request.Availability.GRNAvailReq ObjGRNAvailReq = new GRNLib.Request.Availability.GRNAvailReq();
                //GRNLib.Request.Availability.AvailRequest ObjAvailRequest = new GRNLib.Request.Availability.AvailRequest();
                //Models.GRNAvailRequest objAvail = new Models.GRNAvailRequest();
                //List<Occupancy> list_Occupancy = new List<Occupancy>();
                //string[] array_input = session.Split('_');
                //DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
                //DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));                
                //ObjGRNAvailReq.checkin = fDate.ToString("yyyy-MM-dd");
                //objAvail.CheckIn = fDate.ToString("dd/MM/yyyy");
                //ObjGRNAvailReq.checkout = tDate.ToString("yyyy-MM-dd");
                //objAvail.CheckOut = tDate.ToString("dd/MM/yyyy");
               
                //string occpancy = array_input[5].ToString();
                //objAvail.DestinationCode = array_input[0].ToString();
                //ObjGRNAvailReq.hotelname = array_input[7].ToString();
                //try
                //{
                //    objAvail.HotelCode = array_input[6].ToString();
                //    objAvail.HotelName = array_input[7].ToString();
                //    objAvail.StarRating = array_input[8].ToString();
                //    objAvail.NationalityCode = array_input[9].ToString();
                //}
                //catch { }
                ///*string[] array_occupancy = occpancy.Split('$');
                //int adult = 0;
                //int child = 0;
                //foreach (string str in array_occupancy)
                //{
                //    string[] m_array = str.Split('|');
                //    adult = adult + Convert.ToInt32(m_array[0]);
                //    string[] n_array = m_array[1].ToString().Split('^');
                //    child = child + Convert.ToInt32(n_array[0]);
                //    List<Customer> list_Customer = new List<Customer>();
                //    for (int i = 0; i < n_array.Length; i++)
                //    {
                //        if (i > 0)
                //        {
                //            //...............................under test by maqsood...........................................//
                //            int Age = Convert.ToInt32(n_array[i]);
                //            //...............................under test by maqsood...........................................//
                //            if (Age > 0)
                //                list_Customer.Add(new Customer { Age = Age, type = "CH" });
                //        }
                //    }
                //    list_Occupancy.Add(new Occupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
                //}*/
                //// Start Getting Nationality
                //List<tbl_GRN_Country> CountryList = new List<tbl_GRN_Country>();
                //CountryList = (from Country in DB.tbl_GRN_Countries select Country).ToList();
                //string Nationality = CountryList.Single(s => s.Country3Code == array_input[9].ToString()).Country2Code;
                ////  End Getting Nationality

                ////  Start Getting Destination Code
                //string[] SplitCountry = array_input[1].Split(',');
                //string CName = SplitCountry[1].Trim();
                //string CountryCode = CountryList.Single(x => x.CountryName == CName).Country2Code;
                //var DestinationCode = (from Dest in db.tbl_grn where Dest.Country_code == CountryCode && Dest.Destination_name == SplitCountry[0] select Dest).ToList();
                ////   END Getting Destination Code

                //ObjGRNAvailReq.destination_code = DestinationCode[0].Destination_Code;
                //ObjGRNAvailReq.client_nationality = Nationality;

                //ObjGRNAvailReq.response = "full";
                //ObjGRNAvailReq.currency = "INR";
                //ObjGRNAvailReq.rates = "Concise";

                //#region Getting Adults & Childs
                //GRNLib.Request.Availability.Room ObjRoom = new GRNLib.Request.Availability.Room();
                //ObjGRNAvailReq.rooms = new List<GRNLib.Request.Availability.Room>();
                //string occpancys = array_input[5].ToString();
                //string[] array_occupancy = occpancy.Split('$');
                //int adult = 0;
                //int child = 0;
                //foreach (string str in array_occupancy)
                //{
                //    string[] m_array = str.Split('|');
                //    ObjRoom = new GRNLib.Request.Availability.Room();
                //    adult = adult + Convert.ToInt32(m_array[0]);
                //    ObjRoom.adults = Convert.ToInt32(m_array[0]);
                //    string[] n_array = m_array[1].ToString().Split('^');
                //    child = child + Convert.ToInt32(n_array[0]);
                //    List<Customer> list_Customer = new List<Customer>();
                //    List<int> ChilAge = new List<int>();
                //    for (int i = 0; i < n_array.Length; i++)
                //    {
                //        if (i > 0)
                //        {
                //            int Age = Convert.ToInt32(n_array[i]);
                //            if (Age > 0)
                //            {
                //                list_Customer.Add(new Customer { Age = Age, type = "CH" });
                //                ChilAge.Add(Age);
                //            }
                //        }
                //    }
                //    list_Occupancy.Add(new Occupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
                //    ObjRoom.children_ages = ChilAge;
                //    ObjGRNAvailReq.rooms.Add(ObjRoom);
                //}
                //#endregion

                //ObjAvailRequest.request = ObjGRNAvailReq;
                //string JsonRequest = ObjAvailRequest.GenerateJson();
                //int Status = 0;
                //string RequestHeader;
                //string m_response; ;
                //string ResponseHeader;
                //string TypeOfRequest = "availability";
                //isResponse = ObjAvailRequest.Post(JsonRequest, TypeOfRequest, out  m_response, out  RequestHeader, out  ResponseHeader, out Status);
                //objAvail.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                //objStatusCode = new GRNStatusCode { Night = objAvail.Night, Request = JsonRequest, Response = m_response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objAvail, FDate = fDate, TDate = tDate, Occupancy =  list_Occupancy, NoofRoom = Convert.ToInt16(array_input[4]) };
                //GetHotellist();
                ////HotelBooking();
                ////ImageRequest("H!0020739");
            }
            catch (Exception ex)
            {
                objStatusCode = null;
                isResponse = false;
            }

        }


        #region Parse list

        public void GetHotellist()
        {
            GRNLib.Response.HotelList ObjHotelListRresponse = new GRNLib.Response.HotelList();
            ParseGRNResponse ObjGRNResponse = new ParseGRNResponse();
            ObjGRNResponse.Context = Context;
            isResponse = ObjGRNResponse.ParseJson(objStatusCode, out ObjHotelListRresponse);
            float min_price, max_price;
            Int32 m_counthotel;
            //List<EANLib.Response.HotelSummary> List_EANHotels;
            //EANLib.Response.HotelListResponse objHotelListResponse = new EANLib.Response.HotelListResponse();
            //objEANResponse.Context = Context;
            //bResponse = objEANResponse.ParseXML(objStatusCode.Response, objStatusCode, out objHotelListResponse);
            //List_EANHotels = objHotelListResponse.HotelList.HotelSummary;
            min_price = Convert.ToSingle(ObjHotelListRresponse.hotels.OrderBy(data => data.MaxCutPrice).Select(data => data.MaxCutPrice).FirstOrDefault());
            max_price = Convert.ToSingle(ObjHotelListRresponse.hotels.OrderByDescending(data => data.MaxCutPrice).Select(data => data.MaxCutPrice).FirstOrDefault());
            m_counthotel = ObjHotelListRresponse.hotels.Count;
            //List<string> ValueAds = ObjHotelListRresponse.FacilitiesList;
            //objEANHotels.Facility = ValueAds;
            GRNAvailRequest ObjGRNAvailRequest = new GRNAvailRequest();
            ObjGRNAvailRequest.Adult = ObjHotelListRresponse.no_of_adults;
            ObjGRNAvailRequest.CheckIn = ObjHotelListRresponse.checkin;
            ObjGRNAvailRequest.CheckOut = ObjHotelListRresponse.checkout;
            ObjGRNAvailRequest.Child = ObjHotelListRresponse.no_of_children;
            ObjGRNAvailRequest.Night = ObjHotelListRresponse.no_of_nights;
            ObjGRNAvailRequest.Room = ObjHotelListRresponse.no_of_rooms;
            objHotel = new Models.GRNHotels { MinPrice = min_price, MaxPrice = max_price, Facility = ObjHotelListRresponse.FacilitiesList, CountHotel = m_counthotel, HotelDetail = ObjHotelListRresponse, FromDate = objStatusCode.FDate.ToString(), ToDate = objStatusCode.TDate.ToString(), Occupancy = objStatusCode.Occupancy, noNights = objStatusCode.Night, noRoomRequest = objStatusCode.NoofRoom, DisplayRequest = ObjGRNAvailRequest };
            //Session["EanAvail"] = objEANHotels;
        }

        public void HotelBooking()
        {
            try
            {
                //GRNLib.Request.Availability.AvailRequest ObjAvailRequest = new GRNLib.Request.Availability.AvailRequest();
                bool isResponse;
                //GRNLib.Common.WebClient WebClient = new GRNLib.Common.WebClient();
                string TypeOfRequest = "bookings";
                int Status = 0;
                string RequestHeader;
                string m_response; ;
                string ResponseHeader;
                GRNLib.Request.Booking.HotelBookingReq ObjHotelBooking = new GRNLib.Request.Booking.HotelBookingReq();
                //ObjHotelBooking.search_id = objHotel.HotelDetail.search_id;
                ObjHotelBooking.search_id = "ut26snngfutjqtiph3tb47wpgm";
                ObjHotelBooking.hotel_code = "H!0068507";
                ObjHotelBooking.city_code = "C!000968";
                ObjHotelBooking.group_code = "xgirbylkufug7bk3v2ngoh6twwda";
                ObjHotelBooking.checkin = "2018-01-30";
                ObjHotelBooking.checkout = "2018-01-31";
                ObjHotelBooking.booking_comments = "Testing";
                ObjHotelBooking.payment_type = "AT_WEB";
                //ObjHotelBooking.hotel_code = "0049396";jx36yhni2qfsp47deerl5aercu
                #region Booking Item
                ObjHotelBooking.booking_items = new List<BookingItem>();
                for (int i = 0; i < 2; i++)
                {
                    BookingItem ObjBookingItem = new BookingItem();
                    ObjBookingItem.room_code = "4phvpkrr4urcplzuyy";
                    ObjBookingItem.rate_key = "4dhfnmldwzdsrx2qvsjwkho74tik7qna7pno7pl2wrdhp7ufck6h52htficc527b6jjttbgqzvufiqjw5oywq5oots7wjmoc37tcdrue42mw7azdzjrdkiismq3iqprcw3bwbtzpmqmx6mgd6ht34mhzwta6i7hhkcfbn3277vdy5wnlkuscvvarypra";
                    #region Room
                    ObjBookingItem.rooms = new List<GRNLib.Request.Booking.Room>();
                    for (int j = 0; j < 1; j++)
                    {
                        GRNLib.Request.Booking.Room ObjRoom = new GRNLib.Request.Booking.Room();
                        #region PAX
                        ObjRoom.paxes = new List<Pax>();
                        for (int k = 0; k < 3; k++)
                        {
                            Pax ObjPax = new Pax();
                            ObjPax.title = "Mr";
                            ObjPax.name = "Henry" + k;
                            ObjPax.surname = "Patrick" + k;
                            ObjPax.type = "AD";
                            ObjPax.age = 25;
                            ObjRoom.paxes.Add(ObjPax);
                        }
                        #endregion

                        ObjBookingItem.rooms.Add(ObjRoom);
                    }
                    #endregion

                    ObjHotelBooking.booking_items.Add(ObjBookingItem);
                }
                #endregion
                ObjHotelBooking.holder = new Holder();
                ObjHotelBooking.holder.title = "Mr.";
                ObjHotelBooking.holder.name = "James";
                ObjHotelBooking.holder.surname = "Patrick";
                ObjHotelBooking.holder.email = "a@a.com";
                ObjHotelBooking.holder.phone_number = "0123456789";
                ObjHotelBooking.holder.client_nationality = "FR";
                HotelBookingRequest ObjHotelBook = new HotelBookingRequest();
                ObjHotelBook.request = ObjHotelBooking;
                string JsonRequest = ObjHotelBook.GenerateJson();
                isResponse = ObjHotelBook.Post(JsonRequest, TypeOfRequest, out  m_response, out  RequestHeader, out  ResponseHeader, out Status);
                ParseGRNResponse ObjGRNResponse = new ParseGRNResponse();
                GRNLib.Response.Booking.BookingResponse ObjBookingResponse = new GRNLib.Response.Booking.BookingResponse();
                bool isBook = false;
                if (isResponse)
                    isBook = ObjGRNResponse.BookingResponse(m_response, out ObjBookingResponse);
            }
            catch (Exception ex)
            {
                string LineNum = ex.StackTrace;
            }
        }

        public void Cancellation()
        {
            try
            {
                bool isResponse;
                int Status = 0;
                string RequestHeader;
                string m_response; ;
                string ResponseHeader;
                CancellBooking ObjCancellBooking = new CancellBooking();
                ObjCancellBooking.comments = "";
                string JsonReq = ObjCancellBooking.GenerateJson();
                string TypeOfRequest = "bookings/<bref>";
                isResponse = ObjCancellBooking.Post(JsonReq, TypeOfRequest, out  m_response, out  RequestHeader, out  ResponseHeader, out Status);
            }
            catch (Exception ex)
            {
                string LineNo = ex.StackTrace.ToString();
            }
        }
        #endregion

        public GRNLib.Response.ImagesRes.ImageList ImageRequest(string HotelCode)
        {
            ParseGRNResponse ObjGRNResponse = new ParseGRNResponse();
            GRNLib.Response.ImagesRes.ImageList ObjImageList = new GRNLib.Response.ImagesRes.ImageList();
            ImageRequest ObjImageRequest = new ImageRequest();
            bool IsResponse;
            int Status = 0;
            string m_response;
            string TypeOfRequest = HotelCode + "/images";
            IsResponse = ObjImageRequest.GetImages(TypeOfRequest, out m_response, out Status);
            if (IsResponse)
            {
                IsResponse = ObjGRNResponse.ImagesReseponse(m_response, out ObjImageList);
            }
            return ObjImageList;

        }
    }
}