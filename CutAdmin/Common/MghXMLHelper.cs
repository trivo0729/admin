﻿using CutAdmin.DataLayer;
using MGHLib.Request;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Common
{
    public class MghXMLHelper
    {
        public bool Response { get; set; }
        public string session { get; set; }
        public Models.MGHStatusCode objStatusCode { get; set; }
        public static HttpContext context { get; set; }
        public Models.MGHHotel objMGHHotel { get; set; }
        public AgentDetailsOnAdmin objAgentDetailsOnAdmin { get; set; }
        // public static bool AvailRequest(string session, out CUT.Models.MGHStatusCode objStatusCode)
        public void AvailRequest()
        {
            try
            {

                GlobalDefault objGlobalDefault = (GlobalDefault)context.Session["LoginUser"];
                SearchRequest objSearchRequest = new SearchRequest();
                Models.MGHAvailRequest objMGHAvail = new Models.MGHAvailRequest();
                List<HotelOccupancy> list_Occupancy = new List<HotelOccupancy>();
                string[] array_input = session.Split('_');
                objSearchRequest.EchoToken = Guid.NewGuid().ToString().Replace("-", "");
                objSearchRequest.Language = "ENG";
                objSearchRequest.PageNumber = 1;
                objSearchRequest.SessionID = Common.ToUnixTimestamp(DateTime.Now).ToString();
                objSearchRequest.UserName = System.Configuration.ConfigurationManager.AppSettings["MGHAPIURL"];
                objSearchRequest.Password = System.Configuration.ConfigurationManager.AppSettings["MGHAPIToken"];
                //objSearchRequest.DestinationCode = array_input[0].ToString();
                string[] ArrLocationName = array_input[1].Split(',');
                string LocationName = ArrLocationName[0].ToString().Replace(',', ' ');
                DataTable dtResult;
                MGHManager.GetLocationCode(LocationName.Trim(' '), "Eng", out dtResult);
                objSearchRequest.DestinationCode = dtResult.Rows[0]["id"].ToString();
                objMGHAvail.Location = array_input[1].ToString();
                objMGHAvail.HotelName = array_input[7].ToString();
                objSearchRequest.DestinationType = "SIMPLE";
                DateTime fDate = Convert.ToDateTime(array_input[2].ToString(), new System.Globalization.CultureInfo("en-GB"));
                DateTime tDate = Convert.ToDateTime(array_input[3].ToString(), new System.Globalization.CultureInfo("en-GB"));
                //objSearchRequest.CheckInDate = fDate.ToString("yyyyMMdd");
                objSearchRequest.CheckInDate = fDate.ToString("yyyy-MM-dd");
                objMGHAvail.CheckIn = fDate.ToString("dd/MM/yyyy");
                //objSearchRequest.CheckOutDate = tDate.ToString("yyyyMMdd");
                objSearchRequest.CheckOutDate = tDate.ToString("yyyy-MM-dd");
                objMGHAvail.CheckOut = tDate.ToString("dd/MM/yyyy");
                //objSearchRequest.Nationality = array_input[9];
                string occpancy = array_input[5].ToString();
                objMGHAvail.DestinationCode = array_input[0].ToString();
                try
                {
                    objMGHAvail.HotelCode = array_input[6].ToString();
                    objMGHAvail.HotelName = array_input[7].ToString();
                    objMGHAvail.StarRating = array_input[8].ToString();
                    objMGHAvail.NationalityCode = array_input[9].ToString();
                }
                catch { }
                //objAvail.NationalityCountry = array_input[10].ToString();
                string[] array_occupancy = occpancy.Split('$');
                int adult = 0;
                int child = 0;
                foreach (string str in array_occupancy)
                {
                    string[] m_array = str.Split('|');
                    adult = adult + Convert.ToInt32(m_array[0]);
                    string[] n_array = m_array[1].ToString().Split('^');
                    child = child + Convert.ToInt32(n_array[0]);
                    List<Customer> list_Customer = new List<Customer>();
                    for (int i = 0; i < n_array.Length; i++)
                    {
                        if (i > 0)
                        {
                            //...............................under test by maqsood...........................................//
                            int Age = Convert.ToInt32(n_array[i]);
                            //...............................under test by maqsood...........................................//
                            if (Age > 0)
                                list_Customer.Add(new Customer { Age = Age, type = "CH" });
                        }
                    }
                    list_Occupancy.Add(new HotelOccupancy { RoomCount = 1, AdultCount = Convert.ToInt32(m_array[0]), ChildCount = Convert.ToInt32(n_array[0]), GuestList = list_Customer });
                }
                //...............................under test by maqsood...........................................//
                //List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
                //foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
                //{
                //    if (!DISTINST_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                //        DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
                //}
                List<Occupancy> DISTINST_OCCUPANCY = new List<Occupancy>();
                foreach (HotelOccupancy Check_Duplicate_Occupancy in list_Occupancy)
                {
                    //if (!DISTINST_OCCUPANCY.Exists(data => data.RoomCount == Check_Duplicate_Occupancy.RoomCount && data.AdultCount == Check_Duplicate_Occupancy.AdultCount && data.ChildCount == Check_Duplicate_Occupancy.ChildCount))
                    DISTINST_OCCUPANCY.Add(new Occupancy { RoomCount = Check_Duplicate_Occupancy.RoomCount, AdultCount = Check_Duplicate_Occupancy.AdultCount, ChildCount = Check_Duplicate_Occupancy.ChildCount });
                }
                //...............................under test by maqsood...........................................//
                List<HotelOccupancy> LIST_FINAL_OCCUPANCY = new List<HotelOccupancy>();
                //int k = 0;
                foreach (Occupancy objOccupancy in DISTINST_OCCUPANCY)
                {
                    var Occupancy_List = list_Occupancy.Where(data => data.AdultCount == objOccupancy.AdultCount && data.ChildCount == objOccupancy.ChildCount).ToList();
                    int RoomCount = 0;
                    int AdultCount = 0;
                    int ChildCount = 0;
                    List<Customer> ListCustomer = new List<Customer>();
                    foreach (HotelOccupancy objHotelOccupancy in Occupancy_List)
                    {
                        RoomCount = RoomCount + objHotelOccupancy.RoomCount;
                        ChildCount = objHotelOccupancy.ChildCount;
                        AdultCount = objHotelOccupancy.AdultCount;
                        foreach (Customer objCustomer in objHotelOccupancy.GuestList)
                        {
                            ListCustomer.Add(objCustomer);
                        }
                    }
                    LIST_FINAL_OCCUPANCY.Add(new HotelOccupancy { RoomCount = RoomCount, AdultCount = AdultCount, ChildCount = ChildCount, GuestList = ListCustomer });
                }
                objMGHAvail.Room = list_Occupancy.Sum(data => data.RoomCount);
                objMGHAvail.Adult = list_Occupancy.Sum(data => data.AdultCount);
                objMGHAvail.Child = list_Occupancy.Sum(data => data.ChildCount);
                objMGHAvail.List_Occupancy = LIST_FINAL_OCCUPANCY;
                string HotelCode = array_input[6].ToString();
                objSearchRequest.HotelOccupancy = LIST_FINAL_OCCUPANCY;
                if (!String.IsNullOrEmpty(HotelCode))
                    objSearchRequest.sHotelCodeList = new HotelCodeList { withinResults = "Y", ProductCode = HotelCode };

                string xml = objSearchRequest.GenerateRequest(session);
                string m_response = "";
                string RequestHeader = "";
                string ResponseHeader = "";
                int Status = 0;
                List<Occupancy> m_List_Hotel_Occupancy = new List<Occupancy>();
                foreach (HotelOccupancy objOccupancy in LIST_FINAL_OCCUPANCY)
                {
                    m_List_Hotel_Occupancy.Add(new Occupancy { RoomCount = objOccupancy.RoomCount, AdultCount = objOccupancy.AdultCount, ChildCount = objOccupancy.ChildCount });
                }
                //string txt = System.IO.File.ReadAllText(@"E:\_Projects_\test cut\DelhiLatest.xml");
                //string txt = System.IO.File.ReadAllText(@"E:\_Projects_\test cut\D_test0.xml");

                /*.... to comment/uncomment ..Start..*/
                //string txt = System.IO.File.ReadAllText(@"C:\Users\USER\Desktop\HotelBeds_Latest_Madrid\HotelValuedAvailRS.xml");
                //m_response = txt;
                //bool bResponse = true;
                //Status = 1;
                /*.... to comment/uncomment ..End..*/

                //string txt = System.IO.File.ReadAllText("C:\\Users\\Ayush\\Desktop\\ResponseBody25.xml");
                //bool bResponse = true;
                //objStatusCode = new Models.StatusCode { Request = xml, Response = txt, RequestHeader = "", ResponseHeader = "", Status = 2, DisplayRequest = objAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };

                /*To be comment/uncomment*/
                bool bResponse = objSearchRequest.Post(xml, out m_response, out Status);
                /*.......................*/
                objMGHAvail.Night = Convert.ToInt32((tDate - fDate).TotalDays);
                objStatusCode = new Models.MGHStatusCode { Request = xml, Response = m_response, RequestHeader = RequestHeader, ResponseHeader = ResponseHeader, Status = Status, DisplayRequest = objMGHAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
                //CUT.Common.Common.Session(out objGlobalDefault);
                //LogManager.Add(0, objStatusCode.RequestHeader, objStatusCode.Request, objStatusCode.ResponseHeader, objStatusCode.Response, objGlobalDefault.sid, objStatusCode.Status);
                GelHotelList();
                Response = true;
                //return bResponse;
            }
            catch
            {
                objStatusCode = null;
                Response = false;
                //return false;
            }

        }

        public void GelHotelList()
        {
            int count;
            float min_price;
            float max_price;
            int m_counthotel;
            count = objStatusCode.DisplayRequest.Night;
            ParseMGHResponse objParseMGHResponse = new ParseMGHResponse();
            objParseMGHResponse.context = context;
            objParseMGHResponse.Nationality = objStatusCode.DisplayRequest.NationalityCode;
            //bool bParse = objParseMGHResponse.ParseXML(objMGHStatusCode.Response);
            int RoomCount = objStatusCode.DisplayRequest.Room;
            //bool bParse = objParseMGHResponse.ParseXML(objStatusCode.Response, count, RoomCount);
        
            List<MGHLib.Response.MGHHotelDetails> List_MGHHotelDetails = objParseMGHResponse.GetServiceHotel();
            min_price = List_MGHHotelDetails.OrderBy(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
            max_price = List_MGHHotelDetails.OrderByDescending(data => data.CUTPrice).Select(data => data.CUTPrice).FirstOrDefault();
            m_counthotel = List_MGHHotelDetails.Where(data => data.CUTPrice == min_price).Count();
            objMGHHotel = new Models.MGHHotel { MinPrice = min_price, MaxPrice = max_price, Facility = objParseMGHResponse.Facility, HotelDetail = List_MGHHotelDetails, CountHotel = m_counthotel, Location = objParseMGHResponse.Location, Category = objParseMGHResponse.Category, DisplayRequest = objStatusCode.DisplayRequest };

        }

    }
}