﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using HotelLib.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Xml;using CutAdmin.dbml;

namespace CutAdmin.Common
{
    public class ParseAvailResponse
    {
        XmlDocument objDocument = new XmlDocument();
        XmlElement Element_HotelValuedAvailRS;
        List<HotelLib.Response.Facility> List_Facility = new List<HotelLib.Response.Facility>();
        List<HotelLib.Response.Location> List_Location = new List<HotelLib.Response.Location>();
        List<HotelLib.Response.Category> List_Category = new List<HotelLib.Response.Category>();
        List<String> List_Currency = new List<String>();
        List<String> List_Hotel = new List<String>();
        int _PriceCount = 0;
        int m_RoomCount = 0;
        float MarkupPercentage = 0;
        float m_price = 0;
        float dolarRate = 0;
        string code = "";
        string checkInDate = String.Empty;
        Int64 Night;
        Int64 NoofRooms;
        List<HotelLib.Request.Occupancy> List_RequestOccupancy = new List<HotelLib.Request.Occupancy>();
        List<HotelLib.Request.Occupancy> List_FRequestOccupancy = new List<HotelLib.Request.Occupancy>();
        public ParseAvailResponse(string m_xml, int m_pricelist, Int64 UserID, Int32 RoomCount, Int64 noofNight, List<HotelLib.Request.Occupancy> m_List_RequestOccupancy, List<HotelLib.Request.Occupancy> m_ListF_RequestOccupancy)
        {
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(m_xml);
            Night = noofNight;
            NoofRooms = RoomCount;
            MemoryStream ms = new MemoryStream(buffer);
            using (XmlTextReader reader = new XmlTextReader(ms))
            {
                objDocument.Load(reader);
            }
            ms.Close();
            ms.Dispose();
            Element_HotelValuedAvailRS = objDocument.DocumentElement;
            _PriceCount = m_pricelist;
            m_RoomCount = RoomCount;
            DataTable dtResult = new DataTable();
            if (DataLayer.HotelManager.GetMarkupDetail("HotelBeds", UserID, out dtResult) == BL.DBHelper.DBReturnCode.SUCCESS)
            {
                MarkupPercentage = float.Parse(dtResult.Rows[0]["MarkUpPercentage"].ToString());
            }
            List_RequestOccupancy = m_List_RequestOccupancy;
            List_FRequestOccupancy = m_ListF_RequestOccupancy;
        }

        public List<HotelDetail> GetServiceHotel(HttpContext context)
        {
            XmlNodeList Xml_ServiceHotel = Element_HotelValuedAvailRS.GetElementsByTagName("ServiceHotel");

            List_Currency = ListCurreny(Xml_ServiceHotel);

            XmlNodeList xml_HotelList = Element_HotelValuedAvailRS.GetElementsByTagName("HotelInfo");
            List_Hotel = ListHotel(xml_HotelList);

            List<HotelDetail> List_ServiceHotel = new List<HotelDetail>();

            DataSet ds = new DataSet();
            DataLayer.HotelManager.GetHotelDetail("ENG", List_Hotel, out ds);
            // GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            GlobalDefault objGlobalDefault = (GlobalDefault)context.Session["LoginUser"];
            List<Models.Location> Location_List = new List<Models.Location>();
            List<Models.HotelDescription> HotelDescription_List = new List<Models.HotelDescription>();
            List<Models.Facility> Facility_List = HotelFacility(ds.Tables[0]);
            if (ds.Tables.Count > 1)
                Location_List = HotelLocation(ds.Tables[1]);
            if (ds.Tables.Count > 2)
                HotelDescription_List = Hotel_Description(ds.Tables[2]);

           
            int hotelID = 1;
            var ListAddress = GetAllAddress();
            foreach (XmlNode node in Xml_ServiceHotel)
            {
                m_price = 0;
                DataTable dtResult;
                HotelDetail objServiceHotel = new HotelDetail();
                objServiceHotel.AvailToken = node.Attributes["availToken"].Value;
                objServiceHotel.ServiceType = node.Attributes["xsi:type"].Value;
                objServiceHotel.DateFrom = GetDate(node["DateFrom"]);
                objServiceHotel.DateTo = GetDate(node["DateTo"]);
                string currency = GetCurrency(node["Currency"]);
                objServiceHotel.Currency = currency;
                //dolarRate = (Int32)HashTable_Currency[currency];
                //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(currency, out dtResult);
                //dolarRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());
                dolarRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                checkInDate = objServiceHotel.DateFrom;
                code = GetHotelCode(node["HotelInfo"]);
                objServiceHotel.HotelType = HotelType(node["HotelInfo"]);
                objServiceHotel.Code = code;
                objServiceHotel.HotelID = code + "_" + hotelID.ToString();
                hotelID = hotelID + 1;
                objServiceHotel.Name = GetHotelName(node["HotelInfo"]);
                objServiceHotel.Description = Hotel_Description(code, HotelDescription_List);
                objServiceHotel.Image = GetImage(node["HotelInfo"]);
                objServiceHotel.UserRating = 0;
                objServiceHotel.Review = 0;
                objServiceHotel.ContractList = Contract(node["ContractList"]);
                //objServiceHotel.Category = HotelCategory(GetCategory(node["HotelInfo"]));
                objServiceHotel.Category = GetCategory(node["HotelInfo"]);
                if (objServiceHotel.Category != "1EST" && objServiceHotel.Category != "2EST" && objServiceHotel.Category != "3EST" && objServiceHotel.Category != "4EST" && objServiceHotel.Category != "5EST")
                {
                    objServiceHotel.Category = "Other";
                }
                objServiceHotel.CategoryType = Common.HotelBed_HotelCategory(objServiceHotel.Category);
                objServiceHotel.Facility = HotelFacility(code, Facility_List);
                objServiceHotel.Location = HotelLocation(code, Location_List);
                objServiceHotel.Latitude = GetLatitude(node["HotelInfo"]);
                objServiceHotel.Langitude = GetLongitude(node["HotelInfo"]);
                objServiceHotel.Address = GetAddress(ListAddress, code);
                objServiceHotel.ChildAgeFrom = GetChildAgeFrom(node["HotelInfo"]);
                objServiceHotel.ChildAgeTo = GetChildAgeTo(node["HotelInfo"]);
                objServiceHotel.DestinationCode = GetDestionationCode(node["HotelInfo"]);
                objServiceHotel.DestionationType = GetDestionationType(node["HotelInfo"]);
                objServiceHotel.Supplier = "HotelBeds";
                List<XmlNode> List_AvailableRoom = node.ChildNodes.OfType<XmlNode>().Where(d => d.Name == "AvailableRoom").ToList();
                List<RoomOccupancy> List_RoomOccupancy = GetHotelRoom(List_AvailableRoom, code, context);
                if (List_RoomOccupancy.Count > 0)
                {
                    objServiceHotel.sRooms = List_RoomOccupancy;
                    objServiceHotel.Price = List_RoomOccupancy.Select(data => data.RoomPrice).Min();
                    objServiceHotel.CUTPrice = List_RoomOccupancy.Select(data => data.CUTRoomPrice).Min();
                    objServiceHotel.AgentMarkup = List_RoomOccupancy.Select(data => data.AgentMarkup).Min();
                    List_ServiceHotel.Add(objServiceHotel);
                }
            }
            List_Category = List_Category.OrderBy(data => data.Name).ToList();
            List_Facility = List_Facility.OrderBy(data => data.Name).ToList();
            List_Location = List_Location.OrderBy(data => data.Name).ToList();
            return List_ServiceHotel;
        }
        public List<HotelLib.Response.Facility> Facility
        {
            get { return List_Facility; }
        }
        public List<HotelLib.Response.Location> Location
        {
            get { return List_Location; }
        }
        public List<HotelLib.Response.Category> Category
        {
            get { return List_Category; }
        }

        private string HotelType(XmlElement element)
        {
            if (element == null)
                return "";
            return element.Attributes["xsi:type"].Value;
        }
        private void facilities(List<HotelDetail> List_ServiceHotel)
        {
            var Allow_Facilities = AllowingFacility();
        }
        private string GetDate(XmlElement element_date)
        {
            if (element_date == null)
                return null;
            return element_date.Attributes["date"].Value;
        }
        private string GetCurrency(XmlElement element_currency)
        {
            if (element_currency == null)
                return null;
            return element_currency.Attributes["code"].Value;
        }
        private string GetHotelCode(XmlElement element_HotelInfo)
        {
            if (element_HotelInfo == null)
                return "";
            return element_HotelInfo["Code"].InnerText;

        }
        private string GetHotelName(XmlElement element_HotelInfo)
        {
            if (element_HotelInfo == null)
                return "";
            return element_HotelInfo["Name"].InnerText;
        }
        private string GetCategory(XmlElement element)
        {
            if (element == null)
                return "";
            return element["Category"].Attributes["code"].Value;
        }
        private string GetChildAgeTo(XmlElement element)
        {
            if (element == null)
                return "";
            return element["ChildAge"].Attributes["ageTo"].Value;
        }
        private string GetChildAgeFrom(XmlElement element)
        {
            if (element == null)
                return "";
            return element["ChildAge"].Attributes["ageFrom"].Value;
        }
        private string GetLongitude(XmlElement element)
        {
            if (element == null)
                return "";
            return element["Position"].Attributes["longitude"].Value;
        }
        private string GetLatitude(XmlElement element)
        {
            if (element == null)
                return "";
            return element["Position"].Attributes["latitude"].Value;
        }
        private string GetAddress(object List, string code)
        {
            
            helperDataContext db = new helperDataContext();
            DBHelper.DBReturnCode retCode;
            try
            {
                List<CONTACT> list = (List<CONTACT>)List;

                string Address = list.Single(data => data.HotelCode == code).Address + ",  " + list.Single(data => data.HotelCode == code).City + " - " + list.Single(data => data.HotelCode == code).PostalCode;
                return Address;


            }
            catch
            {
                return "";
            }



        }
        public static object GetAllAddress()
        {

             helperDataContext db = new helperDataContext();

            try
            {
                // var List = (from obj in DB.CONTACTs  where  obj.City.Contains() ).ToList();
                string[] arrSearch = XMLHelper.context.Session["session"].ToString().Split('_');
                arrSearch = arrSearch[1].Split(',');
                var List = (from c in db.CONTACTs
                            where c.City.Contains(arrSearch[0])
                            select c).ToList(); ;
                if (List.Count > 0)
                {
                    return List;

                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }



        }
        private List<Image> GetImage(XmlElement element)
        {
            if (element == null)
                return null;
            List<Image> List_Image = new List<Image>();
            XmlNodeList nodelist = element.GetElementsByTagName("Image");
            foreach (XmlNode node in nodelist)
            {
                List_Image.Add(new Image { Type = node["Type"].InnerText, Url = LargeImage(node["Url"].InnerText), Thumbnail = node["Url"].InnerText });
            }
            return List_Image;
        }
        private AvailableRooms GetRoom(XmlNode node)
        {
            if (node == null)
                return null;
            HotelRoom objHotelRoom = GetHotelRoom(node["HotelRoom"]);
            if (objHotelRoom == null)
                return null;
            HotelOccupancy objHotelOccupancy = GetHotelOccupancy(node["HotelOccupancy"]);
            NoofRooms = objHotelOccupancy.RoomCount;
            return new AvailableRooms { sHotelOccupancy = objHotelOccupancy, sHotelRoom = objHotelRoom };
        }
        private HotelOccupancy GetHotelOccupancy(XmlElement element)
        {
            if (element == null)
                return null;
            HotelOccupancy objHotelOccupancy = new HotelOccupancy();
            objHotelOccupancy.RoomCount = Convert.ToInt32(element["RoomCount"].InnerText);
            objHotelOccupancy.sOccupancy = GetOccupancy(element["Occupancy"]);
            return objHotelOccupancy;
        }
        private Occupancy GetOccupancy(XmlElement element)
        {
            if (element == null)
                return null;
            return new Occupancy { AdultCount = Convert.ToInt32(element["AdultCount"].InnerText), ChildCount = Convert.ToInt32(element["ChildCount"].InnerText) };
        }
        private HotelRoom GetHotelRoom(XmlElement element)
        {
            if (element == null)
                return null;

            HotelRoom objHotelRoom = new HotelRoom();
            objHotelRoom.availCount = element.Attributes["availCount"].Value;
            objHotelRoom.SHRUI = element.Attributes["SHRUI"].Value;
            objHotelRoom.onRequest = element.Attributes["onRequest"].Value;
            objHotelRoom.sBoard = GetBoard(element["Board"]);
            objHotelRoom.sRoomType = GetRoomType(element["RoomType"]);
            objHotelRoom.sPrice = GetPrice(element["Price"]);
            objHotelRoom.sCancellationPolicy = GetCancellationPolicyPrice(element["CancellationPolicies"]);
            //if (objHotelRoom.sCancellationPolicy == null)
            //{
            //    return null;
            //}
            return objHotelRoom;
        }
        private Board GetBoard(XmlElement element)
        {
            if (element == null)
                return null;
            return new Board { type = element.Attributes["type"].Value, code = element.Attributes["code"].Value, shortname = element.Attributes["shortname"].Value, text = element.InnerText };
        }
        private RoomType GetRoomType(XmlElement element)
        {
            if (element == null)
                return null;
            string CharCode = element.Attributes["characteristic"].Value;
            string CharValue = element.InnerText;
            return new RoomType { type = element.Attributes["type"].Value, code = element.Attributes["code"].Value, characteristic = CharCode, text = CharValue };
        }
        private float GetPrice(XmlElement element)
        {
            if (element == null)
                return 0;

            float Amount = (float)Math.Round(((float.Parse(element["Amount"].InnerText)) * dolarRate), 3, MidpointRounding.AwayFromZero);
            if (Amount < 0)
                Amount = 0;
            return Amount;
        }
        private List<PriceList> GetPriceList(XmlElement element)
        {
            if (element == null)
                return null;
            XmlNodeList Node_List = element.GetElementsByTagName("Price");
            List<PriceList> list_price = new List<PriceList>();
            foreach (XmlNode node in Node_List)
            {
                float Amount = float.Parse(node["Amount"].InnerText);
                if (Amount < 0)
                    Amount = 0;
                string date = Convert.ToString(node["DateTimeFrom"].Attributes["date"].Value);
                string dateto = Convert.ToString(node["DateTimeTo"].Attributes["date"].Value);
                int day = Convert.ToInt32((DateTime.ParseExact(dateto, "yyyyMMdd", CultureInfo.InvariantCulture) - DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture)).TotalDays) + 1;
                for (int i = 0; i < day; i++)
                {
                    Amount = (float)Math.Round((Decimal)(Amount * dolarRate), 3, MidpointRounding.AwayFromZero);
                    if (MarkupPercentage > 0)
                        Amount = Amount + ((Amount * MarkupPercentage) / 100);
                    list_price.Add(new PriceList { Price = Amount, Date = DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture).AddDays(i) });
                }
            }
            return list_price;
        }
       
        private CancellationPolicy GetCancellationPolicyPrice(XmlElement element)
        {
            if (element == null)
                return null;
            XmlElement price = element;
            List<float> lstAmount = new List<float>();
            List<DateTime> lstChargeDate = new List<DateTime>();
            List<string> lstsChargeDate = new List<string>();
            DateTime ChargeDate = new DateTime();
            string sChargeDate = "";
            DateTime NoChargeDate = new DateTime();
            string sNoChargeDate = "";
            int i = 0;
            foreach (XmlElement elementCancellationPolicy in element)
            {
                DateTimeFrom dtFrom = new DateTimeFrom { date = elementCancellationPolicy.Attributes["dateFrom"].Value, time = elementCancellationPolicy.Attributes["time"].Value };
                int hour = Hour(dtFrom.time);
                int min = Min(dtFrom.time);
                string time = hour.ToString() + ":" + min.ToString();
                if (i == 0)
                    NoChargeDate = DateTime.ParseExact(dtFrom.date, "yyyyMMdd", CultureInfo.InvariantCulture).AddHours(hour).AddMinutes(min);
                ChargeDate = DateTime.ParseExact(dtFrom.date, "yyyyMMdd", CultureInfo.InvariantCulture).AddHours(hour).AddMinutes(min);
                //NoChargeDate = Convert.ToDateTime(NoChargeDate.ToString("dd/MM/yyyy HH:mm"));
                sNoChargeDate = NoChargeDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                sChargeDate = ChargeDate.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
                lstChargeDate.Add(ChargeDate);
                lstsChargeDate.Add(sChargeDate);
                float amount = float.Parse(elementCancellationPolicy.Attributes["amount"].Value) * dolarRate;
                amount = (float)Math.Round((Decimal)amount, 3, MidpointRounding.AwayFromZero);
                lstAmount.Add(amount);
                i = i + 1;
            }
            return new CancellationPolicy { Amount = lstAmount, NoChargeDate = NoChargeDate, ChargeDate = lstChargeDate, sChargeDate = lstsChargeDate, sNoChargeDate = sNoChargeDate };
        }
        private DateTimeFrom GetDateTimeForm(XmlElement element)
        {
            if (element == null)
                return null;
            return new DateTimeFrom { date = element.Attributes["date"].Value, time = element.Attributes["time"].Value };
        }
       
        private int Hour(string value)
        {
            value = value.Remove(2);
            return Convert.ToInt32(value);
        }
        private int Min(string value)
        {
            value = value.Remove(0, 2);
            return Convert.ToInt32(value);
        }
        private string LargeImage(string m_Image)
        {
            return m_Image.Replace("/small", "");
        }
        private List<HotelLib.Response.Facility> AllowingFacility()
        {
            List<HotelLib.Response.Facility> list_facility = new List<HotelLib.Response.Facility>();
            return list_facility;
        }
        private List<String> ListCurreny(XmlNodeList nodeList)
        {
            return nodeList.Cast<XmlNode>().Select(data => data["Currency"].Attributes["code"].Value).Distinct().ToList();
        }
        private List<String> ListHotel(XmlNodeList nodeList)
        {
            return nodeList.Cast<XmlNode>().Select(data => data["Code"].InnerText).Distinct().ToList();
        }
        private List<Models.Facility> HotelFacility(DataTable dtFacility)
        {
            if (dtFacility != null && dtFacility.Rows.Count > 0)
                return dtFacility.AsEnumerable().Select(data => new Models.Facility { HotelCode = data.Field<String>("HotelCode"), Name = data.Field<String>("Name") }).ToList();
            else
                return new List<Models.Facility>();
        }
        private List<Models.Location> HotelLocation(DataTable dtLocation)
        {
            if (dtLocation != null && dtLocation.Rows.Count > 0)
                return dtLocation.AsEnumerable().Select(data => new Models.Location { HotelCode = data.Field<String>("HotelCode"), Name = data.Field<String>("Name") }).ToList();
            else
                return new List<Models.Location>();
        }
        private List<Models.HotelDescription> Hotel_Description(DataTable dtHotelDescription)
        {
            if (dtHotelDescription != null && dtHotelDescription.Rows.Count > 0)
                //return dtHotelDescription.AsEnumerable().Select(data => new Models.HotelDescription { HotelCode = data.Field<String>("HotelCode"), Description = data.Field<String>("HotelDescription") }).ToList();
                return dtHotelDescription.AsEnumerable().Select(data => new Models.HotelDescription { HotelCode = data.Field<String>("HotelCode"), Description = data.Field<String>("HotelFacilities") }).ToList();
            else
                return new List<Models.HotelDescription>();
        }
        private string Hotel_Description(string code, List<Models.HotelDescription> m_Description)
        {
            string description = m_Description.Where(data => data.HotelCode == code).Select(data => data.Description).FirstOrDefault();
            if (String.IsNullOrEmpty(description))
                description = "N/A.";
            return description;
        }
        private List<String> HotelLocation(string code, List<Models.Location> m_List_Location)
        {
            var ListLocation = m_List_Location.Where(data => data.HotelCode == code).Select(data => data.Name).ToList();
            foreach (string location in ListLocation)
            {
                var Main_List_Location = List_Location.Where(data => data.Name == location).FirstOrDefault();
                if (Main_List_Location != null)
                    List_Location.Remove(Main_List_Location);
                else
                    Main_List_Location = new HotelLib.Response.Location { Name = location, Count = 0 };

                Main_List_Location.Count = Main_List_Location.Count + 1;
                List_Location.Add(Main_List_Location);
            }
            return ListLocation;
        }
        private List<string> HotelFacility(string code, List<Models.Facility> m_List_Facility)
        {
            var ListFacility = m_List_Facility.Where(data => data.HotelCode == code).Select(data => data.Name).ToList();
            foreach (string facility in ListFacility)
            {
                var Main_List_Facility = List_Facility.Where(data => data.Name == facility).FirstOrDefault();
                if (Main_List_Facility != null)
                    List_Facility.Remove(Main_List_Facility);
                else
                    Main_List_Facility = new HotelLib.Response.Facility { Name = facility, Count = 0 };

                Main_List_Facility.Count = Main_List_Facility.Count + 1;
                List_Facility.Add(Main_List_Facility);
            }
            return ListFacility;
        }
        private string AllowCategory(string code)
        {
            if (code == "1EST")
                return code;
            else if (code == "2EST")
                return code;
            else if (code == "3EST")
                return code;
            else if (code == "4EST")
                return code;
            else if (code == "5EST")
                return code;
            else
                return "Other";
        }
        private string HotelCategory(string code)
        {
            code = AllowCategory(code);
            var Main_Hotel_Category = List_Category.Where(data => data.Name == code).FirstOrDefault();
            if (Main_Hotel_Category != null)
                List_Category.Remove(Main_Hotel_Category);
            else
                Main_Hotel_Category = new Category { Name = code, Count = 0 };

            Main_Hotel_Category.Count = Main_Hotel_Category.Count + 1;
            List_Category.Add(Main_Hotel_Category);
            return code;
        }

       

        private List<Contract> Contract(XmlElement element)
        {
            List<Contract> List_Contract = new List<Contract>();
            XmlNodeList nodelist = element.GetElementsByTagName("Contract");
            foreach (XmlNode node in nodelist)
            {
                List_Contract.Add(new Contract { Name = node["Name"].InnerText, Code = node["IncomingOffice"].Attributes["code"].Value });
            }
            return List_Contract;
        }
        private string GetDestionationCode(XmlElement element)
        {
            return element["Destination"].Attributes["code"].Value;
        }
        private string GetDestionationType(XmlElement element)
        {
            return element["Destination"].Attributes["type"].Value;
        }
        private List<RoomOccupancy> GetHotelRoom(List<XmlNode> NodeList_Room, string HotelID, HttpContext context)
        {
            List<Room> List_Room = new List<Room>();
            foreach (XmlNode objNode in NodeList_Room)
            {
                HotelRoom objHotelRoom = GetHotelRoom(objNode["HotelRoom"]);
                CancellationPolicy objCancellationPolicy = new CancellationPolicy();
                List<float> lstAmount = new List<float>();
                List<float> lstCUTAmount = new List<float>();
                List<float> lstAgentCanMarkup = new List<float>();
                DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new DataLayer.MarkupsAndTaxes();
               

                if (context.Session["markups"] != null)
                    objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["markups"];
                else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                    objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["AgentMarkupsOnAdmin"];
                GlobalDefault objGlobalDefault = (GlobalDefault)context.Session["LoginUser"];
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                if (context.Session["AgentDetailsOnAdmin"] != null)
                {
                    objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)context.Session["AgentDetailsOnAdmin"];
                }
                //string currency = objGlobalDefault.Currency;
                string currency;
                if (context.Session["AgentDetailsOnAdmin"] != null)
                {
                    currency = objAgentDetailsOnAdmin.Currency;
                }
                else
                {
                    currency = objGlobalDefault.Currency;
                }
                float ExchangeRate = 0;
                if (currency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    //ExchangeRate = (float)Convert.ToDecimal(dtResult.Rows[0]["INR"].ToString());
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == currency).Rate;
                }
                float BaseAmt = objHotelRoom.sPrice / ExchangeRate;
                // float BaseAmt = objHotelRoom.sPrice;  old rate 

               
                //new logic 16 Jan 16 Begins

                float CutRoomAmount = 0;
                float AgentRoomMarkup = 0;
                MarkupTaxManager.objMarkupsAndTaxes = objMarkupsAndTaxes;
                CutRoomAmount = MarkupTaxManager.GetCutRoomAmount(BaseAmt, currency, NoofRooms, Night, "HotelBeds", out AgentRoomMarkup);

                //new logic 16 Jan 16 Ends

                //float AgentRoomMarkup = ((CutRoomAmount * objMarkupsAndTaxes.PerAgentMarkup) / 100);
                if (objHotelRoom.sCancellationPolicy != null)
                {
                    foreach (float amount in objHotelRoom.sCancellationPolicy.Amount)
                    {
                        float AgentCanMarkup = 0;
                        float CutCanAmount = MarkupTaxManager.GetCutRoomAmount((amount / ExchangeRate), currency, NoofRooms, Night, "HotelBeds", out AgentCanMarkup);
                        lstAmount.Add((amount / ExchangeRate));
                        lstCUTAmount.Add(CutCanAmount);
                        lstAgentCanMarkup.Add(AgentCanMarkup);

                    }
                }

                List<DateTime> lstChargedate = new List<DateTime>();
                List<string> lstsChargedate = new List<string>();
                if (objHotelRoom.sCancellationPolicy != null)
                {
                    foreach (string schargedate in objHotelRoom.sCancellationPolicy.sChargeDate)
                        lstsChargedate.Add(schargedate);
                }
                if (objHotelRoom.sCancellationPolicy != null)
                {
                    foreach (DateTime chargedate in objHotelRoom.sCancellationPolicy.ChargeDate)
                        lstChargedate.Add(chargedate);
                }
                HotelOccupancy objHotelOccupancy = GetHotelOccupancy(objNode["HotelOccupancy"]);
                List_Room.Add(new Room
                {
                    AdultCount = objHotelOccupancy.sOccupancy.AdultCount,
                    availCount = objHotelRoom.availCount,
                    Boardcode = objHotelRoom.sBoard.code,
                    Boardshortname = objHotelRoom.sBoard.shortname,
                    Boardtype = objHotelRoom.sBoard.type,
                    Boardtext = objHotelRoom.sBoard.text,
                    CancellationAmount = objHotelRoom.sCancellationPolicy == null ? lstAmount : lstAmount,
                    CUTCancellationAmount = objHotelRoom.sCancellationPolicy == null ? lstCUTAmount : lstCUTAmount,
                    AgentCancellationMarkup = objHotelRoom.sCancellationPolicy == null ? lstAgentCanMarkup : lstAgentCanMarkup,
                    ChargeDate = objHotelRoom.sCancellationPolicy == null ? lstChargedate : lstChargedate,
                    sChargeDate = objHotelRoom.sCancellationPolicy == null ? lstsChargedate : lstsChargedate,
                    sNoChargeDate = objHotelRoom.sCancellationPolicy == null ? "" : objHotelRoom.sCancellationPolicy.sNoChargeDate,
                    ChildCount = objHotelOccupancy.sOccupancy.ChildCount,
                    NoChargeDate = objHotelRoom.sCancellationPolicy == null ? DateTime.Now : objHotelRoom.sCancellationPolicy.NoChargeDate,
                    onRequest = objHotelRoom.onRequest,
                    RoomCount = objHotelOccupancy.RoomCount,
                    RoomAmount = objHotelRoom.sPrice,
                    CUTRoomAmount = CutRoomAmount,
                    AgentRoomMarkup = AgentRoomMarkup,
                    Roomcharacteristic = objHotelRoom.sRoomType.characteristic,
                    Roomcode = objHotelRoom.sRoomType.code,
                    Roomtext = objHotelRoom.sRoomType.text,
                    Roomtype = objHotelRoom.sRoomType.type,
                    SHRUI = objHotelRoom.SHRUI
                });
            }
            return GetRoomOccupancy(List_Room, HotelID);
        }
        private float GetTotalMarkup(float BookingAmt, float MarkupPer, float MarkupAmt, float CommiPer, float CommAmt)
        {
            float ActualMarkupPerAmt = (MarkupPer / 100) * BookingAmt;
            float ActualCommiPerAmt = (CommiPer / 100) * BookingAmt;
            float ActualGroupMarkTotal = Getgreatervalue(ActualMarkupPerAmt, MarkupAmt) - Getgreatervalue(ActualCommiPerAmt, CommAmt);
            return ActualGroupMarkTotal;

        }
        public float Markuptax(float Servicetax, float BaseAmount)
        {
            float Markuptax = (Servicetax / 100) * BaseAmount;
            return Markuptax;
        }
        private float GetAgentOwnMarkupAmt(float BookingAmt, float PercentageMarkup, float Amount)
        {
            float ActualMarkupAmt = (PercentageMarkup / 100) * BookingAmt;
            float ActualAgentMarkup = Getgreatervalue(ActualMarkupAmt, Amount);
            return ActualAgentMarkup;
        }

        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }
        private List<RoomOccupancy> GetRoomOccupancy(List<Room> List_Room, string HotelID)
        {
            List<RoomOccupancy> List_RoomOccupancy = new List<RoomOccupancy>();
            List<Room>[] Array_Room = new List<Room>[List_RequestOccupancy.Count];
            int min = 0;
            int i = 0;
            float price;
            float cutprice;
            float AgentMarkup;
            int l;
            //float ActualAgentRoomMarkup;
            foreach (HotelLib.Request.Occupancy objOccupancy in List_RequestOccupancy)
            {
                var occupancy_room = List_Room.Where(data => data.AdultCount == objOccupancy.AdultCount && data.ChildCount == objOccupancy.ChildCount && data.RoomCount == objOccupancy.RoomCount).ToList();
                Array_Room[i] = occupancy_room;
                if (occupancy_room.Count == 0)
                    return List_RoomOccupancy;
                if (min == 0 || min > occupancy_room.Count)
                    min = occupancy_room.Count;
                i = i + 1;
                //occupancy_room.Clear();
            }
          
            for (int j = 0; j < min; j++)
            {
                int count = 0;
                List<Room> Final_Room = new List<Room>();
                price = 0;
                cutprice = 0;
                AgentMarkup = 0;
                l = 0;

                while (Array_Room.Length > count)
                {
                    Room objRoom = (Room)Array_Room[count][j];  //original line
                    //Room objRoom = (Room)Array_Room[count][l];
                    if (count == 0)
                    {
                        Final_Room.Add(objRoom);
                        count = count + 1;
                        price = price + objRoom.RoomAmount;
                        cutprice = cutprice + objRoom.CUTRoomAmount;
                        AgentMarkup = AgentMarkup + objRoom.AgentRoomMarkup;
                    }
                    else
                    {
                        if (Final_Room[count - 1].Boardshortname == objRoom.Boardshortname)
                        {
                            Final_Room.Add(objRoom);
                            count = count + 1;
                            price = price + objRoom.RoomAmount;
                            cutprice = cutprice + objRoom.CUTRoomAmount;
                            AgentMarkup = AgentMarkup + objRoom.AgentRoomMarkup;
                        }
                        else
                        {
                            Final_Room.Clear();
                            break;
                            //l++;
                        }
                      
                    }
                }
                if (Final_Room.Count != 0)
                {
                    DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new DataLayer.MarkupsAndTaxes();
                    Models.Hotel objHotel;
                    NoofRooms = Final_Room.Count;
                  
                    List_RoomOccupancy.Add(new RoomOccupancy { RoomID = HotelID + "_" + (j + 1), sRoom = Final_Room, RoomPrice = price, CUTRoomPrice = cutprice, AgentMarkup = AgentMarkup });
                }

                //Final_Room.Clear();
            }

           
            return List_RoomOccupancy;
        }

       
        public List<HotelDetail> GetSortedList(List<HotelDetail> List_Room)
        {
            List<HotelDetail> List_ServiceHotel = new List<HotelDetail>();
            string sRoomID = "";
            string[] sRoomId;
            //int m = 0;
            RoomOccupancy objroomoccupancy;
            for (int i = 0; i < List_Room.Count; i++)
            {
                //HotelCategory(List_Room[i].Category);
                if (i == 0)
                {
                    List_ServiceHotel.Add(List_Room[i]);
                    HotelCategory(List_Room[i].Category);
                }
                else
                {
                    if (List_ServiceHotel.Last().Code == List_Room[i].Code)
                    {
                        objroomoccupancy = new RoomOccupancy();
                        sRoomID = "";
                        sRoomId = null;
                        #region commented
                       
                        #endregion commented
                        foreach (RoomOccupancy objRoomOccupancy in List_Room[i].sRooms)
                        {
                            objroomoccupancy = objRoomOccupancy;
                            sRoomID = objroomoccupancy.RoomID;
                            sRoomId = sRoomID.Split('_');
                            sRoomID = sRoomId[0] + "_" + Convert.ToInt64(List_ServiceHotel.Last().sRooms.Count + 1);
                            objroomoccupancy.RoomID = sRoomID;
                            List_ServiceHotel.Last().sRooms.Add(objroomoccupancy);
                        }
                        //List_ServiceHotel.Last().sRooms.Add(objroomoccupancy);//it will be here only
                    }
                    else
                    {
                        List_ServiceHotel.Add(List_Room[i]);
                        HotelCategory(List_Room[i].Category);
                    }
                }
            }

           

            return List_ServiceHotel;
        }

        public List<HotelDetail> RemoveHotelWithoutCancellationPolicy(List<HotelDetail> List_Room)
        {
            List<HotelDetail> List_ServiceHotel = new List<HotelDetail>();
            for (int i = 0; i < List_Room.Count; i++)
            {
                for (int j = 0; j < List_Room[i].sRooms.Count; j++)
                {
                    for (int k = 0; k < List_Room[i].sRooms[j].sRoom.Count; k++)
                    {
                        if (List_Room[i].sRooms[j].sRoom[k].CUTCancellationAmount.Count != 0)
                        {
                            if (List_ServiceHotel.Count == 0)
                            {
                                List_ServiceHotel.Add(List_Room[i]);
                                HotelCategory(List_Room[i].Category);
                            }
                            if (List_ServiceHotel.Last() != List_Room[i])
                            {
                                List_ServiceHotel.Add(List_Room[i]);
                                HotelCategory(List_Room[i].Category);
                            }
                        }
                    }
                }
            }
            return List_ServiceHotel;
        }
    }
}