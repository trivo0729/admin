﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using MGHLib.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Xml;using CutAdmin.dbml;

namespace CutAdmin.Common
{
    public class ParseMGHResponse
    {
        List<MGHLib.Response.Facilities> List_Facility = new List<MGHLib.Response.Facilities>();
        List<MGHLib.Response.Location> List_Location = new List<MGHLib.Response.Location>();
        List<MGHLib.Response.Category> List_Category = new List<MGHLib.Response.Category>();
        public HttpContext context { get; set; }
        public string Nationality { get; set; }
        public DataTable dtFacility { get; set; }
        public DataTable MyProperty { get; set; }
        //public bool ParseXML(string xmlResponse, int Night, int NoofRooms)
        //{
        //    //int rows;
        //    //SqlParameter[] sqlPara = new SqlParameter[3];
        //    //sqlPara[0] = new SqlParameter("@VarBody", "Testing Mail Body");
        //    //sqlPara[1] = new SqlParameter("@MailMailId", "shahidanwar888@gmail.com");
        //    //sqlPara[2] = new SqlParameter("@CCMailId", "kashifkhan4847@gmail.com");
        //    //DBHelper.DBReturnCode ret = DBHelper.ExecuteNonQuery("Proc_OfferMail", out rows, sqlPara);

        //    DBHelper.DBReturnCode retCode;
        //    DataTable dtResult;
        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(xmlResponse);
        //    XmlNode ndResponseStatus = doc.SelectSingleNode("search_response/responseCode");
        //    bool nStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);
        //    // GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    DataTable dtFacility = new DataTable(); DataTable dtRoomType = new DataTable();
        //    retCode = MGHManager.GetFacility(out dtFacility);
        //    MGHManager.GetRoomType(out dtRoomType);
        //    GlobalDefault objGlobalDefault = (GlobalDefault)context.Session["LoginUser"];

        //    //Only used for Applying Offer
        //    helperDataContext db = new helperDataContext();
        //    List<tbl_HotelOfferUtility> OfferList = new List<tbl_HotelOfferUtility>();
        //    List<tbl_HotelOfferUtility> OfferListNew = new List<tbl_HotelOfferUtility>();
        //    List<tbl_OfferDayUtility> OfferDayList = new List<tbl_OfferDayUtility>();
        //    List<tbl_OfferDayUtility> OfferDayListNew = new List<tbl_OfferDayUtility>();
        //    tbl_OfferDayUtility ObjOfferDay = new tbl_OfferDayUtility();
        //    Offers ObjOffer = new Offers();
        //    OfferList = (from Offer in DB.tbl_HotelOfferUtilities select Offer).ToList();
        //    OfferDayList = (from Offer in DB.tbl_OfferDayUtilities where Offer.Active == true select Offer).ToList();
        //    var National = (from N in DB.tbl_NationalityMasters where N.Nationality == Nationality select N).ToList();
        //    // END Only used for Applying Offer

        //    if (nStatus == true)
        //    {
        //        XmlNode ndHotels = doc.SelectSingleNode("search_response/hotels");
        //        int nNumberOfHotels = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("total").Value);
        //        //HttpContext.Current.Session["MGHNumberOfHotels"] = nNumberOfHotels;
        //        //HttpContext.Current.Session["MGHPage"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("page").Value);
        //        //HttpContext.Current.Session["MGHLimit"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("limit").Value);
        //        context.Session["MGHNumberOfHotels"] = nNumberOfHotels;
        //        context.Session["MGHPage"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("page").Value);
        //        context.Session["MGHLimit"] = Convert.ToInt32(ndHotels.Attributes.GetNamedItem("limit").Value);
        //        List<MGHHotelDetails> MGHHotelDetails = new List<MGHHotelDetails>();

        //        MGHHotelDetails[] objMGHHotelDetails = new MGHHotelDetails[nNumberOfHotels];

        //        Category objCategory = new Category();
        //        objCategory.Count = 1;
        //        Location objLocation = new Location();
        //        objLocation.Count = 1;
        //        DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new DataLayer.MarkupsAndTaxes();
        //        if (context.Session["markups"] != null)
        //            objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["markups"];
        //        else if (context.Session["AgentMarkupsOnAdmin"] != null)
        //            objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["AgentMarkupsOnAdmin"];
        //        MarkupTaxManager.objMarkupsAndTaxes = objMarkupsAndTaxes;


        //        try
        //        {
        //            for (int i = 0; i < nNumberOfHotels; i++)
        //            {
        //                objMGHHotelDetails[i] = new MGHHotelDetails();
        //                XmlNode ndHotel = doc.SelectSingleNode("search_response/hotels").ChildNodes[i];
        //                XmlNode ndRates = ndHotel.SelectSingleNode("rates");
        //                XmlNode nRate = ndRates.SelectSingleNode("rate");
        //                bool isBookAble = Convert.ToBoolean(ndRates.SelectSingleNode("rate").Attributes.GetNamedItem("bookable").Value);
        //                if (isBookAble)
        //                {
        //                    objMGHHotelDetails[i].HotelName = ndHotel.Attributes.GetNamedItem("name").Value;
        //                    objMGHHotelDetails[i].rating = Convert.ToInt32(ndHotel.Attributes.GetNamedItem("ratings").Value);
        //                    objMGHHotelDetails[i].Currency = ndHotel.Attributes.GetNamedItem("currency").Value;
        //                    string currency = (ndHotel.Attributes.GetNamedItem("currency").Value).ToString(); ;
        //                    objMGHHotelDetails[i].Description = ndHotel.SelectSingleNode("shortDescription").InnerText;
        //                    objMGHHotelDetails[i].DateFrom = ndHotel.SelectSingleNode("rates").Attributes.GetNamedItem("checkin").Value;
        //                    objMGHHotelDetails[i].DateTo = ndHotel.SelectSingleNode("rates").Attributes.GetNamedItem("checkout").Value;
        //                    objMGHHotelDetails[i].Location = new List<Location>();
        //                    objMGHHotelDetails[i].Category = new List<Category>();
        //                    objMGHHotelDetails[i].Facilities = new List<Facilities>();
        //                    objMGHHotelDetails[i].rate = new List<Rate>();
        //                    objMGHHotelDetails[i].MediaList = new List<Image>();
        //                    objMGHHotelDetails[i].ChannelComission = new List<ChannelCommission>();
        //                    objMGHHotelDetails[i].MasterInclusions = new List<MasterInclusions>();

        //                    int nLocationId = Convert.ToInt32(ndHotel.Attributes.GetNamedItem("location").Value);
        //                    retCode = MGHManager.GetLocationById(nLocationId, out dtResult);
        //                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        //Location objLocation = new Location();
        //                        objLocation.LocationId = nLocationId;
        //                        objLocation.LocationName = dtResult.Rows[0]["name"].ToString();
        //                        objLocation.Country = dtResult.Rows[0]["Countryname"].ToString();
        //                        //objMGHHotelDetails[i].Location = new List<Location>();
        //                        objMGHHotelDetails[i].Location.Add(objLocation);
        //                        if (!List_Location.Exists(data => data.LocationId == objLocation.LocationId))
        //                        {
        //                            List_Location.Add(new Location { LocationId = objLocation.LocationId, LocationName = objLocation.LocationName, Country = objLocation.Country, Count = objLocation.Count });
        //                            objLocation.Count = objLocation.Count + 1;
        //                        }
        //                        //List_Location.Add(objLocation);
        //                    }
        //                    int nCategoryId = Convert.ToInt32(ndHotel.Attributes.GetNamedItem("category").Value);
        //                    retCode = MGHManager.GetCategoryById(nCategoryId, out dtResult);
        //                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                    {
        //                        objCategory = new Category();
        //                        objCategory.CategoryId = nCategoryId;
        //                        objCategory.CategoryName = dtResult.Rows[0]["name"].ToString();
        //                        //objCategory.Count = 1;
        //                        //objMGHHotelDetails[i].Category = new List<Category>();
        //                        objMGHHotelDetails[i].Category.Add(objCategory);
        //                        if (!List_Category.Exists(data => data.CategoryId == objCategory.CategoryId))
        //                        {
        //                            List_Category.Add(new Category { CategoryId = objCategory.CategoryId, CategoryName = objCategory.CategoryName, Count = objCategory.Count });
        //                            objCategory.Count = objCategory.Count + 1;
        //                        }
        //                        //List_Category.Add(objCategory);
        //                    }
        //                    else
        //                    {
        //                        objCategory = new Category();
        //                        objCategory.CategoryId = nCategoryId;
        //                        objCategory.CategoryName = "Others";
        //                        objMGHHotelDetails[i].Category.Add(objCategory);
        //                        if (!List_Category.Exists(data => data.CategoryId == objCategory.CategoryId))
        //                        {
        //                            List_Category.Add(new Category { CategoryId = objCategory.CategoryId, CategoryName = objCategory.CategoryName, Count = objCategory.Count });
        //                            objCategory.Count = objCategory.Count + 1;
        //                        }
        //                    }

        //                    XmlNodeList ndMasterInclusions = ndHotel.SelectNodes("masterInclusions/inclusion");
        //                    MasterInclusions[] objMasterInclusions = new MasterInclusions[ndMasterInclusions.Count];
        //                    for (int k = 0; k < ndMasterInclusions.Count; k++)
        //                    {
        //                        objMasterInclusions[k] = new MasterInclusions();
        //                        objMasterInclusions[k].InclusionId = Convert.ToInt32(ndMasterInclusions[k].Attributes.GetNamedItem("id").Value);
        //                        objMasterInclusions[k].InclusionDetails = ndMasterInclusions[k].InnerText;
        //                        objMGHHotelDetails[i].MasterInclusions.Add(objMasterInclusions[k]);
        //                    }
        //                    XmlNodeList ndChannelCommission = ndHotel.SelectNodes("channelCommission");
        //                    ChannelCommission[] objChannelCommission = new ChannelCommission[ndChannelCommission.Count];
        //                    for (int k = 0; k < ndChannelCommission.Count; k++)
        //                    {
        //                        objChannelCommission[k] = new ChannelCommission();
        //                        objChannelCommission[k].Included = Convert.ToBoolean(ndChannelCommission[k].Attributes.GetNamedItem("included").Value);
        //                        objChannelCommission[k].unit = ndChannelCommission[k].Attributes.GetNamedItem("unit").Value;
        //                        objChannelCommission[k].value = float.Parse(ndChannelCommission[k].Attributes.GetNamedItem("value").Value);
        //                        objMGHHotelDetails[i].ChannelComission.Add(objChannelCommission[k]);
        //                    }
        //                    XmlNodeList ndFacilities = ndHotel.SelectNodes("facilities/facility");
        //                    Facilities[] objFacilities = new Facilities[ndFacilities.Count];

        //                    if (dtFacility.Rows.Count != 0)
        //                    {
        //                        for (int k = 0; k < ndFacilities.Count; k++)
        //                        {
        //                            objFacilities[k] = new Facilities();
        //                            objFacilities[k].FacilitiesId = Convert.ToInt32(ndFacilities[k].Attributes.GetNamedItem("id").Value);
        //                            DataRow[] arrDR = dtFacility.Select("id=" + objFacilities[k].FacilitiesId);
        //                            try
        //                            {
        //                                objFacilities[k].FacilitiesName = arrDR[0]["name"].ToString();
        //                            }
        //                            catch
        //                            {
        //                                objFacilities[k].FacilitiesName = "Unavailable";
        //                            }
        //                            objMGHHotelDetails[i].Facilities.Add(objFacilities[k]);
        //                            if (!List_Facility.Exists(data => data.FacilitiesId == objFacilities[k].FacilitiesId))
        //                                List_Facility.Add(new Facilities { FacilitiesId = objFacilities[k].FacilitiesId, FacilitiesName = objFacilities[k].FacilitiesName });
        //                        }
        //                    }
        //                    string ratesCheckIn = ndRates.Attributes.GetNamedItem("checkin").Value;
        //                    string ratesCheckOut = ndRates.Attributes.GetNamedItem("checkout").Value;
        //                    XmlNodeList ndRateList = ndHotel.SelectNodes("rates/rate");
        //                    Rate[] objRateList = new Rate[ndRateList.Count];
        //                    RoomCategory[] objRoomCategory = new RoomCategory[ndRateList.Count];
        //                    RoomType[] objRoomType = new RoomType[ndRateList.Count];
        //                    for (int j = 0; j < ndRateList.Count; j++)
        //                    {
        //                        XmlNodeList objRoomList = ndRateList[j].SelectNodes("roomInfo/room");
        //                        int roomcount = objRoomList.Count;
        //                        if (roomcount != NoofRooms)
        //                        {
        //                            continue;
        //                        }
        //                        objRateList[j] = new Rate();
        //                        objRateList[j].checkin = ratesCheckIn;
        //                        objRateList[j].checkout = ratesCheckOut;
        //                        objRoomCategory[j] = new RoomCategory();
        //                        objRoomType[j] = new RoomType();
        //                        float RoomAmount = 0;
        //                        float CutAmount = 0;
        //                        float AgentMarkup = 0;

        //                        float AgentCanMarkup = 0;
        //                        float CancellationAmount = 0;
        //                        List<RoomRate> lstRoomRate = new List<RoomRate>();

        //                        int nRoomCategoryId = Convert.ToInt32(ndRateList[j].Attributes.GetNamedItem("roomCategoryId").Value);
        //                        int nRoomTypeId = Convert.ToInt32(ndRateList[j].Attributes.GetNamedItem("roomTypeId").Value);
        //                        objRateList[j].availCount = Convert.ToInt32(ndRateList[j].Attributes.GetNamedItem("availCount").Value);
        //                        objRateList[j].rateType = ndRateList[j].Attributes.GetNamedItem("rateType").Value;
        //                        objRateList[j].taxIncluded = ndRateList[j].Attributes.GetNamedItem("taxIncluded").Value;

        //                        objRoomCategory[j].RoomCategoryId = nRoomCategoryId;
        //                        objRoomType[j].RoomTypeId = nRoomTypeId;

        //                        XmlNodeList ndRoomCategory = ndHotel.SelectNodes("roomCategories/roomCategory");
        //                        for (int RC = 0; RC < ndRoomCategory.Count; RC++)
        //                        {
        //                            int id = Convert.ToInt32(ndRoomCategory[RC].Attributes.GetNamedItem("id").Value);
        //                            if (id == nRoomCategoryId)
        //                            {
        //                                objRoomCategory[j].RoomCategoryName = ndRoomCategory[RC].Attributes.GetNamedItem("name").Value;
        //                            }
        //                        }
        //                        // retCode = MGHManager.GetTypeName(nRoomTypeId, out dtResult);
        //                        DataRow[] arrRows = dtRoomType.Select("id = '" + nRoomTypeId + "' ");
        //                        objRoomType[j].RoomTypeName = arrRows[0]["name"].ToString();
        //                        objRateList[j].roomCategory = new List<RoomCategory>();
        //                        objRateList[j].roomType = new List<RoomType>();
        //                        //objRateList[j].roomCategory.Add(objRoomCategory[j]);
        //                        objRateList[j].roomType.Add(objRoomType[j]);

        //                        List<RefundPolicy> lstrefundPolicy = new List<RefundPolicy>();
        //                        XmlNodeList ndRoomList = ndRateList[j].SelectNodes("roomInfo/room");

        //                        RoomInfo[] objRoomInfo = new RoomInfo[ndRoomList.Count];
        //                        objRateList[j].roomInfo = new List<RoomInfo>();
        //                        for (int k = 0; k < ndRoomList.Count; k++)
        //                        {
        //                            objRoomInfo[k] = new RoomInfo();
        //                            objRoomInfo[k].roomNumber = Convert.ToInt32(ndRoomList[k].Attributes.GetNamedItem("number").Value);
        //                            XmlNodeList ndRoomRate = ndRoomList[k].SelectNodes("roomrate");
        //                            RoomRate[] objRoomRate = new RoomRate[ndRoomRate.Count];
        //                            objRoomInfo[k].roomRate = new List<RoomRate>();
        //                            for (int x = 0; x < ndRoomRate.Count; x++)
        //                            {
        //                                objRoomRate[x] = new RoomRate();
        //                                objRoomRate[x].type = ndRoomRate[x].Attributes.GetNamedItem("type").Value;
        //                                objRoomRate[x].amount = float.Parse(ndRoomRate[x].InnerText);
        //                                lstRoomRate.Add(objRoomRate[x]);
        //                                AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //                                if (context.Session["AgentDetailsOnAdmin"] != null)
        //                                {
        //                                    objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
        //                                }

        //                                float ExchangeRate = (float)(objGlobalDefault.ExchangeRate.Single(currencytype => currencytype.Currency == currency).Rate);
        //                                float Supplier_ExchangeRate = ExchangeRate;
        //                                // this is supplier currency exchange rate
        //                                if (currency == "INR")// this is supplier currency
        //                                {
        //                                    Supplier_ExchangeRate = 1;
        //                                }
        //                                string PreferedCurrency;
        //                                if (context.Session["AgentDetailsOnAdmin"] != null)
        //                                {
        //                                    PreferedCurrency = objAgentDetailsOnAdmin.Currency;
        //                                }
        //                                else
        //                                {
        //                                    PreferedCurrency = objGlobalDefault.Currency;
        //                                }

        //                                if (PreferedCurrency == "INR")
        //                                {
        //                                    ExchangeRate = 1;
        //                                }
        //                                else
        //                                {
        //                                    ExchangeRate = (float)(objGlobalDefault.ExchangeRate.Single(currencytype => currencytype.Currency == PreferedCurrency).Rate);
        //                                }

        //                                //testing  ................................................................................................................................
        //                                if (objRoomRate[x].type == "aop")
        //                                {
        //                                    float AgentRoomMarkup = 0;

        //                                    #region Variable
        //                                    float BaseAmt = 0.0F;
        //                                    float BaseExtraBed = 0.0F;
        //                                    float BaseKid = 0.0F;
        //                                    #endregion

        //                                    BaseAmt = objRoomRate[x].amount * Night * Supplier_ExchangeRate;
        //                                    BaseAmt = BaseAmt / ExchangeRate;

        //                                    for (int y = 0; y < ndRoomRate.Count; y++)
        //                                    {
        //                                        objRoomRate[y] = new RoomRate();
        //                                        objRoomRate[y].type = ndRoomRate[y].Attributes.GetNamedItem("type").Value;
        //                                        objRoomRate[y].amount = float.Parse(ndRoomRate[y].InnerText);
        //                                        if (objRoomRate[y].type == "extraBed")
        //                                        {
        //                                            BaseExtraBed = objRoomRate[y].amount * Night * Supplier_ExchangeRate;
        //                                            BaseExtraBed = BaseExtraBed / ExchangeRate;
        //                                        }
        //                                        else if (objRoomRate[y].type == "kid")
        //                                        {
        //                                            BaseKid = objRoomRate[y].amount * Night * Supplier_ExchangeRate;
        //                                            BaseKid = BaseKid / ExchangeRate;
        //                                        }
        //                                    }
        //                                    //CUT.Common.XMLHelper objXmlHelper = new XMLHelper();
        //                                    //var NA = objXmlHelper.objStatusCode.DisplayRequest.Room;
        //                                    //var Testr = HttpContext.Current.Session["HBRooms"];
        //                                    #region Applying Offer
        //                                    List<MGHLib.Response.Offers> MyOffer = new List<Offers>();
        //                                    Int64 Hid = Convert.ToInt64(ndHotel.Attributes.GetNamedItem("id").Value);
        //                                    Int64 Rid = Convert.ToInt64(ndRoomCategory[j].Attributes.GetNamedItem("id").Value);

        //                                    OfferListNew = OfferList.Where(z => z.Hotel_Id == Hid && z.Room_Id == Rid).ToList();
        //                                    if (OfferListNew.Count > 0)
        //                                    {
        //                                        DateTime ChkIn = DateTime.ParseExact(ndHotel.SelectSingleNode("rates").Attributes.GetNamedItem("checkin").Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //                                        DateTime ChkOut = DateTime.ParseExact(ndHotel.SelectSingleNode("rates").Attributes.GetNamedItem("checkout").Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //                                        List<DateTime> InOutDate = new List<DateTime>();

        //                                        //Creating CheckIn CheckOut List
        //                                        for (var dt = ChkIn; dt < ChkOut; dt = dt.AddDays(1))
        //                                        {
        //                                            InOutDate.Add(dt);
        //                                        }
        //                                        DateTime CDate = DateTime.Now.Date;
        //                                        // Loop for Offer Available for this Hotel & Room
        //                                        for (int s = 0; s < OfferListNew.Count; s++)
        //                                        {
        //                                            // Getting All Seasons & Block Dates for perticular Offer
        //                                            OfferDayListNew = OfferDayList.Where(z => z.Offer_Id == OfferListNew[s].HotelOfferId).ToList();
        //                                            if (OfferDayListNew.Count > 0)
        //                                            {
        //                                                #region Getting Nationality
        //                                                bool IsNat = false;
        //                                                string[] Arr = OfferListNew[s].OfferNationality.Split(',');
        //                                                for (int ab = 0; ab < Arr.Length; ab++)
        //                                                {
        //                                                    if (ab == 0)
        //                                                    {
        //                                                        if (Arr[ab] == National[0].Country.ToUpper())
        //                                                        {
        //                                                            IsNat = true;
        //                                                            break;
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        string[] Splitter = Arr[ab].Split(' ');
        //                                                        if (Splitter[1] == National[0].Country.ToUpper())
        //                                                        {
        //                                                            IsNat = true;
        //                                                            break;
        //                                                        }
        //                                                    }
        //                                                }
        //                                                #endregion END Getting Nationality

        //                                                if (IsNat)
        //                                                {
        //                                                    DateTime BookDate = DateTime.Now;
        //                                                    //Getting value of fixed date
        //                                                    if (OfferDayListNew[s].BookBefore != null && OfferDayListNew[s].BookBefore != "")
        //                                                    {
        //                                                        string BookBefore = OfferDayListNew[s].BookBefore;
        //                                                        BookDate = DateTime.ParseExact(BookBefore, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                                                    }
        //                                                    //Getting value of Days Prior
        //                                                    else if (OfferDayListNew[s].DaysPrior != null && OfferDayListNew[s].DaysPrior != "")
        //                                                    {
        //                                                        Int64 DaysPrior = Convert.ToInt64(OfferDayListNew[s].DaysPrior);
        //                                                        string ValidD = OfferDayListNew[0].ValidFrom;
        //                                                        BookDate = DateTime.ParseExact(ValidD, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                                                        DaysPrior = DaysPrior - DaysPrior * 2;
        //                                                        BookDate = BookDate.AddDays(DaysPrior);
        //                                                    }
        //                                                    //Checking Offer is available for today date
        //                                                    if (CDate <= BookDate)
        //                                                    {
        //                                                        string[] BlockDaysArr = new string[] { "" };
        //                                                        if (OfferListNew[s].BlockDays != null)
        //                                                            BlockDaysArr = OfferListNew[s].BlockDays.Split('^');
        //                                                        // Loop for All Seasons & Block Dates for perticular Offer
        //                                                        for (int t = 0; t < OfferDayListNew.Count; t++)
        //                                                        {
        //                                                            List<DateTime> ValidDate = new List<DateTime>();
        //                                                            DateTime ValidFrom = DateTime.ParseExact(OfferDayListNew[t].ValidFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                                                            DateTime ValidTo = DateTime.ParseExact(OfferDayListNew[t].ValidTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //                                                            for (var dt = ValidFrom; dt < ValidTo; dt = dt.AddDays(1))
        //                                                            {
        //                                                                ValidDate.Add(dt);
        //                                                            }
        //                                                            if (ValidDate.Count == 0 && ValidFrom == ValidTo)
        //                                                            {
        //                                                                ValidDate.Add(ValidFrom);
        //                                                            }
        //                                                            List<DateTime> CommonDate = InOutDate.Intersect(ValidDate).ToList();
        //                                                            if (CommonDate.Count > 0)
        //                                                            {
        //                                                                for (int y = 0; y < InOutDate.Count; y++)
        //                                                                {
        //                                                                    string MealPlan = OfferDayListNew[s].MealPlan;
        //                                                                    if (OfferDayListNew[s].MealPlan == MealPlan)
        //                                                                    {
        //                                                                        #region Getting Block Day
        //                                                                        string Day = InOutDate[y].DayOfWeek.ToString();
        //                                                                        bool IsOfferDay = true;
        //                                                                        for (int q = 0; q < BlockDaysArr.Length; q++)
        //                                                                        {
        //                                                                            if (Day == BlockDaysArr[q])
        //                                                                            {
        //                                                                                IsOfferDay = false;
        //                                                                                break;
        //                                                                            }
        //                                                                        }
        //                                                                        #endregion END Getting Block Day
        //                                                                        string ApplyingOffer = "";
        //                                                                        if (IsOfferDay)
        //                                                                        {
        //                                                                            #region Add Offer Details to List
        //                                                                            var CommomD = CommonDate.Where(a => a == InOutDate[y]).ToList();
        //                                                                            if (CommomD.Count > 0)
        //                                                                            {
        //                                                                                if (MyOffer.Count > 0)
        //                                                                                {
        //                                                                                    var List = MyOffer.Where(r => r.ValidFrom == InOutDate[y].ToString() && r.Hotel_Id == Convert.ToInt64(OfferDayListNew[t].Hotel_Id) && r.Room_Id == Convert.ToInt64(OfferDayListNew[t].Room_Id) && r.Offer_Id == Convert.ToInt64(OfferDayListNew[t].Offer_Id)).ToList();
        //                                                                                    if (List.Count > 0)
        //                                                                                    {
        //                                                                                        if (List[0].DateType == "Block Date")
        //                                                                                        {
        //                                                                                            continue;
        //                                                                                        }
        //                                                                                        else if (OfferDayListNew[t].DateType == "Block Date")
        //                                                                                        {
        //                                                                                            //MyOffer.Remove(List[0]);
        //                                                                                            //ObjOffer = new Offers();
        //                                                                                            ApplyingOffer = "No Offer";
        //                                                                                            List[0].ApplyingOffer = ApplyingOffer;
        //                                                                                            decimal Prices = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                                            float rate = (float)Convert.ToDouble(Prices);
        //                                                                                            rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                                            List[0].OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                                            float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                            Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                                            List[0].AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                                            List[0].RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                            List[0].IsOffer = false;
        //                                                                                            List[0].ValidFrom = InOutDate[y].ToString();
        //                                                                                            List[0].ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                                            List[0].Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                            List[0].Hotel_Id = List[0].Hotel_Id;
        //                                                                                            List[0].Room_Id = List[0].Room_Id;
        //                                                                                            List[0].Offer_Id = List[0].Offer_Id;
        //                                                                                            List[0].DateType = "Block Date";
        //                                                                                            List[0].Id = s;
        //                                                                                            //MyOffer.Add(ObjOffer);
        //                                                                                        }
        //                                                                                    }
        //                                                                                    else
        //                                                                                    {
        //                                                                                        var NewList = MyOffer.Where(r => r.ValidFrom == InOutDate[y].ToString() && r.Hotel_Id == Convert.ToInt64(OfferDayListNew[t].Hotel_Id) && r.Room_Id == Convert.ToInt64(OfferDayListNew[t].Room_Id) && r.Id == s).ToList();
        //                                                                                        if (NewList.Count == 0)
        //                                                                                        {
        //                                                                                            #region Add Offer to List
        //                                                                                            ObjOffer = new Offers();
        //                                                                                            ObjOffer.IsOffer = true;
        //                                                                                            ObjOffer.Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                            ObjOffer.Room_Id = Convert.ToInt64(OfferDayListNew[t].Room_Id);
        //                                                                                            ObjOffer.Offer_Id = Convert.ToInt64(OfferDayListNew[t].Offer_Id);
        //                                                                                            ObjOffer.Hotel_Id = Convert.ToInt64(OfferDayListNew[t].Hotel_Id);
        //                                                                                            ObjOffer.BookBefore = OfferDayListNew[t].BookBefore;
        //                                                                                            ObjOffer.DateType = OfferDayListNew[t].DateType;
        //                                                                                            ObjOffer.DaysPrior = OfferDayListNew[t].DaysPrior;
        //                                                                                            ObjOffer.DiscountAmount = Convert.ToInt64(OfferDayListNew[t].DiscountAmount);
        //                                                                                            ObjOffer.DiscountPer = Convert.ToInt64(OfferDayListNew[t].DiscountPer);
        //                                                                                            ObjOffer.FreebiItem = OfferDayListNew[t].FreebiItem;
        //                                                                                            ObjOffer.FreebiItemDetail = OfferDayListNew[t].FreebiItemDetail;
        //                                                                                            ObjOffer.HotelOfferCode = OfferDayListNew[t].HotelOfferCode;
        //                                                                                            ObjOffer.MealPlan = OfferDayListNew[t].MealPlan;
        //                                                                                            ObjOffer.MinNights = OfferDayListNew[t].MinNights;
        //                                                                                            ObjOffer.NewRate = Convert.ToInt64(OfferDayListNew[t].NewRate);
        //                                                                                            ObjOffer.OfferCode = OfferDayListNew[t].OfferCode;
        //                                                                                            ObjOffer.OfferNote = OfferDayListNew[t].OfferNote;
        //                                                                                            ObjOffer.OfferOn = OfferDayListNew[t].OfferOn;
        //                                                                                            ObjOffer.OfferTerms = OfferDayListNew[t].OfferTerms;
        //                                                                                            ObjOffer.OfferType = OfferDayListNew[t].OfferType;
        //                                                                                            ObjOffer.SeasonName = OfferDayListNew[t].SeasonName;
        //                                                                                            ObjOffer.ValidFrom = InOutDate[y].ToString();
        //                                                                                            ObjOffer.ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                                            ObjOffer.Id = s;
        //                                                                                            ObjOffer.Supplier = Convert.ToInt64(OfferListNew[s].Supplier_Id);
        //                                                                                            decimal Price = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                                            if (ObjOffer.DateType != "Block Date")
        //                                                                                            {
        //                                                                                                if (ObjOffer.OfferType == "Discount")
        //                                                                                                {
        //                                                                                                    if (ObjOffer.DiscountAmount != 0)
        //                                                                                                    {
        //                                                                                                        Price = Price - ObjOffer.DiscountAmount;
        //                                                                                                        ApplyingOffer = (ObjOffer.DiscountAmount + " Off").ToString();
        //                                                                                                        ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                                    }
        //                                                                                                    if (ObjOffer.DiscountPer != 0)
        //                                                                                                    {
        //                                                                                                        decimal PerAmount = Price * ObjOffer.DiscountPer / 100;
        //                                                                                                        Price = Price - PerAmount;
        //                                                                                                        ApplyingOffer = (ObjOffer.DiscountPer + "% Off").ToString();
        //                                                                                                        ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                                    }
        //                                                                                                }
        //                                                                                            }
        //                                                                                            else
        //                                                                                            {
        //                                                                                                ApplyingOffer = "No Offer";
        //                                                                                                ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                            }
        //                                                                                            float rate = (float)Convert.ToDouble(Price);
        //                                                                                            rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                                            ObjOffer.OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                                            float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                            Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                                            ObjOffer.AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                                            ObjOffer.RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                            MyOffer.Add(ObjOffer);
        //                                                                                            #endregion
        //                                                                                        }
        //                                                                                        else
        //                                                                                        {
        //                                                                                            //MyOffer.Remove(List[0]);
        //                                                                                            //ObjOffer = new Offers();
        //                                                                                            ApplyingOffer = "No Offer";
        //                                                                                            List[0].ApplyingOffer = ApplyingOffer;
        //                                                                                            decimal Prices = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                                            float rate = (float)Convert.ToDouble(Prices);
        //                                                                                            rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                                            List[0].OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                                            float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                            Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                                            List[0].AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                                            List[0].RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                            List[0].IsOffer = false;
        //                                                                                            List[0].ValidFrom = InOutDate[y].ToString();
        //                                                                                            List[0].ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                                            List[0].Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                            List[0].Hotel_Id = List[0].Hotel_Id;
        //                                                                                            List[0].Room_Id = List[0].Room_Id;
        //                                                                                            List[0].Offer_Id = List[0].Offer_Id;
        //                                                                                            List[0].DateType = "Block Date";
        //                                                                                            List[0].Id = s;
        //                                                                                            MyOffer.Add(ObjOffer);
        //                                                                                        }
        //                                                                                    }
        //                                                                                }
        //                                                                                else
        //                                                                                {
        //                                                                                    #region Add Offer to List
        //                                                                                    ObjOffer = new Offers();
        //                                                                                    ObjOffer.IsOffer = true;
        //                                                                                    ObjOffer.Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                    ObjOffer.Room_Id = Convert.ToInt64(OfferDayListNew[t].Room_Id);
        //                                                                                    ObjOffer.Offer_Id = Convert.ToInt64(OfferDayListNew[t].Offer_Id);
        //                                                                                    ObjOffer.Hotel_Id = Convert.ToInt64(OfferDayListNew[t].Hotel_Id);
        //                                                                                    ObjOffer.BookBefore = OfferDayListNew[t].BookBefore;
        //                                                                                    ObjOffer.DateType = OfferDayListNew[t].DateType;
        //                                                                                    ObjOffer.DaysPrior = OfferDayListNew[t].DaysPrior;
        //                                                                                    ObjOffer.DiscountAmount = Convert.ToInt64(OfferDayListNew[t].DiscountAmount);
        //                                                                                    ObjOffer.DiscountPer = Convert.ToInt64(OfferDayListNew[t].DiscountPer);
        //                                                                                    ObjOffer.FreebiItem = OfferDayListNew[t].FreebiItem;
        //                                                                                    ObjOffer.FreebiItemDetail = OfferDayListNew[t].FreebiItemDetail;
        //                                                                                    ObjOffer.HotelOfferCode = OfferDayListNew[t].HotelOfferCode;
        //                                                                                    ObjOffer.MealPlan = OfferDayListNew[t].MealPlan;
        //                                                                                    ObjOffer.MinNights = OfferDayListNew[t].MinNights;
        //                                                                                    ObjOffer.NewRate = Convert.ToInt64(OfferDayListNew[t].NewRate);
        //                                                                                    ObjOffer.OfferCode = OfferDayListNew[t].OfferCode;
        //                                                                                    ObjOffer.OfferNote = OfferDayListNew[t].OfferNote;
        //                                                                                    ObjOffer.OfferOn = OfferDayListNew[t].OfferOn;
        //                                                                                    ObjOffer.OfferTerms = OfferDayListNew[t].OfferTerms;
        //                                                                                    ObjOffer.OfferType = OfferDayListNew[t].OfferType;
        //                                                                                    ObjOffer.SeasonName = OfferDayListNew[t].SeasonName;
        //                                                                                    ObjOffer.ValidFrom = InOutDate[y].ToString();
        //                                                                                    ObjOffer.ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                                    ObjOffer.Id = s;
        //                                                                                    ObjOffer.Supplier = Convert.ToInt64(OfferListNew[s].Supplier_Id);
        //                                                                                    decimal Price = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                                    if (ObjOffer.DateType != "Block Date")
        //                                                                                    {
        //                                                                                        if (ObjOffer.OfferType == "Discount")
        //                                                                                        {
        //                                                                                            if (ObjOffer.DiscountAmount != 0)
        //                                                                                            {
        //                                                                                                Price = Price - ObjOffer.DiscountAmount;
        //                                                                                                ApplyingOffer = (ObjOffer.DiscountAmount + " Off").ToString();
        //                                                                                                ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                            }
        //                                                                                            if (ObjOffer.DiscountPer != 0)
        //                                                                                            {
        //                                                                                                decimal PerAmount = Price * ObjOffer.DiscountPer / 100;
        //                                                                                                Price = Price - PerAmount;
        //                                                                                                ApplyingOffer = (ObjOffer.DiscountPer + "% Off").ToString();
        //                                                                                                ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                            }
        //                                                                                        }
        //                                                                                    }
        //                                                                                    else
        //                                                                                    {
        //                                                                                        ApplyingOffer = "No Offer";
        //                                                                                        ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                    }
        //                                                                                    float rate = (float)Convert.ToDouble(Price);
        //                                                                                    rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                                    ObjOffer.OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                                    float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                    Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                                    ObjOffer.AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                                    ObjOffer.RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                    MyOffer.Add(ObjOffer);
        //                                                                                    #endregion
        //                                                                                }
        //                                                                            }
        //                                                                            else
        //                                                                            {
        //                                                                                var List = MyOffer.Where(r => r.ValidFrom == InOutDate[y].ToString() && r.Hotel_Id == Convert.ToInt64(OfferDayListNew[t].Hotel_Id) && r.Room_Id == Convert.ToInt64(OfferDayListNew[t].Room_Id) && r.Offer_Id == Convert.ToInt64(OfferDayListNew[t].Offer_Id)).ToList();
        //                                                                                if (List.Count == 0)
        //                                                                                {
        //                                                                                    var NewList = MyOffer.Where(r => r.ValidFrom == InOutDate[y].ToString() && r.Hotel_Id == Convert.ToInt64(OfferDayListNew[t].Hotel_Id) && r.Room_Id == Convert.ToInt64(OfferDayListNew[t].Room_Id) && r.Id == s).ToList();
        //                                                                                    if (NewList.Count == 0)
        //                                                                                    {
        //                                                                                        ObjOffer = new Offers();
        //                                                                                        ApplyingOffer = "No Offer";
        //                                                                                        ObjOffer.IsOffer = false;
        //                                                                                        ObjOffer.ValidFrom = InOutDate[y].ToString();
        //                                                                                        ObjOffer.ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                                        decimal Prices = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                                        float rate = (float)Convert.ToDouble(Prices);
        //                                                                                        rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                                        ObjOffer.OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                                        float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                        Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                                        ObjOffer.AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                                        ObjOffer.RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                        ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                        //ObjOffer.Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                        ObjOffer.Hotel_Id = Convert.ToInt64(OfferDayListNew[t].Hotel_Id);
        //                                                                                        ObjOffer.Room_Id = Convert.ToInt64(OfferDayListNew[t].Room_Id);
        //                                                                                        ObjOffer.DateType = "No Offer";
        //                                                                                        ObjOffer.Id = s;
        //                                                                                        MyOffer.Add(ObjOffer);
        //                                                                                    }
        //                                                                                }
        //                                                                                else
        //                                                                                {
        //                                                                                    /*if (List[0].DateType == "Block Date")
        //                                                                                    {
        //                                                                                        continue;
        //                                                                                    }
        //                                                                                    else if (OfferDayListNew[t].DateType == "Block Date")
        //                                                                                    {
        //                                                                                        MyOffer.Remove(List[0]);
        //                                                                                        ApplyingOffer = "-";
        //                                                                                        ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                                        decimal Prices = Convert.ToDecimal(BaseAmt / Night);
        //                                                                                        float rate = (float)Convert.ToDouble(Prices);
        //                                                                                        float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                                        ObjOffer.RoomPrice = Convert.ToDecimal(Amt);
        //                                                                                        ObjOffer.IsOffer = false;
        //                                                                                        ObjOffer.Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                                        MyOffer.Add(ObjOffer);
        //                                                                                    }*/
        //                                                                                }
        //                                                                            }
        //                                                                            #endregion
        //                                                                        }
        //                                                                        else
        //                                                                        {
        //                                                                            ObjOffer = new Offers();
        //                                                                            ApplyingOffer = "No Offer";
        //                                                                            ObjOffer.IsOffer = false;
        //                                                                            ObjOffer.ValidFrom = InOutDate[y].ToString();
        //                                                                            ObjOffer.ValidTo = InOutDate[y].AddDays(1).ToString();
        //                                                                            decimal Prices = Convert.ToDecimal(objRoomRate[x].amount);
        //                                                                            float rate = (float)Convert.ToDouble(Prices);
        //                                                                            rate = (rate * Supplier_ExchangeRate) / ExchangeRate;
        //                                                                            ObjOffer.OfferBaseRate = Convert.ToDecimal(rate + BaseExtraBed + BaseKid);
        //                                                                            float Amt = MarkupTaxManager.GetCutRoomAmount(rate, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                                                            Amt = Amt + BaseExtraBed + BaseKid + AgentRoomMarkup;
        //                                                                            ObjOffer.AgentMarkupOffer = (decimal)AgentRoomMarkup;
        //                                                                            ObjOffer.RoomPrice = Convert.ToDecimal(Amt);
        //                                                                            ObjOffer.ApplyingOffer = ApplyingOffer;
        //                                                                            //ObjOffer.Sid = Convert.ToInt64(OfferDayListNew[t].Sid);
        //                                                                            ObjOffer.Hotel_Id = Convert.ToInt64(OfferDayListNew[t].Hotel_Id);
        //                                                                            ObjOffer.Room_Id = Convert.ToInt64(OfferDayListNew[t].Room_Id);
        //                                                                            ObjOffer.DateType = "No Offer";
        //                                                                            ObjOffer.Id = s;
        //                                                                            MyOffer.Add(ObjOffer);
        //                                                                        }
        //                                                                    }// Checking Meal Plan
        //                                                                }// Loop for All dates
        //                                                            }//Checking Common dates
        //                                                        }// END Loop for All Seasons & Block Dates for perticular Offer
        //                                                    }
        //                                                }// Checking Nationality
        //                                            }//END Getting All Seasons & Block Dates for perticular Offer
        //                                        }// END Loop for Offer Available for this Hotel & Room
        //                                    }
        //                                    else
        //                                    {
        //                                        //objRoom[j].Offer = false;
        //                                    }
        //                                    #endregion
        //                                    //For Comment Offer, uncomment below line
        //                                    //MyOffer = new List<Offers>();
        //                                    //For Comment Offer, uncomment above line
        //                                    objRoomRate[x].RoomAmount = BaseAmt;
        //                                    objRoomRate[x].CutRoomAmount = MarkupTaxManager.GetCutRoomAmount(BaseAmt, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                    decimal rPrice = 0;
        //                                    decimal OfferAgentMarkup = 0;
        //                                    // Calculating Total Offer Agent Markup
        //                                    if (MyOffer.Count > 0)
        //                                    {
        //                                        for (int g = 0; g < MyOffer.Count; g++)
        //                                        {
        //                                            rPrice += MyOffer[g].RoomPrice;
        //                                            OfferAgentMarkup += MyOffer[g].AgentMarkupOffer;
        //                                            MyOffer[g].AgentMarkup = (decimal)AgentRoomMarkup;
        //                                        }
        //                                        AgentRoomMarkup = (float)OfferAgentMarkup;
        //                                    }
        //                                    //END Calculating Total Offer Agent Markup
        //                                    objRoomCategory[j].OfferList = MyOffer;
        //                                    objRoomRate[x].RoomAmount = objRoomRate[x].RoomAmount + BaseExtraBed + BaseKid;
        //                                    objRoomRate[x].CutRoomAmount = objRoomRate[x].CutRoomAmount + BaseExtraBed + BaseKid;
        //                                    objRoomRate[x].AgentRoomMarkup = AgentRoomMarkup;

        //                                    objMGHHotelDetails[i].HotelId = Convert.ToInt64(ndHotel.Attributes.GetNamedItem("id").Value);
        //                                    //var Offers = OfferList.Select(x => x.Hotel_Id == objMGHHotelDetails[i].HotelId).ToList();

        //                                    //if (objRoomRate[x].type == "aop")
        //                                    //{
        //                                    //CutAmount += BaseAmt;
        //                                    RoomAmount += objRoomRate[x].RoomAmount;
        //                                    CutAmount += objRoomRate[x].CutRoomAmount;
        //                                    AgentMarkup += AgentRoomMarkup;
        //                                    //}
        //                                    #region test
        //                                    XmlNodeList ndRefundPolicyList = ndRateList[j].SelectNodes("refundPolicy/policy");
        //                                    RefundPolicy[] objRefundPolicy = new RefundPolicy[ndRefundPolicyList.Count];
        //                                    objRoomRate[x].refundPolicy = new List<RefundPolicy>();

        //                                    //DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new DataLayer.MarkupsAndTaxes();
        //                                    //if (context.Session["markups"] != null)
        //                                    //    objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["markups"];
        //                                    //else if (context.Session["AgentMarkupsOnAdmin"] != null)
        //                                    //    objMarkupsAndTaxes = (DataLayer.MarkupsAndTaxes)context.Session["AgentMarkupsOnAdmin"];
        //                                    //float RoomAmount = lstRoomRate.Where(data => data.type == "aop").Select(data => data.amount).FirstOrDefault();

        //                                    for (int l = 0; l < ndRefundPolicyList.Count; l++)
        //                                    {

        //                                        objRefundPolicy[l] = new RefundPolicy();
        //                                        objRefundPolicy[l].dayMin = ndRefundPolicyList[l].Attributes.GetNamedItem("dayMin").Value;
        //                                        objRefundPolicy[l].dayMax = ndRefundPolicyList[l].Attributes.GetNamedItem("dayMax").Value;
        //                                        objRefundPolicy[l].deduction = float.Parse(ndRefundPolicyList[l].Attributes.GetNamedItem("deduction").Value);
        //                                        objRefundPolicy[l].unit = ndRefundPolicyList[l].Attributes.GetNamedItem("unit").Value;
        //                                        DateTime CancellationDate = Convert.ToDateTime(objMGHHotelDetails[i].DateFrom).AddHours(-(objMarkupsAndTaxes.TimeGap + (Convert.ToInt32(objRefundPolicy[l].dayMax) * 24)));
        //                                        if (CancellationDate < DateTime.Now)
        //                                        {
        //                                            objRefundPolicy[l].nonRefundable = true;
        //                                        }
        //                                        if (rPrice != 0)
        //                                            objRefundPolicy[l].CancellationAmount = ((float)rPrice - (float)OfferAgentMarkup) * objRefundPolicy[l].deduction / 100;
        //                                        else
        //                                            objRefundPolicy[l].CancellationAmount = BaseAmt * objRefundPolicy[l].deduction / 100;
        //                                        objRefundPolicy[l].CUTCancellationAmount = MarkupTaxManager.GetCutRoomAmount(objRefundPolicy[l].CancellationAmount, currency, NoofRooms, Night, "Mgh", out AgentRoomMarkup);
        //                                        objRefundPolicy[l].CancellationAmount = objRefundPolicy[l].CancellationAmount + BaseExtraBed + BaseKid;
        //                                        objRefundPolicy[l].CUTCancellationAmount = objRefundPolicy[l].CUTCancellationAmount + BaseExtraBed + BaseKid;
        //                                        objRoomRate[x].refundPolicy.Add(objRefundPolicy[l]);
        //                                    }
        //                                    #endregion test
        //                                    objRoomInfo[k].roomRate.Add(objRoomRate[x]);
        //                                }
        //                                objRateList[j].roomCategory.Add(objRoomCategory[j]);
        //                                //lstrefundPolicy = objRoomRate[x].refundPolicy;
        //                                //end testing  ........................................................................................................................................
        //                            }
        //                            objRateList[j].roomInfo.Add(objRoomInfo[k]);
        //                        }
        //                        //objRateList[j].refundPolicy = lstrefundPolicy;
        //                        objRateList[j].Price = RoomAmount;
        //                        objRateList[j].CUTPrice = CutAmount;
        //                        objRateList[j].AgentMarkup = AgentMarkup;

        //                        XmlNodeList ndMealPlanList = ndRateList[j].SelectNodes("mealPlans/mealPlan");
        //                        MealPlan[] objMealPlan = new MealPlan[ndMealPlanList.Count];
        //                        objRateList[j].mealPlan = new List<MealPlan>();
        //                        for (int k = 0; k < ndMealPlanList.Count; k++)
        //                        {
        //                            objMealPlan[k] = new MealPlan();
        //                            objMealPlan[k].code = ndMealPlanList[k].Attributes.GetNamedItem("code").Value;
        //                            objMealPlan[k].start = ndMealPlanList[k].Attributes.GetNamedItem("start").Value;
        //                            objMealPlan[k].end = ndMealPlanList[k].Attributes.GetNamedItem("end").Value;
        //                            objMealPlan[k].text = ndMealPlanList[k].InnerText;
        //                            objRateList[j].mealPlan.Add(objMealPlan[k]);
        //                        }
        //                        objMGHHotelDetails[i].rate.Add(objRateList[j]);
        //                    }
        //                    objMGHHotelDetails[i].RoomCategory = objRoomCategory.ToList();
        //                    //if (objMGHHotelDetails[i].rate.Count > 0)
        //                    //{
        //                    var Numbers = new float[objMGHHotelDetails[i].rate.Count];
        //                    for (int c = 0; c < objMGHHotelDetails[i].rate.Count; c++)
        //                    {
        //                        //float f1 = objMGHHotelDetails[i].rate[c].refundPolicy[0].CutRoomAmount;
        //                        float f1 = objMGHHotelDetails[i].rate[c].CUTPrice;
        //                        Numbers[c] = f1;
        //                    }
        //                    float MinValue = 0;
        //                    if (objMGHHotelDetails[i].rate.Count != 0)
        //                        MinValue = Numbers.Min();
        //                    else
        //                        break;

        //                    objMGHHotelDetails[i].CUTPrice = MinValue;
        //                    objMGHHotelDetails[i].AgentMarkup = objMGHHotelDetails[i].rate.Where(data => data.CUTPrice == MinValue).Select(data => data.AgentMarkup).FirstOrDefault();
        //                    objMGHHotelDetails[i].Price = objMGHHotelDetails[i].rate.Where(data => data.CUTPrice == MinValue).Select(data => data.Price).FirstOrDefault();
        //                    //objMGHHotelDetails[i].AgentMarkup = objMGHHotelDetails[i].rate.Where(data => data.CutAmount == MinValue).Select(data => data.AgentMarkup).FirstOrDefault();

        //                    //.....................

        //                    XmlNodeList ndImageList = ndHotel.SelectNodes("mediaList/media");

        //                    Image[] objImage = new Image[ndImageList.Count];

        //                    for (int j = 0; j < ndImageList.Count; j++)
        //                    {
        //                        objImage[j] = new Image();
        //                        objImage[j].Title = ndImageList[j].Attributes.GetNamedItem("title").Value;
        //                        objImage[j].IsDefault = Convert.ToBoolean(ndImageList[j].Attributes.GetNamedItem("default").Value);
        //                        objImage[j].Type = ndImageList[j].Attributes.GetNamedItem("type").Value;
        //                        objImage[j].Url = ndImageList[j].Attributes.GetNamedItem("src").Value;
        //                        objMGHHotelDetails[i].MediaList.Add(objImage[j]);
        //                    }
        //                    //context.Session["HotelMGH" + i] = objMGHHotelDetails[i];
        //                    MGHHotelDetails.Add(objMGHHotelDetails[i]);
        //                    //}
        //                    //HttpContext.Current.Session["Hotel" + i] = objMGHHotelDetails[i];
        //                }
        //            }
        //            context.Session["HotelMGH"] = MGHHotelDetails;
        //        }
        //        catch (Exception ex)
        //        {
        //            string Line = ex.StackTrace.ToString();
        //        }
        //        List_Category = List_Category.OrderBy(data => data.CategoryName).ToList();
        //        List_Facility = List_Facility.OrderBy(data => data.FacilitiesName).ToList();
        //        List_Location = List_Location.OrderBy(data => data.LocationName).ToList();
        //        return nStatus;
        //    }
        //    else
        //        context.Session["MGHAvail"] = null;
        //        context.Session["MGHNumberOfHotels"] = null;
        //    return nStatus;
        //}
        public float Markuptax(float Servicetax, float BaseAmount)
        {
            float Markuptax = (Servicetax / 100) * BaseAmount;
            return Markuptax;
        }

        public List<MGHLib.Response.Facilities> Facility
        {
            get { return List_Facility; }
        }
        public List<MGHLib.Response.Location> Location
        {
            get { return List_Location; }
        }
        public List<MGHLib.Response.Category> Category
        {
            get { return List_Category; }
        }

        public bool ParseHotelDetails(string xmlResponse, out MGHHotelDetails objMGHHotelDetails)
        {
            CutAdmin.Models.MGHHotel objMGHHotel = (CutAdmin.Models.MGHHotel)HttpContext.Current.Session["MGHAvail"];
            DBHelper.DBReturnCode retCode;
            DataTable dtResult;
            objMGHHotelDetails = null;
            //MGHHotelDetails objMGHHotelDetails = null;
            //MGHHotelDetails objMGHHotelDetails = new MGHHotelDetails();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            XmlNode ndResponseStatus = doc.SelectSingleNode("getHotelDetail_response/responseCode");
            bool bStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);
            if (bStatus == true)
            {
                XmlNode ndHotel = doc.SelectSingleNode("getHotelDetail_response/hotel");
                Int64 HotelID = Convert.ToInt64(ndHotel.SelectSingleNode("id").InnerText);

                for (int i = 0; i < objMGHHotel.HotelDetail.Count; i++)
                {
                    if (objMGHHotel.HotelDetail[i].HotelId == HotelID)
                        objMGHHotelDetails = objMGHHotel.HotelDetail[i];
                }
                Locality objLocality = new Locality();
                objLocality.LocalityId = Convert.ToInt64(ndHotel.SelectSingleNode("locality").InnerText);
                objMGHHotelDetails.Locality = objLocality;

                State objState = new State();
                objState.StateId = Convert.ToInt64(ndHotel.SelectSingleNode("state").Attributes.GetNamedItem("id").Value);
                objState.StateName = ndHotel.SelectSingleNode("state").InnerText;
                objMGHHotelDetails.State = objState;

                objMGHHotelDetails.Zip = ndHotel.SelectSingleNode("zip").InnerText;
                objMGHHotelDetails.Address = ndHotel.SelectSingleNode("address").InnerText + "," + ndHotel.SelectSingleNode("locality").Attributes.GetNamedItem("region_name").Value + " , " + ndHotel.SelectSingleNode("state").InnerText + "-" + ndHotel.SelectSingleNode("zip").InnerText + " . ";
                try
                {
                    objMGHHotelDetails.Longitude = double.Parse(ndHotel.SelectSingleNode("geoData/longitude").InnerText);
                    objMGHHotelDetails.Latitude = double.Parse(ndHotel.SelectSingleNode("geoData/latitude").InnerText);
                }
                catch
                {
                    objMGHHotelDetails.Longitude = 0.0;
                    objMGHHotelDetails.Latitude = 0.0;
                }
                Contact objContact = new Contact();
                objContact.ContactName = ndHotel.SelectSingleNode("contactDetail/name").InnerText;
                objContact.ContactEmail = ndHotel.SelectSingleNode("contactDetail/email").InnerText;
                objContact.ContactPhone = ndHotel.SelectSingleNode("contactDetail/phone").InnerText;
                objContact.ContactMobile = ndHotel.SelectSingleNode("contactDetail/mobile").InnerText;
                objMGHHotelDetails.ContactDetails = objContact;

                XmlNodeList ndImageList = ndHotel.SelectNodes("mediaList/media");
                Image[] objImage = new Image[ndImageList.Count];
                objMGHHotelDetails.MediaList = new List<Image>();
                for (int j = 0; j < ndImageList.Count; j++)
                {
                    objImage[j] = new Image();
                    objImage[j].Title = ndImageList[j].Attributes.GetNamedItem("title").Value;
                    objImage[j].IsDefault = Convert.ToBoolean(ndImageList[j].Attributes.GetNamedItem("default").Value);
                    objImage[j].Type = ndImageList[j].Attributes.GetNamedItem("type").Value;
                    objImage[j].Url = ndImageList[j].Attributes.GetNamedItem("src").Value;
                    objMGHHotelDetails.MediaList.Add(objImage[j]);
                }

                XmlNodeList ndFacilityList = ndHotel.SelectNodes("facilities/facility");
                Facilities[] objFacilities = new Facilities[ndFacilityList.Count];
                objMGHHotelDetails.Facilities = new List<Facilities>();
                for (int j = 0; j < ndFacilityList.Count; j++)
                {
                    objFacilities[j] = new Facilities();
                    objFacilities[j].FacilitiesId = Convert.ToInt64(ndFacilityList[j].Attributes.GetNamedItem("id").Value);
                    objFacilities[j].FacilitiesName = ndFacilityList[j].Attributes.GetNamedItem("name").Value;
                    objMGHHotelDetails.Facilities.Add(objFacilities[j]);
                }

                objMGHHotelDetails.Description = ndHotel.SelectSingleNode("description").InnerText;

                XmlNodeList ndRoomCategory = ndHotel.SelectNodes("roomCategories/roomCategory");
                RoomCategory[] objRoomCategory = new RoomCategory[ndRoomCategory.Count];
                objMGHHotelDetails.RoomCategory = new List<RoomCategory>();
                for (int j = 0; j < ndRoomCategory.Count; j++)
                {
                    objRoomCategory[j] = new RoomCategory();
                    objRoomCategory[j].RoomCategoryId = Convert.ToInt64(ndRoomCategory[j].Attributes.GetNamedItem("id").Value);
                    objRoomCategory[j].RoomCategoryName = ndRoomCategory[j].Attributes.GetNamedItem("name").Value;
                    objRoomCategory[j].RoomCategoryCapacity = Convert.ToInt64(ndRoomCategory[j].Attributes.GetNamedItem("capacity").Value);

                    XmlNodeList ndRoomFacilityList = ndRoomCategory[j].SelectNodes("roomFacilities/facility");
                    RoomFacilities[] objRoomFacility = new RoomFacilities[ndRoomFacilityList.Count];
                    objRoomCategory[j].roomFacilities = new List<RoomFacilities>();
                    for (int k = 0; k < ndRoomFacilityList.Count; k++)
                    {
                        objRoomFacility[k] = new RoomFacilities();
                        objRoomFacility[k].Id = Convert.ToInt64(ndRoomFacilityList[k].Attributes.GetNamedItem("id").Value);
                        //ADD NAME FROM DATABASE
                        retCode = MGHManager.GetRoomFacilityNameById(objRoomFacility[k].Id, out dtResult);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            objRoomFacility[k].Name = dtResult.Rows[0]["name"].ToString();
                        }
                        objRoomCategory[j].roomFacilities.Add(objRoomFacility[k]);
                    }

                    XmlNodeList ndRoomMediaList = ndRoomCategory[j].SelectNodes("mediaList/media");
                    Image[] objRoomImage = new Image[ndRoomMediaList.Count];
                    objRoomCategory[j].MediaList = new List<Image>();
                    for (int k = 0; k < ndRoomMediaList.Count; k++)
                    {
                        objRoomImage[k] = new Image();
                        objRoomImage[k].Title = ndRoomMediaList[k].Attributes.GetNamedItem("title").Value;
                        objRoomImage[k].IsDefault = Convert.ToBoolean(ndRoomMediaList[k].Attributes.GetNamedItem("default").Value);
                        objRoomImage[k].Type = ndRoomMediaList[k].Attributes.GetNamedItem("type").Value;
                        objRoomImage[k].Url = ndRoomMediaList[k].Attributes.GetNamedItem("src").Value;
                        objRoomCategory[j].MediaList.Add(objRoomImage[k]);
                    }
                    objMGHHotelDetails.RoomCategory.Add(objRoomCategory[j]);
                }

                XmlNodeList ndAccomodationPolicies = ndHotel.SelectNodes("accomodationPolicies/policy");
                AccomodationPolicies[] objAccomodationPolicies = new AccomodationPolicies[ndAccomodationPolicies.Count];
                objMGHHotelDetails.AccomodationPolicies = new List<AccomodationPolicies>();
                for (int j = 0; j < ndAccomodationPolicies.Count; j++)
                {
                    objAccomodationPolicies[j] = new AccomodationPolicies();
                    objAccomodationPolicies[j].attribute = ndAccomodationPolicies[j].Attributes.GetNamedItem("attribute").Value;
                    objAccomodationPolicies[j].details = ndAccomodationPolicies[j].InnerText;
                    objMGHHotelDetails.AccomodationPolicies.Add(objAccomodationPolicies[j]);
                }
                HttpContext.Current.Session["HotelDetails"] = objMGHHotelDetails;
            }
            return bStatus;
        }


        public bool GetBlockID(string xmlResponse, out string BlockID)
        {
            BlockID = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            XmlNode ndResponseStatus = doc.SelectSingleNode("blockRoom_response/responseCode");
            bool bStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);
            if (bStatus == true)
            {
                XmlNode ndHotel = doc.SelectSingleNode("blockRoom_response/blockRefNo");
                BlockID = (ndHotel.InnerText);
                return bStatus;
            }
            else
            {
                return bStatus;
            }
            //return BlockID;
        }

        public string GetBookingID(string xmlResponse)
        {

            string BookingID = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            //if (doc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
            //    doc.RemoveChild(doc.FirstChild);
            XmlNode ndResponseStatus = doc.SelectSingleNode("bookRoom_response/responseCode");
            //string sStatus = ndResponseStatus.Attributes.GetNamedItem("success").Value;
            bool bStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);
            //bool bStatus = false;
            //if (sStatus == "true")
            //{
            //    bStatus = true;
            //}
            //else
            //{
            //    bStatus = false;
            //}
            if (bStatus == true)
            {
                XmlNode ndHotel = doc.SelectSingleNode("bookRoom_response/bookRefNo");
                BookingID = (ndHotel.InnerText);
            }
            return BookingID;
        }


        public bool GetCancelStatus(string xmlResponse)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            XmlNode ndResponseStatus = doc.SelectSingleNode("processCancelBooking_response/responseCode");
            bool bStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);

            return bStatus;
        }

        public string GetCancelDetail(string xmlResponse)
        {
            string Details = "";
            string BookRef = "";
            string RefundAmt = "";
            string cancelRefNo = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlResponse);
            XmlNode ndResponseStatus = doc.SelectSingleNode("processCancelBooking_response/responseCode");
            bool bStatus = Convert.ToBoolean(ndResponseStatus.Attributes.GetNamedItem("success").Value);
            if (bStatus == true)
            {
                XmlNode ndHotel = doc.SelectSingleNode("processCancelBooking_response/bookRefNo");
                BookRef = (ndHotel.InnerText);

                XmlNode Ammount = doc.SelectSingleNode("processCancelBooking_response/refundAmount");
                RefundAmt = (Ammount.InnerText);

                XmlNode RefNo = doc.SelectSingleNode("processCancelBooking_response/cancelRefNo");
                cancelRefNo = (RefNo.InnerText);
            }
            Details = BookRef + "|" + RefundAmt + "|" + cancelRefNo;
            return Details;
        }

        public List<MGHHotelDetails> GetServiceHotel()
        {

            List<MGHHotelDetails> lstHotels = new List<MGHHotelDetails>();
            List<MGHHotelDetails> MGHHotelDetails = new List<MGHHotelDetails>();
           
            if (context.Session["MGHNumberOfHotels"] != null)
            {

                // int hotelCount = Convert.ToInt32(HttpContext.Current.Session["MGHNumberOfHotels"]);

                MGHHotelDetails = (List<MGHHotelDetails>)context.Session["HotelMGH"];
                int hotelCount = MGHHotelDetails.Count;
                for (int i = 0; i < hotelCount; i++)
                {
                    if (MGHHotelDetails[i] != null)
                    {
                        lstHotels.Add(MGHHotelDetails[i]);
                    }
                }
            }

            return lstHotels;
        }


        private float GetTotalMarkup(float BookingAmt, float MarkupPer, float MarkupAmt, float CommiPer, float CommAmt)
        {
            float ActualMarkupPerAmt = (MarkupPer / 100) * BookingAmt;
            float ActualCommiPerAmt = (CommiPer / 100) * BookingAmt;
            float ActualGroupMarkTotal = Getgreatervalue(ActualMarkupPerAmt, MarkupAmt) - Getgreatervalue(ActualCommiPerAmt, CommAmt);
            return ActualGroupMarkTotal;

        }
        private float GetAgentOwnMarkupAmt(float BookingAmt, float PercentageMarkup, float Amount)
        {
            float ActualMarkupAmt = (PercentageMarkup / 100) * BookingAmt;
            float ActualAgentMarkup = Getgreatervalue(ActualMarkupAmt, Amount);
            return ActualAgentMarkup;
        }

        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }
       
        /*.........................................................................................*/
    }
}