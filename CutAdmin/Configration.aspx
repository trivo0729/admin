﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Configration.aspx.cs" Inherits="CutAdmin.Configration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ManageProfile.js?v=1.2"></script>
    <script src="../HotelAdmin/Scripts/ChangePassword.js"></script>
    <script src="Scripts/SaveMarkUp.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <div class="with-padding">
            <%--<div class="large-box-shadow white-gradient with-border">--%>

            <%--<div class="blue-gradient panel-control">
                    <h3 style="color: white"><span class="icon-gear"></span> Configuration</h3>
                </div>--%>
            <hgroup id="main-title" class="thin">
                <h1>Configuration</h1>
                <hr>
            </hgroup>
            <div class="side-tabs same-height margin-bottom">
                <ul class="tabs">
                    <li class="active"><a href="#sidetab-1">Manage Profile</a></li>
                    <li><a href="#sidetab-2">Change Password</a></li>
                    <li><a href="#sidetab-3">Global Markups</a></li>
                </ul>

                <div class="tabs-content">

                    <div id="sidetab-1" class="with-padding">

                        <div class="columns">
                            <h5 style="padding-left: 1%">Personal Detail</h5>
                            <hr />
                            <div class="three-columns">
                                <label>First Name</label><br>

                                <input type="text" id="txtFirstName" class="input" style="width: 92%" autocomplete="off" />
                            </div>

                            <div class="three-columns">
                                <label>Last Name</label><br>

                                <input type="text" id="txtLastName" class="input" style="width: 92%" autocomplete="off" />
                            </div>

                            <div class="three-columns">
                                <label>Designation</label><br>
                                <input type="text" id="txtDesignation" class="input" style="width: 92%" autocomplete="off" />
                            </div>
                            <div class="three-columns">
                                <label>Mobile</label><br>
                                <input type="text" id="txtMobile" class="input" style="width: 92%" autocomplete="off" />
                            </div>

                        </div>


                        <div class="columns">
                            <h5 style="padding-left: 1%">Other Detail</h5>
                            <hr />
                            <div class="four-columns">
                                <label>Company Name</label><br />
                                <input type="text" id="txtCompanyName" class="input" style="width: 92%" autocomplete="off" />
                            </div>

                            <div class="four-columns">
                                <label>Company mail</label><br />
                                <input type="text" id="txtCompanymail" class="input" style="width: 92%" autocomplete="off" readonly />
                            </div>

                            <div class="four-columns">
                                <label>Phone</label><br />
                                <input type="text" id="txtPhone" class="input" style="width: 92%" autocomplete="off" />
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns full-width">
                                <label>Address</label><br />
                                <input type="text" id="txtAddress" class="input" style="width: 97.5%" autocomplete="off" />
                            </div>
                        </div>

                        <div class="columns">
                            <div class="four-columns" id="drp_Nationality">
                                <label>Nationality</label>
                                <p class="button-height">
                                    <select id="sel_Nationality" class="select full-width replacement select-styled-list tracked OfferType">
                                    </select>
                            </div>
                            <div class="four-columns" id="drp_City">
                                <label>City</label>
                                <p class="button-height">
                                    <select id="sel_City" class="select full-width replacement select-styled-list tracked OfferType">
                                    </select>
                            </div>

                            <div class="four-columns">
                                <label>Pin Code</label><br />
                                <input type="text" id="txtPinCode" class="input" style="width: 92%" autocomplete="off" />
                            </div>
                        </div>

                        <div class="columns">
                            <div class="four-columns">
                                <label>Pan No</label><br />
                                <input type="text" id="txtPanNo" class="input" style="width: 92%" autocomplete="off" />
                            </div>

                            <div class="four-columns">
                                <label>Fax No</label><br />
                                <input type="text" id="txtFaxNo" class="input" style="width: 92%" autocomplete="off" />
                            </div>
                            <div class="four-columns">
                                <label>Logo</label><br />
                                <input id="Logo" class="btn-search5 input" style="width: 92%" type="file" />
                                <br />
                            </div>
                        </div>
                        <div class="columns">
                            <a class="button anthracite-gradient" onclick="Save()" style="float: right;">Update</a>
                        </div>
                        <br />

                    </div>

                    <div id="sidetab-2" class="with-padding">

                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <label>Current Password</label><br>
                                <div class="input full-width">
                                    <input id="txtOldPassword" type="password" class="input-unstyled full-width">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none;" id="lbl_OldPassword">
                                    <b>* This field is required</b>
                                </label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <label>New Password</label><br>
                                <div class="input full-width">
                                    <input id="txtNewPassword" type="password" class="input-unstyled full-width">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_NewPassword">
                                    <b>* This field is required</b>
                                </label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <label>Confirm New Password</label><br>
                                <div class="input full-width">
                                    <input id="txtConfirmNewPassword" type="Password" class="input-unstyled full-width">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmNewPassword">
                                    <b>* This field is required</b>
                                </label>
                            </div>
                        </div>
                        <div class="columns">
                            <input id="btnSubmit" type="button" value="Save Changes" style="float: right;" class="button anthracite-gradient UpdateMarkup" onclick="ChangePassword()">
                        </div>
                        <input type="hidden" id="hdnpassword" />
                    </div>

                    <div id="sidetab-3" class="with-padding">
                        <h6>Hotel</h6>
                        <hr />
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <label>Percentage</label><br>
                                <div class="input full-width">
                                    <input id="txtPercentage" type="text" class="input-unstyled full-width">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none;" id="">
                                    <b>* This field is required</b>
                                </label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <label>Amount</label><br>
                                <div class="input full-width">
                                    <input id="txtAmount" type="text" class="input-unstyled full-width">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="">
                                    <b>* This field is required</b>
                                </label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <br />
                                <p class="text-right" style="margin-top: 4%">
                                    <input id="BtnMarkUp" type="button" value="" class="button anthracite-gradient UpdateMarkup" title="" style="width: 17%" onclick="SaveMarkUp()">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--</div>--%>
        </div>


    </section>

</asp:Content>
