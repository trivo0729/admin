﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CropImage.aspx.cs" Inherits="CutAdmin.CropImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        $(window).load(function () {
            var jcrop_api, jcrop_api1;
            var i, ac;

            initJcrop();
            // initJcroped();

            function initJcrop() {
                jcrop_api = $.Jcrop('#imgCrop', {
                    onSelect: storeCoords,
                    onChange: storeCoords
                });
                //jcrop_api.setOptions({ aspectRatio: 16/ 15 });
                //jcrop_api.setOptions({
                //    minSize: [250, 250],
                //    maxSize: [250, 350]
                //});
                //jcrop_api.setSelect([140, 180, 160, 180]);
            };
            //function initJcroped() {
            //    jcrop_api1 = $.Jcrop('#imgCropped', {
            //        onSelect: storeCoords,
            //        onChange: storeCoords
            //    });
            //    //jcrop_api.setOptions({ aspectRatio: 16/ 15 });
            //    //jcrop_api.setOptions({
            //    //    minSize: [250, 250],
            //    //    maxSize: [250, 350]
            //    //});
            //    //jcrop_api.setSelect([140, 180, 160, 180]);
            //};
            function storeCoords(c) {
                jQuery('#X').val(c.x);
                jQuery('#Y').val(c.y);
                jQuery('#Width').val(c.w);
                jQuery('#Height').val(c.h);
            };
            try {
                <%-- (function () {
                    orig = $.fn.css;
                    $.fn.css = function () {
                        var result = orig.apply(this, arguments);
                        $(this).trigger("stylechanged");
                        //alert("Ji");
                        document.getElementById("<%=Button1.ClientID %>").click();
                        //document.getElementById("<%=HiddenField1.ClientID %>").value ="000";
                        return result;
                    }
                })();--%>
              <%--  $('.jcrop-tracker').click(function (e) {
                    var color = $(this).text();
                    //document.getElementById("<%=Button1.ClientID %>").click();
                   // $('.results').css('background-color', color);
                });
                $(".jcrop-tracker").change(function () {
                    alert("Handler for .change() called.");
                });--%>
                //$(".jcrop-tracker").change(function () {
                //    alert("Ji");
                //});
            }
            catch (ex) {

            }
        });
        function SeeCroped() {
            $("#myModal").modal("show")
        }

        function goBack() {
            window.history.back();
        }
    </script>

</head>
<body>
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Images</h1>

        </hgroup>
        <div class="with-padding">
            <%--<form action="#" class="frmSupplier">--%>
            <form id="form3" runat="server" class="frmSupplier">
                    <div class="demo">
                        <div id="navbar">
                            <!-- Nav tabs -->
                            
                            <!-- Tab panes -->
                            <div class="tab-content5">
                                <!-- Tab 1 -->
                                <div class="tab-pane active" id="Bank">
                                    <div class="twelve-columns bold" >  
                  
                                        <table class=" table table-bordered" >
                        <tr>
                            <td>
                                       <asp:Panel ID="pnlUpload" runat="server">
                                           <span  class=" green2 size14"><b>Upload File</b></span>   
                <asp:FileUpload ID="Upload" runat="server" CssClass="form-control"/>
               
                <script type="text/javascript">
                    function UploadFile(fileUpload) {
                        if (fileUpload.value != '') {
                            document.getElementById("<%=btnUpload.ClientID %>").click();
                        }
                    }

                                                            </script>
                <asp:Label ID="lblError" runat="server" Visible="false" />

            </asp:Panel>
                                <table>
                                    <tr>
                                        <td><asp:Button ID="btnCrop" runat="server" Text="Crop" OnClick="btnCrop_Click" CssClass="button anthracite-gradient" />
                    <asp:Button ID="Button2" runat="server" Text="Upload Crop" CssClass="button anthracite-gradient" Visible="false" OnClick="Button2_Click" />
                 <input type="button" class="btn-search margtop-2"" value="Back" onclick="goBack()" />
                 <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value=""  />
                 <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" Style="display: none" /></td>
                                    </tr>
                                </table>
                            </td>
                            <td> 
                                <table>
                                    <tr>
                                        <td>
                                             <span  class=" green2 size14"><b>Origional Size</b></span>     <asp:Label ID="Label2" runat="server" Text="0 Kb" CssClass="orange size14"></asp:Label>
                                        </td>
                                        <td>
                                               <span  class=" green2 size14"><b>Reduce  Size</b></span> 
                                            <input   type="number" min="5" max="100" name="sel_Quality" value="80" onkeyup="ReduceQuality(this)"/>
                                          
                                             <asp:Button ID="btn_ReduceSize" runat="server" OnClick="btn_ReduceSize_Click" Text="Reduce"  Style="display: none"  />
                <script type="text/javascript">
                    function ReduceQuality(fileUpload) {
                        debugger;
                        if (fileUpload.value != '') {
                            document.getElementById("<%=btn_ReduceSize.ClientID %>").click();
                        }
                    }

                                                            </script>
                                        </td>

                                        <td>
                                            <asp:Button ID="Button3" runat="server" Text="Upload Crop" CssClass="btn-search margtop-2" Visible="false" OnClick="Button2_Click" />
                                        </td>
                                    </tr>
                                    <tr>
   <td> <span  class=" green2 size14"><b> Quality</b></span><asp:Label ID="Label3" runat="server" Text="100 %" CssClass="orange size14">100%</asp:Label>
                             </td>
<asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" Style="display: none" />
                             <td>   <span  class=" green2 size14"><b>New Size</b></span>     <asp:Label ID="Label1" runat="server" Text="0 Kb" CssClass="orange size14"></asp:Label> 
                                
                            </td>
                            <td>

                            </td>

                        </tr>
                                
                                </table>

                            </td>
                            
                        </tr>

                    </table>
                
                                    </div>
                                    <div class="twelve-columns bold ScrollDiv">
                                              <asp:Panel ID="pnlCrop" runat="server" Visible="false">
                                    <center><asp:Image ID="imgCrop" runat="server"  CssClass="" /></center>    
                                        <asp:HiddenField ID="X" runat="server" />
                                        <asp:HiddenField ID="Y" runat="server" />
                                        <asp:HiddenField ID="Width" runat="server" />
                                        <asp:HiddenField ID="Height" runat="server" OnValueChanged="Height_ValueChanged" />
                                                  <%--<asp:TextBox ID="Xx" runat="server"></asp:TextBox>
                                                  <asp:TextBox ID="Yy" runat="server"></asp:TextBox>
                                                  <asp:TextBox ID="Xwidth" runat="server"></asp:TextBox>
                                                  <asp:TextBox ID="Xheight" runat="server"></asp:TextBox>--%>
                                    </asp:Panel>
                                         
                                                        <asp:Panel ID="pnlCropped" runat="server" Visible="false">
                                        <asp:Image ID="imgCropped" runat="server"  CssClass=" thumbnail"/>
                                    </asp:Panel>
                                        </div>
                                </div>
                              
                                
                                
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </section>

</body>
</html>

