﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD = System.Drawing;

namespace CutAdmin
{
    public partial class CropImage : System.Web.UI.Page
    {
        public Boolean CropedAgain { get; set; }
        String path = HttpContext.Current.Request.PhysicalApplicationPath + "VisaImages\\";
        string RefNo, FileNo;
        bool Croped;
        protected void Page_Load(object sender, EventArgs e)
        {
            RefNo = Request.QueryString["RefId"];
            FileNo = Request.QueryString["File"];

            Upload.Attributes["onchange"] = "UploadFile(this)";
            //drop_SelQuality.Attributes["onchange"] = "ReduceQuality(this)";
            try
            {
                if (IsPostBack == false)
                {
                    string sFolderName = Server.MapPath("~/VisaImages/");
                    string sFileName = RefNo + "_" + FileNo + ".jpg";
                    string[] files = Directory.GetFiles(sFolderName);
                    //  var Crop = files.All(d => files.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))));

                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            byte[] Image = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath("~/VisaImages/" + "//" + sFileName));
                            if (Image.Length > 34000)
                            {
                                SetImagegQuality(Image, 80, sFileName);


                                Croped = true;

                                pnlUpload.Visible = true;
                                pnlCrop.Visible = true;
                                //imgCrop.ImageUrl = "~/VisaImages/crop/" + RefNo + "_" + FileNo + ".jpg";
                                Image = ReadImageFile(path + "crop\\" + sFileName);
                                imgCrop.ImageUrl = GetPath(Image);
                                Session["Compess"] = true;
                                //Label2.Text = String.Format("{0:0.00}",byteArray)+ "Kb";
                                Session["UploadImage"] = sFileName;

                            }
                            else
                            {
                                pnlUpload.Visible = true;
                                pnlCrop.Visible = true;
                                imgCrop.ImageUrl = "~/VisaImages/" + RefNo + "_" + FileNo + ".jpg";
                                Session["UploadImage"] = sFileName;
                                FileStream fileStream = new FileStream(fileName, FileMode.Open);
                                float byteArray = Convert.ToSingle(fileStream.Length / 1024);
                                fileStream.Close();
                                Label2.Text = String.Format("{0:0.00}", byteArray) + "Kb";
                            }

                            break;
                        }
                    }

                }

            }
            catch { }


        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Session["UploadImage"] = null;
            Boolean FileOK = false;
            Boolean FileSaved = false;
            Button2.Visible = false;
            imgCrop.ImageUrl = "";
            string Serverpath = Server.MapPath("~/VisaImages/");
            string sFileName = RefNo + "_" + FileNo + "." + "jpg";
            string sFolderName = Serverpath;
            string PhysicalPath = sFolderName + sFileName;
            Label2.Text = (Upload.PostedFile.ContentLength / 1024).ToString() + "Kb";
            if (Upload.HasFile)
            {
                Session["UploadImage"] = sFileName;
                String FileExtension = Path.GetExtension(Session["UploadImage"].ToString()).ToLower();
                String[] allowedExtensions = { ".png", ".jpeg", ".jpg", ".gif" };
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (FileExtension == allowedExtensions[i])
                    {
                        FileOK = true;
                    }
                }
            }

            if (FileOK)
            {
                try
                {

                    if (!Directory.Exists(sFolderName))
                    {
                        Directory.CreateDirectory(sFolderName);
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(sFolderName);
                        foreach (string fileName in files)
                        {
                            if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                            {
                                File.Delete(fileName);
                            }
                        }
                    }
                    //Upload.SaveAs(PhysicalPath);
                    Upload.PostedFile.SaveAs(path + Session["UploadImage"]);
                    FileSaved = true;
                }
                catch (Exception ex)
                {
                    lblError.Text = "File could not be uploaded." + ex.Message.ToString();
                    lblError.Visible = true;
                    FileSaved = false;
                }
            }
            else
            {
                lblError.Text = "Cannot accept files of this type.";
                lblError.Visible = true;
            }

            if (FileSaved)
            {
                ////pnlUpload.Visible = true;
                ////pnlCrop.Visible = true;
                System.Drawing.Image im = System.Drawing.Image.FromStream(Upload.PostedFile.InputStream);
                double h = im.PhysicalDimension.Height;
                double w = im.PhysicalDimension.Width;
                //imgCrop.ImageUrl = "~/VisaImages/" + Session["UploadImage"].ToString();
                imgCrop.ImageUrl = "~/Thumnail.aspx?fn=" + Session["UploadImage"].ToString();
            }
        }

        protected void btnCrop_Click(object sender, EventArgs e)
        {
            if (Session["UploadImage"] != null)
            {
                string ImageName = Session["UploadImage"].ToString();
                int z = 0;
                int a = 0;
                int x = 0;
                int y = 0;
                byte[] CropImage = null;
                try
                {
                    z = Convert.ToInt32(Width.Value.Split('.')[0]);
                    a = Convert.ToInt32(Height.Value.Split('.')[0]);
                    x = Convert.ToInt32(X.Value.Split('.')[0]);
                    y = Convert.ToInt32(Y.Value.Split('.')[0]);
                    CropImage = Crop(path + ImageName, z, a, x, y);
                    Label1.Text = String.Format("{0:0.00}", (CropImage.Length / 1024).ToString()) + "Kb";
                    using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
                    {

                        ms.Write(CropImage, 0, CropImage.Length);
                        using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                        {
                            string SaveTo = path + "crop" + ImageName;
                            //CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                            Session["CropImage"] = CropImage;
                            pnlCrop.Visible = false;
                            pnlCropped.Visible = true;
                            // Stream fs = FilleCropPassport.PostedFile.InputStream;
                            // BinaryReader br = new BinaryReader(fs);
                            //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            string base64String = Convert.ToBase64String(CropImage, 0, CropImage.Length);
                            imgCropped.ImageUrl = "data:image/png;base64," + base64String.ToString();
                            Button2.Visible = true;
                        }
                    }
                }
                catch
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Select Size to Crop')", true);
                    imgCrop.Focus();

                }




            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Select Image for Upload')", true);
                Upload.Focus();
            }


        }

        static byte[] Crop(string Img, int Width, int Height, int X, int Y)
        {
            try
            {
                using (SD.Image OriginalImage = SD.Image.FromFile(Img))
                {
                    using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                    {
                        bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);
                        using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                        {
                            Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                            Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);
                            MemoryStream ms = new MemoryStream();
                            bmp.Save(ms, OriginalImage.RawFormat);
                            return ms.GetBuffer();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw (Ex);
            }
        }

        protected void Height_ValueChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string ImageName = Session["UploadImage"].ToString();
                int z = Convert.ToInt32(Width.Value.Split('.')[0]);
                int a = Convert.ToInt32(Height.Value.Split('.')[0]);
                int x = Convert.ToInt32(X.Value.Split('.')[0]);
                int y = Convert.ToInt32(Y.Value.Split('.')[0]);
                byte[] CropImage = Crop(path + ImageName, z, a, x, y);
                Label1.Text = (CropImage.Length / 1024).ToString() + "Kb";
            }
            catch
            {
                Label1.Text = "0 Kb";
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string ImageName = Session["UploadImage"].ToString();
            byte[] CropImage = (byte[])Session["CropImage"];
            Label1.Text = (CropImage.Length / 1024).ToString() + "Kb";
            string SaveTo = "";
            // if (CropImage.Length <= 34000)
            // {
            try
            {
                using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
                {

                    ms.Write(CropImage, 0, CropImage.Length);
                    using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                    {
                        SaveTo = path + "\\crop\\" + ImageName;
                        CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                        //pnlCrop.Visible = false;
                        //pnlCropped.Visible = true;



                    }
                    if (CropImage.Length > 34000)
                    {
                        //bitmap = new Bitmap(SaveTo);
                        Bitmap bm = (Bitmap)System.Drawing.Image.FromStream(ms);

                        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                        ImageCodecInfo ici = null;

                        foreach (ImageCodecInfo codec in codecs)
                        {
                            if (codec.MimeType == "image/jpeg")
                                ici = codec;
                        }

                        EncoderParameters ep = new EncoderParameters();
                        if (CropImage.Length > 128000)

                            ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)10);

                        else if (CropImage.Length == 128000)
                            ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)15);
                        else
                            if (CropImage.Length > 100000)
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)18);
                            else if (CropImage.Length > 80000)
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)20);
                            else if (CropImage.Length > 60000)
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)40);
                            else if (CropImage.Length > 40000)
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)80);
                            else if (CropImage.Length > 34000)
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)100);
                            else
                                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)10);

                        if (System.IO.File.Exists(SaveTo))
                            System.IO.File.Delete(SaveTo);
                        bm.Save(SaveTo, ici, ep);
                        FileStream fileStream = new FileStream(SaveTo, FileMode.Open);
                        string byteArray = fileStream.Length.ToString();
                        Label1.Text = (fileStream.Length / 1024) + "Kb";
                        bm.Dispose();
                        if (fileStream.Length > 34000)
                        {
                            CropedAgain = true;
                        }


                    }
                    DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateCropedStatus(RefNo, Convert.ToInt16(FileNo));
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Croped Image Saved..')", true);
                }
            }
            catch
            { }

            //}
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Croped files with less than 32KB!!')", true);
            //}
        }
        Bitmap bitmap;
        private System.Drawing.Image CompressImage()
        {
            Bitmap newBitmap = new Bitmap(450, 300, PixelFormat.Format24bppRgb);
            newBitmap = bitmap;
            newBitmap.SetResolution(80, 80);
            return newBitmap.GetThumbnailImage(450, 300, null, IntPtr.Zero);
        }
        private ImageCodecInfo GetImageCoeInfo(string mimeType)
        {
            ImageCodecInfo[] codes = ImageCodecInfo.GetImageEncoders();
            for (int i = 0; i < codes.Length; i++)
            {
                if (codes[i].MimeType == mimeType)
                {
                    return codes[i];
                }
            }
            return null;
        }

        public void SetImagegQuality(byte[] Image, Int64 ReduceTo, string ImageName)
        {
            EncoderParameters ep = new EncoderParameters();
            string SaveTo = path + "crop\\" + ImageName;
            using (MemoryStream ms = new MemoryStream(Image, 0, Image.Length))
            {
                // ms.Close();
                Bitmap bm = (Bitmap)System.Drawing.Image.FromStream(ms);
                ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                ImageCodecInfo ici = null;

                foreach (ImageCodecInfo codec in codecs)
                {
                    if (codec.MimeType == "image/jpeg")
                        ici = codec;
                }
                ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)ReduceTo);
                //if (System.IO.File.Exists(SaveTo))
                //    System.IO.File.Delete(SaveTo);
                bm.Save(SaveTo, ici, ep);
                FileStream fileStream = new FileStream(SaveTo, FileMode.Open);
                string byteArray = fileStream.Length.ToString();
                Label2.Text = (fileStream.Length / 1024) + "Kb";
                Label3.Text = (100 - ReduceTo).ToString() + "%";
                bm.Dispose();
                fileStream.Close();
                ms.Close();
            }
        }

        protected void drop_SelQuality_TextChanged(object sender, EventArgs e)
        {

        }

        protected void drop_SelQuality_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_ReduceSize_Click(object sender, EventArgs e)
        {
            string sFileName = RefNo + "_" + FileNo + ".jpg";
            byte[] Image = null;
            string sFolderName = Server.MapPath("~/VisaImages/");
            Int64 ReduceTo = Convert.ToInt64(Request.Form["sel_Quality"]);
            string[] files = Directory.GetFiles(sFolderName);
            Image = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath("~/VisaImages/" + "//" + sFileName));
            foreach (string fileName in files)
            {
                if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                {
                    SetImagegQuality(Image, ReduceTo, sFileName);
                    //FileStream fileStream = new FileStream(fileName, FileMode.Open);
                    //float byteArray = Convert.ToSingle(fileStream.Length / 1024);
                    //fileStream.Close();
                    Croped = true;

                    pnlUpload.Visible = true;
                    pnlCrop.Visible = true;
                    Image = ReadImageFile(path + "crop\\" + sFileName);
                    imgCrop.ImageUrl = GetPath(Image);
                    Session["UploadImage"] = sFileName;

                    //Label2.Text = String.Format("{0:0.00}", byteArray) + "Kb";
                }
            }
        }

        public static String GetPath(byte[] CropImage)
        {
            string base64String = Convert.ToBase64String(CropImage, 0, CropImage.Length);
            return "data:image/png;base64," + base64String.ToString();
        }
        public static byte[] ReadImageFile(string imageLocation)
        {
            byte[] imageData = null;
            FileInfo fileInfo = new FileInfo(imageLocation);
            long imageFileLength = fileInfo.Length;
            FileStream fs = new FileStream(imageLocation, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imageData = br.ReadBytes((int)imageFileLength);
            fs.Close();
            br.Close();
            return imageData;
        }



        #region Private Methods

        private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            //New Width
            int destWidth = (int)size.Width;
            //New Height
            int destHeight = (int)size.Height;

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw image with new width and height
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }

        private System.Drawing.Image resizeImage(System.Drawing.Image img)
        {
            Bitmap b = new Bitmap(img);
            System.Drawing.Image i = resizeImage(b, new Size(275, 58));
            return i;
        }
        #endregion
    }
}