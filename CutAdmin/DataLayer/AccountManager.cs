﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class AccountManager
    {
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        public static DBHelper.DBReturnCode AddDeposit(string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string Date)
        {
            int rowsAffected = 0;

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGlobalDefaults.sid;
            string Franchisee = objGlobalDefaults.Franchisee;
            string Agency = objGlobalDefaults.AgencyName;
            string DepositDate = Date;
            decimal dlAmountDeposit;
            decimal.TryParse(AmountDeposit, out dlAmountDeposit);
            string sCreationdate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);

            SqlParameter[] SQLParams = new SqlParameter[24];
            SQLParams[0] = new SqlParameter("@uId", uId);
            SQLParams[1] = new SqlParameter("@DepositAmount", dlAmountDeposit);
            SQLParams[2] = new SqlParameter("@BankName", BankName);
            SQLParams[3] = new SqlParameter("@AccountNumber", AccountNumber);
            SQLParams[4] = new SqlParameter("@ChequeDetail", "");
            SQLParams[5] = new SqlParameter("@CreationDate", sCreationdate);
            SQLParams[6] = new SqlParameter("@Remarks", Remarks);
            SQLParams[7] = new SqlParameter("@DepositUpdateFlag", 0);
            SQLParams[8] = new SqlParameter("@CreditedAmount", 000);
            SQLParams[9] = new SqlParameter("@Updatedate", "");
            SQLParams[10] = new SqlParameter("@BankNarration", "");
            SQLParams[11] = new SqlParameter("@ExecutiveName", "");
            SQLParams[12] = new SqlParameter("@DepositDate", DepositDate);
            SQLParams[13] = new SqlParameter("@Typeofdeposit", TypeOfDeposit);
            SQLParams[14] = new SqlParameter("@Typeofcash", TypeOfCash);
            SQLParams[15] = new SqlParameter("@Typeofcheque", TypeOfCheque);
            SQLParams[16] = new SqlParameter("@EmployeeName", EmployeeName);
            SQLParams[17] = new SqlParameter("@ReceiptNumber", ReceiptNumber);
            SQLParams[18] = new SqlParameter("@TransactionId", ReceiptNumber);
            SQLParams[19] = new SqlParameter("@ChequeorDDNumber", ChequeDDNumber);
            SQLParams[20] = new SqlParameter("@ChequeDrawn", ChequeDrawn);
            SQLParams[21] = new SqlParameter("@ExecutiveRemarks", "");
            SQLParams[22] = new SqlParameter("@Mobile", "N/A");
            SQLParams[23] = new SqlParameter("@Franchisee", Franchisee);


            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BankDepositAddByFranchisee", out rowsAffected, SQLParams);//Proc_tbl_BankDepositAddByRequest
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                SupportMail("", objGlobalDefaults.Agentuniquecode, Agency, BankName, AccountNumber, AmountDeposit, Remarks, TypeOfDeposit, TypeOfCash, TypeOfCheque, EmployeeName, ReceiptNumber, ChequeDDNumber, ChequeDrawn, Date);
            }
            return retCode;
        }


        public static void SupportMail(string RequestId, string AgentCode, string AgencyName, string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string DepositDate)
        {
            // BuildMyString.com generated code. Please enjoy your string responsibly.

            // BuildMyString.com generated code. Please enjoy your string responsibly.
            string SUBJECT = "Payment Update Request from " + AgencyName + " " + AgentCode;
            StringBuilder sb = new StringBuilder();

            sb.Append("<html>");
            sb.Append("<head>");
            sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
            sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
            sb.Append("<style>");
            sb.Append("<!--");
            sb.Append(" /* Font Definitions */");
            sb.Append(" @font-face");
            sb.Append("	{font-family:\"Cambria Math\";");
            sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
            sb.Append("@font-face");
            sb.Append("	{font-family:Calibri;");
            sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
            sb.Append("@font-face");
            sb.Append("	{font-family:\"Segoe UI\";");
            sb.Append("	panose-1:2 11 5 2 4 2 4 2 2 3;}");
            sb.Append(" /* Style Definitions */");
            sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
            sb.Append("	{margin-top:0cm;");
            sb.Append("	margin-right:0cm;");
            sb.Append("	margin-bottom:10.0pt;");
            sb.Append("	margin-left:0cm;");
            sb.Append("	line-height:115%;");
            sb.Append("	font-size:11.0pt;");
            sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
            sb.Append("p.yiv7585368971msonormal, li.yiv7585368971msonormal, div.yiv7585368971msonormal");
            sb.Append("	{mso-style-name:yiv7585368971msonormal;");
            sb.Append("	margin-right:0cm;");
            sb.Append("	margin-left:0cm;");
            sb.Append("	font-size:12.0pt;");
            sb.Append("	font-family:\"Times New Roman\",\"serif\";}");
            sb.Append(".MsoPapDefault");
            sb.Append("	{margin-bottom:10.0pt;");
            sb.Append("	line-height:115%;}");
            sb.Append("@page Section1");
            sb.Append("	{size:595.3pt 841.9pt;");
            sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
            sb.Append("div.Section1");
            sb.Append("	{page:Section1;}");
            sb.Append("    .Currency-AED:before {");
            sb.Append("        content: \"AED \";");
            sb.Append("        /*content: \"ر.س\";*/");
            sb.Append("        /*color: #72bf66;*/");
            sb.Append("        font-style: normal;");
            sb.Append("    }");
            sb.Append("    .Currency-AEDOrg:before {");
            sb.Append("        content: \"AED \";");
            sb.Append("        /*content: \"ر.س\";*/");
            sb.Append("        /*color: #ff9900;*/");
            sb.Append("        font-style: normal;");
            sb.Append("    }");
            sb.Append("    .Currency-SAR:before {");
            sb.Append("        content: \"SAR \";");
            sb.Append("        /*color: #72bf66;*/");
            sb.Append("        font-style: normal;");
            sb.Append("    }");
            sb.Append("    .Currency-SAROrg:before {");
            sb.Append("        content: \"SAR \";");
            sb.Append("        /*color: #ff9900;*/");
            sb.Append("        font-style: normal;");
            sb.Append("    }");
            sb.Append("-->");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append("<body lang=EN-IN>");
            sb.Append("<div class=Section1>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white'><span style='font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black'>Dear Accounts Team,</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121028\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>&nbsp;</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121029\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>You have received payment");
            sb.Append("update request from " + AgencyName + ",</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121030\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>&nbsp;</span></p>");
            //sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            //sb.Append("background:white;-webkit-padding-start: 0px'");
            //sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121036\"><span style='font-size:10.0pt;");
            //sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Request");
            //sb.Append("ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            //sb.Append(""+RequestId+"</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121037\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Supplier");
            sb.Append("Code:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + AgentCode + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121038\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Deposit");
            sb.Append("Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + DepositDate + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121039\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Deposit");
            sb.Append("Type:&nbsp;&nbsp;&nbsp;");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + TypeOfDeposit + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121040\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Transaction");
            sb.Append("Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ");
            sb.Append("" + TypeOfCash + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121031\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Bank");
            sb.Append("Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + BankName + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121041\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Account");
            sb.Append("No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + AccountNumber + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121042\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Receipt No/Trans");
            sb.Append("ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ReceiptNumber + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121054\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Amount:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("<i class=\" Currency-SAR\"></i> " + AmountDeposit + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121053\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Cash");
            sb.Append("To:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + EmployeeName + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121052\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Remark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.Append("" + Remarks + "</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121051\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>&nbsp;</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121050\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Request you to kindly verify");
            sb.Append("and update same online</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121043\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>&nbsp;</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121044\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Thanking you,</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121045\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>&nbsp;</span></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121047\"><b");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121046\" style='-webkit-padding-start: 0px'><span");
            sb.Append("style='font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black'>ClickUrHotel.com</span></b></p>");
            sb.Append("<p class=yiv7585368971msonormal style='margin:0cm;margin-bottom:.0001pt;");
            sb.Append("background:white;-webkit-padding-start: 0px'");
            sb.Append("id=\"yui_3_16_0_ym19_1_1490073508288_121048\"><span style='font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black'>Online Support</span></p>");
            sb.Append("<p class=MsoNormal>&nbsp;</p>");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            bool reponse = true;
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]), "");
               // Email1List.Add("desk@clickurhotel.com", "");
                reponse = MailManager.SendMail(accessKey, Email1List, SUBJECT, sb.ToString(), from, attachmentList);

               // return reponse;

            }
            catch
            {
               // return DBHelper.DBReturnCode.EXCEPTION;
            }



        }

        public static DBHelper.DBReturnCode GetAvailableCredit(out DataTable dt)
        {
            //ds = new DataSet();
            Int64 ParentID = 0;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = 0;
            if (objGlobalDefaults.UserType == "Admin")
            {
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uId = objAgentDetailsOnAdmin.sid;
            }
            if (objGlobalDefaults.UserType == "AdminStaff")
            {
                uId = objGlobalDefaults.sid;
            }
            if (objGlobalDefaults.UserType == "Supplier")
            {
                uId = objGlobalDefaults.sid;
            }
            else if (objGlobalDefaults.UserType == "SupplierStaff")
            {
                uId = objGlobalDefaults.ParentId;
            }
            else if (objGlobalDefaults.UserType == "Agent")
            {
                uId = objGlobalDefaults.sid;
            }
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dt, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetFranchiseeOnAgent(out DataTable dt)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = 0;
            if (objGlobalDefaults.UserType == "Admin")
            {
                AgentDetailsOnAdmin objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uId = objAgentDetailsOnAdmin.sid;
            }
            else
                uId = objGlobalDefaults.sid;
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginByFranchiseeContact", out dt, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode CreditInformation(out DataSet ds)
        {
            //ds = new DataSet();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = GetUserByLogin();

            //Int64 uId = objGlobalDefault.sid;
            //if (objGlobalDefault.UserType == "SupplierStaff")
               // uId = objGlobalDefault.ParentId;

            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_CreditInformationLoad", out ds, SQLParams);
            return retCode;
        }

        public static Int64 GetSupplierByUser()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = 0;
            if (objGlobalDefault.UserType == "Supplier")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Admin")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Agent")
            {
                Uid = objGlobalDefault.ParentId;
            }
            return Uid;

        }

        public static Int64 GetUserByLogin()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = 0;
            if (objGlobalDefault.UserType == "Supplier")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Admin")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Agent")
            {
                Uid = objGlobalDefault.sid;
            }
            return Uid;

        }

        public static string GetUserType()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                return objGlobalDefault.UserType;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}