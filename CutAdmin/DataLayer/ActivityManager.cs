﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class ActivityManager
    {
        #region Search Autocomplete
        public static DataLayer.DataManager.DBReturnCode AutocompleteCity(string CityName, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
           DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            //sSQL.Append("SELECT   distinct [Country],[City] FROM  tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id where City  like '%" + CityName + "%'");
            sSQL.Append("SELECT   distinct [Country],[City] FROM  tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        public static DataLayer.DataManager.DBReturnCode GetTripType(string CityName, string Country, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            // sSQL.Append("Select distinct Act_Name,Act_Images,Tour_Type, Description,Country,City,Category,UniqueId from tbl_aeActivityMaster");
            sSQL.Append("SELECT   distinct Tour_Type FROM  tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id where City like '%" + CityName + "%'");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        public static DataLayer.DataManager.DBReturnCode SearchAct(string CityName, string TourType, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
           DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            // sSQL.Append("Select distinct Act_Name,Act_Images,Tour_Type, Description,Country,City,Category,UniqueId from tbl_aeActivityMaster");
            sSQL.Append("SELECT   distinct [Sr_No],[Act_Name],[Description],[Attractions],[Country],[City] ,Tour_Type ,[Valid_from],[Valid_to],Act_Images FROM  tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id where City like '%" + CityName + "%' and Tour_Type like '%" + TourType + "%'");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        #endregion


        #region Get All Activity
        public static DataTable dt { get; set; }
        public static DataSet ds { get; set; }
        public static DBHelper.DBReturnCode GetGenralDetails(out List<Activity> listActivity)
        {
            DataSet dtResult = new DataSet();
            listActivity = new List<Activity>();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            try
            {


                retCode = DBHelper.GetDataSet("Proc_tbl_aeActivityDetailsLoadAll", out dtResult);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    dt = dtResult.Tables[1];
                    listActivity = new List<Activity>();
                    Activity objActivity = new Activity();
                    for (int i = 0; i < dtResult.Tables[0].Rows.Count; i++)
                    {
                        objActivity = new Activity();
                        objActivity.Aid = dtResult.Tables[0].Rows[i]["Sr_No"].ToString();
                        objActivity.Name = dtResult.Tables[0].Rows[i]["Act_Name"].ToString();
                        objActivity.Sub_Title = dtResult.Tables[0].Rows[i]["Sub_Title"].ToString();
                        objActivity.Country = dtResult.Tables[0].Rows[i]["Country"].ToString();
                        objActivity.City = dtResult.Tables[0].Rows[i]["City"].ToString();
                        objActivity.Location = dtResult.Tables[0].Rows[i]["Location"].ToString();
                        objActivity.Desc = dtResult.Tables[0].Rows[i]["Description"].ToString();
                        objActivity.Attractions = GetAtraction(dtResult.Tables[0].Rows[i]["Attractions"].ToString());
                        objActivity.Mode = GetMode(objActivity.Aid);
                        objActivity.Status = dtResult.Tables[0].Rows[i]["Status"].ToString();
                        objActivity.sImages = dtResult.Tables[0].Rows[i]["Act_Images"].ToString();
                        objActivity.TourType = GetAtraction(dtResult.Tables[0].Rows[i]["Tour_Type"].ToString());
                        listActivity.Add(objActivity);
                    }

                }
            }
            catch
            {

            }

            return retCode;
        }
        public static List<ModeDetails> GetMode(string Aid)
        {
            DataRow[] row = null; DataTable dsResult;
            List<ModeDetails> listMode = new List<ModeDetails>();
            dsResult = dt.Clone();
            row = dt.Select("Act_Id = '" + Aid + "'");
            if (row.Length != 0)
            {
                dsResult = row.CopyToDataTable();
                ModeDetails objMode = new ModeDetails();
                for (int i = 0; i < dsResult.Rows.Count; i++)
                {
                    objMode = new ModeDetails();
                    objMode.Type = dsResult.Rows[i]["Act_Type"].ToString();
                    objMode.Priority = dsResult.Rows[i]["Priority"].ToString();
                    listMode.Add(objMode);
                }
            }

            //DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@UniqueId", Aid);
            //retCode = DBHelper.GetDataTable("Proc_tbl_aeActivityDetailsById", out dsResult, sqlParams);
            //if(retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    ModeDetails objMode = new ModeDetails();
            //    for(int i=0;i<dsResult.Rows.Count;i++)
            //    {
            //        objMode = new ModeDetails();
            //        objMode.Type = dsResult.Rows[i]["Act_Type"].ToString();
            //        objMode.Priority = dsResult.Rows[i]["Priority"].ToString();
            //        listMode.Add(objMode);
            //    }
            //}
            return listMode;
        }
        public static List<string> GetAtraction(string sAtraction)
        {
            List<string> Attraction = new List<string>();
            Attraction = sAtraction.Split(';').ToList();
            return Attraction;
        }
        public static DataLayer.DataManager.DBReturnCode GetActivitylist(out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            sSQL.Append("SELECT   distinct [Sr_No],[Act_Name],[Description],[Attractions],[Country],[City] ,Tour_Type , Act_Images FROM tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);

        }
        public static DataLayer.DataManager.DBReturnCode GetActivity(out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
           DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            // sSQL.Append("Select distinct Act_Name,Act_Images,Tour_Type, Description,Country,City,Category,UniqueId from tbl_aeActivityMaster");
            sSQL.Append("SELECT   distinct [Sr_No],[Act_Name],[Description],[Attractions],[Country],[City] ,Tour_Type,Act_Images FROM         tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        public static DataLayer.DataManager.DBReturnCode ActivitySearch(string ActivityMode, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            // sSQL.Append("Select distinct Act_Name,Act_Images,Tour_Type, Description,Country,City,Category,UniqueId from tbl_aeActivityMaster");
            sSQL.Append("SELECT   distinct [Sr_No],[Act_Name],[Description],[Attractions],[Country],[City] ,Tour_Type FROM         tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id where Act_Type like '%" + ActivityMode + "%'");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        #region Get Min Price
        public static DataLayer.DataManager.DBReturnCode GetActivityPrice(string ActivityId, out DataTable dtPrice)
        {
            DataTable dtResult;
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("Select distinct Adult_Price , Child_Price , Act_Type from tbl_aeActivityTariff where Act_Id='" + ActivityId + "'");
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtPrice);


        }
        #endregion
        #endregion

        #region Get Activity Details
        public static DBHelper.DBReturnCode GetDetails(string Aid, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UniqueId", Aid);
            retCode = DBHelper.GetDataSet("Proc_tbl_aeActivityDetailsByKey", out dsResult, sqlParams);
            return retCode;
        }

        #endregion

        #region Get Activity By Trip
        public static DBHelper.DBReturnCode GetTripDetails(string Tid, string Aid, string Act_Type, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@UniqueId", Aid);
            sqlParams[1] = new SqlParameter("@T_Id", Tid);
            sqlParams[2] = new SqlParameter("@Act_Type", Act_Type);
            retCode = DBHelper.GetDataSet("Proc_tbl_aeActivityDetailsByTrip", out dsResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Insert invoice Vocher
        public static DBHelper.DBReturnCode InsertInvoiceVoucher(string T_Id, string Aid, string FirstName, string LastName, string NoOfAdult, string Child, string Child2, string AdultPrice, string Kids1TotalPrice, string Kids2TotalPrice, string Total, string MobileNo, string Email, string BookDate)
        {
            Random Gen = new Random();
            Int64 RandomNo = Gen.Next(100000, 999999);
            string InvoiceNo = "INC" + '-' + RandomNo.ToString();
            string VoucherNo = "VCH" + '-' + RandomNo.ToString();
            int row = 0;
            DataTable dtResult;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[26];
            sqlParams[0] = new SqlParameter("@Request_Id", T_Id);
            sqlParams[1] = new SqlParameter("@Act_Id", Aid);
            sqlParams[2] = new SqlParameter("@Passenger_Name", FirstName + " " + LastName);
            sqlParams[3] = new SqlParameter("@E_Mail", Email);
            sqlParams[4] = new SqlParameter("@Mobile_No", MobileNo);
            sqlParams[5] = new SqlParameter("@Adults", NoOfAdult);
            sqlParams[6] = new SqlParameter("@Childs1", Child);
            sqlParams[7] = new SqlParameter("@Childs2", Child2);
            sqlParams[8] = new SqlParameter("@Adult_Cost", AdultPrice);
            sqlParams[9] = new SqlParameter("@Child1_Cost", Kids1TotalPrice);
            sqlParams[10] = new SqlParameter("@Child2_Cost", Kids2TotalPrice);
            sqlParams[11] = new SqlParameter("@Extras_Addon", "");
            sqlParams[12] = new SqlParameter("@Taxes", "");
            sqlParams[13] = new SqlParameter("@Discount", "");
            sqlParams[14] = new SqlParameter("@Total", Total);
            sqlParams[15] = new SqlParameter("@User_Id", "");
            sqlParams[16] = new SqlParameter("@IsPaid", "");
            sqlParams[17] = new SqlParameter("@PaymentTransaction_Id", "");
            sqlParams[18] = new SqlParameter("@Pickup_Location", "");
            sqlParams[19] = new SqlParameter("@Drop_Location", "");
            sqlParams[20] = new SqlParameter("@Comment", "");
            sqlParams[21] = new SqlParameter("@Vehicle_Id", 0);
            sqlParams[22] = new SqlParameter("@Driver_Id", 0);
            sqlParams[23] = new SqlParameter("@BookDate", BookDate);
            sqlParams[24] = new SqlParameter("@InvoiceNo", InvoiceNo);
            sqlParams[25] = new SqlParameter("@VoucherNo", VoucherNo);

            retCode = DBHelper.GetDataTable("Proc_Insert_tbl_aeActivityVoucher", out dtResult, sqlParams);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefaultTransfers ObjGlobalDefault = new GlobalDefaultTransfers();
                ObjGlobalDefault.Sid = Convert.ToInt64(dtResult.Rows[0]["Sid"]);
                ObjGlobalDefault.sEmail = Convert.ToString(dtResult.Rows[0]["Email"]);
                ObjGlobalDefault.sPassword = Convert.ToString(dtResult.Rows[0]["Password"]);
                ObjGlobalDefault.sMobile = Convert.ToString(dtResult.Rows[0]["Password"]);

                HttpContext.Current.Session["User"] = ObjGlobalDefault;


            }
            return retCode;
        }
        #endregion

        #region Get My Activity
        public static DBHelper.DBReturnCode GetMyActivity(string uid, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UserId", uid);

            retCode = DBHelper.GetDataSet("Proc_tbl_GetaeActivityVoucher", out dsResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Get Voucher Detail
        public static DBHelper.DBReturnCode GetVoucherDetail(string ReservationID, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ReservationId", ReservationID);


            retCode = DBHelper.GetDataSet("Proc_tbl_GetVoucherDetails", out dsResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Booking
        #region GetBookedDetails
        public static DataLayer.DataManager.DBReturnCode GetActivityByType(string Tid, string ActId, string ActivityMode, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            // sSQL.Append("Select distinct Act_Name,Act_Images,Tour_Type, Description,Country,City,Category,UniqueId from tbl_aeActivityMaster");
            sSQL.Append("SELECT * FROM tbl_aeActivityMaster INNER JOIN  tbl_aeActivityTariff ON tbl_aeActivityMaster.Sr_No = tbl_aeActivityTariff.Act_Id where Act_Type ='" + ActivityMode + "' and  T_Id ='" + Tid + "' and  Sr_No='" + ActId + "'");
            return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        #endregion



        #region old
        public static string Book(string ActivitynName, string Type, string TropTotal, string PassengerName, string Email, string MobileNo, string StartDate, string Till, List<string> GenderAdult, List<string> FirstName, List<string> LastName, List<string> GenderChild, List<string> noChilds)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            // BuildMyString.com generated code. Please enjoy your string responsibly.

            string sb = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">" +
           "    " +
           "</head>" +
           "<body lang=\"EN-US\" style=\"tab-interval:.5in\">" +
           "<div class=\"Section1\">" +
                //"<div style=\"min-height:50px;width:auto;\" id=\"yui_3_16_0_ym19_1_1476274275074_7199\"><br><img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" class=\"yiv4451700329CToWUd\" id=\"yui_3_16_0_ym19_1_1476274275074_7202\"></div>"+
           "  <div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\"><div style=\"min-height:50px;width:auto\"><br><img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px\" class=\"CToWUd\"></div><div style=\"margin-left:10%\">  <p> <span>Your Booking Details.</span><br></p>  <div><table style=\"" +
           "    width: 50%;" +
           "\"><tbody><tr style=\"" +
           "    width: 50%;" +
           "\"><td style=\"" +
           "    width: -20%;" +
           "\"><b>Passenger Name</b></td><td>: " + PassengerName + "</td></tr><tr><td><b>E-Mail</b></td><td>:  " + Email + "</td></tr>" +
             "<tr><td><b>Mobile No.</b></td><td>:  " + MobileNo + "</td></tr>" +
           "<tr><td><b>Activity Name</b></td><td>: " + ActivitynName + "</td></tr>" +
           "<tr><td><b>Activity Type</b></td><td>: " + Type + "</td></tr>" +
            "<tr><td><b>Trip Total</b></td><td>:AED " + TropTotal + "</td></tr>" +
            "<tr><td><b>Booking Date</b></td><td>: " + DateTime.Now.ToString("dd-mm-yyyy") + "</td></tr>" +
           "<tr id=\"yui_3_16_0_ym19_1_1476255084166_61026\"><td style=\"padding:.75pt .75pt .75pt .75pt;\" id=\"yui_3_16_0_ym19_1_1476255084166_61025\"><p class=\"yiv3924011790MsoNormal\" id=\"yui_3_16_0_ym19_1_1476255084166_61027\"><b id=\"yui_3_16_0_ym19_1_1476255084166_61028\">Start From</b></p></td><td style=\"padding:.75pt .75pt .75pt .75pt;\" id=\"yui_3_16_0_ym19_1_1476255084166_61049\"><p class=\"yiv3924011790MsoNormal\" id=\"yui_3_16_0_ym19_1_1476255084166_61048\">: " + StartDate + "</p></td></tr>" +
           "<tr id=\"yui_3_16_0_ym19_1_1476255084166_61022\"><td style=\"padding:.75pt .75pt .75pt .75pt;\" id=\"yui_3_16_0_ym19_1_1476255084166_61021\"><p class=\"yiv3924011790MsoNormal\" id=\"yui_3_16_0_ym19_1_1476255084166_61020\"><b id=\"yui_3_16_0_ym19_1_1476255084166_61023\">Till</b></p></td><td style=\"padding:.75pt .75pt .75pt .75pt;\"><p class=\"yiv3924011790MsoNormal\">: " + Till + "</p></td></tr><tr><td><b> No Adults</b></td><td>: " + FirstName.Count + "</td></tr><tr></tr><tr><td><b>No Child</b></td><td>: " + noChilds.Count + "</td></tr></tbody></table>" +
           "<br><b>Visitor Details</b><br> " +
           "<table class=\"MsoNormalTable\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"721\" style=\"width:540.7pt;background:white;border-collapse:collapse;border:none;" +
           " mso-border-alt:solid #DDDDDD .75pt;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">" +
           " <tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes\">" +
           "  <td width=\"108\" valign=\"top\" style=\"width:80.65pt;border:solid #E8E8E8 1.0pt;" +
           "  border-right:solid #DDDDDD 1.0pt;mso-border-alt:solid #E8E8E8 .75pt;" +
           "  mso-border-right-alt:solid #DDDDDD .75pt;padding:5.75pt 5.75pt 5.75pt 5.75pt\">" +
           "  <p class=\"MsoNormal\" style=\"margin-bottom:.2in;line-height:normal\"><span style=\"font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:" +
           "  &quot;Times New Roman&quot;;color:#999999\"><br>" +
           "  </span><span style=\"font-size:8.5pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;" +
           "  mso-fareast-font-family:&quot;Times New Roman&quot;;color:#999999\">(" + FirstName.Count + " x Adults + " + noChilds.Count + " x" +
           "  Child)</span><span style=\"font-size:10.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;" +
           "  mso-fareast-font-family:&quot;Times New Roman&quot;;color:#999999\"><o:p></o:p></span></p>" +
           "  </td>" +
           "  <td valign=\"top\" style=\"border:solid #E8E8E8 1.0pt;border-left:none;mso-border-left-alt:" +
           "  solid #E8E8E8 .75pt;mso-border-alt:solid #E8E8E8 .75pt;padding:5.75pt 5.75pt 5.75pt 5.75pt\">" +
           "  <table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"596\" style=\"width:447.1pt;border-collapse:collapse;mso-yfti-tbllook:1184\">" +
           "   <tbody>" +
           AdultDetails(GenderAdult, FirstName, LastName, GenderChild, noChilds) +
           "  </tbody></table>" +
           "  </td>" +
           " </tr>" +
           "</tbody></table> " +
           "<br/><br/> " +
           "<p><span>For any further clarification & query kindly feel free to contact us</span><br></p>" +
           "<span>" +
           "<b>Thank You,</b><br>" +
           "</span>" +
           "<span>" +
           "Administrator" +
           "</span>" +
           "</div></div></div></div>" +
           "</body></html>";
            return sb;
        }



        public static string AdultDetails(List<string> GenderAdult, List<string> FirstName, List<string> LastName, List<string> GenderChild, List<string> Childs)
        {
            string Details = "";
            string Adults = "";
            for (int i = 0; i < FirstName.Count; i++)
            {
                int j = (i + 1);
                Adults += "<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">" +
                        "    <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:" +
                        "    solid white .75pt;padding:11.5pt 0in 0in .1in\">" +
                        "    <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;" +
                        "    line-height:normal\"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;" +
                        "    mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\">Adult " + j + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:" +
                        "    &quot;Times New Roman&quot;\"><o:p></o:p></span></p>" +
                        "    </td>" +
                        "    <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:" +
                        "    solid white .75pt;padding:5.75pt 5.75pt 5.75pt 5.75pt\">" +
                        "    <p class=\"MsoNormal\" style=\"" +
                        "    margin-bottom:0in;" +
                        "    margin-bottom:.0001pt;" +
                        "    line-height:normal;" +
                        "    padding-top: 3%;" +
                        "    \"><span style=\"" +
                        "    padding-top: 10%;" +
                        "    font-size:12.0pt;" +
                        "    font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;" +
                        "    mso-fareast-font-family:&quot;Times New Roman&quot;;" +
                        "    \"><label>" + GenderAdult[i] + "</label><o:p></o:p></span></p>" +
                        "    </td>" +
                        "    <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:" +
                        "    solid white .75pt;padding:11.5pt .75pt .75pt .75pt\">" +
                        "    <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;padding-top: 3%;" +
                        "    line-height:normal\"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;" +
                        "    mso-fareast-font-family:&quot;Times New Roman&quot;\"><label> " + FirstName[i] + " </label><o:p></o:p></span></p>" +
                        "    </td>" +
                        "    <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:" +
                        "    solid white .75pt;padding:11.5pt .75pt .75pt .75pt\">" +
                        "    <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;padding-top: 3%;" +
                        "    line-height:normal\"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;" +
                        "    mso-fareast-font-family:&quot;Times New Roman&quot;\"><label> " + LastName[i] + " </label><o:p></o:p></span></p>" +
                        "    </td>" +
                        "    <td style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:" +
                        "    solid white .75pt;padding:11.5pt .75pt .75pt .75pt\">" +
                        "    " +
                        "    </td>" +
                        "   </tr>";
            }
            StringBuilder Child = new StringBuilder();

            for (int i = 0; i < Childs.Count; i++)
            {
                int j = (i + 1);
                Child.Append("<tr style=\"mso-yfti-irow:2\">");
                Child.Append(" <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:");
                Child.Append(" solid white .75pt;padding:11.5pt 0in 0in .1in\">");
                Child.Append(" <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;");
                Child.Append(" line-height:normal\"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;");
                Child.Append(" mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\">Child" + j + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:");
                Child.Append(" &quot;Times New Roman&quot;\"><o:p></o:p></span></p>");
                Child.Append(" </td>");
                Child.Append(" <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:");
                Child.Append(" solid white .75pt;padding:5.75pt 5.75pt 5.75pt 5.75pt\">");
                Child.Append(" <p class=\"MsoNormal\" style=\"");
                Child.Append(" margin-bottom:0in;");
                Child.Append(" margin-bottom:.0001pt;");
                Child.Append(" line-height:normal;");
                Child.Append(" padding-top: 8%;");
                Child.Append(" \"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;");
                Child.Append(" mso-fareast-font-family:&quot;Times New Roman&quot;\"><label>" + GenderChild[i] + "</label><o:p></o:p></span></p>");
                Child.Append(" </td>");
                Child.Append(" <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:");
                Child.Append(" solid white .75pt;padding:11.5pt .75pt .75pt .75pt\">");
                Child.Append(" <p class=\"MsoNormal\" style=\"");
                Child.Append(" margin-bottom:0in;");
                Child.Append(" margin-bottom:.0001pt;");
                Child.Append(" line-height:normal;");
                Child.Append(" padding-top: 1%;");
                Child.Append(" \"><span style=\"");
                Child.Append(" font-size:12.0pt;");
                Child.Append(" font-family:\" times=\"\" new=\"\"><label>" + Childs[i] + "</label><o:p></o:p></span></p>");
                Child.Append(" </td>");
                Child.Append(" <td valign=\"top\" style=\"border:none;border-top:solid white 1.0pt;mso-border-top-alt:");
                Child.Append(" solid white .75pt;padding:11.5pt .75pt .75pt .75pt\">");
                Child.Append(" <p class=\"MsoNormal\" style=\"");
                Child.Append(" margin-bottom:0in;");
                Child.Append(" margin-bottom:.0001pt;");
                Child.Append(" line-height:normal;");
                Child.Append(" padding-top: 1%;");
                Child.Append(" \"><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;");
                Child.Append(" mso-fareast-font-family:&quot;Times New Roman&quot;\"><label>2</label><o:p></o:p></span></p>");
                Child.Append(" </td>");
                Child.Append(" <td style=\"padding:.75pt .75pt .75pt .75pt\"></td>");
                Child.Append("</tr>");
            }

            return Details = Adults + Child.ToString();
        }
        #endregion



        //#region New
        //public static string BookActivity(DataTable dtResult, string TropTotal, string PassengerName, string Email, string MobileNo, string StartDate, string Till, List<string> GenderAdult, List<string> FirstName, List<string> LastName, List<string> GenderChild, List<string> noChilds)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    float AdultPrice = Convert.ToSingle(HttpContext.Current.Session["AdultPrice"].ToString()) * FirstName.Count;
        //    float ChildPrice = Convert.ToSingle(HttpContext.Current.Session["ChildPrice"].ToString()) * noChilds.Count;

        //    sb.Append("<html>");
        //    sb.Append("<head>");
        //    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
        //    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
        //    sb.Append("<style>");
        //    sb.Append("<!--");
        //    sb.Append("/* Font Definitions */");
        //    sb.Append("@font-face {");
        //    sb.Append("font-family: \"Cambria Math\";");
        //    sb.Append("panose-1: 2 4 5 3 5 4 6 3 2 4;");
        //    sb.Append("}");
        //    sb.Append("@font-face {");
        //    sb.Append("font-family: Calibri;");
        //    sb.Append("panose-1: 2 15 5 2 2 2 4 3 2 4;");
        //    sb.Append("}");
        //    sb.Append("@font-face {");
        //    sb.Append("font-family: \"Segoe UI\";");
        //    sb.Append("panose-1: 2 11 5 2 4 2 4 2 2 3;");
        //    sb.Append("}");
        //    sb.Append("/* Style Definitions */");
        //    sb.Append("p.MsoNormal, li.MsoNormal, div.MsoNormal {");
        //    sb.Append("margin-top: 0in;");
        //    sb.Append("margin-right: 0in;");
        //    sb.Append("margin-bottom: 10.0pt;");
        //    sb.Append("margin-left: 0in;");
        //    sb.Append("line-height: 115%;");
        //    sb.Append("font-size: 11.0pt;");
        //    sb.Append("font-family: \"Calibri\",\"sans-serif\";");
        //    sb.Append("}");
        //    sb.Append("a:link, span.MsoHyperlink {");
        //    sb.Append("color: blue;");
        //    sb.Append("text-decoration: underline;");
        //    sb.Append("}");
        //    sb.Append("a:visited, span.MsoHyperlinkFollowed {");
        //    sb.Append("color: purple;");
        //    sb.Append("text-decoration: underline;");
        //    sb.Append("}");
        //    sb.Append("p.yiv5021412743msonormal, li.yiv5021412743msonormal, div.yiv5021412743msonormal {");
        //    sb.Append("mso-style-name: yiv5021412743msonormal;");
        //    sb.Append("margin-right: 0in;");
        //    sb.Append("margin-left: 0in;");
        //    sb.Append("font-size: 12.0pt;");
        //    sb.Append("font-family: \"Times New Roman\",\"serif\";");
        //    sb.Append("}");
        //    sb.Append("p.yiv5021412743m-5107501835697684160msolistparagraph, li.yiv5021412743m-5107501835697684160msolistparagraph, div.yiv5021412743m-5107501835697684160msolistparagraph {");
        //    sb.Append("mso-style-name: yiv5021412743m_-5107501835697684160msolistparagraph;");
        //    sb.Append("margin-right: 0in;");
        //    sb.Append("margin-left: 0in;");
        //    sb.Append("font-size: 12.0pt;");
        //    sb.Append("font-family: \"Times New Roman\",\"serif\";");
        //    sb.Append("}");
        //    sb.Append("span.apple-converted-space {");
        //    sb.Append("mso-style-name: apple-converted-space;");
        //    sb.Append("}");
        //    sb.Append(".MsoPapDefault {");
        //    sb.Append("margin-bottom: 10.0pt;");
        //    sb.Append("line-height: 115%;");
        //    sb.Append("}");
        //    sb.Append("@page Section1 {");
        //    sb.Append("size: 8.5in 11.0in;");
        //    sb.Append("margin: 1.0in 1.0in 1.0in 1.0in;");
        //    sb.Append("}");
        //    sb.Append("div.Section1 {");
        //    sb.Append("page: Section1;");
        //    sb.Append("}");
        //    sb.Append("-->");
        //    sb.Append("</style>");
        //    sb.Append("</head>");
        //    sb.Append("<body lang=EN-US link=blue vlink=purple>");
        //    sb.Append("<div class=Section1>");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#4472C4\">Your Booking Request</span></b></p>");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:12.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:#4472C4\">&nbsp;</span></b></p>");
        //    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style=\"background:white;border-collapse:collapse\">");
        //    sb.Append("<tr>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Request ID</span></b></p></td>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Request Date</span></b></p></td>");
        //    sb.Append("<td width=374 colspan=\"6\" valign=top style=\"width:280.5pt;border:solid #A6A6A6 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Location</span></b></p></td>");
        //    sb.Append("</tr>");
        //    sb.Append("<tr>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + GenerateRandomString(6) + "</span></p></td>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + DateTime.Now.ToString()+ "</span></p></td>");
        //    sb.Append("<td width=374 colspan=\"6\" valign=top style=\"width:280.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["City"].ToString() + " ," + dtResult.Rows[0]["Country"].ToString() + "</span></p></td>");
        //    sb.Append("</tr>");
        //    sb.Append("<tr>");
        //    sb.Append("<td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p></td>");
        //    sb.Append("</tr>");
        //    sb.Append(" <tr>");
        //    sb.Append("     <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Passenger Details</span></b></p></td>");
        //    sb.Append(" </tr>");
        //    sb.Append(" <tr>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Passenger Name</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">E-Mail</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt; padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Mobile No.</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Adults</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Child</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("</tr>");
        //    sb.Append("<tr>");
        //    sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+GenderAdult[0]+" " + FirstName[0]  + " " + LastName[0] +"</span></p></td>");
        //    sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+Email+"</span></p>");
        //    sb.Append("     </td>  ");
        //    sb.Append("     <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+MobileNo+"</span></p>");
        //    sb.Append("     </td>");
        //    sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+FirstName.Count+"</span></p>");
        //    sb.Append("     </td>");
        //    sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+noChilds.Count+"</span></p>");
        //    sb.Append("     </td>");
        //    sb.Append(" </tr>");
        //    sb.Append(" <tr>");
        //    sb.Append("     <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p>");
        //    sb.Append("     </td>");
        //    sb.Append(" </tr>");
        //    sb.Append(" <tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Activity Details</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("            <tr>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Activity Name</span></b></p></td><td width=499 colspan=6 valign=top style=\"width:374.0pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height: normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">"+dtResult.Rows[0]["Act_Name"].ToString()+"</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("            <tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("            <tr>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Activity Type</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Actvity Date</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Start Time</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">End Time</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Duration</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("            <tr>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Act_Type"].ToString().Replace("SIC", "Sharing (SIC)").Replace("TKT","Ticket Only") + "</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + HttpContext.Current.Session["TravelDate"].ToString() + "</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Start_Time"].ToString() + " Hrs</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["End_Time"].ToString() + "Hrs</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Duration"].ToString() + " Hrs</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("           ");
        //    sb.Append("<tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;");
        //    sb.Append("  border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;");
        //    sb.Append("  text-align:center;line-height:normal\">");
        //    sb.Append("                        <span style=\"font-size:10.0pt;");
        //    sb.Append("  font-family:\"Segoe UI\",\"sans-serif\";color:#7F7F7F\">&nbsp;</span>");
        //    sb.Append("                    </p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("<tr>");
        //    sb.Append("                <td width=283 colspan=3 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Inclusion</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=47 rowspan=2 valign=top style=\"width:35.45pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                       <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=293 colspan=3 valign=top style=\"width:219.65pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Exclusion</span>");
        //    sb.Append("                        </b>");
        //    sb.Append("                    </p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    // From Here  Inclusion &  Exclusion Will Show //
        //    sb.Append("<tr>");
        //    sb.Append(Inclusion(dtResult.Rows[0]["Inclusions"].ToString()));
        //    sb.Append(Exclusion(dtResult.Rows[0]["Exclusions"].ToString()));
        //    sb.Append("</tr>");
        //    // End Here  Inclusion &  Exclusion Will Show //
        //    // Billing Details //
        //    sb.Append(" <tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Billing</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("<tr>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Adult Cost</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Child</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                   <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Extras/Add on</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Taxes (if any)</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Total</span></b></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");

        //    sb.Append("<tr>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + AdultPrice + "</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + ChildPrice + "</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED 0</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED 0</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED "+HttpContext.Current.Session["TripTotal"].ToString()+"</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("<tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">&nbsp;</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("            </tr>");
        //    sb.Append("<tr>");
        //    sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">&nbsp;</span></p>");
        //    sb.Append("                </td>");
        //    sb.Append("</tr>");


        //    sb.Append("<tr>");
        //    sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">In Words</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=499 colspan=6 valign=top style=\"width:374.0pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span>" + ToWords(Convert.ToDecimal(HttpContext.Current.Session["TripTotal"])) + "</p>");
        //    sb.Append("</td>");
        //    sb.Append("</tr>");

        //    // End Billing Details //

        //    // Price And Activity Note
        //    sb.Append("<tr>");
        //    sb.Append("<td width=283 colspan=3 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Important Note</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=47 rowspan=2 valign=top style=\"width:35.45pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b>   <span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span></b></p>");
        //    sb.Append("</td>");
        //    sb.Append("<td width=293 colspan=3 valign=top style=\"width:219.65pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Pricing Note</span></b> </p>");
        //    sb.Append("</td>");
        //    sb.Append("</tr>");
        //    // End Price And Activity Note
        //    sb.Append("<tr>");
        //    sb.Append(Important(dtResult.Rows[0]["Tour_Note"].ToString()));
        //    sb.Append(PriceNote(dtResult.Rows[0]["Tariff_Note"].ToString()));
        //    sb.Append("</tr>");

        //    sb.Append("        </table>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">");
        //    sb.Append("                Hope the above is correct and clear, we will be processing request");
        //    sb.Append("                according to above given details if in case there is any issues request you to");
        //    sb.Append("                kindly contact us immediately");
        //    sb.Append("            </span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">Thanking you again,</span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">Reservation Team</span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <b>");
        //    sb.Append("                <i>");
        //    sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span>");
        //    sb.Append("                </i>");
        //    sb.Append("            </b>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <b>");
        //    sb.Append("                <i>");
        //    sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">ClickUrTrip</span>");
        //    sb.Append("                </i>");
        //    sb.Append("            </b><span style=\"font-size:10.0pt;");
        //    sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span><b>");
        //    sb.Append("                <i>");
        //    sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">Group</span>");
        //    sb.Append("                </i>");
        //    sb.Append("            </b>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <a href=\"mailto:Inbound.dxb@clickurtrip.com\"");
        //    sb.Append("               target=\"_blank\">");
        //    sb.Append("                <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:#196AD4\">Inbound.dxb@clickurtrip.com</span>");
        //    sb.Append("            </a>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span>");
        //    sb.Append("        </p>");
        //    sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("<b>");
        //    sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif\"");
        //    sb.Append("color:black\">Office:</span>");
        //    sb.Append("</b><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif\"");
        //    sb.Append("");
        //    sb.Append("<span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">");
        //    sb.Append("+971-4-2977792 (09:00 AM to 20:00 PM – Sat - Thu");
        //    //sb.Append("Thu");
        //    sb.Append("</span>");
        //    sb.Append("</b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">)");
        //    sb.Append("</span>");
        //    sb.Append("</p>");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("<b>");
        //    sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
        //    sb.Append("Skype:</span>");
        //    sb.Append("</b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
        //    sb.Append("&nbsp; clickurtripdxb</span>");
        //    sb.Append("</p>");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("<b>");
        //    sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
        //    sb.Append("Whatsapp:</span>");
        //    sb.Append("</b><span style=\"font-size:10.0pt;font-family:");
        //    sb.Append("Segoe UI,sans-serif;color:black\">&nbsp;</span><span style=\"font-size:10.0pt;");
        //    sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black\">+971-501635798 / 567780431</span>");
        //    sb.Append("</p>");
        //    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
        //    sb.Append("normal;background:white\">");
        //    sb.Append("<span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
        //    sb.Append("color:black\">&nbsp;</span>");
        //    sb.Append("</p>");
        //    sb.Append("<p class=MsoNormal>&nbsp;</p>");
        //    sb.Append("</div>");
        //    sb.Append("</body>");
        //    sb.Append("</html>");
        //    return sb.ToString();
        //}
        //#endregion




        #region New
        public static string BookActivity(DataTable dtResult, string TropTotal, string FirstName, string LastName, string NoOfAdult, string noChilds, string noChilds2, string Kids1TotalPrice, string Kids2TotalPrice, string MobileNo, string Email)
        {
            StringBuilder sb = new StringBuilder();
            float AdultPrice = Convert.ToSingle(HttpContext.Current.Session["AdultPrice"].ToString());


            sb.Append("<html>");
            sb.Append("<head>");
            sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
            sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
            sb.Append("<style>");
            sb.Append("<!--");
            sb.Append("/* Font Definitions */");
            sb.Append("@font-face {");
            sb.Append("font-family: \"Cambria Math\";");
            sb.Append("panose-1: 2 4 5 3 5 4 6 3 2 4;");
            sb.Append("}");
            sb.Append("@font-face {");
            sb.Append("font-family: Calibri;");
            sb.Append("panose-1: 2 15 5 2 2 2 4 3 2 4;");
            sb.Append("}");
            sb.Append("@font-face {");
            sb.Append("font-family: \"Segoe UI\";");
            sb.Append("panose-1: 2 11 5 2 4 2 4 2 2 3;");
            sb.Append("}");
            sb.Append("/* Style Definitions */");
            sb.Append("p.MsoNormal, li.MsoNormal, div.MsoNormal {");
            sb.Append("margin-top: 0in;");
            sb.Append("margin-right: 0in;");
            sb.Append("margin-bottom: 10.0pt;");
            sb.Append("margin-left: 0in;");
            sb.Append("line-height: 115%;");
            sb.Append("font-size: 11.0pt;");
            sb.Append("font-family: \"Calibri\",\"sans-serif\";");
            sb.Append("}");
            sb.Append("a:link, span.MsoHyperlink {");
            sb.Append("color: blue;");
            sb.Append("text-decoration: underline;");
            sb.Append("}");
            sb.Append("a:visited, span.MsoHyperlinkFollowed {");
            sb.Append("color: purple;");
            sb.Append("text-decoration: underline;");
            sb.Append("}");
            sb.Append("p.yiv5021412743msonormal, li.yiv5021412743msonormal, div.yiv5021412743msonormal {");
            sb.Append("mso-style-name: yiv5021412743msonormal;");
            sb.Append("margin-right: 0in;");
            sb.Append("margin-left: 0in;");
            sb.Append("font-size: 12.0pt;");
            sb.Append("font-family: \"Times New Roman\",\"serif\";");
            sb.Append("}");
            sb.Append("p.yiv5021412743m-5107501835697684160msolistparagraph, li.yiv5021412743m-5107501835697684160msolistparagraph, div.yiv5021412743m-5107501835697684160msolistparagraph {");
            sb.Append("mso-style-name: yiv5021412743m_-5107501835697684160msolistparagraph;");
            sb.Append("margin-right: 0in;");
            sb.Append("margin-left: 0in;");
            sb.Append("font-size: 12.0pt;");
            sb.Append("font-family: \"Times New Roman\",\"serif\";");
            sb.Append("}");
            sb.Append("span.apple-converted-space {");
            sb.Append("mso-style-name: apple-converted-space;");
            sb.Append("}");
            sb.Append(".MsoPapDefault {");
            sb.Append("margin-bottom: 10.0pt;");
            sb.Append("line-height: 115%;");
            sb.Append("}");
            sb.Append("@page Section1 {");
            sb.Append("size: 8.5in 11.0in;");
            sb.Append("margin: 1.0in 1.0in 1.0in 1.0in;");
            sb.Append("}");
            sb.Append("div.Section1 {");
            sb.Append("page: Section1;");
            sb.Append("}");
            sb.Append("-->");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append("<body lang=EN-US link=blue vlink=purple>");
            sb.Append("<div class=Section1>");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#4472C4\">Your Booking Request</span></b></p>");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:12.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:#4472C4\">&nbsp;</span></b></p>");
            sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style=\"background:white;border-collapse:collapse;width:80%\">");
            sb.Append("<tr>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Request ID</span></b></p></td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Request Date</span></b></p></td>");
            sb.Append("<td width=374 colspan=\"6\" valign=top style=\"width:280.5pt;border:solid #A6A6A6 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Location</span></b></p></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + GenerateRandomString(6) + "</span></p></td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + DateTime.Now.ToString() + "</span></p></td>");
            sb.Append("<td width=374 colspan=\"6\" valign=top style=\"width:280.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["City"].ToString() + " ," + dtResult.Rows[0]["Country"].ToString() + "</span></p></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p></td>");
            sb.Append("</tr>");
            sb.Append(" <tr>");
            sb.Append("     <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Passenger Details</span></b></p></td>");
            sb.Append(" </tr>");
            sb.Append(" <tr>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Passenger Name</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">E-Mail</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt; padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Mobile No.</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Adults</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Kids(7 to 11)</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Kids(2 to 7)</span></b></p>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;  text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\"> " + FirstName + " " + LastName + "</span></p></td>");
            sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + Email + "</span></p>");
            sb.Append("     </td>  ");
            sb.Append("     <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + MobileNo + "</span></p>");
            sb.Append("     </td>");
            sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + NoOfAdult + "</span></p>");
            sb.Append("     </td>");
            sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + noChilds + "</span></p>");
            sb.Append("     </td>");
            sb.Append("     <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + noChilds2 + "</span></p>");
            sb.Append("     </td>");
            sb.Append(" </tr>");
            sb.Append(" <tr>");
            sb.Append("     <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("         <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p>");
            sb.Append("     </td>");
            sb.Append(" </tr>");
            sb.Append(" <tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Activity Details</span></b></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("            <tr>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Activity Name</span></b></p></td><td width=499 colspan=7 valign=top style=\"width:374.0pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height: normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Act_Name"].ToString() + "</span></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("            <tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("            <tr>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Activity Type</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Actvity Date</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 colspan=4 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Start Time</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">End Time</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Duration</span></b></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("            <tr>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Act_Type"].ToString().Replace("SIC", "Sharing (SIC)").Replace("TKT", "Ticket Only") + "</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + HttpContext.Current.Session["TravelDate"].ToString() + "</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 colspan=4 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Start_Time"].ToString() + " Hrs</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"> <span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["End_Time"].ToString() + "Hrs</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + dtResult.Rows[0]["Duration"].ToString() + " Hrs</span></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("           ");
            sb.Append("<tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;");
            sb.Append("  border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;");
            sb.Append("  text-align:center;line-height:normal\">");
            sb.Append("                        <span style=\"font-size:10.0pt;");
            sb.Append("  font-family:\"Segoe UI\",\"sans-serif\";color:#7F7F7F\">&nbsp;</span>");
            sb.Append("                    </p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("<tr>");
            sb.Append("                <td width=283 colspan=3 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Inclusion</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=47 rowspan=2 valign=top style=\"width:35.45pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                       <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=293 colspan=4 valign=top style=\"width:219.65pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Exclusion</span>");
            sb.Append("                        </b>");
            sb.Append("                    </p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            // From Here  Inclusion &  Exclusion Will Show //
            sb.Append("<tr>");
            sb.Append(Inclusion(dtResult.Rows[0]["Inclusions"].ToString()));
            sb.Append(Exclusion(dtResult.Rows[0]["Exclusions"].ToString()));
            sb.Append("</tr>");
            // End Here  Inclusion &  Exclusion Will Show //
            // Billing Details //
            sb.Append(" <tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Billing</span></b></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("<tr>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Adult Cost</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Kids(7 to 11) Cost</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Kids(2 to 7) Cost</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                   <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Extras/Add on</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Taxes (if any)</span></b></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">Total</span></b></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");

            sb.Append("<tr>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + AdultPrice + "</span></p>");
            sb.Append("                </td>");
            //sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            //sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + ChildPrice + "</span></p>");
            //sb.Append("                </td>");
            sb.Append("                <td width=125 colspan=1 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + Kids1TotalPrice + "</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED" + Kids2TotalPrice + "</span></p>");
            sb.Append("                </td>");

            sb.Append("                <td width=125 colspan=3 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED 0</span></p>");
            sb.Append("                </td>");
            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED 0</span></p>");
            sb.Append("                </td>");

            sb.Append("                <td width=125 valign=top style=\"width:93.5pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">AED " + HttpContext.Current.Session["TripTotal"].ToString() + "</span></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("<tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">&nbsp;</span></p>");
            sb.Append("                </td>");
            sb.Append("            </tr>");
            sb.Append("<tr>");
            sb.Append("                <td width=623 colspan=8 valign=top style=\"width:467.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("                    <p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">&nbsp;</span></p>");
            sb.Append("                </td>");
            sb.Append("</tr>");


            sb.Append("<tr>");
            sb.Append("<td width=125 valign=top style=\"width:93.5pt;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">In Words</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=499 colspan=7 valign=top style=\"width:374.0pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span>" + ToWords(Convert.ToDecimal(HttpContext.Current.Session["TripTotal"])) + "</p>");
            sb.Append("</td>");
            sb.Append("</tr>");

            // End Billing Details //

            // Price And Activity Note
            sb.Append("<tr>");
            sb.Append("<td width=283 colspan=4 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Important Note</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=47 rowspan=2 valign=top style=\"width:35.45pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal align=center style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b>   <span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">&nbsp;</span></b></p>");
            sb.Append("</td>");
            sb.Append("<td width=293 colspan=4 valign=top style=\"width:219.65pt;border-top:none;border-left:none;border-bottom:solid #A6A6A6 1.0pt;border-right:solid #A6A6A6 1.0pt;padding:0in 5.4pt 0in 5.4pt\">");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#ED7D31\">Pricing Note</span></b> </p>");
            sb.Append("</td>");
            sb.Append("</tr>");
            // End Price And Activity Note
            sb.Append("<tr>");
            sb.Append(Important(dtResult.Rows[0]["Tour_Note"].ToString()));
            sb.Append(PriceNote(dtResult.Rows[0]["Tariff_Note"].ToString()));
            sb.Append("</tr>");

            sb.Append("        </table>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">");
            sb.Append("                Hope the above is correct and clear, we will be processing request");
            sb.Append("                according to above given details if in case there is any issues request you to");
            sb.Append("                kindly contact us immediately");
            sb.Append("            </span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">Thanking you again,</span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">Reservation Team</span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <b>");
            sb.Append("                <i>");
            sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span>");
            sb.Append("                </i>");
            sb.Append("            </b>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <b>");
            sb.Append("                <i>");
            sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">ClickUrTrip</span>");
            sb.Append("                </i>");
            sb.Append("            </b><span style=\"font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black\">&nbsp;</span><b>");
            sb.Append("                <i>");
            sb.Append("                    <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">Group</span>");
            sb.Append("                </i>");
            sb.Append("            </b>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <a href=\"mailto:Inbound.dxb@clickurtrip.com\"");
            sb.Append("               target=\"_blank\">");
            sb.Append("                <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:#196AD4\">Inbound.dxb@clickurtrip.com</span>");
            sb.Append("            </a>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("            <span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span>");
            sb.Append("        </p>");
            sb.Append("        <p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("<b>");
            sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif\"");
            sb.Append("color:black\">Office:</span>");
            sb.Append("</b><span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif\"");
            sb.Append("");
            sb.Append("<span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:black\">");
            sb.Append("+971-4-2977792 (09:00 AM to 20:00 PM – Sat - Thu");
            //sb.Append("Thu");
            sb.Append("</span>");
            sb.Append("</b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">)");
            sb.Append("</span>");
            sb.Append("</p>");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("<b>");
            sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
            sb.Append("Skype:</span>");
            sb.Append("</b><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
            sb.Append("&nbsp; clickurtripdxb</span>");
            sb.Append("</p>");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("<b>");
            sb.Append("<span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:black\">");
            sb.Append("Whatsapp:</span>");
            sb.Append("</b><span style=\"font-size:10.0pt;font-family:");
            sb.Append("Segoe UI,sans-serif;color:black\">&nbsp;</span><span style=\"font-size:10.0pt;");
            sb.Append("font-family:\"Segoe UI\",\"sans-serif\";color:black\">+971-501635798 / 567780431</span>");
            sb.Append("</p>");
            sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:");
            sb.Append("normal;background:white\">");
            sb.Append("<span style=\"font-size:10.0pt;font-family:\"Segoe UI\",\"sans-serif\";");
            sb.Append("color:black\">&nbsp;</span>");
            sb.Append("</p>");
            sb.Append("<p class=MsoNormal>&nbsp;</p>");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            return sb.ToString();
        }
        #endregion





        #region Inclusion
        public static string Inclusion(string InclusionDetails)
        {
            string[] Inc = InclusionDetails.Split(';');
            StringBuilder sb = new StringBuilder();

            sb.Append("<td width=283 colspan=4 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            for (int i = 0; i < Inc.Length - 1; i++)
            {
                sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">{ " + Inc[i] + " }</span></p>");
            }
            sb.Append("</td>");
            return sb.ToString();
        }
        #endregion



        #region Exclusion
        public static string Exclusion(string ExclusionDetails)
        {
            string[] Exc = ExclusionDetails.Split(';');
            StringBuilder sb = new StringBuilder();

            sb.Append("<td width=283 colspan=4 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
            for (int i = 0; i < Exc.Length - 1; i++)
            {
                sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">{ " + Exc[i] + " }</span></p>");
            }
            sb.Append("</td>");
            return sb.ToString();
        }
        #endregion



        #region ImportantNote
        public static string Important(string NoteDetails)
        {
            string[] Note = NoteDetails.Split(',');
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(" <td width=283 colspan=4 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
                for (int i = 0; i < Note.Length; i++)
                {
                    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + Note[i] + "</span></p>");
                }
                sb.Append("                </td>");
                return sb.ToString();
            }
            catch
            {
                return sb.ToString();
            }

        }
        #endregion



        #region PriceNote
        public static string PriceNote(string NoteDetails)
        {
            string[] Note = NoteDetails.Split(',');
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(" <td width=283 colspan=4 valign=top style=\"width:2.95in;border:solid #A6A6A6 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\">");
                for (int i = 0; i < Note.Length; i++)
                {
                    sb.Append("<p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:10.0pt;font-family:Symbol;color:#7F7F7F\">&nbsp;</span><span style=\"font-size:10.0pt;font-family:Segoe UI,sans-serif;color:#7F7F7F\">" + Note[i] + "</span></p>");
                }
                sb.Append("                </td>");
                return sb.ToString();
            }
            catch
            {
                return sb.ToString();
            }

        }
        #endregion



        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }




        public static string NumberWithComma(Double n)
        {
            string Number = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble((n)));
            return Number;
        }



        public static string ToWords(decimal number)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number));

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            return string.Format("{0} AED", ToWords(intPortion));
        }



        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }


        private static string[] ones = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", };

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };
        #endregion


        #region Update Images
        public static DataLayer.DataManager.DBReturnCode UpdateImages(string ImagePath, string Aid)
        {
            string[] Path = ImagePath.Split('^');
            int i = 0; ImagePath = "";
            foreach (string Image in Path)
            {


                if (Image != "")
                {
                    if ((Path.Length - 1) != i)
                        ImagePath += Image + "^";


                    else
                        ImagePath += Image;

                }
                i++;
            }
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            sSQL.Append("Update tbl_aeActivityMaster set Act_Images='" + ImagePath + "' where Sr_No='" + Aid + "'");
            return retCopde = DataManager.ExecuteNonQuery(sSQL.ToString());
        }
        #endregion



        public static DBHelper.DBReturnCode AddMode(string fName, string myFilePath, string id)
        {
            //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
            int rows;
            string Status = "True";
            //GlobalDefault objGlobalDefault;
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@TourType", fName);
            sqlParams[1] = new SqlParameter("@myFilePath", myFilePath);
            sqlParams[2] = new SqlParameter("@id", Convert.ToInt64(id));
            sqlParams[3] = new SqlParameter("@Status", Status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertTourType", out rows, sqlParams);
            // HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMODE(out DataTable dtResult)
        {
            dtResult = null;
            // int RowEffected;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_Getmode", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetFormList(out DataTable dtResult)
        {
            dtResult = null;
            // int RowEffected;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_Gettourtype", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode AddActivity(string Country, string City, string activityname, string subtitle, string Description, string Attraction, string longitude, string lattitude, string TourNote, string TourType, string Kid1Range, string Kid2Range, out DataTable dt)
        {
            //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
            int rows;
            string uniqueid = GenerateRandomString(3);
            //GlobalDefault objGlobalDefault;
            SqlParameter[] sqlParams = new SqlParameter[12];
            sqlParams[0] = new SqlParameter("@Country", Country);
            sqlParams[1] = new SqlParameter("@City", City);
            sqlParams[2] = new SqlParameter("@Act_Name", activityname);
            sqlParams[3] = new SqlParameter("@Sub_Title", subtitle);
            sqlParams[4] = new SqlParameter("@Description", Description);
            sqlParams[5] = new SqlParameter("@Attractions", Attraction);
            sqlParams[6] = new SqlParameter("@Lon_Leng", longitude + "," + lattitude);
            sqlParams[7] = new SqlParameter("@Tour_Note", TourNote);
            sqlParams[8] = new SqlParameter("@Tour_Type", TourType);
            sqlParams[9] = new SqlParameter("@uniqueid", uniqueid);
            sqlParams[10] = new SqlParameter("@Kid1Range", Kid1Range);
            sqlParams[11] = new SqlParameter("@Kid2Range", Kid2Range);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_InsertAddActivity", out dt, sqlParams);
            //GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            //Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            //Global.Sr_No = Convert.ToInt64(dt.Rows[0]["Sr_No"]);
            // HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
            return retCode;
        }

        public static DBHelper.DBReturnCode Getpriority(out DataTable dtResult)
        {
            dtResult = null;
            // int RowEffected;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetPriority", out dtResult);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetSupplier(out DataTable dtResult)
        {
            dtResult = null;
            // int RowEffected;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivitySupplier", out dtResult);
            return retCode;
        }


        //public static CUTAE.DataLayer.DataManager.DBReturnCode GetActivitylist(out DataTable dtResult)
        //{
        //    StringBuilder sSQL = new StringBuilder();
        //    CUTAE.DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
        //    sSQL.Append("SELECT  ");
        //    return retCopde = DataManager.ExecuteQuery(sSQL.ToString(), out dtResult);
        //}

        public static DBHelper.DBReturnCode AddPriority(string fName)
        {
            //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
            int rows;
            string status = "True";
            //GlobalDefault objGlobalDefault;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@PriorityType", fName);
            sqlParams[1] = new SqlParameter("@status", status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertPriority", out rows, sqlParams);
            // HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
            return retCode;
        }

        public static DBHelper.DBReturnCode Update_Priority(Int64 Pid, string fName)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@PriorityType", fName);
            sqlParams[1] = new SqlParameter("@Sid", Pid);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updatepriority", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetPriorityActiveDeactive(Int64 Sid, string Status)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Sid", Sid);
            sqlParams[1] = new SqlParameter("@Status", Status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_GetPriorityActiveDeactive", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetModeActiveDeactive(Int64 Sid, string Status)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Sid", Sid);
            sqlParams[1] = new SqlParameter("@Status", Status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_GetModeActiveDeactive", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode Update_Mode(Int64 Pid, string fName)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@TourType", fName);
            sqlParams[1] = new SqlParameter("@Sid", Pid);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateMode", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeletePriority(Int64 sid)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Sid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeletePriority", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteActivity(string id)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Sr_No", id);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_Deleteactivity", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteActivityTariff(string id, string TrrifId)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Act_Id", id);
            sqlParams[1] = new SqlParameter("@Act_Type", TrrifId);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteActivityTariff", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteMode(Int64 sid)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Sid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteMode", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateActivityDetails(string id, string Country, string City, string activityname, string subtitle, string Description, string Attraction, string longitude, string lattitude, string TourNote, string TourType)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[10];
            sqlParams[0] = new SqlParameter("@Country", Country);
            sqlParams[1] = new SqlParameter("@City", City);
            sqlParams[2] = new SqlParameter("@Act_Name", activityname);
            sqlParams[3] = new SqlParameter("@Sub_Title", subtitle);
            sqlParams[4] = new SqlParameter("@Description", Description);
            sqlParams[5] = new SqlParameter("@Attractions", Attraction);
            sqlParams[6] = new SqlParameter("@Lon_Leng", longitude + "," + lattitude);
            sqlParams[7] = new SqlParameter("@Tour_Note", TourNote);
            sqlParams[8] = new SqlParameter("@Tour_Type", TourType);
            sqlParams[9] = new SqlParameter("@Sr_No", id);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateactivity", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateTariffDetails(string id, string validfrom, string validto, string priority, string currency, string liveselling, Double minpax, Double maxpax, Double adultheight, Double adultprice, Double childprice, Double childprice2, Double infantprice, Double childage, string activitytype, string tariffnote, string starttime, string endtime, string duration, string pickupreport, string pickupfrom, string droptime, string dropto, string days, string inclusion, string exclusion, Double adultagestarts, string TrrifId)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[28];
            sqlParams[0] = new SqlParameter("@Valid_from", validfrom);
            sqlParams[1] = new SqlParameter("@Valid_to", validto);
            sqlParams[2] = new SqlParameter("@Priority", priority);
            sqlParams[3] = new SqlParameter("@Currency", currency);
            sqlParams[4] = new SqlParameter("@LiveSelling", liveselling);
            // var min = Convert.ToDouble(minpax);
            //var max = Convert.ToDouble(maxpax);
            sqlParams[5] = new SqlParameter("@Min_pax", minpax);
            sqlParams[6] = new SqlParameter("@Max_pax", maxpax);
            var height = Convert.ToDouble(adultheight);
            var price = Convert.ToDouble(adultprice);
            var pricechild = Convert.ToDouble(childprice);

            var pricechild2 = Convert.ToDouble(childprice2);
            sqlParams[7] = new SqlParameter("@Adult_Hight", height);
            sqlParams[8] = new SqlParameter("@Adult_Price", price);
            sqlParams[9] = new SqlParameter("@Child_Price ", pricechild);

            sqlParams[10] = new SqlParameter("@Child_Price2 ", pricechild2);
            // var infant = Convert.ToDouble(infantprice);
            sqlParams[11] = new SqlParameter("@Infant_Price", infantprice);
            // var childstatrtage = Convert.ToDouble(childage);
            sqlParams[12] = new SqlParameter("@Child_Age_Start", childage);
            sqlParams[13] = new SqlParameter("@Act_Type", activitytype);
            sqlParams[14] = new SqlParameter("@Tariff_Note", tariffnote);
            sqlParams[15] = new SqlParameter("@Start_Time", starttime);
            sqlParams[16] = new SqlParameter("@End_Time", endtime);
            sqlParams[17] = new SqlParameter("@Duration", duration);
            sqlParams[18] = new SqlParameter("@PickupReprting", pickupreport);
            sqlParams[19] = new SqlParameter("@Pickup_from", pickupfrom);
            sqlParams[20] = new SqlParameter("@Drop_time", droptime);
            sqlParams[21] = new SqlParameter("@Drop_to", dropto);
            sqlParams[22] = new SqlParameter("@Days", days);
            sqlParams[23] = new SqlParameter("@Inclusions", inclusion);
            sqlParams[24] = new SqlParameter("@Exclusions", exclusion);
            // var adultage = Convert.ToDouble(adultagestart);
            sqlParams[25] = new SqlParameter("@Adult_Age_Start", adultagestarts);
            //  var actid = Convert.ToDouble(id);
            sqlParams[26] = new SqlParameter("@Act_Id", id);
            sqlParams[27] = new SqlParameter("@TrrifId", TrrifId);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateactivitytariff", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetActivity(string id, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[1];
            Sqlpara1[0] = new SqlParameter("@Sr_No", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivityDetails", out dtResult, Sqlpara1);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetAllActivity(out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            //SqlParameter[] Sqlpara1 = new SqlParameter[1];
            //Sqlpara1[0] = new SqlParameter("@Sr_No", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetAllActivities", out dtResult);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetChildPolicyDetails(string id, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[1];
            Sqlpara1[0] = new SqlParameter("@Sr_No", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetChildPolicy", out dtResult, Sqlpara1);
            return retCode;
        }


        public static DBHelper.DBReturnCode SetOperatingDate(string id, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[1];
            Sqlpara1[0] = new SqlParameter("@Sr_No", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetOperatingDates", out dtResult, Sqlpara1);
            return retCode;
        }


        public static DBHelper.DBReturnCode SetSlots(string id, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[1];
            Sqlpara1[0] = new SqlParameter("@Sr_No", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetSlots", out dtResult, Sqlpara1);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetActivityTariff1(string nid, string type, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[2];
            Sqlpara1[0] = new SqlParameter("@Act_Id", nid);
            Sqlpara1[1] = new SqlParameter("@Act_Type", type);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_getactivitytariff", out dtResult, Sqlpara1);
            return retCode;
        }

        public static DBHelper.DBReturnCode AddActivityTariff(string id, string validfrom, string validto, string priority, string currency, string liveselling, Double minpax, Double maxpax, Double adultheight, Double adultprice, Double childprice, Double childprice2, Double infantprice, Double childage, string activitytype, string tariffnote, string starttime, string endtime, string duration, string pickupreport, string pickupfrom, string droptime, string dropto, string days, string inclusion, string exclusion, Double adultagestart, string Location)
        {
            //string sEncryptedPassword = CUT.Common.Cryptography.EncryptText(sPassword);
            int rows;
            string uniqueid = GenerateRandomString(3);
            //GlobalDefault objGlobalDefault;
            SqlParameter[] sqlParams = new SqlParameter[29];
            sqlParams[0] = new SqlParameter("@Valid_from", validfrom);
            sqlParams[1] = new SqlParameter("@Valid_to", validto);
            sqlParams[2] = new SqlParameter("@Priority", priority);
            sqlParams[3] = new SqlParameter("@Currency", currency);
            sqlParams[4] = new SqlParameter("@LiveSelling", liveselling);
            // var min = Convert.ToDouble(minpax);
            //var max = Convert.ToDouble(maxpax);
            sqlParams[5] = new SqlParameter("@Min_pax", minpax);
            sqlParams[6] = new SqlParameter("@Max_pax", maxpax);
            var height = Convert.ToDouble(adultheight);
            var price = Convert.ToDouble(adultprice);
            var pricechild = Convert.ToDouble(childprice);
            var pricechild2 = Convert.ToDouble(childprice2);
            sqlParams[7] = new SqlParameter("@Adult_Hight", height);
            sqlParams[8] = new SqlParameter("@Adult_Price", price);
            sqlParams[9] = new SqlParameter("@Child_Price ", pricechild);
            sqlParams[10] = new SqlParameter("@Child_Price2 ", pricechild2);
            // var infant = Convert.ToDouble(infantprice);
            sqlParams[11] = new SqlParameter("@Infant_Price", infantprice);
            // var childstatrtage = Convert.ToDouble(childage);
            sqlParams[12] = new SqlParameter("@Child_Age_Start", childage);
            sqlParams[13] = new SqlParameter("@Act_Type", activitytype);
            sqlParams[14] = new SqlParameter("@Tariff_Note", tariffnote);
            sqlParams[15] = new SqlParameter("@Start_Time", starttime);
            sqlParams[16] = new SqlParameter("@End_Time", endtime);
            sqlParams[17] = new SqlParameter("@Duration", duration);
            sqlParams[18] = new SqlParameter("@PickupReprting", pickupreport);
            sqlParams[19] = new SqlParameter("@Pickup_from", pickupfrom);
            sqlParams[20] = new SqlParameter("@Drop_time", droptime);
            sqlParams[21] = new SqlParameter("@Drop_to", dropto);
            sqlParams[22] = new SqlParameter("@Days", days);
            sqlParams[23] = new SqlParameter("@Inclusions", inclusion);
            sqlParams[24] = new SqlParameter("@Exclusions", exclusion);
            // var adultage = Convert.ToDouble(adultagestart);
            sqlParams[25] = new SqlParameter("@Adult_Age_Start", adultagestart);
            //var actid = Convert.ToDouble(id);
            sqlParams[26] = new SqlParameter("@Act_Id", id);
            sqlParams[27] = new SqlParameter("@T_Id", uniqueid);
            sqlParams[28] = new SqlParameter("@Location", Location);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_AddActivityTariff", out rows, sqlParams);
            // HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
            return retCode;
        }


        public static DBHelper.DBReturnCode ActTeriiff(string id, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[1];
            Sqlpara1[0] = new SqlParameter("@Act_Id", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivityTeriff", out dtResult, Sqlpara1);
            return retCode;
        }

        #region Blog

        public static DBHelper.DBReturnCode AddBlogDetail(string Title, string ShortDescription, string Description, string Project, string myFilePath, string id)
        {            
            int rows;            
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@Title", Title);
            sqlParams[1] = new SqlParameter("@ShortDescription", ShortDescription);
            sqlParams[2] = new SqlParameter("@Description", Description);
            sqlParams[3] = new SqlParameter("@Project", Project);
            sqlParams[4] = new SqlParameter("@myFilePath", myFilePath);
            sqlParams[5] = new SqlParameter("@id", Convert.ToInt64(id));
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertBlogDetails", out rows, sqlParams);
            
            return retCode;
        }

        #endregion



        public static DBHelper.DBReturnCode SearchTeriff(out DataTable dtResult)
        {

            SqlParameter[] SQLParams = new SqlParameter[0];
            
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_ActivityTeriffLoadAll", out dtResult, SQLParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetLocation(string Location, out DataTable dtResult)
        {

            dtResult = new DataTable();
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[0] = new SqlParameter("@Location", Location);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ActivityLoadAllLocation", out dtResult, sqlParams);
            //DataView view = new DataView(dtResult);
            //dtResult = view.ToTable(true, "City","Sid");
            return retCode;

        }


        public static DBHelper.DBReturnCode GetDetails(Int64 LocationID, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@LocationID", LocationID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetLocationDetails", out dtResult, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetActivityRatelistt(string id, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@id", id);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivityRateList", out dtResult, sqlParams);

            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateLocation(string Location, string Country, string City, Int64 id)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@Location", Location);
            sqlParams[2] = new SqlParameter("@Country", Country);
            sqlParams[3] = new SqlParameter("@City", City);
            //sqlParams[4] = new SqlParameter("@Longitude", Longitude);
            //sqlParams[5] = new SqlParameter("@Latitude", Latitude);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateLocation", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode UpdateRates(Int64 id, string adult, string child, string schild, string Currency)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[5];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@adult", adult);
            sqlParams[2] = new SqlParameter("@child", child);
            sqlParams[3] = new SqlParameter("@schild", schild);
            sqlParams[4] = new SqlParameter("@Currency", Currency);
            //sqlParams[5] = new SqlParameter("@Latitude", Latitude);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateActRate", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteLocation(Int64 LocationId)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@LocationId", LocationId);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteLocation", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteRate(Int64 id)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@id", id);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteActRate", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode TeriffIDForTKT(string id, string TypeActivity, out DataTable dtResult)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            dtResult = null;
            // int RowEffected;
            SqlParameter[] Sqlpara1 = new SqlParameter[2];
            Sqlpara1[0] = new SqlParameter("@Act_Id", id);
            Sqlpara1[1] = new SqlParameter("@TypeActivity", TypeActivity);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivityTeriffforTKT", out dtResult, Sqlpara1);
            return retCode;
        }

        //public static DBHelper.DBReturnCode TeriffIDForSIC(string id, string TypeActivity, out DataTable dtResult)
        //{
        //    //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    dtResult = null;
        //    // int RowEffected;
        //    SqlParameter[] Sqlpara1 = new SqlParameter[2];
        //    Sqlpara1[0] = new SqlParameter("@Act_Id", id);
        //    Sqlpara1[1] = new SqlParameter("@TypeActivity", TypeActivity);

        //    DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetActivityTeriffforSIC", out dtResult, Sqlpara1);
        //    return retCode;
        //}
    }
}