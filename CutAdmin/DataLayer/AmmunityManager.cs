﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class AmmunityManager
    {

        public static DBHelper.DBReturnCode AddRoomAmunities(string RoomAmunityName, string sPhoto)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@RoomAmunityName", RoomAmunityName);
            sqlParams[1] = new SqlParameter("@sPhoto", sPhoto);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertRoomAmunities", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetRoomAmunities(out DataTable dtResult)
        {
            dtResult = null;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetRoomAmunities", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteRoomAmunities(string RoomAmunityID)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomAmunityID", RoomAmunityID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteRoomAmunity", out rows, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetDetails(string RoomAmunityID, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomAmunityID", RoomAmunityID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetAmmunityDetails", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateRoomAmunity(string RoomAmunityName, string AmunityIcon, Int64 id)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@RoomAmunityName", RoomAmunityName);
            sqlParams[2] = new SqlParameter("@AmunityIcon", AmunityIcon);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateAmmunity", out rows, sqlParams);
            return retCode;
        }

    }
}