﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.HotelAdmin;
using System.Data;
using CutAdmin.BL;
using System.Data.SqlClient;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class AuthorizationManager
    {
        public static void GetAgentFormList()
        {
            //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
            try
            {
                using (var db = new helperDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] != null)
                    {
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        Forms objForms = new Forms();
                        if (objGlobalDefault.UserType == "AdminStaff")
                        {
                            Int64 UID = objGlobalDefault.sid;

                            var AssigndAPI = (from objForm in db.tblForms
                                              from objAgent in db.tblStaffRoleManagers  
                                              where objForm.nId == objAgent.nFormId && objAgent.nUid == UID 
                                              select new
                                              {
                                                  objForm.sFormName,
                                                  objAgent.nId
                                              }).ToList();

                        
                            objForms.strAuthorizedFormCollection = new string[AssigndAPI.Count];

                            int i = 0;
                            foreach (var API in AssigndAPI)
                            {
                                objForms.strAuthorizedFormCollection[i] = API.sFormName;
                                i++;
                            }
                            HttpContext.Current.Session["AthorizedFormList"] = objForms;
                        }
                        else if (objGlobalDefault.UserType == "Admin")
                        {
                            var sForms = (from objForm in db.tblForms
                                          select new
                                          {
                                              objForm.sFormName,
                                              objForm.nId
                                          }).ToList(); ;
                            objForms.strAuthorizedFormCollection = new string[sForms.Count];

                            int i = 0;
                            foreach (var Form in sForms)
                            {
                                objForms.strAuthorizedFormCollection[i] = Form.sFormName;
                                i++;
                            }
                            HttpContext.Current.Session["AthorizedFormList"] = objForms;
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public static List<Int64> GetAuthorizedSupplier()
        {
            List<Int64> ListSupplier = new List<Int64>();
            try
            {
                Int64 SupplierID = AccountManager.GetSupplierByUser();
                if( HttpContext.Current.Session["ListSupplier"] == null)
                {
                    //ListSupplier.Add(SupplierID);
                    using (var db = new helperDataContext())
                    {
                       // var arrForms = DB.Comm_SupplierMappings.Where(d => d.UserID == SupplierID).ToList();
                        var arrForms = db.tbl_AdminLogins.Where(d => d.UserType == "Supplier").ToList().Select(r => r.sid).ToList();
                        foreach (var obj in arrForms)
                        {
                            ListSupplier.Add(Convert.ToInt64(obj));
                        }
                    }
                    HttpContext.Current.Session["ListSupplier"] = ListSupplier;
                }
                else
                {
                    ListSupplier = (List<Int64>)HttpContext.Current.Session["ListSupplier"];
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return ListSupplier;
        }

    }
}