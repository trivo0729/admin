﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class Automation
    {
        #region PostVisaApplication
        #region CheckVisaApplication
        //public static DBHelper.DBReturnCode PostVisa(string VCode, string Username, string Password)
        //{
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    DataTable dtResult = new DataTable();
        //    retCode = VisaDetailsManager.GetVisaByCode(out dtResult, VCode);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        string[,] arr1 = Automation.tblRecords(VCode);
        //        bool PostingStatus = IEAutomation(arr1, VCode, Username, Password);
        //        if (PostingStatus == true)
        //        {
        //            UpdateVisaStatus("Processing-Applied", VCode);
        //            retCode = DBHelper.DBReturnCode.SUCCESS;
        //        }
        //        else
        //        {
        //            retCode = DBHelper.DBReturnCode.EXCEPTION;
        //        }

        //    }
        //    else
        //    {
        //        retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    }

        //    return retCode;
        //}
        #endregion

        #region GetData
        public static string[,] tblRecords(string VisaCode)
        {
            DataTable NotNullTable = GetVisaByCode(VisaCode);
            int rws = NotNullTable.Rows.Count;
            int clmsn = NotNullTable.Columns.Count;
            string[,] Arr1 = new string[rws, clmsn];
            for (int i = 0; i < rws; i++)
            {
                for (int j = 0; j < clmsn; j++)
                {
                    Arr1[i, j] = NotNullTable.Rows[i].ItemArray[j].ToString();
                }
            }
            return Arr1;
        }
        public static DataTable GetVisaByCode(string VisaCode)
        {
            DataTable tbl_Visa;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return tbl_Visa;
        }
        #endregion

        #region IEAutomation
        //public static bool IEAutomation(string[,] arr1, string VCode, string Username, string Password)
        //{
        //    try
        //    {
        //        //string[,] arr1 = objBL.tblRecords();
        //        object mVal = System.Reflection.Missing.Value;
        //        string url = "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=";
        //        InternetExplorer oIE = new InternetExplorer();
        //        //ShowWindow((IntPtr)oIE.HWND, SW_MAXIMISE);
        //        oIE.Visible = true;
        //        oIE.Navigate(url, ref mVal, ref mVal, ref mVal, ref mVal);
        //        //oIE.Offline = true;
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //        if (oIE.LocationURL == url)
        //        {
        //            HTMLDocument myDoc = new HTMLDocument();
        //            myDoc = (HTMLDocument)oIE.Document;
        //            HTMLInputElement userID = (HTMLInputElement)myDoc.all.item("ssousername", 0);
        //            //userID.value = "ofwpl17";
        //            userID.value = Username;
        //            HTMLInputElement pwd = (HTMLInputElement)myDoc.all.item("password", 0);
        //            //pwd.value = "Smart2016";
        //            pwd.value = Password;
        //            HTMLInputElement terms = (HTMLInputElement)myDoc.all.item("terms", 0);
        //            terms.defaultChecked = true;
        //            //IHTMLWindow2 win = (IHTMLWindow2)myDoc.parentWindow;
        //            //win.execScript("fnValidate()", "javascript");
        //            while (oIE.LocationURL != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {

        //            }

        //            //while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //            if (oIE.LocationURL == "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {
        //                oIE.Navigate("https://www.ednrd.ae/portal/page/portal/onlineapp/visa:visaapp", ref mVal, ref mVal, ref mVal, ref mVal);
        //                while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }
        //                while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }

        //                if (oIE.LocationURL == "https://www.ednrd.ae/portal/page/portal/onlineapp/visa:visaapp")
        //                {
        //                    oIE.Navigate("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.visa_redirect.show", ref mVal, ref mVal, ref mVal, ref mVal);
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //                    //do while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }
        //                    myDoc = (HTMLDocument)oIE.Document;
        //                    IHTMLSelectElement slt1 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.URGENT.01", 0);
        //                    while (slt1.value == null)
        //                    {
        //                        slt1 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.URGENT.01", 0);

        //                    }
        //                    IHTMLSelectElement slt2 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.GR_MEN.01", 0);
        //                    IHTMLSelectElement slt3 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.VISA_TP.01", 0);
        //                    HTMLSelectElement slt33 = (HTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.VISA_TP.01", 0);
        //                    IHTMLInputElement txt1 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.F_NM_E.01", 0);
        //                    IHTMLInputElement txt2 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.M_NM_E.01", 0);
        //                    IHTMLInputElement txt3 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.L_NM_E.01", 0);
        //                    IHTMLInputElement txt4 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.FATHER_NM_E.01", 0);
        //                    IHTMLInputElement txt5 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.MOTHER_NM_E.01", 0);
        //                    IHTMLSelectElement slt4 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.LANG1.01", 0);
        //                    IHTMLSelectElement slt5 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.SEX.01", 0);
        //                    IHTMLSelectElement slt6 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.MAT_STA.01", 0);
        //                    IHTMLSelectElement slt7 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PRV_NAT.01", 0);
        //                    IHTMLInputElement txt6 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.BIRTH_DT.01", 0);
        //                    IHTMLInputElement txt7 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.BIRTH_PLC.01", 0);
        //                    IHTMLSelectElement slt9 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.BIRTH_CNTRY.01", 0);
        //                    IHTMLSelectElement slt10 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.RLG.01", 0);
        //                    IHTMLInputElement txt8 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.NMA.01", 0);
        //                    IHTMLInputElement txt9 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PROF.01", 0);
        //                    IHTMLInputElement txt10 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_NO.01", 0);
        //                    IHTMLSelectElement slt11 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_TP.01", 0);
        //                    IHTMLSelectElement slt12 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_ISSGOV.01", 0);
        //                    IHTMLSelectElement slt13 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_CNTRY.01", 0);
        //                    IHTMLInputElement txt11 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_PLC.01", 0);
        //                    IHTMLInputElement txt12 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_IS_DT.01", 0);
        //                    IHTMLInputElement txt13 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PASS_EXP_DT.01", 0);
        //                    IHTMLInputElement txt14 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.AOADD1.01", 0);
        //                    IHTMLInputElement txt15 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.AOADD2.01", 0);
        //                    IHTMLInputElement txt16 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.AOCITY.01", 0);
        //                    IHTMLSelectElement slt14 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.AOCNTRY.01", 0);
        //                    IHTMLInputElement txt17 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.AOTEL.01", 0);

        //                    //slt1.value = arr1[0, 1];
        //                    slt1.value = "1";
        //                    slt33.FireEvent("onchange", 0);
        //                    //slt2.value = arr1[0, 2];// Cotact Person//
        //                    slt3.value = arr1[0, 4];
        //                    //slt3.value = "3";//Sevice Type //
        //                    txt1.value = arr1[0, 5] + " " + arr1[0, 6];//First Name //
        //                    txt2.value = arr1[0, 7]; // Middle //
        //                    txt3.value = arr1[0, 8]; // Last//
        //                    txt4.value = arr1[0, 8]; // Father Name // 
        //                    txt5.value = arr1[0, 9]; // Mother Name // 
        //                    slt4.value = arr1[0, 11]; // Language //
        //                    slt5.value = arr1[0, 12]; // Gender //
        //                    slt6.value = arr1[0, 13]; // Marital Status//
        //                    slt7.value = arr1[0, 14]; // Previous Nationality //
        //                    IHTMLSelectElement slt8 = (IHTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PRS_NAT.01", 0);//Previous Nationality //
        //                    HTMLSelectElement slt88 = (HTMLSelectElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PRS_NAT.01", 0);
        //                    slt8.value = arr1[0, 14];
        //                    slt88.FireEvent("onchange", 0);
        //                    txt6.value = arr1[0, 15].Substring(0, 10);
        //                    txt7.value = arr1[0, 17];
        //                    slt9.value = arr1[0, 14];
        //                    slt10.value = "1";
        //                    txt8.value = arr1[0, 20];
        //                    txt9.value = "4112042";
        //                    txt10.value = arr1[0, 21];
        //                    //slt11.value = arr1[0, 22];
        //                    slt11.value = "1";
        //                    slt12.value = arr1[0, 14];
        //                    slt13.value = arr1[0, 14];
        //                    txt11.value = arr1[0, 23];
        //                    txt12.value = arr1[0, 24].Substring(0, 10);
        //                    txt13.value = arr1[0, 25].Substring(0, 10);
        //                    txt14.value = arr1[0, 26];
        //                    txt15.value = arr1[0, 27];
        //                    txt16.value = arr1[0, 28];
        //                    slt14.value = arr1[0, 29];
        //                    txt17.value = arr1[0, 31];

        //                    ///string jscript = "var acc_no;var file_attach;document.forms[0].elements('FRM_VISA_SINGLE_FORM_B.DEFAULT.PICKFLAG.01').value = 3;if (fnAll() == true) {acc_no = false;if (acc_no) {document.forms[0].elements('FRM_VISA_SINGLE_FORM_B.DEFAULT.RES_FNO.01').value = 1;}else {document.forms[0].elements('FRM_VISA_SINGLE_FORM_B.DEFAULT.RES_FNO.01').value =- 1;file_attach = true;if (file_attach) {document.forms[0].elements('FRM_VISA_SINGLE_FORM_B.DEFAULT.APP_TP.01').value = 1;}else {document.forms[0].elements('FRM_VISA_SINGLE_FORM_B.DEFAULT.APP_TP.01').value = 3;}}do_event(this.form, this.name, 1, 'CUSTOM', '');}else {return false;};do_event(this.form, this.name, 1, 'ON_CLICK', '');";
        //                    //win = (IHTMLWindow2)myDoc.parentWindow;
        //                    //win.execScript(jscript, "javascript");

        //                    IHTMLInputElement txt18 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.PICKFLAG.01", 0);
        //                    txt18.value = "3";
        //                    System.Threading.Thread.Sleep(30000);
        //                    //IHTMLDocument iDoc = (IHTMLDocument)oIE.Document;
        //                    //Type scriptType = iDoc.Script.GetType();
        //                    //bool returnValue = (bool)scriptType.InvokeMember("fnAll", System.Reflection.BindingFlags.InvokeMethod, null, iDoc.Script, null);
        //                    //Console.WriteLine("fnAll return is: " + returnValue.ToString());
        //                    //Console.ReadKey();
        //                    IHTMLInputElement txt19 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.RES_FNO.01", 0);
        //                    IHTMLInputElement txt20 = (IHTMLInputElement)myDoc.all.item("FRM_VISA_SINGLE_FORM_B.DEFAULT.APP_TP.01", 0);
        //                    IHTMLInputElement txt21 = (IHTMLInputElement)myDoc.all.item("p_object_name", 0);
        //                    IHTMLInputElement txt22 = (IHTMLInputElement)myDoc.all.item("p_instance", 0);
        //                    IHTMLInputElement txt23 = (IHTMLInputElement)myDoc.all.item("p_event_type", 0);
        //                    IHTMLInputElement txt24 = (IHTMLInputElement)myDoc.all.item("p_user_args", 0);
        //                    IHTMLInputElement txt25 = (IHTMLInputElement)myDoc.all.item("p_session_id", 0);
        //                    string frmName = "WWVM" + txt25.value;
        //                    //if (returnValue == true)
        //                    //{
        //                    //    txt19.value = "-1";
        //                    //    txt20.value = "1";
        //                    //    txt21.value = "FRM_VISA_SINGLE_FORM_B.DEFAULT.NEXT1.01";
        //                    //    txt22.value = "1";
        //                    //    txt23.value = "CUSTOM";
        //                    //    txt24.value = "";
        //                    //    IHTMLFormElement frmMain = (IHTMLFormElement)myDoc.all.item(frmName, 0);
        //                    //    frmMain.submit();
        //                    //}
        //                    txt19.value = "-1";
        //                    txt20.value = "1";
        //                    txt21.value = "FRM_VISA_SINGLE_FORM_B.DEFAULT.NEXT1.01";
        //                    txt22.value = "1";
        //                    txt23.value = "CUSTOM";
        //                    txt24.value = "";
        //                    IHTMLFormElement frmMain = (IHTMLFormElement)myDoc.all.item(frmName, 0);
        //                    frmMain.submit();
        //                    txt21.value = "FRM_VISA_SINGLE_FORM_B.DEFAULT.NEXT1.01";
        //                    txt22.value = "1";
        //                    txt23.value = "ON_CLICK";
        //                    txt24.value = "";
        //                    IHTMLFormElement frmMain1 = (IHTMLFormElement)myDoc.all.item(frmName, 0);
        //                    frmMain1.submit();
        //                    //while (oIE.LocationURL != "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?" + mVal)
        //                    //{

        //                    //}



        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    //Console.WriteLine("Please Enter Refrence Number Here :");
        //                    string[] S = VCode.Split('-');
        //                    string Refrence = S[1];
        //                    string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        //                    string downloadPath = root + "\\Downloads" + "\\CUT-" + Refrence;
        //                    //if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    //{
        //                    //    myDoc = (HTMLDocument)oIE.Document;
        //                    //    IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                    //    slt15.value = "1";
        //                    //    HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                    //    IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);
        //                    //    //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                    //    //txt266.FireEvent("onchange", 0);
        //                    //    txt266.select();
        //                    //    SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                    //    SendKeys.SendWait(" ");       //Click on the browse button
        //                    //    System.Threading.Thread.Sleep(2000);
        //                    //    SendKeys.SendWait(@"E:\Kashif\CUT_Bacup\Exe\FamousIE\ConsoleApplication1\Documents\passportcopy\" + Refrence + "_1.jpg");
        //                    //    SendKeys.SendWait("{ENTER}");
        //                    //    System.Threading.Thread.Sleep(2000);
        //                    //    HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                    //    nxtBtn.click();
        //                    //}
        //                    //while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    //while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //                    //if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    //{
        //                    //    myDoc = (HTMLDocument)oIE.Document;
        //                    //    IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                    //    slt15.value = "2";
        //                    //    HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                    //    IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);

        //                    //    //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                    //    //txt266.FireEvent("onchange", 0);
        //                    //    txt266.select();
        //                    //    SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                    //    SendKeys.SendWait(" ");       //Click on the browse button
        //                    //    System.Threading.Thread.Sleep(2000);

        //                    //    SendKeys.SendWait(@"E:\Kashif\CUT_Bacup\Exe\FamousIE\ConsoleApplication1\Documents\passportcopy\" + Refrence + "_1.jpg");
        //                    //    SendKeys.SendWait("{ENTER}");
        //                    //    System.Threading.Thread.Sleep(2000);
        //                    //    HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                    //    nxtBtn.click();
        //                    //} while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    //while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    {
        //                        myDoc = (HTMLDocument)oIE.Document;
        //                        IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                        slt15.value = "1";
        //                        HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                        IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);
        //                        //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                        //txt266.FireEvent("onchange", 0);
        //                        txt266.select();
        //                        SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                        SendKeys.SendWait(" ");       //Click on the browse button
        //                        System.Threading.Thread.Sleep(2000);
        //                        //SendKeys.SendWait(@"E:\Kashif\CUT_Bacup\Exe\FamousIE\ConsoleApplication1\Documents\passportcopy\" + Refrence + "_1.jpg");
        //                        SendKeys.SendWait(@"" + downloadPath + "_1.jpg");
        //                        SendKeys.SendWait("{ENTER}");
        //                        System.Threading.Thread.Sleep(2000);
        //                        HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                        nxtBtn.click();
        //                        //File.WriteAllText(@"E:\Kashif\CUT_Bacup\FamousIE\ConsoleApplication1\Documents\passportcopy\TestRun.txt", "Test Run Complete");
        //                    }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    {
        //                        myDoc = (HTMLDocument)oIE.Document;
        //                        IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                        slt15.value = "2";
        //                        HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                        IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);
        //                        //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                        //txt266.FireEvent("onchange", 0);
        //                        txt266.select();
        //                        SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                        SendKeys.SendWait(" ");       //Click on the browse button
        //                        System.Threading.Thread.Sleep(2000);
        //                        SendKeys.SendWait(@"" + downloadPath + "_2.jpg");
        //                        SendKeys.SendWait("{ENTER}");
        //                        System.Threading.Thread.Sleep(2000);
        //                        HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                        nxtBtn.click();
        //                        //File.WriteAllText(@"E:\Kashif\CUT_Bacup\FamousIE\ConsoleApplication1\Documents\passportcopy\TestRun.txt", "Test Run Complete");
        //                    }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    {
        //                        myDoc = (HTMLDocument)oIE.Document;
        //                        IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                        slt15.value = "2";
        //                        HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                        IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);
        //                        //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                        //txt266.FireEvent("onchange", 0);
        //                        txt266.select();
        //                        SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                        SendKeys.SendWait(" ");       //Click on the browse button
        //                        System.Threading.Thread.Sleep(2000);
        //                        SendKeys.SendWait(@"" + downloadPath + "_3.jpg");
        //                        SendKeys.SendWait("{ENTER}");
        //                        System.Threading.Thread.Sleep(2000);
        //                        HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                        nxtBtn.click();
        //                        //File.WriteAllText(@"E:\Kashif\CUT_Bacup\FamousIE\ConsoleApplication1\Documents\passportcopy\TestRun.txt", "Test Run Complete");
        //                    }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //                    //if ((oIE.LocationURL).Substring(0, 61) == "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.DYN_EXP.show?")
        //                    //{
        //                    //    myDoc = (HTMLDocument)oIE.Document;
        //                    //    IHTMLSelectElement slt15 = (IHTMLSelectElement)myDoc.all.item("doc_type", 0);
        //                    //    slt15.value = "2";
        //                    //    HTMLInputElement txt266 = (HTMLInputElement)myDoc.all.item("file1", 0);
        //                    //    IHTMLInputFileElement txt26 = (IHTMLInputFileElement)myDoc.all.item("file1", 0);
        //                    //    //txt26.value = @"C:\Users\AbWahid\Downloads\T123456795Pic.jpg";
        //                    //    //txt266.FireEvent("onchange", 0);
        //                    //    txt266.select();
        //                    //    SendKeys.SendWait("{TAB}");   //Navigate to the Browse button
        //                    //    SendKeys.SendWait(" ");       //Click on the browse button
        //                    //    System.Threading.Thread.Sleep(2000);
        //                    //    SendKeys.SendWait(@"E:\Kashif\CUT_Bacup\Exe\FamousIE - Copy (2)\ConsoleApplication1\Documents\passportcopy\" + Refrence + "_5.jpg");
        //                    //    SendKeys.SendWait("{ENTER}");
        //                    //    System.Threading.Thread.Sleep(2000);
        //                    //    HTMLInputElement nxtBtn = (HTMLInputElement)myDoc.all.item("p_add", 0);
        //                    //    nxtBtn.click();
        //                    //    File.WriteAllText(@"E:\Kashif\CUT_Bacup\Exe\FamousIE\ConsoleApplication1\Documents\passportcopy\TestRun.txt", "Test Run Complete");
        //                    //}


        //                }
        //            }
        //        }
        //        return true;


        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
        #endregion
        #endregion

        #region GetVisaStatus
        #region StatusAutomation
        //public static DBHelper.DBReturnCode GetVisaStatus(string VCode, string ApplicationNo, string Username, string Password)
        //{
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    DataTable dtResult = new DataTable();
        //    string Status;
        //    retCode = VisaDetailsManager.GetVisaByCode(out dtResult, VCode);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        bool PostingStatus = IEStatusAutomation(ApplicationNo, Username, Password, out Status);
        //        if (PostingStatus == true)
        //        {

        //            retCode = UpdateVisaStatus(VCode, Status);
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {
        //                retCode = DBHelper.DBReturnCode.SUCCESS;
        //            }
        //            else
        //            {
        //                retCode = DBHelper.DBReturnCode.EXCEPTION;
        //            }
        //        }
        //        else
        //        {
        //            retCode = DBHelper.DBReturnCode.EXCEPTION;
        //        }

        //    }
        //    else
        //    {
        //        retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    }

        //    return retCode;
        //}
        #endregion

        #region IEStatus
        //public static bool IEStatusAutomation(string ApplicationNo, string Username, string Password, out string IEStatus)
        //{
        //    string Status = "";
        //    try
        //    {

        //        //string[,] arr1 = objBL.tblRecords();
        //        object mVal = System.Reflection.Missing.Value;
        //        string url = "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=";
        //        InternetExplorer oIE = new InternetExplorer();
        //        //ShowWindow((IntPtr)oIE.HWND, SW_MAXIMISE);
        //        oIE.Visible = true;
        //        oIE.Navigate(url, ref mVal, ref mVal, ref mVal, ref mVal);
        //        //oIE.Offline = true;
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //        if (oIE.LocationURL == url)
        //        {
        //            HTMLDocument myDoc = new HTMLDocument();
        //            myDoc = (HTMLDocument)oIE.Document;
        //            HTMLInputElement userID = (HTMLInputElement)myDoc.all.item("ssousername", 0);
        //            userID.value = "ofwpl17";
        //            //userID.value = Username;
        //            HTMLInputElement pwd = (HTMLInputElement)myDoc.all.item("password", 0);
        //            pwd.value = "Smart2016";
        //            //pwd.value = Password;
        //            HTMLInputElement terms = (HTMLInputElement)myDoc.all.item("terms", 0);
        //            terms.defaultChecked = true;
        //            //IHTMLWindow2 win = (IHTMLWindow2)myDoc.parentWindow;
        //            //win.execScript("fnValidate()", "javascript");
        //            while (oIE.LocationURL != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {

        //            }

        //            //while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //            if (oIE.LocationURL == "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {
        //                oIE.Navigate("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.QUERY_RESULT_FOR_INQUIRYSINGLE.show?p_arg_names=app_id&p_arg_values=" + ApplicationNo + "&p_arg_names=spn&p_arg_values=8247");

        //                //oIE.Navigate("https://www.ednrd.ae/portal/page/portal/onlineapp/visa:visaapp", ref mVal, ref mVal, ref mVal, ref mVal);
        //                //while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }
        //                //while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }

        //                //if (oIE.LocationURL == "https://www.ednrd.ae/portal/page/portal/onlineapp/visa:visaapp")
        //                //{
        //                //oIE.Navigate("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.dBPK_ENTRYPERMIT.CheckStatus");
        //                //while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //                //while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //                //do while (oIE.Busy) { System.Threading.Thread.Sleep(1000); }
        //                //myDoc = (HTMLDocument)oIE.Document;
        //                //IHTMLInputElement txt1 = (IHTMLInputElement)myDoc.all.item("p_batch", 0);
        //                //while (txt1 == null)
        //                //{
        //                //    txt1 = (IHTMLInputElement)myDoc.all.item("p_batch", 0);

        //                //}
        //                //txt1.value = ApplicationNo;//First Name //
        //                //oIE.Navigate("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.QUERY_RESULT_FOR_INQUIRYSINGLE.show?p_arg_names=app_id&p_arg_values="+ApplicationNo+"&p_arg_names=spn&p_arg_values=8247");
        //                while (oIE.LocationURL != "https://www.ednrd.ae/portal/pls/portal/INIMM_DB.QUERY_RESULT_FOR_INQUIRYSINGLE.show?p_arg_names=app_id&p_arg_values=" + ApplicationNo + "&p_arg_names=spn&p_arg_values=8247") { }
        //                //myDoc = (HTMLDocument)oIE.Document;
        //                //mshtml.HTMLTable table = myDoc.getElementsByTagName("table") as mshtml.HTMLTable;
        //                //while (table == null) { table = myDoc.getElementsByTagName("table") as mshtml.HTMLTable; }
        //                mshtml.IHTMLDocument2 htmlDoc = myDoc.documentElement.document as mshtml.IHTMLDocument2;
        //                string content = htmlDoc.body.outerHTML;
        //                Status = GetStatus(content);
        //                if (Status != "")
        //                {
        //                    oIE.Visible = false;
        //                    IEStatus = Status;
        //                    return true;
        //                }
        //                else
        //                {
        //                    oIE.Visible = false;
        //                    IEStatus = "";
        //                    return false;
        //                }
        //                //content = content.Replace("\r\n", "");
        //                //string Url ="<a href=\"https://www.ednrd.ae/portal/pls/portal/INIMM_DB.RPT_STATUS_CHK.SHOW?p_arg_names=app_id&amp;p_arg_values=44332405&amp;p_arg_names=trans_tp&amp;p_arg_values=87\">";
        //                //string[] s = Regex.Split(content, "</script>");
        //                //string[] Newsplited = Regex.Split(s[2], "CLICKUR TRIP TRAVELS AND TOURISM L L C");
        //                //string[] status = Regex.Split(Newsplited[1], "<a");
        //                //string[] SplitedStatus = Regex.Split(status[1], ">");
        //                //status = Regex.Split(SplitedStatus[1], "</a");

        //                //}
        //            }
        //        }
        //        oIE.Visible = false;
        //        IEStatus = Status;
        //        return true;


        //    }
        //    catch
        //    {
        //        IEStatus = "";
        //        return false;
        //    }
        //}
        #endregion

        #region GetStatus
        public static string GetStatus(string Content)
        {
            Content = Content.Replace("\r\n", "");
            string Status;
            string Url = "<a href=\"https://www.ednrd.ae/portal/pls/portal/INIMM_DB.RPT_STATUS_CHK.SHOW?p_arg_names=app_id&amp;p_arg_values=44332405&amp;p_arg_names=trans_tp&amp;p_arg_values=87\">";
            string[] s = Regex.Split(Content, "</script>");
            string[] Newsplited = Regex.Split(s[2], "CLICKUR TRIP TRAVELS AND TOURISM L L C");
            if (Newsplited.Length == 2)
            {
                string[] status = Regex.Split(Newsplited[1], "<a");
                string[] SplitedStatus = Regex.Split(status[1], ">");
                status = Regex.Split(SplitedStatus[1], "</a");
                Status = status[0];
            }
            else
            {
                Status = "";
            }

            return Status;
        }
        #endregion
        #endregion

        #region Update Visa Status
        public static DBHelper.DBReturnCode UpdateVisaStatus(string RefNo, string status)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Status", status);
            sqlParams[1] = new SqlParameter("@Vcode", RefNo);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByUpdateByIEStatus", out rowsAffected, sqlParams);
            return retCode;

        }
        #endregion

        #region Update Multipule Status
        #region MultipuleStatusAutomation
        //public static DBHelper.DBReturnCode GetMultipuleStatus(string[] VCode, string[] ApplicationNo, string Username, string Password)
        //{
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    string[] Status;
        //    bool PostingStatus = IEMultiPuleStatus(ApplicationNo, Username, Password, out Status);
        //    if (PostingStatus == true)
        //    {

        //        retCode = UpdateMultipuleStatus(VCode, Status);
        //        if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //        {
        //            retCode = DBHelper.DBReturnCode.SUCCESS;
        //        }
        //        else
        //        {
        //            retCode = DBHelper.DBReturnCode.EXCEPTION;
        //        }
        //    }
        //    else
        //    {
        //        retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    }
        //    return retCode;
        //}
        #endregion

        #region IEM Multipule Status
        //public static bool IEMultiPuleStatus(string[] ApplicationNo, string Username, string Password, out string[] IEStatus)
        //{
        //    string[] Status = null;
        //    IEStatus = null;
        //    int i = 0;
        //    try
        //    {

        //        //string[,] arr1 = objBL.tblRecords();
        //        object mVal = System.Reflection.Missing.Value;
        //        string url = "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=";
        //        InternetExplorer oIE = new InternetExplorer();
        //        //ShowWindow((IntPtr)oIE.HWND, SW_MAXIMISE);
        //        oIE.Visible = true;
        //        oIE.Navigate(url, ref mVal, ref mVal, ref mVal, ref mVal);
        //        //oIE.Offline = true;
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //        while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //        if (oIE.LocationURL == url)
        //        {
        //            HTMLDocument myDoc = new HTMLDocument();
        //            myDoc = (HTMLDocument)oIE.Document;
        //            HTMLInputElement userID = (HTMLInputElement)myDoc.all.item("ssousername", 0);
        //            userID.value = "ofwpl17";
        //            //userID.value = Username;
        //            HTMLInputElement pwd = (HTMLInputElement)myDoc.all.item("password", 0);
        //            pwd.value = "Smart2016";
        //            //pwd.value = Password;
        //            HTMLInputElement terms = (HTMLInputElement)myDoc.all.item("terms", 0);
        //            terms.defaultChecked = true;
        //            //IHTMLWindow2 win = (IHTMLWindow2)myDoc.parentWindow;
        //            //win.execScript("fnValidate()", "javascript");
        //            while (oIE.LocationURL != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {

        //            }

        //            //while (oIE.Busy) { System.Threading.Thread.Sleep(4000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }
        //            while (oIE.Busy) { System.Threading.Thread.Sleep(2000); }

        //            if (oIE.LocationURL == "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {
        //                for (i = 0; i < ApplicationNo.Length; i++)
        //                {
        //                    oIE.Navigate("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.QUERY_RESULT_FOR_INQUIRYSINGLE.show?p_arg_names=app_id&p_arg_values=" + ApplicationNo + "&p_arg_names=spn&p_arg_values=8247");
        //                    mshtml.IHTMLDocument2 htmlDoc = myDoc.documentElement.document as mshtml.IHTMLDocument2;
        //                    string content = htmlDoc.body.outerHTML;
        //                    Status[i] = GetStatus(content);
        //                    if (Status[i] != "")
        //                    {
        //                        oIE.Visible = false;
        //                        IEStatus[i] = Status[i];
        //                        //return true;
        //                    }
        //                    else
        //                    {
        //                        oIE.Visible = false;
        //                        IEStatus[i] = "";
        //                        //return false;
        //                    }

        //                }

        //            }
        //        }
        //        oIE.Visible = false;
        //        IEStatus = Status;
        //        return true;


        //    }
        //    catch
        //    {
        //        IEStatus = null;
        //        return false;
        //    }
        //}
        #endregion

        public static DBHelper.DBReturnCode UpdateMultipuleStatus(string[] RefNo, string[] status)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            int i = 0;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            for (i = 0; i < RefNo.Length; i++)
            {
                sqlParams[0] = new SqlParameter("@Status", status[i]);
                sqlParams[1] = new SqlParameter("@Vcode", RefNo[i]);
                retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByUpdateByIEStatus", out rowsAffected, sqlParams);
            }
            return retCode;

        }
        #endregion

    }
}