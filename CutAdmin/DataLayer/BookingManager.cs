﻿using CommonLib.Response;
using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Globalization;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class BookingManager
    {
        public static ReservationDetails objReservation { get; set; }
        //#region Trasaction  Validate
        //public static float TransactionAmount(string Type)
        //{
        //    //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        //    float Amount = 0;
        //    if (Type == "AG")
        //    {
        //        if (objReservation.ListCharges == null)
        //            objReservation.ListCharges = new List<CommonLib.Response.TaxRate>();
        //        Amount = (objReservation.TotalAmount +
        //                // objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()
        //                // + (objReservation.AdminCommission) 
        //                 + objReservation.AddOnsCharge);
        //        objReservation.TotalPayable = Amount;
        //    }
        //    return Amount;
        //}

        //public static void SaveBookinTax()
        //{
        //    //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        //    try
        //    {
        //        using (var dbTax = new dbHotelhelperDataContext())
        //        {
        //            List<Comm_BookingTax> ListBookingTax = new List<Comm_BookingTax>();
        //            foreach (var objCharge in objReservation.ListCharges)
        //            {
        //                string TaxOnDetails = "";
        //                foreach (var objTax in objCharge.TaxOn)
        //                {
        //                    TaxOnDetails += objTax.TaxName + ":" + objTax.TaxRate + "^";
        //                }
        //                ListBookingTax.Add(new Comm_BookingTax
        //                {
        //                    ReservationID = objReservation.objHotelDetails.ReservationID,
        //                    RateName = objCharge.RateName,
        //                    Amount = Convert.ToDecimal(objCharge.TotalRate),
        //                    RateBreckups = TaxOnDetails
        //                });

        //            }
        //            dbTax.Comm_BookingTaxes.InsertAllOnSubmit(ListBookingTax);
        //            dbTax.SubmitChanges();
        //        }
        //    }
        //    catch 
        //    {

        //    }
        //}

        //public static string GetInvoice(string Service, string UserType)
        //{
        //    using (var db = new helperDataContext())
        //    {
        //        //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
        //        string InvoiceNo = "0";
        //        string Prefix = "MQ-";
        //        Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
        //        Int64 uid = AccountManager.GetSupplierByUser();
        //        AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"]; ;

        //        if (Service == "Visa")
        //            Prefix = "VQ-";
        //        if (Service == "Package")
        //            Prefix = "PQ-";
        //        if (Service == "Hotel")
        //            Prefix = "HQ-";
        //        if (Service == "Slightseeing")
        //            Prefix = "SQ-";
        //        if (Service == "SQ")
        //            Prefix = "VQ-";
        //        if (Service == "Airline")
        //            Prefix = "AQ-";
        //        var arrLastInvoice = 0;
        //        ParentID = uid;
        //        if (UserType == "FR" || UserType == "SP")
        //            arrLastInvoice = (from obj in db.tbl_Invoices where obj.ServiceType == Service && obj.ParentID == uid select obj).ToList().Count;
        //        else if (UserType == "AG")
        //            arrLastInvoice = (from obj in db.tbl_Invoices where obj.ServiceType == Service && obj.ParentID == uid select obj).ToList().Count;
        //        InvoiceNo = Prefix + (arrLastInvoice + 1).ToString("D" + 6);
        //        return InvoiceNo;
        //    }
        //}

        //public static DBHelper.DBReturnCode ValidateTransaction(bool BackEnd)
        //{
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    bool transaction = false, franch_transaction = false;
        //    if (HttpContext.Current.Session["Price"] != null || BackEnd)
        //    {

        //        HttpContext.Current.Session["Price"] = null; /*To Validate duplicate Entry*/
        //        Int64 uid = 0;
        //        Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
        //        GlobalDefault objGlobalDefault = new GlobalDefault();
        //        AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //        tbl_AdminCreditLimit objFranchiseeCredit = new tbl_AdminCreditLimit();
        //        if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
        //        {
        //            objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
        //            uid = objAgentDetailsOnAdmin.sid;
        //        }
        //        else if (HttpContext.Current.Session["LoginUser"] != null)
        //        {
        //            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            uid = objGlobalDefault.sid;
        //        }
        //        if (ParentID != 232)
        //        {
        //            #region CheckWhiteLabelBalance /* -Check White Lablel Agent Balance- */
        //            franch_transaction = CheckMinBalance(ParentID, (TransactionAmount("FR")));
        //            #endregion
        //        }
        //            #region UserBalanceCheck /*-Check White Lablel Agent Balance- */
        //        transaction = CheckMinBalance(uid, TransactionAmount("AG"));
        //        #endregion

        //        if (transaction == true && (ParentID != 232 && franch_transaction == false))
        //        {
        //            //OnHold = true;
        //            transaction = false;
        //            retCode = DBHelper.DBReturnCode.EXCEPTION;
        //        }
        //        if (transaction)
        //            retCode = DBHelper.DBReturnCode.SUCCESS;
        //        else
        //            retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    }
        //    else if (HttpContext.Current.Session["Price"] == null)
        //    {
        //        retCode = DBHelper.DBReturnCode.DUPLICATEENTRY;
        //    }

        //    return retCode;
        //}

        //public static bool CheckMinBalance(Int64 uid, float Purchase)
        //{
        //    //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
        //    using (var db = new helperDataContext())
        //    {
        //        bool valid = true;
        //        tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //        #region User Balance Checking
        //        objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //        float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
        //        float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
        //        float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
        //        bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
        //        bool UserOTC = (bool)objUserCredit.OTC;
        //        float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //        float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //        if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
        //        {
        //            valid = true;
        //        }
        //        else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
        //        {
        //            valid = true;
        //        }
        //        else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
        //        {
        //            valid = true;
        //        }
        //        else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
        //        {

        //            valid = true;
        //        }
        //        else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
        //        {
        //            valid = true;
        //        }
        //        else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
        //        {
        //            valid = true;
        //        }

        //        else
        //        {
        //            valid = false;
        //        }
        //        #endregion
        //        return valid;
        //    }
        //}

        //public static bool DebitAccount(Int64 uid, float Purchase)
        //{
        //    //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
        //    bool valid = false;
        //    try
        //    {
        //        using (var db = new helperDataContext())
        //        {
        //            tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //            #region User Balance Checking
        //            objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //            float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
        //            float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
        //            float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
        //            bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
        //            bool UserOTC = (bool)objUserCredit.OTC;
        //            float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //            float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //            if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                valid = true;
        //            }
        //            else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                float OtcReturn = 0;
        //                if (UserAvailableCredit < 0)
        //                {
        //                    OtcReturn = @UserOTClimit + UserAvailableCredit;
        //                }
        //                if (@UserCreditlimit >= OtcReturn)
        //                    objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit + OtcReturn);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.@OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.@OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                if (objUserCredit.AvailableCredit < 0)
        //                    objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit) + Convert.ToDecimal(objUserCredit.AvailableCredit);
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit - Purchase);
        //                valid = true;
        //            }
        //            else
        //            {
        //                valid = false;
        //            }

        //            if (valid)
        //                db.SubmitChanges();

        //            #endregion
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return valid;
        //}

        //public static bool CreditAccount(Int64 uid, float Purchase)
        //{
        //    //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
        //    bool valid = false;
        //    try
        //    {
        //        using (var db = new helperDataContext())
        //        {
        //            tbl_AdminCreditLimit objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //            #region User Balance Checking
        //            objUserCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == uid select obj).FirstOrDefault();
        //            float UserAvailableCredit = Convert.ToSingle(objUserCredit.AvailableCredit);
        //            float @UserCreditlimit = Convert.ToSingle(objUserCredit.CreditAmount);
        //            float @UserCreditAmount_Cmp = Convert.ToSingle(objUserCredit.CreditAmount_Cmp);
        //            bool UserCredit_Flag = (bool)objUserCredit.Credit_Flag;
        //            bool UserOTC = (bool)objUserCredit.OTC;
        //            float @UserOTClimit = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //            float @UserMAXCreditlimit_Cmp = Convert.ToSingle(objUserCredit.MaxCreditLimit);
        //            if (UserAvailableCredit >= 0 && UserAvailableCredit >= Purchase)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                valid = true;
        //            }
        //            else if (UserAvailableCredit <= 0 && @UserCreditlimit <= 0 && @UserOTClimit >= Purchase && UserOTC == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit + @UserCreditlimit) >= Purchase && @UserOTC == true && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(UserAvailableCredit - Purchase);
        //                float OtcReturn = 0;
        //                if (UserAvailableCredit < 0)
        //                {
        //                    OtcReturn = @UserOTClimit + UserAvailableCredit;
        //                }
        //                if (@UserCreditlimit >= OtcReturn)
        //                    objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit + OtcReturn);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.@OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserOTClimit) >= Purchase && UserOTC == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                objUserCredit.MaxCreditLimit = 0;
        //                objUserCredit.MaxCreditLimit_Cmp = 0;
        //                objUserCredit.@OTC = false;
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit >= 0 && (@UserAvailableCredit + @UserCreditlimit) >= Purchase && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                if (objUserCredit.AvailableCredit < 0)
        //                    objUserCredit.CreditAmount = Convert.ToDecimal(@UserCreditlimit) + Convert.ToDecimal(objUserCredit.AvailableCredit);
        //                valid = true;
        //            }
        //            else if (@UserAvailableCredit < 0 && @UserCreditlimit > 0 && @UserCreditlimit >= Purchase && @UserCredit_Flag == true)
        //            {
        //                objUserCredit.AvailableCredit = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                objUserCredit.CreditAmount = Convert.ToDecimal(@UserAvailableCredit - Purchase);
        //                valid = true;
        //            }
        //            else
        //            {
        //                valid = false;
        //            }

        //            if (valid)
        //                db.SubmitChanges();

        //            #endregion
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return valid;
        //}
        //#endregion

        //#region Make Transactions
        //public static bool GetnrateTransaction(Int64 uid, Int64 ParentID, string User, string Type, bool DirectAgent, out string InvoiceNo)
        //{
        //    bool Valid = false;
        //    InvoiceNo = "";
        //    //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        //    //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        //    float BookingAmt = 0;
        //    try
        //    {
        //        using (var dbTax = new dbHotelhelperDataContext ())
        //        {
        //            using (var db = new helperDataContext())
        //            {
        //            #region Genrate Invoices
        //            var arrTransaction = new tbl_Invoice();
        //            arrTransaction.AgentID = uid;
        //            BookingAmt = TransactionAmount(User);
        //            string sInvoiceNo = GetInvoice(Type, User);
        //            if (!sInvoiceNo.Contains("Test")) /* Invoice Test  */
        //            {
        //                Int64 SupplierID = AccountManager.GetSupplierByUser();
        //                int Cycle = Convert.ToInt16((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().CommssionsDays);
        //                bool RefundCancel = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().RefundCancel);
        //                bool onCheckIn = Convert.ToBoolean((from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj).FirstOrDefault().CheckInCommission);
        //                if (User == "FR" || DirectAgent)
        //                {
        //                    arrTransaction.Commission = Convert.ToDecimal(objReservation.AdminCommission);
        //                    arrTransaction.Currency = (from obj in db.tbl_AdminLogins where obj.sid == uid select obj.CurrencyCode).FirstOrDefault();
        //                    arrTransaction.Deleted = false;
        //                    arrTransaction.GstDetails = Convert.ToDecimal(objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum());
        //                    arrTransaction.InvoiceAmount = Convert.ToDecimal(BookingAmt);
        //                    arrTransaction.Commission = Convert.ToDecimal(objReservation.AdminCommission);
        //                    arrTransaction.InvoiceDate = DateTime.Now.ToString("dd/MM/yyyy");
        //                    arrTransaction.InvoiceNo = sInvoiceNo;
        //                    arrTransaction.InvoiceType = "Contarcted";
        //                    arrTransaction.ParentID = AccountManager.GetSupplierByUser(); ;
        //                    arrTransaction.ServiceFee = Convert.ToDecimal(objReservation.AddOnsCharge);
        //                    arrTransaction.ServiceType = Type;
        //                    arrTransaction.Status = 1;
        //                    arrTransaction.TaxCharges = Convert.ToDecimal(0);
        //                    arrTransaction.TDS = Convert.ToDecimal(0);
        //                    DateTime CommDate =DateTime.Now;
        //                    if(onCheckIn)
        //                        CommDate = DateTime.ParseExact(objReservation.objHotelDetails.CheckIn,"dd-MM-yyyy",CultureInfo.InvariantCulture);
        //                    else
        //                    {
        //                        var arrLastReport = (from obj in dbTax.Comm_CommissionReports where obj.SupplierID == SupplierID select obj).ToList().OrderByDescending(d => d.ID).FirstOrDefault();
        //                        if(arrLastReport != null)
        //                            CommDate = DateTime.ParseExact(arrLastReport.InsertDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddDays(Cycle);
        //                    }
        //                    arrTransaction.CommissionDate = CommDate.ToString("dd-MM-yyyy");
        //                    arrTransaction.OnCheckIn = onCheckIn;
        //                    arrTransaction.RefundCancel = RefundCancel;
        //                }
        //                db.tbl_Invoices.InsertOnSubmit(arrTransaction);
        //                dbTax.SubmitChanges();
        //                InvoiceNo = arrTransaction.InvoiceNo;
        //            }
        //            else
        //                InvoiceNo = sInvoiceNo;

        //            #endregion

        //            #region Genrate Booking Statments
        //            var arrBookingTrasaction = new tbl_BookingTransaction();
        //            arrBookingTrasaction.uid = uid;
        //            arrBookingTrasaction.ReservationID = objReservation.objHotelDetails.ReservationID;
        //            arrBookingTrasaction.TransactionDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
        //            arrBookingTrasaction.BookingAmt = Convert.ToDecimal(BookingAmt);
        //            arrBookingTrasaction.SeviceTax = Convert.ToDecimal(objReservation.ServiceTax);
        //            arrBookingTrasaction.BookingAmtWithTax = Convert.ToDecimal(objReservation.TotalPayable);
        //            arrBookingTrasaction.CanAmt = Convert.ToDecimal(objReservation.CancelattionAmount);
        //            arrBookingTrasaction.CanServiceTax = Convert.ToDecimal(0);
        //            arrBookingTrasaction.CanAmtWithTax = Convert.ToDecimal(objReservation.CancelattionAmount);
                   
        //                arrBookingTrasaction.Balance = Convert.ToDecimal((from obj in dbTax.tbl_AdminCreditLimits where obj.uid == uid select obj.AvailableCredit).FirstOrDefault());

        //                arrBookingTrasaction.SalesTax = 0;
        //                arrBookingTrasaction.LuxuryTax = 0;
        //                arrBookingTrasaction.DiscountPer = 0;
        //                arrBookingTrasaction.BookingCreationDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
        //                arrBookingTrasaction.BookingRemarks = objReservation.objHotelDetails.Remarks;
        //                arrBookingTrasaction.BookingCancelFlag = false;
        //                arrBookingTrasaction.Supplier = "From Hotel";
        //                arrBookingTrasaction.SupplierBasicAmt = Convert.ToDecimal(objReservation.TotalAmount);
        //                if (User == "FR" || DirectAgent)
        //                    arrBookingTrasaction.AgentComm = Convert.ToDecimal(objReservation.AdminCommission);
        //                arrBookingTrasaction.CUTComm = Convert.ToDecimal(objReservation.AdminCommission);
        //                arrBookingTrasaction.BookedBy = objReservation.objHotelDetails.Uid.ToString();
        //                arrBookingTrasaction.AffilateCode = objReservation.AffilateCode;
        //                arrBookingTrasaction.Status = "Booking Success";
        //                dbTax.tbl_BookingTransactions.InsertOnSubmit(arrBookingTrasaction);
        //                dbTax.SubmitChanges();

        //                var arrParticulars = new tbl_Transaction();
        //                arrParticulars.uid = uid;
        //                arrParticulars.TransactionID = arrBookingTrasaction.sid;
        //                arrParticulars.Particulars = "On Booking " + Type + " : " + objReservation.objHotelDetails.ReservationID;
        //                arrParticulars.UpdatedBy = objReservation.objHotelDetails.AgentRef;
        //                arrParticulars.CreditedAmount = 0;
        //                arrParticulars.DebitedAmount = Convert.ToDecimal(BookingAmt);
        //                arrParticulars.Balance = Convert.ToDecimal((from obj in dbTax.tbl_AdminCreditLimits where obj.uid == uid select obj.AvailableCredit).FirstOrDefault());
        //                arrParticulars.ReservationID = objReservation.objHotelDetails.ReservationID;
        //                arrParticulars.ReferenceNo = InvoiceNo;
        //                arrParticulars.TransactionDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm");
        //                dbTax.tbl_Transactions.InsertOnSubmit(arrParticulars);
        //                db.SubmitChanges();
        //                dbTax.SubmitChanges();
        //                Valid = true;
        //            }
        //            #endregion
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return Valid;
        //}

        //public static DBHelper.DBReturnCode BookingTransact(string ServiceType)
        //{
        //     static helperDataContext db = new helperDataContext();
        //    DBHelper.DBReturnCode Transaction = DBHelper.DBReturnCode.EXCEPTION;
        //    string AffilateCode = "HTL-AE-123";
        //    bool transaction = false, franch_transaction = false;
        //    try
        //    {
        //        string InvoiceNo = "-";
        //        #region Genrate Transactions
        //        Int64 uid = 0;
        //        Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
        //        GlobalDefault objGlobalDefault = new GlobalDefault();
        //        AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //        tbl_AdminCreditLimit objFranchiseeCredit = new tbl_AdminCreditLimit();
        //        if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
        //        {
        //            objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
        //            uid = objAgentDetailsOnAdmin.sid;
        //        }
        //        else if (HttpContext.Current.Session["LoginUser"] != null)
        //        {
        //            //objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            //uid = objGlobalDefault.sid;
        //            //if (objGlobalDefault.UserType != "Supplier")
        //            //    uid = objGlobalDefault.ParentId;
        //            uid = AccountManager.GetUserByLogin();
                   
        //        }
        //        //if (objGlobalDefault.ParentId != 232)
        //        //{
        //        //    #region WhiteLabel Transaction  /* -Check White Lablel Agent Balance- */
        //        //    franch_transaction = DebitAccount(objGlobalDefault.ParentId, TransactionAmount("FR"));
        //        //    if (franch_transaction )/* Make Transaction Statments For Franchisee*/
        //        //    {
        //        //        GetnrateTransaction(objGlobalDefault.ParentId, 232, "FR", ServiceType, false, out InvoiceNo); /* To Make Franchisee Statments */
        //        //    }
        //        //    #endregion
        //        //}
        //        SaveBookinTax(); /*Tax Breckups*/
        //        /*-Agent Transactions -*/
        //        //sif ((franch_transaction || objGlobalDefault.ParentId == 232) )
        //        {
        //            transaction = DebitAccount(uid, TransactionAmount("AG"));
        //            if (transaction)
        //            {
        //                string AgInvoice = "";
        //                bool DirectAgent = false;
        //                //if (objGlobalDefault.ParentId == 232)
        //                    DirectAgent = true;
        //                GetnrateTransaction(uid, objGlobalDefault.ParentId, "AG", ServiceType, DirectAgent, out AgInvoice);/* To Agent  Statments */
        //                if (InvoiceNo == "-")
        //                    InvoiceNo = AgInvoice;
        //                else
        //                    InvoiceNo = InvoiceNo + "|" + AgInvoice;
        //            }
        //            else
        //            {
        //                transaction = false;
        //                //objReservation.BookingStatus = "Hold";
        //            }
        //        }
        //        //if ((objGlobalDefault.ParentId != 232 && franch_transaction == false))
        //        //{
        //        //    transaction = false;
        //        //    //objReservation.BookingStatus = "Hold";

        //        //}
        //        if (transaction)
        //        {
        //            var arrResrvation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == objReservation.objHotelDetails.ReservationID select obj).FirstOrDefault();
        //            arrResrvation.InvoiceID = InvoiceNo;
        //            db.SubmitChanges();
        //            Transaction = DBHelper.DBReturnCode.SUCCESS;
        //        }
               
        //        else
        //        {
        //            //objReservation.BookingStatus = "Hold";
        //            Transaction = DBHelper.DBReturnCode.EXCEPTION;
        //        }
        //        #endregion
        //    }
        //    catch
        //    {

        //    }
        //    return Transaction;
        //}
        //#endregion

        //#region Cancel Confirm Booking
        //public static string GenrateCancellationMail(string ReservationID, Int64 UID, string User)
        //{
        //    dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        //    helperDataContext db = new helperDataContext();
        //    StringBuilder sMail = new StringBuilder();
        //    string Url = ConfigurationManager.AppSettings["Domain"];
        //    var arrReservation = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
        //    if (arrReservation != null)
        //    {

        //        var arrUserDetails = (from obj in db.tbl_AdminLogins where obj.sid == UID select obj).FirstOrDefault();
        //        var arrBookingTrasaction = (from obj in dbTax.tbl_BookingTransactions where obj.ReservationID == ReservationID && obj.uid == UID select obj).FirstOrDefault();
        //        sMail.Append("<!DOCTYPE html>  <html lang=\"en\"  xmlns=\"http://www.w3.org/1999/xhtml\"> <head>  ");
        //        sMail.Append("<meta charset=\"utf-8\" /> </head>");
        //        sMail.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
        //        sMail.Append("<div style=\"height:50px;width:auto\"><br /> ");
        //        if (User == "FR")
        //            sMail.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
        //        else
        //            sMail.Append("<img src=\"" + Url + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
        //        sMail.Append("</div>   ");
        //        sMail.Append("<div style=\"margin-left:10%\">  ");
        //        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hi " + arrUserDetails.AgencyName + ",</span><br /></p> ");
        //        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p> ");
        //        sMail.Append("<style type=\"text/css\">  ");
        //        sMail.Append(".tg  {border-collapse:collapse;border-spacing:0;}   ");
        //        sMail.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
        //        sMail.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
        //        sMail.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}  ");
        //        sMail.Append(".tg .tg-yw4l{vertical-align:top}   ");
        //        sMail.Append(" @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>   ");
        //        sMail.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
        //        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
        //        sMail.Append(" <td class=\"tg-yw4l\">:  " + ReservationID + "</td></tr>");
        //        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Reservation Status</b></td>    ");
        //        sMail.Append("<td class=\"tg-yw4l\">:" + arrReservation.BookingStatus + "</td></tr>");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Reservation Date</b></td>       ");
        //        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.ReservationDate + "</td></tr>");
        //        sMail.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>                 ");
        //        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.HotelName + "</td></tr>");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Location</b></td>               ");
        //        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.City + "</td></tr>                 ");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-In</b></td>               ");
        //        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckIn + "</td></tr>");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-Out </b> </td>            ");
        //        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckOut + "</td></tr>");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Rate</b></td> ");
        //        sMail.Append("<td class=\"tg-yw4l\">:" + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt / arrReservation.NoOfDays) + "</td></tr>");
        //        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Total  </b></td>");
        //        sMail.Append("<td class=\"tg-yw4l\">:  " + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt) + "</td></tr>");
        //        sMail.Append("</table></div>");
        //        sMail.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
        //        sMail.Append("<span style=\"margin-left:2%\">");
        //        sMail.Append("<b>Thank You,</b><br />                                                                                                                        ");
        //        sMail.Append("</span>");
        //        sMail.Append("<span style=\"margin-left:2%\">");
        //        sMail.Append("Administrator");
        //        sMail.Append("</span>");
        //        sMail.Append("</div>");
        //        sMail.Append("<div>");
        //        sMail.Append(" <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
        //        sMail.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
        //        //if (User == "FR")
        //        //     sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  ClickurTrip.com</div></td>");
        //        //else
        //        sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + ConfigurationManager.AppSettings["DomainUser"] + "</div></td>");
        //        sMail.Append("</tr> </table></div>");
        //        sMail.Append("</body></html>");


        //        var arrEmails = new tbl_ActivityMail();
        //        if (User != "FR")
        //            arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == arrUserDetails.ParentID && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();
        //        else
        //            arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == 232 && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();

        //        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
        //        List<string> from = new List<string>();
        //        List<string> DocPathList = new List<string>();
        //        Dictionary<string, string> Email1List = new Dictionary<string, string>();
        //        Dictionary<string, string> BccList = new Dictionary<string, string>();
        //        if (arrEmails.Email != "")
        //            from.Add(Convert.ToString(arrEmails.Email));
        //        else
        //            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
        //        foreach (var Mails in arrEmails.CcMail.Split(','))
        //        {
        //            if (Mails != "")
        //            {
        //                BccList.Add(Mails, Mails); ;
        //            }
        //        }
        //        if (User != "AG" && BccList.Count == 0)
        //        {
        //            var arrAdmin = (from obj in db.tbl_AdminLogins where obj.sid == arrUserDetails.ParentID select obj).First();
        //            BccList.Add(arrAdmin.AgencyName, arrAdmin.uid);
        //        }

        //        Email1List.Add(arrUserDetails.AgencyName, arrUserDetails.uid);
        //        string title = "Cancel Confirmatio : " + ReservationID;
        //        MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
        //    }

        //    return sMail.ToString();
        //}

        //#endregion

        //#region AgentDetails  On Admin
        //public static bool GetAgentDetails(Int64 uid)
        //{
        //     static helperDataContext db = new helperDataContext();
        //    var list = (from obj in db.tbl_AdminLogins where obj.sid == uid select obj).FirstOrDefault();

        //    AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //    objAgentDetailsOnAdmin.sid = list.sid;
        //    objAgentDetailsOnAdmin.Currency = list.CurrencyCode;
        //    objAgentDetailsOnAdmin.UserType = list.UserType;
        //    objAgentDetailsOnAdmin.AgencyName = list.AgencyName;
        //    objAgentDetailsOnAdmin.Agentuniquecode = list.Agentuniquecode;

        //    HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
        //    return true;

        //}
        //#endregion

        //#region Commission Calculations
        //public static float AdminCommission(Int64 HotelCode, Int64 noNights)
        //{
        //    float AdminCommission = 0;
        //    try
        //    {
        //        Int64 Uid=0;
        //        if (HttpContext.Current.Session["LoginUser"] == null)
        //            throw new Exception("Login Failed.");
        //        Uid = AccountManager.GetSupplierByUser();
        //         static helperDataContext db = new helperDataContext();
        //       using (var DB = new dbHotelhelperDataContext())
        //        string Category = (from obj in db.tbl_CommonHotelMasters where obj.sid == HotelCode select obj).FirstOrDefault().HotelCategory;
        //        var arrCommission = (from obj in dbTax.Comm_Commissions where obj.GroupID == Convert.ToInt64(Category) && obj.SupplierID == Uid select obj).FirstOrDefault();
        //        if (arrCommission != null)
        //        {
        //            if (arrCommission.PerNight == true)
        //            {
        //                AdminCommission = Convert.ToSingle(arrCommission.Commission) * noNights;
        //            }
        //            else
        //            {
        //                AdminCommission = Convert.ToSingle(arrCommission.Commission);
        //            }
        //        }
        //        else
        //            AdminCommission = 0;

        //    }
        //    catch(Exception ex)
        //    {
                 
        //    }
        //    return AdminCommission;
        //}
        //#endregion


        //#region ConfirmHoldBooking

        //public static DBHelper.DBReturnCode ConfirmHoldBooking(string ReservationID)
        //{
        //     static helperDataContext db = new helperDataContext();
        //    DBHelper.DBReturnCode confirmed = DBHelper.DBReturnCode.EXCEPTION;
        //    bool response = true;

        //    var ListHotelReservation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();

        //    #region Booking Transactions
        //    CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
        //    objReservation.TotalAmount =(float)ListHotelReservation.TotalFare;
        //    objReservation.AdminCommission = 0;
        //    objReservation.ListCharges = new List<TaxRate>();

        //    //foreach (var objRoom in Supplier[0].Details)
        //    //{
        //    //    foreach (var objRate in objRoom.Rate.TotalCharge.Charge)
        //    //    {
        //    //        if (objReservation.ListCharges.Where(d => d.RateName == objRate.RateName).ToList().Count == 0)
        //    //        {
        //    //            objReservation.ListCharges.Add(objRate);
        //    //        }
        //    //        else
        //    //        {
        //    //            objReservation.ListCharges.Single(d => d.RateName == objRate.RateName).TotalRate += objRate.TotalRate;
        //    //        }
        //    //    }
        //    //}

        //    Int64 noNights = Convert.ToInt64(ListHotelReservation.NoOfDays);
        //    objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(ListHotelReservation.HotelCode), noNights);
        //    //objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
        //    CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
        //    objReservation.TotalPayable = BookingManager.TransactionAmount("AG");
        //    #region Hotel Booking Details
        //    var arrHotel = (from obj in db.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(ListHotelReservation.HotelCode) select obj).FirstOrDefault();
        //    tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
        //    HotelReserv.ReservationID = Convert.ToString(ReservationID);
        //    HotelReserv.HotelCode = ListHotelReservation.HotelCode;
        //    HotelReserv.HotelName = arrHotel.HotelName;
        //    HotelReserv.mealplan = ListHotelReservation.mealplan;
        //    HotelReserv.TotalRooms = Convert.ToInt16(ListHotelReservation.TotalRooms);
        //    //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
        //    HotelReserv.RoomCode = "";
        //    HotelReserv.sightseeing = 0;
        //    HotelReserv.Source = ListHotelReservation.Source;
        //    if (response)
        //        HotelReserv.Status = "Vouchered";
        //    HotelReserv.SupplierCurrency = "INR";
        //    HotelReserv.terms = arrHotel.HotelNote;
        //    HotelReserv.Uid = ListHotelReservation.Uid;
        //    HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
        //    HotelReserv.Updateid = ListHotelReservation.Uid.ToString();
        //    HotelReserv.VoucherID = "VCH-" + ReservationID;
        //    HotelReserv.DeadLine = "";
        //    HotelReserv.AgencyName = "";
        //    HotelReserv.AgentContactNumber = "";
        //    HotelReserv.AgentEmail = "";
        //    HotelReserv.AgentMarkUp_Per = 0;
        //    HotelReserv.AgentRef = "";
        //    HotelReserv.AgentRemark = "";
        //    HotelReserv.AutoCancelDate = "";
        //    HotelReserv.AutoCancelTime = "";
        //    HotelReserv.bookingname = "";
        //    HotelReserv.BookingStatus = "";
        //    HotelReserv.CancelDate = "";
        //    HotelReserv.CancelFlag = false;
        //    HotelReserv.CheckIn = Convert.ToString(ListHotelReservation.CheckIn);
        //    HotelReserv.CheckOut = Convert.ToString(ListHotelReservation.CheckOut);
        //    HotelReserv.ChildAges = "";
        //    HotelReserv.Children = ListHotelReservation.Children;
        //    HotelReserv.City = arrHotel.CityId;
        //    HotelReserv.deadlineemail = false;
        //    HotelReserv.Discount = 0;
        //    HotelReserv.ExchangeValue = 0;
        //    HotelReserv.ExtraBed = 0;
        //    HotelReserv.GTAId = "";
        //    HotelReserv.holdbooking = 0;
        //    HotelReserv.HoldTime = "";
        //    HotelReserv.HotelBookingData = "";
        //    HotelReserv.HotelDetails = "";
        //    HotelReserv.Infants = 0;
        //    HotelReserv.InvoiceID = "-";
        //    HotelReserv.IsAutoCancel = false;
        //    HotelReserv.IsConfirm = false;
        //    HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
        //    HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
        //    HotelReserv.LuxuryTax = 0;
        //    HotelReserv.mealplan_Amt = 0;
        //    HotelReserv.NoOfAdults = ListHotelReservation.NoOfAdults;
        //    Int32 night = Convert.ToInt16(ListHotelReservation.NoOfDays);
        //    night = night - 1;
        //    // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
        //    HotelReserv.NoOfDays = Convert.ToInt16(night);
        //    HotelReserv.OfferAmount = 0;
        //    HotelReserv.OfferId = 0;
        //    HotelReserv.RefAgency = "";
        //    HotelReserv.ReferenceCode = "";
        //    HotelReserv.Remarks = "";
        //    HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
        //    HotelReserv.ReservationTime = "";
        //    HotelReserv.RoomRate = 0;
        //    HotelReserv.SalesTax = 0;
        //    HotelReserv.Servicecharge = 0;
        //   // HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.ListCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
        //    HotelReserv.ComparedCurrency = "";
        //    // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
        //    // HotelReserv.TotalFare = Total;
        //    HotelReserv.TotalFare = Convert.ToDecimal(BookingManager.TransactionAmount("AG") - objReservation.AdminCommission);

        //    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    if(objGlobalDefault.UserType != "Agent")
        //    {
        //     AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
        //     objAgentDetailsOnAdmin.sid =Convert.ToInt64(ListHotelReservation.Uid);
        //     HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
        //    }
        //    #endregion
        //    objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //    objReservation.objHotelDetails = HotelReserv;
        //    BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //    BookingManager.objReservation.objHotelDetails = HotelReserv;
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
        //    if(HotelReserv.Status != "Vouchered")
        //    {
        //        retCode = CutAdmin.DataLayer.BookingManager.BookingTransact("Hotel");
        //    }
        //    else
        //    {
        //        retCode = DBHelper.DBReturnCode.SUCCESS;
        //    }
        //    #endregion

        //    return confirmed = retCode;

        //}

        //#endregion        

    }
}