﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.BL;
using System.Globalization;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class CommissionReports
    {
        public class InvoiceCategory
        {
            public string Category { get; set; }
            public List<string > InvoiceNo { get; set; }
            public decimal Total { get; set; }
        }
        //static dbHotelhelperDataContext db = new dbHotelhelperDataContext();

        #region Old report
        public static void GenrateReports(List<Int64> SupplierId)
        {
            try
            {
               
             
            }
            catch
            {

            }
        }

        public static decimal GetCycleInvoice(List<tbl_Invoice> arrInvoice, DateTime LastDate, Int64 PriviousID, out Int64 LastInvoiceID)
        {
            decimal Commission = 0;
            LastInvoiceID = 0;
            List<tbl_Invoice> arrNewInvoce = new List<tbl_Invoice>();
            try
            {
                foreach (var objInvoice in arrInvoice)
                {
                    DateTime InvoiceDate = DateTime.ParseExact(objInvoice.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    if (InvoiceDate >= LastDate || PriviousID == 0)
                    {
                        if (objInvoice.ID > PriviousID)
                            arrNewInvoce.Add(objInvoice);
                    }
                }
                Commission = arrNewInvoce.Select(d => Convert.ToDecimal(d.Commission)).ToList().Sum();
                LastInvoiceID = arrNewInvoce.OrderByDescending(d => d.ID).FirstOrDefault().ID;
            }
            catch
            {

            }
            return Commission;
        }

        #endregion

        #region new Report
        public static void GenrateCommision(Int64 UserID)
        {
            try
            {
               
            }
            catch
            {
                
            }
        }
        #endregion
    }
}