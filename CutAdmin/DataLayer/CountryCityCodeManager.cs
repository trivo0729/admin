﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class CountryCityCodeManager
    {
        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCountry", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCity(string country, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Country", country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityByCountry", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCityCode(string Description, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Description", Description);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCityCode", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCountryCityMapping(string Countryname, string CityName, out DataSet dsResult)
        {
            CityName = CityName.Split(' ')[0];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Countryname", Countryname);
            sqlParams[1] = new SqlParameter("@CityName", CityName);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("usp_Proc_CountryCityMappingWithCode", out dsResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode AddMapping(string CityName, string CityCode, string CountryName, string CountryCode, string dotwCityCode, string ExpediaCityCode, string GrnCityCode)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[7];
            sqlParams[0] = new SqlParameter("@CityName", CityName);
            sqlParams[1] = new SqlParameter("@CityCode", CityCode);
            sqlParams[2] = new SqlParameter("@CountryName", CountryName);
            sqlParams[3] = new SqlParameter("@CountryCode", CountryCode);
            sqlParams[4] = new SqlParameter("@dotwCode", dotwCityCode);
            sqlParams[5] = new SqlParameter("@ExpediaCode", ExpediaCityCode);
            sqlParams[6] = new SqlParameter("@GrnCode", GrnCityCode);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_AddCityCountryMapp", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMappedCities(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetMappedCities", out dtResult);
            return retCode;
        }

    }
}