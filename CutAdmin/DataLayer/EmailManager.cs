﻿using CutAdmin.BL;
using CutAdmin.dbml;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin.DataLayer
{
    public class EmailManager
    {


        public static DBHelper.DBReturnCode SendInvoice(string sEmail, string ReservationId, string UID, string Night)
        {


            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            bool reponse = true;
            string Mail = InvoiceManager.GetInvoice(ReservationId, Uid, out invoice);
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sEmail, "");
                reponse = MailManager.SendMail(accessKey, Email1List, "Your Hotel Booking Invoice", Mail, from, attachmentList);

                return DBHelper.DBReturnCode.SUCCESS;

            }
            catch
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static DBHelper.DBReturnCode SendCommissionInvoice(string sEmail, string ReservationId, string UID, string Night)
        {


            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            using (var dbTax = new helperDataContext())
            {
                var dtInvoiceDetails = (from obj in dbTax.tbl_Invoices where obj.InvoiceNo == ReservationId && obj.InvoiceType == "Hotel Commission" select obj).FirstOrDefault();

                Int64 CycleId = Convert.ToInt64(0);
                string invoice = "";
                bool reponse = true;
                string Mail = InvoiceManager.GetCommissionInvoice(ReservationId, Uid, CycleId, out invoice);

                try
                {
                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sEmail, "");
                    reponse = MailManager.SendMail(accessKey, Email1List, "Your Hotel Booking Invoice", Mail, from, attachmentList);

                    return DBHelper.DBReturnCode.SUCCESS;
                }
                catch
                {
                    return DBHelper.DBReturnCode.EXCEPTION;
                }
            }
        }

        public static DBHelper.DBReturnCode SendVoucher(string sEmail, string ReservationId, string UID, string Latitude, string Longitude, string Night, string Supp, string Status)
        {
            DataSet ds = new DataSet();
            DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            Int64 Uid;
            if (objGlobalDefaults.UserType != "Admin")
            {
                Uid = objGlobalDefaults.sid;
            }
            else
                Uid = Convert.ToInt64(UID);

            string invoice = "";
            bool reponse = true;

            string Mail = VoucherManager.GenrateVoucher(ReservationId, Uid.ToString(), Status);
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                Email1List.Add(sEmail, "");
                reponse = MailManager.SendMail(accessKey, Email1List, "Your Hotel Booking Voucher", Mail, from, attachmentList);
                if (reponse)
                {
                    return DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    return DBHelper.DBReturnCode.EXCEPTION;
                }

            }
            catch
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static void GenrateAttachment(string sFormat, string Name, string Type)
        {

            string rootPath = System.Configuration.ConfigurationManager.AppSettings["rootPath"];
            HttpContext context = System.Web.HttpContext.Current;
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
            pdfConverter.PdfToolPath = rootPath + "Agent";
            var pdfBytes = pdfConverter.GeneratePdf(sFormat);
            MemoryStream ms = new MemoryStream(pdfBytes);
            //write to file
            string FilePath = HttpContext.Current.Server.MapPath("~/InvoicePdf/") + Name + "_" + Type + ".pdf";
            FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
            ms.WriteTo(file);
            file.Close();
            ms.Close();
        }

        public static string BookingMail4Agent(string ReservationID)
        {
            StringBuilder sb = new StringBuilder();
            //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
            try
            {
                using (var DB = new helperDataContext())
                {
                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();
                    var sReservation = (from obj in DB.tbl_HotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.Uid == objAgent.sid && obj.ReservationID == ReservationID
                                        select new
                                        {
                                            Currency = objAgent.CurrencyCode,
                                            customer_name = objAgent.ContactPerson,
                                            AgencyName = objAgent.AgencyName,
                                            obj.Source,
                                            obj.ReservationID,
                                            obj.CancelDate,
                                            obj.CancelFlag,
                                            obj.ExchangeValue,
                                            obj.ComparedFare,
                                            guest_name = obj.bookingname,
                                            destination = obj.City
                                        }).FirstOrDefault();
                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:Helvetica;");
                    sb.Append("	panose-1:2 11 6 4 2 2 2 2 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("p.yiv5197691934msonormal, li.yiv5197691934msonormal, div.yiv5197691934msonormal");
                    sb.Append("	{mso-style-name:yiv5197691934msonormal;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	font-size:12.0pt;");
                    sb.Append("	font-family:\"Times New Roman\",\"serif\";}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Dear ");
                    sb.Append(sReservation.customer_name + "(" + sReservation.AgencyName + "),</span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Greetings");
                    sb.Append("from&nbsp;</span><b><span style='font-size:12.0pt;font-family:\"Helvetica\",\"sans-serif\";");
                    //sb.Append("color:#26282A'>Click</span></b><b><span style='font-size:12.0pt;font-family:");
                    //sb.Append("\"Helvetica\",\"sans-serif\";color:#ED7D31'>Ur</span></b><b><span style='font-size:");
                    sb.Append("12.0pt;font-family:\"Helvetica\",\"sans-serif\";color:#4472C4'>" + arrSupplierDetails.CompanyName + "</span></b></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;Thank");
                    sb.Append("you very much for choosing us, kindly find attached invoice &amp; voucher for");
                    sb.Append("the hotel booking of " + sReservation.guest_name + " for " + sReservation.destination + ", you reservation no is ");
                    sb.Append(ReservationID + "</span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;</span><span");
                    sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
                    sb.Append("color:#26282A'>Hope the above is correct &amp;&nbsp;</span><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>your");
                    sb.Append("guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;In");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:acc.online@" + ConfigurationManager.AppSettings["DomainUser"] + "\" target=\"_blank\"><span");
                    sb.Append("style='color:#954F72'></span></a>&nbsp;&amp;&nbsp;<a");
                    sb.Append("href=\"mailto:" + arrSupplierDetails.Uid + "\" target=\"_blank\"><span style='color:#954F72'>" + arrSupplierDetails.Uid + "</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
                    sb.Append("color:#26282A'>&nbsp;Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
                    sb.Append("color:#26282A'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
                    sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");
                    return sb.ToString();
                }
            }
            catch
            {
                return "";
            }
        }

        public static string BookingMail2CUT(string ReservationID, Int64 Uid, out string BCcTeamMails, out string CcTeamMail)
        {
            StringBuilder sb = new StringBuilder();
            ////dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
            BCcTeamMails = ""; CcTeamMail = "";
            //try
            //{
            //    using (var DB = new CutAdmin.dbml.helperDataContext())
            //    {
            //        var arrSupplierDetails =  (from objAgent in DB.tbl_AdminLogins
            //                                   join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
            //                                   where objAgent.sid == AccountManager.GetSupplierByUser() select new {
            //                                       CompanyName = objAgent.AgencyName,
            //                                       Uid = objAgent.uid,
            //                                       website = objConct.Website
            //                                   }).FirstOrDefault();
            //        //var sReservation = (from obj in DB.tbl_
            //        //                    from objAgent in DB.tbl_AdminLogins
            //        //                    where obj.Uid == objAgent.sid && obj.ReservationID == ReservationID
            //        //                    select new
            //        //                    {
            //        //                        customer_name = objAgent.ContactPerson,
            //        //                        AgencyName = objAgent.AgencyName,
            //        //                        Currency = objAgent.CurrencyCode,
            //        //                        obj.Source,
            //        //                        obj.ReservationID,
            //        //                        obj.CancelDate,
            //        //                        obj.CancelFlag,
            //        //                        obj.ExchangeValue,
            //        //                        obj.ComparedFare,
            //        //                        guest_name = obj.bookingname,
            //        //                        destination = obj.City,
            //        //                        bookingStatus = obj.Status
            //        //                    }).FirstOrDefault();
            //        List<decimal> SupplierAmt = new List<decimal>();
            //        foreach (var objRoom in DB.tbl_CommonBookedRooms.Where(d => d.ReservationID == ReservationID))
            //        {
            //            SupplierAmt.Add(Convert.ToDecimal(objRoom.RoomAmount));
            //        }
            //        var sMail = new CutAdmin.dbml.tbl_ActivityMail();
            //        if (sReservation.bookingStatus == "Vouchered")
            //            sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking Confirm" && obj.ParentID == Uid select obj).FirstOrDefault();
            //        else
            //            sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking OnRequest" && obj.ParentID == Uid select obj).FirstOrDefault();

            //        if (sMail != null)
            //        {
            //            BCcTeamMails = sMail.Email;
            //            CcTeamMail = sMail.CcMail;
            //        }
            //        sb.Append("<html>");
            //        sb.Append("<head>");
            //        sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
            //        sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
            //        sb.Append("<style>");
            //        sb.Append("<!--");
            //        sb.Append(" /* Font Definitions */");
            //        sb.Append(" @font-face");
            //        sb.Append("	{font-family:Helvetica;");
            //        sb.Append("	panose-1:2 11 6 4 2 2 2 2 2 4;}");
            //        sb.Append("@font-face");
            //        sb.Append("	{font-family:\"Cambria Math\";");
            //        sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
            //        sb.Append("@font-face");
            //        sb.Append("	{font-family:Calibri;");
            //        sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
            //        sb.Append(" /* Style Definitions */");
            //        sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
            //        sb.Append("	{margin-top:0cm;");
            //        sb.Append("	margin-right:0cm;");
            //        sb.Append("	margin-bottom:10.0pt;");
            //        sb.Append("	margin-left:0cm;");
            //        sb.Append("	line-height:115%;");
            //        sb.Append("	font-size:11.0pt;");
            //        sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
            //        sb.Append("a:link, span.MsoHyperlink");
            //        sb.Append("	{color:blue;");
            //        sb.Append("	text-decoration:underline;}");
            //        sb.Append("a:visited, span.MsoHyperlinkFollowed");
            //        sb.Append("	{color:purple;");
            //        sb.Append("	text-decoration:underline;}");
            //        sb.Append("p.yiv5197691934msonormal, li.yiv5197691934msonormal, div.yiv5197691934msonormal");
            //        sb.Append("	{mso-style-name:yiv5197691934msonormal;");
            //        sb.Append("	margin-right:0cm;");
            //        sb.Append("	margin-left:0cm;");
            //        sb.Append("	font-size:12.0pt;");
            //        sb.Append("	font-family:\"Times New Roman\",\"serif\";}");
            //        sb.Append(".MsoPapDefault");
            //        sb.Append("	{margin-bottom:10.0pt;");
            //        sb.Append("	line-height:115%;}");
            //        sb.Append("@page Section1");
            //        sb.Append("	{size:595.3pt 841.9pt;");
            //        sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
            //        sb.Append("div.Section1");
            //        sb.Append("	{page:Section1;}");
            //        sb.Append("-->");
            //        sb.Append("</style>");
            //        sb.Append("</head>");
            //        sb.Append("<body lang=EN-IN link=blue vlink=purple>");
            //        sb.Append("<div class=Section1>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Dear ");
            //        sb.Append(sReservation.customer_name + "(" + sReservation.AgencyName + "),</span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Greetings");
            //        sb.Append("from&nbsp;</span><b><span style='font-size:12.0pt;font-family:\"Helvetica\",\"sans-serif\";");
            //        //sb.Append("color:#26282A'>Click</span></b><b><span style='font-size:12.0pt;font-family:");
            //        //sb.Append("\"Helvetica\",\"sans-serif\";color:#ED7D31'>Ur</span></b><b><span style='font-size:");
            //        sb.Append("12.0pt;font-family:\"Helvetica\",\"sans-serif\";color:#4472C4'>" + arrSupplierDetails.CompanyName + "</span></b></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;Thank");
            //        sb.Append("you very much for choosing us, kindly find attached invoice &amp; voucher for");
            //        sb.Append("the hotel booking of " + sReservation.guest_name + " for " + sReservation.destination + ", you reservation no is ");
            //        sb.Append(ReservationID + "</span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;</span><b><span");
            //        sb.Append("style='font-size:12.0pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Office");
            //        sb.Append("Information</span></b></p>");
            //        sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
            //        sb.Append(" style='background:white;border-collapse:collapse'>");
            //        sb.Append(" <tr>");
            //        sb.Append("  <td width=198 valign=top style='width:134.45pt;border:solid windowtext 1.0pt;");
            //        sb.Append("  padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Booked By</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append("  <td width=292 valign=top style='width:7.0cm;border:solid windowtext 1.0pt;");
            //        sb.Append("  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>" + sReservation.customer_name + "</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append(" </tr>");
            //        //sb.Append(" <tr>");
            //        //sb.Append("  <td width=198 valign=top style='width:134.45pt;border:solid windowtext 1.0pt;");
            //        //sb.Append("  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>");
            //        //sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        //sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Supplier</span></p>");
            //        //sb.Append("  </td>");
            //        //sb.Append("  <td width=292 valign=top style='width:7.0cm;border-top:none;border-left:none;");
            //        //sb.Append("  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;");
            //        //sb.Append("  padding:0cm 5.4pt 0cm 5.4pt'>");
            //        //sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        //sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>" + sReservation.Source + "</span></p>");
            //        //sb.Append("  </td>");
            //        //sb.Append(" </tr>");
            //        sb.Append(" <tr>");
            //        sb.Append("  <td width=198 valign=top style='width:134.45pt;border:solid windowtext 1.0pt;");
            //        sb.Append("  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Supplier ID</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append("  <td width=292 valign=top style='width:7.0cm;border-top:none;border-left:none;");
            //        sb.Append("  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;");
            //        sb.Append("  padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>" + sReservation.ReservationID + "</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append(" </tr>");
            //        sb.Append(" <tr>");
            //        sb.Append("  <td width=198 valign=top style='width:134.45pt;border:solid windowtext 1.0pt;");
            //        sb.Append("  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Cancellation Deadline</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append("  <td width=292 valign=top style='width:7.0cm;border-top:none;border-left:none;");
            //        sb.Append("  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;");
            //        sb.Append("  padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>" + sReservation.CancelDate + "</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append(" </tr>");
            //        sb.Append(" <tr>");
            //        sb.Append("  <td width=198 valign=top style='width:134.45pt;border:solid windowtext 1.0pt;");
            //        sb.Append("  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>Total Payable</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append("  <td width=292 valign=top style='width:7.0cm;border-top:none;border-left:none;");
            //        sb.Append("  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;");
            //        sb.Append("  padding:0cm 5.4pt 0cm 5.4pt'>");
            //        sb.Append("  <p class=MsoNormal style='line-height:normal'><span style='font-size:7.5pt;");
            //        sb.Append("  font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>" + sReservation.Currency + " " + SupplierAmt.ToList().Sum() + "</span></p>");
            //        sb.Append("  </td>");
            //        sb.Append(" </tr>");
            //        sb.Append("</table>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;</span><span");
            //        sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
            //        sb.Append("color:#26282A'>Hope the above is correct &amp;&nbsp;</span><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>your");
            //        sb.Append("guest will enjoy the stay</span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;In");
            //        sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
            //        sb.Append("href=\"mailto:acc.online@" + arrSupplierDetails.Uid+ "\" target=\"_blank\"><span");
            //        sb.Append("style='color:#954F72'>" + arrSupplierDetails.website + "</span></a>&nbsp;&amp;&nbsp;<a");
            //        sb.Append("href=\"mailto:" + arrSupplierDetails.Uid + "\" target=\"_blank\"><span style='color:#954F72'>" + arrSupplierDetails.Uid + "</span></a></span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
            //        sb.Append("color:#26282A'>&nbsp;Best Regards,</span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("lang=EN-US style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";");
            //        sb.Append("color:#26282A'>Online Reservation Team</span></p>");
            //        sb.Append("<p class=MsoNormal style='line-height:normal;background:white'><span");
            //        sb.Append("style='font-size:7.5pt;font-family:\"Helvetica\",\"sans-serif\";color:#26282A'>&nbsp;</span></p>");
            //        sb.Append("<p class=MsoNormal>&nbsp;</p>");
            //        sb.Append("</div>");
            //        sb.Append("</body>");
            //        sb.Append("</html>");
            //        return sb.ToString();
            //    }
            //}
            //catch
            //{
            //    return "";
            //}
            return sb.ToString();
        }

        public static string BookingMail2Hotel(string ReservationID, string Uid, string Status)
        {
            //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    using (var db = new helperDataContext())
                    {
                        StringBuilder sb = new StringBuilder();
                        string Night = "";
                        string Supplier = "";
                        string Latitude = "";
                        string Longitude = "";
                        string HotelAddress = "";
                        string HotelCity = "";
                        string HotelCountry = "";
                        string HotelPhone = "";
                        string HotelPostal = "";
                        string City = "";
                        string AllPassengers = "";
                        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                        if (ReservationID != "")
                        {
                            Int64 ContactID;
                            Int64 UserID = 0;
                            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                            UserID = AccountManager.GetSupplierByUser();
                            ContactID = db.tbl_AdminLogins.Where(d => d.sid == UserID).FirstOrDefault().ContactID;

                            string Logo = HttpContext.Current.Session["logo"].ToString();
                            var dtHotelReservation = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                            var dtBookedPassenger = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                            var dtBookedRoom = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                            var dtHotelAdd = (from obj in DB.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(dtHotelReservation.HotelCode) select obj).FirstOrDefault();
                            var dtHotelContact = (from obj in DB.Comm_HotelContacts where obj.HotelCode == Convert.ToInt64(dtHotelReservation.HotelCode) && obj.SupplierCode == AccountManager.GetSupplierByUser() select obj).FirstOrDefault();

                            var UserDetail = (from obj in db.tbl_AdminLogins
                                              join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                              join objh in db.tbl_HCities on objc.Code equals objh.Code
                                              where obj.sid == Convert.ToInt64(Uid)
                                              select new
                                              {
                                                  obj.AgencyLogo,
                                                  obj.AgencyName,
                                                  obj.AgencyType,
                                                  obj.Agentuniquecode,
                                                  obj.ContactID,
                                                  objc.Address,
                                                  objc.email,
                                                  objc.Mobile,
                                                  objc.phone,
                                                  objc.Fax,
                                                  objc.PinCode,
                                                  objc.sCountry,
                                                  objc.Website,
                                                  objh.Countryname,
                                                  objh.Description
                                              }).FirstOrDefault();

                            Night = Convert.ToString(dtHotelReservation.NoOfDays);
                            Supplier = dtHotelReservation.Source;
                            if (dtHotelReservation.LatitudeMGH != "")
                                Latitude = dtHotelReservation.LatitudeMGH;
                            Longitude = dtHotelReservation.LongitudeMGH;
                            HotelAddress = dtHotelAdd.HotelAddress;
                            City = dtHotelAdd.CityId;
                            HotelCountry = dtHotelAdd.CountryId;
                            string Hoteldestination = dtHotelReservation.City;
                            string InvoiceID = dtHotelReservation.InvoiceID;
                            string VoucherID = dtHotelReservation.VoucherID;
                            string CheckIn = dtHotelReservation.CheckIn;


                            CheckIn = CheckIn.Replace("00:00", "");
                            if (CheckIn.Contains('/'))
                            {
                                string[] CheckInDate = CheckIn.Split('/');
                                CheckIn = CheckInDate[2] + '-' + CheckInDate[1] + '-' + CheckInDate[0];
                            }
                            string CheckOut = dtHotelReservation.CheckOut;
                            CheckOut = CheckOut.Replace("00:00", "");
                            if (CheckIn.Contains('/'))
                            {
                                string[] CheckOutDate = CheckOut.Split('/');
                                CheckOut = CheckOutDate[2] + '-' + CheckOutDate[1] + '-' + CheckOutDate[0];
                            }


                            string ReservationDate = dtHotelReservation.ReservationDate;
                            ReservationDate = ReservationDate.Replace("00:00", "");
                            if (CheckIn.Contains('/'))
                            {
                                string[] ReservationBookingDate = ReservationDate.Split('/');
                                ReservationDate = ReservationBookingDate[2] + '-' + ReservationBookingDate[1] + '-' + ReservationBookingDate[0];

                            }



                            string HotelName = (dtHotelReservation.HotelName);
                            string AgentRef = (dtHotelReservation.AgentRef);
                            string AgencyName = UserDetail.AgencyName;
                            string Address = UserDetail.Address;
                            string Description = UserDetail.Description;
                            string Countryname = UserDetail.Countryname;
                            string phone = UserDetail.phone;
                            string email = UserDetail.email;
                            string agentcode = UserDetail.Agentuniquecode;
                            string CanAmtWoutNight = "";
                            string CanAmtWithTax = "";

                            //var SendDate = DateTime.Now;
                            string SendDate = DateTime.Now.ToString("dd-MM-yy");

                            string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                            sb.Append("<div style=\"background-color: #00AEEF; height: 13px\"> &nbsp;</span>");
                            sb.Append("</div>");
                            sb.Append("<div>");
                            sb.Append("<table style=\"height: 100px; width: 100%;\">");
                            sb.Append("<tbody>");
                            sb.Append("<tr style=\" color: #57585A;\">");

                            sb.Append("<td colspan=\"8\" style=\"width: 33%; padding-right:15px\" align=\"left\">");
                            sb.Append("<table style=\"height: 50px; width: 100%;\">");
                            sb.Append("<tbody>");

                            sb.Append("<tr style=\" color: #57585A;\">");
                            sb.Append("<td>");
                            sb.Append("<span style=\"margin-left: 15px\">Date</span>");
                            sb.Append("</td>");

                            sb.Append("<td>");
                            sb.Append(": <span style=\"margin-left: 15px\">" + SendDate + "</span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style=\" color: #57585A;\">");
                            sb.Append("<td>");
                            sb.Append("<span style=\"margin-left: 15px\">To ReservationDepartment</span>");
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(": <span style=\"margin-left: 15px\"><b>" + dtHotelAdd.HotelName + "</b></span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style=\" color: #57585A;\">");
                            sb.Append("<td>");
                            sb.Append("<span style=\"margin-left: 15px\">Reservation ID</span>");
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(": <span style=\"margin-left: 15px\">" + dtHotelReservation.ReservationID + "</span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");

                            sb.Append("</tbody>");
                            sb.Append("</table>");
                            sb.Append("</td>");

                            //sb.Append("<td style=\"width: 33%; padding-right:15px\" align=\"left\">");
                            //sb.Append("<span style=\"margin-right: 15px\">");
                            //sb.Append("<br>");
                            //sb.Append("<b>" + UserDetail.AgencyName + "</b><br>");
                            //sb.Append("" + Address + ",<br>");
                            ////sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                            //sb.Append("" + Description + "-" + UserDetail.PinCode + "," + Countryname + "<br>");
                            //sb.Append("Tel: " + UserDetail.Mobile + ", Fax:" + UserDetail.Fax + "<br>");
                            //sb.Append(" <B>Email: " + UserDetail.email + "<br>");
                            //sb.Append("</span>");
                            if (objGlobalDefault.UserType == "SupplierStaff" || objGlobalDefault.UserType == "Agent")
                            {
                                Logo = (from obj in db.tbl_AdminLogins where obj.sid == objGlobalDefault.ParentId select obj.Agentuniquecode).FirstOrDefault();
                            }



                            sb.Append("<td colspan=\"8\" id=\"AgentLogo\" style=\"width: 33%;padding-left:15px\">");
                            if (DefaultManager.UrlExists(Url + "AgencyLogo/" + Logo + ".jpg"))
                                sb.Append("<img  src=\"" + Url + "AgencyLogo/" + Logo + ".jpg\" height=\"auto\" width=\"auto\"></img>");
                            else
                                sb.Append("<h1 class=\"\" height=\"auto\" width=\"auto\">" + UserDetail.AgencyName + "</h1>"); ;

                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</tbody>");
                            sb.Append("</table>");
                            sb.Append("</div>");
                            sb.Append("<br>");

                            sb.Append("<div>");
                            sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                            sb.Append("<tr>");
                            sb.Append("<td rowspan=\"3\" >");
                            sb.Append("<b style=\"width:35%; color: #00CCFF; font-size: 30px; text-align: left; border: none; padding-right:35px; padding-left:10px\">Booking Request</b>");
                            if (Status == "Hold")
                                sb.Append("<b style=\"width:35%; color: red; font-size: 30px; text-align: left; border: none;\">: On Hold</b>");
                            sb.Append("</td>");
                            sb.Append("<td style=\"border-top: none; border-bottom-width: 2px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px;color: #57585A\">");
                            sb.Append("<b> Booking Date &nbsp;&nbsp; &nbsp; &nbsp;     :</b><span style=\"color: #757575\">" + ReservationDate + "</span>");
                            sb.Append("</td>");
                            sb.Append("<td style=\"border-top: none; border-bottom-width: 2px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px;color: #57585A\">");
                            sb.Append("<b>Voucher No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     : </b><span style=\"color: #757575\">" + VoucherID + "</span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("<tr>");
                            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
                            sb.Append("<b> Agent Ref No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + AgentRef + " </span>");
                            sb.Append("</td>");
                            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
                            sb.Append("<b>  Supplier Ref No &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\"color:#757575\">");
                            //sb.Append(ReservationID);
                            sb.Append("</span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("<tr>");
                            sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</div>");


                            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>BOOKING DETAILS</b></span>");
                            sb.Append("</div>");
                            sb.Append("<div>");
                            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; height: 250px; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                            sb.Append("<tr style=\"border: none\">");
                            sb.Append("<td rowspan=\"4\" style=\"width: auto; border: none\" id=\"googleMap\">");
                            sb.Append("<img  src='https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=400x250&sensor=false&maptype=roadmap&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c&markers=color:red|" + Latitude + "," + Longitude + "' style='width:auto; height:auto' >");
                            sb.Append("</td>");
                            sb.Append("<td style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:20%\">");
                            sb.Append("<span style=\"margin-left: 15px\"><b>Check In</span>");
                            sb.Append("</br>");
                            sb.Append("</td>");
                            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #00AEEF; font-size: 24px; color: white; border: none; width:47%\">");
                            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckIn + "</b></span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("<tr style=\"border: none\">");
                            sb.Append("<td style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
                            sb.Append("<span style=\"margin-left: 15px\"><b>Check Out</b></span>");
                            sb.Append("</td>");
                            sb.Append("<td colspan=\"2\" style=\"height: 45px; background-color: #27B4E8; font-size: 24px; color: white; border: none\">");
                            sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckOut + "</b></span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("<tr style=\"border: none\">");
                            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 24px; color: white; border: none\">");
                            sb.Append("<span style=\" margin-left: 15px\"><b>Hotel Name</b></span>");
                            sb.Append("</td>");
                            sb.Append("<td style=\"height: 45px; background-color: #35C2F1; font-size: 22px; color: white; border: none\">");
                            sb.Append(": <span style=\" margin-left: 15px\"><b>" + HotelName + "</b></span>");
                            sb.Append("</td>");
                            sb.Append("<td rowspan=\"2\" align=\"center\" style=\"background-color: #35C2F1; font-size: 22px; color: white; border: none; width:10%\">");
                            sb.Append("<br> <br><b>Nights</b>");
                            sb.Append("<br><br>");
                            sb.Append("<span style=\"font-size:40px; font-weight:700\">" + Night + "</span>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("<tr style=\"border: none\">");
                            sb.Append("<td style=\"border: none;color: #57585A\">");
                            sb.Append("<span style=\" margin-left: 15px\"><b>Address</b></span><br>");
                            sb.Append("<span style=\" margin-left: 15px\"><b>City</b></span><br>");
                            sb.Append("<span style=\" margin-left: 15px\"><b>Country</b></span><br>");
                            sb.Append("<span style=\" margin-left: 15px\"><b>Phone</b></span><br>");
                            sb.Append("<span style=\" margin-left: 15px\"><b>Postal Code</b></span>");
                            sb.Append("</td>");
                            sb.Append("<td style=\"border: none;color: #57585A\">");



                            sb.Append(": <span style=\" margin-left: 15px\">" + HotelAddress + "</span><br>");
                            sb.Append(": <span style=\" margin-left: 15px\">" + City + "</span><br>");
                            sb.Append(": <span style=\" margin-left: 15px\">" + HotelCountry + "</span><br>");
                            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPhone + "</span><br>");
                            sb.Append(": <span style=\" margin-left: 15px\">" + HotelPostal + "</span>");
                            sb.Append("</td>");
                            sb.Append("<!--<td colspan=\"2\">Sum: $180</td>-->");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</div>");


                            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>GUEST & ROOM DETAILS</b></span>");
                            sb.Append("</div>");
                            sb.Append("<div>");
                            sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                            for (var i = 0; i < dtBookedRoom.Count; i++)
                            {
                                AllPassengers = "";
                                for (int p = 0; p < dtBookedPassenger.Count; p++)
                                {
                                    string brrn = (dtBookedRoom[i].RoomNumber);
                                    string bprn = (dtBookedPassenger[p].RoomNumber);

                                    if (dtBookedPassenger[p].RoomNumber == dtBookedRoom[i].RoomNumber)
                                    {
                                        AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";

                                    }

                                }

                                AllPassengers = AllPassengers.TrimEnd(' ');
                                AllPassengers = AllPassengers.TrimEnd(',');
                                string Chid_Age = dtBookedRoom[i].ChildAge;
                                string chidage = "";
                                string[] splitChid_Age = Chid_Age.Split('|');
                                Int32 Length = splitChid_Age.Length;
                                for (int m = 0; m < splitChid_Age.Length - 1; m++)
                                {
                                    if (m != Length - 2)
                                    {
                                        chidage += splitChid_Age[m] + '|';
                                    }
                                    else
                                    {
                                        chidage += splitChid_Age[m];
                                    }
                                }

                                string Remark = dtBookedRoom[i].Remark;
                                if (Chid_Age == "")
                                {
                                    Chid_Age = "-";
                                }
                                if (Remark == "")
                                {
                                    Remark = "-";
                                }
                                sb.Append("<tr style=\"border-spacing: 0px;\">");
                                sb.Append("<td style=\"border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700\">" + "Room " + dtBookedRoom[i].RoomNumber + "</td>");
                                //sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                                sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #f1d135; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                                //  sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + dtBookedRoom.Rows[i]["LeadingGuest"].ToString() + "</span></td>");
                                sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + AllPassengers + "</span></td>");
                                sb.Append("</tr>");
                                sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Room Type</td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"> Board </td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\">Total Rooms</td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"> Adults </td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\">Child </td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\">Child Age</td>");
                                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px\">Remark</td>");
                                sb.Append("</tr>");
                                sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                                sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtBookedRoom[i].RoomType + "</td>");
                                if (dtBookedRoom[i].BoardText == "BB")
                                {
                                    dtBookedRoom[i].BoardText = "Bed & Breakfast";
                                }
                                if (dtBookedRoom[i].BoardText == "RO")
                                {
                                    dtBookedRoom[i].BoardText = "Room Only";
                                }
                                if (dtBookedRoom[i].BoardText == "HB")
                                {
                                    dtBookedRoom[i].BoardText = "Half Board";
                                }
                                if (dtBookedRoom[i].BoardText == "FB")
                                {
                                    dtBookedRoom[i].BoardText = "Full Board";
                                }
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].BoardText + "</td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].TotalRooms + "</td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].Adults + "</td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].Child + "</td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + chidage + "</td>");
                                sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\">" + Remark + "</td>");
                                sb.Append("</tr>");
                            }
                            if (Supplier == "GRN")
                            {
                                sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                                sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Email Id</td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"> Nationality </td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\">Booking Status</td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                                sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                                //sb.Append("</tr>");
                                sb.Append("</tr>");
                                sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                                //  sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtHotelAdd.Rows[0]["ContactEmail"].ToString() + "</td>");
                                // sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtHotelAdd.Rows[0]["PaxNationality"].ToString() + "</td>");

                                if (dtHotelReservation.Status == "Vouchered")
                                {
                                    sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">Confirmed</td>");
                                }
                                else
                                {
                                    sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtHotelReservation.Status + "</td>");
                                }



                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                                sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                                sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\"></td>");
                                sb.Append("</tr>");
                            }

                            sb.Append("</table>");
                            sb.Append("</div>");

                            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
                            sb.Append("</div>");
                            sb.Append("<hr style=\"border-top-width: 3px\">");
                            sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
                            sb.Append("<ul style=\"list-style-type: disc\">");
                            sb.Append("<li>Please collect all extras directly from clients prior to departure</li>");
                            sb.Append("<li>");
                            sb.Append("It is mandatory to present valid Passport at the time of check-in for International");
                            sb.Append("hotel booking & any valid photo ID for domestic hotel booking");
                            sb.Append("</li>");
                            sb.Append("<li>");
                            sb.Append("Hotel holds right to reject check in or charge supplement due to wrongly selection");
                            sb.Append("of Nationality or Residency at the time of booking");
                            sb.Append("</li>");
                            sb.Append("<li>");
                            sb.Append("Early check-in & late check-out will be subject to availability and approval by");
                            sb.Append("respective hotel only");
                            sb.Append("</li>");
                            sb.Append("<li>");
                            sb.Append("Kindly inform hotel in advance for any late check-in, as hotel may automatically");
                            sb.Append("cancel the room in not check-in by 18:00Hrs local time");
                            sb.Append("</li>");
                            sb.Append("<li>");
                            sb.Append(" You may need to deposit security amount at some destination as per the local rule");
                            sb.Append("and regulations at the time of check-in");
                            sb.Append("</li>");
                            sb.Append("<li>");
                            sb.Append(" Tourism fees may apply at the time of check-in at some destinations like Dubai,Paris, etc...");

                            sb.Append("</li>");
                            sb.Append("<li>General check-in time is 16:00Hrs & check-out time is 12:00Hrs</li>");
                            sb.Append("</ul>");
                            sb.Append("</div>");
                            sb.Append("<br>");
                            sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 20px; color: white; height: 23px\">");
                            sb.Append("<span>");
                            sb.Append("Check your booking details carefully and inform us immediately before it’s too late...");
                            sb.Append("</span>");
                            sb.Append("</div><br>");
                            //sb.Append("</body>");
                            //sb.Append("</html>");

                            bool reponse = true;
                            try
                            {
                                List<string> from = new List<string>();
                                from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                                List<string> attachmentList = new List<string>();
                                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                                if (dtHotelContact != null)
                                    Email1List.Add(Convert.ToString(dtHotelContact.email), "");
                                // Email1List.Add("desk@clickurhotel.com", "");
                                reponse = MailManager.SendMail(accessKey, Email1List, "Booking Request", sb.ToString(), from, attachmentList);
                            }
                            catch
                            {

                            }
                        }
                    }
                    
                    
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        public static string GetErrorMessage(string Type)
        {
            string MMessage = "";
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = 0;
                if (objGlobalDefault.UserType == "AdminStaff")
                {
                    Uid = objGlobalDefault.ParentId;
                }
                else
                {
                    if (objGlobalDefault.UserType != "Supplier")
                        Uid = objGlobalDefault.ParentId;
                    else
                        Uid = objGlobalDefault.sid;
                }
                //dbHotelhelperDataContext db = new dbHotelhelperDataContext();

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                using (var db = new helperDataContext())
                {
                    var Message = (from obj in db.tbl_ActivityMails where obj.Activity == Type && obj.ParentID == Uid select obj.ErroMessage).FirstOrDefault();

                    MMessage = Convert.ToString(Message);
                }
                return MMessage;
            }
            catch
            {
                return MMessage;
            }

            // jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, ErrorMessage = ErrorMessage });
        }

        public static bool CancelHoldBooking(string ReservationID, Int64 ParentID, string Supplier, string AffiliateCode)
        {
            //AdmindbHotelhelperDataContext DB = new AdmindbHotelhelperDataContext();
            //dbHotelhelperDataContext db = new dbHotelhelperDataContext();

            // var Data = new List<tbl_HotelReservation>();
            //var AdminData=new List<CutAdmin.HotelAdmin.tbl_AdminLogin>();
            using (var DB = new helperDataContext())
            {
                using (var db = new  CutAdmin.dbml.helperDataContext())
                {
                    var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                              join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                              where objAgent.sid == AccountManager.GetSupplierByUser()
                                              select new
                                              {
                                                  CompanyName = objAgent.AgencyName,
                                                  Uid = objAgent.uid,
                                                  website = objConct.Website
                                              }).FirstOrDefault();
                    var AdminData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();
                    var Data = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();

                    //string ContactPerson = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj.ContactPerson).ToString();
                    //string CurrencyCode = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj.CurrencyCode).ToString();
                    //string InvoiceID = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.InvoiceID).ToString();
                    //string Status = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.Status).ToString();
                    //string ReservationDate = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.ReservationDate).ToString();
                    //string HotelName = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj.HotelName).ToString();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("'<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>   ");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>    ");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto\">  ");
                    sb.Append("<div style=\"margin-left:9%;height:50px;width:auto\">");
                    sb.Append("<br />");
                    sb.Append(" <img src=\"https://www.clickurtrip.co.in/images/logosmal.png\"  style=\"padding-left:10px;\" />");
                    sb.Append("</div>");
                    sb.Append("<div style=\"margin-left:10%\">");
                    sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Dear " + AdminData.ContactPerson + ",</span><br /></p>");
                    sb.Append(" <p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p>");
                    sb.Append("<style type=\"text/css\">");
                    sb.Append(".tg  {border-collapse:collapse;border-spacing:0;} ");
                    sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
                    sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;} ");
                    sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                    sb.Append(".tg .tg-yw4l{vertical-align:top}");
                    sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>");
                    sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                    sb.Append(" <tr>");
                    sb.Append("    <td class=\"tg-9hbo\"><b>InvoiceID</b></td>");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.InvoiceID + "</td> ");
                    sb.Append(" </tr> ");
                    sb.Append("<tr> ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Status</b> </td> ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.Status + "</td>");
                    sb.Append(" </tr>   ");
                    sb.Append("<tr>  ");
                    sb.Append("   <td class=\"tg-9hbo\"><b>Reservation-ID</b></td>");
                    sb.Append("  <td class=\"tg-yw4l\">: " + ReservationID + "</td> ");
                    sb.Append("</tr>  ");
                    sb.Append(" <tr>   ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>BookingDate</b>     </td> ");
                    sb.Append(" <td class=\"tg-yw4l\">:  " + Data.ReservationDate + "</td>");
                    sb.Append("</tr>   ");
                    sb.Append(" <tr>    ");
                    sb.Append(" <td class=\"tg-9hbo\"><b>Hotel Name</b>        </td>  ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + Data.HotelName + "</td>    ");
                    sb.Append(" </tr>    ");
                    sb.Append(" <tr>    ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Check-In</b>            </td>   ");
                    sb.Append("   <td class=\"tg-yw4l\">:  " + Data.CheckIn + "</td>   ");
                    sb.Append("</tr>    ");
                    sb.Append("<tr>    ");
                    sb.Append(" <td class=\"tg-9hbo\"><b>Check-Out </b>        </td>    ");
                    sb.Append(" <td class=\"tg-yw4l\">:  " + Data.CheckOut + "</td>");
                    sb.Append(" </tr>    ");
                    sb.Append(" <tr>    ");
                    sb.Append("  <td class=\"tg-9hbo\"><b>Total  </b>                </td>  ");
                    sb.Append("  <td class=\"tg-yw4l\">:  " + AdminData.CurrencyCode + " " + Data.TotalFare + "</td>  ");
                    sb.Append("</tr>    ");
                    sb.Append("</table></div>    ");
                    sb.Append(" <p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>  ");
                    sb.Append("  <span style=\"margin-left:2%\"> ");
                    sb.Append(" <b>Thank You,</b><br />    ");
                    sb.Append(" </span>    ");
                    sb.Append(" <span style=\"margin-left:2%\">    ");
                    sb.Append(" Administrator    ");
                    sb.Append(" </span>");
                    sb.Append(" </div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("   <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("       <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + arrSupplierDetails.website + "</div></td>");
                    sb.Append("   </tr>");
                    sb.Append("  </table>");
                    sb.Append("  </div> ");
                    sb.Append("  </body>");
                    sb.Append(" </html>' ");

                    try
                    {


                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                        List<string> DocLinksList = new List<string>();
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        string CcTeamMail = "";
                        var sMail = new CutAdmin.dbml.tbl_ActivityMail();
                        sMail = (from obj in db.tbl_ActivityMails where obj.Activity.Contains("Hold Cancel Confirmation") && obj.ParentID == 232 select obj).FirstOrDefault();

                        if (sMail != null)
                        {
                            //BCcTeamMails = sMail.Email;
                            CcTeamMail = sMail.CcMail;
                        }
                        foreach (string mail in AdminData.uid.Split(','))
                        {
                            if (mail != "")
                            {
                                Email1List.Add(mail, mail);
                            }
                        }
                        Dictionary<string, string> BccList = new Dictionary<string, string>();
                        foreach (string mail in CcTeamMail.Split(';'))
                        {
                            if (mail != "")
                            {
                                BccList.Add(mail, mail);
                            }
                        }
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                        bool reponse = MailManager.SendMail(accessKey, Email1List, BccList, "Hold Cancel Confirmation", sb.ToString(), from, DocLinksList);

                        return reponse;


                    }
                    catch
                    {
                        return false;
                    }
                }
            }
        }

        public static string GenrateCancellationMail(string ReservationID, Int64 UID, string User)
        {
            //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
            //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
            using (var DB = new dbHotelhelperDataContext())
            {
                using (var db = new helperDataContext())
                {
                    StringBuilder sMail = new StringBuilder();
                    string Url = ConfigurationManager.AppSettings["URL"];
                    string Logo = HttpContext.Current.Session["logo"].ToString();
                    var arrReservation = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                    if (arrReservation != null)
                    {
                        var arrUserDetails = (from obj in db.tbl_AdminLogins where obj.sid == UID select obj).FirstOrDefault();
                        var arrBookingTrasaction = (from obj in db.tbl_BookingTransactions where obj.ReservationID == ReservationID && obj.uid == UID select obj).FirstOrDefault();
                        sMail.Append("<!DOCTYPE html>  <html lang=\"en\"  xmlns=\"http://www.w3.org/1999/xhtml\"> <head>  ");
                        sMail.Append("<meta charset=\"utf-8\" /> </head>");
                        sMail.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
                        sMail.Append("<div style=\"height:50px;width:auto\"><br /> ");

                        sMail.Append("<img src=\"" + Url + "AgencyLogo/" + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />  ");
                        sMail.Append("</div>   ");
                        sMail.Append("<div style=\"margin-left:10%\">  ");
                        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hi " + arrUserDetails.AgencyName + ",</span><br /></p> ");
                        sMail.Append("<p> <span style=\"margin-left:2%;font-weight:400\">We have received your request, your Booking Details.</span><br /></p> ");
                        sMail.Append("<style type=\"text/css\">  ");
                        sMail.Append(".tg  {border-collapse:collapse;border-spacing:0;}   ");
                        sMail.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                        sMail.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}   ");
                        sMail.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}  ");
                        sMail.Append(".tg .tg-yw4l{vertical-align:top}   ");
                        sMail.Append(" @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>   ");
                        sMail.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                        sMail.Append(" <td class=\"tg-yw4l\">:  " + ReservationID + "</td></tr>");
                        sMail.Append(" <tr><td class=\"tg-9hbo\"><b>Reservation Status</b></td>    ");
                        sMail.Append("<td class=\"tg-yw4l\">:" + arrReservation.BookingStatus + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Reservation Date</b></td>       ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.ReservationDate + "</td></tr>");
                        sMail.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>                 ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.HotelName + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Location</b></td>               ");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrReservation.City + "</td></tr>                 ");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-In</b></td>               ");
                        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckIn + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Check-Out </b> </td>            ");
                        sMail.Append("<td class=\"tg-yw4l\">:   " + arrReservation.CheckOut + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Rate</b></td> ");
                        sMail.Append("<td class=\"tg-yw4l\">:" + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt / arrReservation.NoOfDays) + "</td></tr>");
                        sMail.Append("<tr><td class=\"tg-9hbo\"><b>Total  </b></td>");
                        sMail.Append("<td class=\"tg-yw4l\">:  " + arrUserDetails.CurrencyCode + " " + (arrBookingTrasaction.BookingAmt) + "</td></tr>");
                        sMail.Append("</table></div>");
                        sMail.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
                        sMail.Append("<span style=\"margin-left:2%\">");
                        sMail.Append("<b>Thank You,</b><br />                                                                                                                        ");
                        sMail.Append("</span>");
                        sMail.Append("<span style=\"margin-left:2%\">");
                        sMail.Append("Administrator");
                        sMail.Append("</span>");
                        sMail.Append("</div>");
                        sMail.Append("<div>");
                        sMail.Append(" <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                        sMail.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                        //if (User == "FR")
                        //     sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  ClickurTrip.com</div></td>");
                        //else
                        sMail.Append("    <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> " + ConfigurationManager.AppSettings["DomainUser"] + "</div></td>");
                        sMail.Append("</tr> </table></div>");
                        sMail.Append("</body></html>");


                        CutAdmin.dbml.tbl_ActivityMail arrEmails = new CutAdmin.dbml.tbl_ActivityMail();
                        //if (User == "SupplierStaff")
                        //    arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == arrUserDetails.ParentID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();
                        //else
                        //    arrEmails = (from obj in db.tbl_ActivityMails where obj.ParentID == UID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();

                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                        List<string> from = new List<string>();
                        List<string> DocPathList = new List<string>();
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Dictionary<string, string> BccList = new Dictionary<string, string>();
                        if (arrEmails.Email != "")
                            from.Add(Convert.ToString(arrEmails.Email));
                        else
                            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                        foreach (var Mails in arrEmails.CcMail.Split(','))
                        {
                            if (Mails != "")
                            {
                                BccList.Add(Mails, Mails); ;
                            }
                        }
                        if (User != "AG" && BccList.Count == 0)
                        {
                            var arrAdmin = (from obj in db.tbl_AdminLogins where obj.sid == arrUserDetails.ParentID select obj).First();
                            BccList.Add(arrAdmin.AgencyName, arrAdmin.uid);
                        }

                        Email1List.Add(arrUserDetails.AgencyName, arrUserDetails.uid);
                        string title = "Cancel Confirmation : " + ReservationID;
                        MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
                    }
                    return sMail.ToString();
                }
            }
        }

        // Add Update Agent Email //
        public static bool VerificationEmail(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage, string sAgencyName)
        {
            //Work start
            string Url = ConfigurationManager.AppSettings["URL"].TrimEnd('/') + "?Email=" + sEmail;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div id=\"yui_3_16_0_ym19_1_1489469507252_52741\"><div class=\"yiv6464921239WordSection1\" id=\"yui_3_16_0_ym19_1_1489469507252_52740\">");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52778\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52777\">Hello " + sName + " " + ",</span>");
            sb.Append("</p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52775\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52828\">Welcome to ");
            sb.Append("<b id=\"yui_3_16_0_ym19_1_1489469507252_52827\"> " + ConfigurationManager.AppSettings["AdminName"] + "</b></span></p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\" style=\"margin-right:30.0pt;\" id=\"yui_3_16_0_ym19_1_1489469507252_52854\"><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\" id=\"yui_3_16_0_ym19_1_1489469507252_52853\">");
            sb.Append("We have received your registration request as a  " + " " + sAgencyName + " </span></p><p class=\"yiv6464921239MsoNormal\" id=\"yui_3_16_0_ym19_1_1489469507252_52823\">");
            sb.Append("<span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">");
            sb.Append("To complete registration, kindly <b><u><a href=\"" + Url + "\" target=\"_blank\">click here</a></u></b>, you will receive your temporary password on next mail</span>");
            sb.Append("</p>");
            sb.Append("");
            sb.Append("<p class=\"yiv6464921239MsoNormal\"><b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">Thanking You,</span>");
            sb.Append("</b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\"><br>Administrator</span></p>");
            //sb.Append("<p class=\"yiv6464921239MsoNormal\"><b><span lang=\"EN\" style=\"font-family:&quot;Segoe UI&quot;, sans-serif;\">ClickUrTrip.com Pvt. Ltd.</span></b></p>");
            sb.Append("<div id=\"yui_3_16_0_ym19_1_1489469507252_52802\">");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Password Detail", sb.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmailAdmin(string sAgencyName, string AgentUniqueCode, string nMobile, string sEmail)
        {
            //Work start
            string Url = ConfigurationManager.AppSettings["URL"];
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"" + Url + "/images/logo.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello Admin,</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">A New Agent is Registered With following Details. Activate Agent</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agency Name : <b>" + sAgencyName + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agent Unique Code : <b>" + AgentUniqueCode + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Email-ID : <b>" + sEmail + "</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Mobile Number : <b>" + nMobile + "</b></span><br />");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                Email1List.Add(ConfigurationManager.AppSettings["supportMail"], ConfigurationManager.AppSettings["AdminName"]);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Password Detail", sb.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmail(string sTo, string sSubject, string sMessageTitle, string sName, string sEmail, string sPhone, string sMessage)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + sName + ",</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Username is : <b>" + sEmail + "</b> and Password is : <b>" + sMessage + "</b></span><br />");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.clickurtrip.com/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            //sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            sb.ToString();


            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Password Detail", sb.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

            //try
            //{

            //    int rowsAffected = 0;
            //    SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
            //    sqlParamsGrpName[0] = new SqlParameter("@sTo", sTo);
            //    sqlParamsGrpName[1] = new SqlParameter("@sSubject", "Your Password Detail");
            //    sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
            //    DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_RegistrationMail", out rowsAffected, sqlParamsGrpName);
            //    //string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
            //    //sMailMessage = sMailMessage.Replace("#name#", sName);
            //    //sMailMessage = sMailMessage.Replace("#CompanyName#", sCompanyName);
            //    //sMailMessage = sMailMessage.Replace("#CompanyAddress#", sCompanyAddress);
            //    //sMailMessage = sMailMessage.Replace("#City#", sCity);
            //    //sMailMessage = sMailMessage.Replace("#State#", sState);
            //    //sMailMessage = sMailMessage.Replace("#Desgn#", sDesgn);
            //    //sMailMessage = sMailMessage.Replace("#email#", sEmail);
            //    //sMailMessage = sMailMessage.Replace("#phone#", sPhone);
            //    //sMailMessage = sMailMessage.Replace("#selectoption#", sSelectProducts);
            //    //sMailMessage = sMailMessage.Replace("#text#", sMessage);
            //    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
            //    //mailMsg.From = new MailAddress("support@royalsheets.asia");
            //    //mailMsg.To.Add(sTo);
            //    //mailMsg.Subject = sSubject;
            //    //mailMsg.IsBodyHtml = true;
            //    //mailMsg.Body = sb.ToString(); ;
            //    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
            //    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
            //    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //    //mailObj.EnableSsl = true;
            //    //mailObj.Send(mailMsg);
            //    //mailMsg.Dispose();
            //    return true;

            //}
            //catch
            //{
            //    return false;
            //}
        }


        // Active Deactive Agent Email //
        public static bool ActivateNotificationMail(string sEmail, string AgentCode, string AgencyName)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"http://www.© 2018 - 2019 + "  " + ConfigurationManager.AppSettings["AdminName"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<br />");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Welcome, " + AgencyName + "</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Account is now activated. Your Code is " + AgentCode + "</b></span><br />");

            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.© 2018 - 2019 + "  " + ConfigurationManager.AppSettings["AdminName"] + "/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">© 2018 - 2019  " + ConfigurationManager.AppSettings["AdminName"] + "</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");

            try
            {
                using (var db = new dbHotelhelperDataContext())
                {
                    //string sTo = sEmail;
                    //string sSubject = "Acount is Activated";
                    //int rowsAffected = 0;
                    //SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                    //sqlParamsGrpName[0] = new SqlParameter("@sTo", sEmail);
                    //sqlParamsGrpName[1] = new SqlParameter("@sSubject", sSubject);
                    //sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                    //DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_Mail", out rowsAffected, sqlParamsGrpName);

                    //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                    List<string> DocLinksList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();


                    foreach (string mail in sEmail.Split(','))
                    {
                        if (mail != "")
                        {
                            Email1List.Add(mail, mail);
                        }
                    }

                    string CcTeamMail = "";
                    var sMail = new  CutAdmin.dbml.tbl_ActivityMail();
                    //sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Account Activated" && obj.ParentID == 232 select obj).FirstOrDefault();

                    if (sMail != null)
                    {
                        //BCcTeamMails = sMail.Email;
                        CcTeamMail = sMail.CcMail;
                    }

                    Dictionary<string, string> BccList = new Dictionary<string, string>();
                    if (CcTeamMail != null)
                    {
                        foreach (string mail in CcTeamMail.Split(';'))
                        {
                            if (mail != "")
                            {
                                BccList.Add(mail, mail);
                            }
                        }
                    }
                    //Email1List.Add(ConfigurationManager.AppSettings["supportMail"], ConfigurationManager.AppSettings["AdminName"]);
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    bool reponse = MailManager.SendMail(accessKey, Email1List, "Account Activated", sb.ToString(), from, DocLinksList);
                    return reponse;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool DeActivateNotificationMail(string sEmail, string AgentCode, string AgencyName)
        {
            //Work start
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"http://www.© 2018 - 2019 + "  " + ConfigurationManager.AppSettings["AdminName"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<br />");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello, " + AgencyName + "</span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Your Account is now Deactivated. Contact Admistrator.</b></span><br />");

            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:2%\">");
            sb.Append("Administrator");
            sb.Append("</span>");
            sb.Append("</div>");
            //sb.Append("<img src=\"http://www.© 2018 - 2019 + "  " + ConfigurationManager.AppSettings["AdminName"] + "/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">© 2018 - 2019  " + ConfigurationManager.AppSettings["AdminName"] + "</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");



            try
            {
                //string sTo = sEmail;
                //string sSubject = "Acount is Deactivated";
                //int rowsAffected = 0;
                //SqlParameter[] sqlParamsGrpName = new SqlParameter[3];
                //sqlParamsGrpName[0] = new SqlParameter("@sTo", sEmail);
                //sqlParamsGrpName[1] = new SqlParameter("@sSubject", sSubject);
                //sqlParamsGrpName[2] = new SqlParameter("@VarBody", sb.ToString());
                //DBHelper.DBReturnCode RetEmail = DBHelper.ExecuteNonQuery("Proc_Mail", out rowsAffected, sqlParamsGrpName);


                //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
                using (var db = new dbHotelhelperDataContext())
                {
                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                    List<string> DocLinksList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();

                    foreach (string mail in sEmail.Split(','))
                    {
                        if (mail != "")
                        {
                            Email1List.Add(mail, mail);
                        }
                    }

                    string CcTeamMail = "";
                    var sMail = new CutAdmin.dbml.tbl_ActivityMail();
                    //sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Account Deactivated" && obj.ParentID == 232 select obj).FirstOrDefault();

                    if (sMail != null)
                    {
                        //BCcTeamMails = sMail.Email;
                        CcTeamMail = sMail.CcMail;
                    }

                    Dictionary<string, string> BccList = new Dictionary<string, string>();
                    if (CcTeamMail != null)
                    {
                        foreach (string mail in CcTeamMail.Split(';'))
                        {
                            if (mail != "")
                            {
                                BccList.Add(mail, mail);
                            }
                        }
                    }

                    //Email1List.Add(ConfigurationManager.AppSettings["supportMail"], ConfigurationManager.AppSettings["AdminName"]);
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    bool reponse = MailManager.SendMail(accessKey, Email1List, BccList, "Account Deactivated", sb.ToString(), from, DocLinksList);
                    return reponse;
                }
            }
            catch
            {
                return false;
            }
        }


        // Bank Transaction Mails //
        public static bool AccountCredited(string sTo, decimal dlAmountDeposit, string Name)
        {
            var Message = "Your account is credited against your successful payment.<br> Amount of Rs." + dlAmountDeposit + " added in your account.";



            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" style=\"padding-left:10px\" />");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<div style=\"margin-left:10%\">");
            sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hello " + Name + ",</span><br /></p>");
            sb.Append(" <p><span style=\"margin-left:2%;font-weight:400\">" + Message + "</span><br /></p>");
            sb.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
            sb.Append(" <span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append(" <span style=\"margin-left:2%\">");
            sb.Append(" Administrator");
            sb.Append(" </span>");

            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append(" <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append(" <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"></div></td>");
            sb.Append("</tr>");
            sb.Append(" </table>");
            sb.Append("</div>");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>'");

            try
            {


                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, "Account credited", sb.ToString(), from, DocLinksList);
                return reponse;


            }
            catch
            {
                return false;
            }

        }

        public static bool AccountDebited(string sTo, decimal ReduceAmmount, decimal DepositAmmount, string Name, string AgainInvoice)
        {
            var Message = "";
            var Subject = "";
            if (DepositAmmount > 0)
            {
                Message = "Your account is credited by Admin.<br> Amount of Rs." + DepositAmmount + " added in your account.";
                Subject = "Account credited";
            }
            if (ReduceAmmount > 0)
            {
                Message = "Your account is debited against " + AgainInvoice + ".<br> Amount of Rs." + ReduceAmmount + " debited from your account.";
                Subject = "Account Debited";
            }


            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" style=\"padding-left:10px\" />");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<div style=\"margin-left:10%\">");
            sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hello " + Name + ",</span><br /></p>");
            sb.Append(" <p><span style=\"margin-left:2%;font-weight:400\">" + Message + "</span><br /></p>");
            sb.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");
            sb.Append(" <span style=\"margin-left:2%\">");
            sb.Append("<b>Thank You,</b><br />");
            sb.Append("</span>");
            sb.Append(" <span style=\"margin-left:2%\">");
            sb.Append(" Administrator");
            sb.Append(" </span>");

            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append(" <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append(" <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"></div></td>");
            sb.Append("</tr>");
            sb.Append(" </table>");
            sb.Append("</div>");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>'");

            try
            {


                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, Subject, sb.ToString(), from, DocLinksList);
                return reponse;


            }
            catch
            {
                return false;
            }

        }

        public static bool CreditLimit(string sTo, decimal CreditAmmount, string Name, string AgainInvoice, string AgentUniqueCode, string CurrencyCode, decimal MaxCreditAmmount)
        {
            var Message = "";
            var Subject = "";
            if (CreditAmmount > 0)
            {
                Message = "Your Agency ID " + AgentUniqueCode + " have been granted with " + CurrencyCode + " " + CreditAmmount + " credit limit for your further transactions, same should be showing on top right corner of your login, if not please contact our accounts team immediately at acc.online@trivo.in";
                Subject = "Fix Credit Limit Approved";
            }
            if (MaxCreditAmmount > 0)
            {

                Message = "Your Agency ID " + AgentUniqueCode + " have been granted with " + CurrencyCode + " " + MaxCreditAmmount + " temporary credit limit for your further transactions, same should be showing on top right corner of your login, if not please contact our accounts team immediately at acc.online@trivo.in";
                Subject = "Temporary Credit Limit Approved";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" style=\"padding-left:10px\" />");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<div style=\"margin-left:10%\">");
            sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hello " + Name + ",</span><br /></p>");
            sb.Append(" <p><span style=\"margin-left:2%;font-weight:400\">" + Message + "</span><br /></p>");
            sb.Append("<p><span style=\"margin-left:2%;font-weight:400\">Happy selling...</span><br /></p>");
            sb.Append(" <span style=\"margin-left:2%;font-weight:400\">Regards</span><br /> ");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Accounts & Finance Team</span><br /> ");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"></span><br /> ");

            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append(" <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append(" <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"></div></td>");
            sb.Append("</tr>");
            sb.Append(" </table>");
            sb.Append("</div>");
            sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>'");

            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, Subject, sb.ToString(), from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            //int effected = 0;
            //SqlParameter[] sqlParams = new SqlParameter[5];
            //sqlParams[0] = new SqlParameter("@sTo", sTo);
            //sqlParams[1] = new SqlParameter("@sSubject", title);
            //sqlParams[2] = new SqlParameter("@VarBody", sMail);
            //sqlParams[3] = new SqlParameter("@Path", DocPath);
            //sqlParams[4] = new SqlParameter("@Cc", "online@clickurtrip.com");
            //DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);

            List<string> from = new List<string>();
            List<string> DocPathList = new List<string>();
            Dictionary<string, string> Email1List = new Dictionary<string, string>();
            Dictionary<string, string> BccList = new Dictionary<string, string>();
            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));

            foreach (string mail in sTo.Split(','))
            {
                if (mail != "")
                {
                    Email1List.Add(mail, mail);
                }
            }
            foreach (string mail in Cc.Split(';'))
            {
                if (mail != "")
                {
                    BccList.Add(mail, mail);
                }
            }
            foreach (string link in DocPath.Split(';'))
            {
                if (link != "")
                {
                    DocPathList.Add(link);
                }
            }

            //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

            MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
        }
    }
}