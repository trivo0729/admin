﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.Common;
namespace CutAdmin.DataLayer
{
    public class GenralManager
    {
        public static CommonHotelDetails GetHotelInfo(string Search)
        {
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
            }
            catch (Exception)
            {
                
                throw;
            }
            return objHotelInfo;
        }

        public static RateGroup GetRateInfo(string Search,out string HotelID)
        {
            RateGroup objRates = new RateGroup();
            HotelID = "";
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                HotelID = objHotelInfo.HotelId; 
                objRates = objHotelInfo.RateList[0];
            }
            catch (Exception)
            {

                throw;
            }
            return objRates;
        }

        public static RateGroup GetRateInfo(string Search,string Supplier,string HotelID)
        {
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                HotelFilter objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search];
                objHotelInfo = objHotel.arrHotels.Where(d => d.HotelId == HotelID.ToString()).FirstOrDefault();
                objRates = objHotelInfo.RateList.Where(d=>d.Name==Supplier).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
            return objRates;
        }

        public static List<HotelOccupancy> GetOccupancyInfo(string Search)
        {
            List<HotelOccupancy> arrOccupancy = new List<HotelOccupancy>();
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                arrOccupancy = objHotelInfo.RateList[0].RoomOccupancy;
            }
            catch (Exception)
            {

                throw;
            }
            return arrOccupancy;
        }


    }
}