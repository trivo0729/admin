﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class GlobalDefault
    {
        public Int64 sid;
        public string uid;
        public string UserType;
        public string password;
        public string AgencyName;
        public string AgencyLogo;
        public string AgencyType;
        public string MerchantID;
        public string ContactPerson;
        public string Last_Name;
        public string Designation;
        public Int64 ContactID;
        public Int64 RoleID;
        public DateTime Validity;
        public DateTime dtLastAccess;
        public string Remarks;
        public string ValidationCode;
        public bool LoginFlag;
        public string RefAgency;
        public string PANNo;
        public string ServiceTaxNO;
        public string Donecarduser;
        public string MerchantURL;
        public bool TDSFlag;
        public bool EnableDeposit;
        public bool EnableCard;
        public bool EnableCheckAccount;
        public string Agentuniquecode;
        public string Staffuniquecode;
        public DateTime Updatedate;
        public string FailedReports;
        public string BookingDate;
        public string Bankdetails;
        public string IATANumber;
        public string DistAgencyName;
        public string agentCategory;
        public Int64 ParentId;
        public Int64 GroupId;
        public float AvailableCrdit;
        public float CreditLimit;
        public float OTC;
        public bool IsFirstLogIn;
        public string Currency;
        public string Franchisee;
        public bool SupplierVisible;
        public List<ExchangeRate> ExchangeRate;
        //public List<MarkupsAndTaxes> MarkupsAndTaxes;

    }
    public class geoLocation
    {
        public string IPAddress { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string CityName { get; set; }
        public string RegionName { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
    }
    public class AgentDetailsOnAdmin
    {
        public Int64 sid { get; set; }
        public string uid { get; set; }
        public string UserType { get; set; }
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string Last_Name { get; set; }
        public Int64 RoleID { get; set; }
        public string Agentuniquecode { get; set; }
        public Int64 GroupId { get; set; }
        public float AvailableCredit { get; set; }
        public bool SupplierVisible { get; set; }
        public string Currency { get; set; }
    }

    public class AgentDetailsOnAgentStaff
    {
        public Int64 sid { get; set; }
        public string uid { get; set; }
        public string UserType { get; set; }
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string Last_Name { get; set; }
        public Int64 RoleID { get; set; }
        public string Agentuniquecode { get; set; }
        public Int64 GroupId { get; set; }
        public float AvailableCredit { get; set; }
    }

    public class Forms
    {
        public string[] strAuthorizedFormCollection;
    }


    public class MarkupsAndTaxes
    {

        public List<IndividualMarkup> listIndividualMarkup;
        public List<GroupMarkup> listGroupMarkup;
        public List<GlobalMarkup> listGlobalMarkup;
        public List<AgentMarkup> listAgentMarkup;
        public List<SupplierCommision> SupplierCommision;
        public float PerCutMarkup { get; set; }
        public float PerGroupMarkup { get; set; }
        public float PerIndividualMarkup { get; set; }
        //public float AgentOwnMarkupPer { get; set; }
        //public float AgentOwnMarkupAmt { get; set; }
        public float PerAgentMarkup { get; set; }
        public float PerServiceTax { get; set; }
        public int TimeGap { get; set; }
        public bool IsStaff { get; set; }
        public float PerStaffMarkup { get; set; }
        public float CutCommsion { get; set; }
        public float CutCanCommsion { get; set; }
        public List<CommonLib.Response.TaxRate> ListCharge { get; set; }

    }

    public class MarkupsAndTaxesTrans
    {

        public List<IndividualMarkup> listIndividualMarkup;
        public List<GroupMarkup> listGroupMarkup;
        public List<GlobalMarkup> listGlobalMarkup;
        public List<AgentMarkup> listAgentMarkup;
        public float PerCutMarkup { get; set; }
        public float PerGroupMarkup { get; set; }
        public float PerIndividualMarkup { get; set; }
        //public float AgentOwnMarkupPer { get; set; }
        //public float AgentOwnMarkupAmt { get; set; }
        public float PerAgentMarkup { get; set; }
        public float PerServiceTax { get; set; }
        public int TimeGap { get; set; }
        public bool IsStaff { get; set; }
        public float PerStaffMarkup { get; set; }
        public float CutCommsion { get; set; }
        public float CutCanCommsion { get; set; }


    }
    public class IndividualMarkup
    {
        public string Supplier { get; set; }
        public string SupplierType { get; set; }
        public float IndMarkupPer { get; set; }
        public float IndMarkAmt { get; set; }
        public float IndCommPer { get; set; }
        public float IndCommAmt { get; set; }
    }
    public class GroupMarkup
    {
        public string Supplier { get; set; }
        public string SupplierType { get; set; }
        public float GroupMarkupPer { get; set; }
        public float GroupMarkAmt { get; set; }
        public float GroupCommPer { get; set; }
        public float GroupCommAmt { get; set; }
        public bool TaxOnMarkup { get; set; }
    }
    public class GlobalMarkup
    {
        public string Supplier { get; set; }
        public string SupplierType { get; set; }
        public float GlobalMarkupPer { get; set; }
        public float GlobalMarkAmt { get; set; }
        public float GlobalCommPer { get; set; }
        public float GlobalCommAmt { get; set; }
    }

    public class AgentMarkup
    {
        public float AgentOwnMarkupPer { get; set; }
        public float AgentOwnMarkupAmt { get; set; }
        public string SupplierType { get; set; }
    }

    public class AgentInfo
    {
        public float AgentCredit { get; set; }
        public string SupplierType { get; set; }
        public float AgentCreditLimit { get; set; }
        public float AgentVisaMarkup { get; set; }
        public DataTable dtVisaDetails { get; set; }
        //public string[] VisaPath { get; set; }
    }
    public class ExchangeRate
    {
        public string Currency { get; set; }
        public float Rate { get; set; }
        public string LastUpdateDt { get; set; }
        public bool MailFlag { get; set; }
        //public bool Status { get; set; }
    }
    public class SupplierCommision
    {
        public string Supplier { get; set; }
        public float Commision { get; set; }
        public float TDS { get; set; }
    }
    public class HotelInfo
    {
        public Int64 sid { get; set; }
        public string HotelName { get; set; }
        public string SupplierName { get; set; }
    }
}