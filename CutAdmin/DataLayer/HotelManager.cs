﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class HotelManager
    {
        public static DBHelper.DBReturnCode Get(string name, string destination, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@HotelName", name);
            sqlParams[1] = new SqlParameter("@DestinationCode", String.IsNullOrEmpty(destination) ? null : destination);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotel", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetCategory(string code, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@LaungageCode", code);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelCategory", out dtResult, sqlParams);
            return retCode;
        }
        public static string GetDescription(string code, string LanCode)
        {
            string m_description = "N/A";
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@HotelCode", code);
            sqlParams[1] = new SqlParameter("@LanCode", LanCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelDescripton", out dtResult, sqlParams);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                m_description = dtResult.Rows[0]["Descriptions"].ToString().Length > 150 ? dtResult.Rows[0]["Descriptions"].ToString().Remove(150) : dtResult.Rows[0]["Descriptions"].ToString();
            return m_description;
        }
        public static DBHelper.DBReturnCode GetHotelFacility(string m_hotel, string m_lang, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            return DBHelper.GetDataTable("usp_List_Hotel_Facility", out dtResult, sqlparam);
        }
        public static float GetUserRating(string m_hotel, string m_lang)
        {
            float rating = 0;
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_GetHotelUserRating", out dtResult, sqlparam);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                float.TryParse(dtResult.Rows[0]["AverageValue"].ToString(), out rating);
            return rating;
        }
        public static int GetHotelReview(string m_hotel, string m_lang)
        {
            int review = 0;
            DataTable dtResult = new DataTable();
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("usp_GetHotelUserReview", out dtResult, sqlparam);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
                review = Convert.ToInt32(dtResult.Rows[0][0]);
            return review;
        }
        public static DBHelper.DBReturnCode GetHotelAccomodation(string m_hotel, string m_lang, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            return DBHelper.GetDataTable("usp_Get_Accommodation", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode GetHotelRoomFacility(string m_hotel, string m_lang, string roomType, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[3];
            sqlparam[0] = new SqlParameter("@LanguageCode", m_lang);
            sqlparam[1] = new SqlParameter("@HotelCode", m_hotel);
            sqlparam[2] = new SqlParameter("@RoomType", roomType);
            return DBHelper.GetDataTable("usp_GET_ROOM_FACILITIES", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode GetHotelDetail(string LangCode, List<String> List_HotelCode, out DataSet ds)
        {
            ds = null;
            DataTable dtHotel = new DataTable();
            DataColumn dcHotel = new DataColumn("HotelCode", typeof(string));
            dtHotel.Columns.Add(dcHotel);
            foreach (string data in List_HotelCode)
            {
                DataRow dr = dtHotel.NewRow();
                dr[0] = data;
                dtHotel.Rows.Add(dr);
            }
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@LangCode", LangCode);
            sqlparam[1] = new SqlParameter("@HotelCode", SqlDbType.Structured);
            sqlparam[1].Value = dtHotel;
            return DBHelper.GetDataSet("usp_GetHotelDetail", out ds, sqlparam);
        }
        public static DBHelper.DBReturnCode GetMarkupDetail(string Supplier, Int64 UserID, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@UserID", UserID);
            sqlparam[1] = new SqlParameter("@Supplier", Supplier);
            return DBHelper.GetDataTable("usp_GetMarkUpDetails", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Facility(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_FACILITY", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Room_Facility(string HotelCode, List<string> RoomType, out DataTable dtResult)
        {
            dtResult = null;
            DataTable dtRoomType = new DataTable();
            DataColumn dcCol = new DataColumn("RoomType", typeof(string));
            dtRoomType.Columns.Add(dcCol);
            foreach (string roomType in RoomType)
            {
                DataRow dr = dtRoomType.NewRow();
                dr[0] = roomType;
                dtRoomType.Rows.Add(dr);
            }
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            sqlparam[1] = new SqlParameter("@RoomType", SqlDbType.Structured);
            sqlparam[1].Value = dtRoomType;
            return DBHelper.GetDataTable("usp_GET_HOTEL_ROOM_FACILITY", out dtResult, sqlparam);
        }
        public static DBHelper.DBReturnCode Get_Hotel_Image(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_IMAGE", out dtResult, sqlparam);
        }

        public static DBHelper.DBReturnCode Get_Hotel_Address(string HotelCode, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@HotelCode", HotelCode);
            return DBHelper.GetDataTable("usp_GET_HOTEL_ADDRESS", out dtResult, sqlparam);
        }

        //...............................................MGH............................B........................//

        #region HotelSearch
        public static DBHelper.DBReturnCode AddHotelSearch(string CityName, string CheckIn, string CheckOut, string HotelCode, string TotalNights, string StarRating, string Nationality, string NumberOfRooms, string EntryDate, Int64 UserId, string AddSearchsession)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[11];
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            sqlParams[0] = new SqlParameter("@Location", CityName);
            sqlParams[1] = new SqlParameter("@CheckIn", CheckIn);
            sqlParams[2] = new SqlParameter("@CheckOut", CheckOut);
            sqlParams[3] = new SqlParameter("@HotelName", HotelCode);
            sqlParams[4] = new SqlParameter("@TotalNights", TotalNights);
            sqlParams[5] = new SqlParameter("@StarRating", StarRating);
            sqlParams[6] = new SqlParameter("@Nationality", Nationality);
            sqlParams[7] = new SqlParameter("@NumberOfRooms", NumberOfRooms);
            sqlParams[8] = new SqlParameter("@EntryDate", EntryDate);
            sqlParams[9] = new SqlParameter("@UserId", UserId);
            sqlParams[10] = new SqlParameter("@AddSearchsession", AddSearchsession);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_HotelSearchLogAddBySearch", out rowsAffected, sqlParams);
            return retCode;
        }

        #endregion

        #region HotelCancel

        public static DBHelper.DBReturnCode CancelBooking(string ReservationID, string CancellationAmount, string BookingStatus, string Remark, string TotalFare, string ServiceCharge, string Total)
        {
            int rowsAffected = 0;
           
            MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            if (HttpContext.Current.Session["markups"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
            else if (HttpContext.Current.Session["AgentMarkupsOnAdmin"] != null)
                objMarkupsAndTaxes = (MarkupsAndTaxes)HttpContext.Current.Session["AgentMarkupsOnAdmin"];


            Int64 ParentID = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                ParentID = objGlobalDefault.sid;
            }
            else if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                ParentID = objAgentDetailsOnAdmin.sid;
            }

            //float CanAmountWithTax = Convert.ToSingle(CancellationAmount);
            //float CanAmountWithoutTax = (CanAmountWithTax / (objMarkupsAndTaxes.PerServiceTax + 100)) * 100;
            //float CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
            //float SupplierCanAmount = (CanAmountWithoutTax / (objMarkupsAndTaxes.PerCutMarkup + 100)) * 100;
            //float CutComm = (SupplierCanAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
            //float AgenComm = (CanAmountWithTax * objMarkupsAndTaxes.PerAgentMarkup) / 100;
            float CanAmountWithTax = 0;
            float CanAmountWithoutTax = 0;
            float CanServiceTax = 0;
            float SupplierCanAmount = 0;
            if (Convert.ToSingle(CancellationAmount) != 0)
            {
                CanAmountWithTax = Convert.ToSingle(CancellationAmount) + Convert.ToSingle(ServiceCharge);
                CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                CanAmountWithoutTax = CanAmountWithTax;
                //CanAmountWithoutTax = Convert.ToSingle(CancellationAmount);
                //CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                //CanAmountWithTax = CanAmountWithoutTax + CanServiceTax;
            }
            else
            {
                CanAmountWithoutTax = Convert.ToSingle(CancellationAmount);
                CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
                CanAmountWithTax = Convert.ToSingle(CancellationAmount) + Convert.ToSingle(ServiceCharge);
            }
            //CanAmountWithoutTax = CanAmountWithTax - Convert.ToSingle(ServiceCharge);
            //CanServiceTax = (CanAmountWithoutTax * objMarkupsAndTaxes.PerServiceTax) / 100;
            SupplierCanAmount = (CanAmountWithoutTax / (objMarkupsAndTaxes.PerCutMarkup + 100)) * 100;
            float CutComm = (SupplierCanAmount * objMarkupsAndTaxes.PerCutMarkup) / 100;
            float AgenComm = (CanAmountWithTax * objMarkupsAndTaxes.PerAgentMarkup) / 100;
            //bool CancelFlag = true;
            SqlParameter[] sqlParams = new SqlParameter[11];
            sqlParams[0] = new SqlParameter("@uid", ParentID);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@BookingStatus", BookingStatus);
            sqlParams[3] = new SqlParameter("@CanAmountWithoutTax", CanAmountWithoutTax);
            sqlParams[4] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[5] = new SqlParameter("@CanAmountWithTax", CanAmountWithTax);
            sqlParams[6] = new SqlParameter("@SupplierCanAmount", SupplierCanAmount);
            sqlParams[7] = new SqlParameter("@CutCanComm", CutComm);
            sqlParams[8] = new SqlParameter("@AgentCanComm", AgenComm);
            sqlParams[9] = new SqlParameter("@CancellationDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[10] = new SqlParameter("@Remark", Remark);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_CancelBooking", out rowsAffected, sqlParams);

            return retCode;
        }

        #endregion

    }
}