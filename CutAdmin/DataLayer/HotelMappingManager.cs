﻿using CommonLib.Response;
using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class HotelMappingManager
    {
        public static  List<CommonLib.Response.CommonHotelDetails> skipMappedHotels( List<CommonLib.Response.CommonHotelDetails> CommonHotels)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
                List<CommonLib.Response.CommonHotelDetails> newCommonHotels = new List<CommonLib.Response.CommonHotelDetails>();
                List<CommonLib.Response.CommonHotelDetails> tempHotels = new List<CommonLib.Response.CommonHotelDetails>();
                List<tbl_CommonHotelMaster> ListHotel = new List<tbl_CommonHotelMaster>();
                int j = 0;

                foreach (CommonHotelDetails CommonHotel in CommonHotels)
                {

                    switch (CommonHotel.Supplier)
                    {
                        case "HotelBeds":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.HotelBedsCode == CommonHotel.HotelId.Split('_')[0] select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                            {
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            }

                            break;
                        case "MGH":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.MGHCode == CommonHotel.HotelId select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            break;
                        case "DoTW":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.DotwCode == CommonHotel.HotelId select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                            {
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            }

                            break;
                        case "Expedia":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.ExpediaCode == CommonHotel.HotelId select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            break;
                        case "GRN":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.GRNCode == CommonHotel.HotelId select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            break;
                        case "GTA":
                            ListHotel = (from obj in DB.tbl_CommonHotelMasters where obj.GTACode == CommonHotel.HotelId select obj).ToList();
                            if (ListHotel.Count == 0)
                                tempHotels.Add(CommonHotel);
                            else
                                tempHotels.Add(AddHotel(ListHotel[0]));
                            break;

                    }
                }
                newCommonHotels = tempHotels.GroupBy(x => x.HotelId)
                                      .Select(g => g.First())
                                      .ToList();

                return newCommonHotels;
            }
         }


        #region Add Cut Hotel
        public static CommonHotelDetails AddHotel(object CutHotel)
        {   
            CommonHotelDetails objHotel = new CommonHotelDetails();
            var Data = (tbl_CommonHotelMaster)CutHotel;
            string HotelName = Data.HotelName;
      
            objHotel = new CommonHotelDetails {
                HotelName = Data.HotelName,
                HotelId = (Data.sid).ToString(),
                Address = Data.HotelAddress,
                Category = Data.HotelCategory,
                DotwCode =Data.DotwCode,
                HotelBedsCode = Data.HotelBedsCode,
                MGHCode =Data.MGHCode,
                GTACode=Data.GTACode,
                Description = Data.HotelDescription,
                Facility = Data.HotelFacilities.Split(',').ToList(),
                Langitude = Data.HotelLangitude,
                Latitude = Data.HotelLatitude,
                Image = GetImage(Data.SubImages),
                Supplier= "ClickUrTrip"
            };
            return objHotel;
        }

            public static List<Image> GetImage(string Image)
            {
                List<Image> Images = new List<Image>();
                if (Image != null && Image != "")
                {
                    List<string> Url = Image.Split('^').ToList();
                   
                    foreach (string Link in Url)
                    {
                        Images.Add(new Image
                        {
                            Url = Link,
                            Count = Url.Count
                        });
                    }
                   
                }
                return Images;
              
            }

        
        #endregion
        //#region add mapping done with linq
        //    public static DBHelper.DBReturnCode AddHotelMapping(string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sMainImage, string sSubImages, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sCountryCode, string sCityCode, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime)
        //    {
        //        DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Int64 UserID = objUser.sid;
        //        string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
        //        int rowsAffected = 0;
        //        SqlParameter[] sqlParams = new SqlParameter[29];
        //        sqlParams[0] = new SqlParameter("@HotelName", sHotelName);
        //        sqlParams[1] = new SqlParameter("@HotelAddress", sHotelAddress);
        //        sqlParams[2] = new SqlParameter("@Description", sDescription);
        //        sqlParams[3] = new SqlParameter("@Ratings", sRatings);
        //        sqlParams[4] = new SqlParameter("@Facilities", sFacilities);
        //        sqlParams[5] = new SqlParameter("@Langitude", sLangitude);
        //        sqlParams[6] = new SqlParameter("@Latitude", sLatitude);
        //        sqlParams[7] = new SqlParameter("@MainImage", sMainImage);
        //        sqlParams[8] = new SqlParameter("@SubImages", sSubImages);
        //        sqlParams[9] = new SqlParameter("@HotelBedsCode", sHotelBedsCode);
        //        sqlParams[10] = new SqlParameter("@DotwCode", sDotwCode);
        //        sqlParams[11] = new SqlParameter("@MGHCode", sMGHCode);
        //        sqlParams[11] = new SqlParameter("@GRNCode", sGRNCode);
        //        sqlParams[12] = new SqlParameter("@ExpediaCode", sExpediaCode);
        //        sqlParams[13] = new SqlParameter("@CountryCode", sCountryCode);
        //        sqlParams[14] = new SqlParameter("@CityCode", sCityCode);
        //        sqlParams[15] = new SqlParameter("@ZipCode", sZipCode);
        //        sqlParams[16] = new SqlParameter("@PatesAllowed", sPatesAllowed);
        //        sqlParams[17] = new SqlParameter("@LiquorPolicy", sLiquorPolicy);
        //        sqlParams[18] = new SqlParameter("@TripAdviserLink", sTripAdviserLink);
        //        sqlParams[19] = new SqlParameter("@HotelGroup", sHotelGroup);
        //        sqlParams[20] = new SqlParameter("@CheckinTime", sCheckinTime);
        //        sqlParams[21] = new SqlParameter("@AdultMinAge", sAdultMinAge);
        //        sqlParams[22] = new SqlParameter("@ChildAgeFrom", sChildAgeFrom);
        //        sqlParams[23] = new SqlParameter("@ChildAgeTo", sChildAgeTo);
        //        sqlParams[24] = new SqlParameter("@CheckoutTime", sCheckoutTime);
        //        sqlParams[25] = new SqlParameter("@CreatedBy", UserID);
        //        sqlParams[26] = new SqlParameter("@CreatedDate", DateTimes);
        //        sqlParams[27] = new SqlParameter("@UpdatedBy", UserID);
        //        sqlParams[28] = new SqlParameter("@UpdatedDate", DateTimes);
        //        DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_AddHotelMapping", out rowsAffected, sqlParams);
        //        return retCode;
        //    }
       
        //#endregion

            #region Images
            public static DataLayer.DataManager.DBReturnCode HotelImages(string ImagePath, string Hid)
            {
                string[] Path = ImagePath.Split('^');
                string MainImage = ImagePath.Split('^')[0];
                int i = 0; ImagePath = "";
                foreach (string Image in Path)
                {


                    if (Image != "")
                    {
                        if ((Path.Length - 1) != i)
                            ImagePath += Image + "^";
                        else
                            ImagePath += Image;

                    }
                    i++;
                }
                StringBuilder sSQL = new StringBuilder();
                DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
                if (ImagePath != "")
                {
                    sSQL.Append("Update tbl_CommonHotelMaster set HotelImage ='" + MainImage + "', SubImages='" + ImagePath + "' where sid='" + Hid + "'");
                }                
                return retCopde = DataManager.ExecuteNonQuery(sSQL.ToString());
            }

            #endregion

            public static DBHelper.DBReturnCode SearchHotelMapp(string sHotelName, Int64 sUserID, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sGTACode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@HotelName", sHotelName);
            sqlParams[1] = new SqlParameter("@sUserID", sUserID);
            sqlParams[2] = new SqlParameter("@HotelBedsCode", sHotelBedsCode);
            sqlParams[3] = new SqlParameter("@DotwCode", sDotwCode);
            sqlParams[4] = new SqlParameter("@MGHCode", sMGHCode);
            sqlParams[5] = new SqlParameter("@GRNCode", sGRNCode);
            sqlParams[6] = new SqlParameter("@ExpediaCode", sExpediaCode);
            sqlParams[7] = new SqlParameter("@GTACode", sGTACode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_SearchHotelMapp", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_HotelMapLoadAllCountry", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCity(string country, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@CountryCode", country);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_HotelLoadAllCityByCountry", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMappedHotels(string sid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetMappedHotelsByUser", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetMappedHotelstoUpdate(string Country, string City, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Country", Country);
            sqlParams[1] = new SqlParameter("@City", City);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_MappedHotelsbyCity", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetHotelByHotelId(Int64 HotelCode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@HotelID", HotelCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_MappedHotelById", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetFacilities(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetFacilitiesMappedHotels", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode CheckFacilities(string Facility, out DataTable dtResult)
        {          
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@FacilitiesText", Facility);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_CheckFacilities", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetHalal(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetGetHalal", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode AddSupplier(Int64 srno, string SupplierName, string SupplierCode)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@HotelId", srno);
            sqlParams[1] = new SqlParameter("@SupplierName", SupplierName);
            sqlParams[2] = new SqlParameter("@SupplierCode", SupplierCode);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_UpdateHotelSupplier", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode SaveFacilities(string sFacilities, Int64 sHotelId)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@HotelId", sHotelId);
            sqlParams[1] = new SqlParameter("@Facilities", sFacilities);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_SaveFacilitiesHotelMapp", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode SaveRatings(string sRatings, Int64 sHotelId)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@HotelId", sHotelId);
            sqlParams[1] = new SqlParameter("@Ratings", sRatings);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_SaveRatingsHotelMapp", out rowsAffected, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode SaveImageUrl(string ImgUrl, Int64 HotelId)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@HotelId", HotelId);
            sqlParams[1] = new SqlParameter("@AddedUrl", ImgUrl);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_SaveImageUrlHotelMapp", out rowsAffected, sqlParams);
            return retCode;
        }
    }
}