﻿using CutAdmin.BL;
using CutAdmin.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class InventoryManager
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //static dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;

        public static List<RecordInv> Record = new List<RecordInv>();
        

       public static List<tbl_CommonHotelInventory> Allocation = new List<tbl_CommonHotelInventory>();
       public static List<tbl_CommonHotelInventory> Freesale = new List<tbl_CommonHotelInventory>();
       public static List<tbl_CommonHotelInventory> Allotment = new List<tbl_CommonHotelInventory>();

        public class AlllistInventory
        {
            public List<tbl_CommonHotelInventory> List_AllInventory { get; set; }
        }

        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
  
        public static bool CheckInventory(List<RatesManager.Supplier> Supplier)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    bool result = true;

                    Int32 noOfRoom = 0;
                    string Room = "";
                    for (int i = 0; i < Supplier[0].Details.Count; i++)
                    {
                        List<AlllistInventory> Inventory = new List<AlllistInventory>();
                        List<string> ListMonth = new List<string>();
                        List<string> ListDate = new List<string>();
                        for (int j = 0; j < Supplier[0].Details[i].Rate.ListDates.Count; j++)
                        {
                            string Date = Supplier[0].Details[i].Rate.ListDates[j].Date;
                            string Type = Supplier[0].Details[i].Rate.ListDates[j].Type;
                            Room = Supplier[0].Details[i].RateTypeID.ToString();
                            noOfRoom = Supplier[0].Details[i].noRooms;
                            ListDate.Add(Date);

                            string mnth = Date.Split('-')[1];


                            if (!ListMonth.Exists(d => d.Contains(mnth)))
                            {
                                var listInventory = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == Supplier[0].HotelCode && obj.RoomType == Room && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RateType == Type select obj).ToList();

                                Inventory.Add(new AlllistInventory
                                {

                                    List_AllInventory = listInventory,
                                });
                            }

                            ListMonth.Add(mnth);

                        }

                        for (int k = 0; k < Inventory.Count; k++)
                        {

                            foreach (var item in Inventory[k].List_AllInventory)
                            {
                                if (item.InventoryType == "Allocation")
                                {
                                    Allocation.Add(item);
                                }
                                else if (item.InventoryType == "FreeSale")
                                {
                                    Freesale.Add(item);
                                }
                                else if (item.InventoryType == "Allotment")
                                {
                                    Allotment.Add(item);
                                }

                            }
                        }


                        foreach (var Date in ListDate)
                        {

                            var dt = Date.Split('-')[0];
                            var mn = Date.Split('-')[1];
                            bool res;
                            switch (dt)
                            {

                                case "01":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "02":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "03":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "04":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "05":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "06":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;

                                case "07":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "08":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "09":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "10":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "11":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "12":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "13":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "14":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "15":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "16":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "17":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "18":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "19":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "20":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "21":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "22":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "23":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "24":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "25":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "26":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "27":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "28":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "29":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "30":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "31":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                            }
                        }

                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Check Inventory 
        public static bool CheckInventory(string HotelCode, List<CommonLib.Response.RoomType> ListRate)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    bool result = true;

                    Int32 noOfRoom = 0;
                    string Room = "";
                    for (int i = 0; i < ListRate.Count; i++)
                    {
                        List<AlllistInventory> Inventory = new List<AlllistInventory>();
                        List<string> ListMonth = new List<string>();
                        List<string> ListDate = new List<string>();
                        for (int j = 0; j < ListRate[i].Dates.Count; j++)
                        {
                            string Date = ListRate[i].Dates[j].datetime;
                            string Type = ListRate[i].Dates[j].Type;
                            Room = ListRate[i].RoomTypeId;
                            noOfRoom = 1;
                            ListDate.Add(Date);

                            string mnth = Date.Split('-')[1];


                            if (!ListMonth.Exists(d => d.Contains(mnth)))
                            {
                                var listInventory = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType == Room && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RateType == Type select obj).ToList();

                                Inventory.Add(new AlllistInventory
                                {

                                    List_AllInventory = listInventory,
                                });
                            }

                            ListMonth.Add(mnth);

                        }

                        for (int k = 0; k < Inventory.Count; k++)
                        {

                            foreach (var item in Inventory[k].List_AllInventory)
                            {
                                if (item.InventoryType == "Allocation")
                                {
                                    Allocation.Add(item);
                                }
                                else if (item.InventoryType == "FreeSale")
                                {
                                    Freesale.Add(item);
                                }
                                else if (item.InventoryType == "Allotment")
                                {
                                    Allotment.Add(item);
                                }

                            }
                        }


                        foreach (var Date in ListDate)
                        {

                            var dt = Date.Split('-')[0];
                            var mn = Date.Split('-')[1];
                            bool res;
                            switch (dt)
                            {

                                case "01":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "02":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "03":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "04":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "05":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "06":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;

                                case "07":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "08":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "09":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "10":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "11":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "12":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "13":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "14":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "15":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "16":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "17":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "18":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "19":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "20":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "21":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "22":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "23":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "24":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "25":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "26":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "27":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "28":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "29":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "30":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                                case "31":
                                    res = Check(dt, noOfRoom, mn, Room);
                                    if (res == false)
                                        return result = res;
                                    break;
                            }
                        }

                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        public static bool Check(string date, Int32 noOfRoom, string month, string Room)
        {
            try
            {
                var Invtype = "";
                List<tbl_CommonHotelInventory> arrAllocationn = new List<tbl_CommonHotelInventory>();
                List<tbl_CommonHotelInventory> arrAllotmentt = new List<tbl_CommonHotelInventory>();
                List<tbl_CommonHotelInventory> arrFreesales = new List<tbl_CommonHotelInventory>();
                arrAllocationn.Add(Allocation.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                arrAllotmentt.Add(Allotment.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                arrFreesales.Add(Freesale.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                string newdate = date.TrimStart('0');
                
                if (arrAllocationn[0] != null)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(arrAllocationn);

                    date = "Date_" + newdate;
                    string noRooms = dt.Rows[0][date].ToString();
                    Int64 id = Convert.ToInt64(dt.Rows[0]["Sid"]);
                    if(noRooms != "")
                    {
                        string noTotalRooms = noRooms.Split('_')[1];
                        string Type = noRooms.Split('_')[0];
                        string Sold = noRooms.Split('_')[2];
                        if (noTotalRooms != "0")
                        {
                            return true;
                        }
                        else if (arrFreesales[0] != null)
                        {
                            ListtoDataTable lsttodttt = new ListtoDataTable();
                            DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                            string newdatet = date.TrimStart('0');
                            date = "Date_" + newdate;
                            var Option = "Option_" + date;
                            string Opt = dtt.Rows[0][Option].ToString();
                            string[] OptValue = Opt.Split('_');
                            string noTotalRoom = dtt.Rows[0][date].ToString();
                            Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                            string[] room = noTotalRoom.Split('_');
                            if (room[0] == "fs" && room[1] == "")
                            {
                                if (OptValue[0] != "-" && OptValue[0] != "")
                                {
                                    if (Convert.ToInt32(OptValue[0]) >= noOfRoom)
                                        return true;
                                    else
                                        return false;
                                }
                                else
                                {
                                    return true;
                                }
                            }
                            else if (room[0] == "fs" && room[1] != "")
                            {
                                if (OptValue[0] != "-" && OptValue[0] != "")
                                {
                                    if (Convert.ToInt32(OptValue[0]) >= noOfRoom && Convert.ToInt32(room[1]) >= noOfRoom)
                                         return true;
                                    else
                                        return false;
                                }

                                else if (Convert.ToInt32(room[1]) >= noOfRoom)
                                {
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }

                            else
                            {
                                return false;
                            }

                        }
                    }
                    else if (arrFreesales[0] != null)
                    {
                        ListtoDataTable lsttodttt = new ListtoDataTable();
                        DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                        string newdatet = date.TrimStart('0');
                        date = "Date_" + newdate;
                        var Option = "Option_" + date;
                        string Opt = dtt.Rows[0][Option].ToString();
                        string[] OptValue = Opt.Split('_');
                        string noTotalRoom = dtt.Rows[0][date].ToString();
                        Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                        string[] room = noTotalRoom.Split('_');
                        if (room[0] == "fs" && room[1] == "")
                        {
                            if (OptValue[0] != "-" && OptValue[0] != "")
                            {
                                if (Convert.ToInt32(OptValue[0]) >= noOfRoom)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else if (room[0] == "fs" && room[1] != "")
                        {
                            if (OptValue[0] != "-" && OptValue[0] != "")
                            {
                                if (Convert.ToInt32(OptValue[0]) >= noOfRoom && Convert.ToInt32(room[1]) >= noOfRoom)
                                    return true;
                                else
                                    return false;
                            }

                            else if (Convert.ToInt32(room[1]) >= noOfRoom)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }

                        else
                        {
                            return false;
                        }

                    }
                  
                    // decimal avlroom = Convert.ToInt32(noTotalRooms) - Convert.ToInt32(Sold);
                  

                    return false;
                }

                else if (arrFreesales[0] != null)
                {
                    ListtoDataTable lsttodttt = new ListtoDataTable();
                    DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                    string newdatet = date.TrimStart('0');
                    date = "Date_" + newdate;
                    var Option = "Option_" + date;
                    string Opt = dtt.Rows[0][Option].ToString();
                    string[] OptValue = Opt.Split('_');
                    string noTotalRooms = dtt.Rows[0][date].ToString();
                    Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                    string[] room = noTotalRooms.Split('_');
                    if (room[0] == "fs" && room[1] == "")
                    {
                        if (OptValue[0] != "-" && OptValue[0] != "")
                        {
                            if (Convert.ToInt32(OptValue[0]) >= noOfRoom)
                                return true;
                            else
                                return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else if (room[0] == "fs" && room[1] != "")
                    {
                        if (OptValue[0] != "-" && OptValue[0] != "")
                        {
                            if (Convert.ToInt32(OptValue[0]) >= noOfRoom && Convert.ToInt32(room[1]) >= noOfRoom)
                                return true;
                            else
                                return false;
                        }

                        else if (Convert.ToInt32(room[1]) >= noOfRoom)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }

                else if (arrAllotmentt[0] != null)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(arrAllotmentt);

                    date = "Date_" + newdate;
                    string noRooms = dt.Rows[0][date].ToString();
                    Int64 id = Convert.ToInt64(dt.Rows[0]["Sid"]);
                    if(noRooms != "")
                    {
                        string noTotalRooms = noRooms.Split('_')[1];
                        string Type = noRooms.Split('_')[0];
                        string Sold = noRooms.Split('_')[2];
                        //  decimal avlroom = Convert.ToInt32(noTotalRooms) - Convert.ToInt32(Sold);
                        if (noTotalRooms != "0")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                        return false;
                  
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateInventory(List<RatesManager.Supplier> Supplier, string ReservationID)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    List<RecordInv> Record = new List<RecordInv>();
                    Int32 noOfRoom = 0;
                    string Room = "";
                    for (int i = 0; i < Supplier[0].Details.Count; i++)
                    {
                        List<AlllistInventory> Inventory = new List<AlllistInventory>();
                        List<string> ListMonth = new List<string>();
                        List<string> ListDate = new List<string>();
                        for (int j = 0; j < Supplier[0].Details[i].Rate.ListDates.Count; j++)
                        {
                            string Date = Supplier[0].Details[i].Rate.ListDates[j].Date;
                            string Type = Supplier[0].Details[i].Rate.ListDates[j].Type;
                            Room = Supplier[0].Details[i].RateTypeID.ToString();
                            noOfRoom = Supplier[0].Details[i].noRooms;
                            ListDate.Add(Date);

                            string mnth = Date.Split('-')[1];


                            if (!ListMonth.Exists(d => d.Contains(mnth)))
                            {
                                var listInventory = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == Supplier[0].HotelCode && obj.RoomType == Room && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RateType == Type select obj).ToList();

                                Inventory.Add(new AlllistInventory
                                {

                                    List_AllInventory = listInventory,
                                });
                            }

                            ListMonth.Add(mnth);

                        }

                        for (int k = 0; k < Inventory.Count; k++)
                        {

                            foreach (var item in Inventory[k].List_AllInventory)
                            {
                                if (item.InventoryType == "Allocation")
                                {
                                    Allocation.Add(item);
                                }
                                else if (item.InventoryType == "FreeSale")
                                {
                                    Freesale.Add(item);
                                }
                                else if (item.InventoryType == "Allotment")
                                {
                                    Allotment.Add(item);
                                }

                            }
                        }

                        foreach (var Date in ListDate)
                        {

                            var dt = Date.Split('-')[0];
                            var mn = Date.Split('-')[1];
                            bool res;
                            switch (dt)
                            {

                                case "01":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "02":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "03":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "04":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "05":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "06":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "07":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "08":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "09":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "10":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "11":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "12":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "13":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "14":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "15":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "16":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "17":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "18":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "19":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "20":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "21":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "22":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "23":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "24":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "25":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "26":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "27":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "28":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "29":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "30":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "31":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                            }
                        }

                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;

            }
        }

        #region Update Inventory
        public static bool UpdateInventory(List<CommonLib.Response.RoomType> ListRate, string ReservationID,string HotelCode)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    List<RecordInv> Record = new List<RecordInv>();
                    Int32 noOfRoom = 0;
                    string Room = "";
                    for (int i = 0; i < ListRate.Count; i++)
                    {
                        List<AlllistInventory> Inventory = new List<AlllistInventory>();
                        List<string> ListMonth = new List<string>();
                        List<string> ListDate = new List<string>();
                        for (int j = 0; j < ListRate[i].Dates.Count; j++)
                        {
                            string Date = ListRate[i].Dates[j].datetime;
                            string Type = ListRate[i].Dates[j].Type;
                            Room = ListRate[i].RoomTypeId.ToString();
                            noOfRoom = 1;
                            ListDate.Add(Date);

                            string mnth = Date.Split('-')[1];


                            if (!ListMonth.Exists(d => d.Contains(mnth)))
                            {
                                var listInventory = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelCode && obj.RoomType == Room && obj.Month == Date.Split('-')[1] && obj.Year == Date.Split('-')[2] && obj.RateType == Type select obj).ToList();

                                Inventory.Add(new AlllistInventory
                                {

                                    List_AllInventory = listInventory,
                                });
                            }

                            ListMonth.Add(mnth);

                        }

                        for (int k = 0; k < Inventory.Count; k++)
                        {

                            foreach (var item in Inventory[k].List_AllInventory)
                            {
                                if (item.InventoryType == "Allocation")
                                {
                                    Allocation.Add(item);
                                }
                                else if (item.InventoryType == "FreeSale")
                                {
                                    Freesale.Add(item);
                                }
                                else if (item.InventoryType == "Allotment")
                                {
                                    Allotment.Add(item);
                                }

                            }
                        }


                        foreach (var Date in ListDate)
                        {

                            var dt = Date.Split('-')[0];
                            var mn = Date.Split('-')[1];
                            bool res;
                            switch (dt)
                            {

                                case "01":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "02":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "03":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "04":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "05":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "06":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "07":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "08":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "09":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "10":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "11":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "12":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "13":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "14":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "15":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "16":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "17":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "18":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "19":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "20":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "21":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "22":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "23":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "24":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "25":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "26":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "27":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "28":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "29":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "30":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                                case "31":
                                    res = Update(dt, noOfRoom, mn, Room, ReservationID);
                                    break;
                            }
                        }

                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;

            }
        }
        #endregion

        public static bool Update(string date, Int32 noOfRoom, string month, string Room, string ReservationID)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var Invtype = "";
                    List<tbl_CommonHotelInventory> arrAllocationn = new List<tbl_CommonHotelInventory>();
                    List<tbl_CommonHotelInventory> arrAllotmentt = new List<tbl_CommonHotelInventory>();
                    List<tbl_CommonHotelInventory> arrFreesales = new List<tbl_CommonHotelInventory>();
                    arrAllocationn.Add(Allocation.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                    arrAllotmentt.Add(Allotment.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                    arrFreesales.Add(Freesale.Where(d => d.Month == month && d.RoomType == Room).FirstOrDefault());
                    string newdate = date.TrimStart('0');

                    if (arrAllocationn[0] != null)
                    {
                        ListtoDataTable lsttodt = new ListtoDataTable();
                        DataTable dt = lsttodt.ToDataTable(arrAllocationn);

                        date = "Date_" + newdate;
                        string noRooms = dt.Rows[0][date].ToString();
                        Int64 id = Convert.ToInt64(dt.Rows[0]["Sid"]);
                        if (noRooms != "")
                        {
                            string noTotalRooms = noRooms.Split('_')[1];
                            string Type = noRooms.Split('_')[0];
                            string Sold = noRooms.Split('_')[2];
                            //decimal avlroom = Convert.ToInt32(noTotalRooms) - Convert.ToInt32(Sold);
                            if (noTotalRooms != "0")
                            {
                                decimal noOfSold = Convert.ToInt32(Sold) + noOfRoom;
                                var noRoom = (Convert.ToInt32(noTotalRooms) - noOfRoom).ToString();

                                string newValue = Type + "_" + noRoom + "_" + noOfSold;
                                decimal OldAvlRoom = Convert.ToInt32(noOfSold) + Convert.ToInt32(noRoom);
                                Invtype = "Allocation";

                                tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == id);

                                switch (newdate)
                                {

                                    case "1":
                                        Update.Date_1 = newValue;
                                        break;
                                    case "2":
                                        Update.Date_2 = newValue;
                                        break;
                                    case "3":
                                        Update.Date_3 = newValue;
                                        break;
                                    case "4":
                                        Update.Date_4 = newValue;
                                        break;
                                    case "5":
                                        Update.Date_5 = newValue;
                                        break;
                                    case "6":
                                        Update.Date_6 = newValue;
                                        break;
                                    case "7":
                                        Update.Date_7 = newValue;
                                        break;
                                    case "8":
                                        Update.Date_8 = newValue;
                                        break;
                                    case "9":
                                        Update.Date_9 = newValue;
                                        break;
                                    case "10":
                                        Update.Date_10 = newValue;
                                        break;
                                    case "11":
                                        Update.Date_11 = newValue;
                                        break;
                                    case "12":
                                        Update.Date_12 = newValue;
                                        break;
                                    case "13":
                                        Update.Date_13 = newValue;
                                        break;
                                    case "14":
                                        Update.Date_14 = newValue;
                                        break;
                                    case "15":
                                        Update.Date_15 = newValue;
                                        break;
                                    case "16":
                                        Update.Date_16 = newValue;
                                        break;
                                    case "17":
                                        Update.Date_17 = newValue;
                                        break;
                                    case "18":
                                        Update.Date_18 = newValue;
                                        break;
                                    case "19":
                                        Update.Date_19 = newValue;
                                        break;
                                    case "20":
                                        Update.Date_20 = newValue;
                                        break;
                                    case "21":
                                        Update.Date_21 = newValue;
                                        break;
                                    case "22":
                                        Update.Date_22 = newValue;
                                        break;
                                    case "23":
                                        Update.Date_23 = newValue;
                                        break;
                                    case "24":
                                        Update.Date_24 = newValue;
                                        break;
                                    case "25":
                                        Update.Date_25 = newValue;
                                        break;
                                    case "26":
                                        Update.Date_26 = newValue;
                                        break;
                                    case "27":
                                        Update.Date_27 = newValue;
                                        break;
                                    case "28":
                                        Update.Date_28 = newValue;
                                        break;
                                    case "29":
                                        Update.Date_29 = newValue;
                                        break;
                                    case "30":
                                        Update.Date_30 = newValue;
                                        break;
                                    case "31":
                                        Update.Date_31 = newValue;
                                        break;
                                }
                                DB.SubmitChanges();
                                //return true;
                                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                Record.Add(new RecordInv
                                {
                                    BookingId = ReservationID,
                                    InvSid = arrAllocationn[0].Sid.ToString(),
                                    SupplierId = objGlobalDefault.sid.ToString(),
                                    HotelCode = arrAllocationn[0].HotelCode,
                                    RoomType = Room,
                                    RateType = arrAllocationn[0].RateType,
                                    InvType = Invtype,
                                    Date = date,
                                    Month = month,
                                    Year = "",
                                    TotalAvlRoom = noRoom,
                                    OldAvlRoom = OldAvlRoom.ToString(),
                                    NoOfBookedRoom = noOfRoom.ToString(),
                                    NoOfCancleRoom = "",
                                    UpdateDate = DateTime.Now,
                                    UpdateOn = "Booking",
                                });

                            }
                            else if (arrFreesales[0] != null)
                            {
                                ListtoDataTable lsttodttt = new ListtoDataTable();
                                DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                                string newdatet = date.TrimStart('0');
                                date = "Date_" + newdate;
                                var Option = "Option_" + date;
                                string OptValue = dtt.Rows[0][Option].ToString();
                                string noTotalRoom = dtt.Rows[0][date].ToString();
                                Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                                string[] room = noTotalRoom.Split('_');

                                if (room[0] == "fs" && room[1] == "")
                                {
                                    //string noRoomf = noRoomss.Split('_')[1];
                                    //string Types = noRoomss.Split('_')[0];
                                    //string Solds = noRoomss.Split('_')[2];
                                    // noOfRoom
                                    //  noRoom = (Convert.ToInt32(noRoomf) - noOfRoom).ToString();
                                    var sold = Convert.ToInt32(room[2]) + noOfRoom;
                                    string newValue = room[0] + "_" + room[1] + "_" + sold;
                                    Invtype = "Freesale";
                                    //var TotalAvlRoom = Convert.ToInt32(room[1]) - sold;
                                    tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                                    switch (newdate)
                                    {

                                        case "1":
                                            Update.Date_1 = newValue;
                                            break;
                                        case "2":
                                            Update.Date_2 = newValue;
                                            break;
                                        case "3":
                                            Update.Date_3 = newValue;
                                            break;
                                        case "4":
                                            Update.Date_4 = newValue;
                                            break;
                                        case "5":
                                            Update.Date_5 = newValue;
                                            break;
                                        case "6":
                                            Update.Date_6 = newValue;
                                            break;
                                        case "7":
                                            Update.Date_7 = newValue;
                                            break;
                                        case "8":
                                            Update.Date_8 = newValue;
                                            break;
                                        case "9":
                                            Update.Date_9 = newValue;
                                            break;
                                        case "10":
                                            Update.Date_10 = newValue;
                                            break;
                                        case "11":
                                            Update.Date_11 = newValue;
                                            break;
                                        case "12":
                                            Update.Date_12 = newValue;
                                            break;
                                        case "13":
                                            Update.Date_13 = newValue;
                                            break;
                                        case "14":
                                            Update.Date_14 = newValue;
                                            break;
                                        case "15":
                                            Update.Date_15 = newValue;
                                            break;
                                        case "16":
                                            Update.Date_16 = newValue;
                                            break;
                                        case "17":
                                            Update.Date_17 = newValue;
                                            break;
                                        case "18":
                                            Update.Date_18 = newValue;
                                            break;
                                        case "19":
                                            Update.Date_19 = newValue;
                                            break;
                                        case "20":
                                            Update.Date_20 = newValue;
                                            break;
                                        case "21":
                                            Update.Date_21 = newValue;
                                            break;
                                        case "22":
                                            Update.Date_22 = newValue;
                                            break;
                                        case "23":
                                            Update.Date_23 = newValue;
                                            break;
                                        case "24":
                                            Update.Date_24 = newValue;
                                            break;
                                        case "25":
                                            Update.Date_25 = newValue;
                                            break;
                                        case "26":
                                            Update.Date_26 = newValue;
                                            break;
                                        case "27":
                                            Update.Date_27 = newValue;
                                            break;
                                        case "28":
                                            Update.Date_28 = newValue;
                                            break;
                                        case "29":
                                            Update.Date_29 = newValue;
                                            break;
                                        case "30":
                                            Update.Date_30 = newValue;
                                            break;
                                        case "31":
                                            Update.Date_31 = newValue;
                                            break;
                                    }
                                    DB.SubmitChanges();
                                    //return true;
                                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                    Record.Add(new RecordInv
                                    {
                                        BookingId = ReservationID,
                                        InvSid = arrFreesales[0].Sid.ToString(),
                                        SupplierId = objGlobalDefault.sid.ToString(),
                                        HotelCode = arrFreesales[0].HotelCode,
                                        RoomType = Room,
                                        RateType = arrFreesales[0].RateType,
                                        InvType = Invtype,
                                        Date = date,
                                        Month = month,
                                        Year = "",
                                        TotalAvlRoom = "",
                                        OldAvlRoom = room[1],
                                        NoOfBookedRoom = noOfRoom.ToString(),
                                        NoOfCancleRoom = "",
                                        UpdateDate = DateTime.Now,
                                        UpdateOn = "Booking",
                                    });

                                }

                                else if (room[0] == "fs" && room[1] != "")
                                {
                                    if (Convert.ToInt32(room[1]) >= noOfRoom)
                                    {
                                        var sold = Convert.ToInt32(room[2]) + noOfRoom;
                                        var AvlRoom = Convert.ToInt32(room[1]) - sold;
                                        string newValue = room[0] + "_" + AvlRoom + "_" + sold;
                                        Invtype = "Freesale";
                                        var TotalAvlRoom = Convert.ToInt32(AvlRoom) + sold;
                                        tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                                        switch (newdate)
                                        {

                                            case "1":
                                                Update.Date_1 = newValue;
                                                break;
                                            case "2":
                                                Update.Date_2 = newValue;
                                                break;
                                            case "3":
                                                Update.Date_3 = newValue;
                                                break;
                                            case "4":
                                                Update.Date_4 = newValue;
                                                break;
                                            case "5":
                                                Update.Date_5 = newValue;
                                                break;
                                            case "6":
                                                Update.Date_6 = newValue;
                                                break;
                                            case "7":
                                                Update.Date_7 = newValue;
                                                break;
                                            case "8":
                                                Update.Date_8 = newValue;
                                                break;
                                            case "9":
                                                Update.Date_9 = newValue;
                                                break;
                                            case "10":
                                                Update.Date_10 = newValue;
                                                break;
                                            case "11":
                                                Update.Date_11 = newValue;
                                                break;
                                            case "12":
                                                Update.Date_12 = newValue;
                                                break;
                                            case "13":
                                                Update.Date_13 = newValue;
                                                break;
                                            case "14":
                                                Update.Date_14 = newValue;
                                                break;
                                            case "15":
                                                Update.Date_15 = newValue;
                                                break;
                                            case "16":
                                                Update.Date_16 = newValue;
                                                break;
                                            case "17":
                                                Update.Date_17 = newValue;
                                                break;
                                            case "18":
                                                Update.Date_18 = newValue;
                                                break;
                                            case "19":
                                                Update.Date_19 = newValue;
                                                break;
                                            case "20":
                                                Update.Date_20 = newValue;
                                                break;
                                            case "21":
                                                Update.Date_21 = newValue;
                                                break;
                                            case "22":
                                                Update.Date_22 = newValue;
                                                break;
                                            case "23":
                                                Update.Date_23 = newValue;
                                                break;
                                            case "24":
                                                Update.Date_24 = newValue;
                                                break;
                                            case "25":
                                                Update.Date_25 = newValue;
                                                break;
                                            case "26":
                                                Update.Date_26 = newValue;
                                                break;
                                            case "27":
                                                Update.Date_27 = newValue;
                                                break;
                                            case "28":
                                                Update.Date_28 = newValue;
                                                break;
                                            case "29":
                                                Update.Date_29 = newValue;
                                                break;
                                            case "30":
                                                Update.Date_30 = newValue;
                                                break;
                                            case "31":
                                                Update.Date_31 = newValue;
                                                break;
                                        }
                                        DB.SubmitChanges();
                                        //return true;
                                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                        Record.Add(new RecordInv
                                        {
                                            BookingId = ReservationID,
                                            InvSid = arrFreesales[0].Sid.ToString(),
                                            SupplierId = objGlobalDefault.sid.ToString(),
                                            HotelCode = arrFreesales[0].HotelCode,
                                            RoomType = Room,
                                            RateType = arrFreesales[0].RateType,
                                            InvType = Invtype,
                                            Date = date,
                                            Month = month,
                                            Year = "",
                                            TotalAvlRoom = AvlRoom.ToString(),
                                            OldAvlRoom = TotalAvlRoom.ToString(),
                                            NoOfBookedRoom = noOfRoom.ToString(),
                                            NoOfCancleRoom = "",
                                            UpdateDate = DateTime.Now,
                                            UpdateOn = "Booking",
                                        });
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {

                                    //Response.Write("<script LANGUAGE='JavaScript' >alert('Login Successful')</script>");
                                    return false;
                                }
                                return true;
                            }
                        }
                        else if (arrFreesales[0] != null)
                        {
                            ListtoDataTable lsttodttt = new ListtoDataTable();
                            DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                            string newdatet = date.TrimStart('0');
                            date = "Date_" + newdate;
                            var Option = "Option_" + date;
                            string OptValue = dtt.Rows[0][Option].ToString();
                            string noTotalRoom = dtt.Rows[0][date].ToString();
                            Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                            string[] room = noTotalRoom.Split('_');

                            if (room[0] == "fs" && room[1] == "")
                            {
                                //string noRoomf = noRoomss.Split('_')[1];
                                //string Types = noRoomss.Split('_')[0];
                                //string Solds = noRoomss.Split('_')[2];
                                // noOfRoom
                                //  noRoom = (Convert.ToInt32(noRoomf) - noOfRoom).ToString();
                                var sold = Convert.ToInt32(room[2]) + noOfRoom;
                                string newValue = room[0] + "_" + room[1] + "_" + sold;
                                Invtype = "Freesale";
                                //var TotalAvlRoom = Convert.ToInt32(room[1]) - sold;
                                tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                                switch (newdate)
                                {

                                    case "1":
                                        Update.Date_1 = newValue;
                                        break;
                                    case "2":
                                        Update.Date_2 = newValue;
                                        break;
                                    case "3":
                                        Update.Date_3 = newValue;
                                        break;
                                    case "4":
                                        Update.Date_4 = newValue;
                                        break;
                                    case "5":
                                        Update.Date_5 = newValue;
                                        break;
                                    case "6":
                                        Update.Date_6 = newValue;
                                        break;
                                    case "7":
                                        Update.Date_7 = newValue;
                                        break;
                                    case "8":
                                        Update.Date_8 = newValue;
                                        break;
                                    case "9":
                                        Update.Date_9 = newValue;
                                        break;
                                    case "10":
                                        Update.Date_10 = newValue;
                                        break;
                                    case "11":
                                        Update.Date_11 = newValue;
                                        break;
                                    case "12":
                                        Update.Date_12 = newValue;
                                        break;
                                    case "13":
                                        Update.Date_13 = newValue;
                                        break;
                                    case "14":
                                        Update.Date_14 = newValue;
                                        break;
                                    case "15":
                                        Update.Date_15 = newValue;
                                        break;
                                    case "16":
                                        Update.Date_16 = newValue;
                                        break;
                                    case "17":
                                        Update.Date_17 = newValue;
                                        break;
                                    case "18":
                                        Update.Date_18 = newValue;
                                        break;
                                    case "19":
                                        Update.Date_19 = newValue;
                                        break;
                                    case "20":
                                        Update.Date_20 = newValue;
                                        break;
                                    case "21":
                                        Update.Date_21 = newValue;
                                        break;
                                    case "22":
                                        Update.Date_22 = newValue;
                                        break;
                                    case "23":
                                        Update.Date_23 = newValue;
                                        break;
                                    case "24":
                                        Update.Date_24 = newValue;
                                        break;
                                    case "25":
                                        Update.Date_25 = newValue;
                                        break;
                                    case "26":
                                        Update.Date_26 = newValue;
                                        break;
                                    case "27":
                                        Update.Date_27 = newValue;
                                        break;
                                    case "28":
                                        Update.Date_28 = newValue;
                                        break;
                                    case "29":
                                        Update.Date_29 = newValue;
                                        break;
                                    case "30":
                                        Update.Date_30 = newValue;
                                        break;
                                    case "31":
                                        Update.Date_31 = newValue;
                                        break;
                                }
                                DB.SubmitChanges();
                                //return true;
                                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                Record.Add(new RecordInv
                                {
                                    BookingId = ReservationID,
                                    InvSid = arrFreesales[0].Sid.ToString(),
                                    SupplierId = objGlobalDefault.sid.ToString(),
                                    HotelCode = arrFreesales[0].HotelCode,
                                    RoomType = Room,
                                    RateType = arrFreesales[0].RateType,
                                    InvType = Invtype,
                                    Date = date,
                                    Month = month,
                                    Year = "",
                                    TotalAvlRoom = "",
                                    OldAvlRoom = room[1],
                                    NoOfBookedRoom = noOfRoom.ToString(),
                                    NoOfCancleRoom = "",
                                    UpdateDate = DateTime.Now,
                                    UpdateOn = "Booking",
                                });

                            }

                            else if (room[0] == "fs" && room[1] != "")
                            {
                                if (Convert.ToInt32(room[1]) >= noOfRoom)
                                {
                                    var sold = Convert.ToInt32(room[2]) + noOfRoom;
                                    var AvlRoom = Convert.ToInt32(room[1]) - sold;
                                    string newValue = room[0] + "_" + AvlRoom + "_" + sold;
                                    Invtype = "Freesale";
                                    var TotalAvlRoom = Convert.ToInt32(AvlRoom) + sold;
                                    tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                                    switch (newdate)
                                    {

                                        case "1":
                                            Update.Date_1 = newValue;
                                            break;
                                        case "2":
                                            Update.Date_2 = newValue;
                                            break;
                                        case "3":
                                            Update.Date_3 = newValue;
                                            break;
                                        case "4":
                                            Update.Date_4 = newValue;
                                            break;
                                        case "5":
                                            Update.Date_5 = newValue;
                                            break;
                                        case "6":
                                            Update.Date_6 = newValue;
                                            break;
                                        case "7":
                                            Update.Date_7 = newValue;
                                            break;
                                        case "8":
                                            Update.Date_8 = newValue;
                                            break;
                                        case "9":
                                            Update.Date_9 = newValue;
                                            break;
                                        case "10":
                                            Update.Date_10 = newValue;
                                            break;
                                        case "11":
                                            Update.Date_11 = newValue;
                                            break;
                                        case "12":
                                            Update.Date_12 = newValue;
                                            break;
                                        case "13":
                                            Update.Date_13 = newValue;
                                            break;
                                        case "14":
                                            Update.Date_14 = newValue;
                                            break;
                                        case "15":
                                            Update.Date_15 = newValue;
                                            break;
                                        case "16":
                                            Update.Date_16 = newValue;
                                            break;
                                        case "17":
                                            Update.Date_17 = newValue;
                                            break;
                                        case "18":
                                            Update.Date_18 = newValue;
                                            break;
                                        case "19":
                                            Update.Date_19 = newValue;
                                            break;
                                        case "20":
                                            Update.Date_20 = newValue;
                                            break;
                                        case "21":
                                            Update.Date_21 = newValue;
                                            break;
                                        case "22":
                                            Update.Date_22 = newValue;
                                            break;
                                        case "23":
                                            Update.Date_23 = newValue;
                                            break;
                                        case "24":
                                            Update.Date_24 = newValue;
                                            break;
                                        case "25":
                                            Update.Date_25 = newValue;
                                            break;
                                        case "26":
                                            Update.Date_26 = newValue;
                                            break;
                                        case "27":
                                            Update.Date_27 = newValue;
                                            break;
                                        case "28":
                                            Update.Date_28 = newValue;
                                            break;
                                        case "29":
                                            Update.Date_29 = newValue;
                                            break;
                                        case "30":
                                            Update.Date_30 = newValue;
                                            break;
                                        case "31":
                                            Update.Date_31 = newValue;
                                            break;
                                    }
                                    DB.SubmitChanges();
                                    //return true;
                                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                    Record.Add(new RecordInv
                                    {
                                        BookingId = ReservationID,
                                        InvSid = arrFreesales[0].Sid.ToString(),
                                        SupplierId = objGlobalDefault.sid.ToString(),
                                        HotelCode = arrFreesales[0].HotelCode,
                                        RoomType = Room,
                                        RateType = arrFreesales[0].RateType,
                                        InvType = Invtype,
                                        Date = date,
                                        Month = month,
                                        Year = "",
                                        TotalAvlRoom = AvlRoom.ToString(),
                                        OldAvlRoom = TotalAvlRoom.ToString(),
                                        NoOfBookedRoom = noOfRoom.ToString(),
                                        NoOfCancleRoom = "",
                                        UpdateDate = DateTime.Now,
                                        UpdateOn = "Booking",
                                    });
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {

                                //Response.Write("<script LANGUAGE='JavaScript' >alert('Login Successful')</script>");
                                return false;
                            }
                            return true;
                        }


                    }

                    else if (arrFreesales[0] != null)
                    {
                        ListtoDataTable lsttodttt = new ListtoDataTable();
                        DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                        string newdatet = date.TrimStart('0');
                        date = "Date_" + newdate;
                        string noTotalRooms = dtt.Rows[0][date].ToString();
                        Int64 idd = Convert.ToInt64(dtt.Rows[0]["Sid"]);
                        string[] room = noTotalRooms.Split('_');
                        if (room[0] == "fs" && room[1] == "")
                        {
                            //string noRoomf = noRoomss.Split('_')[1];
                            //string Types = noRoomss.Split('_')[0];
                            //string Solds = noRoomss.Split('_')[2];
                            // noOfRoom
                            //  noRoom = (Convert.ToInt32(noRoomf) - noOfRoom).ToString();
                            var sold = Convert.ToInt32(room[2]) + noOfRoom;
                            string newValue = room[0] + "_" + room[1] + "_" + sold;
                            Invtype = "Freesale";
                            //var TotalAvlRoom = Convert.ToInt32(room[1]) - sold;
                            tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                            switch (newdate)
                            {

                                case "1":
                                    Update.Date_1 = newValue;
                                    break;
                                case "2":
                                    Update.Date_2 = newValue;
                                    break;
                                case "3":
                                    Update.Date_3 = newValue;
                                    break;
                                case "4":
                                    Update.Date_4 = newValue;
                                    break;
                                case "5":
                                    Update.Date_5 = newValue;
                                    break;
                                case "6":
                                    Update.Date_6 = newValue;
                                    break;
                                case "7":
                                    Update.Date_7 = newValue;
                                    break;
                                case "8":
                                    Update.Date_8 = newValue;
                                    break;
                                case "9":
                                    Update.Date_9 = newValue;
                                    break;
                                case "10":
                                    Update.Date_10 = newValue;
                                    break;
                                case "11":
                                    Update.Date_11 = newValue;
                                    break;
                                case "12":
                                    Update.Date_12 = newValue;
                                    break;
                                case "13":
                                    Update.Date_13 = newValue;
                                    break;
                                case "14":
                                    Update.Date_14 = newValue;
                                    break;
                                case "15":
                                    Update.Date_15 = newValue;
                                    break;
                                case "16":
                                    Update.Date_16 = newValue;
                                    break;
                                case "17":
                                    Update.Date_17 = newValue;
                                    break;
                                case "18":
                                    Update.Date_18 = newValue;
                                    break;
                                case "19":
                                    Update.Date_19 = newValue;
                                    break;
                                case "20":
                                    Update.Date_20 = newValue;
                                    break;
                                case "21":
                                    Update.Date_21 = newValue;
                                    break;
                                case "22":
                                    Update.Date_22 = newValue;
                                    break;
                                case "23":
                                    Update.Date_23 = newValue;
                                    break;
                                case "24":
                                    Update.Date_24 = newValue;
                                    break;
                                case "25":
                                    Update.Date_25 = newValue;
                                    break;
                                case "26":
                                    Update.Date_26 = newValue;
                                    break;
                                case "27":
                                    Update.Date_27 = newValue;
                                    break;
                                case "28":
                                    Update.Date_28 = newValue;
                                    break;
                                case "29":
                                    Update.Date_29 = newValue;
                                    break;
                                case "30":
                                    Update.Date_30 = newValue;
                                    break;
                                case "31":
                                    Update.Date_31 = newValue;
                                    break;
                            }
                            DB.SubmitChanges();
                            //return true;
                            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                            Record.Add(new RecordInv
                            {
                                BookingId = ReservationID,
                                InvSid = arrFreesales[0].Sid.ToString(),
                                SupplierId = objGlobalDefault.sid.ToString(),
                                HotelCode = arrFreesales[0].HotelCode,
                                RoomType = Room,
                                RateType = arrFreesales[0].RateType,
                                InvType = Invtype,
                                Date = date,
                                Month = month,
                                Year = "",
                                TotalAvlRoom = "",
                                OldAvlRoom = room[1],
                                NoOfBookedRoom = noOfRoom.ToString(),
                                NoOfCancleRoom = "",
                                UpdateDate = DateTime.Now,
                                UpdateOn = "Booking",
                            });

                        }

                        else if (room[0] == "fs" && room[1] != "")
                        {
                            if (Convert.ToInt32(room[1]) >= noOfRoom)
                            {
                                var sold = Convert.ToInt32(room[2]) + noOfRoom;
                                string newValue = room[0] + "_" + room[1] + "_" + sold;
                                var AvlRoom = Convert.ToInt32(room[1]) - sold;
                                Invtype = "Freesale";
                                var TotalAvlRoom = Convert.ToInt32(room[1]) + sold;
                                tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == idd);

                                switch (newdate)
                                {

                                    case "1":
                                        Update.Date_1 = newValue;
                                        break;
                                    case "2":
                                        Update.Date_2 = newValue;
                                        break;
                                    case "3":
                                        Update.Date_3 = newValue;
                                        break;
                                    case "4":
                                        Update.Date_4 = newValue;
                                        break;
                                    case "5":
                                        Update.Date_5 = newValue;
                                        break;
                                    case "6":
                                        Update.Date_6 = newValue;
                                        break;
                                    case "7":
                                        Update.Date_7 = newValue;
                                        break;
                                    case "8":
                                        Update.Date_8 = newValue;
                                        break;
                                    case "9":
                                        Update.Date_9 = newValue;
                                        break;
                                    case "10":
                                        Update.Date_10 = newValue;
                                        break;
                                    case "11":
                                        Update.Date_11 = newValue;
                                        break;
                                    case "12":
                                        Update.Date_12 = newValue;
                                        break;
                                    case "13":
                                        Update.Date_13 = newValue;
                                        break;
                                    case "14":
                                        Update.Date_14 = newValue;
                                        break;
                                    case "15":
                                        Update.Date_15 = newValue;
                                        break;
                                    case "16":
                                        Update.Date_16 = newValue;
                                        break;
                                    case "17":
                                        Update.Date_17 = newValue;
                                        break;
                                    case "18":
                                        Update.Date_18 = newValue;
                                        break;
                                    case "19":
                                        Update.Date_19 = newValue;
                                        break;
                                    case "20":
                                        Update.Date_20 = newValue;
                                        break;
                                    case "21":
                                        Update.Date_21 = newValue;
                                        break;
                                    case "22":
                                        Update.Date_22 = newValue;
                                        break;
                                    case "23":
                                        Update.Date_23 = newValue;
                                        break;
                                    case "24":
                                        Update.Date_24 = newValue;
                                        break;
                                    case "25":
                                        Update.Date_25 = newValue;
                                        break;
                                    case "26":
                                        Update.Date_26 = newValue;
                                        break;
                                    case "27":
                                        Update.Date_27 = newValue;
                                        break;
                                    case "28":
                                        Update.Date_28 = newValue;
                                        break;
                                    case "29":
                                        Update.Date_29 = newValue;
                                        break;
                                    case "30":
                                        Update.Date_30 = newValue;
                                        break;
                                    case "31":
                                        Update.Date_31 = newValue;
                                        break;
                                }
                                DB.SubmitChanges();
                                //return true;
                                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                Record.Add(new RecordInv
                                {
                                    BookingId = ReservationID,
                                    InvSid = arrFreesales[0].Sid.ToString(),
                                    SupplierId = objGlobalDefault.sid.ToString(),
                                    HotelCode = arrFreesales[0].HotelCode,
                                    RoomType = Room,
                                    RateType = arrFreesales[0].RateType,
                                    InvType = Invtype,
                                    Date = date,
                                    Month = month,
                                    Year = "",
                                    TotalAvlRoom = AvlRoom.ToString(),
                                    OldAvlRoom = TotalAvlRoom.ToString(),
                                    NoOfBookedRoom = noOfRoom.ToString(),
                                    NoOfCancleRoom = "",
                                    UpdateDate = DateTime.Now,
                                    UpdateOn = "Booking",
                                });
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {

                            //Response.Write("<script LANGUAGE='JavaScript' >alert('Login Successful')</script>");
                            return false;
                        }

                    }

                    else if (arrAllotmentt[0] != null)
                    {
                        ListtoDataTable lsttodt = new ListtoDataTable();
                        DataTable dt = lsttodt.ToDataTable(arrAllotmentt);

                        date = "Date_" + newdate;
                        string noRooms = dt.Rows[0][date].ToString();
                        Int64 id = Convert.ToInt64(dt.Rows[0]["Sid"]);
                        if (noRooms != "")
                        {
                            string noTotalRooms = noRooms.Split('_')[1];
                            string Type = noRooms.Split('_')[0];
                            string Sold = noRooms.Split('_')[2];
                            // decimal avlroom = Convert.ToInt32(noTotalRooms) - Convert.ToInt32(Sold);
                            if (noTotalRooms != "0")
                            {

                                decimal noOfSold = Convert.ToInt32(Sold) + noOfRoom;
                                var noRoom = (Convert.ToInt32(noTotalRooms) - noOfRoom).ToString();

                                string newValue = Type + "_" + noRoom + "_" + noOfSold;
                                decimal OldAvlRoom = Convert.ToInt32(noOfSold) + Convert.ToInt32(noRoom);

                                //var noRoom = (Convert.ToInt32(noTotalRooms) - noOfRoom).ToString();
                                //string newValue = Type + "_" + noTotalRooms + "_" + noRoom;


                                tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventories.Single(x => x.Sid == id);

                                switch (newdate)
                                {

                                    case "1":
                                        Update.Date_1 = newValue;
                                        break;
                                    case "2":
                                        Update.Date_2 = newValue;
                                        break;
                                    case "3":
                                        Update.Date_3 = newValue;
                                        break;
                                    case "4":
                                        Update.Date_4 = newValue;
                                        break;
                                    case "5":
                                        Update.Date_5 = newValue;
                                        break;
                                    case "6":
                                        Update.Date_6 = newValue;
                                        break;
                                    case "7":
                                        Update.Date_7 = newValue;
                                        break;
                                    case "8":
                                        Update.Date_8 = newValue;
                                        break;
                                    case "9":
                                        Update.Date_9 = newValue;
                                        break;
                                    case "10":
                                        Update.Date_10 = newValue;
                                        break;
                                    case "11":
                                        Update.Date_11 = newValue;
                                        break;
                                    case "12":
                                        Update.Date_12 = newValue;
                                        break;
                                    case "13":
                                        Update.Date_13 = newValue;
                                        break;
                                    case "14":
                                        Update.Date_14 = newValue;
                                        break;
                                    case "15":
                                        Update.Date_15 = newValue;
                                        break;
                                    case "16":
                                        Update.Date_16 = newValue;
                                        break;
                                    case "17":
                                        Update.Date_17 = newValue;
                                        break;
                                    case "18":
                                        Update.Date_18 = newValue;
                                        break;
                                    case "19":
                                        Update.Date_19 = newValue;
                                        break;
                                    case "20":
                                        Update.Date_20 = newValue;
                                        break;
                                    case "21":
                                        Update.Date_21 = newValue;
                                        break;
                                    case "22":
                                        Update.Date_22 = newValue;
                                        break;
                                    case "23":
                                        Update.Date_23 = newValue;
                                        break;
                                    case "24":
                                        Update.Date_24 = newValue;
                                        break;
                                    case "25":
                                        Update.Date_25 = newValue;
                                        break;
                                    case "26":
                                        Update.Date_26 = newValue;
                                        break;
                                    case "27":
                                        Update.Date_27 = newValue;
                                        break;
                                    case "28":
                                        Update.Date_28 = newValue;
                                        break;
                                    case "29":
                                        Update.Date_29 = newValue;
                                        break;
                                    case "30":
                                        Update.Date_30 = newValue;
                                        break;
                                    case "31":
                                        Update.Date_31 = newValue;
                                        break;
                                }
                                DB.SubmitChanges();
                                //return true;
                                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                                Record.Add(new RecordInv
                                {
                                    BookingId = ReservationID,
                                    InvSid = arrAllotmentt[0].Sid.ToString(),
                                    SupplierId = objGlobalDefault.sid.ToString(),
                                    HotelCode = arrAllotmentt[0].HotelCode,
                                    RoomType = Room,
                                    RateType = arrAllotmentt[0].RateType,
                                    InvType = Invtype,
                                    Date = date,
                                    Month = month,
                                    Year = "",
                                    TotalAvlRoom = noRoom,
                                    OldAvlRoom = OldAvlRoom.ToString(),
                                    NoOfBookedRoom = noOfRoom.ToString(),
                                    NoOfCancleRoom = "",
                                    UpdateDate = DateTime.Now,
                                    UpdateOn = "Booking",
                                });
                            }
                            else
                                return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateInvOnCancellation(string ReservationID)
        {
            try
            {
                //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                using (var DB = new dbHotelhelperDataContext())
                {
                    Record = new List<RecordInv>();
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Uid = objGlobalDefault.sid;
                    if (objGlobalDefault.UserType != "Supplier")
                        Uid = objGlobalDefault.ParentId;
                    var InvRecord = (from obj in DB.tbl_CommonInventoryRecords where obj.BookingId == ReservationID && obj.SupplierId == Convert.ToString(Uid) select obj).ToList();
                    for (int i = 0; i < InvRecord.Count; i++)
                    {
                        tbl_CommonHotelInventory InvList = DB.tbl_CommonHotelInventories.Where(x => x.Sid == Convert.ToInt64(InvRecord[i].InvSid)).FirstOrDefault();
                        string BackRoom = InvRecord[i].NoOfBookedRoom;
                        var ValueInv = "";
                        string[] NewValue;
                        Int64 UpValue = 0;
                        switch (InvRecord[i].Date)
                        {

                            case "Date_1":
                                ValueInv = InvList.Date_1;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) + Convert.ToInt64(BackRoom);
                                InvList.Date_1 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_2":
                                ValueInv = InvList.Date_2;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_2 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_3":
                                ValueInv = InvList.Date_3;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_3 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_4":
                                ValueInv = InvList.Date_4;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_4 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_5":
                                ValueInv = InvList.Date_5;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_5 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_6":
                                ValueInv = InvList.Date_6;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_6 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_7":
                                ValueInv = InvList.Date_7;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_7 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_8":
                                ValueInv = InvList.Date_8;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_8 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_9":
                                ValueInv = InvList.Date_9;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_9 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_10":
                                ValueInv = InvList.Date_10;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_10 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_11":
                                ValueInv = InvList.Date_11;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_11 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_12":
                                ValueInv = InvList.Date_12;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_12 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_13":
                                ValueInv = InvList.Date_13;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_13 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_14":
                                ValueInv = InvList.Date_14;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_14 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_15":
                                ValueInv = InvList.Date_15;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_15 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_16":
                                ValueInv = InvList.Date_16;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_16 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_17":
                                ValueInv = InvList.Date_17;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_17 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_18":
                                ValueInv = InvList.Date_18;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_18 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_19":
                                ValueInv = InvList.Date_19;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_19 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_20":
                                ValueInv = InvList.Date_20;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_20 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_21":
                                ValueInv = InvList.Date_21;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_21 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_22":
                                ValueInv = InvList.Date_22;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_22 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_23":
                                ValueInv = InvList.Date_23;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_23 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_24":
                                ValueInv = InvList.Date_24;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_24 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_25":
                                ValueInv = InvList.Date_25;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_25 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_26":
                                ValueInv = InvList.Date_26;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_26 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_27":
                                ValueInv = InvList.Date_27;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_27 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_28":
                                ValueInv = InvList.Date_28;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_28 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_29":
                                ValueInv = InvList.Date_29;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_29 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_30":
                                ValueInv = InvList.Date_30;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_30 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                            case "Date_31":
                                ValueInv = InvList.Date_31;
                                NewValue = ValueInv.Split('_');
                                UpValue = Convert.ToInt64(NewValue[2]) - Convert.ToInt64(BackRoom);
                                InvList.Date_31 = NewValue[0] + "_" + NewValue[1] + "_" + UpValue;
                                break;
                        }
                        DB.SubmitChanges();

                        Record.Add(new RecordInv
                        {
                            BookingId = ReservationID,
                            InvSid = InvRecord[i].InvSid.ToString(),
                            SupplierId = Uid.ToString(),
                            HotelCode = InvRecord[i].HotelCode,
                            RoomType = InvRecord[i].RoomType,
                            RateType = InvList.RateType,
                            InvType = InvList.InventoryType,
                            Date = InvRecord[i].Date,
                            Month = InvRecord[i].Month,
                            Year = "",
                            TotalAvlRoom = ValueInv.Split('_')[1],
                            OldAvlRoom = ValueInv.Split('_')[2],
                            NoOfBookedRoom = "",
                            NoOfCancleRoom = BackRoom,
                            UpdateDate = DateTime.Now,
                            UpdateOn = "Cancle Booking",
                        });
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Int64 GetInventoryCount(string HotelID, Decimal RateID, string RoomID, string Date,Int64 Supplier)
        {
            Int64 Count = 0;
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Uid = objGlobalDefault.sid;
                    if (objGlobalDefault.UserType != "Supplier")
                        Uid = objGlobalDefault.ParentId;
                    var Type = (from obj in DB.tbl_commonRoomRates where obj.HotelRateID == Convert.ToInt64(RateID) && obj.SupplierId ==Supplier select obj.Type).FirstOrDefault();
                    var dt = Date.Split('-')[0];
                    var mn = Date.Split('-')[1];
                    var yr = Date.Split('-')[2];

                    List<AlllistInventory> Inventory = new List<AlllistInventory>();
                    List<string> ListMonth = new List<string>();
                    List<string> ListDate = new List<string>();

                    if (!ListMonth.Exists(d => d.Contains(mn)))
                    {
                        var listInventory = (from obj in DB.tbl_CommonHotelInventories where obj.HotelCode == HotelID && obj.RoomType == RoomID && obj.Month == mn && obj.Year == yr && obj.RateType == Type && obj.SupplierId == Supplier.ToString() select obj).ToList();

                        Inventory.Add(new AlllistInventory
                        {
                            List_AllInventory = listInventory,
                        });
                    }

                    ListMonth.Add(mn);

                    for (int k = 0; k < Inventory.Count; k++)
                    {
                        foreach (var item in Inventory[k].List_AllInventory)
                        {
                            if (item.InventoryType == "Allocation")
                            {
                                Allocation.Add(item);
                            }
                            else if (item.InventoryType == "FreeSale")
                            {
                                Freesale.Add(item);
                            }
                            else if (item.InventoryType == "Allotment")
                            {
                                Allotment.Add(item);
                            }
                        }
                    }

                    #region MySWitch

                    switch (dt)
                    {

                        case "01":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "02":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "03":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "04":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "05":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "06":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "07":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "08":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "09":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "10":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "11":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "12":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "13":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "14":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "15":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "16":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "17":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "18":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "19":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "20":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "21":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "22":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "23":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "24":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "25":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "26":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "27":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "28":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "29":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "30":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                        case "31":
                            Count = GetCount(dt, mn, HotelID, RoomID, Type);
                            break;
                    }

                    #endregion

                    return Count;
                }
            }
            catch
            {
                return Count;
            }
        }

        public static Int64 GetCount(string date, string Month, string HotelID, string RoomID, string Type)
        {
            Int64 Count=0;
            try
            {
                List<tbl_CommonHotelInventory> arrAllocationn = new List<tbl_CommonHotelInventory>();
                List<tbl_CommonHotelInventory> arrAllotmentt = new List<tbl_CommonHotelInventory>();
                List<tbl_CommonHotelInventory> arrFreesales = new List<tbl_CommonHotelInventory>();
                arrAllocationn.Add(Allocation.Where(d => d.Month == Month && d.RoomType == RoomID).FirstOrDefault());
                arrAllotmentt.Add(Allotment.Where(d => d.Month == Month && d.RoomType == RoomID).FirstOrDefault());
                arrFreesales.Add(Freesale.Where(d => d.Month == Month && d.RoomType == RoomID).FirstOrDefault());
                string newdate = date.TrimStart('0');

                if (arrAllocationn[0] != null)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(arrAllocationn);

                    date = "Date_" + newdate;
                    string noRooms = dt.Rows[0][date].ToString();
                    if (noRooms != "")
                    {
                        string noTotalRooms = noRooms.Split('_')[1];
                        if (noTotalRooms != "0")
                        {
                             Count = Count + Convert.ToInt64(noTotalRooms);
                        }
                    }
                    
                }

                if (arrFreesales[0] != null)
                {
                    ListtoDataTable lsttodttt = new ListtoDataTable();
                    DataTable dtt = lsttodttt.ToDataTable(arrFreesales);
                    string newdatet = date.TrimStart('0');
                    date = "Date_" + newdate;
                    string noTotalRooms = dtt.Rows[0][date].ToString();
                    string[] room = noTotalRooms.Split('_');
                    if (room[0] == "fs" && room[1] == "")
                    {
                        Count = Count + 100;
                      
                    }
                    else if (room[0] == "fs" && room[1] != "")
                    {
                        Count = Count + Convert.ToInt64(room[1]);
                    }

                    else
                    {
                        Count = Count + 0;
                    }

                }

                if (arrAllotmentt[0] != null)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(arrAllotmentt);

                    date = "Date_" + newdate;
                    string noRooms = dt.Rows[0][date].ToString();
                    Int64 id = Convert.ToInt64(dt.Rows[0]["Sid"]);
                    if (noRooms != "")
                    {
                        string noTotalRooms = noRooms.Split('_')[1];
                        //  decimal avlroom = Convert.ToInt32(noTotalRooms) - Convert.ToInt32(Sold);
                        if (noTotalRooms != "0")
                        {
                            Count = Count + Convert.ToInt64(noTotalRooms);
                        }
                        else
                        {
                            Count = Count + 0;
                        }
                    }
                    else
                        Count = Count + 0;

                }
               

                return Count;
            }
            catch
            {
                return Count;
            }
        }
    }
}