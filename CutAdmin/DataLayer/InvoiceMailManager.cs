﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class InvoiceMailManager
    {
        public static string Invoice { get; set; }
        public static string Status { get; set; }

        public static string Subject { get; set; }

        public static string MailTo { get; set; }

        public static string Service { get; set; }

        public static string MailMessage { get; set; }

        #region Mail Body
        public static void ConfromationMsg(string Agency, string PaxName, string BookingDate, string BookingAmt, string Curruncy)
        {

            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            StringBuilder sb = new StringBuilder();
            if (Service == "Visa" || Service == "OTB")
            {
                Subject = "New " + Service + " has been requested against Invoice " + Invoice;
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\"><head>  ");
                sb.Append("<meta charset=\"utf-8\">  ");
                sb.Append("</head>  ");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
                sb.Append("<div height:=\"\" 5px\"=\"\">  ");
                sb.Append("</div>  ");
                sb.Append("<div style=\"height:50px;width:auto\">  ");
                sb.Append("<br>  ");
                sb.Append("<img src=\"" + URL + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\">  ");
                sb.Append("</div>  ");
                sb.Append("<img src=\"" + URL + "/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\">  ");
                sb.Append("<div>  ");
                sb.Append("<div style=\"margin-left:5%\">  ");
                sb.Append("<p> <span style=\"margin-left:2%;font-weight:400\">Hello , " + Agency + ",Your Booking for " + Service + "  is under process.Kindly check attachment Invoice.</span><br></p>  ");
                sb.Append("<p><b>Booking Details:</b><table>");
                sb.Append("  <tbody><tr><td><b>Service:</b></td><td>");
                sb.Append(" " + Service + "</td></tr>");
                sb.Append("<tr><td><b>Passenger Name:</b></td><td>" + PaxName + "</td></tr>");
                sb.Append("<tr><td><b>Booking Date:</b></td><td>" + BookingDate + "</td></tr>");
                sb.Append("<tr><td><b>Booking Amount:</b></td><td>" + Curruncy + " " + BookingAmt + "/-</td></tr>");
                sb.Append("</tbody></table>");
                sb.Append("<br></p>  ");
                sb.Append("<p><span style=\"\\&quot;margin-left:2%;font-weight:400\\&quot;\">For any further clarification &amp; query kindly feel free to contact us.</span><br></p>  ");
                sb.Append("<span style=\"\\&quot;margin-left:2%\\&quot;\">  ");
                sb.Append("<b>Thank You,</b><br>  ");
                sb.Append("</span>  ");
                sb.Append("<span style=\"\\&quot;margin-left:2%\\&quot;\">  ");
                sb.Append("Administrator  ");
                sb.Append("</span>  ");
                sb.Append("</div>  ");
                sb.Append("</div>  ");
                sb.Append("<img src=\"" + URL + "/images/unnamed.jpg\" style=\"width:100%;height:auto\">  ");
                sb.Append("<div>  ");
                sb.Append("</div>  ");
                sb.Append("</body></html>");

                MailMessage = sb.ToString();

            }
        }
        #endregion


        #region Bank Details
        public static string GetBankDetails()
        {
            DataTable dtResult; string BankDetails = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.ParentId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_BankDetailsLoad", out dtResult, sqlParams);
            StringBuilder sb = new StringBuilder();
            sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
            sb.Append("<span style=\"padding-left: 10px; color: #57585A\"><b>Bank Details</b></span>");
            sb.Append("</div>");
            sb.Append("<table border=\"1\" style=\"margin-top: 3px; height:100px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-left: none; border-right: none\">");
            sb.Append("<tbody><tr style=\"border: none; background-color: #F7B85B; border-bottom-color: gray; color: #57585A;\">");
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                foreach (DataRow Row in dtResult.Rows)
                {
                    sb.Append("<td align=\"left\" style=\"width: 150px; height: 35px; font-size: 15px; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;\">");
                    sb.Append("<span>");
                    sb.Append("<b>Bank Name </b><br>");
                    sb.Append("<b>Account No </b><br>");
                    sb.Append("<b>Branch   </b><br>");
                    sb.Append("<b>Swift Code   </b><br>");
                    sb.Append("</span>");
                    sb.Append("</td>");
                    sb.Append("<td align=\"left\" style=\" width: 300px; height: 35px; font-size: 15px; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray; font-weight: 500\">");
                    sb.Append(": &nbsp;&nbsp;<span>" + Row["BankName"].ToString() + "</span><br>");
                    sb.Append(": &nbsp;&nbsp;<span>" + Row["AccountNo"].ToString() + "</span><br>");
                    sb.Append(": &nbsp;&nbsp;<span>" + Row["Branch"].ToString() + "</span><br>");
                    sb.Append(": &nbsp;&nbsp;<span>" + Row["SwiftCode"].ToString() + "</span><br>");
                    sb.Append("</td>");

                }
            }
            sb.Append("</tr>");
            sb.Append("</tbody></table>");


            return BankDetails = sb.ToString();
        }

        #endregion


        #region Agency Details
        public static string GetInvDetails()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
            sb.Append("<tbody>");
            sb.Append("<tr><td rowspan=\"3\" style=\"width:35%; color: #00CCFF; font-size: 30px; text-align: center; border: none; padding-right:35px; padding-left:10px\"><b>SALES INVOICE</b>");
            sb.Append("<br><span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span></td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px; color: #57585A\">");
            sb.Append("<b> Invoice Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b><span style=\"color: #757575\"> " + DateTime.Now.ToString("dd-MM-yyyy") + " </span></td>");
            sb.Append("<td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px; color: #57585A\">");
            sb.Append("<b>Invoice No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;     : </b><span style=\"color: #757575\">" + Invoice + " </span></td></tr><tr><td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
            sb.Append("<b> Voucher No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + Invoice.Replace("INV", "VCH") + "</span></td><td style=\"border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
            sb.Append("<b>  Agent Code &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\" color:#757575\">" + objGlobalDefault.Agentuniquecode + "</span></td></tr><tr><td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            return sb.ToString();
        }

        #endregion

        #region Booking Details
        public static string BookingDetails()
        {
            StringBuilder sb = new StringBuilder();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            #region Visa Booking Details
            if (Service == "Visa" || Service == "OTB")
            {
                DataSet ds; DataTable dtSource, dtBookingTransactions, dtAgentDetail;
                if (Service == "Visa")
                {
                    DBHelper.DBReturnCode retcode = VisaDetailsManager.VisaInvoiceDetails(Invoice, 0, out ds);
                    dtSource = ds.Tables[0];
                    dtBookingTransactions = ds.Tables[1];
                    dtAgentDetail = ds.Tables[2];
                }
                else
                {
                    DBHelper.DBReturnCode retcode = OTBManager.OTBInvoiceDetails(Invoice, 0, out ds);
                    dtSource = ds.Tables[0];
                    dtBookingTransactions = ds.Tables[1];
                    dtAgentDetail = ds.Tables[2];
                }

                string CurrencyClass = "";
                sb.Append("<table border=\"1\" style=\"border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC\">");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A\"><b>Invoice To</b></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"6\" style=\"width: 10%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px;color: #57585A;font-weight:900\">");
                sb.Append("<span style=\"padding-left: 10px\">Name</span><br>");
                sb.Append("<span style=\"padding-left:10px\">Address</span><br>");
                sb.Append("<span style=\"padding-left:10px\">City</span><br>");
                sb.Append("<span style=\"padding-left:10px\">Country</span><br>");
                sb.Append("</td>");
                sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A; font-weight:500\">");
                sb.Append(": <span id=\"spnName\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["AgencyName"].ToString() + "</span><br>");
                sb.Append(":<span id=\"spnAdd\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["Address"].ToString() + "</span><br>");
                sb.Append(":<span id=\"spnCity\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["Description"].ToString() + "</span><br>");
                sb.Append(":<span id=\"spnCountry\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["Countryname"].ToString() + "</span><br>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td rowspan=\"6\" style=\"width: 10%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; color: #57585A;font-weight:900\">");
                sb.Append("<span style=\"padding-left:10px\">Phone</span><br>");
                sb.Append("<span style=\"padding-left:10px\">Email</span><br>");
                sb.Append("<span style=\"padding-left:10px\">Agency Code</span><br>");
                sb.Append("</td>");
                sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A; font-weight:500\">");
                sb.Append(":<span id=\"spnPhone\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["phone"].ToString() + "</span><br>");
                sb.Append(":<span id=\"spnemail\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["email"].ToString() + "</span><br>");
                sb.Append(":<span id=\"spnCountry\" style=\"padding-left:10px\"></span>");
                sb.Append("<span id=\"spnAgency\" style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["Agentuniquecode"].ToString() + "</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<div style=\"font-size: 20px;padding-bottom:10px; padding-top:8px\">");
                sb.Append("<span style=\"padding-left: 10px; color: #57585A\"><b>Rate</b></span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table id=\"tbl_Rate\" border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                sb.Append("<tbody>");
                sb.Append("<tr style=\"border: none;  \">");
                sb.Append("<td style=\"background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px;width: 30%; \">");
                if (Service == "Visa")
                    sb.Append("<span>Service Details</span>");
                else
                    sb.Append("<span>Airline</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                sb.Append("<span>Passenger Name</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center\">");
                sb.Append("    <span>Document Number</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                sb.Append("    <span>Fees</span>");
                sb.Append("</td>");
                sb.Append("</tr>");
                if (Service == "Visa")
                {
                    sb.Append("<tr>");
                    sb.Append("<td style=\"height: 35px; font-size: 15px; border: none;font-weight: 700;/* text-align:center; */\">");
                    sb.Append("<span id=\"Type\" style=\"margin-left: 0.5em;color: #57585A;\">" + dtSource.Rows[0]["IeService"] + "</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"height: 35px; font-size: 15px; border: none; font-weight: 700;\">");
                    sb.Append("<span id=\"PassengerName\" style=\"color: #57585A;\">" + dtSource.Rows[0]["FirstName"] + "  " + dtSource.Rows[0]["LastName"] + "</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                    sb.Append("<span id=\"Service\" style=\"color: #57585A;\">" + dtSource.Rows[0]["PassportNo"] + "</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"width: 100px; height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                    sb.Append("<span id=\"Total\" style=\"color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["TotalAmount"])) + "</span>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    string Total = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["TotalAmount"]));
                    string Agency = dtAgentDetail.Rows[0]["AgencyName"].ToString();
                    string PaxName = dtSource.Rows[0]["FirstName"] + "  " + dtSource.Rows[0]["LastName"];
                    MailTo = dtAgentDetail.Rows[0]["email"].ToString();
                    ConfromationMsg(Agency, PaxName, DateTime.Now.ToString("dd-MM-yyyy"), Total, dtAgentDetail.Rows[0]["CurrencyCode"].ToString());
                }
                else
                {
                    foreach (DataRow Row in dtSource.Rows)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td style=\"height: 35px; font-size: 15px; border: none;font-weight: 700;/* text-align:center; */\">");
                        sb.Append("<span id=\"Type\" style=\"margin-left: 0.5em;color: #57585A;\">" + Row["In_AirLine"] + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"height: 35px; font-size: 15px; border: none; font-weight: 700;\">");
                        sb.Append("<span id=\"PassengerName\" style=\"color: #57585A;\">" + Row["Name"] + "  " + Row["LastName"] + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span id=\"Service\" style=\"color: #57585A;\">" + Row["VisaNo"] + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"width: 100px; height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span id=\"Total\" style=\"color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(Row["VisaFee"])) + "</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    // Genrate Email  // 
                    string Total = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["Total"]));
                    string Agency = dtAgentDetail.Rows[0]["AgencyName"].ToString();
                    string PaxName = dtSource.Rows[0]["Name"] + "  " + dtSource.Rows[0]["LastName"].ToString() + "*(Pax- " + dtSource.Rows.Count.ToString() + ")";
                    MailTo = dtAgentDetail.Rows[0]["email"].ToString();
                    ConfromationMsg(Agency, PaxName, DateTime.Now.ToString("dd-MM-yyyy"), Total, dtAgentDetail.Rows[0]["CurrencyCode"].ToString());
                }

                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<div style=\"margin-left: 80%;\">");
                if (Service == "Visa")
                {
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Urgent Charges</span> :<span id=\"UrgenCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["UrgentFee"])) + "</span><br>");
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Other Charges</span> :<span id=\"OtherCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["OtherFee"])) + "</span><br>");
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Service Charges</span> :<span id=\"ServicesCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["ServiceTax"])) + "</span>");
                }
                else
                {
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Urgent Charges</span> :<span id=\"UrgenCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["UrgentFee"])) + "</span><br>");
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Other Charges</span> :<span id=\"OtherCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(0)) + "</span><br>");
                    sb.Append("<span style=\"margin-left: 0.5em; color: #57585A; font-weight: 900;\">Service Charges</span> :<span id=\"ServicesCharges\" style=\"margin-left: 0.5em;color: #57585A;\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["ServiceCharge"]) * dtSource.Rows.Count) + "</span>");
                }

                sb.Append("</div>");
                sb.Append("<table id=\"tbl_Rate\" border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                sb.Append("<tbody>");
                sb.Append("<tr style=\"border: none;  \">");
                sb.Append("<td style=\"background-color: #35C2F1;color: white;font-size: 15px;border: none;font-weight: 700;text-align:left;padding-left:10px;\">");
                sb.Append("<span>In - Words</span> :");
                sb.Append("");
                if (objGlobalDefault.UserType == "Agent")
                    CurrencyClass = (HttpContext.Current.Session["CurrencyClass"]).ToString();
                else
                    CurrencyClass = dtAgentDetail.Rows[0]["CurrencyCode"].ToString();
                if (Service == "Visa")
                    sb.Append("<label style=\"font-size: 15px;/* margin-left: 100px; */\" id=\"lblAmmountinwords\">" + ToWords(Convert.ToDecimal(dtSource.Rows[0]["TotalAmount"]), CurrencyClass) + "</label>");
                else
                    sb.Append("<label style=\"font-size: 15px;/* margin-left: 100px; */\" id=\"lblAmmountinwords\">" + ToWords(Convert.ToDecimal(dtSource.Rows[0]["Total"]), CurrencyClass) + "</label>");
                sb.Append("</td>");
                sb.Append("");
                sb.Append("");
                sb.Append("<td style=\"background-color: #35C2F1;color: white;font-size: 20px;border: none;font-weight: 700;/* text-align:left; *//* padding-left:10px; */width: 20%;\">");
                sb.Append("<span>Total </span><span>Ammount :</span>");
                sb.Append("</td>");
                sb.Append("<td style=\"background-color: #35C2F1; color: white;font-size: 20px;border: none;font-weight: 700;/* text-align:left; *//* padding-left:10px; */\">");
                if (Service == "Visa")
                    sb.Append("<span id=\"spnRealAmt\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["TotalAmount"])) + "</span>");
                else
                    sb.Append("<span id=\"spnRealAmt\">" + String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(dtSource.Rows[0]["Total"])) + "</span>");
                sb.Append("<br>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div>");
            }
            #endregion Visa Booking Details

            #region Otb Booking Details

            #endregion


            return sb.ToString();

        }
        #endregion


        #region Create Invoice
        public static bool SendInvoiceMail()
        {

            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string rootPath = Convert.ToString(ConfigurationManager.AppSettings["rootPath"]);

            try
            {
                CheckIfProcessAlreadyRunning();
                // BuildMyString.com generated code. Please enjoy your string responsibly.
                //Invoice = InvoiceNo;
                // Service = "OTB";
                StringBuilder sb = new StringBuilder();
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
                sb.Append("    ");
                sb.Append("<title>Print Invoice</title>");
                sb.Append("<script src=\"./Print Invoice_files/jquery.v2.0.3.js.download\"></script>");
                sb.Append("<script src=\"./Print Invoice_files/jquery.dataTables.min.js.download\"></script>");
                sb.Append("<!--<script type=\"text/javascript\" src=\"scripts/VisaInvoice.js\"></script>-->");
                sb.Append("<script type=\"text/javascript\" src=\"./Print Invoice_files/VisaInvoice.js.download\"></script>");
                sb.Append("<style type=\"text/css\" media=\"print\">");
                sb.Append("@page {");
                sb.Append("size: landscape;");
                sb.Append("}");
                sb.Append("</style>");
                sb.Append("");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: &quot;Segoe UI&quot;, Tahoma, sans-serif; margin: 0px 40px; width: auto; border: 2px solid gray;\">");
                sb.Append("<div id=\"maincontainer\">");
                sb.Append("<div style=\"background-color: #F7B85B; height: 13px\">");
                sb.Append("<span>&nbsp;</span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table style=\" height: 90px; width: 100%;\">");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<td style=\"width: auto;padding-left:15px\">");
                sb.Append("<img src=\"/images/logo.png\" alt=\"\" height=\"auto\" width=\"auto\">");
                sb.Append("</td>");
                sb.Append("");
                sb.Append("<td style=\"width: 60%;padding-right: 15px;color: #57585A;\" align=\"right\">");
                sb.Append("<span style=\" margin-right: 15px\">");
                sb.Append("<br>");
                sb.Append("2nd Floor, Vishnu Complex, CA Road,<br>");
                sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                sb.Append("Nagpur - 440008, Maharashtra, INDIA<br>");
                sb.Append("<b>Tel:</b> +91-712-6660666, <b>Fax:</b>+91-712-2766520<br>");
                sb.Append("<b>Email:</b> info@clickurtrip.com/accounts@clickurtrip.com<br>");
                sb.Append("</span>");
                sb.Append("    </td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style=\"color: #57585A\"><span> <b>Service Tax Reg No:</b> AADPZ22639MST001</span></td>");
                sb.Append("<td></td>");
                sb.Append("<td></td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append(GetInvDetails());
                sb.Append("</div>");
                sb.Append("<div>");
                //sb.Append(GetInvDetails());
                sb.Append("</div>");
                sb.Append(BookingDetails());
                sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                sb.Append(GetBankDetails());
                sb.Append("<table border=\"1\" style=\" height:100px; width: 100%; border-spacing: 0px; border-bottom:none; border-left: none; border-right: none\">");
                sb.Append("<tbody><tr style=\"  font-size: 20px; border-spacing: 0px\">");
                sb.Append("<td colspan=\"5\" height=\"20px\" style=\"width: 70%; background-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A\"><b> Terms &amp; Conditions</b></td>");
                sb.Append("<td rowspan=\"2\" style=\" border-bottom:none; border-left:none; text-align:center\">");
                sb.Append("<img src=\"" + URL + "/images/signature.png\" alt=\"\" height=\"auto\" width=\"auto\">");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"font-size: 15px\">");
                sb.Append("<td colspan=\"5\" style=\"background-color: #E6E7E9; border-top-width: 3px; border-top-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A;\">");
                sb.Append("<ul style=\"list-style-type: disc\">");
                sb.Append("<li>Kindly check all details carefully to avoid un-necessary complications</li>");
                sb.Append("<li> Cheque to be drawn in our company name on presentation of invoice</li>");
                sb.Append("<li>Subject to NAGPUR (INDIA) jurisdiction </li>");
                sb.Append("</ul>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</tbody></table>");
                sb.Append("</div>");
                sb.Append("<div style=\"background-color: #00AEEF; text-align: center; font-size: 21px; color: white\">");
                sb.Append("<span>");
                sb.Append("Computer generated invoice do not require signature...");
                sb.Append("</span>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</body></html>");
                HttpContext context = System.Web.HttpContext.Current;
                // var htmlContent = String.Format(sb.ToString());
                NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
                // pdfConverter.PdfToolPath = "E:\\Kashif\\CUT\\CUT\\Agent";

                // pdfConverter.PdfToolPath = "C:\\inetpub\\vhosts\\clickurtrip.com\\httpdocs\\Agent";
                pdfConverter.PdfToolPath = rootPath + "\\Agent";
                var pdfBytes = pdfConverter.GeneratePdf(sb.ToString());
                MemoryStream ms = new MemoryStream(pdfBytes);
                //write to file
                string FilePath = HttpContext.Current.Server.MapPath("~/InvoicePdf/") + Invoice + ".pdf";
                FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.Write);
                ms.WriteTo(file);
                file.Close();
                ms.Close();
                DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
                retcode = SendInvoice(URL + "InvoicePdf/" + Invoice + ".pdf");
                if (retcode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
                {
                    DeleteAttachment();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }



        }
        #endregion


        #region Send Mail
        public static DBHelper.DBReturnCode SendInvoice(string DocPath)
        {
            // var attachmentPath=

            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;

            try
            {

                //int effected = 0;
                //SqlParameter[] sqlParams = new SqlParameter[5];
                //sqlParams[0] = new SqlParameter("@sTo", MailTo);
                //sqlParams[1] = new SqlParameter("@sSubject", Subject);
                //sqlParams[2] = new SqlParameter("@VarBody", MailMessage);
                //sqlParams[3] = new SqlParameter("@Path", DocPath);
                //sqlParams[4] = new SqlParameter("@Cc", "");
                //retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);
                //return retcode;

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["VISAMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in MailTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                foreach (string link in DocPath.Split(';'))
                {
                    if (link != "")
                    {
                        DocLinksList.Add(link);
                    }
                }

                //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, Subject, MailMessage.ToString(), from, DocLinksList);
                if (reponse == true)
                {
                    retcode = BL.DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
                return retcode;
            }
            catch
            {
                return retcode;
            }
        }

        #endregion

        #region Genral Method
        public static string ToWords(decimal number, string CurrencyName)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number), CurrencyName);

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            if (CurrencyName == "Currency-AED")
            {
                return string.Format("{0} AED and {1} Fils", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-SAR")
            {
                return string.Format("{0} SAR and {1} Halalah", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-eur")
            {
                return string.Format("{0} Euro and {1} 	Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-gbp")
            {
                return string.Format("{0} Great Britain Pounds and {1} Penny", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-dollar")
            {
                return string.Format("{0} Dollar and {1} Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else
                return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }
        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }
        private static string[] ones = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", };

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static bool CheckIfProcessAlreadyRunning()
        {
            //if (Process.GetProcessesByName("phantomjs").Length > 0)
            //{
            //    rocess.GetProcessesByName("phantomjs")
            //    return true;
            //}
            foreach (Process proc in Process.GetProcessesByName("wkhtmltopdf"))
            {
                proc.Kill();
            }
            return false;
        }

        public static void DeleteAttachment()
        {
            try
            {
                string Path = System.Web.HttpContext.Current.Server.MapPath("~/InvoicePdf/");
                string FName = Invoice + ".pdf";
                DirectoryInfo dir = new DirectoryInfo(Path);
                FileInfo[] files = null;

                files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    if (file.Name == FName)
                    {
                        file.Delete();
                        break;
                    }
                    else
                    {
                        continue;
                    }

                }
            }
            catch
            {

            }
        }
        #endregion

      
      
    }
}