﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class InvoiceManager
    {
        //static dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        public static string GetInvoice(string ReservationID, Int64 Uid, out string title)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                using (var db = new helperDataContext())
                {
                    //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                    string CurrencyClass = "";
                    title = "";
                    // CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(ReservationID);
                    string AllPassengers = "";
                    if (ReservationID != "")
                    {
                        Int64 ContactID = 0;
                        DataSet ds = new DataSet();
                        Int64 ParentID = AccountManager.GetSupplierByUser();
                        ContactID = db.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().ContactID;
                        string Logo = HttpContext.Current.Session["logo"].ToString();
                        string Night = "";
                        var dtHotelReservation = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                        var dtBookedPassenger = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                        var dtBookedRoom = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                        var dtSupplierDetails = (from obj in db.tbl_AdminLogins
                                                 join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                 from objh in db.tbl_HCities
                                                 where obj.ContactID == ContactID && objc.Code == objh.Code
                                                 select new
                                                 {
                                                     obj.AgencyName,
                                                     obj.Agentuniquecode,
                                                     objc.Address,
                                                     objc.email,
                                                     objc.Mobile,
                                                     objc.phone,
                                                     objc.PinCode,
                                                     objc.Fax,
                                                     objc.sCountry,
                                                     objc.Website,
                                                     objc.StateID,
                                                     objh.Countryname,
                                                     objh.Description
                                                 }).FirstOrDefault();

                        var UserDetail = (from obj in db.tbl_AdminLogins
                                          join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                          join objh in db.tbl_HCities on objc.Code equals objh.Code
                                          //join objs in DB.tbl_GstStates on objc.StateID equals objs.StateID
                                          where obj.sid == Uid
                                          select new
                                          {
                                              obj.ContactPerson,
                                              obj.CurrencyCode,
                                              obj.Last_Name,
                                              obj.Designation,
                                              obj.AgencyName,
                                              obj.AgencyType,
                                              obj.PANNo,
                                              obj.Agentuniquecode,
                                              obj.ContactID,
                                              obj.uid,
                                              objc.Address,
                                              objc.email,
                                              objc.Mobile,
                                              objc.phone,
                                              objc.PinCode,
                                              objc.Fax,
                                              objc.sCountry,
                                              objc.Website,
                                              objc.StateID,
                                              // objs.SateName,
                                              objh.Countryname,
                                              objh.Description
                                          }).FirstOrDefault();

                        string ReservationDate = dtHotelReservation.ReservationDate;
                        ReservationDate = ReservationDate.Replace("00:00:", "");
                        switch (UserDetail.CurrencyCode)
                        {
                            case "AED":
                                CurrencyClass = "Currency-AED";

                                break;
                            case "SAR":
                                CurrencyClass = "Currency-SAR";
                                break;
                            case "EUR":
                                CurrencyClass = "fa fa-eur";
                                break;
                            case "GBP":
                                CurrencyClass = "fa fa-gbp";
                                break;
                            case "USD":
                                CurrencyClass = "fa fa-dollar";
                                break;
                            case "INR":
                                CurrencyClass = "fa fa-inr";
                                break;
                        }
                        string Status = "";
                        if (dtHotelReservation.Status == "Vouchered" || dtHotelReservation.Status == "Cancelled")
                            title = "Hotel Invoice -  " + dtHotelReservation.Status + " on " + ReservationDate + " For " + dtHotelReservation.bookingname;
                        else
                            title = "Hotel Invoice - ";


                        //   decimal SalesTax = Convert.ToDecimal(dtBookingTransactions.Rows[0]["SeviceTax"]);
                        decimal Ammount = Convert.ToDecimal(dtHotelReservation.TotalFare);
                        // decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);
                        //   SalesTax = decimal.Round(SalesTax, 2, MidpointRounding.AwayFromZero);
                        Ammount = decimal.Round(Ammount, 2, MidpointRounding.AwayFromZero);
                        string InvoiceID = dtHotelReservation.InvoiceID;
                        string Hoteldestination = dtHotelReservation.City;
                        string VoucherID = dtHotelReservation.VoucherID;
                        string CheckIn = dtHotelReservation.CheckIn;
                        CheckIn = CheckIn.Replace("00:00", "");
                        string CheckOut = dtHotelReservation.CheckOut;
                        CheckOut = CheckOut.Replace("00:00", "");
                        string HotelName = dtHotelReservation.HotelName;
                        Status = dtHotelReservation.Status;
                        string HotelConfirmNo = dtHotelReservation.HotelConfirmationNo;

                        string AgentRef = UserDetail.Agentuniquecode;
                        string AgencyName = UserDetail.AgencyName;
                        string Address = UserDetail.Address;
                        string Description = UserDetail.Description;
                        string Countryname = UserDetail.Countryname;
                        string phone = UserDetail.Mobile;
                        string email = UserDetail.email;
                        Night = Convert.ToString(dtHotelReservation.NoOfDays);
                        string words;
                        //words = ToWords((Ammount) + SalesTax);
                        words = ToWords((Ammount), CurrencyClass);
                        //string board;

                        string CanAmtWoutNight = "";
                        string CanAmtWithTax = "";

                        string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);

                        string Supplier = dtHotelReservation.Source;
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<div>");
                        sb.Append("<table style=\" height: 100px; width: 100%;\">");
                        sb.Append("<tbody>");
                        sb.Append("<tr>");
                        sb.Append("<td style=\"width: auto;padding-left:15px\">");
                        if (DefaultManager.UrlExists(Url + "AgencyLogos/" + dtSupplierDetails.Agentuniquecode + ".png"))
                            sb.Append("<img  src=\"" + Url + "AgencyLogo/" + Logo + "\" height=\"auto\" width=\"auto\"></img>");
                        else
                            sb.Append("<h1 style=\"text-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);\" height=\"auto\" width=\"auto\">" + dtSupplierDetails.AgencyName + "</h1>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"width: 20%\"></td>");
                        sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
                        sb.Append("<span style=\"margin-right: 15px\">");
                        sb.Append("<br>");
                        sb.Append("<b>" + dtSupplierDetails.AgencyName + "</b><br>");
                        sb.Append("" + dtSupplierDetails.Address + ",<br>");
                        //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                        sb.Append("" + dtSupplierDetails.Description + "-" + dtSupplierDetails.PinCode + "," + dtSupplierDetails.Countryname + "<br>");
                        sb.Append("Tel: " + dtSupplierDetails.Mobile + ", Fax:" + dtSupplierDetails.Fax + "<br>");
                        sb.Append(" <B>Email: " + dtSupplierDetails.email + "<br>");
                        sb.Append("</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</tbody>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        sb.Append("<div>");
                        sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                        sb.Append("<tr>");
                        sb.Append("<td rowspan=\"3\" style=\"width:35%; color:gray; font-size: 30px; text-align: center; border: none; padding-right:35px; padding-left:10px\">");
                        sb.Append("<b>SALES INVOICE</b><br><br>");
                        if (Status == "Cancelled")
                        {
                            sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">Cancelled Booking</span>");
                        }
                        else
                        {
                            sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td style=\"border-top: none;border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px; color: #57585A\">");
                        sb.Append("<b> Invoice Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b><span style=\"color: #757575\"> " + ReservationDate + " </span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"border-top: none; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px; color: #57585A\">");
                        sb.Append("<b>Invoice No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;     : </b><span style=\"color: #757575\">" + InvoiceID + " </span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        sb.Append("<b> Voucher No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + VoucherID + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        sb.Append("<b>  Agent Code &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\" color:#757575\">" + AgentRef + "</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        sb.Append("<b> Hotel Confirmation No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + HotelConfirmNo + "</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"1\" style=\"height: 25px; border:none\"></td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        sb.Append("<div>");
                        sb.Append("<table border=\"1\" style=\"border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC\">");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"2\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A\"><b>Invoice To</b></td>");
                        sb.Append("<td colspan=\"3\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size:20px; color: #57585A\"> <b>Service Details</b></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td rowspan=\"6\" style=\"width:10%; border-width:3px 0px 0px 0px; border-bottom-color:gray; border-spacing:0px; color: #57585A\">");
                        sb.Append("<span style=\"padding-left:10px\"><b>Name</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Address</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>City</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Country</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Phone</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Email</b></span><br>");
                        //sb.Append("<span style=\"padding-left:10px\"><b>GST No.</b></span><br>");
                        sb.Append("</td>");
                        sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A\">");
                        sb.Append(":<span style=\"padding-left:10px\">" + AgencyName + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Address + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Description + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Countryname + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + phone + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + email + "</span><br>");
                        //sb.Append(":<span style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["GSTNumber"] + "</span><br>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"3\" style=\"border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; background-color: ; padding-left: 8px\"><span style=\"color:;font-size:14px\"><b>Hotel Name  :</b></span> <span style=\"color:;font-size:18px\">" + HotelName + "</span></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"3\" style=\"border: none; background-color: ; padding-left:8px\"><span style=\"color:; font-size: 14px\"><b>Destination :</b> <span style=\"color:;font-size:18px\">" + Hoteldestination + "</span></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr style=\"color:;\">");
                        sb.Append("<td style=\"width: 150px; border: none; background-color: ; padding-bottom:3px\" align=\"center\">");
                        sb.Append(" <span><b> Check In</b></span><br>");
                        sb.Append("<span>" + CheckIn + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"width: 150px; border: none; background-color: ; padding-bottom: 3px\" align=\"center\">");
                        sb.Append("<span><b>Check Out</b></span><br>");
                        sb.Append("<span>" + CheckOut + "</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"width: 150px; border: none; background-color: #A8A9AD; padding-bottom: 3px\" align=\"center\">");
                        sb.Append("<span><b>Total Night(s)</b></span><br>");
                        sb.Append(" <span>" + Night + "Night(s)</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                        //sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        //Room Rate table goes here.............................................
                        sb.Append("<div style=\"font-size: 20px;padding-bottom:10px; padding-top:8px\">");
                        //sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Rate</b></span>");
                        sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Booking Details</b></span>");
                        sb.Append("</div>");
                        sb.Append("<div>");

                        sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        sb.Append("<tr style=\"border: none\">");
                        sb.Append("<td style=\"background-color: ; color: ; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px \">");
                        sb.Append("<span>No.</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\" height: 35px; background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                        sb.Append("<span>Room Type</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\" height: 35px; background-color: ; color: ; font-size: 15px; border: none;  font-weight: 700; text-align:center\">");
                        sb.Append("<span>Board</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\" height: 35px; background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: center\">");
                        sb.Append("<span>Rooms</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\" height: 35px; background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Nights</span>");
                        sb.Append("</td>");
                        //passenger details
                        sb.Append("<td style=\" background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Guest Name</span>");
                        sb.Append("</td>");
                        //passenger
                        sb.Append("<td style=\"height: 35px; background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Rate (" + UserDetail.CurrencyCode + ")</span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\" height: 35px; background-color: ; color: ; font-size: 15px; border: none; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Total (" + UserDetail.CurrencyCode + ")</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        #region Other Supplier
                        for (var i = 0; i < dtBookedRoom.Count; i++)
                        {

                            sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                            sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                            sb.Append("<td style=\"border: none\">" + dtBookedRoom[i].RoomType + "</td>");
                            if (dtBookedRoom[i].BoardText == "BB")
                            {
                                dtBookedRoom[i].BoardText = "Bed & Breakfast";
                            }
                            if (dtBookedRoom[i].BoardText == "RO")
                            {
                                dtBookedRoom[i].BoardText = "Room Only";
                            }
                            if (dtBookedRoom[i].BoardText == "HB")
                            {
                                dtBookedRoom[i].BoardText = "Half Board";
                            }
                            if (dtBookedRoom[i].BoardText == "FB")
                            {
                                dtBookedRoom[i].BoardText = "Full Board";
                            }
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom[i].BoardText + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + "Room &nbsp" + dtBookedRoom[i].RoomNumber + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                            AllPassengers = "";
                            for (int p = 0; p < dtBookedPassenger.Count; p++)
                            {
                                string brrn = dtBookedRoom[i].RoomNumber;
                                string bprn = dtBookedPassenger[p].RoomNumber;

                                if (dtBookedPassenger[p].RoomNumber == dtBookedRoom[i].RoomNumber)
                                {
                                    if (dtBookedPassenger[p].LastName == "")
                                    {

                                    }

                                    else if (dtBookedPassenger[p].LastName == null)
                                    {
                                        var Master = dtBookedPassenger[p].Name.Split(' ')[1];
                                        if (Master != "")
                                        {
                                            AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";
                                        }
                                    }
                                    else
                                        AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";
                                }

                            }

                            AllPassengers = AllPassengers.TrimEnd(' ');
                            AllPassengers = AllPassengers.TrimEnd(',');
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom[i].RoomAmount) / (Convert.ToInt64(Night)), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                            sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom[i].RoomAmount), 2, MidpointRounding.AwayFromZero))).ToString("#,##0.00") + "</td>");
                            sb.Append("</tr>");

                        }
                        #endregion
                        sb.Append("<tr border=\"1\" style=\"border-spacing:0px\">");
                        sb.Append("<td align=\"center\" colspan=\"7\" style=\"height: 35px; background-color: ; color: ;text-align: right; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                        var arrRates = (from obj in DB.Comm_BookingTaxes where obj.ReservationID == ReservationID select obj).ToList();
                        foreach (var objRate in arrRates)
                        {
                            sb.Append("<span style=\"font-size: 15;padding-left: 10px;font-weight: 500;\">" + objRate.RateName.ToUpper() + "</span><br/>");
                        }
                        float arrAddOnCharge = (from obj in DB.tbl_CommonBookingHotelAddons where obj.BookingId == ReservationID select obj).ToList().Select(d => Convert.ToSingle(d.Quantity) * Convert.ToSingle(d.Rate)).ToList().Sum();
                        sb.Append("<span style=\"font-size: 15;padding-left: 10px;font-weight: 500;\">AddOns Rates</span><hr/>");

                        sb.Append("<b> In Words</b> <span>: ( " + words + " )</span> <span style=\"font-size: 15px;padding-left: 10px;font-weight: 500;\">Total Amount</span>");
                        sb.Append("</td>");
                        sb.Append("<td align=\"center\" style=\"height: 35px; background-color: ; color: ;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                        foreach (var objRate in arrRates)
                        {
                            sb.Append("<span style=\"font-size: 15;padding-left: 10px;font-weight: 500;\" title=\"" + objRate.RateBreckups.Replace("^", "\n") + "\">" + (Convert.ToSingle(objRate.Amount)).ToString("#,##0.00") + "</span><br/>");
                        }
                        sb.Append("<span style=\"font-size: 15;padding-left: 10px;font-weight: 500;\">" + ((arrAddOnCharge)).ToString("#,##0.00") + "</span><hr>");
                        sb.Append("<span style=\"font-size: 15px;\">" + ((Ammount).ToString("#,##0.00")) + "</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</div>");

                        sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                        sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
                        sb.Append("</div>");
                        sb.Append("<hr style=\"border-top-width: 3px\">");
                        sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
                        sb.Append("<ul style=\"list-style-type: disc\">");
                        sb.Append("<li>In case no show for any guaranteed reservations, full charge will be applied</li>");
                        sb.Append("<li>");
                        sb.Append("We require 7 days advance notice for any amendment & cancellations, otherwise one night room charge will be applied");

                        sb.Append("</li>");
                        sb.Append("<li>If you required to cancel before 5 days full charge will be applied</li>");

                        sb.Append("<li>");
                        sb.Append("ACCORDING TO THE SAUDI AUTHORITIES 5 % VAT WILL BE IMPLEAMENTED ON THE ABOVE RATES,");

                        sb.Append("</li>");
                        sb.Append("<li>VAT NO:300056209610003</li>");
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div>");
                        sb.Append("</div>");
                        sb.Append("<div style=\"background-color: ; text-align: center; font-size: 21px; color:  ;height: 23px\">");
                        sb.Append("<span>");
                        sb.Append("Computer generated invoice do not require signature...");

                        sb.Append("</span>");
                        sb.Append("</div>");
                        return sb.ToString();
                    }
                    else
                        return "";
                }
            }
        }

        public static string GetCommissionInvoice(string ReservationID, Int64 Uid, Int64 Type, out string title)
        {
            
                using (var DB = new dbHotelhelperDataContext())
                {
                     using (var db = new helperDataContext())
                     {

                    //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                    string CurrencyClass = "";
                    title = "";
                    // CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(ReservationID);
                    string AllPassengers = "";
                    if (ReservationID != "")
                    {
                        Int64 ContactID = 0;
                        DataSet ds = new DataSet();
                        Int64 ParentID = AccountManager.GetSupplierByUser();
                        ContactID = db.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().ContactID;
                        string Logo = HttpContext.Current.Session["logo"].ToString();
                        string Night = "";
                        //var dtHotelReservation = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                        //var dtBookedPassenger = (from obj in db.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                        //var dtBookedRoom = (from obj in db.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();

                        var dtInvoiceDetails = (from obj in db.tbl_Invoices where obj.InvoiceType != "Hotel Commission" select obj).ToList();

                        var dtHotelReservation = (from obj in db.tbl_Invoices where  obj.InvoiceNo == ReservationID && obj.InvoiceType == "Hotel Commission" select obj).FirstOrDefault();
                      
                        var dtSupplierDetails = (from obj in db.tbl_AdminLogins
                                                 join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                 from objh in db.tbl_HCities
                                                 where obj.ContactID == ContactID && objc.Code == objh.Code
                                                 select new
                                                 {
                                                     obj.AgencyName,
                                                     obj.Agentuniquecode,
                                                     objc.Address,
                                                     objc.email,
                                                     objc.Mobile,
                                                     objc.phone,
                                                     objc.PinCode,
                                                     objc.Fax,
                                                     objc.sCountry,
                                                     objc.Website,
                                                     objc.StateID,
                                                     objh.Countryname,
                                                     objh.Description
                                                 }).FirstOrDefault();

                        var UserDetail = (from obj in db.tbl_AdminLogins
                                          join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                          join objh in db.tbl_HCities on objc.Code equals objh.Code
                                          //join objs in db.tbl_GstStates on objc.StateID equals objs.StateID
                                          where obj.sid == Uid
                                          select new
                                          {
                                              obj.ContactPerson,
                                              obj.CurrencyCode,
                                              obj.Last_Name,
                                              obj.Designation,
                                              obj.AgencyName,
                                              obj.AgencyType,
                                              obj.PANNo,
                                              obj.Agentuniquecode,
                                              obj.ContactID,
                                              obj.uid,
                                              objc.Address,
                                              objc.email,
                                              objc.Mobile,
                                              objc.phone,
                                              objc.PinCode,
                                              objc.Fax,
                                              objc.sCountry,
                                              objc.Website,
                                              objc.StateID,
                                              // objs.SateName,
                                              objh.Countryname,
                                              objh.Description
                                          }).FirstOrDefault();

                        string ReservationDate = dtHotelReservation.InvoiceDate;
                        ReservationDate = ReservationDate.Replace("00:00:", "");
                        switch (UserDetail.CurrencyCode)
                        {
                            case "AED":
                                CurrencyClass = "Currency-AED";

                                break;
                            case "SAR":
                                CurrencyClass = "Currency-SAR";
                                break;
                            case "EUR":
                                CurrencyClass = "fa fa-eur";
                                break;
                            case "GBP":
                                CurrencyClass = "fa fa-gbp";
                                break;
                            case "USD":
                                CurrencyClass = "fa fa-dollar";
                                break;
                            case "INR":
                                CurrencyClass = "fa fa-inr";
                                break;
                        }
                        string Status = "";
                        if (dtHotelReservation.Status == 1 )
                            title = "Commission Invoice -  " + ReservationDate + " For " + dtSupplierDetails.AgencyName;
                        else
                            title = "Commission Invoice - ";


                        //   decimal SalesTax = Convert.ToDecimal(dtBookingTransactions.Rows[0]["SeviceTax"]);
                        decimal Ammount = Convert.ToDecimal(dtHotelReservation.InvoiceAmount);
                        // decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);
                        //   SalesTax = decimal.Round(SalesTax, 2, MidpointRounding.AwayFromZero);
                        Ammount = decimal.Round(Ammount, 2, MidpointRounding.AwayFromZero);
                        string InvoiceID = dtHotelReservation.InvoiceNo;
                       // string Hoteldestination = dtHotelReservation.City;
                       // string VoucherID = dtHotelReservation.VoucherID;
                       // string CheckIn = dtHotelReservation.CheckIn;
                       // CheckIn = CheckIn.Replace("00:00", "");
                       // string CheckOut = dtHotelReservation.CheckOut;
                       // CheckOut = CheckOut.Replace("00:00", "");
                       // string HotelName = dtHotelReservation.HotelName;
                       // Status = dtHotelReservation.Status;
                       // string HotelConfirmNo = dtHotelReservation.HotelConfirmationNo;

                        string AgentRef = UserDetail.Agentuniquecode;
                        string AgencyName = UserDetail.AgencyName;
                        string Address = UserDetail.Address;
                        string Description = UserDetail.Description;
                        string Countryname = UserDetail.Countryname;
                        string phone = UserDetail.Mobile;
                        string email = UserDetail.email;
                      //  Night = Convert.ToString(dtHotelReservation.NoOfDays);
                        string words;
                        //words = ToWords((Ammount) + SalesTax);
                        words = ToWords((Ammount), CurrencyClass);
                        //string board;

                        string CanAmtWoutNight = "";
                        string CanAmtWithTax = "";

                        string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);

                       // string Supplier = dtHotelReservation.Source;
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<div>");
                        sb.Append("<table style=\" height: 100px; width: 100%;\">");
                        sb.Append("<tbody>");
                        sb.Append("<tr>");
                        sb.Append("<td style=\"width: auto;padding-left:15px\">");
                        if (DefaultManager.UrlExists(Url + "AgencyLogos/" + dtSupplierDetails.Agentuniquecode + ".png"))
                            sb.Append("<img  src=\"" + Url + "AgencyLogo/" + Logo + "\" height=\"auto\" width=\"auto\"></img>");
                        else
                            sb.Append("<h1 style=\"text-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);\" height=\"auto\" width=\"auto\">" + dtSupplierDetails.AgencyName + "</h1>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"width: 20%\"></td>");
                        sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
                        sb.Append("<span style=\"margin-right: 15px\">");
                        sb.Append("<br>");
                        sb.Append("<b>" + dtSupplierDetails.AgencyName + "</b><br>");
                        sb.Append("" + dtSupplierDetails.Address + ",<br>");
                        //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                        sb.Append("" + dtSupplierDetails.Description + "-" + dtSupplierDetails.PinCode + "," + dtSupplierDetails.Countryname + "<br>");
                        sb.Append("Tel: " + dtSupplierDetails.Mobile + ", Fax:" + dtSupplierDetails.Fax + "<br>");
                        sb.Append(" <B>Email: " + dtSupplierDetails.email + "<br>");
                        sb.Append("</span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</tbody>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        sb.Append("<div>");
                        sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                        sb.Append("<tr>");
                        sb.Append("<td rowspan=\"3\" style=\"width:35%; color:gray; font-size: 30px; text-align: center; border: none; padding-right:35px; padding-left:10px\">");
                        sb.Append("<b>SALES INVOICE</b>");
                        //if (Status == "Cancelled")    
                        //{
                        //    sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">Cancelled Booking</span>");
                        //}
                        //else
                        //{
                        //    sb.Append("<span style=\"font-size:25px\"><b>Status : </b> </span><span id=\"spnStatus\" style=\"font-size:25px; font-weight:700\">" + Status + "</span>");
                        //}
                        sb.Append("</td>");
                        sb.Append("<td style=\"border-top: none;border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px; color: #57585A\">");
                        sb.Append("<b> Invoice Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b><span style=\"color: #757575\"> " + ReservationDate + " </span>");
                        sb.Append("</td>");
                        sb.Append("<td style=\"border-top: none; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px; color: #57585A\">");
                        sb.Append("<b>Invoice No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;     : </b><span style=\"color: #757575\">" + InvoiceID + " </span>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        //sb.Append("<b> Voucher No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + VoucherID + "</span>");
                        //sb.Append("</td>");
                        //sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        //sb.Append("<b>  Agent Code &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\" color:#757575\">" + AgentRef + "</span>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px; color: #57585A\">");
                        //sb.Append("<b> Hotel Confirmation No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + HotelConfirmNo + "</span>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"1\" style=\"height: 25px; border:none\"></td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        sb.Append("<div>");
                        sb.Append("<table border=\"1\" style=\"border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC\">");
                        sb.Append("<tr>");
                        sb.Append("<td colspan=\"5\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A\"><b>Invoice To</b></td>");
                       // sb.Append("<td colspan=\"3\" style=\"border: none; padding-bottom: 10px; padding-left: 10px; font-size:20px; color: #57585A\"> <b>Service Details</b></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td rowspan=\"6\" style=\"width:10%; border-width:3px 0px 0px 0px; border-bottom-color:gray; border-spacing:0px; color: #57585A\">");
                        sb.Append("<span style=\"padding-left:10px\"><b>Name</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Address</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>City</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Country</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Phone</b></span><br>");
                        sb.Append("<span style=\"padding-left:10px\"><b>Email</b></span><br>");
                        //sb.Append("<span style=\"padding-left:10px\"><b>GST No.</b></span><br>");
                        sb.Append("</td>");
                        sb.Append("<td rowspan=\"6\" style=\"width: 40%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A\">");
                        sb.Append(":<span style=\"padding-left:10px\">" + AgencyName + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Address + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Description + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + Countryname + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + phone + "</span><br>");
                        sb.Append(":<span style=\"padding-left:10px\">" + email + "</span><br>");
                        //sb.Append(":<span style=\"padding-left:10px\">" + dtAgentDetail.Rows[0]["GSTNumber"] + "</span><br>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td colspan=\"3\" style=\"border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; background-color: ; padding-left: 8px\"><span style=\"color:;font-size:14px\"><b>Hotel Name  :</b></span> <span style=\"color:;font-size:18px\">" + HotelName + "</span></td>");
                        //sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td colspan=\"3\" style=\"border: none; background-color: ; padding-left:8px\"><span style=\"color:; font-size: 14px\"><b>Destination :</b> <span style=\"color:;font-size:18px\">" + Hoteldestination + "</span></td>");
                        //sb.Append("</tr>");
                        //sb.Append("<tr style=\"color:;\">");
                        //sb.Append("<td style=\"width: 150px; border: none; background-color: ; padding-bottom:3px\" align=\"center\">");
                        //sb.Append(" <span><b> Check In</b></span><br>");
                        //sb.Append("<span>" + CheckIn + "</span>");
                        //sb.Append("</td>");
                        //sb.Append("<td style=\"width: 150px; border: none; background-color: ; padding-bottom: 3px\" align=\"center\">");
                        //sb.Append("<span><b>Check Out</b></span><br>");
                        //sb.Append("<span>" + CheckOut + "</span>");
                        //sb.Append("</td>");
                        //sb.Append("<td style=\"width: 150px; border: none; background-color: #A8A9AD; padding-bottom: 3px\" align=\"center\">");
                        //sb.Append("<span><b>Total Night(s)</b></span><br>");
                        //sb.Append(" <span>" + Night + "Night(s)</span>");
                        //sb.Append("</td>");
                        //sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                        //sb.Append("</tr>");
                        //sb.Append("<tr>");
                        //sb.Append("<td colspan=\"3\" style=\"border:none\">&nbsp;</td>");
                        //sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("</div>");
                        //Room Rate table goes here.............................................
                        sb.Append("<div style=\"font-size: 20px;padding-bottom:10px; padding-top:8px\">");
                        //sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Rate</b></span>");
                        sb.Append("<span style=\"padding-left:10px; color: #57585A\"><b>Commission Details</b></span>");
                        sb.Append("</div>");
                        sb.Append("<div>");

                        sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        sb.Append("<tr style=\"border: none\">");
                        sb.Append("<td style=\" font-size: 15px;  border-bottom-color: gray; border-bottom-width: 3px;  font-weight: 700; text-align:left; padding-left:10px \">");
                        sb.Append("<span>No.</span>");
                        sb.Append("</td>");

                        sb.Append("<td style=\" height: 25px; font-size: 15px;  border-bottom-color: gray; border-bottom-width: 3px; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Invoice No</span>");
                        sb.Append("</td>");

                        sb.Append("<td style=\" height: 25px; font-size: 15px; border-bottom-color: gray; border-bottom-width: 3px; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Invoice Date</span>");
                        sb.Append("</td>");

                        sb.Append("<td style=\" height: 25px;  font-size: 15px;  border-bottom-color: gray; border-bottom-width: 3px; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Total (" + UserDetail.CurrencyCode + ")</span>");
                        sb.Append("</td>");

                        sb.Append("<td colspan=\"\" style=\" height: 25px; font-size: 15px;  border-bottom-color: gray; border-bottom-width: 3px; font-weight: 700; text-align: center;\">");
                        sb.Append("<span>Commission Amount</span>");
                        sb.Append("</td>");

                        sb.Append("</tr>");

                        for (int i = 0; i < dtInvoiceDetails.Count; i++)
                        {
                            decimal Ammnt = Convert.ToDecimal(dtInvoiceDetails[i].InvoiceAmount);
                            Ammnt = decimal.Round(Ammnt, 2, MidpointRounding.AwayFromZero);

                            sb.Append("<tr style=\"border: none\">");
                            sb.Append("<td style=\"font-size: 12px; border: none;  font-weight: 400;; text-align:left; padding-left:10px \">");
                            sb.Append("<span>"+ (i + 1)+"</span>");
                            sb.Append("</td>");

                            sb.Append("<td style=\" height: 35px; ; font-size: 12px; border: none;font-weight: 400;; text-align: center;\">");
                            sb.Append("<span>" + dtInvoiceDetails[i].InvoiceNo + "</span>");
                            sb.Append("</td>");

                            sb.Append("<td style=\" height: 35px;  ; font-size: 12px; border: none;font-weight: 400; ; text-align: center;\">");
                            sb.Append("<span>" + dtInvoiceDetails[i].InvoiceDate + "</span>");
                            sb.Append("</td>");

                            sb.Append("<td style=\" height: 35px;  ; font-size: 12px; border: none;font-weight: 400;; text-align: center;\">");
                           sb.Append("<span> " + ((Ammnt).ToString("#,##0.00")) + "</span>");
                           sb.Append("</td>");

                           sb.Append("<td colspan=\"\" style=\" height: 35px;  ; font-size: 12px; border: none; ; text-align: center;\">");
                           sb.Append("<span> " + dtInvoiceDetails[i].Commission + "</span>");
                           sb.Append("</td>");

                           sb.Append("</tr>");
                        }

                       #region Other Supplier
                        //for (var i = 0; i < dtBookedRoom.Count; i++)
                        //{

                        //    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                        //    sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                        //    sb.Append("<td style=\"border: none\">" + dtBookedRoom[i].RoomType + "</td>");
                        //    if (dtBookedRoom[i].BoardText == "BB")
                        //    {
                        //        dtBookedRoom[i].BoardText = "Bed & Breakfast";
                        //    }
                        //    if (dtBookedRoom[i].BoardText == "RO")
                        //    {
                        //        dtBookedRoom[i].BoardText = "Room Only";
                        //    }
                        //    if (dtBookedRoom[i].BoardText == "HB")
                        //    {
                        //        dtBookedRoom[i].BoardText = "Half Board";
                        //    }
                        //    if (dtBookedRoom[i].BoardText == "FB")
                        //    {
                        //        dtBookedRoom[i].BoardText = "Full Board";
                        //    }
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + dtBookedRoom[i].BoardText + "</td>");
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + "Room &nbsp" + dtBookedRoom[i].RoomNumber + "</td>");
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + (Night) + "</td>");
                        //    AllPassengers = "";
                        //    for (int p = 0; p < dtBookedPassenger.Count; p++)
                        //    {
                        //        string brrn = dtBookedRoom[i].RoomNumber;
                        //        string bprn = dtBookedPassenger[p].RoomNumber;

                        //        if (dtBookedPassenger[p].RoomNumber == dtBookedRoom[i].RoomNumber)
                        //        {
                        //            if (dtBookedPassenger[p].LastName == "")
                        //            {

                        //            }

                        //            else if (dtBookedPassenger[p].LastName == null)
                        //            {
                        //                var Master = dtBookedPassenger[p].Name.Split(' ')[1];
                        //                if (Master != "")
                        //                {
                        //                    AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";
                        //                }
                        //            }
                        //            else
                        //                AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";
                        //        }

                        //    }

                        //    AllPassengers = AllPassengers.TrimEnd(' ');
                        //    AllPassengers = AllPassengers.TrimEnd(',');
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + AllPassengers + "</td>");
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom[i].RoomAmount) / (Convert.ToInt64(Night)), 2, MidpointRounding.AwayFromZero)).ToString("#,##0.00")) + "</td>");
                        //    sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + ((decimal.Round(Convert.ToDecimal(dtBookedRoom[i].RoomAmount), 2, MidpointRounding.AwayFromZero))).ToString("#,##0.00") + "</td>");
                        //    sb.Append("</tr>");

                        //}
                       #endregion

                        //sb.Append("<tr border=\"1\" style=\"border-spacing:0px\">");
                        //sb.Append("<td align=\"center\" colspan=\"7\" style=\"height: 35px; background-color: ; color: ;text-align: right; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                        //    sb.Append("<b> In Words</b> <span>: ( " + words + " )</span> <span style=\"font-size: 15px;padding-left: 10px;font-weight: 500;\"> Total </span>");
                        //    sb.Append("</td>");
                        //    sb.Append("<td align=\"center\" style=\"height: 35px; background-color: ; color: ;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;\">");
                        //    sb.Append("<span>" + ((Ammount).ToString("#,##0.00")) + "</span>");
                        //    sb.Append("</td>");
                        //    sb.Append("</tr>");
                            sb.Append("</table>");
                            sb.Append("</div>");

                            sb.Append("<div style=\"font-size: 20px; padding-bottom: 0px; padding-top: 8px\">");
                            sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b> In Words</b> <span>: ( " + words + " )</span> <span style=\"float:right;margin-right:15%;\"> <b>Total :  " + UserDetail.CurrencyCode + " " + ((Ammount).ToString("#,##0.00")) + " </b></span>");
                            sb.Append("</div>");
                            sb.Append("<hr style=\"border-top-width: 3px\">");

                        sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                        sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
                        sb.Append("</div>");
                        sb.Append("<hr style=\"border-top-width: 3px\">");
                        sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
                        sb.Append("<ul style=\"list-style-type: disc\">");
                        sb.Append("<li>In case no show for any guaranteed reservations, full charge will be applied</li>");
                        sb.Append("<li>");
                        sb.Append("We require 7 days advance notice for any amendment & cancellations, otherwise one night room charge will be applied");

                        sb.Append("</li>");
                        sb.Append("<li>If you required to cancel before 5 days full charge will be applied</li>");

                        sb.Append("<li>");
                        sb.Append("ACCORDING TO THE SAUDI AUTHORITIES 5 % VAT WILL BE IMPLEAMENTED ON THE ABOVE RATES,");

                        sb.Append("</li>");
                        sb.Append("<li>VAT NO:300056209610003</li>");
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div>");
                        sb.Append("</div>");
                        sb.Append("<div style=\"background-color: ; text-align: center; font-size: 21px; color:  ;height: 23px\">");
                        sb.Append("<span>");
                        sb.Append("Computer generated invoice do not require signature...");

                        sb.Append("</span>");
                        sb.Append("</div>");
                        return sb.ToString();
                    }
                    else
                        return "";
                }
                }
            }
        

        private static string[] ones = {
    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", 
    "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen",
};

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static string ToWords(decimal number, string CurrencyName)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number), CurrencyName);

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            if (CurrencyName == "Currency-AED")
            {
                return string.Format("{0} AED and {1} Fils", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-SAR")
            {
                return string.Format("{0} SAR and {1}  /100", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-eur")
            {
                return string.Format("{0} Euro and {1} 	Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-gbp")
            {
                return string.Format("{0} Great Britain Pounds and {1} Penny", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-dollar")
            {
                return string.Format("{0} Dollar and {1} Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else
                return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }

        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }
    }
}
