﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.BL;
using System.Globalization;
using System.Data;
namespace CutAdmin.DataLayer
{
    public class ManageInventory : InventoryManager
    {
        public class Room
        {
            public string RoomName { get; set; }
            public Int64 RoomTypeID { get; set; }
        }
        public class Inventory
        {
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string Address { get; set; }
            public List<date> Dates { get; set; }
            public List<RoomType> ListRoom { get; set; }
        }
       public static List<DateTime> RateDate{ get; set; }
       public static List<Inventory> GetInventory(string CheckIn, string CheckOut, Int64 HotelCode, string InventoryType, Int64 SupplierID)
       {
           List<Inventory> ListInventory = new List<Inventory>();
           try
           {
               using (var db = new dbHotelhelperDataContext())
               {
                   DateTime From = DateTime.ParseExact(CheckIn, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                   DateTime To = DateTime.ParseExact(CheckOut, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                   RateDate = new List<DateTime>();
                   double noGDays = (To - From).TotalDays;
                   List<string> ListDates = new List<string>();
                   for (int i = 0; i <= noGDays; i++)
                   {
                       ListDates.Add(From.AddDays(i).ToString("dd-MM-yyyy"));
                   }
                   DateTime targetDate = From;
                   while (targetDate <= To)
                   {
                       RateDate.Add(targetDate);
                       targetDate = targetDate.AddDays(1);
                   }
                   var arrHotels = (from objHotel in db.tbl_CommonHotelMasters
                                    join objInvent in db.tbl_CommonHotelInventories on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString()
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   if (HotelCode != 0)
                   {
                       arrHotels = (from objHotel in db.tbl_CommonHotelMasters
                                    join objInvent in db.tbl_CommonHotelInventories on objHotel.sid equals Convert.ToInt64(objInvent.HotelCode)
                                    where objInvent.SupplierId == SupplierID.ToString() && objInvent.HotelCode == HotelCode.ToString()
                                    select new
                                    {
                                        objInvent.HotelCode,
                                        objHotel.HotelName,
                                        Address = objHotel.HotelAddress,
                                        Rating = objHotel.HotelCategory
                                    }).Distinct().ToList();
                   }
                   foreach (var objHotel in arrHotels)
                   {
                       try
                       {
                           var arrRooms = (from objRoom in db.tbl_commonRoomDetails
                                           join objRoomType in db.tbl_commonRoomTypes on objRoom.RoomTypeId equals objRoomType.RoomTypeID
                                           //join objInvent in db.tbl_CommonHotelInventories on objRoomType.RoomTypeID equals Convert.ToInt64(objInvent.RoomType)
                                           where objRoom.HotelId == Convert.ToInt64(objHotel.HotelCode)
                                           select new Room
                                           {
                                               RoomTypeID = objRoomType.RoomTypeID,
                                               RoomName = objRoomType.RoomType,
                                           }).ToList().Distinct();
                           List<RoomType> ListRooms = new List<RoomType>();
                           foreach (var objRoom in arrRooms)
                           {
                               List<date> ListDate = new List<date>();
                               for (int i = 0; i <= noGDays; i++)
                               {
                                   string Type = "";
                                   string Date = From.AddDays(i).ToString("dd-MM-yyyy");
                                   var listInventory = (from obj in db.tbl_CommonHotelInventories
                                                        where obj.HotelCode == objHotel.HotelCode.ToString() && obj.Month == Date.Split('-')[1] &&
                                                        obj.Year == Date.Split('-')[2] && obj.RoomType == objRoom.RoomTypeID.ToString() &&
                                                        obj.SupplierId == SupplierID.ToString() && obj.InventoryType == InventoryType
                                                        select obj).ToList();
                                   Int64 Count = 0;
                                   Int64 Booked = 0;
                                   if (listInventory.Count != 0)
                                   {
                                       ListtoDataTable lsttodt = new ListtoDataTable();
                                       DataTable dt = lsttodt.ToDataTable(listInventory);

                                       string date = "";
                                       if (From.AddDays(i).ToString("dd").StartsWith("0"))
                                           date = "Date_" + From.AddDays(i).ToString("dd").TrimStart('0');
                                       else
                                           date = "Date_" + From.AddDays(i).ToString("dd");
                                       string noRooms = dt.Rows[0][date].ToString();
                                       if (noRooms != "")
                                       {
                                           string noTotalRooms = noRooms.Split('_')[1];
                                           if (noTotalRooms != "")
                                           {
                                               Count = Count + Convert.ToInt64(noTotalRooms);
                                               Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                           }
                                           else
                                           {
                                               Booked = Convert.ToInt64(noRooms.Split('_')[2]);
                                           }
                                       }
                                   }
                                   Int64 Sid = 0;
                                   if (listInventory.Count != 0)
                                   {
                                       Sid = listInventory.FirstOrDefault().Sid;
                                       Type = listInventory.FirstOrDefault().RateType;
                                       InventoryType = listInventory.FirstOrDefault().InventoryType;
                                   }
                                   else
                                       InventoryType = "0";

                                   ListDate.Add(new date
                                   {
                                       RateTypeId = Convert.ToInt16(Sid),
                                       datetime = From.AddDays(i).ToString("dd-MM-yyyy"),
                                       day = From.AddDays(0).Day.ToString(),
                                       NoOfCount = Count,
                                       Type = Booked.ToString(),
                                       InventoryType = InventoryType,
                                   });
                               }
                               ListRooms.Add(new RoomType
                               {
                                   RoomTypeName = objRoom.RoomName,
                                   RoomTypeId = objRoom.RoomTypeID.ToString(),
                                   Dates = ListDate
                               });
                           }

                           if (ListRooms.Count != 0)
                           {
                               List<date> Listdate = new List<date>();
                               foreach (var objDates in ListDates)
                               {
                                   Int64 Count = 0;
                                   Int64 NoBooked = 0;
                                   var arrRoomDates = ListRooms.Select(d => d.Dates.Where(r => r.datetime == objDates)).ToList();
                                   foreach (var item in arrRoomDates)
                                   {
                                       Count += item.Select(d => d.NoOfCount).Sum();
                                       NoBooked += item.Select(d => Convert.ToInt64(d.Type)).Sum();
                                   }
                                   if (Listdate.Where(d => d.datetime == objDates).ToList().Count == 0)
                                   {
                                       Listdate.Add(new date
                                       {
                                           datetime = objDates,
                                           NoOfCount = Count,
                                           Type = NoBooked.ToString(),
                                       });
                                   }
                                   else
                                   {
                                       Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().NoOfCount = Listdate.Where(d => d.datetime == objDates).ToList().Count + Count;
                                       Listdate.Where(d => d.datetime == objDates).ToList().FirstOrDefault().Type = (Convert.ToInt64(Listdate.Where(d => d.datetime == objDates).ToList().Count) + NoBooked).ToString();
                                   }
                               }
                               ListInventory.Add(new Inventory
                               {
                                   HotelCode = objHotel.HotelCode,
                                   HotelName = objHotel.HotelName,
                                   Address = objHotel.Address,
                                   ListRoom = ListRooms,
                                   Dates = Listdate,
                               });
                           }
                       }
                       catch (Exception)
                       {
                       }
                   }
               }
           }
           catch (Exception ex)
           {

               // throw new Exception(ex.Message);
           }
           return ListInventory;
       }

    }
}