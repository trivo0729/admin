﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
namespace CutAdmin.DataLayer
{
    public class MarkupTaxManager
    {

        #region Old Markup Work
        public HttpContext Context { get; set; }

        public static int NoNights { get; set; }

        public static string UserCurrency { get; set; }

        public static DataLayer.MarkupsAndTaxes objMarkupsAndTaxes { get; set; }

        public static float GetCutRoomAmount(float BaseAmt, string currency, Int64 NoofRooms, Int64 Night, string Supplier, out float AgentRoomMarkup)
        {
            try
            {
                float CutRoomAmtWithMarkup = 0;
                float CutRoomAmount = 0;

                //For Global Markup Amount // 
                float CutGlobalMarkupAmt = 0; float CutGlobalMarkupPer = 0; float ActualGlobalMarkupAmt = 0;

                //For Group Markup Amount // 
                float CutGroupMarkupAmt = 0; float CutGroupMarkupPer = 0; float ActualGroupMarkupAmt = 0;

                //For Individual Markup Amount // 
                float CutIndividualMarkupAmt = 0; float CutIndividualMarkupPer = 0; float ActualIndividualMarkupAmt = 0;

                //For Agent Own Markup Amount // 
                float ActualAgentRoomMarkup = 0; AgentRoomMarkup = 0; float AgentOwnMarkupPer = 0; float AgentOwnMarkupAmt = 0;

                float GlobalMarkup = 0; float GroupMarkup = 0; float IndividualMarkup = 0;

                float ServiceTaxAmount = 0;
                if (currency == "INR")
                {
                    // CutGlobalMarkup //
                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalMarkupPer;
                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                    // CutGroupMarkup //
                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupMarkupPer;
                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                    // CutIndividualMarkup //
                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                    {
                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                        float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
                        CutIndividualMarkupAmt = (NoofRooms * Night) * IndMarkAmt;
                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                    }


                    CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                    bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
                    if (TaxOnMarkup == true)
                    {
                        ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
                        CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
                    }
                    // Start Agent Markup //

                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                    float AgentMarkupPer = 0;
                    if (objMarkupsAndTaxes.listAgentMarkup != null)
                    {
                        AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                        AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                    }

                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);

                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                    //End Agent Markup // 

                    CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
                    AgentRoomMarkup = ActualAgentRoomMarkup;


                }
                else
                {
                    // CutGlobalMarkup //
                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalMarkupPer;
                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                    // CutGroupMarkup //
                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupMarkupPer;
                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                    // CutIndividualMarkup //
                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                    {
                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                        float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
                        CutIndividualMarkupAmt = (NoofRooms * Night) * IndMarkAmt;
                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                    }


                    CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                    bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
                    if (TaxOnMarkup == true)
                    {
                        ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
                        CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
                    }
                    // Start Agent Markup //

                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                    float AgentMarkupPer = 0;
                    if (objMarkupsAndTaxes.listAgentMarkup != null)
                    {
                        AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                    }
                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                    //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                    //End Agent Markup // 

                    CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
                    AgentRoomMarkup = ActualAgentRoomMarkup;


                }
                return CutRoomAmount;
            }
            catch (Exception ex)
            {
                string Line = ex.StackTrace.ToString();
                throw new Exception("");
            }




        }

        public static float GetMarkupServiceTax(float MarkupAmt, float ServiceTax)
        {
            return (MarkupAmt * ServiceTax / 100);
        }
        #endregion

        #region Markup Module
        public static MarkupCommission objMarkupCommission { get; set; }
        public static MarkupCommission GetMarkupTax(Int64 SupplierID,Int64 HotelCode)
        {
            //dbTaxHandlerDataContext db = new dbTaxHandlerDataContext();
            //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
            MarkupCommission objMarkups = new MarkupCommission();
            objMarkups.S2SMarkup = new Markup();
            objMarkups.GlobalMarkup = new Markup();
            objMarkups.GroupMarkup = new Markup();
            objMarkups.IndividualMarkup = new Markup();
            objMarkups.SupplierMarkup = new Markup();
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                Int64 S2S_ID = 0;
                using (var db = new helperDataContext())
                {
                    //S2S_ID = (from obj in db.tbl_AdminLogins
                    //          join objSupply in db.tbl_CommonHotelMasters on obj.sid equals objSupply.ParentID
                    //                   where objSupply.sid == HotelCode
                    //                   select obj).FirstOrDefault().sid;

                    #region S2S Markup
                    if (S2S_ID != SupplierID)
                    {
                        var arrS2SGlobal = (from obj in db.tbl_GlobalMarkups select obj).FirstOrDefault();
                        if (arrS2SGlobal != null)
                        {
                            objMarkups.S2SMarkup.MarkupPer = Convert.ToSingle(arrS2SGlobal.MarkupPercentage);
                            objMarkups.S2SMarkup.MarkupAmt = Convert.ToSingle(arrS2SGlobal.MarkupAmmount);
                            objMarkups.S2SMarkup.CommessionPer = Convert.ToSingle(arrS2SGlobal.CommessionPercentage);
                            objMarkups.S2SMarkup.CommessionAmt = Convert.ToSingle(arrS2SGlobal.CommessionAmmount);
                        }
                    }

                    #endregion

                    #region Global Markup
                    var arrGlobal = (from obj in db.tbl_GlobalMarkups select obj).FirstOrDefault();
                    if (arrGlobal != null)
                    {
                        objMarkups.GlobalMarkup.MarkupPer = Convert.ToSingle(arrGlobal.MarkupPercentage);
                        objMarkups.GlobalMarkup.MarkupAmt = Convert.ToSingle(arrGlobal.MarkupAmmount);
                        objMarkups.GlobalMarkup.CommessionPer = Convert.ToSingle(arrGlobal.CommessionPercentage);
                        objMarkups.GlobalMarkup.CommessionAmt = Convert.ToSingle(arrGlobal.CommessionAmmount);
                    }
                    #endregion

                    #region Group Markup
                    var arrGroup = (from obj in db.tbl_GroupMarkupDetails
                                    from objMapGroup in db.tbl_AgentGroupMarkupMappings
                                    where obj.Type == 1 && obj.GroupId == objMapGroup.GroupId && objMapGroup.AgentId == Uid
                                    select new
                                    {
                                        obj.MarkupPercentage,
                                        obj.MarkupAmmount,
                                        obj.CommessionPercentage,
                                        obj.CommessionAmmount
                                    }).Distinct().FirstOrDefault();
                    if (arrGroup != null)
                    {
                        objMarkups.GroupMarkup.MarkupPer = Convert.ToSingle(arrGroup.MarkupPercentage);
                        objMarkups.GroupMarkup.MarkupAmt = Convert.ToSingle(arrGroup.MarkupAmmount);
                        objMarkups.GroupMarkup.CommessionPer = Convert.ToSingle(arrGroup.CommessionPercentage);
                        objMarkups.GroupMarkup.CommessionAmt = Convert.ToSingle(arrGroup.CommessionAmmount);
                    }
                    #endregion

                    #region Agent Individual
                    var arrIndividual = (from obj in db.tbl_IndividualMarkups
                                         where obj.AgentId == Uid && obj.Type == 1
                                         select new
                                         {
                                             obj.MarkupPercentage,
                                             obj.MarkupAmmount,
                                             obj.CommessionPercentage,
                                             obj.CommessionAmmount
                                         }).FirstOrDefault();
                    if (arrIndividual != null)
                    {
                        objMarkups.IndividualMarkup.MarkupPer = Convert.ToSingle(arrIndividual.MarkupPercentage);
                        objMarkups.IndividualMarkup.MarkupAmt = Convert.ToSingle(arrIndividual.MarkupAmmount);
                        objMarkups.IndividualMarkup.CommessionPer = Convert.ToSingle(arrIndividual.CommessionPercentage);
                        objMarkups.IndividualMarkup.CommessionAmt = Convert.ToSingle(arrIndividual.CommessionAmmount);
                    }
                    #endregion

                    #region Agent Own Markup
                    var arrSupplierMarkupOwn = (from obj in db.tbl_AgentMarkups where obj.uid == SupplierID && obj.ServiceType == 1 select obj).FirstOrDefault();
                    if (arrSupplierMarkupOwn != null)
                    {
                        objMarkups.SupplierMarkup.MarkupAmt = Convert.ToSingle(arrSupplierMarkupOwn.Amount);
                        objMarkups.GroupMarkup.MarkupPer = Convert.ToSingle(arrSupplierMarkupOwn.Percentage);
                    }
                    #endregion
                }
               
            }
            catch
            {

            }
            return objMarkups;
        }

        public static float GetRoomAmount(float BaseAmt, out float SupplierMarkup,out float S2SMarkup )
        {
            try
            {
                float CutRoomAmtWithMarkup = 0;
                float CutRoomAmount = 0;

                //For Global Markup Amount // 
                float CutGlobalMarkupAmt = 0; float CutGlobalMarkupPer = 0; float ActualGlobalMarkupAmt = 0;

                //For Group Markup Amount // 
                float CutGroupMarkupAmt = 0; float CutGroupMarkupPer = 0; float ActualGroupMarkupAmt = 0;

                //For Individual Markup Amount // 
                float CutIndividualMarkupAmt = 0; float CutIndividualMarkupPer = 0; float ActualIndividualMarkupAmt = 0;

                //For Agent Own Markup Amount // 
                float ActualAgentRoomMarkup = 0; SupplierMarkup = 0; float AgentOwnMarkupPer = 0; float AgentOwnMarkupAmt = 0;

                float GlobalMarkup = 0; float GroupMarkup = 0; float IndividualMarkup = 0;

                float ServiceTaxAmount = 0;

                // Supplier to Supplier Markup
                float S2SMarkupPer = 0, S2SGlobalMarkupPer = 0, ActualS2SMarkupAmt=0; S2SMarkup = 0;
                if (objMarkupCommission.GlobalMarkup != null)
                {
                    S2SMarkupPer = objMarkupCommission.S2SMarkup.MarkupPer;
                    S2SGlobalMarkupPer = ((S2SMarkupPer / 100) * BaseAmt);
                    ActualS2SMarkupAmt = Getgreatervalue(S2SGlobalMarkupPer, objMarkupCommission.S2SMarkup.MarkupAmt);
                    S2SMarkup = ActualS2SMarkupAmt;

                }


                // CutGlobalMarkup //
                float GlobalMarkupPer = 0;
                if (objMarkupCommission.GlobalMarkup != null )
                {
                      GlobalMarkupPer = objMarkupCommission.GlobalMarkup.MarkupPer;
                      CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                      ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, objMarkupCommission.GlobalMarkup.MarkupAmt);
                      GlobalMarkup = ActualGlobalMarkupAmt + ActualS2SMarkupAmt + BaseAmt;
                }
        
            

                // CutGroupMarkup //
                float GroupMarkupPer = objMarkupCommission.GroupMarkup.MarkupPer;
                CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, objMarkupCommission.GroupMarkup.MarkupAmt);
                GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                // CutIndividualMarkup //
                if (objMarkupCommission.IndividualMarkup!= null)
                {
                    float IndMarkupPer = objMarkupCommission.IndividualMarkup.MarkupPer;
                    CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                    float IndMarkAmt = objMarkupCommission.IndividualMarkup.MarkupAmt;
                    CutIndividualMarkupAmt =  IndMarkAmt;
                    ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                    IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                }


                CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;

                //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                float AgentMarkupPer = 0;
                if (objMarkupCommission.SupplierMarkup != null)
                {
                    AgentMarkupPer = objMarkupCommission.SupplierMarkup.MarkupPer;
                    AgentOwnMarkupAmt = objMarkupCommission.SupplierMarkup.MarkupAmt;
                }

                AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);

                ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                //End Agent Markup // 

                CutRoomAmount = (CutRoomAmtWithMarkup);
                SupplierMarkup = ActualAgentRoomMarkup;
                return CutRoomAmount;
            }
            catch (Exception ex)
            {
                string Line = ex.StackTrace.ToString();
                throw new Exception("");
            }




        }
        #endregion
       




     

     

        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

       
    }
}