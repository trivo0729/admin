﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Transactions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class OTBAdminManager
    {
        #region OTList.
        public static DBHelper.DBReturnCode GetOtbList(out DataSet tbl_OTB)
        {
            ////GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            ////Int64 Uid = objGlobalDefaults.sid;
            ////SqlParameter[] sqlParams = new SqlParameter[1];
            ////sqlParams[0] = new SqlParameter("@UID", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OTBLoadAllByAdmin", out tbl_OTB);
            //return retCode;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            retCode = DBHelper.GetDataSet("Proc_tbl_OTBLoadAllByMaster", out tbl_OTB, sqlParams);
            return retCode;
        }
        #endregion

        #region Airports List
        public static DBHelper.DBReturnCode GetAirports(string Airlines, out DataTable tbl_Airports)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@AirlinesName", Airlines);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OpertingAirlineLoadByAirline", out tbl_Airports, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOtbAirports(string Airports, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@City", Airports);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AirportSearchByCity", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOtbMails(string Airlines, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Airlines", Airlines);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OtbChargesDetailsByMail", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region OtbCharges
        public static DBHelper.DBReturnCode GetOtbCharges(string Airline, string Airports, out DataTable tbl_Otb)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Airline", Airline);
            sqlParams[1] = new SqlParameter("@Airports", Airports);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOtbChargesLoadByAirports", out tbl_Otb, sqlParams);
            return retCode;
        }
        #endregion


        #region Update Otb Charges
        public static DBHelper.DBReturnCode UpdateOtbCharges(string Airline, string From, string Currency, decimal OtbFee, decimal UrgentFee)
        {
            int RowEffected;
            SqlParameter[] sqlParams = new SqlParameter[5];
            sqlParams[0] = new SqlParameter("@Airline", Airline);
            sqlParams[1] = new SqlParameter("@Airports", From);
            sqlParams[2] = new SqlParameter("@Currency", Currency);
            sqlParams[3] = new SqlParameter("@OtbFee", OtbFee);
            sqlParams[4] = new SqlParameter("@UrgentFee", UrgentFee);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OtbChargesDetailsUpdate", out RowEffected, sqlParams);
            return retCode;
        }
        #endregion

        #region Search
        public static DBHelper.DBReturnCode SerchOTBAll(string dteTo, string dteFrom, string dteTravel, string FirstName, string LastName, string VisaType, string AirLine, string PnrNo, string uid, out DataTable dtResult)
        {
            //Int64 uid = 0;
            //GlobalDefault objGlobalDefault = new GlobalDefault();
            //objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //uid = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode;
            Int64 sid = 0;
            if (uid != "")
            {
                sid = Convert.ToInt64(uid);
            }
            SqlParameter[] sqlParams = new SqlParameter[9];
            sqlParams[0] = new SqlParameter("@dteTo", dteTo);
            sqlParams[1] = new SqlParameter("@dteFrom", dteFrom);
            sqlParams[2] = new SqlParameter("@dteTravel", dteTravel);
            sqlParams[3] = new SqlParameter("@FirstName", FirstName);
            sqlParams[4] = new SqlParameter("@LastName", LastName);
            sqlParams[5] = new SqlParameter("@VisaType", VisaType);
            sqlParams[6] = new SqlParameter("@AirLine", AirLine);
            sqlParams[7] = new SqlParameter("@PnrNo", PnrNo);
            sqlParams[8] = new SqlParameter("@uid", sid);
            retCode = DBHelper.GetDataTable("Proc_tbl_OTBBySearchAllBYAdminONAgent", out dtResult, sqlParams);
            return retCode;

        }

        //#region Otb Search
        public static DBHelper.DBReturnCode GetOTBbyCode(string Refrence, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@RefrenceNo", Refrence);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByRefNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyVisaNo(string VisaNo, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@VisaNo", VisaNo);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByVisaNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyName(string Name, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@Name", Name);
            ////sqlParams[1] = new SqlParameter("@Uid", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByNameOnAdmin", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Name", Name);
            sqlParams[1] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByNameOnMaster", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyLastName(string LastName, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@LastName", LastName);
            ////sqlParams[1] = new SqlParameter("@Uid", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByLastNameONAdmin", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@LastName", LastName);
            retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByLastNameMaster", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyPnrNo(string PnrNo, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@In_PNR", PnrNo);
            ////sqlParams[1] = new SqlParameter("@Uid", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOtbSearchByPnrNoONAdmin", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            string Franchisee = objGlobalDefault.Franchisee;
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@In_PNR", PnrNo);
            retCode = DBHelper.GetDataTable("Proc_tblOtbSearchByPnrNoOnMaster", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetAgencyName(string AgencyName, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OTBLoadByAgencyName", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[1] = new SqlParameter("@ParentId", UserId);
            retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyNameMaster", out dtResult, sqlParams);
            return retCode;
        }

        //public static DBHelper.DBReturnCode GetAgentName(Int64 uid, out DataTable dtResult)
        //{
        //    SqlParameter[] sqlParams = new SqlParameter[1];
        //    sqlParams[0] = new SqlParameter("@sId", uid);
        //    DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllById", out dtResult, sqlParams);
        //    return retCode;
        //}
        #endregion

        #region Update Mails
        #region Update Otb Charges
        public static DBHelper.DBReturnCode UpdateOtbMails(string Airline, string MailsId, string CcMails)
        {
            int RowEffected;
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Airline", Airline);
            sqlParams[1] = new SqlParameter("@MailsId", MailsId);
            sqlParams[2] = new SqlParameter("@CcMails", CcMails);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OtbChargesDetailsUpdateByMail", out RowEffected, sqlParams);
            return retCode;
        }
        #endregion
        #endregion

        #region Otb Call Center

        #region Get Agent Visa List
        public static DBHelper.DBReturnCode GetVisaByOTB(Int64 AgentId, out DataTable tbl_Visa)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UserId", AgentId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByOTBStatus", out tbl_Visa, sqlParams);
            return retCode;
        }
        #endregion

        #region OtbMarkups
        public static DBHelper.DBReturnCode GetAgentOtbMarkups(Int64 UserId, string Supplier, out DataSet dsResult)
        {
            //Supplier = "Jet";
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@AgentId", UserId);
            sqlparam[1] = new SqlParameter("@Supplier", Supplier);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentOtbMarkpDetailsByAgentId", out dsResult, sqlparam);
            return retCode;
        }
        #endregion

        #region OtbCharges
        public static DBHelper.DBReturnCode GetOtbAgentCharges(Int64 UserId, string Airline, string Airports, out DataSet tbl_Otb)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Airline", Airline);
            sqlParams[1] = new SqlParameter("@Airports", Airports);
            sqlParams[2] = new SqlParameter("@AgentId", UserId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tblOtbChargesLoadByAirportsAndCurrency", out tbl_Otb, sqlParams);
            return retCode;
        }
        #endregion

        #region Emirates
        public static DBHelper.DBReturnCode VisaForEmirates(string[] RefrenceNo, string Airline)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            DataTable dtResult;
            string OTBRefrenceNo = "OTB" + GenerateRandomNumber();
            string VisaNo, VisaType, PassengerName, sDateIssue, PassportNo;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            Int64 ParentID = 0;
            Int64 AdminID = 0;
            int rowsAffected = 0;
            string OTBStatus;
            for (int i = 0; i < RefrenceNo.Length; i++)
            {
                if (i == 0)
                {
                    ParentID = 1;
                }
                else
                {
                    ParentID = 0;
                }
                if (Airline == "EK")
                {
                    OTBStatus = "Applied Directly";
                }
                else
                {
                    OTBStatus = "Otb Not Required";
                }
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@Vcode", RefrenceNo[i]);
                sqlParams[1] = new SqlParameter("@Uid", Uid);
                retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoForOTB", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {

                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    DateTime now = DateTime.Now;
                    string LastUpdateDate = now.ToString("dd-MM-yyyy");
                    string Visa_Path = "../VisaImages/" + RefrenceNo[i] + ".pdf";
                    VisaNo = dtResult.Rows[0]["TReference"].ToString();
                    VisaType = dtResult.Rows[0]["IeService"].ToString();
                    sDateIssue = dtResult.Rows[0]["CopletedDate"].ToString();
                    PassportNo = dtResult.Rows[0]["PassportNo"].ToString();
                    PassengerName = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    SqlParameter[] SQLParams = new SqlParameter[15];
                    SQLParams[0] = new SqlParameter("@uid", Uid);
                    SQLParams[1] = new SqlParameter("@Type", VisaType);
                    SQLParams[2] = new SqlParameter("@Name", PassengerName);
                    SQLParams[3] = new SqlParameter("@VisaRefNo", RefrenceNo[i]);
                    SQLParams[4] = new SqlParameter("@VisaNo", VisaNo);
                    SQLParams[5] = new SqlParameter("@VisaType", VisaType);
                    SQLParams[6] = new SqlParameter("@IssueDate", sDateIssue);
                    SQLParams[7] = new SqlParameter("@Visa_Path", Visa_Path);
                    SQLParams[8] = new SqlParameter("@ParentID ", ParentID);
                    SQLParams[9] = new SqlParameter("@AdminID", AdminID);
                    SQLParams[10] = new SqlParameter("@OTBNo", OTBRefrenceNo);
                    SQLParams[11] = new SqlParameter("@CUTOTB", 1);
                    SQLParams[12] = new SqlParameter("@PassportNo", PassportNo);
                    SQLParams[13] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                    SQLParams[14] = new SqlParameter("@OTBStatus", OTBStatus);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBInsertForEmirates", out rowsAffected, SQLParams);
                }

            }
            return retCode;
        }

        #endregion

        #region Cut OTB Request
        public static DBHelper.DBReturnCode ApllyOtb(Int64 Uid, string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] RefNo, string sRdbType, string OtbRefNo, string Email1, string Email2, string CCEmailId)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Int64 ParentID = 0;
            Int64 AdminID = 0;
            DateTime now = DateTime.Now;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string FranchiseeId = objGlobalDefaults.Franchisee, PassName = "";
            string LastUpdateDate = now.ToString("dd-MM-yyyy");
            int i = 0;
            int rowsAffected = 0;
            DataTable dtResult;
            List<string> CutRef;
            string VisaNo, VisaType, PassengerName = "", sDateIssue, PassportNo = "";
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (i = 0; i < PaxNo; i++)
                {

                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@Vcode", RefNo[i]);
                    sqlParams[1] = new SqlParameter("@Uid", Uid);
                    retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoForOTB", out dtResult, sqlParams);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        retCode = DBHelper.DBReturnCode.EXCEPTION;
                        if (i == 0) { ParentID = 1; }
                        else { ParentID = 0; }
                        VisaNo = dtResult.Rows[0]["VisaNo"].ToString();
                        string VCode = dtResult.Rows[0]["Vcode"].ToString();
                        string Visa_Path = "../VisaImages/" + VCode + ".pdf";
                        VisaType = dtResult.Rows[0]["IeService"].ToString();
                        sDateIssue = dtResult.Rows[0]["CopletedDate"].ToString();
                        PassportNo = dtResult.Rows[0]["PassportNo"].ToString();
                        PassengerName = dtResult.Rows[0]["FirstName"].ToString();
                        string LastName = dtResult.Rows[0]["LastName"].ToString();
                        PassName = PassengerName + " " + LastName;
                        SqlParameter[] SQLParams = new SqlParameter[33];
                        SQLParams[0] = new SqlParameter("@uid", Uid);
                        SQLParams[1] = new SqlParameter("@Type", sRdbType);
                        SQLParams[2] = new SqlParameter("@Name", PassengerName);
                        SQLParams[3] = new SqlParameter("@VisaRefNo", RefNo[i]);
                        SQLParams[4] = new SqlParameter("@VisaNo", VisaNo);
                        SQLParams[5] = new SqlParameter("@VisaType", VisaType);
                        SQLParams[6] = new SqlParameter("@IssueDate", sDateIssue);
                        SQLParams[7] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                        SQLParams[8] = new SqlParameter("@In_AirLine ", ArrivalAirLine);
                        SQLParams[9] = new SqlParameter("@In_DepartureDt ", ArrivalDate);
                        SQLParams[10] = new SqlParameter("@In_FlightNo ", ArrivingFlightNo);
                        SQLParams[11] = new SqlParameter("@In_PNR ", ArrivingPnr);
                        SQLParams[12] = new SqlParameter("@Out_AirLine  ", DepartingAirline);
                        SQLParams[13] = new SqlParameter("@Out_DepartureDt ", DepartingDate);
                        SQLParams[14] = new SqlParameter("@Out_FlightNo ", DerartingFlightNo);
                        SQLParams[15] = new SqlParameter("@Out_PNR ", DerartingPnr);
                        SQLParams[16] = new SqlParameter("@VisaFee", VisaFee);
                        SQLParams[17] = new SqlParameter("@UrgentFee ", UrgentCharges);
                        SQLParams[18] = new SqlParameter("@ServiceCharge", 0);
                        SQLParams[19] = new SqlParameter("@Total", TotalPerPass);
                        SQLParams[20] = new SqlParameter("@Visa_Path", Visa_Path);
                        SQLParams[21] = new SqlParameter("@In_TicketPath", OtbRefNo + "_InBoundTicket.pdf");
                        SQLParams[22] = new SqlParameter("@Out_TicketPath", OtbRefNo + "_OutBoundTicket.pdf");
                        SQLParams[23] = new SqlParameter("@ParentID ", ParentID);
                        SQLParams[24] = new SqlParameter("@AdminID", AdminID);
                        SQLParams[25] = new SqlParameter("@OTBNo", OtbRefNo);
                        SQLParams[26] = new SqlParameter("@CUTOTB", 1);
                        SQLParams[27] = new SqlParameter("@PassportNo", PassportNo);
                        SQLParams[28] = new SqlParameter("@LastName", LastName);
                        SQLParams[29] = new SqlParameter("@DepartingFrom", Arrivalfrom);
                        SQLParams[30] = new SqlParameter("@ArrivingFrom", DepartingFrom);
                        SQLParams[31] = new SqlParameter("@Sponsor", "ClickUrTrip");
                        SQLParams[32] = new SqlParameter("@FranchiseeId", FranchiseeId);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBAddByOtbRequest", out rowsAffected, SQLParams);
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    float BookingAmtWithoutTax = TotalAmmount;
                    //float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["ServiceTax"]);
                    float BookingServiceTax = 0;
                    string BookingDate = LastUpdateDate;
                    string BookedBy = Uid.ToString();
                    string BookingRemark = "";
                    float SupplierBasicAmt = BookingAmtWithoutTax;
                    float CutComm = SupplierBasicAmt;
                    float AgentComm = SupplierBasicAmt;
                    retCode = OTBBookingTxnAdd(OtbRefNo, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, TotalAmmount, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "OTB", SupplierBasicAmt, AgentComm, CutComm, BookedBy, PassengerName, PaxNo);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        float OtbCharges = TotalPerPass - 0;
                        float otbTotalAmmount = OtbCharges * PaxNo;
                        float ServiceCharges = OtbCharges - VisaFee;
                        OTBAdminManager.SendEmailToAgent(ArrivingPnr, ArrivalAirLine, Arrivalfrom, DepartingFrom, PaxNo, RefNo, RefNo, RefNo, RefNo, VisaFee, UrgentCharges, OtherCharges, OtbCharges, otbTotalAmmount, true, OtbRefNo, Uid);
                        objTransactionScope.Complete();
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }
                }
                else
                {
                    objTransactionScope.Dispose();
                }
            }
            return retCode;

        }

        public static DBHelper.DBReturnCode OTBBookingTxnAdd(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName, Int64 PaxNo)
        {
            int rowsaffected;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Int64 uid = Convert.ToInt64(BookedBy);
            string Perticular = "Booking (OTB) " + " " + PassengerName + " x " + PaxNo;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticular);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByOTB", out rowsaffected, sqlParams);
            return retCode;
        }
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
        #endregion

        #region Other Otb Request
        public static DBHelper.DBReturnCode OtherCompanyOtb(Int64 Uid, string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] FirstName, string[] LastName, string[] VisaType, string[] VisaNo, string[] Issue, string[] PassportNo, string[] VisaCopy, string sRdbType, string otbType, string[] Sponsor, string[] SponsorNo, string EmailFormat, string Email1, string Email2, string CCEmailId, string OtbRefNo, string[] DOB, string[] OtherPassportNo, string[] PlaceIssue, string[] IssueCountry, string[] ContactNo)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            string ArrivalTicketPath = "", DepartingTicketPath = "", Out_TicketPath = "";
            Int64 ParentID = 0;
            Int64 AdminID = 0;
            DateTime now = DateTime.Now;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string FranchiseeId = objGlobalDefaults.Franchisee;
            string LastUpdateDate = now.ToString("dd-MM-yyyy");
            int i = 0;
            int rowsAffected = 0;
            string In_TicketPath = ArrivalTicketPath;
            if (ArrivingPnr != DerartingPnr)
            {
                Out_TicketPath = DepartingTicketPath;
            }
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (i = 0; i < PaxNo; i++)
                {
                    if (i == 0)
                    {
                        ParentID = 1;
                    }
                    else
                    {
                        ParentID = 0;

                    }
                    string[] s = VisaCopy[i].Split('\\');
                    int LENGTH = s.Length;
                    string Visa_Path = "../OTBDocument/" + s[LENGTH - 1];
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    SqlParameter[] SQLParams = new SqlParameter[37];
                    SQLParams[0] = new SqlParameter("@uid", Uid);
                    SQLParams[1] = new SqlParameter("@Type", sRdbType);
                    SQLParams[2] = new SqlParameter("@Name", FirstName[i]);
                    SQLParams[3] = new SqlParameter("@VisaRefNo", VisaNo[i]);
                    SQLParams[4] = new SqlParameter("@VisaNo", VisaNo[i]);
                    SQLParams[5] = new SqlParameter("@VisaType", VisaType[i]);
                    SQLParams[6] = new SqlParameter("@IssueDate", Issue[i]);
                    SQLParams[7] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                    SQLParams[8] = new SqlParameter("@In_AirLine ", ArrivalAirLine);
                    SQLParams[9] = new SqlParameter("@In_DepartureDt ", ArrivalDate);
                    SQLParams[10] = new SqlParameter("@In_FlightNo ", ArrivingFlightNo);
                    SQLParams[11] = new SqlParameter("@In_PNR ", ArrivingPnr);
                    SQLParams[12] = new SqlParameter("@Out_AirLine  ", DepartingAirline);
                    SQLParams[13] = new SqlParameter("@Out_DepartureDt ", DepartingDate);
                    SQLParams[14] = new SqlParameter("@Out_FlightNo ", DerartingFlightNo);
                    SQLParams[15] = new SqlParameter("@Out_PNR ", DerartingPnr);
                    SQLParams[16] = new SqlParameter("@VisaFee", VisaFee);
                    SQLParams[17] = new SqlParameter("@UrgentFee ", UrgentCharges);
                    SQLParams[18] = new SqlParameter("@ServiceCharge", OtherCharges);
                    SQLParams[19] = new SqlParameter("@Total", TotalAmmount);
                    SQLParams[20] = new SqlParameter("@Visa_Path", Visa_Path);
                    SQLParams[21] = new SqlParameter("@In_TicketPath", In_TicketPath);
                    SQLParams[22] = new SqlParameter("@Out_TicketPath", Out_TicketPath);
                    SQLParams[23] = new SqlParameter("@ParentID ", ParentID);
                    SQLParams[24] = new SqlParameter("@AdminID", AdminID);
                    SQLParams[25] = new SqlParameter("@OTBNo", OtbRefNo);
                    SQLParams[26] = new SqlParameter("@CUTOTB", 0);
                    SQLParams[27] = new SqlParameter("@PassportNo", PassportNo[i]);
                    SQLParams[28] = new SqlParameter("@LastName", LastName[i]);
                    SQLParams[29] = new SqlParameter("@DepartingFrom", Arrivalfrom);
                    SQLParams[30] = new SqlParameter("@ArrivingFrom", DepartingFrom);
                    SQLParams[31] = new SqlParameter("@Sponsor", Sponsor[i] + "Tel:" + SponsorNo[i]);
                    SQLParams[32] = new SqlParameter("@FranchiseeId", FranchiseeId);
                    SQLParams[33] = new SqlParameter("@DOB", DOB[i]);
                    //SQLParams[34] = new SqlParameter("@OtherPassportNo", OtherPassportNo[i]);
                    SQLParams[34] = new SqlParameter("@IssuingPlace", PlaceIssue[i]);
                    SQLParams[35] = new SqlParameter("@IssuingCountry", IssueCountry[i]);
                    SQLParams[36] = new SqlParameter("@ContactNo", ContactNo[i]);

                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBAddByOtbRequestNew", out rowsAffected, SQLParams);
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
                    float AvailableCrdit = 0;
                    float BookingAmtWithoutTax = TotalAmmount;
                    //float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["ServiceTax"]);
                    float BookingServiceTax = 0;
                    string BookingDate = LastUpdateDate;
                    string BookedBy = Uid.ToString();
                    string BookingRemark = "";
                    float SupplierBasicAmt = BookingAmtWithoutTax;
                    float CutComm = SupplierBasicAmt;
                    float AgentComm = SupplierBasicAmt;
                    retCode = OTBBookingTxnOther(OtbRefNo, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "OTB", SupplierBasicAmt, AgentComm, CutComm, BookedBy, FirstName[0], PaxNo);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
                        float OtbCharges = TotalPerPass;
                        float otbTotalAmmount = OtbCharges * PaxNo;
                        float ServiceCharges = OtbCharges - VisaFee;
                        OTBAdminManager.SendEmailToAgent(ArrivingPnr, ArrivalAirLine, Arrivalfrom, DepartingFrom, PaxNo, FirstName, LastName, PassportNo, VisaNo, VisaFee, UrgentCharges, OtherCharges, OtbCharges, otbTotalAmmount, false, OtbRefNo, Uid);
                        objTransactionScope.Complete();
                        //retCode = Airlinemail(ArrivingPnr, DerartingPnr, ArrivalDate, FirstName, LastName, VisaNo, VisaCopy, VisaType, Issue, false, Email1, Email2, CCEmailId, OtbRefNo, MailSubject(ArrivalAirLine, PaxNo, FirstName[0] + " " + LastName[0]), ArrivalAirLine);

                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }




                }
                else
                {
                    objTransactionScope.Dispose();
                }


            }
            return retCode;

        }

        public static DBHelper.DBReturnCode OTBBookingTxnOther(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName, Int64 PaxNo)
        {
            int rowsaffected;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Int64 uid = Convert.ToInt64(BookedBy);
            string Perticular = "Booking (OTB)" + PassengerName + "*" + PaxNo;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticular);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByOTB", out rowsaffected, sqlParams);
            return retCode;
        }

        #endregion

        #region Otb Mail Sending
        #region Visa Load By Code
        public static DBHelper.DBReturnCode VisaLoadByCode(string Vcode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", Vcode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Otb Mail Subject
        public static string MailSubject(string Airlines, Int64 PaxNo, string PassengerName)
        {
            string Subject = "";
            if (Airlines == "Indigo Airlines ") { Subject = "6E OTB – AGENT CODE: 00348 - " + PaxNo.ToString() + " PAX"; }
            else if (Airlines == "Air India") { Subject = "Fwd: AI - OK TO BOARD REQUEST - ( " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Air India Express") { Subject = "Fwd: IX- OK TO BOARD REQUEST- " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Spicejet Airlines") { Subject = "APPLY OTB " + PassengerName + "+" + PaxNo.ToString() + " PAX )"; }
            else if (Airlines == "Air Arabia") { Subject = "APPLY OTB " + PassengerName + "(" + PaxNo.ToString() + "PAX )"; }
            else { Subject = "OK TO BOARD REQUEST- " + PassengerName + " (" + PaxNo.ToString() + "- PAX )"; }
            return Subject;
        }
        #endregion

        #region New Mail Format
        public static DBHelper.DBReturnCode Airlinemail(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string OtbRefNo, string sSubject, string Airline)
        {
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            string VisaLinks;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
            sb.Append("<br />");
            sb.Append(AirlineMailFormat(Pnr, DerartingPnr, TravelDate, PassengerName, LastName, VisaNo, VisaCopy, VisaType, Issue, Cut, Airline, OtbRefNo, out VisaLinks));
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            try
            {

                int effected = 0;
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@sTo", Email1);
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                sqlParams[4] = new SqlParameter("@Cc", CCEmailId);
                retcode = DBHelper.ExecuteNonQuery("Proc_AirlinesOTBMail", out effected, sqlParams);
                return retcode;
            }
            catch
            {
                return retcode;
            }
        }
        #endregion

        #region Send Mail to Agent
        public static bool SendEmailToAgent(string ArrivingPnr, string ArrivalAirLine, string Arrivalfrom, string DepartingFrom, Int64 PaxNo, string[] RefNo, string[] PassengerName, string[] PassportNo, string[] VisaNo, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, float TotalAmmount, bool Cut, string OtbRefNo, Int64 AgentId)
        {
            DataTable dtAdminLogin = new DataTable();
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", AgentId);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadByAgentMail", out dtAdminLogin, sqlParams);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string sAgencyName = dtAdminLogin.Rows[0]["AgencyName"].ToString();
                string TO = dtAdminLogin.Rows[0]["uid"].ToString();
                string AgentCurrency = dtAdminLogin.Rows[0]["CurrencyCode"].ToString();
                string sSubject = "";
                string Currency = "";
                if (AgentCurrency == "INR")
                {
                    Currency = "<img src=\"http://clickurtrip.com/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (AgentCurrency == "AED")
                {
                    Currency = "AED ";
                }
                else if (AgentCurrency == "USD")
                {
                    Currency = "$ ";
                }
                StringBuilder sb = new StringBuilder();
                if (Cut == true)
                {

                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                    sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");
                    string Name, PermitNo, No;
                    DataTable dtResult;
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                        sSubject = "Thanks for OTB request for " + Name + " * " + Convert.ToString(PassengerName.Length);
                        PermitNo = dtResult.Rows[0]["VisaNo"].ToString();
                        No = dtResult.Rows[0]["PassportNo"].ToString();
                        sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                        sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + Name + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + No + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + PermitNo + "<o:p></o:p></span></b></p></td>");
                        sb.Append("</tr>");

                    }
                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody></table></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                }
                else
                {
                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    //sb.Append("<div style=\"height:50px;width:auto\">");
                    ////sb.Append("<br />");
                    ////sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                    //sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                    //sb.Append("<br />");
                    //sb.Append("<br />");
                    sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");
                    sSubject = "Thanks for OTB request for " + RefNo[0] + " " + PassengerName[0] + " * " + Convert.ToString(PassengerName.Length);
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                        sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + RefNo[i] + " " + PassengerName[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + PassportNo[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + VisaNo[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("</tr>");

                    }
                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody></table></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\";padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");


                }





                try
                {
                    System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    mailMsg.To.Add(TO);
                    mailMsg.Subject = sSubject;
                    mailMsg.IsBodyHtml = true;
                    mailMsg.Body = sb.ToString();
                    SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = true;
                    mailObj.Send(mailMsg);
                    mailMsg.Dispose();
                    return true;

                }
                catch
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Airlines Format
        public static string AirlineMailFormat(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool IsCutOtb, string Airline, string OtbRefNo, out string VisaLink)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dtResult;
            bool SamePnr = false;
            if (Pnr == DerartingPnr)
            {
                SamePnr = true;
            }
            string PaxInfo = "";
            #region Indigo Mail
            if (Airline == "Indigo Airlines ")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left: 2%;background:white;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">SR. NO</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot; color:#222222\">PAX NAME</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#222222\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td></tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"> " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Pnr + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"></span></span><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"> " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Pnr + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region SpiceJet
            else if (Airline == "Spicejet Airlines")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\"><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR.NO</span></b></p></td>");
                sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">NO OF PASSENGER</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">TRAVEL DATE</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td></tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region Air Arabia
            else if (Airline == "Air Arabia" || Airline == "Oman Airways" || Airline == "Fly Dubai")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA TYPE</span></b></p></td>");
                sb.Append("</tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[0]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[0]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[0]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[0]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[0]["VisaNo"]) + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[0]["IeService"]) + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaType[i] + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region AirIndiaExp
            else if (Airline == "Air India Express" || Airline == "Air India")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">Travel Date</span></b></p></td>");
                sb.Append("</tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["VisaNo"]) + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region Visa Link
            if (IsCutOtb == true)
            {
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetCutVisaPath(PassengerName);
            }
            else
            {
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
            }
            #endregion

            return PaxInfo = sb.ToString();

        }
        #endregion

        #region Otb File Path
        public static string GetCutVisaPath(string[] RefNo)
        {
            string Path = "";
            string Colun = ";";
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (RefNo.Length - 1 != i)
                {
                    Path += "C:\\inetpub\\wwwroot\\VisaImages\\" + RefNo[i] + "_Visa.pdf;";
                }
                else
                {
                    Path += "C:\\inetpub\\wwwroot\\VisaImages\\" + RefNo[i] + "_Visa.pdf";
                }

            }
            return Path;

        }
        public static string GetOtherVisaPath(string[] RefNo, string OtbId)
        {
            string Path = "";
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (i != (RefNo.Length - 1))
                {
                    Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf;";
                }
                else
                {
                    Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf";
                }

            }
            return Path;

        }
        public static string GetTicketCopy(string OtbId, bool SamePnr)
        {
            string Path = "";
            string InExt = "";
            string OutExt = "";
            if (SamePnr == false)
            {
                InExt = HttpContext.Current.Session["InboundFileName"].ToString();
                OutExt = HttpContext.Current.Session["OutboundFileName"].ToString();
            }
            else
            {
                InExt = HttpContext.Current.Session["InboundFileName"].ToString();
                OutExt = InExt;
            }

            if (SamePnr == false)
            {
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_InBoundTicket" + InExt + ";";
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_OutBoundTicket" + OutExt;
            }
            else
            {
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_InBoundTicket" + InExt;
            }

            return Path;
        }
        #endregion

        #endregion
        #endregion

    }
}