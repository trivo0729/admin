﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class OTBEmailManager
    {
        #region OtbLoadByReferenceNo
        public static DBHelper.DBReturnCode OtbLoadByRefrenceNo(string RefrenceNo, Int64 UserId, out DataSet dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@RefrenceNo", RefrenceNo);
            sqlParams[1] = new SqlParameter("@UserId", UserId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_OTBLoadByRefrenceNoAndUid", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Otb Update Conformation
        public static DBHelper.DBReturnCode ConformationEmailToAgent(string RefrenceNo, Int64 UsreId)
        {
            string sSubject = "", ArrivalAirLine = "", sAgencyName = "", ArrivingPnr = "", Arrivalfrom = "", PaxNo = "", DepartingFrom = "";
            StringBuilder sb = new StringBuilder();
            DataSet dsResult;
            DataTable dtOtbdetails = null, dtAgentDetails = null;
            OtbLoadByRefrenceNo(RefrenceNo, UsreId, out dsResult);
            dtOtbdetails = dsResult.Tables[0];
            dtAgentDetails = dsResult.Tables[1];
            sAgencyName = dtAgentDetails.Rows[0]["AgencyName"].ToString();
            ArrivalAirLine = dtOtbdetails.Rows[0]["Out_AirLine"].ToString();
            ArrivingPnr = dtOtbdetails.Rows[0]["In_PNR"].ToString();
            Arrivalfrom = dtOtbdetails.Rows[0]["ArrivingFrom"].ToString();
            DepartingFrom = dtOtbdetails.Rows[0]["DepartingFrom"].ToString();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear  " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Your OTB request for UAE is update successfully & your passenger(s) given below are ready to fly<o:p></o:p></span></p></span><br /><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left:10%\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left:4%\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left:3.5%\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left:7%\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left:5%\"><span style='mso-tab-count:2'></span>" + dtOtbdetails.Rows.Count + "<o:p></o:p></span></p></span><br />");
            sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
            sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
            sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
            sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
            sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
            sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
            sb.Append("</tr>");
            string Name, PermitNo, No;
            string Email = "";
            Email = dtAgentDetails.Rows[0]["uid"].ToString();
            sSubject = dtOtbdetails.Rows[0]["Name"].ToString() + " * " + Convert.ToString(dtOtbdetails.Rows.Count) + "OTB Updated Successfully";
            for (int i = 0; i < dtOtbdetails.Rows.Count; i++)
            {

                Name = dtOtbdetails.Rows[i]["Name"].ToString() + " " + dtOtbdetails.Rows[i]["LastName"].ToString();
                PermitNo = dtOtbdetails.Rows[i]["VisaNo"].ToString();
                No = dtOtbdetails.Rows[i]["PassportNo"].ToString();
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + Name + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + No + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + PermitNo + "<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400;color: red\">Request you to kindly re check with airline to ensure if the required message is reflecting at their end.</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\">Thank you very much for given us this opportunity and we expect you to remember</span><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\">us in all your future services related to Dubai & Saudi Arabia.</span><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 90%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            try
            {
                string sTo = Email;
                int roweffected = 0;
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@sTo", sTo);
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out roweffected, sqlParams);
                return DBHelper.DBReturnCode.SUCCESS;

            }
            catch
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }
        }
        #endregion

        #region Update Otb Status
        public static DBHelper.DBReturnCode UpdateOtbStatus(string RefrenceNo, string Status)
        {
            int RowEffected;
            StringBuilder sb = new StringBuilder();
            DataSet dsResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string UpdateBy = objGlobalDefaults.AgencyName;
            DataTable dtAgentDetails, dtOtbDetails, dtFranchisee;
            string ContactPerson = "", Location = "", Franchisee = "", ArrivalAirLine = "", ArrivingPnr = "", Arrivalfrom = "", OtbRefNo = "", sAgencyName = "", FranchiseeMail = "", Name = "", PermitNo = "", sSubject = "";
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            try
            {
                retCode = OtbLoadByRefrenceNo(RefrenceNo, 0, out dsResult);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    dtOtbDetails = dsResult.Tables[0];
                    dtAgentDetails = dsResult.Tables[1];
                    dtFranchisee = dsResult.Tables[2];
                    if (dtAgentDetails.Rows.Count != 0)
                    {
                        ContactPerson = dtAgentDetails.Rows[0]["AgencyName"].ToString();
                        Location = dtAgentDetails.Rows[0]["Address"].ToString() + " -" + dtAgentDetails.Rows[0]["PinCode"].ToString();
                    }
                    if (dtFranchisee.Rows.Count != 0)
                    {
                        if (dtFranchisee.Rows[0]["UserType"].ToString() == "Franchisee")
                        {
                            Franchisee = dtFranchisee.Rows[0]["AgencyType"].ToString();
                            FranchiseeMail = dtFranchisee.Rows[0]["uid"].ToString();
                        }
                        else
                        {
                            Franchisee = "ClickUrTrip.com";
                        }
                    }
                    if (dtOtbDetails.Rows.Count != 0)
                    {
                        Name = dtOtbDetails.Rows[0]["Name"].ToString() + " " + dtOtbDetails.Rows[0]["LastName"].ToString();
                        PermitNo = dtOtbDetails.Rows[0]["VisaNo"].ToString();
                        ArrivalAirLine = dtOtbDetails.Rows[0]["In_AirLine"].ToString();
                        ArrivingPnr = dtOtbDetails.Rows[0]["In_PNR"].ToString();
                        Arrivalfrom = dtOtbDetails.Rows[0]["ArrivingFrom"].ToString();

                    }

                }
                sSubject = "OTB for application " + RefrenceNo + ", is " + Status.Replace("Processing-Applied", "Applied").Replace("Otb Updated", "Approved").Replace("Otb Not Required", "Not Required").Replace("Processing", "Recived");
                sb.Append("<!DOCTYPE html>");
                sb.Append(" <html lang=\"en\"  xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append(" <head>");
                sb.Append("<meta charset=\"utf-8\" />  ");
                sb.Append("</head> ");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\"> ");
                sb.Append(" <div  height:\"5px\"> ");
                sb.Append("</div>  ");
                sb.Append(" <div style=\"height:50px;width:auto\">");
                sb.Append("<br /> ");
                //sb.Append("<img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
                sb.Append("</div>");
                sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />  ");
                sb.Append("<div> ");
                sb.Append("<div style=\"margin-left:5%\"> ");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">OTB application RefrenceNo," + Status.Replace("Processing-Applied", "Applied").Replace("Otb Updated", "Approved").Replace("Otb Not Required", "Not Required").Replace("Processing", "Recived") + "</span><br /><br /><br />");
                sb.Append("<table style=\"width:100%\">");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Agency Name :</span></td><td><b>" + ContactPerson + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Location :</span><br /><br /></td><td><b>" + Location + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Under :</span><br /><br /></td><td><b>" + Franchisee + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Airline : </span><br /><br /></td><td><b>" + ArrivalAirLine + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Airline PNR : </span><br /><br /></td><td><b>" + ArrivingPnr + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Arrival From : </span><br /><br /></td><td><b>" + Arrivalfrom + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Application No : </span><br /><br /></td><td><b>" + RefrenceNo + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">PAX Name : </span><br /><br /></td><td><b>" + Name + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Visa No : </span><br /><br /></td><td><b>" + PermitNo + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Update by: </span><br /><br /><td><td><b>" + UpdateBy + "</b></td></tr>");
                sb.Append("</table>");
                sb.Append("<span style=\"margin-left:2%\">");
                sb.Append("<b>Thank You,</b><br />  ");
                sb.Append("</span>");
                sb.Append("<span style=\"margin-left:2%\">");
                sb.Append("Administrator");
                sb.Append(" </span> ");
                sb.Append("</div>  ");
                sb.Append("</div>  ");
                sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />  ");
                sb.Append("<div>  ");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">  ");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>  ");
                sb.Append("</tr>  ");
                sb.Append("  </table>  ");
                sb.Append(" </div>  ");
                sb.Append(" </body>  ");
                sb.Append(" </html>");
                string Mail = sb.ToString();
                SqlParameter[] sqlParams = new SqlParameter[4];
                int effected = 0;
                if (Status == "Processing-Applied")
                {
                    sqlParams[0] = new SqlParameter("@Type", "OTB");
                    sqlParams[1] = new SqlParameter("@Activity", "OTB Applied");
                }
                else if (Status == "Otb Updated")
                {
                    sqlParams[0] = new SqlParameter("@Type", "OTB");
                    sqlParams[1] = new SqlParameter("@Activity", "OTB Approved");
                }
                else if (Status == "Otb Not Required")
                {
                    sqlParams[0] = new SqlParameter("@Type", "OTB");
                    sqlParams[1] = new SqlParameter("@Activity", "If OTB not required");
                }
                sqlParams[2] = new SqlParameter("@sSubject", sSubject);
                sqlParams[3] = new SqlParameter("@VarBody", sb.ToString());
                retCode = DBHelper.ExecuteNonQuery("Proc_ActivityMail", out effected, sqlParams);
                if (retCode == BL.DBHelper.DBReturnCode.SUCCESSNOAFFECT)
                {
                    Status = Status.Replace("Processing-Applied", "Applied");
                    SqlParameter[] sqlParams1 = new SqlParameter[2];
                    sqlParams1[0] = new SqlParameter("@OTBNo", RefrenceNo);
                    sqlParams1[1] = new SqlParameter("@Status", Status);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tblOTBUpdateByStatus", out RowEffected, sqlParams1);
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            //Status = Status.Replace("Processing-Applied", "Applied");
            //SqlParameter[] sqlParams = new SqlParameter[2];
            //sqlParams[0] = new SqlParameter("@OTBNo", RefrenceNo);
            //sqlParams[1] = new SqlParameter("@Status", Status);
            //DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tblOTBUpdateByStatus", out RowEffected, sqlParams);
            return retCode;
        }
        #region Get Franchisee Details On Agent
        public static DBHelper.DBReturnCode GetFranchiseeDetais(Int64 AgentId, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@AgentId", AgentId);
            retCode = DBHelper.GetDataSet("Proc_GetAgentFranchiseeByAgent", out dsResult, sqlParams);

            return retCode;

        }
        #endregion Get Franchisee Details On Agent
        #endregion

        #region Airline Mails
        #region GetOtb By RefNo
        public static DBHelper.DBReturnCode AirlinesOtbMail(string RefNo, Int64 Uid)
        {
            DBHelper.DBReturnCode retcode;
            DataSet dsResult = new DataSet();
            retcode = OtbLoadByRefrenceNo(RefNo, Uid, out dsResult);
            if (dsResult.Tables[0].Rows.Count != 0)
            {
                string Pnr, DerartingPnr, TravelDate, Email1, Email2 = "", CCEmailId, OtbRefNo = RefNo, sSubject = "", Airline;
                bool Cut = false;
                if (Convert.ToString(dsResult.Tables[0].Rows[0]["CUTOTB"]) == null || Convert.ToString(dsResult.Tables[0].Rows[0]["CUTOTB"]) == "")
                {
                    Cut = false;
                }
                else
                {
                    Cut = Convert.ToBoolean(dsResult.Tables[0].Rows[0]["CUTOTB"]);
                }
                Pnr = Convert.ToString(dsResult.Tables[0].Rows[0]["In_PNR"]);
                DerartingPnr = Convert.ToString(dsResult.Tables[0].Rows[0]["Out_PNR"]);
                TravelDate = Convert.ToString(dsResult.Tables[0].Rows[0]["In_DepartureDt"]);
                Airline = Convert.ToString(dsResult.Tables[0].Rows[0]["In_AirLine"]);
                retcode = GetAirlineMails(Airline, out Email1, out CCEmailId);
                string[] PassengerName = new string[dsResult.Tables[0].Rows.Count], LastName = new string[dsResult.Tables[0].Rows.Count], VisaNo = new string[dsResult.Tables[0].Rows.Count], VisaCopy = new string[dsResult.Tables[0].Rows.Count], VisaType = new string[dsResult.Tables[0].Rows.Count], Issue = new string[dsResult.Tables[0].Rows.Count];
                string[] DOB = new string[dsResult.Tables[0].Rows.Count], PassportNo = new string[dsResult.Tables[0].Rows.Count], PlaceIssue = new string[dsResult.Tables[0].Rows.Count], IssuingCountry = new string[dsResult.Tables[0].Rows.Count], ContactNo = new string[dsResult.Tables[0].Rows.Count], SponsorName = new string[dsResult.Tables[0].Rows.Count];
                for (int i = 0; i < dsResult.Tables[0].Rows.Count; i++)
                {
                    PassengerName[i] = Convert.ToString(dsResult.Tables[0].Rows[i]["Name"]);
                    LastName[i] = Convert.ToString(dsResult.Tables[0].Rows[i]["LastName"]);
                    VisaNo[i] = Convert.ToString(dsResult.Tables[0].Rows[i]["VisaRefNo"]);
                    VisaCopy[i] = Convert.ToString(dsResult.Tables[0].Rows[i]["VisaRefNo"] + "_Visa.pdf");
                    Issue[i] = Convert.ToString(dsResult.Tables[0].Rows[i]["IssueDate"]);

                    // Indigo Airline 
                    DOB[i] = dsResult.Tables[0].Rows[i]["DOB"].ToString();
                    PlaceIssue[i] = dsResult.Tables[0].Rows[i]["PlaceIssue"].ToString();
                    IssuingCountry[i] = dsResult.Tables[0].Rows[i]["IssuingCountry"].ToString();
                    ContactNo[i] = dsResult.Tables[0].Rows[i]["ContactNo"].ToString();
                    PassportNo[i] = dsResult.Tables[0].Rows[i]["PassportNo"].ToString();
                    SponsorName[i] = dsResult.Tables[0].Rows[i]["Sponsor"].ToString();

                }
                sSubject = MailSubject(Airline, dsResult.Tables[0].Rows.Count, PassengerName[0] + " " + LastName[0]);
                Airlinemail(Pnr, DerartingPnr, TravelDate, PassengerName, LastName, VisaNo, VisaCopy, VisaType, Issue, Cut, Email1, Email2, CCEmailId, OtbRefNo, sSubject, Airline, DOB, PlaceIssue, IssuingCountry, ContactNo, PassportNo, SponsorName);
            }
            else
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }
        public static DBHelper.DBReturnCode GetAirlineMails(string Airlines, out string Mail, out string CcMail)
        {
            DataTable dtResult = new DataTable();
            if (Airlines == "Air Arabia") { Airlines = "G9"; }
            else if (Airlines == "Air India") { Airlines = "AI"; }
            else if (Airlines == "Air India Express") { Airlines = "Ix"; }
            else if (Airlines == "Jet Airways Airlines") { Airlines = "9w"; }
            else if (Airlines == "Indigo Airlines ") { Airlines = "G9"; }
            else if (Airlines == "Oman Airways") { Airlines = "6E"; }
            else if (Airlines == "Spicejet Airlines") { Airlines = "Wy"; }
            else if (Airlines == "Emirates Airlines") { Airlines = "SG"; }
            else if (Airlines == "Etihad Airways") { Airlines = "EY"; }
            else if (Airlines == "Fly Dubai") { Airlines = "FZ"; }
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Airline", Airlines);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OtbChargesDetailsloadByMails", out dtResult, sqlParams);
            if (retCode == BL.DBHelper.DBReturnCode.SUCCESS)
            {
                Mail = Convert.ToString(dtResult.Rows[0]["Email1"]);
                CcMail = Convert.ToString(dtResult.Rows[0]["CCEmailId"]);
            }
            else
            {
                Mail = "";
                CcMail = "";
            }
            return retCode;
        }
        #endregion

        #region New Mail Format
        public static DBHelper.DBReturnCode Airlinemail(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string OtbRefNo, string sSubject, string Airline, string[] DOB, string[] PlaceIssue, string[] IssuingCountry, string[] ContactNo, string[] PassportNo, string[] SponsorName)
        {
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            string VisaLinks;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
            sb.Append("<br />");
            sb.Append(AirlineMailFormat(Pnr, DerartingPnr, TravelDate, PassengerName, LastName, VisaNo, VisaCopy, VisaType, Issue, Cut, Airline, OtbRefNo, out VisaLinks, DOB, PlaceIssue, IssuingCountry, ContactNo, PassportNo, SponsorName));
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            try
            {

                //int effected = 0;
                //SqlParameter[] sqlParams = new SqlParameter[5];
                //sqlParams[0] = new SqlParameter("@sTo", Email1);
                //sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                //sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                //sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                //sqlParams[4] = new SqlParameter("@Cc", CCEmailId);
                //retcode = DBHelper.ExecuteNonQuery("Proc_AirlinesOTBMail", out effected, sqlParams);
                //return retcode;

                List<string> from = new List<string>();

                from.Add(Convert.ToString(ConfigurationManager.AppSettings["OTBMail"]));
                List<string> VisaLinksList = new List<string>();

                //VisaLinksList.Add(Convert.ToString(VisaLinks));
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in Email1.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                foreach (string link in VisaLinks.Split(';'))
                {
                    if (link != "")
                    {
                        VisaLinksList.Add(link);
                    }
                }

                //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, sSubject, sb.ToString(), from, VisaLinksList);
                if (reponse == true)
                {
                    retcode = BL.DBHelper.DBReturnCode.SUCCESS;
                }
                return retcode;
            }
            catch
            {
                return retcode;
            }
        }
        #endregion

        #region Send Mail to Agent
        public static bool SendEmailToAgent(string ArrivingPnr, string ArrivalAirLine, string Arrivalfrom, string DepartingFrom, Int64 PaxNo, string[] RefNo, string[] PassengerName, string[] PassportNo, string[] VisaNo, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, float TotalAmmount, bool Cut, string OtbRefNo, Int64 AgentId)
        {
            DataTable dtAdminLogin = new DataTable();
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", AgentId);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadByAgentMail", out dtAdminLogin, sqlParams);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string sAgencyName = dtAdminLogin.Rows[0]["AgencyName"].ToString();
                string TO = dtAdminLogin.Rows[0]["uid"].ToString();
                string AgentCurrency = dtAdminLogin.Rows[0]["CurrencyCode"].ToString();
                string sSubject = "";
                string Currency = "";
                if (AgentCurrency == "INR")
                {
                    Currency = "<img src=\"http://clickurtrip.com/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (AgentCurrency == "AED")
                {
                    Currency = "AED ";
                }
                else if (AgentCurrency == "USD")
                {
                    Currency = "$ ";
                }
                StringBuilder sb = new StringBuilder();
                if (Cut == true)
                {

                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                    sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");
                    string Name, PermitNo, No;
                    DataTable dtResult;
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                        sSubject = "Thanks for OTB request for " + Name + " * " + Convert.ToString(PassengerName.Length);
                        PermitNo = dtResult.Rows[0]["VisaNo"].ToString();
                        No = dtResult.Rows[0]["PassportNo"].ToString();
                        sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                        sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + Name + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + No + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + PermitNo + "<o:p></o:p></span></b></p></td>");
                        sb.Append("</tr>");

                    }
                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody></table></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                }
                else
                {
                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    //sb.Append("<div style=\"height:50px;width:auto\">");
                    ////sb.Append("<br />");
                    ////sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                    //sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                    //sb.Append("<br />");
                    //sb.Append("<br />");
                    sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");
                    sSubject = "Thanks for OTB request for " + RefNo[0] + " " + PassengerName[0] + " * " + Convert.ToString(PassengerName.Length);
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                        sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + RefNo[i] + " " + PassengerName[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + PassportNo[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + VisaNo[i] + "<o:p></o:p></span></b></p></td>");
                        sb.Append("</tr>");

                    }
                    sb.Append("</table>");
                    sb.Append("<br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                    sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                    sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody></table></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                    sb.Append("<br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                    sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\";padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("<div style=\"height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");


                }





                try
                {
                    System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    mailMsg.To.Add(TO);
                    mailMsg.Subject = sSubject;
                    mailMsg.IsBodyHtml = true;
                    mailMsg.Body = sb.ToString();
                    SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = true;
                    mailObj.Send(mailMsg);
                    mailMsg.Dispose();
                    return true;

                }
                catch
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Airlines Format
        public static string AirlineMailFormat(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool IsCutOtb, string Airline, string OtbRefNo, out string VisaLink, string[] DOB, string[] PlaceIssue, string[] IssuingCountry, string[] ContactNo, string[] PassportNo, string[] SponsorName)
        {
            string PaxInfo = "";
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dtResult;
                bool SamePnr = false;
                if (Pnr == DerartingPnr)
                {
                    SamePnr = true;
                }

                #region Indigo Mail
                if (Airline == "Indigo Airlines ")
                {
                    //sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left: 2%;background:white;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                    //sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    //sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">SR. NO</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                    //sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot; color:#222222\">PAX NAME</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#222222\"><o:p></o:p></span></p></td>");
                    //sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td></tr>");
                    if (IsCutOtb == true)
                    {
                        for (int i = 0; i < VisaNo.Length; i++)
                        {
                            VisaLoadByCode(VisaNo[i], out dtResult);
                            //sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            //sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                            //sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"> " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span></p></td>");
                            //sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Pnr + "</span></p></td>");
                            //sb.Append("</tr>");
                            sb.Append("<table class=\"yiv7548598392m_6777535398107067616gmail-m_-1426882243265148272gmail-m_-1322221495191294578gmail-m_2616610884023316279gmail-m_-5914916574228658407gmail-m_6940654179228033455gmail-m_2065586898356323177gmail-m_-4767676071881556798gmail-m_-7293088675286340295gmail-m_-2650746283250910099gmail-MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"747\" style=\"font-family:verdana, sans-serif;width:560pt;margin-left:4.75pt;border-collapse:collapse;\" id=\"yui_3_16_0_ym19_1_1489138076467_10818\"><tbody id=\"yui_3_16_0_ym19_1_1489138076467_10817\">");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10843\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10842\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10841\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10844\">PNR</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:1pt solid windowtext;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:none;padding:0in 5.4pt;min-height:15pt;\">*" + Pnr + "<b><span style=\"font-size:10pt;line-height:15.3333px;\"><font color=\"#222222\">*</font></span></b><br></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10816\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10815\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10814\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10813\">PAX FIRST NAME</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\">" + dtResult.Rows[0]["FirstName"].ToString() + "&nbsp;<br></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">PAX LAST NAME</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\">&nbsp;" + dtResult.Rows[0]["LastName"].ToString() + "<span style=\"font-size:12.8px;\">&nbsp;</span><br></td></tr><tr style=\"min-height:15pt;\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">Passport No.</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + PassportNo[i] + "<br></p></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10928\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10927\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10926\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10925\">Date Of Birth</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + dtResult.Rows[0]["Birth"].ToString() + "<br></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10932\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10931\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10930\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10929\">Passport issuing Country</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">" + dtResult.Rows[0]["IssuingGovernment"].ToString() + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10935\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10934\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10933\"><span style=\"font-size:12pt;\">&nbsp;</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">&nbsp;</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10869\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10868\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10867\"><span style=\"font-size:12pt;\">Visa type</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">" + dtResult.Rows[0]["IeService"].ToString() + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10866\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10865\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10864\"><span style=\"font-size:12pt;\">Visa No.</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + dtResult.Rows[0]["VisaNo"].ToString() + "&nbsp;</p></td></tr>");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10924\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10923\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10922\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10921\">Place of Issue</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\">" + dtResult.Rows[0]["City"].ToString() + "</font></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10920\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10919\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10918\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10917\">Visa Issue Date</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\"><span style=\"font-size:16px;\">" + dtResult.Rows[0]["IssuingDate"].ToString() + "</span></font><br></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10863\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10862\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10861\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10860\">Sponsor Name &amp; Address</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:16px;\">" + dtResult.Rows[0]["Sponsor"].ToString() + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10859\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10858\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10857\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10856\">Contact No.</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\"><span style=\"font-size:16px;\">" + ContactNo[i] + ",</span></font><br></p></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10855\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10854\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10853\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10852\">Email Id and Contact Person name</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><br></p></td></tr>");
                            sb.Append("</tbody>");
                            sb.Append("</table> <br/>");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < PassengerName.Length; i++)
                        {
                            sb.Append("<table class=\"yiv7548598392m_6777535398107067616gmail-m_-1426882243265148272gmail-m_-1322221495191294578gmail-m_2616610884023316279gmail-m_-5914916574228658407gmail-m_6940654179228033455gmail-m_2065586898356323177gmail-m_-4767676071881556798gmail-m_-7293088675286340295gmail-m_-2650746283250910099gmail-MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"747\" style=\"font-family:verdana, sans-serif;width:560pt;margin-left:4.75pt;border-collapse:collapse;\" id=\"yui_3_16_0_ym19_1_1489138076467_10818\"><tbody id=\"yui_3_16_0_ym19_1_1489138076467_10817\">");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10843\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10842\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10841\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10844\">PNR</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:1pt solid windowtext;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:none;padding:0in 5.4pt;min-height:15pt;\">*" + Pnr + "<b><span style=\"font-size:10pt;line-height:15.3333px;\"><font color=\"#222222\">*</font></span></b><br></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10816\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10815\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10814\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10813\">PAX FIRST NAME</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\">" + PassengerName[i] + "&nbsp;<br></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">PAX LAST NAME</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\">&nbsp;" + LastName[i] + "<span style=\"font-size:12.8px;\">&nbsp;</span><br></td></tr><tr style=\"min-height:15pt;\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">Passport No.</span></p></td><td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + PassportNo[i] + "<br></p></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10928\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10927\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10926\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10925\">Date Of Birth</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + DOB[i] + "<br></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10932\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10931\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10930\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10929\">Passport issuing Country</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">" + IssuingCountry[i] + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10935\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10934\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10933\"><span style=\"font-size:12pt;\">&nbsp;</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">&nbsp;</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10869\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10868\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10867\"><span style=\"font-size:12pt;\">Visa type</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:12pt;\">" + VisaType[i] + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10866\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10865\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10864\"><span style=\"font-size:12pt;\">Visa No.</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\">" + VisaNo[i] + "&nbsp;<font face=\"times new roman, serif\"><span style=\"font-size:16px;\">/ 2017&nbsp;/</span></font><span style=\"font-size:12pt;\">&nbsp;212</span></p></td></tr>");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10924\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10923\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10922\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10921\">Place of Issue</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\">" + PlaceIssue[i] + "</font></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10920\"><td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10919\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10918\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10917\">Visa Issue Date</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\"><span style=\"font-size:16px;\">" + Issue[i] + "</span></font><br></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10863\">");
                            sb.Append("  <td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10862\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10861\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10860\">Sponsor Name &amp; Address</span></p></td>");
                            sb.Append("  <td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><span style=\"font-size:16px;\">" + SponsorName[i].Replace("Tel:", "") + "</span></p></td></tr>");
                            sb.Append("  ");
                            sb.Append("  <tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10859\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10858\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10857\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10856\">Contact No.</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><font face=\"times new roman, serif\"><span style=\"font-size:16px;\">" + ContactNo[i] + ",</span></font><br></p></td></tr>");
                            sb.Append("");
                            sb.Append("<tr style=\"min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10855\">");
                            sb.Append("<td width=\"305\" nowrap=\"\" valign=\"bottom\" style=\"width:228.75pt;border-right:1pt solid windowtext;border-bottom:1pt solid windowtext;border-left:1pt solid windowtext;border-top:none;padding:0in 5.4pt;min-height:15pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10854\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\" id=\"yui_3_16_0_ym19_1_1489138076467_10853\"><span style=\"font-size:12pt;\" id=\"yui_3_16_0_ym19_1_1489138076467_10852\">Email Id and Contact Person name</span></p></td>");
                            sb.Append("<td width=\"442\" nowrap=\"\" valign=\"bottom\" style=\"width:331.25pt;border-top:none;border-left:none;border-bottom:1pt solid windowtext;border-right:1pt solid windowtext;padding:0in 5.4pt;min-height:15pt;\"><p class=\"yiv7548598392MsoNormal\" style=\"margin-bottom:0.0001pt;line-height:normal;\"><br></p></td></tr>");
                            sb.Append("</tbody>");
                            sb.Append("</table> <br/>");

                        }
                    }
                    //sb.Append("</table>");

                }
                #endregion

                #region SpiceJet
                else if (Airline == "Spicejet Airlines")
                {
                    sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\"><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR.NO</span></b></p></td>");
                    sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">NO OF PASSENGER</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                    sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">TRAVEL DATE</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td></tr>");
                    if (IsCutOtb == true)
                    {
                        for (int i = 0; i < VisaNo.Length; i++)
                        {
                            VisaLoadByCode(VisaNo[i], out dtResult);
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < PassengerName.Length; i++)
                        {
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    sb.Append("</table>");
                }
                #endregion

                #region Air Arabia
                else if (Airline == "Air Arabia" || Airline == "Oman Airways" || Airline == "Fly Dubai")
                {
                    sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                    sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                    sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                    sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                    sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA TYPE</span></b></p></td>");
                    sb.Append("</tr>");
                    if (IsCutOtb == true)
                    {
                        for (int i = 0; i < VisaNo.Length; i++)
                        {
                            VisaLoadByCode(VisaNo[i], out dtResult);
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                            sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["VisaNo"]) + "</span></p></td>");
                            sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["IeService"]) + "</span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < PassengerName.Length; i++)
                        {
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                            sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                            sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaType[i] + "</span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    sb.Append("</table>");
                }
                #endregion

                #region AirIndiaExp
                else if (Airline == "Air India Express" || Airline == "Air India")
                {
                    sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                    sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                    sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                    sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                    sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                    sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">Travel Date</span></b></p></td>");
                    sb.Append("</tr>");
                    if (IsCutOtb == true)
                    {
                        for (int i = 0; i < VisaNo.Length; i++)
                        {
                            VisaLoadByCode(VisaNo[i], out dtResult);
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                            sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"1", "Mr.").Replace(@"2", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["VisaNo"]) + "</span></p></td>");
                            sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < PassengerName.Length; i++)
                        {
                            sb.Append("<tr style=\"mso-yfti-irow:1\">");
                            sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                            sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                            sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                            sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                            sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                            sb.Append("</tr>");
                        }
                    }
                    sb.Append("</table>");
                }
                PaxInfo = sb.ToString();
                #endregion

                #region Visa Link
                if (IsCutOtb == true)
                {
                    //VisaLink = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetCutVisaPath(VisaNo);
                    VisaLink = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetCutVisaPath(VisaNo);
                }
                else
                {
                    //VisaLink = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
                    VisaLink = GetTicketCopy(OtbRefNo, SamePnr);
                }
                #endregion


            }
            catch
            {
                VisaLink = "";

            }

            return PaxInfo;
        }
        #endregion

        #region Otb File Path
        public static string GetCutVisaPath(string[] RefNo)
        {
            string Path = "";
            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (RefNo.Length - 1 != i)
                {
                    Path += URL + "VisaImages/" + RefNo[i] + "_Visa.pdf;";
                }
                else
                {
                    Path += URL + "VisaImages/" + RefNo[i] + "_Visa.pdf";
                }

            }
            return Path;


        }
        public static string GetOtherVisaPath(string[] RefNo, string OtbId)
        {
            string Path = "";
            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (i != (RefNo.Length - 1))
                {
                    Path += URL + "OTBDocument/" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf;";
                }
                else
                {
                    Path += URL + "OTBDocument/" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf";
                }

            }
            return Path;

        }
        public static string GetTicketCopy(string OtbId, bool SamePnr)
        {

            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string Path = "";
            string InExt = "";
            string OutExt = "";
            string TicketCopy = System.Web.HttpContext.Current.Server.MapPath("~/OTBDocument/");
            DirectoryInfo dir = new DirectoryInfo(TicketCopy);
            FileInfo[] files = null;
            files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Name.Contains(OtbId))
                {
                    Path += URL + "OTBDocument/" + file.Name + ";";
                }

            }
            return Path = Path.Remove(Path.Length - 1);
        }
        static public bool URLExists(string url, out string Ext)
        {
            bool result = false;
            Ext = "";
            string[] CheckExt = { ".jpg", ".jpeg", ".pdf" };
            for (int i = 0; i < CheckExt.Length; i++)
            {
                string Url = "http://clickurtrip.com/OTBDocument/" + url + CheckExt[i];
                HttpWebResponse response = null;
                var request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "HEAD";
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                    if (response.ContentType == "image/jpeg" || response.ContentType == "application/pdf")
                    {
                        Ext = ".pdf";
                        result = true;
                        break;
                    }
                    else
                    {
                        result = false;
                    }
                }
                catch (WebException ex)
                {
                    /* A WebException will be thrown if the status of the response is not `200 OK` */
                    // MessageBox.Show(ex.Message);
                    result = false;
                }
                finally
                {
                    // Don't forget to close your response.
                    if (response != null)
                    {
                        response.Close();
                    }
                }
            }

            return result;
        }
        #endregion

        #region Otb Mail Sending
        #region Visa Load By Code
        public static DBHelper.DBReturnCode VisaLoadByCode(string Vcode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", Vcode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Otb Mail Subject
        public static string MailSubject(string Airlines, Int64 PaxNo, string PassengerName)
        {
            string Subject = "";
            if (Airlines == "Indigo Airlines ") { Subject = "6E OTB – AGENT CODE: 00348 - " + PaxNo.ToString() + " PAX"; }
            else if (Airlines == "Air India") { Subject = "Fwd: AI - OK TO BOARD REQUEST - ( " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Air India Express") { Subject = "Fwd: IX- OK TO BOARD REQUEST- " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Spicejet Airlines") { Subject = "APPLY OTB " + PassengerName + "+" + PaxNo.ToString() + " PAX )"; }
            else if (Airlines == "Air Arabia") { Subject = "APPLY OTB " + PassengerName + "(" + PaxNo.ToString() + "PAX )"; }
            else { Subject = "OK TO BOARD REQUEST- " + PassengerName + " (" + PaxNo.ToString() + "- PAX )"; }
            return Subject;
        }
        #endregion



        #endregion
        #endregion
    }
}