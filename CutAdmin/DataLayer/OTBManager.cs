﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class OTBManager
    {
        #region Visa Search
        public static DBHelper.DBReturnCode GetVisaCode(string Vcode, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Vcode", Vcode);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoOtbSide", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaByOTB(out DataTable tbl_Visa)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UserId", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByOTBStatus", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode VisaLoadByCode(string Vcode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", Vcode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Airports List
        public static DBHelper.DBReturnCode GetAirports(string Airlines, out DataTable tbl_Airports)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@AirlinesName", Airlines);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_OpertingAirlineLoadByAirline", out tbl_Airports, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOtbAirports(string Airports, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@City", Airports);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AirportSearchByCity", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region OtbCharges
        public static DBHelper.DBReturnCode GetOtbCharges(string Airline, string Airports, out DataTable tbl_Otb)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Airline", Airline);
            sqlParams[1] = new SqlParameter("@Airports", Airports);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOtbChargesLoadByAirports", out tbl_Otb, sqlParams);
            return retCode;
        }
        #endregion

        #region VisaLoad By Id
        public static DBHelper.DBReturnCode GetVisaByCode(out DataTable tbl_Visa, string VisaCode)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return retCode;
        }
        #endregion

        #region Emirates
        public static DBHelper.DBReturnCode VisaForEmirates(string[] RefrenceNo, string Airline)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            DataTable dtResult;
            string OTBRefrenceNo = "OTB" + GenerateRandomNumber();
            string VisaNo, VisaType, PassengerName, sDateIssue, PassportNo;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            Int64 ParentID = 0;
            Int64 AdminID = 0;
            int rowsAffected = 0;
            string OTBStatus;
            for (int i = 0; i < RefrenceNo.Length; i++)
            {
                if (i == 0)
                {
                    ParentID = 1;
                }
                else
                {
                    ParentID = 0;
                }
                if (Airline == "EK")
                {
                    OTBStatus = "Applied Directly";
                }
                else
                {
                    OTBStatus = "Otb Not Required";
                }
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@Vcode", RefrenceNo[i]);
                sqlParams[1] = new SqlParameter("@Uid", Uid);
                retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoForOTB", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {

                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    DateTime now = DateTime.Now;
                    string LastUpdateDate = now.ToString("dd-MM-yyyy");
                    string Visa_Path = "../VisaImages/" + RefrenceNo[i] + ".pdf";
                    VisaNo = dtResult.Rows[0]["TReference"].ToString();
                    VisaType = dtResult.Rows[0]["IeService"].ToString();
                    sDateIssue = dtResult.Rows[0]["CopletedDate"].ToString();
                    PassportNo = dtResult.Rows[0]["PassportNo"].ToString();
                    PassengerName = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    SqlParameter[] SQLParams = new SqlParameter[15];
                    SQLParams[0] = new SqlParameter("@uid", Uid);
                    SQLParams[1] = new SqlParameter("@Type", VisaType);
                    SQLParams[2] = new SqlParameter("@Name", PassengerName);
                    SQLParams[3] = new SqlParameter("@VisaRefNo", RefrenceNo[i]);
                    SQLParams[4] = new SqlParameter("@VisaNo", VisaNo);
                    SQLParams[5] = new SqlParameter("@VisaType", VisaType);
                    SQLParams[6] = new SqlParameter("@IssueDate", sDateIssue);
                    SQLParams[7] = new SqlParameter("@Visa_Path", Visa_Path);
                    SQLParams[8] = new SqlParameter("@ParentID ", ParentID);
                    SQLParams[9] = new SqlParameter("@AdminID", AdminID);
                    SQLParams[10] = new SqlParameter("@OTBNo", OTBRefrenceNo);
                    SQLParams[11] = new SqlParameter("@CUTOTB", 1);
                    SQLParams[12] = new SqlParameter("@PassportNo", PassportNo);
                    SQLParams[13] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                    SQLParams[14] = new SqlParameter("@OTBStatus", OTBStatus);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBInsertForEmirates", out rowsAffected, SQLParams);
                }

            }
            return retCode;
        }

        #endregion

        #region Cut OTB Request
        public static DBHelper.DBReturnCode ApllyOtb(string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] RefNo, string sRdbType, string OtbRefNo, string Email1, string Email2, string CCEmailId, out bool IsMailSend, string In_TicketPath, string Out_Ticket)
        {
            IsMailSend = false;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            Int64 ParentID = 0;
            Int64 AdminID = 0;
            DateTime now = DateTime.Now;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            string FranchiseeId = objGlobalDefaults.Franchisee;
            string LastUpdateDate = now.ToString("dd-MM-yyyy");
            int i = 0;
            int rowsAffected = 0;
            DataTable dtResult;
            List<string> CutRef;
            string VisaNo, VisaType, PassengerName = "", sDateIssue, PassportNo = "", PassName = "";
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (i = 0; i < PaxNo; i++)
                {

                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@Vcode", RefNo[i]);
                    sqlParams[1] = new SqlParameter("@Uid", Uid);
                    retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoForOTB", out dtResult, sqlParams);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {

                        retCode = DBHelper.DBReturnCode.EXCEPTION;
                        if (i == 0)
                        {
                            ParentID = 1;
                        }
                        else
                        {
                            ParentID = 0;
                        }
                        VisaNo = dtResult.Rows[0]["VisaNo"].ToString();
                        string VCode = dtResult.Rows[0]["Vcode"].ToString();
                        string Visa_Path = "../VisaImages/" + VCode + ".pdf";
                        VisaType = dtResult.Rows[0]["IeService"].ToString();
                        sDateIssue = dtResult.Rows[0]["CopletedDate"].ToString();
                        PassportNo = dtResult.Rows[0]["PassportNo"].ToString();
                        PassengerName = dtResult.Rows[0]["FirstName"].ToString();
                        string LastName = dtResult.Rows[0]["LastName"].ToString();
                        PassName = PassengerName + " " + LastName;
                        SqlParameter[] SQLParams = new SqlParameter[33];
                        SQLParams[0] = new SqlParameter("@uid", Uid);
                        SQLParams[1] = new SqlParameter("@Type", sRdbType);
                        SQLParams[2] = new SqlParameter("@Name", PassengerName);
                        SQLParams[3] = new SqlParameter("@VisaRefNo", RefNo[i]);
                        SQLParams[4] = new SqlParameter("@VisaNo", VisaNo);
                        SQLParams[5] = new SqlParameter("@VisaType", VisaType);
                        SQLParams[6] = new SqlParameter("@IssueDate", sDateIssue);
                        SQLParams[7] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                        SQLParams[8] = new SqlParameter("@In_AirLine ", ArrivalAirLine);
                        SQLParams[9] = new SqlParameter("@In_DepartureDt ", ArrivalDate);
                        SQLParams[10] = new SqlParameter("@In_FlightNo ", ArrivingFlightNo);
                        SQLParams[11] = new SqlParameter("@In_PNR ", ArrivingPnr);
                        SQLParams[12] = new SqlParameter("@Out_AirLine  ", DepartingAirline);
                        SQLParams[13] = new SqlParameter("@Out_DepartureDt ", DepartingDate);
                        SQLParams[14] = new SqlParameter("@Out_FlightNo ", DerartingFlightNo);
                        SQLParams[15] = new SqlParameter("@Out_PNR ", DerartingPnr);
                        SQLParams[16] = new SqlParameter("@VisaFee", VisaFee);
                        SQLParams[17] = new SqlParameter("@UrgentFee ", UrgentCharges);
                        SQLParams[18] = new SqlParameter("@ServiceCharge", 0);
                        SQLParams[19] = new SqlParameter("@Total", TotalPerPass);
                        SQLParams[20] = new SqlParameter("@Visa_Path", Visa_Path);
                        SQLParams[21] = new SqlParameter("@In_TicketPath", "");
                        SQLParams[22] = new SqlParameter("@Out_TicketPath", "");
                        SQLParams[23] = new SqlParameter("@ParentID ", ParentID);
                        SQLParams[24] = new SqlParameter("@AdminID", AdminID);
                        SQLParams[25] = new SqlParameter("@OTBNo", OtbRefNo);
                        SQLParams[26] = new SqlParameter("@CUTOTB", 1);
                        SQLParams[27] = new SqlParameter("@PassportNo", PassportNo);
                        SQLParams[28] = new SqlParameter("@LastName", LastName);
                        SQLParams[29] = new SqlParameter("@DepartingFrom", Arrivalfrom);
                        SQLParams[30] = new SqlParameter("@ArrivingFrom", DepartingFrom);
                        SQLParams[31] = new SqlParameter("@Sponsor", "ClickUrTrip");
                        SQLParams[32] = new SqlParameter("@FranchiseeId", FranchiseeId);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBAddByOtbRequest", out rowsAffected, SQLParams);
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
                    float AvailableCrdit = 0;
                    GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    float BookingAmtWithoutTax = TotalAmmount;
                    float BookingServiceTax = 0;
                    string BookingDate = LastUpdateDate;
                    string BookedBy = Uid.ToString();
                    string BookingRemark = "";
                    float SupplierBasicAmt = BookingAmtWithoutTax;
                    float CutComm = SupplierBasicAmt;
                    float AgentComm = SupplierBasicAmt;
                    retCode = OTBBookingTxnAdd(OtbRefNo, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "OTB", SupplierBasicAmt, AgentComm, CutComm, BookedBy, PassengerName, PaxNo);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        IsMailSend = false;
                        float OtbCharges = TotalPerPass - 0;
                        float otbTotalAmmount = OtbCharges * PaxNo;
                        float ServiceCharges = OtbCharges - VisaFee;
                        SendEmailToAgent(ArrivingPnr, ArrivalAirLine, Arrivalfrom, DepartingFrom, PaxNo, RefNo, RefNo, RefNo, RefNo, VisaFee, UrgentCharges, OtherCharges, OtbCharges, otbTotalAmmount, true, OtbRefNo);
                        objTransactionScope.Complete();
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }

                }
                else
                {
                    objTransactionScope.Dispose();
                }
            }
            return retCode;

        }

        public static DBHelper.DBReturnCode OTBBookingTxnAdd(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName, Int64 PaxNo)
        {
            int rowsaffected;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uid = objAgentDetailsOnAdmin.sid;
                if (BookedBy == "")
                    BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            }
            else if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                uid = objGlobalDefault.sid;
                if (BookedBy == "")
                    BookedBy = objGlobalDefault.Agentuniquecode;
            }
            string Perticular = "Booking (OTB) " + " " + PassengerName + " x " + PaxNo;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticular);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByOTB", out rowsaffected, sqlParams);
            return retCode;
        }
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
        #endregion

        #region Other Otb Request
        public static DBHelper.DBReturnCode OtherCompanyOtb(string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] FirstName, string[] LastName, string[] VisaType, string[] VisaNo, string[] Issue, string[] PassportNo, string[] VisaCopy, string sRdbType, string otbType, string[] Sponsor, string[] SponsorNo, string OtbRefNo, string Email1, string Email2, string CCEmailId, out bool IsMailSend, string In_TicketPath, string Out_Ticket, string[] DOB, string[] OtherPassportNo, string[] PlaceIssue, string[] IssueCountry, string[] ContactNo)
        {
            IsMailSend = false;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            string ArrivalTicketPath = "", DepartingTicketPath = "", Out_TicketPath = "";
            string IFN = (string)HttpContext.Current.Session["InboundFileName"];
            string OFN = (string)HttpContext.Current.Session["OutboundFileName"];
            //string OTBRefrenceNo = "OTB" + GenerateRandomNumber();
            string[] DepartingTicketFile = null;
            string[] ArrivalTicketFile = IFN.Split('\\');
            ArrivalTicketPath = "../OTBDocument/" + ArrivalTicketFile[(ArrivalTicketFile.Length - 1)];
            if (ArrivingPnr != DerartingPnr)
            {
                //OFN = (string)HttpContext.Current.Session["OutboundFileName"];
                //DepartingTicketFile = OFN.Split('\\');
                //DepartingTicketPath = "../OTBDocument/" + DepartingTicketFile[(DepartingTicketFile.Length - 1)];
            }

            Int64 ParentID = 0;
            Int64 AdminID = 0;
            DateTime now = DateTime.Now;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            string FranchiseeId = objGlobalDefaults.Franchisee;
            string LastUpdateDate = now.ToString("dd-MM-yyyy");
            int i = 0;
            int rowsAffected = 0;
            //In_TicketPath = ArrivalTicketPath;
            if (ArrivingPnr != DerartingPnr)
            {
                Out_TicketPath = DepartingTicketPath;
            }
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (i = 0; i < PaxNo; i++)
                {
                    if (i == 0)
                    {
                        ParentID = 1;
                    }
                    else
                    {
                        ParentID = 0;

                    }
                    string[] s = VisaCopy[i].Split('\\');
                    int LENGTH = s.Length;
                    string Visa_Path = "../OTBDocument/" + s[LENGTH - 1];
                    retCode = DBHelper.DBReturnCode.EXCEPTION;
                    SqlParameter[] SQLParams = new SqlParameter[37];
                    SQLParams[0] = new SqlParameter("@uid", Uid);
                    SQLParams[1] = new SqlParameter("@Type", sRdbType);
                    SQLParams[2] = new SqlParameter("@Name", FirstName[i]);
                    SQLParams[3] = new SqlParameter("@VisaRefNo", VisaNo[i]);
                    SQLParams[4] = new SqlParameter("@VisaNo", VisaNo[i]);
                    SQLParams[5] = new SqlParameter("@VisaType", VisaType[i]);
                    SQLParams[6] = new SqlParameter("@IssueDate", Issue[i]);
                    SQLParams[7] = new SqlParameter("@LastUpdateDate", LastUpdateDate);
                    SQLParams[8] = new SqlParameter("@In_AirLine ", ArrivalAirLine);
                    SQLParams[9] = new SqlParameter("@In_DepartureDt ", ArrivalDate);
                    SQLParams[10] = new SqlParameter("@In_FlightNo ", ArrivingFlightNo);
                    SQLParams[11] = new SqlParameter("@In_PNR ", ArrivingPnr);
                    SQLParams[12] = new SqlParameter("@Out_AirLine  ", DepartingAirline);
                    SQLParams[13] = new SqlParameter("@Out_DepartureDt ", DepartingDate);
                    SQLParams[14] = new SqlParameter("@Out_FlightNo ", DerartingFlightNo);
                    SQLParams[15] = new SqlParameter("@Out_PNR ", DerartingPnr);
                    SQLParams[16] = new SqlParameter("@VisaFee", VisaFee);
                    SQLParams[17] = new SqlParameter("@UrgentFee ", UrgentCharges);
                    SQLParams[18] = new SqlParameter("@ServiceCharge", OtherCharges);
                    SQLParams[19] = new SqlParameter("@Total", TotalAmmount);
                    SQLParams[20] = new SqlParameter("@Visa_Path", Visa_Path);
                    SQLParams[21] = new SqlParameter("@In_TicketPath", In_TicketPath);
                    SQLParams[22] = new SqlParameter("@Out_TicketPath", Out_TicketPath);
                    SQLParams[23] = new SqlParameter("@ParentID ", ParentID);
                    SQLParams[24] = new SqlParameter("@AdminID", AdminID);
                    SQLParams[25] = new SqlParameter("@OTBNo", OtbRefNo);
                    SQLParams[26] = new SqlParameter("@CUTOTB", 0);
                    SQLParams[27] = new SqlParameter("@PassportNo", PassportNo[i]);
                    SQLParams[28] = new SqlParameter("@LastName", LastName[i]);
                    SQLParams[29] = new SqlParameter("@DepartingFrom", Arrivalfrom);
                    SQLParams[30] = new SqlParameter("@ArrivingFrom", DepartingFrom);
                    SQLParams[31] = new SqlParameter("@Sponsor", Sponsor[i] + "Tel:" + SponsorNo[i]);
                    SQLParams[32] = new SqlParameter("@FranchiseeId", FranchiseeId);
                    // Indigo Parameters //
                    SQLParams[33] = new SqlParameter("@DOB", DOB[i]);
                    //SQLParams[34] = new SqlParameter("@OtherPassportNo", OtherPassportNo[i]);
                    SQLParams[34] = new SqlParameter("@IssuingPlace", PlaceIssue[i]);
                    SQLParams[35] = new SqlParameter("@IssuingCountry", IssueCountry[i]);
                    SQLParams[36] = new SqlParameter("@ContactNo", ContactNo[i]);

                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBAddByOtbRequestNew", out rowsAffected, SQLParams);
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    retCode = BL.DBHelper.DBReturnCode.EXCEPTION;
                    float BookingAmtWithoutTax = TotalAmmount;
                    float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
                    //float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["ServiceTax"]);
                    float BookingServiceTax = 0;
                    string BookingDate = LastUpdateDate;
                    string BookedBy = Uid.ToString();
                    string BookingRemark = "";
                    float SupplierBasicAmt = BookingAmtWithoutTax;
                    float CutComm = SupplierBasicAmt;
                    float AgentComm = SupplierBasicAmt;
                    retCode = OTBBookingTxnOther(OtbRefNo, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "OTB", SupplierBasicAmt, AgentComm, CutComm, BookedBy, FirstName[0], PaxNo);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        float AgentOtbMarkup = objAgentInfo.AgentVisaMarkup;
                        float OtbCharges = TotalPerPass - AgentOtbMarkup;
                        float otbTotalAmmount = OtbCharges * PaxNo;
                        float ServiceCharges = OtbCharges - VisaFee;
                        OTBManager.SendEmailToAgent(ArrivingPnr, ArrivalAirLine, Arrivalfrom, DepartingFrom, PaxNo, FirstName, LastName, PassportNo, VisaNo, VisaFee, UrgentCharges, OtherCharges, OtbCharges, otbTotalAmmount, false, OtbRefNo);
                        objTransactionScope.Complete();
                        //retCode = DBHelper.DBReturnCode.EXCEPTION;
                        //retCode = OTBManager.Airlinemail(ArrivingPnr, DerartingPnr, ArrivalDate, FirstName, LastName, VisaNo, VisaCopy, VisaType, Issue, false, Email1, Email2, CCEmailId, OtbRefNo, MailSubject(ArrivalAirLine, PaxNo, FirstName[0] + " " + LastName[0]), ArrivalAirLine, In_TicketPath, Out_Ticket);
                        //if(retCode == DBHelper.DBReturnCode.SUCCESSNOAFFECT || retCode == BL.DBHelper.DBReturnCode.EXCEPTION)
                        //{
                        //    if(retCode == DBHelper.DBReturnCode.EXCEPTION)
                        //    {
                        //        IsMailSend = false;
                        //    }
                        //    else
                        //    {
                        //        IsMailSend = true;
                        //    }

                        //}
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }
                }
                else
                {
                    objTransactionScope.Dispose();
                }
            }

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
                float AvailableCrdit = 0;
                GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objAgentInfo != null)
                {
                    AvailableCrdit = objAgentInfo.AgentCredit + objAgentInfo.AgentCreditLimit;
                }

                if (AvailableCrdit < BookingAmtWithTax)
                {
                    return DBHelper.DBReturnCode.EXCEPTION; ;
                }
                else
                {

                }
            }
            return retCode;

        }

        public static DBHelper.DBReturnCode OTBBookingTxnOther(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName, Int64 PaxNo)
        {
            int rowsaffected;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uid = objAgentDetailsOnAdmin.sid;
                if (BookedBy == "")
                    BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            }
            else if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                uid = objGlobalDefault.sid;
                if (BookedBy == "")
                    BookedBy = objGlobalDefault.Agentuniquecode;
            }
            string Perticular = "Booking (OTB)" + PassengerName + "*" + PaxNo;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticular);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByOTB", out rowsaffected, sqlParams);
            return retCode;
        }

        #endregion

        #region OTList.
        public static DBHelper.DBReturnCode GetOtbList(out DataSet tbl_OTB)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UID", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_OTBLoadAllByUsre", out tbl_OTB, sqlParams);
            return retCode;
        }
        public static DataTable GetOtbPaxNo(DataSet dsResult)
        {
            DataTable dt_Otb = new DataTable();
            dsResult.Tables[0].Columns.Add("num_Pax", typeof(string));
            dt_Otb = dsResult.Tables[0].Clone();
            DataRow[] Rows = null;
            foreach (DataRow Row in dsResult.Tables[0].Rows)
            {
                string OtbNo = Row["OTBNo"].ToString();
                Rows = dsResult.Tables[1].Select("OTBNo = '" + OtbNo + "'");
                Row["num_Pax"] = Rows.Count().ToString();
            }
            dt_Otb = dsResult.Tables[0];
            return dt_Otb;

        }
        #endregion

        #region OTB Invoice
        public static DBHelper.DBReturnCode GetAgentMarkp(Int64 Uid, out DataTable dtable)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_MarkUpGetAgentMarkup", out dtable, sqlParams);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AgentMarkupLoadByOTB", out dtable, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode OTBInvoiceDetails(string RefNo, Int64 UID, out DataSet ds)
        {
            SqlParameter[] SQLParams = new SqlParameter[2];
            SQLParams[0] = new SqlParameter("@ReservationID", RefNo);
            SQLParams[1] = new SqlParameter("@sid", UID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_OTBInvoiceDetails", out ds, SQLParams);
            return retCode;
        }
        #endregion

        #region Otb Search
        public static DBHelper.DBReturnCode GetOTBbyCode(string Refrence, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@RefrenceNo", Refrence);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByRefNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyVisaNo(string VisaNo, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@VisaNo", VisaNo);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByVisaNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyName(string Name, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Name", Name);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByName", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyLastName(string LastName, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@LastName", LastName);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOTBLoadByLastName", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetOTBbyPnrNo(string PnrNo, Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@In_PNR", PnrNo);
            sqlParams[1] = new SqlParameter("@Uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblOtbSearchByPnrNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode SerchVisaAll(string dteTo, string dteFrom, string dteTravel, string FirstName, string LastName, string VisaType, string AirLine, string PnrNo, out DataSet dtResult)
        {
            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            uid = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[9];
            sqlParams[0] = new SqlParameter("@dteTo", dteTo);
            sqlParams[1] = new SqlParameter("@dteFrom", dteFrom);
            sqlParams[2] = new SqlParameter("@dteTravel", dteTravel);
            sqlParams[3] = new SqlParameter("@FirstName", FirstName);
            sqlParams[4] = new SqlParameter("@LastName", LastName);
            sqlParams[5] = new SqlParameter("@VisaType", VisaType);
            sqlParams[6] = new SqlParameter("@AirLine", AirLine);
            sqlParams[7] = new SqlParameter("@PnrNo", PnrNo);
            sqlParams[8] = new SqlParameter("@uid", uid);
            retCode = DBHelper.GetDataSet("Proc_tbl_OTBBySearchAllBYAgent", out dtResult, sqlParams);
            return retCode;

        }
        #endregion

        #region Mail SendStatus
        public static DBHelper.DBReturnCode IsMailNotSend(string OtbRefNo)
        {
            int Rows = 0;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@OtbRefNo", OtbRefNo);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_OTBUpdateByMailSend", out Rows, sqlParams);
            return retCode;
        }
        #endregion



        #region Otb Mail Sending
        #region Old OtbMail Format
        public static bool OTB_6EEmail(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string[] Sponsor, string[] SponsorNos, string OtbRefNo, string sSubject, string In_TicketPath, string Out_Ticket)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //string sSubject = "Requested For OK to Board Service";
            string IFN = (string)HttpContext.Current.Session["InboundFileName"];
            string OFN = "", ArrivalTicketPath = "", DepartingTicketPath = "";
            string[] DepartingTicketFile = null;
            string[] ArrivalTicketFile = IFN.Split('\\');
            string VisaLink = "";

            ArrivalTicketPath = "http://clickurtrip.com/OTBDocument/" + ArrivalTicketFile[(ArrivalTicketFile.Length - 1)];
            if (Pnr != DerartingPnr)
            {
                OFN = (string)HttpContext.Current.Session["OutboundFileName"];
                DepartingTicketFile = OFN.Split('\\');
                DepartingTicketPath = "http://clickurtrip.com/OTBDocument/" + DepartingTicketFile[(DepartingTicketFile.Length - 1)];
            }
            bool SamePnr = false;
            if (OFN == "")
                SamePnr = true;
            StringBuilder sb = new StringBuilder();
            if (Cut == true)
            {
                string Currency = "";
                if (objGlobalDefaults.Currency == "INR")
                {
                    Currency = "<img src=\"http://clickurtrip.com/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (objGlobalDefaults.Currency == "AED")
                {
                    Currency = "AED ";
                }
                else if (objGlobalDefaults.Currency == "USD")
                {
                    Currency = "$ ";
                }
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Greetings from,</span><img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;width: 80px;margin-top: 2px;\" /><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                string Name, PermitNo, ExpiryDate;
                DataTable dtResult;
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    VisaLoadByCode(PassengerName[i], out dtResult);
                    Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    PermitNo = dtResult.Rows[0]["TReference"].ToString();
                    ExpiryDate = dtResult.Rows[0]["ExpDate"].ToString();
                    string VisaPath = "http://clickurtrip.com/VisaImages/" + PassengerName[i] + "_Visa.pdf";
                    sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Passenger" + (i + 1) + " :</p><br/>");
                    sb.Append("<table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;border:none;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;background-color: white;margin-left: 5%;\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:17.5pt\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;height:17.5pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;mso-bidi-font-family:Calibri;color:black\">PNR:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt;height:17.5pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;mso-bidi-font-family:Calibri\">" + Pnr + " &nbsp;</span><span style=\"font-size:9.0pt;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Pax</span></b></span><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family: Calibri;color:black\"> Name:</span></b></p></td>");
                    sb.Append(" <td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left: none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + Name + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Entry Permit No:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + PermitNo + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Date Of Expire:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + ExpiryDate + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Sponsor Name &amp; Contact:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">ClickUrTrip Travel & Tourism Tel:042977792</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Visa Copy:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + VisaPath + " download>Download</a></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                }
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetCutVisaPath(PassengerName);
                sb.Append("<br />");
                sb.Append("<br />");
                //sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Arrival Ticket: <span style=\"mso-spacerun:yes\">&nbsp;</span></span></b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + ArrivalTicketPath + " download>Download</a></span><o:p></o:p></p>");
                //if (ArrivalTicketPath != DepartingTicketPath)
                //{
                //    sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Departing Ticket: <span style=\"mso-spacerun:yes\">&nbsp;</span></span></b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + DepartingTicketPath + " download>Download</a></span><o:p></o:p></p>");

                //}
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br />");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Visa Department</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 100%\" /></b></span><br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Head Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\" <o:p></o:p></span><span style=\"margin-left:2%;font-weight:400\">2nd Floor, Vishnu Complex, C.A Road Opp. Rahate Hospital, Juni Mangalwari Nagpur-440008, M.S. INDIA</span><br/><b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+91-712-6660666 (100 Lines) Fax:. +91-712-2766520  eMail: info@clickurtrip.com</span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Dubai Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Al Maktoum Street, Beside Dnata Near Clock Tower, Port Saeed, Deira, P.O. Box 43430, Dubai-UAE</b></span><br />");
                //sb.Append("<b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+971-4-2977792  Fax:. +971-4-2977793    eMail: dubai@clickurtrip.com</span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }
            else
            {
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Greetings from,</span><img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;width: 80px;margin-top: 2px;\" /><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    string[] VisaFile = VisaCopy[i].Split('\\');
                    string VisaPath = "http://clickurtrip.com/OTBDocument/" + VisaFile[(VisaFile.Length - 1)];
                    sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Passenger" + (i + 1) + " :</p><br/><br/>");
                    sb.Append("<table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;border:none;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;background-color: white;margin-left: 5%;\">");
                    sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:17.5pt\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt;height:17.5pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;mso-bidi-font-family:Calibri;color:black\">PNR:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt;height:17.5pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;mso-bidi-font-family:Calibri\">" + Pnr + " &nbsp;</span><span style=\"font-size:9.0pt;mso-ascii-font-family:Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\"><o:p></o:p></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style=\"mso-yfti-irow:1\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Pax</span></b></span><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family: Calibri;color:black\"> Name:</span></b></p></td>");
                    sb.Append(" <td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left: none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + PassengerName[i] + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Entry Permit No:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + VisaNo[i] + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Date Of Expire:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + Issue[i] + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Sponsor Name &amp; Contact:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\">" + Sponsor[i] + " Tel:" + SponsorNos[i] + "</span></p></td>");
                    sb.Append("</tr>");
                    sb.Append(" <tr style=\"mso-yfti-irow:2\">");
                    sb.Append("<td width=\"249\" valign=\"top\" style=\"width:149.4pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt; mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Calibri;color:black\">Visa Copy:</span></b></p></td>");
                    sb.Append("<td width=\"549\" valign=\"top\" style=\"width:329.4pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + VisaPath + " download>Download</a></span></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                }
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
                sb.Append("<br />");
                //sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Arrival Ticket: <span style=\"mso-spacerun:yes\">&nbsp;</span></span></b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + ArrivalTicketPath + " download>Download</a></span><o:p></o:p></p>");
                //if (Pnr != DerartingPnr)
                //{
                //    sb.Append("<p class=\"MsoNormal\" style=\"margin-left:2%;font-weight:400;margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><b><span style=\"font-size:9.0pt;font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;;color:black\">Departing Ticket: <span style=\"mso-spacerun:yes\">&nbsp;</span></span></b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:black\"><a href=" + DepartingTicketPath + " download>Download</a></span><o:p></o:p></p>");

                //}
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Visa Department</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;width: 80px;margin-top: 2px;\" /></b></span><br />");

                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Head Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\" <o:p></o:p></span><span style=\"margin-left:2%;font-weight:400\">2nd Floor, Vishnu Complex, C.A Road Opp. Rahate Hospital, Juni Mangalwari Nagpur-440008, M.S. INDIA</span><br/><b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+91-712-6660666 (100 Lines) Fax:. +91-712-2766520  eMail: info@clickurtrip.com</span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Dubai Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Al Maktoum Street, Beside Dnata Near Clock Tower, Port Saeed, Deira, P.O. Box 43430, Dubai-UAE</b></span><br />");
                //sb.Append("<b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+971-4-2977792  Fax:. +971-4-2977793    eMail: dubai@clickurtrip.com</span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }




            try
            {
                DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
                int effected = 0;
                //string[] CCiD = null;
                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                //mailMsg.To.Add("otb@clickUrTrip.com");
                ////if (Email1 != "")
                ////{
                ////    mailMsg.To.Add(Email1);
                ////}
                ////if (Email2 != "")
                ////{
                ////    mailMsg.To.Add(Email2);
                ////}
                ////if (CCEmailId != "")
                ////{
                ////    CCiD = CCEmailId.Split(',');
                ////    mailMsg.CC.Add(CCiD[0]);
                ////    mailMsg.CC.Add(CCiD[1]);
                ////}
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Attachments.Add(new Attachment(IFN));
                //if (OFN != "")
                //{
                //    mailMsg.Attachments.Add(new Attachment(OFN));
                //}
                //mailMsg.Body = sb.ToString();
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@sTo", "otb@clickUrTrip.com");
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                sqlParams[3] = new SqlParameter("@Path", VisaLink);
                retcode = DBHelper.ExecuteNonQuery("Proc_OTBMail", out effected, sqlParams);

                return true;

            }
            catch
            {
                return false;
            }
        }
        public static bool SGEmailFormatForCut(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string OtbRefNo, string sSubject, string In_TicketPath, string Out_Ticket)
        {
            //sSubject = "OTB Request Received Confirmation";
            string IFN = (string)HttpContext.Current.Session["InboundFileName"];
            string OFN = "", ArrivalTicketPath = "", DepartingTicketPath = "";
            string[] DepartingTicketFile = null;
            string[] ArrivalTicketFile = IFN.Split('\\');
            ArrivalTicketPath = "http://clickurtrip.com/OTBDocument/" + ArrivalTicketFile[(ArrivalTicketFile.Length - 1)];
            string VisaLinks = "";
            if (Pnr != DerartingPnr)
            {
                OFN = (string)HttpContext.Current.Session["OutboundFileName"];
                DepartingTicketFile = OFN.Split('\\');
                DepartingTicketPath = "http://clickurtrip.com/OTBDocument/" + DepartingTicketFile[(DepartingTicketFile.Length - 1)];
            }
            bool SamePnr = false;
            if (OFN == "")
                SamePnr = true;
            //SendEmailToAgent(Pnr, TravelDate, PassengerName, LastName, VisaNo, Issue, Cut, Email1, Email2, CCEmailId);
            StringBuilder sb = new StringBuilder();
            if (Cut == true)
            {

                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                sb.Append("<table align=\"center\" style=\"border-collapse:collapse;border:none;mso-border-alt:solid #F9B074 1.0pt;mso-border-themecolor:accent6;mso-border-themetint:191;mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\">");
                sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>PNR No</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>No of Passenger</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Travel Date</span></b></p></td>");
                //sb.Append("<td width=\"150\" valign=\"top\" style=\"width:90.1pt;border:solid black 1.0pt;border-left:none;mso-border-left-alt:inset windowtext .75pt;mso-border-alt:solid black .75pt;mso-border-left-alt:inset windowtext .75pt;padding:0in 0in 0in 0in;height:12.75pt\"> <p class=\"MsoNormal\" align=\"center\" style=\"margin-top: 9%;margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;color:black\">Arrival Ticket</span></b><b><span style=\"font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family: Arial\"><o:p></o:p></span></b></p></td>");
                //sb.Append("<td width=\"156\" valign=\"top\" style=\"width:93.45pt;border:solid black 1.0pt;border-left:none;mso-border-left-alt:inset windowtext .75pt;mso-border-alt: solid black .75pt;mso-border-left-alt:inset windowtext .75pt;padding:0in 0in 0in 0in; height:12.75pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-top: 9%;margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-family:&quot;Segoe UI&quot;,&quot;sans-serif&quot;color:black\">Departing Ticket</span><o:p></o:p></b></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + Pnr + "</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + PassengerName.Length + "</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + TravelDate + "</span></b></p></td>");
                //sb.Append("<td width=\"150\" valign=\"top\" style=\"width:90.1pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt; mso-border-top-alt:inset windowtext .75pt;mso-border-left-alt:inset windowtext .75pt; mso-border-top-alt:inset windowtext;mso-border-left-alt:inset windowtext;mso-border-bottom-alt:solid black;mso-border-right-alt:solid black;mso-border-width-alt:.75pt;padding:0in 0in 0in 0in;height:12.75pt\"> <p class=\"MsoNormal\" align=\"center\" style=\"margin-top: 9%;margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span class=\"SpellE\"><span style=\"font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:Arial;mso-bidi-font-weight:bold\"><a href=" + ArrivalTicketPath + " download>Download</a></span></span><span style=\"font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:Arial;mso-bidi-font-weight:bold\"><o:p></o:p></span></p></td>");
                //if (Pnr != DerartingPnr)
                //{
                //    sb.Append("<td width=\"150\" valign=\"top\" style=\"width:90.1pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt; mso-border-top-alt:inset windowtext .75pt;mso-border-left-alt:inset windowtext .75pt; mso-border-top-alt:inset windowtext;mso-border-left-alt:inset windowtext;mso-border-bottom-alt:solid black;mso-border-right-alt:solid black;mso-border-width-alt:.75pt;padding:0in 0in 0in 0in;height:12.75pt\"> <p class=\"MsoNormal\" align=\"center\" style=\"margin-top: 9%;margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span class=\"SpellE\"><span style=\"font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:Arial;mso-bidi-font-weight:bold\"><a href=" + DepartingTicketPath + " download>Download</a></span></span><span style=\"font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:Arial;mso-bidi-font-weight:bold\"><o:p></o:p></span></p></td>");

                //}
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 15%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                //sb.Append("<td width=\"500\" style=\"width: 200px;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'verdana','sans-serif'; mso-fareast-font-family:'times=''  roman';mso-bidi-font-family:arial\">Visa Copy<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                string Name, PermitNo;
                DataTable dtResult;
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    VisaLoadByCode(PassengerName[i], out dtResult);
                    Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    PermitNo = dtResult.Rows[0]["TReference"].ToString();
                    string VisaPath = "http://clickurtrip.com/VisaImages/" + PassengerName[i] + "_Visa.pdf";
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + Name + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + PermitNo + "<o:p></o:p></span></b></p></td>");
                    //sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><a href=" + VisaPath + " download>Download</a></b></p></td>");

                    sb.Append("</tr>");

                }
                VisaLinks = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetCutVisaPath(PassengerName);
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }
            else
            {
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Greetings from,</span><img src=\"http://www.clickurtrip.com/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;width: 80px;margin-top: 2px;\" /><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                sb.Append("<table align=\"center\" style=\"border-collapse:collapse;border:none;mso-border-alt:solid #F9B074 1.0pt;mso-border-themecolor:accent6;mso-border-themetint:191;mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\">");
                sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>PNR No</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>No of Passenger</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Travel Date</span></b></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + Pnr + "</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + PassengerName.Length + "</span></b></p></td>");
                sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + TravelDate + "</span></b></p></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 15%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                //sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Visa Copy<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");

                for (int i = 0; i < PassengerName.Length; i++)
                {
                    //sSubject = "Thanks for OTB request for " + PassengerName[i] + " " + LastName[i] + " * " + Convert.ToString(PassengerName.Length);
                    string[] VisaFile = VisaCopy[i].Split('\\');
                    string VisaPath = "http://clickurtrip.com/OTBDocument/" + VisaFile[(VisaFile.Length - 1)];
                    string Visa = (string)HttpContext.Current.Session["VisaFileName" + i];
                    //string VisaPath = 
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + PassengerName[i] + " " + LastName[i] + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + VisaNo[i] + "<o:p></o:p></span></b></p></td>");
                    //sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><a href=" + VisaPath + " download>Download</a></b></p></td>");
                    sb.Append("</tr>");

                }
                VisaLinks = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"padding-left:10px;width: 80%;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  © 2016 - 2017 ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }


            try
            {
                DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
                int effected = 0;
                //string[] CCiD = null;
                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                ////mailMsg.To.Add("otb@clickurtrip.com");
                //mailMsg.To.Add("kashifkhan4847@gmail.com");
                //if (Email1 != "")
                //{
                //    mailMsg.To.Add(Email1);
                //}
                //if (Email2 != "")
                //{
                //    mailMsg.To.Add(Email2);
                //}
                //if (CCEmailId != "")
                //{
                //    CCiD = CCEmailId.Split(',');
                //    mailMsg.CC.Add(CCiD[0]);
                //    mailMsg.CC.Add(CCiD[1]);
                //}
                //mailMsg.Attachments.Add(new Attachment(IFN));
                //if (OFN !="")
                //{
                //    mailMsg.Attachments.Add(new Attachment(OFN));
                //}
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString();
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@sTo", "otb@clickUrTrip.com");
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                retcode = DBHelper.ExecuteNonQuery("Proc_OTBMail", out effected, sqlParams);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #region IndigoMail
        public static bool IndigoMailFormat(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string OtbRefNo, string sSubject, string In_TicketPath, string Out_Ticket)
        {
            //string sSubject = "OTB Request Received Confirmation";
            string IFN = (string)HttpContext.Current.Session["InboundFileName"];
            string OFN = "", ArrivalTicketPath = "", DepartingTicketPath = "";
            string[] DepartingTicketFile = null;
            string[] ArrivalTicketFile = IFN.Split('\\');
            ArrivalTicketPath = "http://clickurtrip.com/OTBDocument/" + ArrivalTicketFile[(ArrivalTicketFile.Length - 1)];
            string VisaLinks = "";
            if (Pnr != DerartingPnr)
            {
                OFN = (string)HttpContext.Current.Session["OutboundFileName"];
                DepartingTicketFile = OFN.Split('\\');
                DepartingTicketPath = "http://clickurtrip.com/OTBDocument/" + DepartingTicketFile[(DepartingTicketFile.Length - 1)];
            }
            bool SamePnr = false;
            if (OFN == "")
                SamePnr = true;
            //SendEmailToAgent(Pnr, TravelDate, PassengerName, LastName, VisaNo, Issue, Cut, Email1, Email2, CCEmailId);
            StringBuilder sb = new StringBuilder();
            if (Cut == true)
            {

                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                string Name, PermitNo;
                DataTable dtResult;
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    VisaLoadByCode(PassengerName[i], out dtResult);
                    Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    PermitNo = dtResult.Rows[0]["TReference"].ToString();
                    sb.Append("<table  style=\"border-collapse:collapse;margin-left: 5%;border:none;mso-border-alt:solid #F9B074 1.0pt;mso-border-themecolor:accent6;mso-border-themetint:191;mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\">");
                    sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Airline No</span></b></p></td>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Pax Name</span></b></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + Pnr + "</span></b></p></td>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + Name + "</span></b></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</table><br/>");

                }
                VisaLinks = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetCutVisaPath(PassengerName);
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }
            else
            {
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
                sb.Append("<br />");
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    sSubject = "Thanks for OTB request for " + PassengerName[i] + " " + LastName[i] + " * " + Convert.ToString(PassengerName.Length);
                    sb.Append("<table  style=\"border-collapse:collapse;border:none;margin-left: 5%;mso-border-alt:solid #F9B074 1.0pt;mso-border-themecolor:accent6;mso-border-themetint:191;mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\">");
                    sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Airline No</span></b></p></td>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>Pax Name</span></b></p></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + Pnr + "</span></b></p></td>");
                    sb.Append("<td width=\"266\" valign=\"top\" style=\"width:159.6pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:9.0pt;font-family:'Verdana','sans-serif';mso-bidi-font-family:Calibri;color:black\"><span style='mso-spacerun:yes'></span>" + PassengerName[i] + " " + LastName[i] + "</span></b></p></td>");
                    sb.Append("</tr>");
                    sb.Append("</table><br/>");
                }
                VisaLinks = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
            }


            try
            {
                DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
                int effected = 0;
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@sTo", "otb@clickUrTrip.com");
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                retcode = DBHelper.ExecuteNonQuery("Proc_OTBMail", out effected, sqlParams);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
        #endregion

        #region New Mail Format
        public static DBHelper.DBReturnCode Airlinemail(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool Cut, string Email1, string Email2, string CCEmailId, string OtbRefNo, string sSubject, string Airline, string In_TicketPath, string Out_Ticket)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            string VisaLinks;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Dear Sir/Madam,</span><br/>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=MsoNormal style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Request you to kindly apply for OTB (Ok to board) as per details given below & attached as per your requirement</b></span><br />");
            sb.Append("<br />");
            sb.Append(AirlineMailFormat(Pnr, DerartingPnr, TravelDate, PassengerName, LastName, VisaNo, VisaCopy, VisaType, Issue, Cut, Airline, OtbRefNo, out VisaLinks, In_TicketPath, Out_Ticket));
            //VisaLinks = GetTicketCopy(OtbRefNo, SamePnr) + ";" + GetCutVisaPath(PassengerName);
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Waiting for your quickest action and request you to kindly update us once it’s done.</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\"></b></span><br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">Best Regards,</b></span><br /><br />");
            sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
            sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
            sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");
            sb.Append("<div style=\"height: 5px\">");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            try
            {
                //int effected = 0;
                //SqlParameter[] sqlParams = new SqlParameter[4];
                //sqlParams[0] = new SqlParameter("@sTo", "kashifkhan4847@gmail.com");
                //sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                //sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                //sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                //retCode = DBHelper.ExecuteNonQuery("Proc_OTBMail", out effected, sqlParams);
                int effected = 0;
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@sTo", Email1);
                sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                sqlParams[3] = new SqlParameter("@Path", VisaLinks);
                sqlParams[4] = new SqlParameter("@Cc", CCEmailId);
                retCode = DBHelper.ExecuteNonQuery("Proc_AirlinesOTBMail", out effected, sqlParams);
                return retCode;
            }
            catch
            {
                return retCode;
            }
        }
        #endregion

        #region Send Mail to Agent
        public static bool SendEmailToAgent(string ArrivingPnr, string ArrivalAirLine, string Arrivalfrom, string DepartingFrom, Int64 PaxNo, string[] RefNo, string[] PassengerName, string[] PassportNo, string[] VisaNo, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, float TotalAmmount, bool Cut, string OtbRefNo)
        {

            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string sAgencyName = objGlobalDefaults.AgencyName;
            string Email = objGlobalDefaults.uid;
            string sSubject = "";
            string Currency = "";
            if (objGlobalDefaults.Currency == "INR")
            {

                Currency = "<img src=\"" + URL + "/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";

            }
            else if (objGlobalDefaults.Currency == "AED")
            {
                Currency = "AED ";
            }
            else if (objGlobalDefaults.Currency == "USD")
            {
                Currency = "$ ";
            }
            StringBuilder sb = new StringBuilder();
            if (Cut == true)
            {

                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td  style=\"width:10%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td  style=\"width:40%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                sb.Append("<td  style=\"width:50%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Permit No.<o:p></o:p></span></b></p></td>");
                //sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                string Name, PermitNo, No;
                DataTable dtResult;
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    VisaLoadByCode(PassengerName[i], out dtResult);
                    Name = dtResult.Rows[0]["FirstName"].ToString() + " " + dtResult.Rows[0]["LastName"].ToString();
                    sSubject = "Thanks for OTB request for " + Name + " * " + Convert.ToString(PassengerName.Length);
                    PermitNo = dtResult.Rows[0]["VisaNo"].ToString();
                    No = dtResult.Rows[0]["PassportNo"].ToString();
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\"  style=\"width:10%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:40%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + Name + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:50%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + PermitNo + "<o:p></o:p></span></b></p></td>");
                    //sb.Append("<td width=\"346\" style=\";padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + PermitNo + "<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");

                }
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("</tbody></table></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\'" + URL + "'/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");

            }
            else
            {
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying OTB for your passengers, your request have been proceeded and we’ll update you once updated by airlines.<o:p></o:p></span></p></span><br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 10%;\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 4%;\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 3.5%;\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 7%;\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222;margin-left: 5%;\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names:<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Permit No<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                sSubject = "Thanks for OTB request for " + RefNo[0] + " " + PassengerName[0] + " * " + Convert.ToString(PassengerName.Length);
                for (int i = 0; i < PassengerName.Length; i++)
                {
                    sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                    sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + (i + 1) + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + RefNo[i] + " " + PassengerName[i] + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>" + PassportNo[i] + "<o:p></o:p></span></b></p></td>");
                    sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">" + VisaNo[i] + "<o:p></o:p></span></b></p></td>");
                    sb.Append("</tr>");

                }
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">OTB Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("</tbody></table></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"" + URL + "/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\";padding-left:10px;margin-top: 2px;\" /></b></span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");


            }





            try
            {
                string sTo = Email;

                //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                //mailMsg.To.Add(sTo);
                //mailMsg.Subject = sSubject;
                //mailMsg.IsBodyHtml = true;
                //mailMsg.Body = sb.ToString();
                //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //mailObj.EnableSsl = true;
                //mailObj.Send(mailMsg);
                //mailMsg.Dispose();

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["OTBMail"]));

                List<string> attachmentList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sTo.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }


                //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, sSubject, sb.ToString(), from, attachmentList);
                if (reponse == true)
                {
                    AdminOtbMail(ArrivingPnr, ArrivalAirLine, Arrivalfrom, DepartingFrom, PaxNo, RefNo, PassengerName, PassportNo, VisaNo, VisaFee, UrgentCharges, OtherCharges, TotalPerPass, TotalAmmount, Cut, OtbRefNo);
                    return true;
                }
                return false;





            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Email for CUT & Franchisee

        public static bool AdminOtbMail(string ArrivingPnr, string ArrivalAirLine, string Arrivalfrom, string DepartingFrom, Int64 PaxNo, string[] RefNo, string[] PassengerName, string[] PassportNo, string[] VisaNo, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, float TotalAmmount, bool Cut, string OtbRefNo)
        {
            bool Status = false;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sAgentId = objGlobalDefaults.sid;
            string sAgencyName = objGlobalDefaults.AgencyName;
            try
            {
                DBHelper.DBReturnCode retCode;
                DataSet dsResult = new DataSet();
                DataTable dtAgent = new DataTable(), dtVisa = new DataTable(), dtFranchisee = new DataTable();
                string Location = "n/a", Franchisee = "n/a", ApplicantName = "n/a", ContactPerson = "n/a", FranchiseeMail = "";
                retCode = GetFranchiseeDetais(sAgentId, out dsResult);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    dtAgent = dsResult.Tables[0];
                    dtFranchisee = dsResult.Tables[1];
                    if (dtFranchisee.Rows.Count != 0)
                    {
                        if (dtFranchisee.Rows[0]["UserType"].ToString() == "Franchisee")
                        {
                            Franchisee = dtFranchisee.Rows[0]["AgencyType"].ToString();
                            FranchiseeMail = dtFranchisee.Rows[0]["uid"].ToString();
                        }
                        else
                        {
                            Franchisee = "ClickUrTrip.com";
                        }
                    }
                    if (dtAgent.Rows.Count != 0)
                    {
                        ContactPerson = dtAgent.Rows[0]["ContactPerson"].ToString();
                        Location = dtAgent.Rows[0]["Address"].ToString() + " -" + dtAgent.Rows[0]["PinCode"].ToString();
                    }

                }


                string sSubject = ContactPerson + " " + "Requested For new Otb Application " + OtbRefNo;
                StringBuilder sb = new StringBuilder();
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid white\">");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">New OTB application received,</span><br /><br /><br />");
                sb.Append("<table style=\"width:100%\">");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Agency Name :</span><td><td><b>" + ContactPerson + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Location :</span><br /><br /><td><td><b>" + Location + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Under :</span><br /><br /><td><td><b>" + Franchisee + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Airline : </span><br /><br /><td><td><b>" + ArrivalAirLine + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Airline PNR : </span><br /><br /><td><td><b>" + ArrivingPnr + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Arrival From : </span><br /><br /><td><td><b>" + Arrivalfrom + "</b></td></tr>");
                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Application No : </span><br /><br /><td><td><b>" + OtbRefNo + "</b></td></tr>");
                if (Cut == false)
                {
                    sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">PAX Name : </span><br /><br /><td><td><b>" + RefNo[0] + " " + PassengerName[0] + "* (" + PaxNo.ToString() + " )" + "</b></td></tr>");
                    sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Passport No : </span><br /><br /><td><td><b>" + VisaNo[0] + "</b></td></tr>");
                }
                else
                {
                    VisaLoadByCode(PassengerName[0], out dtVisa);
                    if (dtVisa.Rows.Count != 0)
                    {
                        //for(int i=0;i<dtVisa.Rows.Count;i++)
                        //{

                        //}
                        sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">PAX Name : </span><br /><br /><td><td><b>" + dtVisa.Rows[0]["FirstName"].ToString() + " " + dtVisa.Rows[0]["LastName"].ToString() + "* (" + dtVisa.Rows.Count.ToString() + ")" + "</b></td></tr>");
                        sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Passport No : </span><br /><br /><td><td><b>" + VisaNo[0] + "</b></td></tr>");
                    }

                }

                sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Form filled by: </span><br /><br /><td><td><b>" + sAgencyName + "</b></td></tr>");
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:0%\">");
                sb.Append("Kindly review the application and process for posting,<br /><br />");
                sb.Append("</span>");
                sb.Append("<span style=\"margin-left:0%\"><br /><br />");
                sb.Append("Regards");
                sb.Append("</span> <br/>");
                sb.Append("<span style=\"margin-left:0%\"><br /><br />");
                sb.Append("ClickUrTrip.com");
                sb.Append("</span> <br/>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
                string mail = sb.ToString();
                string sTo = FranchiseeMail;
                int effected = 0;
                SqlParameter[] sqlParams = new SqlParameter[4];
                if (Cut == false)
                {
                    sqlParams[0] = new SqlParameter("@Type", "OTB");
                    sqlParams[1] = new SqlParameter("@Activity", "OTB Applied");
                }
                else
                {
                    sqlParams[0] = new SqlParameter("@Type", "OTB");
                    sqlParams[1] = new SqlParameter("@Activity", "Visa Issued by us");
                }
                sqlParams[2] = new SqlParameter("@sSubject", sSubject);
                sqlParams[3] = new SqlParameter("@VarBody", sb.ToString());
                retCode = DBHelper.ExecuteNonQuery("Proc_ActivityMail", out effected, sqlParams);
                if (sTo != "")
                {
                    //effected = 0;
                    //SqlParameter[] sqlParams1 = new SqlParameter[3];
                    //sqlParams1[0] = new SqlParameter("@sTo", sTo);
                    //sqlParams1[1] = new SqlParameter("@sSubject", sSubject);
                    //sqlParams1[2] = new SqlParameter("@VarBody", sb.ToString());
                    //retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out effected, sqlParams1);


                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["OTBMail"]));

                    List<string> attachmentList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();

                    foreach (string mailItem in sTo.Split(','))
                    {
                        if (mailItem != "")
                        {
                            Email1List.Add(mailItem, mailItem);
                        }
                    }


                    //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                    return MailManager.SendMail(accessKey, Email1List, sSubject, sb.ToString(), from, attachmentList);



                }
            }
            catch
            {

            }

            return Status;
        }

        #region Get Franchisee Details On Agent
        public static DBHelper.DBReturnCode GetFranchiseeDetais(Int64 AgentId, out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@AgentId", AgentId);
            retCode = DBHelper.GetDataSet("Proc_GetAgentFranchiseeByAgent", out dsResult, sqlParams);

            return retCode;

        }
        #endregion Get Franchisee Details On Agent
        #endregion


        #region Airlines Format
        public static string AirlineMailFormat(string Pnr, string DerartingPnr, string TravelDate, string[] PassengerName, string[] LastName, string[] VisaNo, string[] VisaCopy, string[] VisaType, string[] Issue, bool IsCutOtb, string Airline, string OtbRefNo, out string VisaLink, string In_TicketPath, string Out_Ticket)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dtResult;
            bool SamePnr = false;
            if (Pnr == DerartingPnr)
            {
                SamePnr = true;
            }
            string PaxInfo = "";
            #region Indigo Mail
            if (Airline == "Indigo Airlines ")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left: 2%;background:white;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">SR. NO</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot; color:#222222\">PAX NAME</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#222222\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td></tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"> " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Pnr + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"307\" valign=\"top\" style=\"width:184.25pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"></span></span><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\"> " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:#222222\">" + Pnr + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region SpiceJet
            else if (Airline == "Spicejet Airlines")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\"><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR.NO</span></b></p></td>");
                sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">NO OF PASSENGER</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family: &quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">TRAVEL DATE</span></b><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td></tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"154\" valign=\"top\" style=\"width:92.15pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + PassengerName.Length.ToString() + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"163\" valign=\"top\" style=\"width:97.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region Air Arabia
            else if (Airline == "Air Arabia" || Airline == "Oman Airways" || Airline == "Fly Dubai")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA TYPE</span></b></p></td>");
                sb.Append("</tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"1", "Mr.").Replace(@"2", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["VisaNo"]) + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["IeService"]) + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaType[i] + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region AirIndiaExp
            else if (Airline == "Air India Express" || Airline == "Air India")
            {
                sb.Append("<table class=\"MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;; mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">SR. NO</span></b></p></td>");
                sb.Append("<td width=\"260\" valign=\"top\" style=\"width:155.9pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">PAX NAME</span></b></p></td>");
                sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">AIRLINE PNR</span></b></p></td>");
                sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">VISA NO</span></b></p></td>");
                sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><b><span style=\"font-size:12.0pt;font-family:&quot;Cambria&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">Travel Date</span></b></p></td>");
                sb.Append("</tr>");
                if (IsCutOtb == true)
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        VisaLoadByCode(PassengerName[i], out dtResult);
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">" + Convert.ToString(dtResult.Rows[i]["Gender"]).Replace(@"Male", "Mr.").Replace(@"Female", "Mrs.") + "</span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + Convert.ToString(dtResult.Rows[i]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[i]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[i]["LastName"]) + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + Convert.ToString(dtResult.Rows[i]["VisaNo"]) + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    for (int i = 0; i < PassengerName.Length; i++)
                    {
                        sb.Append("<tr style=\"mso-yfti-irow:1\">");
                        sb.Append("<td width=\"80\" valign=\"top\" style=\"width:47.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + (i + 1) + "</span></p></td>");
                        sb.Append("<td width=\"260\" valign=\"bottom\" style=\"width:155.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span class=\"SpellE\"><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\"></span></span><span style=\"font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black\">  " + PassengerName[i] + " " + LastName[i] + "&nbsp;&nbsp;</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></td>");
                        sb.Append("<td width=\"142\" valign=\"top\" style=\"width:85.05pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:10.5pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;\">" + Pnr + "</span></p></td>");
                        sb.Append("<td width=\"213\" valign=\"top\" style=\"width:127.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + VisaNo[i] + "</span></p></td>");
                        sb.Append("<td width=\"201\" valign=\"top\" style=\"width:120.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:normal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;\">" + TravelDate + "</span></p></td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");
            }
            #endregion

            #region Visa Link
            if (IsCutOtb == true)
            {
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetCutVisaPath(PassengerName);
            }
            else
            {
                VisaLink = GetTicketCopy(OtbRefNo, SamePnr, In_TicketPath, Out_Ticket) + ";" + GetOtherVisaPath(PassengerName, OtbRefNo);
            }
            #endregion

            return PaxInfo = sb.ToString();

        }
        #endregion

        #region OTB Mail Subject
        public static string MailSubject(string Airlines, Int64 PaxNo, string PassengerName)
        {
            string Subject = "";
            if (Airlines == "Indigo Airlines ") { Subject = "6E OTB – AGENT CODE: 00348 - " + PaxNo.ToString() + " PAX"; }
            else if (Airlines == "Air India") { Subject = "Fwd: AI - OK TO BOARD REQUEST - ( " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Air India Express") { Subject = "Fwd: IX- OK TO BOARD REQUEST- " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Spicejet Airlines") { Subject = "APPLY OTB " + PassengerName + "+" + PaxNo.ToString() + " PAX )"; }
            else if (Airlines == "Air Arabia") { Subject = "APPLY OTB " + PassengerName + "(" + PaxNo.ToString() + "PAX )"; }
            else { Subject = "OK TO BOARD REQUEST- " + PassengerName + " (" + PaxNo.ToString() + "- PAX )"; }
            return Subject;
        }
        #endregion

        #endregion

        #region OtbMarkups
        public static DBHelper.DBReturnCode GetAgentOtbMarkups(Int64 UserId, string Supplier, out DataSet dsResult)
        {
            //Supplier = "Jet";
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@AgentId", UserId);
            sqlparam[1] = new SqlParameter("@Supplier", Supplier);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentOtbMarkpDetailsByAgentId", out dsResult, sqlparam);
            return retCode;
        }
        #endregion

        #region Otb File Path
        public static string GetCutVisaPath(string[] RefNo)
        {
            string Path = "";
            string Colun = ";";
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (RefNo.Length - 1 != i)
                {
                    Path += "C:\\inetpub\\wwwroot\\VisaImages\\" + RefNo[i] + "_Visa.pdf;";
                }
                else
                {
                    Path += "C:\\inetpub\\wwwroot\\VisaImages\\" + RefNo[i] + "_Visa.pdf";
                }

            }
            return Path;

        }
        public static string GetOtherVisaPath(string[] RefNo, string OtbId)
        {
            string Path = "";
            for (int i = 0; i < RefNo.Length; i++)
            {
                if (i != (RefNo.Length - 1))
                {
                    Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf;";
                }
                else
                {
                    Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_UploadVisa" + ((i + 1)).ToString() + ".pdf";
                }

            }
            return Path;

        }
        public static string GetTicketCopy(string OtbId, bool SamePnr, string In_TicketPath, string Out_Ticket)
        {
            string Path = "";
            if (SamePnr == false)
            {
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_InBoundTicket." + In_TicketPath + ";";
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_OutBoundTicket." + Out_Ticket;
            }
            else
            {
                Path += "C:\\inetpub\\wwwroot\\OTBDocument\\" + OtbId + "_InBoundTicket." + In_TicketPath;
            }

            return Path;
        }
        #endregion
    }
}