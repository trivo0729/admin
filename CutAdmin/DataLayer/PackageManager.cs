﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.BL;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading;

namespace CutAdmin.DataLayer
{
    public class PackageManager
    {
        #region core ***********************************************
        public enum DBReturnCode
        {
            SUCCESS = 0,
            CONNECTIONFAILURE = -1,
            SUCCESSNORESULT = -2,
            SUCCESSNOAFFECT = -3,
            DUPLICATEENTRY = -4,
            EXCEPTION = -5,
            INVALIDXML = -11,
            INPUTPARAMETEREMPTY = -6,
            UNIQUEKEYVIOLATION = -7,
            REFERENCECONFLICT = -8,
            NOQUESTIONS = -9,
            INTERMISSION = -10,
            TESTCOMPLETED = -12,
            TESTTIMEOUT = -13,
            QUESTIONALREADYANSWERED = -14,
            TESTNOTFOUND = -15
        }
        public enum CountryReturnCode
        {
            SUCCESS = 0,
            EXCEPTION = -1,
            NORECORDSFOUND = -2,
            COUNTRYNOTFOUND = -3,
            COUNTRYEXISTS = -4,
        }
        private static SqlConnection OpenConnection()
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ClickurTripConnectionString"].ToString());
                conn.Open();
            }
            catch
            {
                conn = null;
            }
            return conn;
        }
        private static void CloseConnection(SqlConnection conn)
        {
            try
            {
                conn.Close();
                conn.Dispose();
            }
            catch { }
        }
        protected static DBReturnCode ExecuteQuery(string sQuery, out DataTable dtResult)
        {

            DBReturnCode retcode = DBReturnCode.SUCCESS;
            dtResult = null;
            SqlDataAdapter dataAdapter = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    dataAdapter = new SqlDataAdapter(sQuery, conn);
                    DataSet dsTmp = new DataSet();
                    dataAdapter.Fill(dsTmp);
                    dtResult = dsTmp.Tables[0];
                    if (dtResult.Rows.Count == 0)
                        return DBReturnCode.SUCCESSNORESULT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    dataAdapter.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        private static DBReturnCode ExecuteNonQuery(string sQuery)
        {
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    cmd = new SqlCommand(sQuery, conn);
                    if (cmd.ExecuteNonQuery() == 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2627 || ex.Number == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                    {
                        retcode = DBReturnCode.DUPLICATEENTRY;
                    }
                    else
                        retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        private static DBReturnCode ExecuteScaler(string sQuery, out Int64 nRecordID)
        {
            nRecordID = 0;
            /*HttpContext.Current.Response.Write(sQuery);
            HttpContext.Current.Response.Write("<br/>");*/
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    sQuery += "; SELECT SCOPE_IDENTITY();";
                    cmd = new SqlCommand(sQuery, conn);
                    nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                    if (nRecordID <= 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch //(Exception e)
                {
                    retcode = DBReturnCode.EXCEPTION;
                    //HttpContext.Current.Response.Write(e.Message); 
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }

        #region " DB Opration With Transection "

        public static DBReturnCode ExecuteQueryT(SqlConnection Conn, String sSQL, out DataTable dtResult)
        {
            DBReturnCode retCode = DBReturnCode.EXCEPTION;
            dtResult = null;

            try
            {
                using (SqlCommand Cmd = new SqlCommand(sSQL, Conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(sSQL, Conn))
                    {
                        da.Fill(dtResult);

                        if (dtResult.Rows.Count > 0)
                            retCode = DBReturnCode.SUCCESS;
                        else
                            retCode = DBReturnCode.SUCCESSNORESULT;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        #endregion " DB Opration With Transection "

        #endregion core
        public static DBHelper.DBReturnCode AddPackageBasicDetails(string sPackageName, string sCity, string sCategory, Int64 nDuration, string dvalidFrom, string dvalidupto, string sThemes, string sDesctiption, Decimal dTax, Int64 nCancelDays, Decimal dCancelCharge, string sTermsCondition, string sInventory, Int64 ParentID, out object nID)
        {

            DateTime dtFrom = ConvertDateTime(dvalidFrom);
            DateTime dtUpto = ConvertDateTime(dvalidupto);

            string dFrom = dtFrom.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            string dUpto = dtUpto.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);


            DataTable dtResult;
            int rowsAffected = 0;
            nID = 0;
            SqlParameter[] sqlParams = new SqlParameter[15];
            sqlParams[0] = new SqlParameter("@sPackageName", sPackageName);
            sqlParams[1] = new SqlParameter("@sPackageDestination", sCity);

            sqlParams[2] = new SqlParameter("@sPackageCategory", sCategory);
            sqlParams[3] = new SqlParameter("@sPackageThemes", sThemes);
            sqlParams[4] = new SqlParameter("@nDuration", nDuration);
            sqlParams[5] = new SqlParameter("@dValidityTo", dUpto);

            sqlParams[6] = new SqlParameter("@sPackageDescription", sDesctiption);
            sqlParams[7] = new SqlParameter("@dValidityFrom", dFrom);

            sqlParams[8] = new SqlParameter("@dTax", dTax);
            sqlParams[9] = new SqlParameter("@nCancelDays", nCancelDays);

            sqlParams[10] = new SqlParameter("@dCancelCharge", dCancelCharge);
            sqlParams[11] = new SqlParameter("@sTermsCondition", sTermsCondition);
            sqlParams[12] = new SqlParameter("@sInventory", sInventory);
            sqlParams[13] = new SqlParameter("@ParentID", ParentID);



            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_Insert_tbl_Package", out rowsAffected, sqlParams);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                SqlParameter[] sqlParams1 = new SqlParameter[6];
                sqlParams1[0] = new SqlParameter("@sPackageName", sPackageName);
                sqlParams1[1] = new SqlParameter("@sPackageDestination", sCity);

                sqlParams1[2] = new SqlParameter("@sPackageCategory", sCategory);
                sqlParams1[3] = new SqlParameter("@sPackageThemes", sThemes);
                sqlParams1[4] = new SqlParameter("@nDuration", nDuration);
                sqlParams1[5] = new SqlParameter("@dValidityTo", dUpto);



                retCode = DBHelper.GetDataTable("Proc_Inserted_tbl_Package_ID", out dtResult, sqlParams1);
                nID = dtResult.Rows[0]["nID"].ToString();
                string[] sPackageCategory = dtResult.Rows[0]["sPackageCategory"].ToString().Split(',');
                int count = sPackageCategory.Length;
                int ncount = 0;

                foreach (string scategory in sPackageCategory)
                {
                    SqlParameter[] sqlParams2 = new SqlParameter[3];
                    sqlParams2[0] = new SqlParameter("@nID", nID);
                    sqlParams2[1] = new SqlParameter("@scategory", Convert.ToInt64(scategory));
                    sqlParams2[2] = new SqlParameter("@CategoryName", GetCategoryName(Convert.ToInt64(scategory)));

                    DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_Insert_PackageToMultiple", out rowsAffected, sqlParams2);
                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        ncount++;
                    }
                }

                if (count == ncount)
                {
                    SqlParameter[] sqlParams3 = new SqlParameter[1];
                    sqlParams3[0] = new SqlParameter("@nID", nID);
                    DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_Insert_tbl_PackageImages", out rowsAffected, sqlParams3);
                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                        return DBHelper.DBReturnCode.SUCCESS;
                    else
                    {
                        return DBHelper.DBReturnCode.EXCEPTION;
                    }
                }

            }

            return retCode;
        }


        public static string GetCategoryName(Int64 categoryID)
        {
            if (categoryID == 1)
            {
                return "Standard";
            }
            else if (categoryID == 2)
            {
                return "Deluxe";
            }
            else if (categoryID == 3)
            {
                return "Premium";
            }
            else if (categoryID == 4)
            {
                return "Luxury";
            }
            else
            {
                return "no category found";
            }
        }


        public static DBHelper.DBReturnCode SinglePackageDetails(Int64 nID, out DataTable dtResult)
        {

            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_SinglePackageDetails", out dtResult, sqlParams);

            return retcode;
            //sSQL.Append("SELECT * FROM tbl_Package WHERE nID= ").Append(nID);
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBHelper.DBReturnCode UpdatePricingDetails(Int64 nPackageID, Int64 nCategoryID, String nCategoryName, Decimal dSingleAdult, Decimal dCouple, Decimal dExtraAdult, Decimal dInfantKid, Decimal dKidWBed, Decimal dKidWOBed, String sStaticInclusion, String sDynamicInclusion, String sStaticExclusion, String sDynamicExclusion)
        {

            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[12];
            sqlParams[0] = new SqlParameter("@nPackageID", nPackageID);
            sqlParams[1] = new SqlParameter("@nCategoryID", nCategoryID);
            sqlParams[2] = new SqlParameter("@dSingleAdult", dSingleAdult);
            sqlParams[3] = new SqlParameter("@dCouple", dCouple);
            sqlParams[4] = new SqlParameter("@dExtraAdult", dExtraAdult);
            sqlParams[5] = new SqlParameter("@dInfantKid", dInfantKid);
            sqlParams[6] = new SqlParameter("@dKidWBed", dKidWBed);
            sqlParams[7] = new SqlParameter("@dKidWOBed", dKidWOBed);
            sqlParams[8] = new SqlParameter("@sStaticInclusions", sStaticInclusion);
            sqlParams[9] = new SqlParameter("@sDynamicInclusion", sDynamicInclusion);
            sqlParams[10] = new SqlParameter("@sStaticExclusion", sStaticExclusion);
            sqlParams[11] = new SqlParameter("@sDynamicExclusion", sDynamicExclusion);


            DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_tbl_PackagePricing_Update", out rowsAffected, sqlParams);
            return retcode;


        }


        public static DBHelper.DBReturnCode GetPricingDetail(Int64 nID, out DataTable dtResult)
        {
            dtResult = null;

            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_GetPackage_Pricing", out dtResult, sqlParams);

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("SELECT * FROM tbl_PackagePricing WHERE nPackageID=").Append(nID).Append(" ORDER BY nPackageID, nCategoryID");
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
            return retcode;
        }


        public static DBHelper.DBReturnCode GetItineraryDetail(Int64 nID, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_Get_tbl_ItineraryDetail", out dtResult, sqlParams);

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("SELECT tbl_Itinerary.*,tbl_Package.nDuration FROM tbl_Itinerary INNER JOIN tbl_Package ON tbl_Package.nID= tbl_Itinerary.nPackageID WHERE nPackageID=").Append(nID);
            return retcode;
        }

        public static DBReturnCode SaveItinerary(Int64 nID, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("DELETE FROM tbl_Itinerary WHERE nPackageID=").Append(nID).Append(" AND nCategoryID=").Append(nCategoryID)
                .Append("; INSERT INTO tbl_HotelImages (nPackageID, nCategoryID, sCategoryName) VALUES (").Append(nID).Append(",").Append(nCategoryID).Append(",'").Append(sCategoryName).Append("');")
                .Append(" INSERT INTO tbl_Itinerary (nPackageID, nCategoryID, nCategoryName,");
            for (int i = 1; i <= listItinerary.Count; i++)
            {
                if (i == listItinerary.Count)
                {
                    sSQL.Append(" sItinerary_" + i).Append(")");
                }
                else
                {
                    sSQL.Append(" sItinerary_" + i).Append(",");
                }
            }
            sSQL.Append(" VALUES(").Append(nID).Append(",").Append(nCategoryID).Append(",'").Append(sCategoryName).Append("','");
            for (int i = 0; i < listItinerary.Count; i++)
            {
                if (i == listItinerary.Count - 1)
                {
                    sSQL.Append(listItinerary[i]).Append("')");
                }
                else
                {
                    sSQL.Append(listItinerary[i]).Append("','");
                }
            }
            return ExecuteNonQuery(sSQL.ToString());
        }

        //public static DBHelper.DBReturnCode SaveItinerary(Int64 nID, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        //{

        //    int rowsAffected = 0;
        //    SqlParameter[] sqlParams = new SqlParameter[18];

        //    for (int i = 0; i < listItinerary.Count; i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sItinerary_" + (i + 1), listItinerary[i]);
        //    }
        //    for (int i = listItinerary.Count; i <= 14; i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sItinerary_" + (i + 1), " ");
        //    }
        //    sqlParams[15] = new SqlParameter("@nID", nID);
        //    sqlParams[16] = new SqlParameter("@nCategoryID", nCategoryID);
        //    sqlParams[17] = new SqlParameter("@nCategoryName", sCategoryName);
        //    DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_Save_tbl_ItineraryDetail", out rowsAffected, sqlParams);
        //    return retcode;

        //    //    StringBuilder sSQL = new StringBuilder();
        //    //    sSQL.Append("DELETE FROM tbl_Itinerary WHERE nPackageID=").Append(nID).Append(" AND nCategoryID=").Append(nCategoryID)
        //    //        .Append("; INSERT INTO tbl_HotelImages (nPackageID, nCategoryID, sCategoryName) VALUES (").Append(nID).Append(",").Append(nCategoryID).Append(",'").Append(sCategoryName).Append("');")
        //    //        .Append(" INSERT INTO tbl_Itinerary (nPackageID, nCategoryID, nCategoryName,");
        //    //    for (int i = 1; i <= listItinerary.Count; i++)
        //    //    {
        //    //        if (i == listItinerary.Count)
        //    //        {
        //    //            sSQL.Append(" sItinerary_" + i).Append(")");
        //    //        }
        //    //        else
        //    //        {
        //    //            sSQL.Append(" sItinerary_" + i).Append(",");
        //    //        }
        //    //    }
        //    //    sSQL.Append(" VALUES(").Append(nID).Append(",").Append(nCategoryID).Append(",'").Append(sCategoryName).Append("','");
        //    //    for (int i = 0; i < listItinerary.Count; i++)
        //    //    {
        //    //        if (i == listItinerary.Count - 1)
        //    //        {
        //    //            sSQL.Append(listItinerary[i]).Append("')");
        //    //        }
        //    //        else
        //    //        {
        //    //            sSQL.Append(listItinerary[i]).Append("','");
        //    //        }
        //    //    }
        //    //    return ExecuteNonQuery(sSQL.ToString());
        //    //}
        //}

        public static DBHelper.DBReturnCode GetItineraryHotelDetail(Int64 nID, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_GetItineraryHotelDetail", out dtResult, sqlParams);
            return retcode;

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("SELECT tbl_ItineraryHotel.*, tbl_HotelImages.sHotelImage1,tbl_HotelImages.sHotelImage2,tbl_HotelImages.sHotelImage3,tbl_HotelImages.sHotelImage4,tbl_HotelImages.sHotelImage5,tbl_HotelImages.sHotelImage6,tbl_HotelImages.sHotelImage7,tbl_HotelImages.sHotelImage8,tbl_HotelImages.sHotelImage9,tbl_HotelImages.sHotelImage10,")
            //    .Append(" tbl_HotelImages.sHotelImage11,tbl_HotelImages.sHotelImage12,tbl_HotelImages.sHotelImage13,tbl_HotelImages.sHotelImage14,tbl_Package.nDuration FROM tbl_ItineraryHotel ")
            //    .Append("INNER JOIN tbl_Package ON tbl_Package.nID= tbl_ItineraryHotel.nPackageID INNER JOIN tbl_HotelImages ON (tbl_HotelImages.nCategoryID = tbl_ItineraryHotel.nCategoryID AND tbl_HotelImages.nPackageID=tbl_ItineraryHotel.nPackageID) WHERE tbl_ItineraryHotel.nPackageID=").Append(nID).Append(" ORDER BY tbl_ItineraryHotel.nCategoryID");
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }


        //public static DBHelper.DBReturnCode SaveHotelDetails(Int64 nID, List<String> listHotelName, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        //{
        //    int rowsAffected = 0;
        //    int y = 0;
        //    int j = 1;
        //    SqlParameter[] sqlParams1 = new SqlParameter[31];
        //    for (int i = 0; i < listHotelName.Count; i++)
        //    {
        //        sqlParams1[i] = new SqlParameter("@sHotelName_" + (i + 1), listHotelName[i]);
        //    }
        //    for (int i = listHotelName.Count; i <= 13; i++)
        //    {
        //        sqlParams1[i] = new SqlParameter("@sHotelName_" + (i + 1), " ");
        //    }

        //    for (int k = 14; k < (listItinerary.Count + 14); k++)
        //    {
        //        sqlParams1[k] = new SqlParameter("@sHotelDescrption_" + j, listItinerary[(j - 1)]);
        //        j++;
        //        y = k;
        //    }

        //    for (int z = (y + 1); z <= 27; z++)
        //    {
        //        sqlParams1[z] = new SqlParameter("@sHotelDescrption_" + j, " ");
        //        j++;
        //    }
        //    sqlParams1[28] = new SqlParameter("@nID", nID);
        //    sqlParams1[29] = new SqlParameter("@nCategoryID", nCategoryID);
        //    sqlParams1[30] = new SqlParameter("@sCategoryName", sCategoryName);
        //    DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_SaveHotelDetails", out rowsAffected, sqlParams1);
        //    return retcode;

        //}


        public static DBHelper.DBReturnCode SaveHotelDetails(Int64 nID, List<String> listHotelName, List<String> listHotelCode, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        {
            int rowsAffected = 0;
            int y = 0;
            int n = 0;
            int j = 1;
            int m = 1;
            SqlParameter[] sqlParams1 = new SqlParameter[45];
            for (int i = 0; i < listHotelName.Count; i++)
            {
                sqlParams1[i] = new SqlParameter("@sHotelName_" + (i + 1), listHotelName[i]);
            }
            for (int i = listHotelName.Count; i <= 13; i++)
            {
                sqlParams1[i] = new SqlParameter("@sHotelName_" + (i + 1), " ");
            }

            for (int l = 14; l < (listHotelCode.Count + 14); l++)
            {
                sqlParams1[l] = new SqlParameter("@sHotelCode_" + m, listHotelCode[(m - 1)]);
                m++;
                n = l;
            }

            for (int x = (n + 1); x <= 27; x++)
            {
                sqlParams1[x] = new SqlParameter("@sHotelCode_" + m, " ");
                m++;
            }

            for (int k = 28; k < (listItinerary.Count + 28); k++)
            {
                sqlParams1[k] = new SqlParameter("@sHotelDescrption_" + j, listItinerary[(j - 1)]);
                j++;
                y = k;
            }

            for (int z = (y + 1); z <= 41; z++)
            {
                sqlParams1[z] = new SqlParameter("@sHotelDescrption_" + j, " ");
                j++;
            }


            sqlParams1[42] = new SqlParameter("@nID", nID);
            sqlParams1[43] = new SqlParameter("@nCategoryID", nCategoryID);
            sqlParams1[44] = new SqlParameter("@sCategoryName", sCategoryName);
            DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_SaveHotelDetail", out rowsAffected, sqlParams1);
            return retcode;

        }




        //public static DBHelper.DBReturnCode SaveHotelDetails1(Int64 nID, List<String> listHotelName, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        //{
        //    int rowsAffected = 0;
        //    int y = 0;
        //    int j = 1;
        //    SqlParameter[] sqlParams = new SqlParameter[31];

        //    for (int i = 0; i < listHotelName.Count; i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sHotelName_" + (i + 1), listHotelName[i]);
        //    }
        //    for (int i = listHotelName.Count; i <= 13; i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sHotelName_" + (i + 1), " ");
        //    }


        //    for (int i = 14; i < (listItinerary.Count + 14); i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sHotelDescrption_" + j, listItinerary[(j - 1)]);
        //        j++;
        //        y = i;
        //    }

        //    for (int i = y; i < 27; i++)
        //    {
        //        sqlParams[i] = new SqlParameter("@sHotelDescrption_" + j, " ");
        //        j++;
        //    }

        //    sqlParams[28] = new SqlParameter("@nID", nID);
        //    sqlParams[29] = new SqlParameter("@nCategoryID", nCategoryID);
        //    sqlParams[30] = new SqlParameter("@sCategoryName", sCategoryName);
        //    DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_SaveHotelDetails", out rowsAffected, sqlParams);
        //    return retcode;


        //    //sSQL.Append("DELETE FROM tbl_ItineraryHotel WHERE nPackageID=").Append(nID).Append(" AND nCategoryID=").Append(nCategoryID)
        //    //    .Append("; INSERT INTO tbl_ItineraryHotel (nPackageID, nCategoryID, sCategoryName,");
        //    //for (int i = 1; i <= listItinerary.Count; i++)
        //    //{
        //    //    if (i == listItinerary.Count)
        //    //    {
        //    //        sSQL.Append(" sHotelName_" + i).Append(",sHotelDescrption_" + i).Append(")");
        //    //    }
        //    //    else
        //    //    {
        //    //        sSQL.Append(" sHotelName_" + i).Append(",sHotelDescrption_" + i).Append(",");
        //    //    }
        //    //}
        //    //sSQL.Append(" VALUES(").Append(nID).Append(",").Append(nCategoryID).Append(",'").Append(sCategoryName).Append("','");
        //    //for (int i = 0; i < listItinerary.Count; i++)
        //    //{
        //    //    if (i == listItinerary.Count - 1)
        //    //    {
        //    //        sSQL.Append(listHotelName[i]).Append("','").Append(listItinerary[i]).Append("')");
        //    //    }
        //    //    else
        //    //    {
        //    //        sSQL.Append(listHotelName[i]).Append("','").Append(listItinerary[i]).Append("','");
        //    //    }
        //    //}
        //    //return ExecuteNonQuery(sSQL.ToString());
        //}

        public static DBHelper.DBReturnCode Update_HotelImages(Int64 nPackageID, Int64 nCategoryID, string sCategoryName, string sImageName, string sFileName)
        {
            int rowsAffected = 0;

            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@sImageName", sImageName);
            sqlParams[1] = new SqlParameter("@sFileName", sFileName);
            sqlParams[2] = new SqlParameter("@nPackageID", nPackageID);
            sqlParams[3] = new SqlParameter("@nCategoryID", nCategoryID);
            DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_Update_HotelImages", out rowsAffected, sqlParams);
            return retcode;

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("UPDATE tbl_HotelImages SET ").Append(sImageName).Append("='").Append(sFileName).Append("' WHERE nPackageID=").Append(nPackageID).Append(" AND nCategoryID=").Append(nCategoryID);
            //return ExecuteNonQuery(sSQL.ToString());
        }

        public static DBHelper.DBReturnCode GetProductImages(Int64 nPackageID, out DataTable dtResult)
        {

            dtResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nPackageID", nPackageID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_GetProductImages", out dtResult, sqlParams);
            return retcode;
        }

        public static DBHelper.DBReturnCode add_ProductImagesData(Int64 PackageID, string sFileNameArray)
        {
            int rowsAffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@nPackageID", PackageID);
            sqlParams[1] = new SqlParameter("@ImageArray", sFileNameArray);
            DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_add_ProductImagesData", out rowsAffected, sqlParams);
            return retcode;
            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("DELETE FROM tbl_PackageImages WHERE nPackageID=").Append(PackageID).Append("; INSERT INTO tbl_PackageImages (nPackageID,ImageArray) VALUES(").Append(PackageID).Append(",'").Append(sFileNameArray).Append("')");
            //return ExecuteNonQuery(sSQL.ToString());
        }
        public static DBHelper.DBReturnCode GetPackageDetails(out DataTable dtResult)
        {

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetPackageDetails", out dtResult);
            return retCode;

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("SELECT * FROM tbl_Package");
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

       

        public static DBHelper.DBReturnCode DeletePackage(Int64 nID)
        {
            int rows = 0;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_DeletePackage", out rows, sqlParams);
            return retCode;
            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("DELETE FROM tbl_Package WHERE nID=").Append(nID).Append("; DELETE FROM tbl_PackagePricing WHERE nPackageID=").Append(nID)
            //    .Append("; DELETE FROM tbl_Itinerary WHERE nPackageID=").Append(nID).Append(";DELETE FROM tbl_ItineraryHotel WHERE nPackageID=").Append(nID)
            //    .Append("; DELETE FROM tbl_HotelImages WHERE nPackageID=").Append(nID).Append("; DELETE FROM tbl_PackageImages WHERE nPackageID=").Append(nID);
            //return ExecuteNonQuery(sSQL.ToString());
        }

        public static DBHelper.DBReturnCode GetItineraryCounts(Int64 nID, out DataTable dtResult)
        {
            dtResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_GetItineraryCounts", out dtResult, sqlParams);
            return retcode;

            //sSQL.Append("SELECT nDuration, sPackageCategory FROM tbl_Package WHERE nID=").Append(nID);
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }


        public static DBHelper.DBReturnCode GetCategoryCounts(Int64 nID, out DataTable dtResult)
        {

            dtResult = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode = DBHelper.GetDataTable("Proc_GetCategoryCounts", out dtResult, sqlParams);
            return retcode;


            //sSQL.Append("SELECT sPackageCategory FROM tbl_Package WHERE nID=").Append(nID);
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        public static DBHelper.DBReturnCode UpdatePackageBasicDetails(Int64 nID, string sPackageName, string sCity, string sCategory, Int64 nDuration, string dvalidFrom, string dvalidupto, string sThemes, string sDesctiption, Decimal dTax, Int64 nCancelDays, Decimal dCancelCharge, string sTermsCondition, string sInventory)
        {
            List<string> sCategorytoAdd = new List<string>();
            List<string> sCategorytoDelete = new List<string>();


            DateTime dtFrom = ConvertDateTime(dvalidFrom);
            DateTime dtUpto = ConvertDateTime(dvalidupto);

            string dFrom = dtFrom.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            string dUpto = dtUpto.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);

            DataTable dtResult = new DataTable();
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retcode1 = DBHelper.GetDataTable("Proc_sPackageCategoryLoadBynID", out dtResult, sqlParams);
            string[] sOldCategory = dtResult.Rows[0]["sPackageCategory"].ToString().Split(',');
            string[] sNewCategory = sCategory.Split(',');
            foreach (string cat in sNewCategory)
            {
                if (cat != "")
                {
                    if (!sOldCategory.Contains(cat))
                    {
                        sCategorytoAdd.Add(cat);
                    }
                }
            }
            foreach (string oldCat in sOldCategory)
            {
                if (oldCat != "")
                {
                    if (!sNewCategory.Contains(oldCat))
                    {
                        sCategorytoDelete.Add(oldCat);
                    }
                }
            }
            int rows = 0;
            SqlParameter[] sqlParams1 = new SqlParameter[14];
            sqlParams1[0] = new SqlParameter("@sPackageName", sPackageName);
            sqlParams1[1] = new SqlParameter("@sCity", sCity);
            sqlParams1[2] = new SqlParameter("@sCategory", sCategory);
            sqlParams1[3] = new SqlParameter("@nDuration", nDuration);
            sqlParams1[4] = new SqlParameter("@sThemes", sThemes);
            sqlParams1[5] = new SqlParameter("@dvalidFrom", dFrom);
            sqlParams1[6] = new SqlParameter("@dvalidupto", dUpto);
            sqlParams1[7] = new SqlParameter("@sDesctiption", sDesctiption);
            sqlParams1[8] = new SqlParameter("@dTax", dTax);
            sqlParams1[9] = new SqlParameter("@nCancelDays", nCancelDays);
            sqlParams1[10] = new SqlParameter("@dCancelCharge", dCancelCharge);
            sqlParams1[11] = new SqlParameter("@sTermsCondition", sTermsCondition);
            sqlParams1[12] = new SqlParameter("@sInventory", sInventory);
            sqlParams1[13] = new SqlParameter("@nID", nID);
            DBHelper.DBReturnCode retCode2 = DBHelper.ExecuteNonQuery("Proc_Update_tbl_PackageBynID", out rows, sqlParams1);


            int newcount = sCategorytoAdd.Count;
            int nnewcount = 0;
            foreach (string scategory in sCategorytoAdd)
            {

                SqlParameter[] sqlParams2 = new SqlParameter[3];
                sqlParams2[0] = new SqlParameter("@nPackageID", nID);
                sqlParams2[1] = new SqlParameter("@scategory", Convert.ToInt64(scategory));
                sqlParams2[2] = new SqlParameter("@sCategoryName", (GetCategoryName(Convert.ToInt64(scategory))));
                DBHelper.DBReturnCode retCode3 = DBHelper.ExecuteNonQuery("Proc_Insert_CategoryUpdate", out rows, sqlParams2);


                if (retCode3 == DBHelper.DBReturnCode.SUCCESS)
                {
                    nnewcount++;
                }
            }
            int oldcount = sCategorytoDelete.Count;
            int noldcount = 0;
            foreach (string scategory in sCategorytoDelete)
            {
                SqlParameter[] sqlParams3 = new SqlParameter[2];
                sqlParams3[0] = new SqlParameter("@nPackageID", nID);
                sqlParams3[1] = new SqlParameter("@scategory", Convert.ToInt64(scategory));
                DBHelper.DBReturnCode retCode4 = DBHelper.ExecuteNonQuery("Proc_Delete_CategoryUpdate", out rows, sqlParams3);

                if (retCode4 == DBHelper.DBReturnCode.SUCCESS)
                {
                    noldcount++;
                }
            }
            if (newcount == nnewcount && oldcount == noldcount)
            {
                return DBHelper.DBReturnCode.SUCCESS;
            }
            else
            {
                return DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true &&

              Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true

&& Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true

&& Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true

&& Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode GetPackageDetailsForfran(Int64 ParentID, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ParentID", ParentID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetPackageDetailsForfran", out dtResult, sqlParams);
            return retCode;

            //StringBuilder sSQL = new StringBuilder();
            //sSQL.Append("SELECT * FROM tbl_Package");
            //return ExecuteQuery(sSQL.ToString(), out dtResult);
        }
    }
}