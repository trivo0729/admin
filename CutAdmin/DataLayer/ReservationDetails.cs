﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.BL;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class ReservationDetails
    {
        public string AffilateCode { get; set; }

        public string BookingStatus { get; set; }
        public tbl_CommonHotelReservation objHotelDetails { get; set; }
        public string sCheckIn { get; set; }
        public string sCheckOut { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public DateTime ChargeDate { get; set; }
        public DateTime DeadLineDate { get; set; }
        public DateTime HoldingTime { get; set; }
        public string BookingDate { get; set; }
        public string CurrentStatus { get; set; }
        public int noNights { get; set; }


        //... Booking Room Details ..//
        public List<tbl_CommonBookedRoom> sRooms { get; set; }

        //.. Amount & Currency Details ..//
        public string ComparedCurrency { get; set; }
       
        public float AdminCommission { get; set; }
        public CommonLib.Response.ServiceCharge objCancellationCharge { get; set; } /* Booking Amount  */
        public float ServiceTax { get; set; }
        public float CancelattionAmount { get; set; }
        public float Markup { get; set; }   
        public float  SupplierMarkup { get; set; }

        public float TotalAmount { get; set; }
        public float TotalPayable { get; set; }
        public float AddOnsCharge { get; set; }
        public List<CommonLib.Response.TaxRate> ListCharges { get; set; }

    }
}