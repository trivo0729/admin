﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.Common;
using CutAdmin.BL;
using CommonLib.Response;
using System.Web.Script.Serialization;
namespace CutAdmin.DataLayer
{
    public class SearchManager : RatesManager
    {
        #region Hotel Details
        /// <summary>
        /// Get HotelDeatils By Supplier
        /// </summary>
        /// <param name="HotelCodes"></param>
        /// <param name="CheckIn"></param>
        /// <param name="CheckOut"></param>
        /// <param name="Nationality"></param>
        /// <param name="SearchValid"></param>
        /// <param name="City"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        public static List<CommonHotelDetails> GetHotelDetails(Int64[] HotelCodes, string CheckIn, string CheckOut, string[] Nationality, string SearchValid, string Search_Params)
        {
            List<CommonHotelDetails> objHotelDetails = new List<CommonHotelDetails>();
            List<tbl_commonRoomDetail> ListRoom = new List<tbl_commonRoomDetail>();
            try
            {
                string City = "", Country = "";
                City = Search_Params.Split('_')[0];
                Country = Search_Params.Split('_')[1].Split(',')[1];
                if (HotelCodes.Length == 0)
                {
                    List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier();
                    using (var DB = new dbHotelhelperDataContext())
                    {
                        var db = new helperDataContext();
                        foreach (var SupplierID in arrSupplier)
                        {
                            var arrHotel = (from obj in DB.tbl_CommonHotelMasters
                                            join objSupplier in db.tbl_AdminLogins on obj.ParentID equals objSupplier.sid
                                            where obj.ParentID == SupplierID && obj.CityId.Contains(City) && obj.CountryId.Contains(Country) 
                                            select new HotelInfo
                                            {
                                                HotelName = obj.HotelName,
                                                sid = obj.sid,
                                                SupplierName = objSupplier.AgencyName
                                            }).ToList();
                            foreach (var objHotel in arrHotel)
                            {
                                List<RateGroup> ListRateGroup = GetRateGroup(objHotel.sid, CheckIn, CheckOut, Nationality, SearchValid);
                                if (ListRateGroup.Count != 0)
                                {
                                    var arrRates = ListRateGroup.Where(d => d.Total != 0).ToList();
                                    if (arrRates.Count != 0)
                                    objHotelDetails.Add(GetHotelDetails(objHotel.sid, Search_Params, ListRateGroup));
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var HotelID in HotelCodes)
                    {
                        List<RateGroup> ListRateGroup = GetRateGroup(HotelID, CheckIn, CheckOut, Nationality, SearchValid);
                        if (ListRateGroup.Count != 0)
                        {
                            var arrRates = ListRateGroup.Where(d => d.Total != 0).ToList();
                            if (arrRates.Count !=0)
                                objHotelDetails.Add(GetHotelDetails(HotelID, Search_Params, arrRates));
                        }
                    }
                }
                if (objHotelDetails.Count == 0)
                    throw new Exception("Hotels Not Available..");

                 HotelFilter objFliter = new HotelFilter();
                 objFliter.arrHotels = objHotelDetails;
                 objFliter.arrSupplier= objHotelDetails.Select(d => d.Supplier).Distinct().ToList();
                 objFliter.SearchParams = Search_Params;
                 objFliter.GenrateHotelFilter();
                 HttpContext.Current.Session["ModelHotel" + Search_Params] = objFliter;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return objHotelDetails;
        }

        public static HotelFilter GetSearchHotel(string Search_Params)
        {
            HotelFilter objHotel = new HotelFilter();
            try
            {
                if (HttpContext.Current.Session["ModelHotel" + Search_Params] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search_Params];   
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return objHotel;
        }

        public static string Availbility(string Serach, string RoomID, string RoomDescID, int RoomNo, Int64 HotelCode,string Supplier)
        {
            JavaScriptSerializer objSerilize = new JavaScriptSerializer();
            try
            {
                HotelFilter objHotel = new HotelFilter();
                if (HttpContext.Current.Session["ModelHotel" + Serach] == null)
                    throw new Exception("No Result Found.");
                objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Serach];
                CommonHotelDetails arrHotelDetails = objHotel.arrHotels.Where(d => d.HotelId == HotelCode.ToString()).FirstOrDefault();
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo, HotelCode.ToString(), Supplier);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), HotelCode);
                return objSerilize.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {

                return objSerilize.Serialize(new object { });
                ;
            }
        }
        #endregion

        #region ValidRates
        /// <summary>
        /// Validate Hotel have Ocuupancy &  Channels
        /// </summary>
        /// <returns></returns>
        public static List<RateGroup> GetRateGroup(Int64 HotelCode, string Checkin, string Checkout, string[] Nationality, string SearchValid)
        {
            List<RateGroup> RateList = new List<RateGroup>();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var HotelDetails = (from obj in DB.tbl_CommonHotelMasters where obj.sid == HotelCode select obj).FirstOrDefault();
                    var ListRoom = (from Room in DB.tbl_commonRoomDetails where Room.HotelId == HotelCode select Room).ToList();
                    if (ListRoom.Count != 0)
                    {
                       

                        RateList = GetRateList(HotelCode, Checkin, Checkout, Nationality, ListRoom, SearchValid);
                    }

                }
            }
            catch (Exception)
            {

            }
            return RateList;
        }

       
        #endregion

        #region Get Hotel Details
        public static CommonHotelDetails GetHotelDetails(Int64 HotelCode, string Search_Params, List<RateGroup> ListRate)
        {
            var arrHotel = new CommonLib.Response.CommonHotelDetails();
            try
            {
                GlobalDefault objGlobalDeafult = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                #region AvailRates
               
                string[] arrSearchAttr = Search_Params.Split('_');
                using (var DB = new helperDataContext())
                {
                    var db = new dbHotelhelperDataContext();
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMasters
                                        join objSupplier in DB.tbl_AdminLogins on obj.ParentID equals objSupplier.sid
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.sid,
                                            obj.LocationId,
                                            obj.HotelName,
                                            obj.HotelLatitude,
                                            obj.HotelLangitude,
                                            obj.HotelAddress,
                                            obj.HotelZipCode,
                                            obj.HotelImage,
                                            obj.SubImages,
                                            obj.HotelCategory,
                                            obj.HotelDescription,
                                            obj.HotelFacilities,
                                            Supplier = objSupplier.AgencyName,
                                            LocationName = "",
                                        }).FirstOrDefault();
                    string Image = arrHotelInfo.HotelImage;
                    string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                    List<Image> arrImage = new List<Image>();
                    if (Image != null)
                    {
                        List<string> Url = Image.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    if (arrHotelInfo.SubImages != null)
                    {
                        List<string> Url = arrHotelInfo.SubImages.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    List<string> sFacilities = new List<string>();
                    foreach (var objID in arrHotelInfo.HotelFacilities.Split(','))
                    {
                        if (objID != "")
                            sFacilities.Add((from obj in db.tbl_commonFacilities where obj.HotelFacilityID == Convert.ToInt64(objID) select obj).First().HotelFacilityName);
                    }

                     List<Location> arrLocation= new List<Location>();
                     string Location = "";
                     var arrLocationName = db.tbl_CommonLocations.Where(d => d.Lid == arrHotelInfo.LocationId).FirstOrDefault();
                     if (arrLocationName != null)
                         Location = db.tbl_CommonLocations.Where(d => d.Lid == arrHotelInfo.LocationId).FirstOrDefault().LocationName;
                     arrLocation.Add(new Location { Name = Location, HotelCode = arrHotelInfo.sid.ToString() });
                    arrHotel = new CommonHotelDetails
                    {
                        DateFrom = arrSearchAttr[2],
                        DateTo = arrSearchAttr[3],
                        HotelId = arrHotelInfo.sid.ToString(),
                        Description = arrHotelInfo.HotelDescription,
                        HotelName = arrHotelInfo.HotelName,
                        Category = arrHotelInfo.HotelCategory,
                        Address = arrHotelInfo.HotelAddress,
                        Image = arrImage,
                        RateList = ListRate,
                        Supplier = arrHotelInfo.Supplier,
                        Langitude = arrHotelInfo.HotelLangitude,
                        Latitude = arrHotelInfo.HotelLatitude,
                        Facility = sFacilities.Distinct().ToList(),
                        Location =arrLocation ,
                        Currency = objGlobalDeafult.Currency,
                    };
                }
                if(ListRate.Count !=0)
                {
                    arrHotel.MinPrice = ListRate.Select(d => d.Total).ToList().Min();
                    arrHotel.MaxPrice = ListRate.Select(d => d.Total).ToList().Max();
                    arrHotel.Total = arrHotel.MinPrice; 
                }
                #endregion
            }
            catch (Exception)
            {

            }
            return arrHotel;
        }
        #endregion
    }
}