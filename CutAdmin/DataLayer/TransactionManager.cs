﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class TransactionManager
    {
        public static DBHelper.DBReturnCode GetTransactions(out DataTable dtResult)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            //if (objGlobalDefault.UserType != "Supplier")
               // Uid = objGlobalDefault.ParentId;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            sqlParams[1] = new SqlParameter("@usertype", "Admin");
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_TransactionLoadAll", out dtResult, sqlParams);
            return retCode;
        }
    }
}