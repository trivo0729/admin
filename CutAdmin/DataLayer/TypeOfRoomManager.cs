﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class TypeOfRoomManager
    {


        public static DBHelper.DBReturnCode AddRoomType(string RoomType)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomType", RoomType);
            //sqlParams[1] = new SqlParameter("@Description", Description);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_InsertRoomType", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetRoomType(out DataTable dtResult)
        {
            dtResult = null;
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetRoomType", out dtResult);
            return retCode;
        }


        public static DBHelper.DBReturnCode DeleteRoomType(string RoomTypeID)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomTypeID", RoomTypeID);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_DeleteRoomType", out rows, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetDetails(string RoomTypeID, out DataTable dtResult)
        {
            int rows;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoomTypeID", RoomTypeID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("proc_GetRoomDetails", out dtResult, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode UpdateRoomType(string RoomType, Int64 id)
        {
            int rows = 0;
            //string UniqueCode = CountryCode + GenerateRandomString(4);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@id", id);
            sqlParams[1] = new SqlParameter("@RoomType", RoomType);
            

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("proc_updateRoomType", out rows, sqlParams);
            return retCode;
        }
    }
}