﻿using CutAdmin.BL;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class UploadVisaManager : VisaPostingManager
    {

        public static DBHelper.DBReturnCode GetVisa(out DataTable tbl_Visa)
        {
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAll", out tbl_Visa);
            //return retCode;
            tbl_Visa = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[0];

            //retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMaster", out tbl_Visa, sqlParams);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAllByAutomation", out tbl_Visa, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetPassword(string Username, out DataTable tbl_Visa)
        {
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAll", out tbl_Visa);
            //return retCode;
            tbl_Visa = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Username", Username);
            //retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMaster", out tbl_Visa, sqlParams);
            retCode = DBHelper.GetDataTable("Proc_tbl_EdnrdLoginDetailsLoadByUsername", out tbl_Visa, sqlParams);

            return retCode;
        }


        //public static bool VisaDownload(string[] RefrenceNo, string[] Appl_No, string Username, string Password, out string[] VisaNo, out string[] Status, string Supplier)
        //{
        //    string Path = HttpContext.Current.Server.MapPath("~/lib/").ToString();
        //    string Snap = HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap").ToString();
        //    bool Run = CheckIfProcessAlreadyRunning();
        //    driver = new PhantomJSDriver(Path);
        //    string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
        //    // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMinutes(120));
        //    VisaNo = new string[RefrenceNo.Length];
        //    Status = new string[RefrenceNo.Length];
        //    int k = 0; IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
        //    Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot();
        //    bool failure = false;
        //    IWebElement query = null; bool Valid = true;
        //    try
        //    {
        //        driver.Navigate().GoToUrl("https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=");
        //        while (driver.Url != "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=") { };
        //        js.ExecuteScript(SetValueByName("ssousername", Username));
        //        js = (IJavaScriptExecutor)driver;
        //        js.ExecuteScript(SetValueByName("password", Password));
        //        js.ExecuteScript(CheckedByName("terms"));
        //        js.ExecuteScript("javascript:fnValidate()");
        //        string Usser = "", ServiceType = "", Session = "", App_Id = "";
        //        while (driver.Url != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //        {
        //            //if (SeleniumManager.verify(driver, "/html/body/form/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[10]/td[2]"))
        //            //{
        //            //    AppNo[0] = "";
        //            //    Status[k] = "Error : Authentication has failed. Please enter valid username/password";
        //            //    Valid = false;
        //            //    break;
        //            //}
        //            //else
        //            continue;
        //        };


        //        if (Valid)
        //        {
        //            int i = 0;
        //            foreach (string Ref in Appl_No)
        //            {
        //                try
        //                {
        //                    if (failure)
        //                    {
        //                        driver.Dispose();
        //                        driver = new PhantomJSDriver(Path);
        //                        // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMinutes(120));
        //                        driver.Navigate().GoToUrl("https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=");
        //                        while (driver.Url != "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=") { };
        //                        js.ExecuteScript(SetValueByName("ssousername", Username));
        //                        js = (IJavaScriptExecutor)driver;
        //                        js.ExecuteScript(SetValueByName("password", Password));
        //                        js.ExecuteScript(CheckedByName("terms"));
        //                        js.ExecuteScript("javascript:fnValidate()");
        //                        while (driver.Url != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //                        {
        //                            continue;
        //                        };
        //                        failure = false;
        //                    }
        //                    if (driver.Url == "https://www.ednrd.ae/portal/page/portal/ePortal" || driver.Url == "https://www.ednrd.ae/portal/pls/portal/inimm_db.disp_visa_det")
        //                    {
        //                        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        Is.SaveAsFile(Snap + "\\" + Ref + "_Uploads.jpeg");
        //                        // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Uploads.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        try
        //                        {
        //                            string url = "";
        //                            driver.Navigate().GoToUrl("https://www.ednrd.ae/portal/pls/portal/PORTAL.wwa_app_module.new_instance?p_moduleid=22836738692");
        //                            IWebElement AppNo = driver.FindElement(By.Name("PRINT_VISA_NEW.DEFAULT.P_APP_ID.01"));
        //                            while (AppNo == null) { AppNo = driver.FindElement(By.Name("PRINT_VISA_NEW.DEFAULT.P_APP_ID.01")); }
        //                            js.ExecuteScript(SetValueByName("PRINT_VISA_NEW.DEFAULT.P_APP_ID.01", Ref.Replace(" ", "")));
        //                            // string[] listElement = Regex.Split(driver.PageSource.Replace("\r\n", ""), "p_session_id");
        //                            // listElement = Regex.Split(listElement[1], "\"");
        //                            // string Sessionid = "WWVM" + listElement[2];
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_Uploads.jpeg");
        //                            //js.ExecuteScript(SetValueByName("p_object_name", Sessionid));
        //                            IWebElement btn_submit = driver.FindElement(By.Name("PRINT_VISA_NEW.DEFAULT.SUBMIT_TOP.01"));
        //                            btn_submit.Click();
        //                            //js.ExecuteScript(SetValueByName("p_instance", "1"));

        //                            //js.ExecuteScript(SetValueByName("p_event_type", "ON_CLICK"));

        //                            //js.ExecuteScript(SetValueByName("p_user_args", ""));

        //                            System.Threading.Thread.Sleep(500);

        //                            // driver.FindElement(By.Name(Sessionid)).Submit();
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_Uploads.jpeg");
        //                            //js.ExecuteScript(Submit(Sessionid, Ref.Replace(" ", "")));
        //                            while (driver.Url != "https://www.ednrd.ae/portal/pls/portal/!PORTAL.wwa_app_module.accept") { }
        //                            System.Threading.Thread.Sleep(500);
        //                            string txt = driver.PageSource;
        //                            //VisaNo = GetVisaNo(txt);
        //                            //if (VisaNo == "")
        //                            //{
        //                            //    continue;
        //                            //}

        //                            string[] Split = Regex.Split(txt, "<a href=");
        //                            string[] anchortag = Regex.Split(Split[1], "\"");

        //                            try
        //                            {
        //                                string[] aaVisa = Regex.Split(txt, "<a href=");
        //                                aaVisa = Regex.Split(aaVisa[1], "<u>");
        //                                aaVisa = Regex.Split(aaVisa[1], "</u>");
        //                                if (aaVisa.Length == 2)
        //                                {
        //                                    VisaNo[i] = aaVisa[0];
        //                                }
        //                                else
        //                                {
        //                                    VisaNo[i] = "";
        //                                }

        //                            }
        //                            catch
        //                            {
        //                                VisaNo[i] = "";
        //                            }

        //                            js.ExecuteScript(anchortag[1]);
        //                            while (driver.Url == "https://www.ednrd.ae/portal/pls/portal/!PORTAL.wwa_app_module.accept") { }
        //                            System.Threading.Thread.Sleep(500);
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_Uploads.jpeg");
        //                            string Visa = driver.PageSource;
        //                            //bool Downlaod;
        //                            //string DownloadPath;
        //                            //System.Threading.Thread.Sleep(2000);
        //                            //Visa = myDoc.documentElement.outerHTML;
        //                            while (Visa == "") { };
        //                            Visa = Regex.Replace(Visa, @"/images/posting_files", "https://www.ednrd.ae/images/posting_files");
        //                            Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/images/posting_files/moi_logo.gif", "http://clickurtrip.com//VisaImages//moi_logo.gif");
        //                            Visa = Regex.Replace(Visa, @"INIMM_DB.VISA_VIEW_IMAGE_VISA.show", "https://clickurtrip.com//VisaImages//" + RefrenceNo[i] + "_1.jpg");
        //                            //Downlaod = PassportImage(Visa, RefNo[i], out DownloadPath);
        //                            //Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/portal/pls/portal/INIMM_DB.VISA_VIEW_IMAGE_VISA.show?p_arg_names=v_row_id&amp;p_arg_values=", DownloadPath);
        //                            Visa = Regex.Replace(Visa, @"amp;", "");
        //                            Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/images/posting_files/barcodegif", "http://clickurtrip.com/VisaImages/barcodegif");
        //                            Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/images/posting_files/signature_mol.jpg", "http://clickurtrip.com/VisaImages//signature_mol.jpg");
        //                            Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/images/posting_files/amer_logo.gif", "http://clickurtrip.com/VisaImages//amer_logo.gif");
        //                            Visa = Regex.Replace(Visa, @"https://www.ednrd.ae/images/posting_files/visa_instructions.jpg", "");
        //                            Visa = Regex.Replace(Visa, @"https://clickurtrip.com/VisaImages//am_logo.gif", "https://clickurtrip.com/VisaImages//am_logo.gif");
        //                            Visa = Regex.Replace(Visa, @"BreakMyPage", "");
        //                            Visa = Regex.Replace(Visa, "amp", "");
        //                            System.Threading.Thread.Sleep(2000);
        //                            HttpContext context = System.Web.HttpContext.Current;
        //                            var htmlContent = Visa;
        //                            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
        //                            pdfConverter.Size = NReco.PdfGenerator.PageSize.A4;
        //                            var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
        //                            //root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        //                            //pdfConverter.Size = NReco.PdfGenerator.PageSize.A4;
        //                            //pdfConverter.PdfToolPath = @"D:\Kashif\ClickUrTrip\CUT\CUT\Agent";
        //                            ////pdfConverter.PdfToolPath = "C:\\inetpub\\vhosts\\clickurtrip.com\\httpdocs\\Agent";
        //                            //pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\httpdocs\\Agent";

        //                            //context.Response.ContentType = "application/pdf";
        //                            root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        //                            downloadPath = HttpContext.Current.Server.MapPath("~/VisaImages/").ToString() + "\\" + RefrenceNo[i] + "_Visa.pdf";
        //                            FileStream file = File.Create(downloadPath);
        //                            file.Write(pdfBytes, 0, pdfBytes.Length);
        //                            file.Close();
        //                            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaStatus(RefrenceNo[i], VisaNo[i]);
        //                        }
        //                        catch
        //                        {
        //                            continue;
        //                        }



        //                    }


        //                }
        //                catch
        //                {
        //                    try
        //                    {
        //                        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                    }
        //                    catch
        //                    {

        //                    }

        //                    Status[k] = "Fail";
        //                    k++;
        //                }
        //                i++;
        //            }
        //        }
        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Status[0] = ex.Message;
        //        driver.Dispose();
        //        // CheckIfProcessAlreadyRunning();
        //        return true;
        //    }

        //}


        #region CozmoPosting
        //public static bool CozmoDownload(string[] RefrenceNo, string[] PaxType, string Username, string Password, out string[] AppNo, out string[] Status, string Supplier)
        //{
        //    string Path = HttpContext.Current.Server.MapPath("~/lib/").ToString();
        //    string Snap = HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap").ToString();
        //    bool Run = CheckIfProcessAlreadyRunning();
        //    //IWebDriver driver = new PhantomJSDriver(Path);
        //    // IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
        //    IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
        //    string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
        //    AppNo = new string[RefrenceNo.Length];
        //    Status = new string[RefrenceNo.Length];
        //    IAlert Alerts;
        //    int k = 0; IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
        //    Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot();
        //    string ValidateStatus = "";
        //    IWebElement query = null; bool bvalid = true;
        //    try
        //    {
        //        driver.Navigate().GoToUrl("http://czt.dyndns.info:99/cozmovisa/login.aspx");
        //        while (driver.Url != "http://czt.dyndns.info:99/cozmovisa/login.aspx") { };
        //        js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtLoginName", Username));
        //        js = (IJavaScriptExecutor)driver;
        //        js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPassword", Password));

        //        js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnLogin').click();");
        //        System.Threading.Thread.Sleep(1000);
        //        if (driver.Url == "http://czt.dyndns.info:99/cozmovisa/login.aspx")
        //            throw new Exception("Unable to login");
        //        driver.Navigate().GoToUrl("http://czt.dyndns.info:99/cozmovisa/TranxVisaSales.aspx");
        //        while (driver.Url != "http://czt.dyndns.info:99/cozmovisa/TranxVisaSales.aspx") { }

        //        #region Visa Type and Visa Details
        //        IWebElement element = driver.FindElement(By.Id("ctl00_cphTransaction_ddlVisaFeeCode"));
        //        SelectElement oSelect = new SelectElement(element);
        //        oSelect.SelectByValue("6500");
        //        int Adult, Child, Infant;
        //        NoPax(out  Adult, out   Child, out  Infant, PaxType);
        //        if (Adult != 1)
        //        {
        //            IWebElement SelAdult = driver.FindElement(By.Id("ctl00_cphTransaction_ddlAdults"));
        //            SelectElement oSelAdult = new SelectElement(SelAdult);
        //            oSelAdult.SelectByValue(Adult.ToString());
        //        }

        //        if (Child != 0)
        //        {
        //            IWebElement SelChild = driver.FindElement(By.Id("ctl00_cphTransaction_ddlChildren"));
        //            SelectElement oSelChild = new SelectElement(SelChild);
        //            oSelChild.SelectByValue(Child.ToString());
        //        }
        //        if (Infant != 0)
        //        {
        //            IWebElement SelInfant = driver.FindElement(By.Id("ctl00_cphTransaction_ddlInfants"));
        //            SelectElement oSelInfant = new SelectElement(SelInfant);
        //            oSelInfant.SelectByValue(Infant.ToString());
        //        }
        //        #endregion


        //        //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //        //// OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);

        //        #region Add Nultipule Visa
        //        int p = 0;
        //        foreach (string Ref in RefrenceNo)
        //        {
        //            DataTable dtVisaDetails = new DataTable();
        //            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaLoadByCode(Ref, out dtVisaDetails);
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {

        //                try
        //                {
        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxName", dtVisaDetails.Rows[0]["FirstName"].ToString()));// First Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxSurName", dtVisaDetails.Rows[0]["LastName"].ToString()));// Last Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxPassport", dtVisaDetails.Rows[0]["PassportNo"].ToString()));// Passport No


        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxProfession", dtVisaDetails.Rows[0]["Profession"].ToString()));// Profession 

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtFatherName", dtVisaDetails.Rows[0]["FatherName"].ToString()));// Father Name 

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtMotherName", dtVisaDetails.Rows[0]["MotherName"].ToString()));// Mother Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$dcPaxPspExpiryOn$Date", GetDateTime(dtVisaDetails.Rows[0]["ExpDate"].ToString())));// Passport Expiry

        //                    js.ExecuteScript(SelectDropDown(PaxType[p].Replace("Adult", "ADULT"), "ctl00_cphTransaction_ddlPaxType"));  // Pax Type

        //                    js.ExecuteScript(SelectDropDown(dtVisaDetails.Rows[0]["MaritalStatus"].ToString().Replace("1", "MARRIED").Replace("2", "SINGLE"), "ctl00_cphTransaction_ddlVstMaritalStatus"));


        //                    if (dtVisaDetails.Rows[0]["MaritalStatus"].ToString() == "1" && dtVisaDetails.Rows[0]["Gender"].ToString() == "1")
        //                    {
        //                        if (dtVisaDetails.Rows[0]["HusbandMame"].ToString() != "")
        //                            js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtSpouseName", dtVisaDetails.Rows[0]["HusbandMame"].ToString()));// Spouse Name
        //                        else
        //                            ValidateStatus = "Spouse Name cannot be blank !\n";
        //                    }


        //                    IWebElement SelNationality = driver.FindElement(By.Id("ctl00_cphTransaction_ddlPaxNationality"));
        //                    SelectElement oSelNationality = new SelectElement(SelNationality);
        //                    oSelNationality.SelectByValue(Nationality(dtVisaDetails.Rows[0]["PresentNationality"].ToString()));


        //                    js.ExecuteScript(SelectDropDown(dtVisaDetails.Rows[0]["Gender"].ToString().Replace("1", "Male").Replace("2", "Female"), "ctl00_cphTransaction_ddlVstSex"));// Present Nationality

        //                    string today = DateTime.Now.Date.AddDays(6).ToShortDateString();
        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$dcTravelOn$Date", GetDateTime(today)));// Travel Date

        //                    js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnPaxAdd').click();");
        //                    try
        //                    {
        //                        var myvar = (string)js.ExecuteScript("var myvar = document.getElementById('ctl00_cphTransaction_lnkNewPax').style.display; return myvar");
        //                        if (myvar == "none")
        //                        {
        //                            AppNo[k] = "";
        //                            Status[k] = ValidateStatus;
        //                            k++;
        //                            bvalid = false;
        //                            break;
        //                        }

        //                    }
        //                    catch
        //                    {

        //                    }
        //                    System.Threading.Thread.Sleep(500);
        //                    if (driver.PageSource.Contains("Your session has been expired"))
        //                    {
        //                        throw new Exception("Your session has been expired");
        //                    }

        //                    if (p <= RefrenceNo.Length - 1)
        //                        js.ExecuteScript("document.getElementById('ctl00_cphTransaction_lnkNewPax').click();");
        //                    p++;
        //                }
        //                catch (Exception ex)
        //                {
        //                    if (ex.Message != "Your session has been expired")
        //                    {
        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        AppNo[k] = "";
        //                        Status[k] = "Posting Fail";
        //                        k++;
        //                    }
        //                    else
        //                    {
        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        AppNo[k] = "";
        //                        Status[k] = ex.Message;
        //                        k++; break;
        //                    }

        //                }
        //                //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                //// OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Basic.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //            }
        //            else
        //            {
        //                AppNo[k] = "";
        //                Status[k] = "Application Not Found";
        //                k++;
        //            }

        //        }
        //        #endregion

        //        #region Document Submission
        //        if (bvalid == true)
        //        {
        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl08$ITchkReqSelect"));

        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl07$ITchkReqSelect"));


        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl06$ITchkReqSelect"));

        //            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //            // OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Save.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //            //js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnSave').click();");
        //        }

        //        #endregion

        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.Contains("errorMessage"))
        //            Status[0] = "Posting Error!!";
        //        else
        //            Status[0] = ex.Message;

        //        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //        // OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }

        //}
        #endregion

    }
}