﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin.DataLayer
{
    /// <summary>
    /// Summary description for VisaDetailsManager
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VisaDetailsManager : System.Web.Services.WebService
    {
        #region Visa Add Updte
        public static DBHelper.DBReturnCode AddVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 UserId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            string Sponsor = "Not Assign";
            DBHelper.DBReturnCode retCode;
            if (sTelephone == "")
            {
                sTelephone = "0";
            }
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            float AcyualAmmount = Convert.ToSingle(TotalAmount) - objAgentInfo.AgentVisaMarkup;
            TotalAmount = Convert.ToString(AcyualAmmount);
            SqlParameter[] sqlParams = new SqlParameter[48];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", UserId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaInsert", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode AddIncompleteVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 UserId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            string Sponsor = "Not Assign";
            DBHelper.DBReturnCode retCode;
            if (sTelephone == "")
            {
                sTelephone = "0";
            }
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            float AcyualAmmount = Convert.ToSingle(TotalAmount) - objAgentInfo.AgentVisaMarkup;
            TotalAmount = Convert.ToString(AcyualAmmount);
            SqlParameter[] sqlParams = new SqlParameter[48];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", UserId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaInsertIncomplete", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode UpdateVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 UserId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            string Sponsor = "Not Assign";
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[48];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", UserId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByAgentSide", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode UpdateVisaoffline(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 UserId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, string Supplier, string App_No, string Visa_No)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[50];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Supplier);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", UserId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            sqlParams[48] = new SqlParameter("@VisaNo", Visa_No);
            sqlParams[49] = new SqlParameter("@TReference", App_No);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByoffline", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode DocumentStatus(string VCode)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            string Dstatus = "Documents Submitted";
            sqlParams[0] = new SqlParameter("@DocumentsStatus", Dstatus);
            sqlParams[1] = new SqlParameter("@VCode", VCode);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByStatus", out rowsAffected, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode GetAgentMarkups(Int64 UserId, string Process, out DataSet dsResult)
        {
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@AgentId", UserId);
            sqlparam[1] = new SqlParameter("@Supplier", Process);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentVisaMarkpDetailsByAgentId", out dsResult, sqlparam);
            return retCode;
        }

        public static DBHelper.DBReturnCode ConformBooking(string VCode, out string Message)
        {
            Message = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            DBHelper.DBReturnCode retCode = BL.DBHelper.DBReturnCode.EXCEPTION;
            DataTable dtResult;
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@VisaCode", VCode);
                retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out dtResult, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {

                    Int64 uid = 0;
                    int rowsaffected = 0;
                    float BookingAmtWithoutTax = Convert.ToSingle(dtResult.Rows[0]["TotalAmount"]);
                    string PassengerName = Convert.ToString(dtResult.Rows[0]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[0]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[0]["LastName"]);
                    //float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["ServiceTax"]);
                    float BookingServiceTax = 0;
                    string BookingDate = dtResult.Rows[0]["IssuingDate"].ToString();
                    string BookedBy = dtResult.Rows[0]["UserId"].ToString();
                    string BookingRemark = "";
                    float SupplierBasicAmt = BookingAmtWithoutTax;
                    float CutComm = SupplierBasicAmt;
                    float AgentComm = SupplierBasicAmt;
                    AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                    //if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
                    //{
                    //    objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                    //    uid = objAgentDetailsOnAdmin.sid;
                    //    if (BookedBy == "")
                    //        BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
                    //}
                    //else if (HttpContext.Current.Session["LoginUser"] != null)
                    //{
                    objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    if (objGlobalDefault.UserType == "Agent")
                        uid = objGlobalDefault.sid;
                    else
                        uid = Convert.ToInt64(dtResult.Rows[0]["UserId"]);
                    if (BookedBy == "")
                        BookedBy = objGlobalDefault.Agentuniquecode;
                    //BookingAmtWithoutTax = BookingAmtWithoutTax - objAgentInfo.AgentVisaMarkup;
                    //BookingAmtWithoutTax = BookingAmtWithoutTax;
                    // }
                    string Perticulars = "Booking (Visa)" + PassengerName;
                    SqlParameter[] sqlParams1 = new SqlParameter[22];
                    sqlParams1[0] = new SqlParameter("@uid", uid);
                    sqlParams1[1] = new SqlParameter("@ReservationID", VCode);
                    sqlParams1[2] = new SqlParameter("@TransactionDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                    sqlParams1[3] = new SqlParameter("@BookingAmt", BookingAmtWithoutTax);
                    sqlParams1[4] = new SqlParameter("@SeviceTax", BookingServiceTax);
                    sqlParams1[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithoutTax);
                    sqlParams1[6] = new SqlParameter("@CanAmt", 0);
                    sqlParams1[7] = new SqlParameter("@CanServiceTax", 0);
                    sqlParams1[8] = new SqlParameter("@CanAmtWithTax", 0);
                    sqlParams1[9] = new SqlParameter("@Balance", 0);
                    sqlParams1[10] = new SqlParameter("@SalesTax", BookingServiceTax);
                    sqlParams1[11] = new SqlParameter("@LuxuryTax", 0);
                    sqlParams1[12] = new SqlParameter("@DiscountPer", 0);
                    sqlParams1[13] = new SqlParameter("@BookingCreationDate", BookingDate);
                    sqlParams1[14] = new SqlParameter("@BookingRemarks", BookingRemark);
                    sqlParams1[15] = new SqlParameter("@BookingCancelFlag", 0);
                    sqlParams1[16] = new SqlParameter("@Supplier", "Visa");
                    sqlParams1[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
                    sqlParams1[18] = new SqlParameter("@AgentComm", AgentComm);
                    sqlParams1[19] = new SqlParameter("@CUTComm", CutComm);
                    sqlParams1[20] = new SqlParameter("@BookedBy", BookedBy);
                    sqlParams1[21] = new SqlParameter("@Particulars", Perticulars);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByVisa", out rowsaffected, sqlParams1);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        SqlParameter[] sqlParams2 = new SqlParameter[2];
                        int rowsAffected = 0;
                        string Dstatus = "Documents Submitted";
                        sqlParams2[0] = new SqlParameter("@DocumentsStatus", Dstatus);
                        sqlParams2[1] = new SqlParameter("@VCode", VCode);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByStatus", out rowsAffected, sqlParams2);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            Message = "Visa Transaction Completed";
                            objTransactionScope.Complete();
                        }
                    }
                    else
                    {
                        Message = "Visa Transaction Fails";
                        objTransactionScope.Dispose();
                    }
                }
                else
                {
                    Message = "Visa Not Find";
                    objTransactionScope.Dispose();
                }
            }
            //Message = "";
            return retCode;

        }

        public static DBHelper.DBReturnCode VisaBookingTxnAdd(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName)
        {
            int rowsaffected;

            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uid = objAgentDetailsOnAdmin.sid;
                if (BookedBy == "")
                    BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            }
            else if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                uid = objGlobalDefault.sid;
                if (BookedBy == "")
                    BookedBy = objGlobalDefault.Agentuniquecode;
            }
            string Perticulars = "Booking (Visa)" + PassengerName;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticulars);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByVisa", out rowsaffected, sqlParams);
            return retCode;
        }

        #endregion

        #region Get Visa
        public static DBHelper.DBReturnCode UpdateUploadStatus(string VCode, int DocNo)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@VCode", VCode);
            sqlParams[1] = new SqlParameter("@DocNo", DocNo);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaStatusUpdateByDocumentUpload", out rowsAffected, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode UpdateCropedStatus(string VCode, int DocNo)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@VCode", VCode);
            sqlParams[1] = new SqlParameter("@DocNo", DocNo);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaStatusUpdateByCropedStatus", out rowsAffected, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode VisaStatus(string VCode, string VisaNo)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[4];
            int rowsAffected = 0;
            int VStatus = 1;
            sqlParams[0] = new SqlParameter("@VStatus", VStatus);
            sqlParams[1] = new SqlParameter("@VCode", VCode);
            sqlParams[2] = new SqlParameter("@CopletedDate", Convert.ToString(DateTime.Now));
            sqlParams[3] = new SqlParameter("@VisaNo", VisaNo);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaStatusUpdateByAdmin", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode Visa(Int64 @DisplayLength, Int64 @DisplayStart, string @Search, int @SortCol, string @SortDirec, out DataTable tbl_Visa)
        {
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAll", out tbl_Visa);
            //return retCode;
            tbl_Visa = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[6];
            string Franchisee = objGlobalDefault.Franchisee;
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@DisplayLength", @DisplayLength);
            sqlParams[02] = new SqlParameter("@DisplayStart", @DisplayStart);
            sqlParams[03] = new SqlParameter("@Search", @Search);
            sqlParams[04] = new SqlParameter("@SortCol", @SortCol);
            sqlParams[05] = new SqlParameter("@SortDirec", @SortDirec);
            //retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMaster", out tbl_Visa, sqlParams);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMasterDataTable", out tbl_Visa, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetPostingVisa(Int64 @DisplayLength, Int64 @DisplayStart, string @Search, int @SortCol, string @SortDirec, out DataTable tbl_Visa)
        {
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAll", out tbl_Visa);
            //return retCode;
            tbl_Visa = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[6];
            string Franchisee = objGlobalDefault.Franchisee;
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@DisplayLength", @DisplayLength);
            sqlParams[02] = new SqlParameter("@DisplayStart", @DisplayStart);
            sqlParams[03] = new SqlParameter("@Search", @Search);
            sqlParams[04] = new SqlParameter("@SortCol", @SortCol);
            sqlParams[05] = new SqlParameter("@SortDirec", @SortDirec);
            //retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMaster", out tbl_Visa, sqlParams);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsMasterLoadBYPosting", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisa(out DataTable tbl_Visa)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadMaster", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode OfflineVisa(out DataTable tbl_Visa)
        {
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadAll", out tbl_Visa);
            //return retCode;
            tbl_Visa = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[1];
            string Franchisee = objGlobalDefault.Franchisee;
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsLoadOffline", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode VisaLoadByUserId(out DataTable tbl_Visa, Int64 UserId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UserId", UserId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByUserId", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetAgentByUid(out DataTable tbl_AgentDetails, Int64 sid)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@AgentId", sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAgentId", out tbl_AgentDetails, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaByCode(out DataTable tbl_Visa, string VisaCode)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaByPassport(out DataTable tbl_Visa, string VisaCode, string Passport)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            sqlParams[1] = new SqlParameter("@Passport", Passport);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCodeAndPassport", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetLanguages(out DataTable tbl_Visa, string Code)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Code", Code);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLanguageByCode", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetCountry(out DataTable tbl_VisaCountry, string Code)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Code", Code);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaCountryByCode", out tbl_VisaCountry, sqlParams);
            return retCode;
        }
        //public static DBHelper.DBReturnCode GetLanguages(out DataTable tbl_Languaes, string Code)
        //{
        //    SqlParameter[] sqlParams = new SqlParameter[1];
        //    sqlParams[0] = new SqlParameter("@Code", Code);
        //    DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLanguageByCode", out tbl_Languaes, sqlParams);
        //    return retCode;
        //}
        public static DBHelper.DBReturnCode GetVisaDetailsAgentByDate(Int64 Uid, string dFrom, string dTo, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@UserId", Uid);
            sqlParams[1] = new SqlParameter("@Start", dFrom);
            sqlParams[2] = new SqlParameter("@End", dTo);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByDateandUserId", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaDetailsByDate(string dFrom, string dTo, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Start", dFrom);
            sqlParams[1] = new SqlParameter("@End", dTo);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByDate", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaByService(out DataTable tbl_Visa, Int64 type, string Process)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@sid", type);
            sqlParams[1] = new SqlParameter("@Processing", Process);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaChargesLoadByService", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetCountry(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_HCityLoadAllCountryforVisa", out dtResult);
            return retCode;
        }
        public static DBHelper.DBReturnCode VisaLoadByCode(string Vcode, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", Vcode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region Update Visa Charges
        public static DBHelper.DBReturnCode UpdateVisaCharges(Int64 sid, decimal Urgent, decimal Visa, decimal Other, decimal Service, decimal Total, Int64 UserId, string Process)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[8];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Urgent", Urgent);
            sqlParams[2] = new SqlParameter("@Visa", Visa);
            sqlParams[3] = new SqlParameter("@Other", Other);
            sqlParams[4] = new SqlParameter("@Service", Service);
            sqlParams[5] = new SqlParameter("@Total", Total);
            sqlParams[6] = new SqlParameter("@uid", UserId);
            sqlParams[7] = new SqlParameter("@Processing", Process);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_VisaChargesUpdateByAdmin", out rowsAffected, sqlParams);
            return retCode;

        }
        #endregion

        #region Visa Mail Sending
        public static DBHelper.DBReturnCode SendEmailAdmin(string sAgencyName, Int64 sAgentId, string AgentUniqueCode, string nMobile, string sEmail, string RefNo)
        {
            DBHelper.DBReturnCode retCode;
            DataSet dsResult = new DataSet();
            DataTable dtAgent = new DataTable(), dtVisa = new DataTable(), dtFranchisee = new DataTable();
            string Location = "n/a", Franchisee = "n/a", VisaType = "n/a",
            ApplicantName = "n/a", PassportNo = "n/a", Nationality = "n/a", Formfilled = "n/a",
            ContactPerson = "n/a", FranchiseeMail = "", Processing = "Normal";
            retCode = GetAgentDetails(sAgentId, RefNo, out dsResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                dtAgent = dsResult.Tables[0];
                dtVisa = dsResult.Tables[1];
                dtFranchisee = dsResult.Tables[2];
                if (dtFranchisee.Rows.Count != 0)
                {
                    if (dtFranchisee.Rows[0]["UserType"].ToString() == "Franchisee")
                    {
                        Franchisee = dtFranchisee.Rows[0]["AgencyType"].ToString();
                        FranchiseeMail = dtFranchisee.Rows[0]["uid"].ToString();
                    }
                    else
                    {
                        Franchisee = "ClickUrTrip.com";
                    }
                }
                if (dtAgent.Rows.Count != 0)
                {
                    ContactPerson = dtAgent.Rows[0]["ContactPerson"].ToString();
                    Location = dtAgent.Rows[0]["Address"].ToString() + " -" + dtAgent.Rows[0]["PinCode"].ToString();
                }
                if (dtVisa.Rows.Count != 0)
                {
                    VisaType = dtVisa.Rows[0]["IeService"].ToString();
                    ApplicantName = dtVisa.Rows[0]["FirstName"].ToString() + " " + dtVisa.Rows[0]["MiddleName"].ToString() + " " + dtVisa.Rows[0]["LastName"].ToString();
                    PassportNo = dtVisa.Rows[0]["PassportNo"].ToString();
                    string CuntryCode = "";
                    CuntryCode = dtVisa.Rows[0]["PresentNationality"].ToString();
                    DataTable dtNationality;
                    retCode = GetCountry(out dtNationality, CuntryCode);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        Nationality = dtNationality.Rows[0]["Country"].ToString();
                    }

                }


            }

            if (dtVisa.Rows[0]["Processing"] == "2")
                Processing = "Urgent";
            string sSubject = dtAgent.Rows[0]["AgencyName"].ToString() + " " + "Requested For new Visa Application " + RefNo;
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.Append("<head>");
            sb.Append("<meta charset=\"utf-8\" />");
            sb.Append("</head>");
            sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid white\">");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("<div style=\"height:50px;width:auto\">");
            sb.Append("<br />");
            //sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("<span style=\"margin-left:2%;font-weight:400\">New Visa application received,</span><br /><br /><br />");
            sb.Append("<table style=\"width:100%\">");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Agency Name :</span><td><td><b>" + dtAgent.Rows[0]["AgencyName"].ToString() + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Location :</span><br /><br /><td><td><b>" + Location + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Under :</span><br /><br /><td><td><b>" + Franchisee + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Application No : </span><br /><br /><td><td><b>" + RefNo + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Processing :</span><br /><br /><td><td><b>" + Processing + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Visa Type :</span><br /><br /><td><td> <b>" + VisaType + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Applicant Name : </span><br /><br /><td><td><b>" + ApplicantName + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Passport No : </span><br /><br /><td><td><b>" + PassportNo + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Nationality : </span><br /><br /><td><td><b>" + Nationality + "</b></td></tr>");
            sb.Append("<tr><td><span style=\"margin-left:12%;font-weight:400\">Form filled by: </span><br /><br /><td><td><b>" + sAgencyName + "</b></td></tr>");
            sb.Append("</table>");
            sb.Append("<br />");
            sb.Append("<br />");
            sb.Append("<span style=\"margin-left:0%\">");
            sb.Append("Kindly review the application and process for posting,<br /><br />");
            sb.Append("</span>");
            sb.Append("<span style=\"margin-left:0%\"><br /><br />");
            sb.Append("Regards");
            sb.Append("</span> <br/>");
            sb.Append("<span style=\"margin-left:0%\"><br /><br />");
            sb.Append("ClickUrTrip.com");
            sb.Append("</span> <br/>");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("</div>");
            //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
            //sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");



            try
            {
                string sTo = FranchiseeMail;
                int effected = 0;
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@Type", "Visa");
                sqlParams[1] = new SqlParameter("@Activity", "Apply");
                sqlParams[2] = new SqlParameter("@sSubject", sSubject);
                sqlParams[3] = new SqlParameter("@VarBody", sb.ToString());
                retCode = DBHelper.ExecuteNonQuery("Proc_ActivityMail", out effected, sqlParams);
                if (sTo != "")
                {
                    //effected = 0;
                    //SqlParameter[] sqlParams1 = new SqlParameter[3];
                    //sqlParams1[0] = new SqlParameter("@sTo", sTo);
                    //sqlParams1[1] = new SqlParameter("@sSubject", sSubject);
                    //sqlParams1[2] = new SqlParameter("@VarBody", sb.ToString());
                    //retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out effected, sqlParams1);

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["OTBMail"]));

                    List<string> attachmentList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();

                    foreach (string mailItem in sTo.Split(','))
                    {
                        if (mailItem != "")
                        {
                            Email1List.Add(mailItem, mailItem);
                        }
                    }


                    //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                    bool response = MailManager.SendMail(accessKey, Email1List, sSubject, sb.ToString(), from, attachmentList);
                    if (response.Equals(true))
                    {
                        SendEmailToAgent(RefNo);
                        retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                    }


                    //return true;
                } return retCode;
            }
            catch
            {
                return retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
        }
        #endregion

        #region Visa Invoice
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode GetAgentMarkp(Int64 Uid, out DataTable dtable)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_MarkUpGetAgentMarkup", out dtable, sqlParams);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AgentMarkupLoadByVisa", out dtable, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode VisaInvoiceDetails(string RefNo, Int64 UID, out DataSet ds)
        {
            SqlParameter[] SQLParams = new SqlParameter[2];
            SQLParams[0] = new SqlParameter("@ReservationID", RefNo);
            SQLParams[1] = new SqlParameter("@sid", UID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_VisaInvoiceDetails", out ds, SQLParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaApplication(out DataTable tbl_Visa, string VisaCode)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return retCode;
        }
        #endregion

        #region Visa Search
        public static DBHelper.DBReturnCode GetVisaCode(string Vcode, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@Vcode", Vcode);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNo", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if (objGlobalDefault.UserType == "Admin")
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@Vcode", Vcode);
                retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNo", out dtResult, sqlParams);
            }
            else if (objGlobalDefault.UserType == "Franchisee")
            {
                SqlParameter[] sqlParams = new SqlParameter[2];
                string Franchisee = objGlobalDefault.Franchisee;
                sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
                sqlParams[1] = new SqlParameter("@Vcode", Vcode);
                retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoByMaster", out dtResult, sqlParams);
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Int64 uid = objGlobalDefault.sid;
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@uid", uid);
                sqlParams[1] = new SqlParameter("@Vcode", Vcode);
                retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByRefNoOnAdminStaff", out dtResult, sqlParams);
            }
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaCodeUser(string Vcode, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Vcode", Vcode);
            sqlParams[1] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByUserRefNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaApplication(string Name, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@FirstName", Name);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByApplicant", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@FirstName", Name);
            retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByApplicantByMaster", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaApplicationByUser(string Name, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@FirstName", Name);
            sqlParams[1] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByApplicantUserSide", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaDcumentNo(string PassportNo, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@PassportNo", PassportNo);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByDocument", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            sqlParams[1] = new SqlParameter("@PassportNo", PassportNo);
            retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByDocumentNoByMaster", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetVisaDcumentNoByUser(string PassportNo, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@PassportNo", PassportNo);
            sqlParams[1] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByDocumentByUser", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetAgencyName(string AgencyName, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tblVisaLoadByAgencyName", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[1] = new SqlParameter("@ParentId", UserId);
            retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyNameMaster", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode SerchVisaAll(string RefNo, string ApplicationName, string PassportNo, string Status, string fromDate, string ToDate, string VisaType, out DataTable dtResult)
        {
            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            uid = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@RefNo", RefNo);
            sqlParams[1] = new SqlParameter("@ApplicationName", ApplicationName);
            sqlParams[2] = new SqlParameter("@PassportNo", PassportNo);
            sqlParams[3] = new SqlParameter("@Status", Status);
            sqlParams[4] = new SqlParameter("@fromDate", fromDate);
            sqlParams[5] = new SqlParameter("@ToDate", ToDate);
            sqlParams[6] = new SqlParameter("@VisaType", VisaType);
            sqlParams[7] = new SqlParameter("@uid", uid);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsBySearchAll", out dtResult, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode AdminSerchVisaAll(string RefNo, string ApplicationName, string PassportNo, string Status, string fromDate, string ToDate, string VisaType, Int64 Agency, out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@RefNo", RefNo);
            sqlParams[1] = new SqlParameter("@ApplicationName", ApplicationName);
            sqlParams[2] = new SqlParameter("@PassportNo", PassportNo);
            sqlParams[3] = new SqlParameter("@Status", Status);
            sqlParams[4] = new SqlParameter("@fromDate", fromDate);
            sqlParams[5] = new SqlParameter("@ToDate", ToDate);
            sqlParams[6] = new SqlParameter("@VisaType", VisaType);
            sqlParams[7] = new SqlParameter("@Agency", Agency);
            retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsByAdminSearchAll", out dtResult, sqlParams);
            return retCode;

        }
        #endregion

        #region Change Status
        public static DBHelper.DBReturnCode UpdateVisaStatus(Int64 sid, string status)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Status", status);
            sqlParams[1] = new SqlParameter("@UserId", sid);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByStatus", out rowsAffected, sqlParams);
            return retCode;

        }
        #region Application No
        public static DBHelper.DBReturnCode UpdateStatusbyApplication(string VisaNo, string Username, string Password, string AppNo)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[6];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Status", "Posted");
            sqlParams[1] = new SqlParameter("@Vcode", VisaNo);
            sqlParams[2] = new SqlParameter("@Username", Username);
            sqlParams[3] = new SqlParameter("@Password", Password);
            sqlParams[4] = new SqlParameter("@ApplicationNo", AppNo);
            sqlParams[5] = new SqlParameter("@Supplier", Username);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaStatusByUpdateByIEPosting", out rowsAffected, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode AOfflineVisa(Int64 sid, string FirstName, string MiddleName, string LastName, string Gender, string VisaType, string Nationality, string ApplicationNo, string Sponsor, string Vcode)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[10];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@FirstName", FirstName);
            sqlParams[1] = new SqlParameter("@MiddleName", MiddleName);
            sqlParams[2] = new SqlParameter("@Gender", Gender);
            sqlParams[3] = new SqlParameter("@IeService", VisaType);
            sqlParams[4] = new SqlParameter("@PresentNationality", Nationality);
            sqlParams[5] = new SqlParameter("@TReference", ApplicationNo);
            sqlParams[6] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[7] = new SqlParameter("@LastName", LastName);
            sqlParams[8] = new SqlParameter("@Vcode", Vcode);
            sqlParams[9] = new SqlParameter("@sid", sid);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_VisaDetailsAddOffline", out rowsAffected, sqlParams);
            return retCode;

        }
        #endregion
        #endregion

        #region Visa New Charges
        public static DBHelper.DBReturnCode GetVisaByNationality(out DataTable tbl_Visa, string type, string Nationality)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@VisaType", type);
            sqlParams[1] = new SqlParameter("@Nationality", Nationality);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaChargesLoadByNationality", out tbl_Visa, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode UpdateChargesByNationality(string Type, string Nationality, string Currency, decimal Fee, decimal Urgent)
        {
            DBHelper.DBReturnCode retCode;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uid = objGlobalDefault.sid;
            SqlParameter[] sqlParams = new SqlParameter[6];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Type", Type);
            sqlParams[1] = new SqlParameter("@Nationality", Nationality);
            sqlParams[2] = new SqlParameter("@Currency", Currency);
            sqlParams[3] = new SqlParameter("@Fee", Fee);
            sqlParams[4] = new SqlParameter("@Urgent", Urgent);
            sqlParams[5] = new SqlParameter("@Uid", uid);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_VisaChargesUpdate", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode GetVisaChargesByCurrency(out DataTable tbl_Visa, string type, string Nationality, string Currency)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@VisaType", type);
            sqlParams[1] = new SqlParameter("@Nationality", Nationality);
            sqlParams[2] = new SqlParameter("@Currency", Currency);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaChargesLoadByCurrency", out tbl_Visa, sqlParams);
            return retCode;
        }

        #endregion

        #region Visa Conformation Mail
        #region Conformation Mail To Admin
        public static bool VisaStatusMail(string VisaRefNo, Int64 UserId, string status)
        {

            DBHelper.DBReturnCode retCode;
            string sAgencyName, AgentUniqueCode, sEmail, Name, Message;
            DataSet dsResult;
            DataTable tbl_Visadetails, tbl_AgentDetails;
            retCode = GetAgentDetails(UserId, VisaRefNo, out dsResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                tbl_Visadetails = dsResult.Tables[1];
                tbl_AgentDetails = dsResult.Tables[0];
                sAgencyName = tbl_AgentDetails.Rows[0]["ContactPerson"].ToString();
                AgentUniqueCode = tbl_AgentDetails.Rows[0]["Agentuniquecode"].ToString();
                sEmail = tbl_AgentDetails.Rows[0]["uid"].ToString();
                Name = sAgencyName;
                Message = "Your Visa Application No " + VisaRefNo + " is " + status;


                //string sSubject = sAgencyName + " " + "Requested For new Visa Application";
                StringBuilder sb = new StringBuilder();
                //sb.Append("<!DOCTYPE html>");
                //sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                //sb.Append("<head>");
                //sb.Append("<meta charset=\"utf-8\" />");
                //sb.Append("</head>");
                //sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
                //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
                //sb.Append("</div>");
                //sb.Append("<div style=\"height:50px;width:auto\">");
                //sb.Append("<br />");
                ////sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                //sb.Append("</div>");
                //sb.Append("<div>");
                ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello Admin,</span><br />");
                ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">" + sSubject + "</b></span><br />");
                ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agency Name : <b>" + sAgencyName + "</b></span><br />");
                ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Agent Unique Code : <b>" + AgentUniqueCode + "</b></span><br />");
                ////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Email-ID : <b>" + sEmail + "</b></span><br />");
                //////sb.Append("<span style=\"margin-left:2%;font-weight:400\">Mobile Number : <b>" + nMobile + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello Admin,</span><br /><br />");
                //sb.Append("<span style=\"margin-left:12%;font-weight:400\">" + sSubject + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:12%;font-weight:400\">Application No : <b>" + VisaRefNo + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:12%;font-weight:400\">Agency Name : <b>" + sAgencyName + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:12%;font-weight:400\">Agent Unique Code : <b>" + AgentUniqueCode + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:12%;font-weight:400\">Email-ID : <b>" + sEmail + "</b></span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                //sb.Append("<span style=\"margin-left:2%\">");
                //sb.Append("<b>Thank You,</b><br />");
                //sb.Append("</span>");
                //sb.Append("<span style=\"margin-left:2%\">");
                //sb.Append("Administrator");
                //sb.Append("</span>");
                //sb.Append("</div>");
                //sb.Append("<div>");
                //sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                //sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                //sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
                //sb.Append("</tr>");
                //sb.Append("</table>");
                //sb.Append("</div>");
                //sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
                //sb.Append("</div>");
                //sb.Append("</body>");
                //sb.Append("</html>");

                sb.Append("<!DOCTYPE html>  ");
                sb.Append("<html lang=\"en' id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">  ");
                sb.Append("<head>  ");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>  ");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">  ");
                sb.Append("<div  height:\"5px\">  ");
                sb.Append("</div>  ");
                sb.Append("<div style=\"height:50px;width:auto\">  ");
                sb.Append("    <br />  ");
                sb.Append("    <img src=\"http://www.clickurtrip.co.in/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />  ");
                sb.Append("</div>  ");
                sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />  ");
                sb.Append("<div>  ");
                sb.Append("    <div style=\"margin-left:5%\">  ");
                sb.Append("        <p> <span style=\"margin-left:2%;font-weight:400\">Hello  " + Name + ",</span><br /></p>  ");
                sb.Append("      <p><span style=\"margin-left:2%;font-weight:400\">' " + Message + "'</span><br /></p>  ");
                sb.Append("        <p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us.</span><br /></p>  ");
                sb.Append("         ");
                sb.Append("        <span style=\"margin-left:2%\">  ");
                sb.Append("        <b>Thank You,</b><br />  ");
                sb.Append("        </span>  ");
                sb.Append("        <span style=\"margin-left:2%\">  ");
                sb.Append("        Administrator  ");
                sb.Append("        </span>  ");
                sb.Append("    </div>  ");
                sb.Append(" </div>  ");
                sb.Append(" <img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />  ");
                sb.Append(" <div>  ");
                sb.Append("    <table border=\"0\" style=\"height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">  ");
                sb.Append("        <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">  ");
                sb.Append("            <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  2014 - 2015 ClickurTrip.com</div></td>  ");
                sb.Append("        </tr>  ");
                sb.Append("    </table>  ");
                sb.Append(" </div>  ");
                sb.Append(" </body>  ");
                sb.Append(" </html>' ");


                try
                {
                    ////string sTo = sEmail;
                    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    //mailMsg.To.Add(sEmail);
                    //mailMsg.Subject = sSubject;
                    //mailMsg.IsBodyHtml = true;
                    //mailMsg.Body = sb.ToString();
                    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    //mailObj.EnableSsl = true;
                    //mailObj.Send(mailMsg);
                    //mailMsg.Dispose();

                    string Activity = "";
                    if (status == "Incomplete")
                    {
                        Activity = "Incomplete application";
                    }
                    else if (status == "Approved")
                    {
                        Activity = "Application Approved";
                    }
                    else if (status == "Rejected")
                    {
                        Activity = "Application Rejected";
                    }
                    else if (status == "Entered in country")
                    {
                        Activity = "Entered Country	";
                    }
                    else if (status == "Left the country")
                    {
                        Activity = "Exit Country";
                    }
                    else if (status == "Documents Required")
                    {
                        Activity = "Documents Required	";
                    }
                    else if (status == "Posted")
                    {
                        Activity = "Visa Apply";
                    }
                    else if (status == "Under Process")
                    {
                        Activity = "Visa Apply";
                    }

                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["parentID"]);

                    helperDataContext db = new helperDataContext();
                    string BCcTeamMails = "", CcTeamMail = "";
                    string title = "Visa Status";
                    var sMail = new tbl_ActivityMail();
                    sMail = (from obj in db.tbl_ActivityMails where obj.Activity == Activity && obj.ParentID == ParentID select obj).FirstOrDefault();
                    if (sMail != null)
                    {
                        BCcTeamMails = sMail.Email;
                        CcTeamMail = sMail.CcMail;
                    }

                    SendVisaMail(BCcTeamMails, title, sb.ToString(), "", CcTeamMail);

                    return true;

                }
                catch
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

        }
        #endregion

        public static void SendVisaMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            //int effected = 0;
            //SqlParameter[] sqlParams = new SqlParameter[5];
            //sqlParams[0] = new SqlParameter("@sTo", sTo);
            //sqlParams[1] = new SqlParameter("@sSubject", title);
            //sqlParams[2] = new SqlParameter("@VarBody", sMail);
            //sqlParams[3] = new SqlParameter("@Path", DocPath);
            //sqlParams[4] = new SqlParameter("@Cc", "online@clickurtrip.com");
            //DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);

            List<string> from = new List<string>();
            List<string> DocPathList = new List<string>();
            Dictionary<string, string> Email1List = new Dictionary<string, string>();
            Dictionary<string, string> BccList = new Dictionary<string, string>();
            from.Add(Convert.ToString(ConfigurationManager.AppSettings["VISAMail"]));

            foreach (string mail in sTo.Split(','))
            {
                if (mail != "")
                {
                    Email1List.Add(mail, mail);
                }
            }
            foreach (string mail in Cc.Split(';'))
            {
                if (mail != "")
                {
                    BccList.Add(mail, mail);
                }
            }
            foreach (string link in DocPath.Split(';'))
            {
                if (link != "")
                {
                    DocPathList.Add(link);
                }
            }

            //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

            MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);


        }

        #region Conformation Mail To Agent
        public static DBHelper.DBReturnCode SendEmailToAgent(string VCode)
        {

            string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefaults.sid;
            Int64 Franchisee = objGlobalDefaults.ParentId;
            string Currency = objGlobalDefaults.Currency;

            DataSet dtResult;
            string sAgencyName = "";


            DataTable dtAgentDetails, dtVisa;
            string sSubject = "";
            StringBuilder sb = new StringBuilder();
            DBHelper.DBReturnCode retCode;
            retCode = GetAgentDetails(UserId, VCode, out dtResult);
            string Email = "";
            if (objGlobalDefaults.UserType == "Agent")
            {
                sAgencyName = objGlobalDefaults.AgencyName;
                if (objGlobalDefaults.Currency == "INR")
                {
                    Currency = "<img src=\"" + URL + "/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (objGlobalDefaults.Currency == "AED")
                {
                    Currency = "AED ";
                }
                else if (objGlobalDefaults.Currency == "USD")
                {
                    Currency = "$ ";
                }
            }
            else
            {
                if (dtResult.Tables[0].Rows[0]["CurrencyCode"].ToString() == "INR")
                {
                    Currency = "<img src=\"" + URL + "/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (objGlobalDefaults.Currency == "AED")
                {
                    Currency = "AED ";
                }
                else if (objGlobalDefaults.Currency == "USD")
                {
                    Currency = "$ ";
                }
            }
            string Name, VisaType, No, VisaFee, UrgentCharges, OtherCharges, TotalAmmount;
            if (objGlobalDefaults.UserType == "Agent")
            {
                Email = objGlobalDefaults.uid;
            }
            else
            {
                if (dtResult.Tables[0].Rows.Count != 0)
                {
                    sAgencyName = dtResult.Tables[0].Rows[0]["AgencyName"].ToString();
                    Email = dtResult.Tables[0].Rows[0]["uid"].ToString();
                }

            }
            if (Franchisee != 127)
            {
                DataSet dtFranchisee = new DataSet();
                DBHelper.DBReturnCode retCode1 = GetAgentDetails(UserId, VCode, out dtFranchisee);
                if (retCode1 == DBHelper.DBReturnCode.SUCCESS)
                {
                    if (Email != "" && dtFranchisee.Tables[2].Rows.Count != 0)
                        Email = Email + ";" + Convert.ToString(dtFranchisee.Tables[2].Rows[0]["uid"]);
                    else if (dtFranchisee.Tables[2].Rows.Count != 0)
                        Email = Convert.ToString(dtFranchisee.Tables[0].Rows[0]["uid"]);
                }
            }
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                dtAgentDetails = dtResult.Tables[0];
                dtVisa = dtResult.Tables[1];
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                //sb.Append("<div style=\"height:50px;width:auto\">");
                ////sb.Append("<br />");
                ////sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                //sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying Visa for your passengers, your request have been proceeded and we’ll update you once verified..<o:p></o:p></span></p></span><br />");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Airlines:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><span style='mso-tab-count:3'></span>" + ArrivalAirLine + "<o:p></o:p></span></p></span>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">PNR/Ticket No:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><span style='mso-tab-count:2'></span>" + ArrivingPnr + "<o:p></o:p></span></p></span>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Departing From:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><span style='mso-tab-count:2'></span>" + DepartingFrom + "<o:p></o:p></span></p></span>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Arriving To:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><span style='mso-tab-count:2'></span>" + Arrivalfrom + "<o:p></o:p></span></p></span>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:115%;background:white\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Total No of Pax:</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><span style='mso-tab-count:2'></span>" + PaxNo + "<o:p></o:p></span></p></span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:10%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Application No.<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:50%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Visa Type<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                Name = dtVisa.Rows[0]["FirstName"].ToString() + " " + dtVisa.Rows[0]["LastName"].ToString();
                sSubject = "Thanks for Visa request for " + Name + " * " + 1;
                VisaType = dtVisa.Rows[0]["IeService"].ToString();
                No = dtVisa.Rows[0]["PassportNo"].ToString();
                VisaFee = dtVisa.Rows[0]["VisaFee"].ToString();
                if (dtVisa.Rows[0]["UrgentFee"].ToString() == "")
                {
                    UrgentCharges = "0.00";
                }
                else
                {
                    UrgentCharges = dtVisa.Rows[0]["UrgentFee"].ToString();
                }
                if (dtVisa.Rows[0]["OtherFee"].ToString() == "")
                {
                    OtherCharges = "0.00";
                }
                else
                {
                    OtherCharges = dtVisa.Rows[0]["OtherFee"].ToString();
                }


                //TotalAmmount = dtVisa.Rows[0]["TotalAmount"].ToString();
                TotalAmmount = Convert.ToString(Convert.ToDecimal(VisaFee) + Convert.ToDecimal(OtherCharges) + Convert.ToDecimal(UrgentCharges));
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + (1) + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + VCode + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + Name + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + No + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + VisaType + "<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Visa Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("</tbody></table></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Visa Department</b></span><br />");
                //sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;width: 80px;margin-top: 2px;\" /></b></span><br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Head Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\" <o:p></o:p></span><span style=\"margin-left:2%;font-weight:400\">2nd Floor, Vishnu Complex, C.A Road Opp. Rahate Hospital, Juni Mangalwari Nagpur-440008, M.S. INDIA</span><br/><b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+91-712-6660666 (100 Lines) Fax:. +91-712-2766520  eMail: info@clickurtrip.com</span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Dubai Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Al Maktoum Street, Beside Dnata Near Clock Tower, Port Saeed, Deira, P.O. Box 43430, Dubai-UAE</b></span><br />");
                //sb.Append("<b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+971-4-2977792  Fax:. +971-4-2977793    eMail: dubai@clickurtrip.com</span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">ClickurTrip.com</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");

                //}
                try
                {
                    //string sTo = Email;
                    //int effected = 0;
                    //SqlParameter[] sqlParams = new SqlParameter[3];
                    //sqlParams[0] = new SqlParameter("@sTo", Email);
                    //sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                    //sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                    //retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out effected, sqlParams);

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["OTBMail"]));

                    List<string> attachmentList = new List<string>();
                    Dictionary<string, string> Email1List = new Dictionary<string, string>();

                    foreach (string mailItem in Email.Split(','))
                    {
                        if (mailItem != "")
                        {
                            Email1List.Add(mailItem, mailItem);
                        }
                    }


                    //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                    bool response = MailManager.SendMail(accessKey, Email1List, sSubject, sb.ToString(), from, attachmentList);
                    if (response.Equals(true))
                    {
                        retCode = DBHelper.DBReturnCode.SUCCESS;
                    }

                    //return retCode;
                    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    //mailMsg.To.Add(sTo);
                    //mailMsg.Subject = sSubject;
                    //mailMsg.IsBodyHtml = true;
                    //mailMsg.Body = sb.ToString();
                    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    //mailObj.EnableSsl = true;
                    //mailObj.Send(mailMsg);
                    //mailMsg.Dispose();
                    //return true;
                    return retCode;

                }
                catch
                {
                    return retCode = DBHelper.DBReturnCode.EXCEPTION;
                }

            }
            else
            {
                return retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        #endregion

        #region AgentDetailsBYVisa
        public static DBHelper.DBReturnCode GetAgentDetails(Int64 UserId, string VisaRefNo, out DataSet tbl_dsResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgentId", UserId);
            sqlParams[1] = new SqlParameter("@RefNo", VisaRefNo);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentVisaMailDetails", out tbl_dsResult, sqlParams);
            return retCode;
        }
        #endregion

        #endregion

        #region Delete Attachments
        public static DBHelper.DBReturnCode DeleteAttachments(string VisaNo, string FileNo)
        {
            DBHelper.DBReturnCode retCode;
            try
            {
                string VisaPath = System.Web.HttpContext.Current.Server.MapPath("~/VisaImages/");
                string VisaFName = VisaNo + "_" + FileNo + ".jpg";
                DirectoryInfo dir = new DirectoryInfo(VisaPath);
                FileInfo[] files = null;

                files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    if (file.Name == VisaFName)
                    {
                        file.Delete();

                        break;
                    }
                    else
                    {
                        continue;
                    }

                }
                retCode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
            }
            return retCode;

        }
        #endregion


        #region Visa list By Status
        public static DBHelper.DBReturnCode VisalistByAppList(out DataSet dsResult)
        {
            DBHelper.DBReturnCode retCode;
            retCode = DBHelper.GetDataSet("Proc_tblVisaStatusLoadByApp_Id", out dsResult);
            return retCode;

        }
        #endregion
    }
}
