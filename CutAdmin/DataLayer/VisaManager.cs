﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class VisaManager
    {
        #region Visa Call Center


        #region Visa Add Updte
        public static DBHelper.DBReturnCode AddVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 AgentId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, string AppDate, Int64 UserId, string Supplier, string User, string Password, bool offlineStatus, string AppNo, string VisaNo)
        {
            string Sponsor = Supplier;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            if (sTelephone == "")
            {
                sTelephone = "0";
            }
            if (offlineStatus == false)
            {
                SqlParameter[] sqlParams = new SqlParameter[50];
                int rowsAffected = 0;
                Int64 sid = 0;
                sqlParams[0] = new SqlParameter("@sid", sid);
                sqlParams[1] = new SqlParameter("@Processing", sProcessing);
                sqlParams[2] = new SqlParameter("@Members", "1");
                sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
                sqlParams[4] = new SqlParameter("@ServiceType", sType);
                sqlParams[5] = new SqlParameter("@FirstName", sFirst);
                sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
                sqlParams[7] = new SqlParameter("@LastName", sLast);
                sqlParams[8] = new SqlParameter("@FatherName", sFather);
                sqlParams[9] = new SqlParameter("@MotherName", sMother);
                sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
                sqlParams[11] = new SqlParameter("@Language", sLanguage);
                sqlParams[12] = new SqlParameter("@Gender", sGender);
                sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
                sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
                sqlParams[15] = new SqlParameter("@Birth", sBirth);
                sqlParams[16] = new SqlParameter("@Department", dteDeparting);
                sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
                sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
                sqlParams[19] = new SqlParameter("@Religion", sReligion);
                sqlParams[20] = new SqlParameter("@Profession", sProfession);
                sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
                sqlParams[22] = new SqlParameter("@PassportType", sPassport);
                sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
                sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
                sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
                sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
                sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
                sqlParams[28] = new SqlParameter("@City", sCity);
                sqlParams[29] = new SqlParameter("@Country", sCountry);
                sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
                sqlParams[31] = new SqlParameter("@Vcode", sVisa);
                sqlParams[32] = new SqlParameter("@UserId", AgentId);
                sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
                sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
                sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
                sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
                sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
                sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
                sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
                sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
                sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
                sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
                sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
                sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
                sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
                sqlParams[46] = new SqlParameter("@AppliedDate", AppDate);
                sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
                sqlParams[48] = new SqlParameter("@CallCenterId", UserId);
                sqlParams[49] = new SqlParameter("@CallCenterDate", AppDate);
                retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaADDByCallCenter", out rowsAffected, sqlParams);
            }
            else
            {
                SqlParameter[] sqlParams = new SqlParameter[54];
                int rowsAffected = 0;
                Int64 sid = 0;
                sqlParams[0] = new SqlParameter("@sid", sid);
                sqlParams[1] = new SqlParameter("@Processing", sProcessing);
                sqlParams[2] = new SqlParameter("@Members", "1");
                sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
                sqlParams[4] = new SqlParameter("@ServiceType", sType);
                sqlParams[5] = new SqlParameter("@FirstName", sFirst);
                sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
                sqlParams[7] = new SqlParameter("@LastName", sLast);
                sqlParams[8] = new SqlParameter("@FatherName", sFather);
                sqlParams[9] = new SqlParameter("@MotherName", sMother);
                sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
                sqlParams[11] = new SqlParameter("@Language", sLanguage);
                sqlParams[12] = new SqlParameter("@Gender", sGender);
                sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
                sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
                sqlParams[15] = new SqlParameter("@Birth", sBirth);
                sqlParams[16] = new SqlParameter("@Department", dteDeparting);
                sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
                sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
                sqlParams[19] = new SqlParameter("@Religion", sReligion);
                sqlParams[20] = new SqlParameter("@Profession", sProfession);
                sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
                sqlParams[22] = new SqlParameter("@PassportType", sPassport);
                sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
                sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
                sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
                sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
                sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
                sqlParams[28] = new SqlParameter("@City", sCity);
                sqlParams[29] = new SqlParameter("@Country", sCountry);
                sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
                sqlParams[31] = new SqlParameter("@Vcode", sVisa);
                sqlParams[32] = new SqlParameter("@UserId", AgentId);
                sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
                sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
                sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
                sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
                sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
                sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
                sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
                sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
                sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
                sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
                sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
                sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
                sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
                sqlParams[46] = new SqlParameter("@AppliedDate", AppDate);
                sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
                sqlParams[48] = new SqlParameter("@CallCenterId", UserId);
                sqlParams[49] = new SqlParameter("@CallCenterDate", AppDate);
                sqlParams[50] = new SqlParameter("@EdnrdUserName", User);
                sqlParams[51] = new SqlParameter("@EdnrdPassword", Password);
                sqlParams[52] = new SqlParameter("@TReference", AppNo);
                sqlParams[53] = new SqlParameter("@VisaNo", VisaNo);
                retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaADDByCallCenterOffline", out rowsAffected, sqlParams);
            }
            //string msg="";

            //  retCode = VisaDetailsManager.ConformBooking(sVisa, out msg);

            return retCode;

        }

        public static DBHelper.DBReturnCode AddIncompleteVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 UserId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            string Sponsor = "ClickUrTrip";
            DBHelper.DBReturnCode retCode;
            if (sTelephone == "")
            {
                sTelephone = "0";
            }
            SqlParameter[] sqlParams = new SqlParameter[48];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", UserId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaInsertIncomplete", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode UpdateVisaDetails(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, Int64 AgentId, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            string Sponsor = "ClickUrTrip";
            if (sTelephone == "")
            {
                sTelephone = "0";
            }
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[48];
            int rowsAffected = 0;
            Int64 sid = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            sqlParams[1] = new SqlParameter("@Processing", sProcessing);
            sqlParams[2] = new SqlParameter("@Members", "1");
            sqlParams[3] = new SqlParameter("@Sponsor", Sponsor);
            sqlParams[4] = new SqlParameter("@ServiceType", sType);
            sqlParams[5] = new SqlParameter("@FirstName", sFirst);
            sqlParams[6] = new SqlParameter("@MiddleName", sMiddle);
            sqlParams[7] = new SqlParameter("@LastName", sLast);
            sqlParams[8] = new SqlParameter("@FatherName", sFather);
            sqlParams[9] = new SqlParameter("@MotherName", sMother);
            sqlParams[10] = new SqlParameter("@HusbandMame", sHusband);
            sqlParams[11] = new SqlParameter("@Language", sLanguage);
            sqlParams[12] = new SqlParameter("@Gender", sGender);
            sqlParams[13] = new SqlParameter("@MaritalStatus", sMarital);
            sqlParams[14] = new SqlParameter("@PresentNationality", sNationality);
            sqlParams[15] = new SqlParameter("@Birth", sBirth);
            sqlParams[16] = new SqlParameter("@Department", dteDeparting);
            sqlParams[17] = new SqlParameter("@BirthPlace", sBirthPlace);
            sqlParams[18] = new SqlParameter("@BirthCountry", sBirthCountry);
            sqlParams[19] = new SqlParameter("@Religion", sReligion);
            sqlParams[20] = new SqlParameter("@Profession", sProfession);
            sqlParams[21] = new SqlParameter("@PassportNo", sPassport);
            sqlParams[22] = new SqlParameter("@PassportType", sPassport);
            sqlParams[23] = new SqlParameter("@IssuingGovernment", sIssuing);
            sqlParams[24] = new SqlParameter("@IssuingDate", sIssueDate);
            sqlParams[25] = new SqlParameter("@ExpDate", sExpirationDate);
            sqlParams[26] = new SqlParameter("@AddressLine1", sAddress1);
            sqlParams[27] = new SqlParameter("@AddressLine2", sAddress2);
            sqlParams[28] = new SqlParameter("@City", sCity);
            sqlParams[29] = new SqlParameter("@Country", sCountry);
            sqlParams[30] = new SqlParameter("@Telephone", sTelephone);
            sqlParams[31] = new SqlParameter("@Vcode", sVisa);
            sqlParams[32] = new SqlParameter("@UserId", AgentId);
            sqlParams[33] = new SqlParameter("@ArrivalAirLine", sArriAirLine);
            sqlParams[34] = new SqlParameter("@ArrivalflightNo", sArriFlight);
            sqlParams[35] = new SqlParameter("@Arrivalfrom", sArrivalFrom);
            sqlParams[36] = new SqlParameter("@ArrivalDate", dteArrival);
            sqlParams[37] = new SqlParameter("@DeprtureAirLine", sDeptAirLine);
            sqlParams[38] = new SqlParameter("@DeprtureflightNo", sDeptFlight);
            sqlParams[39] = new SqlParameter("@Deprturefrom", sDeptFrom);
            sqlParams[40] = new SqlParameter("@DepartingDate", dteDeparting);
            sqlParams[41] = new SqlParameter("@VisaFee", VisaFee);
            sqlParams[42] = new SqlParameter("@OtherFee", OtherFee);
            sqlParams[43] = new SqlParameter("@UrgentFee", UrgentFee);
            sqlParams[44] = new SqlParameter("@ServiceTax", ServiceTax);
            sqlParams[45] = new SqlParameter("@TotalAmount", TotalAmount);
            sqlParams[46] = new SqlParameter("@AppliedDate", Convert.ToString(DateTime.Now));
            sqlParams[47] = new SqlParameter("@VisaCountry", sVisaCountry);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByAgentSide", out rowsAffected, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode DocumentStatus(string VCode)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            int rowsAffected = 0;
            string Dstatus = "Documents Submitted";
            sqlParams[0] = new SqlParameter("@DocumentsStatus", Dstatus);
            sqlParams[1] = new SqlParameter("@VCode", VCode);
            retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaUpdateByStatus", out rowsAffected, sqlParams);
            return retCode;

        }
        public static DBHelper.DBReturnCode GetAgentMarkups(Int64 UserId, string Process, out DataSet dsResult)
        {
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@AgentId", UserId);
            sqlparam[1] = new SqlParameter("@Supplier", Process);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentVisaMarkpDetailsByAgentId", out dsResult, sqlparam);
            return retCode;
        }
        #endregion

        #region AgenList
        #region Agent Autocomplete
        public static DBHelper.DBReturnCode GetAgentRoleList(string AgencyName, out DataTable dtResult)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefaults.sid;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            sqlParams[1] = new SqlParameter("@ParentId", UserId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyNameMaster", out dtResult, sqlParams);
            return retCode;
        }
        #endregion

        #region GetPassportDetails
        public static DBHelper.DBReturnCode GetPassportNo(string PassportNo, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@PassportNo", PassportNo);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaDetailsByPassportNo", out dtResult, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode GetPassportDetails(Int64 sid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_Get_tbl_VisaDetailsByPassportNo", out dtResult, sqlParams);
            return retCode;
        }

        #endregion

        #region Agent Datails
        public static DBHelper.DBReturnCode GetAgentDetails(Int64 uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyDetals", out dtResult, sqlParams);
            return retCode;
        }
        #endregion
        #endregion

        #region GetVisaCharges
        public static DBHelper.DBReturnCode GetVisaCharges(string Visatype, string Nationality, out DataTable tbl_Visa)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@VisaType", Visatype);
            sqlParams[1] = new SqlParameter("@Nationality", Nationality);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaChargesDetailsLoadByNationality", out tbl_Visa, sqlParams);
            return retCode;
        }
        #endregion

        #region Get AgenDetails
        public static DBHelper.DBReturnCode GetVisaByCode(out DataTable tbl_Visa, string VisaCode)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return retCode;
        }
        #endregion

        #region Visa Transaction
        public static DBHelper.DBReturnCode VisaBookingTxnAdd(string Vcode, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string PassengerName)
        {
            int rowsaffected;

            Int64 uid = Convert.ToInt64(BookedBy);
            string Perticulars = "Booking (Visa)" + PassengerName;
            SqlParameter[] sqlParams = new SqlParameter[22];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", Vcode);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
            sqlParams[21] = new SqlParameter("@Particulars", Perticulars);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddByVisa", out rowsaffected, sqlParams);
            return retCode;
        }

        #endregion

        #region Conformation Mail To Agent
        public static DBHelper.DBReturnCode SendEmailAdmin(Int64 UserId, string RefNo)
        {
            DBHelper.DBReturnCode retCode;
            DataTable dtResult = new DataTable();
            retCode = GetAgentDetails(UserId, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                string sAgencyName = "", AgentUniqueCode = "", sEmail = "";
                sAgencyName = dtResult.Rows[0]["AgencyName"].ToString();
                sEmail = dtResult.Rows[0]["uid"].ToString();
                AgentUniqueCode = dtResult.Rows[0]["AgentUniqueCode"].ToString();
                string sSubject = sAgencyName + " " + "Requested For new Visa Application";
                StringBuilder sb = new StringBuilder();
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
                sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div style=\"height:50px;width:auto\">");
                sb.Append("<br />");
                sb.Append("<img src=\"" + ConfigurationManager.AppSettings["Domain"] + "/images/logosmal.png\" alt=\"\" style=\"padding-left:10px;\" />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\">Hello Admin,</span><br /><br />");
                sb.Append("<span style=\"margin-left:12%;font-weight:400\">" + sSubject + "</b></span><br />");
                sb.Append("<span style=\"margin-left:12%;font-weight:400\">Application No : <b>" + RefNo + "</b></span><br />");
                sb.Append("<span style=\"margin-left:12%;font-weight:400\">Agency Name : <b>" + sAgencyName + "</b></span><br />");
                sb.Append("<span style=\"margin-left:12%;font-weight:400\">Agent Unique Code : <b>" + AgentUniqueCode + "</b></span><br />");
                sb.Append("<span style=\"margin-left:12%;font-weight:400\">Email-ID : <b>" + sEmail + "</b></span><br />");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Mobile Number : <b>" + nMobile + "</b></span><br />");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:12%\">");
                sb.Append("<b>Thank You,</b><br />");
                sb.Append("</span>");
                sb.Append("<span style=\"margin-left:12%\">");
                sb.Append("Administrator");
                sb.Append("</span>");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> © 2018 - 2019  " + ConfigurationManager.AppSettings["AdminName"] + "</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");



                try
                {
                    string sTo = "dubaivisa@clickurtrip.com";
                    int effected = 0;
                    SqlParameter[] sqlParams = new SqlParameter[3];
                    sqlParams[0] = new SqlParameter("@sTo", sTo);
                    sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                    sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                    retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out effected, sqlParams);
                    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    //mailMsg.To.Add(sTo);
                    //mailMsg.Subject = sSubject;
                    //mailMsg.IsBodyHtml = true;
                    //mailMsg.Body = sb.ToString();
                    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    //mailObj.EnableSsl = true;
                    //mailObj.Send(mailMsg);
                    SendEmailToAgent(UserId, RefNo);
                    return retCode = DBHelper.DBReturnCode.SUCCESS;

                }
                catch
                {
                    return retCode = DBHelper.DBReturnCode.EXCEPTION;
                }
            }
            else
            {
                return retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        public static DBHelper.DBReturnCode SendEmailToAgent(Int64 UserId, string VCode)
        {
            DataSet dtResult;
            string sAgencyName = "";
            string Name, VisaType, No, VisaFee, UrgentCharges, OtherCharges, TotalAmmount;
            string Email = "";
            DataTable dtAgentDetails, dtVisa;
            string sSubject = "";
            StringBuilder sb = new StringBuilder();
            DBHelper.DBReturnCode retCode;
            retCode = GetAgentDetails(UserId, VCode, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                dtAgentDetails = dtResult.Tables[0];
                string Currency = "";
                if (dtAgentDetails.Rows[0]["CurrencyCode"].ToString() == "INR")
                {
                    Currency = "<img src=\"http://clickurtrip.com/images/rupee_icon.png\" style=\" MARGIN-TOP: 0%;\">";
                }
                else if (dtAgentDetails.Rows[0]["CurrencyCode"].ToString() == "AED")
                {
                    Currency = "AED ";
                }
                else if (dtAgentDetails.Rows[0]["CurrencyCode"].ToString() == "USD")
                {
                    Currency = "$ ";
                }
                sAgencyName = dtAgentDetails.Rows[0]["AgencyName"].ToString();
                Email = dtAgentDetails.Rows[0]["uid"].ToString();
                dtVisa = dtResult.Tables[1];
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                sb.Append("<head>");
                sb.Append("<meta charset=\"utf-8\" />");
                sb.Append("</head>");
                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: 'Segoe UI', Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black;background:white\">Dear " + sAgencyName + ",</span><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:'Times New Roman''><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:black\">Greetings from<b>&nbsp;Click</b></span><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#ED7D31\">Ur</span></b><b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#4472C4\">Trip</span></b><span style=\"font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-left:4%;margin-bottom:.0001pt;line-height:normal;background:white\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial;color:#222222\">Thank you for applying Visa for your passengers, your request have been proceeded and we’ll update you once verified..<o:p></o:p></span></p></span><br />");
                sb.Append("<table class=\"MsoTableGrid border=0 cellspacing=0 cellpadding=0\" style=\"border-collapse:collapse;margin-left: 4%;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none\">");
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:10%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Sr. No<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Application No.<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passenger Names<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:20%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style='font-family:'Verdana','sans-serif';mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial'>Passport No.<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:50%;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\">Visa Type<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                Name = dtVisa.Rows[0]["FirstName"].ToString() + " " + dtVisa.Rows[0]["LastName"].ToString();
                sSubject = "Thanks for Visa request for " + Name + " * " + 1;
                VisaType = dtVisa.Rows[0]["IeService"].ToString();
                No = dtVisa.Rows[0]["PassportNo"].ToString();
                VisaFee = dtVisa.Rows[0]["VisaFee"].ToString();
                UrgentCharges = dtVisa.Rows[0]["UrgentFee"].ToString();
                OtherCharges = dtVisa.Rows[0]["OtherFee"].ToString();
                //TotalAmmount = dtVisa.Rows[0]["TotalAmount"].ToString();
                TotalAmmount = Convert.ToString(Convert.ToDecimal(VisaFee) + Convert.ToDecimal(OtherCharges) + Convert.ToDecimal(UrgentCharges));
                sb.Append("<tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.1pt\">");
                sb.Append("<td width=\"82\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + (1) + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + VCode + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + Name + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"338\" style=\"width:202.5pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b style=\"mso-bidi-font-weight:normal\"><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + No + "<o:p></o:p></span></b></p></td>");
                sb.Append("<td width=\"346\" style=\"width:49.25pt;padding:0in 5.4pt 0in 5.4pt;height:21.1pt\"><p class=\"MsoNormal\"><b><span style=\"margin-left:4%;font-family:'Verdana','sans-serif';color:#736262\">" + VisaType + "<o:p></o:p></span></b></p></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:normal\"><span style=\"margin-left:4%;font-size:15.5pt;mso-bidi-font-size:11.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#0099CC\">Billing Details</span><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;\"><o:p></o:p></span></p></span>");
                sb.Append("<span style=\"margin-left:2%;font-weight:400\"><table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left:4%;border-collapse:collapse;border:none;mso-border-alt:solid black .5pt; mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt\">");
                sb.Append("<tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes\">");
                sb.Append(" <td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Visa Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:  text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;background:white\">Urgent Fee</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor: text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333;background:white\">Other Charges</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p> </td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color:#ff9900;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1; border-left:none; mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1; mso-border-alt:solid black .5pt; mso-border-themecolor: text1; padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;color:#333333; /* background:white */\">No Of <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\"><o:p></o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1; border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#333333;/* background:white */\">Total Per <span class=\"SpellE\">Pax</span></span></b><span style=\"font-size: 8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border:solid black 1.0pt; mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;  mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><b><span style=\"font-size: 10pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color: white; /* background:white */\">Total Amount</span></b><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p></o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("<tr style=\"mso-yfti-irow:1;mso-yfti-lastrow:yes\">");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border:solid black 1.0pt;mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + VisaFee + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1; border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt; mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:#333333\">" + UrgentCharges + "<o:p></o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + OtherCharges + "</o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #ff9900; width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;  mso-border-right-themecolor:text1;  mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + PaxNo + "</o:p></span></p></td>");
                //sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: yellow;width:79.8pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\"><span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;color:#333333\"><o:p>" + TotalPerPass + "</o:p></span></p></td>");
                sb.Append("<td width=\"133\" valign=\"top\" style=\"background-color: #0099cc;width:79.8pt;border-top:none;border-left:none; border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;padding:0in 5.4pt 0in 5.4pt\"><p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:center;line-height:11.55pt\">" + Currency + "<span style=\"font-size:8.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;color:WHITESMOKE\"><o:p>" + TotalAmmount + "</o:p></span></p></td>");
                sb.Append("</tr>");
                sb.Append("</tbody></table></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Hope the above is correct & clear ,For any further clarification & query kindly feel free to contact us</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\"></b></span><br />");
                sb.Append("<br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Best Regards,</b></span><br />");
                sb.Append("<span style=\"margin-left:4%;font-weight:400\">Visa Department</b></span><br />");
                //sb.Append("<span style=\"margin-left:4%;font-weight:400\"><img src=\"http://www.clickurtrip.com/updates/update1/img/OTBMailSignatures.jpg\" alt=\"\" style=\"width: 80%;padding-left:10px;width: 80px;margin-top: 2px;\" /></b></span><br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Head Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif'; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\" <o:p></o:p></span><span style=\"margin-left:2%;font-weight:400\">2nd Floor, Vishnu Complex, C.A Road Opp. Rahate Hospital, Juni Mangalwari Nagpur-440008, M.S. INDIA</span><br/><b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+91-712-6660666 (100 Lines) Fax:. +91-712-2766520  eMail: info@clickurtrip.com</span><br />");
                //sb.Append("<br />");
                //sb.Append("<br />");
                //sb.Append("<span style\"font-family:'Verdana','sans-serif';margin-left:2%; mso-fareast-font-family:'Times New Roman';mso-bidi-font-family:Arial\"><o:p></o:p><b><span style=\" margin-left: 2%;\"><u></u><u></u>Dubai Office:</span><b></b></b></span><br/>");
                //sb.Append("<span style=\"margin-left:2%;font-weight:400\">Al Maktoum Street, Beside Dnata Near Clock Tower, Port Saeed, Deira, P.O. Box 43430, Dubai-UAE</b></span><br />");
                //sb.Append("<b style=\"margin-left:2%;font-weight:400\">Ph:.</b><span style=\"margin-left:2%;font-weight:400\"></b>+971-4-2977792  Fax:. +971-4-2977793    eMail: dubai@clickurtrip.com</span><br />");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("<table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                sb.Append("<tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                sb.Append("<td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\"> © 2018 - 2019  " + ConfigurationManager.AppSettings["AdminName"] + "</div></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"height: 5px\">");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");

                //}
                try
                {
                    string sTo = Email;
                    int effected = 0;
                    SqlParameter[] sqlParams = new SqlParameter[3];
                    sqlParams[0] = new SqlParameter("@sTo", Email);
                    sqlParams[1] = new SqlParameter("@sSubject", sSubject);
                    sqlParams[2] = new SqlParameter("@VarBody", sb.ToString());
                    retCode = DBHelper.ExecuteNonQuery("Proc_Mail", out effected, sqlParams);
                    //System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    //mailMsg.From = new MailAddress("support@clickUrTrip.com");
                    //mailMsg.To.Add(sTo);
                    //mailMsg.Subject = sSubject;
                    //mailMsg.IsBodyHtml = true;
                    //mailMsg.Body = sb.ToString();
                    //SmtpClient mailObj = new SmtpClient("smtp.gmail.com", 587);
                    //mailObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["EmailID"], System.Configuration.ConfigurationManager.AppSettings["PassWord"]);
                    //mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    //mailObj.EnableSsl = true;
                    //mailObj.Send(mailMsg);
                    return retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                catch
                {
                    return retCode = DBHelper.DBReturnCode.EXCEPTION;
                }

            }
            else
            {
                return retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

        }

        #endregion

        #region AgentDetailsBYVisa
        public static DBHelper.DBReturnCode GetAgentDetails(Int64 UserId, string VisaRefNo, out DataSet tbl_dsResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@AgentId", UserId);
            sqlParams[1] = new SqlParameter("@RefNo", VisaRefNo);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_GetAgentVisaMailDetails", out tbl_dsResult, sqlParams);
            return retCode;
        }
        #endregion
        #endregion


        #region Ednrd Users
        #region UsersList
        public static DBHelper.DBReturnCode GetUsersList(out DataTable dtResult)
        {

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_EdnrdLoginLoadAll", out dtResult);
            return retCode;
        }
        #endregion
        #region UsersList
        public static DBHelper.DBReturnCode GetUsersListBySid(Int64 sid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@sid", sid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_EdnrdLoginLoadBySid", out dtResult, sqlParams);
            return retCode;
        }
        #endregion
        #region Api AddUpdate
        public static DBHelper.DBReturnCode AddUser(string SupplierName, string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[18];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Username", Username);
            sqlParams[1] = new SqlParameter("@Password", Password);
            sqlParams[2] = new SqlParameter("@96Hr", Hours96);
            sqlParams[3] = new SqlParameter("@14DaysService", Sevice14Day);
            sqlParams[4] = new SqlParameter("@30DaysSingle", Single30Days);
            sqlParams[5] = new SqlParameter("@30DaysMultipule", Multipule30Days);
            sqlParams[6] = new SqlParameter("@90DaysSingle", Single90Days);
            sqlParams[7] = new SqlParameter("@90DaysMultipule", Multipule90Days);
            sqlParams[8] = new SqlParameter("@90DaysCovertable", Covertable90Days);
            sqlParams[9] = new SqlParameter("@Active", Active);
            sqlParams[10] = new SqlParameter("@SupplierName", SupplierName);
            sqlParams[11] = new SqlParameter("@Hurs96Price", Hurs96Price);
            sqlParams[12] = new SqlParameter("@Service14DaysPrice", Service14DaysPrice);
            sqlParams[13] = new SqlParameter("@Single30DaysPrice", Single30DaysPrice);
            sqlParams[14] = new SqlParameter("@Multipule30DaysPrice", Multipule30DaysPrice);
            sqlParams[15] = new SqlParameter("@Single90DaysPrice", Single90DaysPrice);
            sqlParams[16] = new SqlParameter("@Multipule90DaysPrice", Multipule90DaysPrice);
            sqlParams[17] = new SqlParameter("@Covertable90DaysPrice", Covertable90DaysPrice);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_tbl_EdnrdLoginDetailsAddByUser", out rowsAffected, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode AddPostingLogins(Int64 Sid, string SupplierName, Int64 SupplierId, string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice, bool Qouta, Int64 Qouta_Hurs96, Int64 Qouta_14Days, Int64 Qouta_Single30Days, Int64 Qouta_Multipule30Days, Int64 Qouta_Single90Days, Int64 Qouta_Multipule90Days, Int64 Qouta_Covertable90Days)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[28];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Username", Username);
            sqlParams[1] = new SqlParameter("@Password", Password);
            sqlParams[2] = new SqlParameter("@96Hr", Hours96);
            sqlParams[3] = new SqlParameter("@14DaysService", Sevice14Day);
            sqlParams[4] = new SqlParameter("@30DaysSingle", Single30Days);
            sqlParams[5] = new SqlParameter("@30DaysMultipule", Multipule30Days);
            sqlParams[6] = new SqlParameter("@90DaysSingle", Single90Days);
            sqlParams[7] = new SqlParameter("@90DaysMultipule", Multipule90Days);
            sqlParams[8] = new SqlParameter("@90DaysCovertable", Covertable90Days);
            sqlParams[9] = new SqlParameter("@Active", Active);
            sqlParams[10] = new SqlParameter("@SupplierName", SupplierName);
            sqlParams[11] = new SqlParameter("@Hurs96Price", Hurs96Price);
            sqlParams[12] = new SqlParameter("@Service14DaysPrice", Service14DaysPrice);
            sqlParams[13] = new SqlParameter("@Single30DaysPrice", Single30DaysPrice);
            sqlParams[14] = new SqlParameter("@Multipule30DaysPrice", Multipule30DaysPrice);
            sqlParams[15] = new SqlParameter("@Single90DaysPrice", Single90DaysPrice);
            sqlParams[16] = new SqlParameter("@Multipule90DaysPrice", Multipule90DaysPrice);
            sqlParams[17] = new SqlParameter("@Covertable90DaysPrice", Covertable90DaysPrice);
            sqlParams[18] = new SqlParameter("@Sid", Sid);
            sqlParams[19] = new SqlParameter("@Qouta", @Qouta);
            sqlParams[20] = new SqlParameter("@Qouta_Hurs96", Qouta_Hurs96);
            sqlParams[21] = new SqlParameter("@Qouta_14Days", @Qouta_14Days);
            sqlParams[22] = new SqlParameter("@Qouta_Single30Days", @Qouta_Single30Days);
            sqlParams[23] = new SqlParameter("@Qouta_Multipule30Days", @Qouta_Multipule30Days);
            sqlParams[24] = new SqlParameter("@Qouta_Single90Days", @Qouta_Single90Days);
            sqlParams[25] = new SqlParameter("@Qouta_Multipule90Days", @Qouta_Multipule90Days);
            sqlParams[26] = new SqlParameter("@Qouta_Covertable90Days", @Qouta_Covertable90Days);
            sqlParams[27] = new SqlParameter("@SupplierId", @SupplierId);
            if (Sid == 0)
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_tbl_EdnrdLoginDetailsAddByPosting", out rowsAffected, sqlParams);
            else
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_tbl_EdnrdLoginDetailsUpdateByPosting", out rowsAffected, sqlParams);
            return retCode;
        }
        public static DBHelper.DBReturnCode UpdateUser(string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[17];
            int rowsAffected = 0;
            sqlParams[0] = new SqlParameter("@Username", Username);
            sqlParams[1] = new SqlParameter("@Password", Password);
            sqlParams[2] = new SqlParameter("@96Hr", Sevice14Day);
            sqlParams[3] = new SqlParameter("@14DaysService", Hours96);
            sqlParams[4] = new SqlParameter("@30DaysSingle", Multipule30Days);
            sqlParams[5] = new SqlParameter("@30DaysMultipule", Multipule30Days);
            sqlParams[6] = new SqlParameter("@90DaysSingle", Single90Days);
            sqlParams[7] = new SqlParameter("@90DaysMultipule", Multipule90Days);
            sqlParams[8] = new SqlParameter("@90DaysCovertable", Covertable90Days);
            sqlParams[9] = new SqlParameter("@Active", Active);
            sqlParams[10] = new SqlParameter("@Hurs96Price", Hurs96Price);
            sqlParams[11] = new SqlParameter("@Service14DaysPrice", Service14DaysPrice);
            sqlParams[12] = new SqlParameter("@Single30DaysPrice", Single30DaysPrice);
            sqlParams[13] = new SqlParameter("@Multipule30DaysPrice", Multipule30DaysPrice);
            sqlParams[14] = new SqlParameter("@Single90DaysPrice", Single90DaysPrice);
            sqlParams[15] = new SqlParameter("@Multipule90DaysPrice", Multipule90DaysPrice);
            sqlParams[16] = new SqlParameter("@Covertable90DaysPrice", Covertable90DaysPrice);
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_tbl_EdnrdLoginDetailsUpdateByUser", out rowsAffected, sqlParams);

            return retCode;
        }
        #endregion
        #region Get Active Supplier
        public static DBHelper.DBReturnCode GetActiveSupplier(out DataTable dtResult)
        {
            dtResult = null;
            return DBHelper.GetDataTable("Proc_tbl_APIDetailsLoadBySupplier", out dtResult);
        }
        #endregion
        #endregion

        #region Visa Application Rejection
        public static DBHelper.DBReturnCode ApplicationRejected(string Vcode, Int64 AgentId, decimal DepositAmmount)
        {
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            int rows;
            Int64 sid = 0;
            int bActiveFlag = 1;
            decimal CreditAmmount = 0, MaxCreditAmmount = 0, ReduceAmmount = 0;
            string Remark = "Credit  Against Visa Invoice " + Vcode;
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                SqlParameter[] sqlParams = new SqlParameter[2];
                int rowsAffected = 0;
                sqlParams[0] = new SqlParameter("@Status", "Rejected By Administrator");
                sqlParams[1] = new SqlParameter("@UserId", Vcode);
                retcode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByApplicationRejection", out rowsAffected, sqlParams);
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    Int64 sessionsid;
                    //string ExecutiveName = "";
                    //int MinLimit = 0;
                    string AgainInvoice = "";
                    string PerticulaAmount = "";
                    string otc;
                    if (MaxCreditAmmount > 0)
                    {
                        otc = "True";
                    }
                    else
                    {
                        otc = "false";
                    }
                    if (DepositAmmount != 0)
                    {
                        PerticulaAmount = Convert.ToString(DepositAmmount);
                    }
                    else if (ReduceAmmount != 0)
                    {
                        PerticulaAmount = Convert.ToString(ReduceAmmount);
                    }
                    string Particulars = "";
                    if (Vcode != "undefined")
                    {
                        AgainInvoice = " - " + Vcode;
                    }

                    GlobalDefault objGlobal;
                    objGlobal = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    sessionsid = objGlobal.sid;
                    string ContactPerson = objGlobal.ContactPerson;
                    Particulars = "Credit" + " - " + ContactPerson + " - " + PerticulaAmount + AgainInvoice;
                    SqlParameter[] sqlParams1 = new SqlParameter[12];
                    sqlParams1[0] = new SqlParameter("@sid", sid);
                    sqlParams1[1] = new SqlParameter("@uid", AgentId);
                    sqlParams1[2] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                    sqlParams1[3] = new SqlParameter("@ActiveFlag", bActiveFlag);
                    sqlParams1[4] = new SqlParameter("@DepositAmmount", DepositAmmount);
                    sqlParams1[5] = new SqlParameter("@CreditAmmount", CreditAmmount);
                    sqlParams1[6] = new SqlParameter("@MaxCreditAmmount", MaxCreditAmmount);
                    sqlParams1[7] = new SqlParameter("@ReduceAmmount", ReduceAmmount);
                    sqlParams1[8] = new SqlParameter("@OTCActive", otc);
                    sqlParams1[9] = new SqlParameter("@Remark", Remark);
                    sqlParams1[10] = new SqlParameter("@Particulars", Particulars);
                    sqlParams1[11] = new SqlParameter("@InvoiceNo", AgainInvoice);
                    retcode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitCreditDebit", out rows, sqlParams1);
                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                    {
                        objTransactionScope.Dispose();
                    }
                    else
                    {
                        objTransactionScope.Complete();
                    }
                }
                else
                {
                    objTransactionScope.Dispose();
                }
            }
            return retcode;
        }
        #endregion

        #region Visa Application Status
        public static DBHelper.DBReturnCode RejectByStatus(string Vcode, Int64 AgentId, decimal Amount, bool IsRefund)
        {
            DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.EXCEPTION;
            int rows;
            Int64 sid = 0;
            int bActiveFlag = 1;
            decimal CreditAmmount = 0, MaxCreditAmmount = 0, ReduceAmmount = 0;
            string Remark = "Credit  Against Visa Invoice " + Vcode;
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                SqlParameter[] sqlParams = new SqlParameter[2];
                int rowsAffected = 0;
                sqlParams[0] = new SqlParameter("@Status", "Rejected By Administrator");
                sqlParams[1] = new SqlParameter("@UserId", Vcode);
                retcode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByApplicationRejection", out rowsAffected, sqlParams);
                if (retcode == DBHelper.DBReturnCode.SUCCESS && IsRefund == true)
                {
                    Int64 sessionsid;
                    //string ExecutiveName = "";
                    //int MinLimit = 0;
                    string AgainInvoice = "";
                    string PerticulaAmount = "";
                    string otc;
                    if (MaxCreditAmmount > 0)
                    {
                        otc = "True";
                    }
                    else
                    {
                        otc = "false";
                    }
                    if (Amount != 0)
                    {
                        PerticulaAmount = Convert.ToString(Amount);
                    }
                    else if (ReduceAmmount != 0)
                    {
                        PerticulaAmount = Convert.ToString(ReduceAmmount);
                    }
                    string Particulars = "";
                    if (Vcode != "undefined")
                    {
                        AgainInvoice = " - " + Vcode;
                    }

                    GlobalDefault objGlobal;
                    objGlobal = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    sessionsid = objGlobal.sid;
                    string ContactPerson = objGlobal.ContactPerson;
                    Particulars = "Credit" + " - " + ContactPerson + " - " + PerticulaAmount + AgainInvoice;
                    SqlParameter[] sqlParams1 = new SqlParameter[12];
                    sqlParams1[0] = new SqlParameter("@sid", sid);
                    sqlParams1[1] = new SqlParameter("@uid", AgentId);
                    sqlParams1[2] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                    sqlParams1[3] = new SqlParameter("@ActiveFlag", bActiveFlag);
                    sqlParams1[4] = new SqlParameter("@DepositAmmount", Amount);
                    sqlParams1[5] = new SqlParameter("@CreditAmmount", CreditAmmount);
                    sqlParams1[6] = new SqlParameter("@MaxCreditAmmount", MaxCreditAmmount);
                    sqlParams1[7] = new SqlParameter("@ReduceAmmount", ReduceAmmount);
                    sqlParams1[8] = new SqlParameter("@OTCActive", otc);
                    sqlParams1[9] = new SqlParameter("@Remark", Remark);
                    sqlParams1[10] = new SqlParameter("@Particulars", Particulars);
                    sqlParams1[11] = new SqlParameter("@InvoiceNo", AgainInvoice);
                    retcode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitCreditDebit", out rows, sqlParams1);
                    if (retcode != DBHelper.DBReturnCode.SUCCESS)
                    {
                        objTransactionScope.Dispose();
                    }
                    else
                    {
                        objTransactionScope.Complete();
                    }
                }
                else if (IsRefund == false)
                {
                    objTransactionScope.Complete();
                }
                else
                {
                    objTransactionScope.Dispose();
                }
            }
            return retcode;
        }
        #endregion

        #region Visa Activity Mails
        public static DBHelper.DBReturnCode GetVisaMails(string Activity, string Type, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Activity", Activity);
            sqlParams[1] = new SqlParameter("@Type", Type);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ActivityMailLoadByMails", out dtResult, sqlParams);
            return retCode;
        }
        #region Update Visa Activity Mails
        public static DBHelper.DBReturnCode UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail)
        {
            int RowEffected;
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@Activity", Activity);
            sqlParams[1] = new SqlParameter("@MailsId", MailsId);
            sqlParams[2] = new SqlParameter("@CcMail", CcMails);
            sqlParams[3] = new SqlParameter("@BCcMail", BCcMail);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ActivityMailUpdateByActivity", out RowEffected, sqlParams);
            return retCode;
        }
        #endregion
        #endregion
    }
}