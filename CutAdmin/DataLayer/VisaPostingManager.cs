﻿using CutAdmin.BL;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class VisaPostingManager
    {
        public static IWebDriver driver { get; set; }
        public static string Validation { get; set; }
        #region Visa Posting
        public static string fnNext()
        {
            return "fnNext('#Pe')";
        }
        public static string GetProfession(string Profession)
        {
            string ProfessionCode = "";
            if (Profession == "NONE")
            {
                ProfessionCode = "2";
            }
            else if (Profession == "STUDENT")
            {
                ProfessionCode = "9900015";
            }
            else if (Profession == "SALES REPRESENTATIVE")
            {
                ProfessionCode = "4112043";
            }
            else if (Profession == "MARKETING ASSISTANT")
            {
                ProfessionCode = "4112067";
            }
            else if (Profession == "MARKETING EXECUTIVE")
            {
                ProfessionCode = "4112041";
            }
            else if (Profession == "BUSINESSMAN")
            {
                ProfessionCode = "9800611";
            }
            else if (Profession == "BUSINESSWOMAN")
            {
                ProfessionCode = "9800612";
            }
            else if (Profession == "BUSINESS PERSON")
            {
                ProfessionCode = "9800041";
            }
            else if (Profession == "BUSINESS")
            {
                ProfessionCode = "9800601";
            }
            else if (Profession == "HOUSE WIFE")
            {
                ProfessionCode = "9900011";
            }
            return ProfessionCode;
        }

        static public bool URLExists(string url, string downloadPath, string VCode, int FileNo)
        {
            bool result = false;
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                if (response.ContentType == "image/jpeg")
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (WebException ex)
            {
                result = false;
            }
            finally
            {

                if (response != null)
                {
                    response.Close();
                }
            }
            return result;
        }

        public static bool FlieSize(string Croped, string FileNo, string RefrenceNo)
        {
            string VisaPath = ""; Int64 FileSize = 0;
            if (Croped == "True")
                VisaPath = System.Web.HttpContext.Current.Server.MapPath("~/VisaImages/crop/");
            else
                VisaPath = System.Web.HttpContext.Current.Server.MapPath("~/VisaImages/");
            string VisaFName = RefrenceNo + "_" + FileNo + ".jpg";
            DirectoryInfo dir = new DirectoryInfo(VisaPath);
            FileInfo[] files = null;

            files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                if (file.Name == VisaFName)
                {

                    FileSize = file.Length / 1000;
                    if (FileSize > 40)
                        return false;
                    else
                        return true;
                }
            }

            return false;
        }

        public static string FilePath(string Croped, string FileNo, string RefrenceNo)
        {
            string Path = ConfigurationManager.AppSettings["rootPath"];
            if (Croped == "True")
                return Path + @"VisaImages\crop\" + RefrenceNo + "_" + FileNo + ".jpeg";
            else
                return Path + @"VisaImages\" + RefrenceNo + "_" + FileNo + ".jpeg";
        }
        //public static bool VisaPosting(string[] RefrenceNo, string Username, string Password, out string[] AppNo, out string[] Status, string Supplier)
        //{
        //    string Path = HttpContext.Current.Server.MapPath("~/lib/").ToString();
        //    string Snap = HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap").ToString();
        //    bool Run = CheckIfProcessAlreadyRunning();
        //    driver = new PhantomJSDriver(Path);
        //    string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(120);
        //    AppNo = new string[RefrenceNo.Length];
        //    Status = new string[RefrenceNo.Length];
        //    int k = 0; IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
        //    Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot();
        //    bool failure = false;
        //    IWebElement query = null; bool Valid = true;
        //    try
        //    {
        //        if (Username == "ofwpl17")
        //        {

        //            driver.Navigate().GoToUrl("https://vlogin.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~44F441AB~5B2FB16CD0DCF1FDB5CFA9FFE41E62A6A1F64630ED75B27D1D903A4BE823B4DE69479C298F7B1A54529492434628D0E057490C581C78A8A46C9B3329EF85016CA3C8284EBA8739BB8626A9DAEE73465484C3C53C682A9CA99824B54703ACEA8DF5552764E5E4C2ED9185FE7199466AFB03CB48002D4D138F2FD960D7B2C9614F9C731C7717F361DE99A18D25603E394E8DC1D98C4439A0D8E8813AEBD126FCAB1E6C27FDB2F5F81DE45645AEBE72AC91577B748138A4577E93D1771E3DB646E7A816C0B03C8E250DC5803E59605F8CDE33213CF91C7E0387B3DD5781EB5FAD1CB6208988680490B3A0D8A5359636F1C6E7D9E9EF134BF2C028AB583B6DF2252664EDAC1AC98B0D9B&p_error_code=&p_submit_url=https%3A%2F%2Fvlogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.gdrfa.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=");
        //            while (driver.Url != "https://vlogin.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~44F441AB~5B2FB16CD0DCF1FDB5CFA9FFE41E62A6A1F64630ED75B27D1D903A4BE823B4DE69479C298F7B1A54529492434628D0E057490C581C78A8A46C9B3329EF85016CA3C8284EBA8739BB8626A9DAEE73465484C3C53C682A9CA99824B54703ACEA8DF5552764E5E4C2ED9185FE7199466AFB03CB48002D4D138F2FD960D7B2C9614F9C731C7717F361DE99A18D25603E394E8DC1D98C4439A0D8E8813AEBD126FCAB1E6C27FDB2F5F81DE45645AEBE72AC91577B748138A4577E93D1771E3DB646E7A816C0B03C8E250DC5803E59605F8CDE33213CF91C7E0387B3DD5781EB5FAD1CB6208988680490B3A0D8A5359636F1C6E7D9E9EF134BF2C028AB583B6DF2252664EDAC1AC98B0D9B&p_error_code=&p_submit_url=https%3A%2F%2Fvlogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.gdrfa.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=") { };

        //        }
        //        else
        //        {
        //            driver.Navigate().GoToUrl("https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=");
        //            while (driver.Url != "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=") { };


        //        }

        //        js.ExecuteScript(SetValueByName("ssousername", Username));
        //        js = (IJavaScriptExecutor)driver;
        //        js.ExecuteScript(SetValueByName("password", Password));
        //        js.ExecuteScript(CheckedByName("terms"));
        //        js.ExecuteScript("javascript:fnValidate()");
        //        string Usser = "", ServiceType = "", Session = "", App_Id = "";
        //        string urll = "";
        //        if (Username == "ofwpl17")
        //        {
        //            urll = "https://www.gdrfa.ae";
        //            while (driver.Url != "https://www.gdrfa.ae/portal/page/portal/ePortal")
        //            {
        //                continue;
        //            };
        //        }
        //        else
        //        {
        //            urll = "https://www.ednrd.ae";
        //            while (driver.Url != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //            {
        //                continue;
        //            };
        //        }

        //        if (Valid)
        //        {
        //            foreach (string Ref in RefrenceNo)
        //            {
        //                try
        //                {
        //                    if (failure)
        //                    {
        //                        driver.Dispose();
        //                        driver = new PhantomJSDriver(Path);
        //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(120);
        //                        driver.Navigate().GoToUrl("https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=");
        //                        while (driver.Url != "https://login.ednrd.ae/DNRDsso/login.jsp?site2pstoretoken=v1.2~4C198B41~65F44F9328DA64573C822CCEC56045A16AC1E00149676561ABC490AC383BC8225EB772BC0387088C750AE14EC6F2134275FA9FA284CA5E6E3236CC68532A7837AD8E51096D0E602484C4C9AB960B652F900C706686ED1CF0B502E05CE7A41AFF2C5DF501919092B53AD010FB6059EBA01A07EFEEA7E5A53730A29486C872B5A6E31DB3F684ACA6F18B8BC4D684B6B356F26236BBCD79CD4762E62FB0133DB3EF47D5B388B678496AC93E74A718EC33D435767A4ABAEBFE223324F639DC8473DFD244D117202A263D0CBB36137CBD6B7FF7917CE11739C2E2E49E8C81DDA82A5D&p_error_code=&p_submit_url=https%3A%2F%2Flogin.ednrd.ae%2Fsso%2Fauth&p_cancel_url=https%3A%2F%2Fwww.ednrd.ae%2Fportal%2Fpls%2Fportal%2FPORTAL.home&ssousername=") { };
        //                        js.ExecuteScript(SetValueByName("ssousername", Username));
        //                        js = (IJavaScriptExecutor)driver;
        //                        js.ExecuteScript(SetValueByName("password", Password));
        //                        js.ExecuteScript(CheckedByName("terms"));
        //                        js.ExecuteScript("javascript:fnValidate()");
        //                        while (driver.Url != "https://www.ednrd.ae/portal/page/portal/ePortal")
        //                        {
        //                            continue;
        //                        };
        //                        failure = false;
        //                    }
        //                    string[,] arr1 = tblRecords(Ref);
        //                    string CropedPhoto = arr1[0, 72], CropedFirst = arr1[0, 73], CropedLast = arr1[0, 74], CropedSupp1 = arr1[0, 75], CropedSupp2 = arr1[0, 76];
        //                    // if (driver.Url == "https://www.ednrd.ae/portal/page/portal/ePortal")
        //                    //  {
        //                    driver.Navigate().GoToUrl(urll + "/portal/pls/portal/INIMM_DB.visa_redirect.show");
        //                    while (driver.Url == urll + "/portal/pls/portal/INIMM_DB.visa_redirect.show") { };

        //                    if (arr1[0, 56] == "90 Days Tourist Single Entry Convertible")
        //                    {
        //                        driver.Navigate().GoToUrl(urll + "/portal/pls/portal/PORTAL.wwa_app_module.new_instance?p_moduleid=9860148142");
        //                        while (driver.Url == urll + "/portal/pls/portal/INIMM_DB.visa_redirect.show") { }
        //                    }
        //                    else
        //                    {
        //                        string Elements = "FRM_VISA_SINGLE_FORM_B.DEFAULT.";
        //                        if (Username == "ofwpl17")
        //                            Elements = "FRM_VISA_SINGLE_FORM_CONN.DEFAULT.";
        //                        #region Visa Details
        //                        try
        //                        {

        //                            GetOldScript(driver.PageSource, out Usser, out ServiceType, out Session);
        //                            js.ExecuteScript(ExcecuteScript(Usser, Elements + "SPN_NO.01"));
        //                            string[] s = Regex.Split(Usser, @"\^");

        //                            js.ExecuteScript(SelectByValue(Elements + "SPN_NO.01", s[0]));
        //                            IWebElement ServiceTypes = driver.FindElement(By.Name(Elements + "VISA_TP.01"));
        //                            s = Regex.Split(ServiceType, ";");
        //                            for (int i = 0; i < s.Length; i++)
        //                            {
        //                                if (s[i] == "")
        //                                    continue;
        //                                js.ExecuteScript(ExcecuteScript(s[i], Elements + "VISA_TP.01"));
        //                            }

        //                        }
        //                        catch (Exception exs)
        //                        {
        //                            Console.WriteLine(exs.Message);
        //                        }
        //                        //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        //Is.SaveAsFile(Snap + "\\" + Ref + "error.jpeg");
        //                        IWebElement Processing = driver.FindElement(By.Name(Elements + "URGENT.01"));
        //                        js.ExecuteScript(SetValueByName(Elements + "URGENT.01", "1"), Processing);

        //                        IWebElement type = driver.FindElement(By.Name(Elements + "VISA_TP.01"));
        //                        ReadOnlyCollection<IWebElement> Serviceoptions = type.FindElements(By.TagName("option"));
        //                        if (arr1[0, 56] == "90 days Tourist Single Entry")
        //                        {
        //                            arr1[0, 56] = "77";
        //                        }
        //                        else if (arr1[0, 56] == "14 days Service VISA")
        //                        { arr1[0, 56] = "8"; }
        //                        else if (arr1[0, 56] == "30 days Tourist Single Entry")
        //                        { arr1[0, 56] = "87"; }
        //                        else if (arr1[0, 56] == "90 days Tourist Multiple Entry")
        //                        {
        //                            arr1[0, 56] = "78";
        //                        }
        //                        else if (arr1[0, 56] == "90 Days Tourist Single Entry Convertible")
        //                        {
        //                            arr1[0, 56] = "86";
        //                        }
        //                        js.ExecuteScript(SelectByValue(Elements + "VISA_TP.01", arr1[0, 56]));

        //                        #endregion

        //                        #region Personal Informations
        //                        js.ExecuteScript(SetValueByName(Elements + "F_NM_E.01", arr1[0, 5]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.F_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.F_NM_A.01')");
        //                        }



        //                        if (arr1[0, 6] != "")
        //                            js.ExecuteScript(SetValueByName(Elements + "M_NM_E.01", arr1[0, 6]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.M_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.M_NM_A.01')");
        //                        }

        //                        js.ExecuteScript(SetValueByName(Elements + "L_NM_E.01", arr1[0, 8]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.L_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.L_NM_A.01')");
        //                        }

        //                        js.ExecuteScript(SetValueByName(Elements + "FATHER_NM_E.01", arr1[0, 8]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.FATHER_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.FATHER_NM_A.01')");
        //                        }

        //                        js.ExecuteScript(SetValueByName(Elements + "MOTHER_NM_E.01", arr1[0, 9]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.MOTHER_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.MOTHER_NM_A.01')");
        //                        }

        //                        js.ExecuteScript(SetValueByName(Elements + "HUSBAN_NM_E.01", arr1[0, 10]));
        //                        if (Username == "ofwpl17")
        //                        {
        //                            js.ExecuteScript("javascript:fnARA('internal', '2','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.HUSBAN_NM_E.01','FRM_VISA_SINGLE_FORM_CONN.DEFAULT.HUSBAN_NM_A.01')");
        //                        }
        //                        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        Is.SaveAsFile(Snap + "\\" + Ref + "error6.jpeg");


        //                        js.ExecuteScript(SelectByValue(Elements + "LANG1.01", arr1[0, 11]));

        //                        js.ExecuteScript(SelectByValue(Elements + "SEX.01", arr1[0, 12]));


        //                        js.ExecuteScript(SelectByValue(Elements + "MAT_STA.01", arr1[0, 13]));

        //                        js.ExecuteScript(SelectByValue(Elements + "PRS_NAT.01", arr1[0, 14]));

        //                        IWebElement BirthDate = driver.FindElement(By.Name(Elements + "BIRTH_DT.01"));
        //                        js.ExecuteScript(SetValueByName(Elements + "BIRTH_DT.01", arr1[0, 15]), BirthDate);


        //                        IWebElement Birth_PLC = driver.FindElement(By.Name(Elements + "BIRTH_PLC.01"));
        //                        js.ExecuteScript(SetValueByName(Elements + "BIRTH_PLC.01", arr1[0, 17]), Birth_PLC);



        //                        js.ExecuteScript(SelectByValue(Elements + "BIRTH_CNTRY.01", arr1[0, 14]));

        //                        js.ExecuteScript(SelectByValue(Elements + "RLG.01", arr1[0, 19]));


        //                        IWebElement Prof_NMA = driver.FindElement(By.Name(Elements + "NMA.01"));


        //                        IWebElement PROF = driver.FindElement(By.Name(Elements + "PROF.01"));

        //                        if (arr1[0, 20] != "")
        //                        {
        //                            js.ExecuteScript(SetValueByName(Elements + "NMA.01", arr1[0, 20]), Prof_NMA);
        //                            js.ExecuteScript(SetValueByName(Elements + "PROF.01", GetProfession(arr1[0, 20])), PROF);

        //                        }
        //                        else
        //                        {
        //                            js.ExecuteScript(SetValueByName(Elements + "NMA.01", arr1[0, 20]), Prof_NMA);
        //                            js.ExecuteScript(SetValueByName(Elements + "PROF.01", "4112042"), PROF);
        //                        }
        //                        #endregion

        //                        #region Passport Details & Address Outside UAE
        //                        js.ExecuteScript(SetValueByName(Elements + "PASS_NO.01", arr1[0, 21]));

        //                        js.ExecuteScript(SelectByValue(Elements + "PASS_TP.01", "1"));


        //                        js.ExecuteScript(SelectByValue(Elements + "PASS_ISSGOV.01", arr1[0, 14]));

        //                        js.ExecuteScript(SelectByValue(Elements + "PASS_CNTRY.01", arr1[0, 14]));

        //                        js.ExecuteScript(SetValueByName(Elements + "PASS_PLC.01", arr1[0, 23]));

        //                        js.ExecuteScript(SetValueByName(Elements + "PASS_IS_DT.01", arr1[0, 24].Substring(0, 10)));


        //                        js.ExecuteScript(SetValueByName(Elements + "PASS_EXP_DT.01", arr1[0, 25].Substring(0, 10)));

        //                        js.ExecuteScript(SetValueByName(Elements + "AOADD1.01", arr1[0, 26]));


        //                        js.ExecuteScript(SetValueByName(Elements + "AOADD2.01", arr1[0, 27]));

        //                        js.ExecuteScript(SetValueByName(Elements + "AOCITY.01", arr1[0, 28]));

        //                        js.ExecuteScript(SelectByValue(Elements + "AOCNTRY.01", arr1[0, 29]));

        //                        js.ExecuteScript(SetValueByName(Elements + "AOTEL.01", "0"));

        //                        //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        //// OpenQA.Selenium.Support(Snap + "\\PassportDetails.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        IWebElement Save = driver.FindElement(By.Name(Elements + "NEXT1.01"));



        //                        js.ExecuteScript(SetValueByName(Elements + "RES_FNO.01", "-1"));

        //                        js.ExecuteScript(SetValueByName(Elements + "APP_TP.01", "1"));

        //                        js.ExecuteScript(SetValueByName("p_object_name", Elements + "NEXT1.01"));

        //                        js.ExecuteScript(SetValueByName("p_instance", "1"));

        //                        js.ExecuteScript(SetValueByName("p_event_type", "CUSTOM"));

        //                        js.ExecuteScript(SetValueByName("p_user_args", null));

        //                        System.Threading.Thread.Sleep(500);

        //                        driver.FindElement(By.Name(Session)).Submit();

        //                        while (driver.Url != urll + "/portal/pls/portal/PORTAL.wwa_app_module.show?p_header=true&p_sessionid=" + Session.Replace("WWVM", "")) { System.Threading.Thread.Sleep(500); };

        //                        // For PotomJs Browser //
        //                        IWebElement Sponsors = driver.FindElement(By.Name(Elements + "SPN_NO.01"));
        //                        js.ExecuteScript(ExcecuteScript(Usser, Elements + "SPN_NO.01"), Sponsors);

        //                        string[] S = Regex.Split(Usser, @"\^");
        //                        js.ExecuteScript(SelectByValue(Elements + "SPN_NO.01", S[0]));


        //                        S = Regex.Split(ServiceType, ";");
        //                        for (int i = 0; i < S.Length; i++)
        //                        {
        //                            if (S[i] == "")
        //                                continue;
        //                            js.ExecuteScript(ExcecuteScript(S[i], Elements + "VISA_TP.01"));
        //                        }
        //                        js.ExecuteScript(ExcecuteScript(ServiceType, Elements + "VISA_TP.01"));
        //                        js.ExecuteScript(SelectByValue(Elements + "VISA_TP.01", arr1[0, 56]));




        //                        js.ExecuteScript(SetValueByName("p_object_name", Elements + "NEXT1.01"));
        //                        js.ExecuteScript(SetValueByName("p_instance", "1"));
        //                        js.ExecuteScript(SetValueByName("p_event_type", "ON_CLICK"));
        //                        js.ExecuteScript(SetValueByName("p_user_args", null));
        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_BasicDetails.jpeg");
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_BasicDetails.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        string OldUrl = driver.Url; string NewUrl = driver.Url;
        //                        js.ExecuteScript(Submit(Session, Elements + "NEXT1.01"));
        //                        while (OldUrl == NewUrl) { NewUrl = driver.Url; }




        //                        #endregion


        //                        #region Upload Attacments
        //                        string url, VCode = Ref; int[] Photo_Count = new int[5];


        //                        // Passport Photo // 

        //                        if (FlieSize(CropedPhoto, "1", Ref))
        //                        {
        //                            if (Username != "ofwpl17")
        //                                js.ExecuteScript(SelectByValue("doc_type", "1"));
        //                            else
        //                                js.ExecuteScript(SelectByValue("doc_type", "5"));
        //                            IWebElement file1 = driver.FindElement(By.Name("file1"));
        //                            driver.FindElement(By.Name("file1")).SendKeys(FilePath(CropedPhoto, "1", Ref));

        //                            IWebElement nxtBtn = driver.FindElement(By.Name("p_add"));
        //                            js.ExecuteScript(Click("p_add"), nxtBtn);
        //                            //js.ExecuteScript("javascript:submit_form()");
        //                            System.Threading.Thread.Sleep(1000);
        //                            while (Photo_Count[0] != 1)
        //                            {
        //                                Photo_Count[0] = GetNoDoc(driver.PageSource); Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                                Is.SaveAsFile(Snap + "\\" + Ref + "_BasicDetails.jpeg");
        //                            };
        //                        }
        //                        else
        //                        {
        //                            Valid = false;
        //                            Status[k] += "Photograph :  Should Not Exceed By 40 KB \n";

        //                        }

        //                        if (FlieSize(CropedFirst, "2", Ref))
        //                        {
        //                            if (Username != "ofwpl17")
        //                                js.ExecuteScript(SelectByValue("doc_type", "2"));
        //                            else
        //                                js.ExecuteScript(SelectByValue("doc_type", "41"));
        //                            IWebElement file2 = driver.FindElement(By.Name("file1"));
        //                            driver.FindElement(By.Name("file1")).SendKeys(FilePath(CropedFirst, "2", Ref));
        //                            IWebElement nxtBtn2 = driver.FindElement(By.Name("p_add"));
        //                            js.ExecuteScript(Click("p_add"), nxtBtn2);
        //                            System.Threading.Thread.Sleep(1000);
        //                            while (Photo_Count[1] != 2) { Photo_Count[1] = GetNoDoc(driver.PageSource); };
        //                        }
        //                        else
        //                        {
        //                            Valid = false;
        //                            Status[k] += "First  Page :  Should Not Exceed By 40 KB \n";
        //                        }
        //                        // End Last page //

        //                        if (FlieSize(CropedLast, "3", Ref))
        //                        {
        //                            //  Passport Second Page //
        //                            if (Username != "ofwpl17")
        //                                js.ExecuteScript(SelectByValue("doc_type", "2"));
        //                            else
        //                                js.ExecuteScript(SelectByValue("doc_type", "42"));
        //                            IWebElement file3 = driver.FindElement(By.Name("file1"));
        //                            driver.FindElement(By.Name("file1")).SendKeys(FilePath(CropedLast, "3", Ref));

        //                            IWebElement nxtBtn3 = driver.FindElement(By.Name("p_add"));
        //                            js.ExecuteScript(Click("p_add"), nxtBtn3);
        //                            System.Threading.Thread.Sleep(1000);
        //                            while (Photo_Count[2] != 3) { Photo_Count[2] = GetNoDoc(driver.PageSource); };
        //                        }
        //                        else
        //                        {
        //                            Valid = false;
        //                            Status[k] += "Last  Page :  Should Not Exceed By 40 KB \n";
        //                        }
        //                        // End Last page //

        //                        // Supporting Documents // 

        //                        bool FileExist;
        //                        url = "http://clickurtrip.com/VisaImages/" + VCode + "_4.jpg";
        //                        FileExist = URLExists(url, downloadPath, VCode, 4);
        //                        if (FileExist == true)
        //                        {
        //                            if (FlieSize(CropedSupp1, "4", Ref))
        //                            {

        //                                if (Username != "ofwpl17")
        //                                    js.ExecuteScript(SelectByValue("doc_type", "2"));
        //                                else
        //                                    js.ExecuteScript(SelectByValue("doc_type", "43"));
        //                                IWebElement file4 = driver.FindElement(By.Name("file1"));
        //                                driver.FindElement(By.Name("file1")).SendKeys(FilePath(CropedSupp1, "4", Ref));

        //                                IWebElement nxtBtn4 = driver.FindElement(By.Name("p_add"));
        //                                js.ExecuteScript(Click("p_add"), nxtBtn4);
        //                                System.Threading.Thread.Sleep(1000);
        //                                while (Photo_Count[3] != 4) { Photo_Count[3] = GetNoDoc(driver.PageSource); };
        //                            }
        //                            else
        //                            {
        //                                Valid = false;
        //                                Status[k] += "Supporting1 :  Should Not Exceed By 40 KB \n";
        //                            }

        //                        }
        //                        url = "http://clickurtrip.com/VisaImages/" + VCode + "_5.jpg";
        //                        FileExist = URLExists(url, downloadPath, VCode, 5);
        //                        if (FileExist == true)
        //                        {
        //                            if (FlieSize(CropedSupp2, "5", Ref))
        //                            {
        //                                if (Username != "ofwpl17")
        //                                    js.ExecuteScript(SelectByValue("doc_type", "2"));
        //                                else
        //                                    js.ExecuteScript(SelectByValue("doc_type", "44"));
        //                                IWebElement file4 = driver.FindElement(By.Name("file1"));
        //                                driver.FindElement(By.Name("file1")).SendKeys(FilePath(CropedSupp2, "5", Ref));

        //                                IWebElement nxtBtn4 = driver.FindElement(By.Name("p_add"));
        //                                js.ExecuteScript(Click("p_add"), nxtBtn4);
        //                                System.Threading.Thread.Sleep(1000);
        //                                while (Photo_Count[4] != 5) { Photo_Count[4] = GetNoDoc(driver.PageSource); };
        //                            }
        //                            else
        //                            {
        //                                Valid = false;
        //                                Status[k] += "Supporting2 :  Should Not Exceed By 40 KB \n";
        //                            }
        //                        }
        //                        if (Valid == false)
        //                        {
        //                            AppNo[k] = "n/a";
        //                            k++;
        //                            continue;
        //                        }
        //                        // End Supporting Documents // 

        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_Uploads.jpeg");
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Uploads.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        #endregion End Upload files


        //                        #region Payment & Status Update
        //                        //System.Threading.Thread.Sleep(2000);
        //                        //// Next Page // 
        //                        IWebElement appno = driver.FindElement(By.Name("app_id"));
        //                        string id = appno.GetAttribute("value");
        //                        //js.ExecuteScript(Redirect("https://www.ednrd.ae/portal/pls/portal/INIMM_DB.OTHER_SERVICES3.show?p_arg_names=vid&p_arg_values=" + id), driver.Url);
        //                        driver.Navigate().GoToUrl(urll + "/portal/pls/portal/INIMM_DB.OTHER_SERVICES3.show?p_arg_names=vid&p_arg_values=" + id);
        //                        while (driver.Url != urll + "/portal/pls/portal/INIMM_DB.OTHER_SERVICES3.show?p_arg_names=vid&p_arg_values=" + id)
        //                            //    Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            //// OpenQA.Selenium.Support(Snap + "\\Payement.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
        //                            System.Threading.Thread.Sleep(1000);
        //                        OldUrl = driver.Url; NewUrl = driver.Url;
        //                        // js.ExecuteScript(Click("name"));
        //                        IWebElement Pay = driver.FindElement(By.Name("name"));
        //                        Pay.Click();
        //                        while (OldUrl == NewUrl) { NewUrl = driver.Url; }

        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            Is.SaveAsFile(Snap + "\\" + Ref + "_Payment.jpeg");
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Payment.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        string AppNos = driver.Url;
        //                        string[] status = Regex.Split(AppNos, "&");
        //                        AppNo[k] = Regex.Split(status[3], "=")[1];
        //                        //AppNo[k] = "1234";
        //                        Status[k] = "Posted";
        //                        UpdateApplicationNo(Ref, Username, Password, AppNo[k], Supplier);
        //                        k++;

        //                        #endregion

        //                    }
        //                    //}

        //                    // }
        //                }
        //                catch
        //                {
        //                    try
        //                    {
        //                        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                        Is.SaveAsFile(Snap + "\\" + Ref + "_Error.jpeg");
        //                        // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                    }
        //                    catch
        //                    {

        //                    }
        //                    AppNo[k] = "";
        //                    Status[k] = "Posting Fail";
        //                    k++;
        //                }
        //            }
        //        }
        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Status[0] = ex.Message;
        //        driver.Dispose();
        //        // CheckIfProcessAlreadyRunning();
        //        return true;
        //    }

        //}
        public static bool CheckIfProcessAlreadyRunning()
        {
            try
            {

                foreach (Process proc in Process.GetProcessesByName("phantomjs"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public static string NoImageUpload(string Name, int ImageNo)
        {
            //return "p_form1.elements['" + Name + "'].length";
            return "var x = document.getElementsByName('" + Name + "')[+" + ImageNo + "].defaultValue";
        }
        public static int GetNoDoc(string Cotaint)
        {
            int noDoc = 0;
            string[] s = Regex.Split(Cotaint, "PortletHeading1");
            try
            {
                if (s.Length == 3)
                {
                    if (s[2].Contains("Picture(1)"))
                    {
                        noDoc += 2;
                    }
                    if (s[2].Contains("Picture(2)"))
                    {
                        noDoc += 1;
                    }
                    if (s[2].Contains("Picture(3)"))
                    {
                        noDoc += 1;
                    }
                    if (s[2].Contains("Picture(4)"))
                    {
                        noDoc += 1;
                    }

                }
                else
                    noDoc = s.Length - 1;
            }
            catch
            {

            }
            return noDoc;
        }

        #region GetData
        public static string[,] tblRecords(string VisaCode)
        {
            DataTable NotNullTable = GetVisaByCode(VisaCode);
            int rws = NotNullTable.Rows.Count;
            int clmsn = NotNullTable.Columns.Count;
            string[,] Arr1 = new string[rws, clmsn];
            for (int i = 0; i < rws; i++)
            {
                for (int j = 0; j < clmsn; j++)
                {
                    Arr1[i, j] = NotNullTable.Rows[i].ItemArray[j].ToString();
                }
            }
            return Arr1;
        }
        public static DataTable GetVisaByCode(string VisaCode)
        {
            DataTable tbl_Visa;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@VisaCode", VisaCode);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_VisaLoadByVisaCode", out tbl_Visa, sqlParams);
            return tbl_Visa;
        }
        public static bool UpdateApplicationNo(string Vcode, string Username, string Password, string App, string Supplier)
        {
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[6];
            int rowsAffected = 0;
            if (App != "")
            {
                sqlParams[0] = new SqlParameter("@Status", "Posted");
                sqlParams[1] = new SqlParameter("@Vcode", Vcode);
                sqlParams[2] = new SqlParameter("@Username", Username);
                sqlParams[3] = new SqlParameter("@Password", Password);
                sqlParams[4] = new SqlParameter("@ApplicationNo", App);
                sqlParams[5] = new SqlParameter("@Supplier", Supplier);
                retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaStatusByUpdateByIEPosting", out rowsAffected, sqlParams);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion

        #endregion

        #region CozmoPosting
        //public static bool CozmoPosting(string[] RefrenceNo, string[] PaxType, string Username, string Password, out string[] AppNo, out string[] Status, string Supplier)
        //{
        //    string Path = HttpContext.Current.Server.MapPath("~/lib/").ToString();
        //    string Snap = HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap").ToString();
        //    bool Run = CheckIfProcessAlreadyRunning();
        //    //IWebDriver driver = new PhantomJSDriver(Path);
        //    // IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
        //    IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
        //    string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
        //    AppNo = new string[RefrenceNo.Length];
        //    Status = new string[RefrenceNo.Length];
        //    IAlert Alerts;
        //    int k = 0; IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
        //    Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot();
        //    string ValidateStatus = "";
        //    IWebElement query = null; bool bvalid = true;
        //    try
        //    {
        //        driver.Navigate().GoToUrl("http://czt.dyndns.info:99/cozmovisa/login.aspx");
        //        while (driver.Url != "http://czt.dyndns.info:99/cozmovisa/login.aspx") { };
        //        js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtLoginName", Username));
        //        js = (IJavaScriptExecutor)driver;
        //        js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPassword", Password));

        //        js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnLogin').click();");
        //        System.Threading.Thread.Sleep(1000);
        //        if (driver.Url == "http://czt.dyndns.info:99/cozmovisa/login.aspx")
        //            throw new Exception("Unable to login");
        //        driver.Navigate().GoToUrl("http://czt.dyndns.info:99/cozmovisa/TranxVisaSales.aspx");
        //        while (driver.Url != "http://czt.dyndns.info:99/cozmovisa/TranxVisaSales.aspx") { }

        //        #region Visa Type and Visa Details
        //        IWebElement element = driver.FindElement(By.Id("ctl00_cphTransaction_ddlVisaFeeCode"));
        //        SelectElement oSelect = new SelectElement(element);
        //        oSelect.SelectByValue("6500");
        //        int Adult, Child, Infant;
        //        NoPax(out  Adult, out   Child, out  Infant, PaxType);
        //        if (Adult != 1)
        //        {
        //            IWebElement SelAdult = driver.FindElement(By.Id("ctl00_cphTransaction_ddlAdults"));
        //            SelectElement oSelAdult = new SelectElement(SelAdult);
        //            oSelAdult.SelectByValue(Adult.ToString());
        //        }

        //        if (Child != 0)
        //        {
        //            IWebElement SelChild = driver.FindElement(By.Id("ctl00_cphTransaction_ddlChildren"));
        //            SelectElement oSelChild = new SelectElement(SelChild);
        //            oSelChild.SelectByValue(Child.ToString());
        //        }
        //        if (Infant != 0)
        //        {
        //            IWebElement SelInfant = driver.FindElement(By.Id("ctl00_cphTransaction_ddlInfants"));
        //            SelectElement oSelInfant = new SelectElement(SelInfant);
        //            oSelInfant.SelectByValue(Infant.ToString());
        //        }
        //        #endregion


        //        //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //        //// OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);

        //        #region Add Nultipule Visa
        //        int p = 0;
        //        foreach (string Ref in RefrenceNo)
        //        {
        //            DataTable dtVisaDetails = new DataTable();
        //            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaLoadByCode(Ref, out dtVisaDetails);
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {

        //                try
        //                {
        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxName", dtVisaDetails.Rows[0]["FirstName"].ToString()));// First Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxSurName", dtVisaDetails.Rows[0]["LastName"].ToString()));// Last Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxPassport", dtVisaDetails.Rows[0]["PassportNo"].ToString()));// Passport No


        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtPaxProfession", dtVisaDetails.Rows[0]["Profession"].ToString()));// Profession 

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtFatherName", dtVisaDetails.Rows[0]["FatherName"].ToString()));// Father Name 

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtMotherName", dtVisaDetails.Rows[0]["MotherName"].ToString()));// Mother Name

        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$dcPaxPspExpiryOn$Date", GetDateTime(dtVisaDetails.Rows[0]["ExpDate"].ToString())));// Passport Expiry

        //                    js.ExecuteScript(SelectDropDown(PaxType[p].Replace("Adult", "ADULT"), "ctl00_cphTransaction_ddlPaxType"));  // Pax Type

        //                    js.ExecuteScript(SelectDropDown(dtVisaDetails.Rows[0]["MaritalStatus"].ToString().Replace("1", "MARRIED").Replace("2", "SINGLE"), "ctl00_cphTransaction_ddlVstMaritalStatus"));


        //                    if (dtVisaDetails.Rows[0]["MaritalStatus"].ToString() == "1" && dtVisaDetails.Rows[0]["Gender"].ToString() == "1")
        //                    {
        //                        if (dtVisaDetails.Rows[0]["HusbandMame"].ToString() != "")
        //                            js.ExecuteScript(SetValueByName("ctl00$cphTransaction$txtSpouseName", dtVisaDetails.Rows[0]["HusbandMame"].ToString()));// Spouse Name
        //                        else
        //                            ValidateStatus = "Spouse Name cannot be blank !\n";
        //                    }


        //                    IWebElement SelNationality = driver.FindElement(By.Id("ctl00_cphTransaction_ddlPaxNationality"));
        //                    SelectElement oSelNationality = new SelectElement(SelNationality);
        //                    oSelNationality.SelectByValue(Nationality(dtVisaDetails.Rows[0]["PresentNationality"].ToString()));


        //                    js.ExecuteScript(SelectDropDown(dtVisaDetails.Rows[0]["Gender"].ToString().Replace("1", "Male").Replace("2", "Female"), "ctl00_cphTransaction_ddlVstSex"));// Present Nationality

        //                    string today = DateTime.Now.Date.AddDays(6).ToShortDateString();
        //                    js.ExecuteScript(SetValueByName("ctl00$cphTransaction$dcTravelOn$Date", GetDateTime(today)));// Travel Date

        //                    js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnPaxAdd').click();");
        //                    try
        //                    {
        //                        var myvar = (string)js.ExecuteScript("var myvar = document.getElementById('ctl00_cphTransaction_lnkNewPax').style.display; return myvar");
        //                        if (myvar == "none")
        //                        {
        //                            AppNo[k] = "";
        //                            Status[k] = ValidateStatus;
        //                            k++;
        //                            bvalid = false;
        //                            break;
        //                        }

        //                    }
        //                    catch
        //                    {

        //                    }
        //                    System.Threading.Thread.Sleep(500);
        //                    if (driver.PageSource.Contains("Your session has been expired"))
        //                    {
        //                        throw new Exception("Your session has been expired");
        //                    }

        //                    if (p <= RefrenceNo.Length - 1)
        //                        js.ExecuteScript("document.getElementById('ctl00_cphTransaction_lnkNewPax').click();");
        //                    p++;
        //                }
        //                catch (Exception ex)
        //                {
        //                    if (ex.Message != "Your session has been expired")
        //                    {
        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        AppNo[k] = "";
        //                        Status[k] = "Posting Fail";
        //                        k++;
        //                    }
        //                    else
        //                    {
        //                        try
        //                        {
        //                            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                            // OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //                        }
        //                        catch
        //                        {

        //                        }
        //                        AppNo[k] = "";
        //                        Status[k] = ex.Message;
        //                        k++; break;
        //                    }

        //                }
        //                //Is = ((ITakesScreenshot)driver).GetScreenshot();
        //                //// OpenQA.Selenium.Support(Snap + "\\" + Ref + "_Basic.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //            }
        //            else
        //            {
        //                AppNo[k] = "";
        //                Status[k] = "Application Not Found";
        //                k++;
        //            }

        //        }
        //        #endregion

        //        #region Document Submission
        //        if (bvalid == true)
        //        {
        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl08$ITchkReqSelect"));

        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl07$ITchkReqSelect"));


        //            js.ExecuteScript(CheckedByName("ctl00$cphTransaction$gvDocuments$ctl06$ITchkReqSelect"));

        //            Is = ((ITakesScreenshot)driver).GetScreenshot();
        //            // OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Save.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //            //js.ExecuteScript("document.getElementById('ctl00_cphTransaction_btnSave').click();");
        //        }

        //        #endregion

        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.Contains("errorMessage"))
        //            Status[0] = "Posting Error!!";
        //        else
        //            Status[0] = ex.Message;

        //        Is = ((ITakesScreenshot)driver).GetScreenshot();
        //        // OpenQA.Selenium.Support(Snap + "\\" + RefrenceNo[0] + "_Error.jpeg", System.Drawing.Imaging.ImageFormat.Png);
        //        driver.Dispose();
        //        CheckIfProcessAlreadyRunning();
        //        return true;
        //    }

        //}
        #endregion

        #region Visa Status Check
        public void StatusByVisa(DataTable dtVisa, string Path, string Snap)
        {

            IWebDriver driver = new PhantomJSDriver(Path); string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";

        }
        public void StatusByApp_No(DataTable dtApplicationn, string Path, string Snap, out string[] ListStatus)
        {
            ListStatus = new string[dtApplicationn.Rows.Count];
            IWebDriver driver = new PhantomJSDriver(Path); string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
            Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot(); int k = 0;
            foreach (DataRow Row in dtApplicationn.Rows)
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver; string OldUrl = driver.Url, NewUrl = driver.Url;
                //js.ExecuteScript(Redirect("http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity"));

                driver.Navigate().GoToUrl("http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity");
                // if (driver.Url == "http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity")
                // {
                while (driver.Url != "http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity") { }

                js = (IJavaScriptExecutor)driver;
                js.ExecuteScript(SubmitQuery(Row["TReference"].ToString().Replace(" ", ""), "3", Row["FirstName"].ToString().Split(' ')[0], Row["Gender"].ToString(), Row["PresentNationality"].ToString(), Row["Birth"].ToString()));
                OldUrl = driver.Url; NewUrl = driver.Url; while (OldUrl == NewUrl) { NewUrl = driver.Url; }

                Is = ((ITakesScreenshot)driver).GetScreenshot();
                try
                {
                    ListStatus[k] = VisaPostingManager.SetStatus(driver.PageSource.ToString());

                    // // OpenQA.Selenium.Support(Snap + "\\" + Row["TReference"].ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Png);
                }
                catch
                {
                }
                k++;

            }


        }

        public static string SetStatus(string Content)
        {
            string Status = "";
            try
            {
                Content = Content.Replace("\r\n", "");
                string[] S = Regex.Split(Content, "FONT-SIZE: 11pt; COLOR: red; FONT-FAMILY: Arial;FONT-WEIGHT: bold");
                S = Regex.Split(S[2], ">");
                S = Regex.Split(S[1], "</font");
                S = Regex.Split(S[0], "'");
                return Status = S[1];
            }
            catch
            {
                return "";
            }


        }

        public static string SubmitQuery(string App_No, string Sevc_Type, string vname, string v_gender, string p_nationality, string vdob)
        {
            string Query = "";
            Query += "document.pform.p_qry_no.value = '" + App_No + "';document.pform.p_qry_type.value ='" + Sevc_Type + "';document.pform.p_firstname.value = '" + vname + "';";

            Query += "document.pform.p_gender.value = '" + v_gender + "';document.pform.p_nationality.value = '" + p_nationality + "';document.pform.p_dob.value = '" + vdob + "'";
            return Query;
        }
        #endregion

        #region Genral Method
        public static string GetElementValue(string Element)
        {
            return "var Value = document.getElementsByName('" + Element + "')[0].defaultValue";
        }

        public static string SelectByValue(string Element, string Value)
        {
            return "var textToFind = '" + Value + "';var dd = document.getElementsByName('" + Element + "')[0];for (var i = 0; i < dd.options.length; i++) {if (dd.options[i].value === textToFind) {dd.selectedIndex = i;break; }}";
        }
        public static string GetStatus(string Content)
        {
            Content = Content.Replace("\r\n", "");
            string Status;
            string[] s = Regex.Split(Content, "</script>");
            string[] Newsplited = Regex.Split(s[2], "<a ");
            if (Newsplited.Length == 5)
            {
                string[] status = Regex.Split(Newsplited[4], ">");
                string[] SplitedStatus = Regex.Split(status[1], "</a");
                status = Regex.Split(SplitedStatus[1], "</a");
                Status = SplitedStatus[0];
            }
            else
            {
                Status = "";
            }

            return Status;
        }
        public static string SetValueByName(string Name, string Value)
        {
            string SetFunction = "";
            if (Value != null)
                SetFunction = "document.getElementsByName('" + Name + "')[0].setAttribute('value', '" + Value + "')";
            else
                SetFunction = "document.getElementsByName('" + Name + "')[0].setAttribute('value', '')";
            return SetFunction;
        }
        public static string CheckedByName(string Name)
        {
            string SetFunction = "";
            SetFunction = "document.getElementsByName('" + Name + "')[0].setAttribute('checked', 'checked')";
            return SetFunction;
        }
        public static string CheckedById(string Name)
        {
            string SetFunction = "";
            SetFunction = "document.getElementsById('" + Name + "').setAttribute('checked', 'checked')";
            return SetFunction;
        }
        public static string Click(string InputValue)
        {
            string SetFunction = "";
            SetFunction = "document.getElementsByName('" + InputValue + "')[0].click()";
            return SetFunction;
        }

        public static string GetOldScript(string Content, out  string User, out string ServiceType, out string Session)
        {
            Content = Content.Replace("\r\n", "");
            User = ""; ServiceType = ""; Session = ""; string SeesionContent = Content;
            //Session Id // 

            string[] listElement = Regex.Split(SeesionContent, "p_session_id");
            listElement = Regex.Split(listElement[1], "\"");
            Session = "WWVM" + listElement[2];

            string[] s = Regex.Split(Content, "</script>");
            s = Regex.Split(s[2], "javascript");
            s = Regex.Split(s[1], ">");
            s = Regex.Split(s[1], "function");
            Content = s[0].Replace(@"\\", "");
            // For User  //
            s = Regex.Split(s[0], ";");
            s = Regex.Split(s[3], "=");
            s = Regex.Split(s[1], "\"");
            // s = Regex.Split(s[1], @"\^");
            User = s[1];
            // Content = User;
            // End User

            //Service Type // 
            Content = Content.Replace("\ttt", "");
            s = Regex.Split(Content, ";"); int j = 1;
            for (int i = 3; i < s.Length; i++)
            {
                if (s[i].Contains("str[\"0\"][\"" + j + "\"]"))
                {
                    ServiceType += s[i] + ";";
                    j++;
                }

            }
            s = Regex.Split(ServiceType, ";"); string Types; string[] ListService; ServiceType = "";
            foreach (string Type in s)
            {
                if (Type == "")
                    continue;
                ListService = Regex.Split(Type, "=");
                ListService = Regex.Split(ListService[1], "\"");
                ServiceType += ListService[1] + ";";
            }


            // End Service Type //

            return Content;
        }
        public static string ExcecuteScript(string OldScript, string ddName)
        {
            string NewScipt = "";
            string[] s = Regex.Split(OldScript, @"\^");
            NewScipt = "var select = document.getElementsByName('" + ddName + "')[0];var el = document.createElement('option'); el.textContent = '" + s[1] + "';el.value = '" + s[0] + "';(document.getElementsByName('" + ddName + "')[0]).appendChild(el)";
            return NewScipt;

        }
        public static string SelectDropDown(string Value, string dropDownName)
        {
            string NewScipt = "";
            NewScipt += "var select = document.getElementById('" + dropDownName + "');" +
        "for(var i = 0;i < select.options.length;i++){" +
         "   if(select.options[i].value == '" + Value + "' ){" +
          "      select.options[i].selected = true;" +
         "   }" +
      "  }";
            return NewScipt;

        }
        public static string Submit(string Form, string InputName)
        {
            string Save = "";
            Save += "var form = document.getElementsByName('" + Form + "')[0];";
            Save += "form.submit();";
            return Save;
        }

        public static string Redirect(string Path)
        {
            return "window.location.href='" + Path + "'";
        }
        #endregion


        #region Cozmo Methods


        public static string GetDateTime(string Date)
        {
            string[] Month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            string[] s = Regex.Split(Date, "-");
            if (s.Length == 3)
            {
                Date = s[0] + "-" + Month[Convert.ToInt16(s[1]) - 1] + "-" + s[2];
            }
            return Date;
        }


        public static string GetVisaService(string Iservice)
        {
            if (Iservice == "14 days Service VISA")
                return "";
            else if (Iservice == "30 days Tourist Single Entry")
                return "6500";
            else if (Iservice == "90 days Tourist Single Entry")
                return "6501";
            else
                return "";
        }

        public static string Nationality(string Nation)
        {
            if (Nation == "205")    // India
                return Nation = "85";
            else if (Nation == "243")// Indonasia
                return "86";
            else if (Nation == "217")// Shri Lanka
                return "170";
            else if (Nation == "203")// Pakistan
                return "139";
            else if (Nation == "349")// SOUTH AFRICA
                return "167";
            else if (Nation == "337")// MOZAMBIQUE
                return "127";
            else if (Nation == "237")// PHILIPPINES
                return "3";

            return Nation;
        }


        public static void NoPax(out int Adult, out  int Child, out int Infant, string[] Pax)
        {
            Adult = 0; Child = 0; Infant = 0;
            var Result = Pax.ToList().Where(x => x == "Adult").Count();
            Adult = Result;
            Result = Pax.ToList().Where(x => x == "Child").Count();
            Child = Result;
            Result = Pax.ToList().Where(x => x == "Infant").Count();
            Infant = Result;
        }


        //public static void UploadImages(IWebDriver driver, string RefNo, string FileNo, string VisaNo, out string Status)
        //{
        //    try
        //    {
        //        string Url = "http://czt.dyndns.info:99/cozmovisa/EdocumentUploader.aspx?visaId=" + VisaNo + "&applicantId=" + FileNo + "&refresh=Y";
        //        driver.Navigate().GoToUrl(Url);
        //        IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
        //        IWebElement SelDocument = driver.FindElement(By.Id("lblDocuments"));
        //        SelectElement oSelNationality = new SelectElement(SelDocument);
        //        oSelNationality.SelectByIndex(1);  // Passport First Page
        //        driver.FindElement(By.Name("fupUpload")).SendKeys(FilePath("False", "2", RefNo));

        //        driver.FindElement(By.Name("")).Click();

        //        oSelNationality.SelectByIndex(1);  // Passport First Page
        //        driver.FindElement(By.Name("fupUpload")).SendKeys(FilePath("False", "3", RefNo));
        //        driver.FindElement(By.Name("")).Click();


        //        oSelNationality.SelectByIndex(1);  // Passport First Page
        //        driver.FindElement(By.Name("fupUpload")).SendKeys(FilePath("False", "1", RefNo));
        //        driver.FindElement(By.Name("")).Click();

        //        Status = "Uploded";


        //    }
        //    catch
        //    {
        //        Status = "Uploding Exception!!";
        //    }


        //}


        public static string AddPax()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" function validateDtlAddUpdate() {var action='A';");
            sb.Append(" var msg = '';");
            sb.Append("try {");
            sb.Append("//event.returnValue = false;");
            sb.Append("//alert(getElement('hdfBasePspExpiry').value)");
            sb.Append("var basePspExpiry = new Date(getElement('hdfBasePspExpiry').value);");
            sb.Append("");
            sb.Append("if (!getElement('rdblRevisitYes').checked && !getElement('rdblRevisitNo').checked) msg += 'Please Check the revisit Status ! \\n';");
            sb.Append("if (getElement('ddlVisaFeeCode').selectedIndex <= 0) msg += 'Please select Visa Fee Code from the List! \\n';");
            sb.Append("if (getElement('hdfVisaActivity').value == 'SYRWOE2' && getElement('hdfVisaEntryNoStatus').value != '1') msg += 'Please Copy from Visa File No! \\n';");
            sb.Append("if (getElement('txtGurantorName').value == '' || getElement('txtGurantorName').value == 'Guarantor Name') msg += 'Guarantor Name cannot be blank! \\n';");
            sb.Append("if (getElement('txtGrntorPassport').value == '' || getElement('txtGrntorPassport').value == 'Guarantors Passport no') msg += 'Guarantor Passport No cannot be blank! \\n';");
            sb.Append("if (getElement('ddlGurantorMobCode').selectedIndex <= 0) msg += 'Please select Mobile  Provider! \\n';");
            sb.Append("if (getElement('txtGurantorMobNo').value == '' || getElement('txtGurantorMobNo').value == 'Mobile no.') msg += 'Guarantor Mobile # cannot be blank! \\n';");
            sb.Append("            else if (getElement('txtGurantorMobNo').value.length != getElement('hdfMaxLenGrntrMobNo').value) msg += 'Guarantor Mobile # is incorrect !\\n';");
            sb.Append("            if (getElement('txtGurantorEmail').value == '') msg += 'Guarantor Email # cannot be blank! \\n';");
            sb.Append("            else if (!checkEmail(getElement('txtGurantorEmail').value)) msg += 'Guarantor Email is not valid! \\n';");
            sb.Append("            if (getElement('hdfVisaActivity').value == 'VISANO' && getElement('rdblRevisitYes').checked) msg += 'Revisit Status should be No, for the Extension !\\n';");
            sb.Append("            var rowId = getElement('hdfDetailRowId').value;");
            sb.Append("            var adults = parseInt(getElement('ddlAdults').value);");
            sb.Append("            var child = parseInt(getElement('ddlChildren').value);");
            sb.Append("            var infant = parseInt(getElement('ddlInfants').value);");
            sb.Append("            var adults = parseInt(getElement('ddlAdults').value);");
            sb.Append("            var child = parseInt(getElement('ddlChildren').value);");
            sb.Append("            var infant = parseInt(getElement('ddlInfants').value);");
            sb.Append("            var paxCount = parseInt(getElement('hdfVisaDetailsCount').value);");
            sb.Append("            //alert('adults: '+adults+',count:'+paxCount);");
            sb.Append("            if (paxCount >= (adults + child + infant) && getElement('hdfVisaDetailsMode').value == '0') {");
            sb.Append("                msg += 'Pax count should not exceed no.of pax selected !\\n';");
            sb.Append("            }");
            sb.Append("            else {");
            sb.Append("                var pspExpOn = GetDateObject('ctl00_cphTransaction_dcPaxPspExpiryOn');");
            sb.Append("                if (getElement('txtPaxName').value == '' || getElement('txtPaxName').value == 'Visitor Name') msg += 'Visitor Name cannot be blank !\\n';");
            sb.Append("                if (getElement('ddlPaxType').selectedIndex <= 0) msg += 'Please Select Pax Type from the list !\\n';");
            sb.Append("                if (getElement('txtPaxPassport').value == '' || getElement('txtPaxPassport').value == 'Passport No') msg += 'Passport # cannot be blank !\\n';");
            sb.Append("                if (getElement('ddlPaxNationality').selectedIndex <= 0) msg += 'Please Select Nationality from the list !\\n';");
            sb.Append("                else {");
            sb.Append("                    getElement('hdfNationalityId').value = getElement('ddlPaxNationality').value;");
            sb.Append("                    var e = document.getElementById(\"ctl00_cphTransaction_ddlPaxNationality\");");
            sb.Append("                    var selectedTiext = e.options[e.selectedIndex].text;");
            sb.Append("                    getElement('hdfNationalityName').value = selectedTiext;                                     ");
            sb.Append("                }");
            sb.Append("                ");
            sb.Append("                if (getElement('ddlVstMaritalStatus').selectedIndex <= 0) msg += 'Please Select Maritial Status from the list !\\n';");
            sb.Append("                if (getElement('txtPaxProfession').value == '' || getElement('txtPaxProfession').value == 'Profession') msg += 'Profession cannot be blank !\\n';");
            sb.Append("                if (getElement('ddlVstSex').selectedIndex <= 0) msg += 'Please Select Gender from the list !\\n';");
            sb.Append("                if (getElement('ddlVstMaritalStatus').value == 'MARRIED')");
            sb.Append("                    if (getElement('txtSpouseName').value == '' || getElement('txtSpouseName').value == 'Spouse Name') msg += 'Spouse Name cannot be blank !\\n';");
            sb.Append("                //if (getElement('hdfVisaActivity').value == 'VISANO' && document.getElementById(rowId + '_FTtxtVisaEntryNo').value == '') msg += 'Visa Entry # cannot be Blank!\\n';");
            sb.Append("                if (getElement('hdfVisaActivity').value == 'VISANO') {");
            sb.Append("                    var EntryDate = GetDateObject('ctl00_cphTransaction_dcLastEntryDate');");
            sb.Append("                    if (document.getElementById('txtPaxVisaEntryNo').value == '') msg += 'Visa Entry # cannot be Blank!\\n';");
            sb.Append("                    if (EntryDate == null) msg += 'Please select Entry Date !\\n';");
            sb.Append("                }");
            sb.Append("                if (getElement('rdblRevisitYes').checked) {");
            sb.Append("                    var LastEntryDate = GetDateObject('ctl00_cphTransaction_dcLastEntryDate');");
            sb.Append("                    if (LastEntryDate == null) msg += 'Please select Entry Date !\\n';");
            sb.Append("                    var LastExitDate = GetDateObject('ctl00_cphTransaction_dcLastExitDate');");
            sb.Append("                    if (LastExitDate == null) msg += 'Please select Last Exit Date !\\n';");
            sb.Append("                    if (LastEntryDate > LastExitDate) msg += 'Last Entry Date should not be later than Last Exit Date !\\n';");
            sb.Append("                }");
            sb.Append("                if (pspExpOn == null) msg += 'Please select Passsport Exp.On !\\n';");
            sb.Append("                else if (pspExpOn < basePspExpiry) msg += 'Passport Exp.On is less than 6 months !\\n';");
            sb.Append("                //Added check of 72 hrs travel date");
            sb.Append("                var currentDate = new Date(getElement('hdfCurrentDate').value);                ");
            sb.Append("                var travelDate = GetDateObject('ctl00_cphTransaction_dcTravelOn');        ");
            sb.Append("//                if (travelDate == null) msg +='Please select Travel Date !\\n';");
            sb.Append("                //                else if (travelDate < currentDate) msg += 'Travel Date should be later than Current Date !\\n';");
            sb.Append("                //India agent travel date is mandatory");
            sb.Append("                if (getElement('hdfTravelOnStatus').value == \"Y\") {");
            sb.Append("                    if (travelDate == null) msg += 'Please select Travel Date !\\n';");
            sb.Append("                    else if (travelDate < currentDate) msg += 'Travel Date should be later than Current Date !\\n';");
            sb.Append("                        else if (getElement('hdfVisaActivity').value != 'XPR' && !getElement('chkUrgentFee').checked)");
            sb.Append("                                if (!validateTravelOn()) msg += 'If Travel date is within 72 hrs,Normal Visa Cannot be Processed.Please apply Urgent or Express Visa !\\n';");
            sb.Append("                            //Travel date validation of 72 hrs check for india agents                                          ");
            sb.Append("                }");
            sb.Append("                if (getElement('chkAllPax').checked) {");
            sb.Append("                    if ((getElement('hdfAllPaxpassport').value != '') && document.getElementById('txtPaxPassport').value.toUpperCase() != getElement('hdfAllPaxpassport').value.toUpperCase()) msg += 'Passport Number should be same for All pax !\\n';");
            sb.Append("                }");
            sb.Append("                if (getElement('hdfVisaDetailsMode').value == '0')//Add");
            sb.Append("                {");
            sb.Append("                    if (validatePaxType(adults, child, infant, getElement('ddlPaxType').value, '', action) != true) msg += 'Pax type is Exceeded !\\n';");
            sb.Append("                }");
            sb.Append("                else {");
            sb.Append("                    if (validatePaxType(adults, child, infant, getElement('ddlPaxType').value, getElement('hdfEditPaxType').value, action) != true) msg += 'Pax type is Exceeded !\\n';");
            sb.Append("                }");
            sb.Append("            }");
            sb.Append("            if (msg != '') {");
            sb.Append("              ");
            sb.Append("                return msg;");
            sb.Append("            }");
            sb.Append("            return msg;");
            sb.Append("        }");
            sb.Append("        catch (e) {");
            sb.Append("            //alert(e.description);");
            sb.Append("            return msg;");
            sb.Append("        }");
            sb.Append("    }");
            return sb.ToString();
        }

        #endregion


        #region Track Visa
        public static DBHelper.DBReturnCode TrackVisa(DataTable dtResult, out string Status, out string LastDate)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            Status = "";
            LastDate = "";
            string pATH = HttpContext.Current.Server.MapPath(@"~\Admin\Browser\ExchangeRate\");
            IWebDriver driver = new PhantomJSDriver(pATH);
            string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
            Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot(); int k = 0;
            driver.Navigate().GoToUrl("http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity");
            while (driver.Url != "http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity") { }
            try
            {
                string ByAppNo = "";
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver; string OldUrl = driver.Url, NewUrl = driver.Url;
                js = (IJavaScriptExecutor)driver;
                string AppNo = dtResult.Rows[0]["TReference"].ToString().Replace(" ", "");
                string VisaNo = GetVisaNo(dtResult.Rows[0]["VisaNo"].ToString());
                if (AppNo != "")
                {
                    ByAppNo = "3";
                }
                else if (VisaNo != "")
                {
                    ByAppNo = "1";
                    AppNo = VisaNo;
                }
                string FirstName = dtResult.Rows[0]["FirstName"].ToString().Split(' ')[0];
                string Gender = dtResult.Rows[0]["Gender"].ToString();
                string PresentNationality = dtResult.Rows[0]["PresentNationality"].ToString();
                string Birth = dtResult.Rows[0]["Birth"].ToString();
                js.ExecuteScript(SubmitQuery(AppNo, "3", FirstName, Gender, PresentNationality, Birth));
                while (!driver.PageSource.Contains("pform")) { }
                driver.FindElement(By.Name("pform")).Submit();
                while (!driver.PageSource.Contains(FirstName)) { }
                if (verify(driver, "/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"))
                {
                    IWebElement IStatus = driver.FindElement(By.XPath("/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"));
                    Status = IStatus.Text;
                    Status = GetCutStatus(Status, out LastDate);
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }

            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION; ;
            }
            driver.Dispose();
            return retCode;
        }
        public static string GetVisaNo(string VisaNo)
        {
            string[] s = Regex.Split(VisaNo, "/");
            string a, b, c;
            if (s.Length == 3)
            {
                if (s[2].Length > 7)
                {

                    var v_fileno3 = s[0]; var v_mesg = "Entry Permit No.";
                    var v_appnum = v_fileno3;
                    var x = s[0];
                    var y = s[1];
                    var z = s[2];
                    y = y.ToString().Substring(s[1].Length - 2);
                    var w = v_appnum.Length;
                    if (w == 7)
                    {
                        z = "0" + z;
                    }
                    else
                    {
                        z = z;
                    }

                    a = z.Substring(0, 2);
                    b = z.Substring(z.Length - 6);
                    return v_appnum = x + a + y + b;

                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }
        static public bool verify(IWebDriver driver, string elementName)
        {
            try
            {
                bool isElementDisplayed = driver.FindElement(By.XPath(elementName)).Displayed;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetCutStatus(String NewStatus, out string LastDate)
        {
            try
            {
                LastDate = "";
                if (NewStatus.Contains(@"(UAE\Date\Time)"))
                {
                    LastDate = NewStatus.Split(' ')[NewStatus.Split(' ').Length - 1];
                    return "Approved";
                }
                else if (NewStatus.Contains("ALLOWED TO REMAIN"))
                {
                    LastDate = NewStatus.Split(' ')[NewStatus.Split(' ').Length - 1];
                    return "Entered in the country";
                }
                else if (NewStatus.Contains("NOT VALID"))
                {
                    return "Left the country";
                }
                else if (NewStatus.Contains("UNABLE TO PROCESS"))
                {
                    return "Application processing error";
                }
                else if (NewStatus.Contains("REQUEST IS REFUSED"))
                {
                    return "Document Required";
                }
                else
                {
                    return "";
                }
                //return arrStatus.All(x => x.EdnrdStatus.Contains(NewStatus)).ToString();
                //return "";
            }
            catch
            {
                LastDate = "";
                return "";
            }
        }
        #endregion


    }
}