﻿using CutAdmin.BL;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class VisaStatusManager
    {
        public static DBHelper.DBReturnCode VisalistByAppList(out DataTable dtResult_AppNo, out DataTable dtResult_VisaNo)
        {
            DataSet dsResult = new DataSet();
            dtResult_AppNo = new DataTable();
            dtResult_VisaNo = new DataTable();
            DBHelper.DBReturnCode retCode;
            retCode = DBHelper.GetDataSet("Proc_tblVisaStatusLoadByApp_Id", out dsResult);
            if (dsResult.Tables[0].Rows.Count != 0)
            {
                dtResult_AppNo = dsResult.Tables[0].Clone();
                dtResult_AppNo = VisaExpiraion(dsResult.Tables[0]);
            }
            if (dsResult.Tables[1].Rows.Count != 0)
            {
                dtResult_VisaNo = dsResult.Tables[1].Clone();
                dtResult_VisaNo = VisaExpiraion(dsResult.Tables[1]);
            }

            return retCode;

        }
        public static DataTable VisaExpiraion(DataTable dtSource)
        {
            DataTable dtVisa = new DataTable();
            dtVisa = dtSource.Clone();
            if (dtSource.Columns.Contains("App_Date") == false)
                dtSource.Columns.Add("App_Date", typeof(DateTime));
            foreach (DataRow Row in dtSource.Rows)
            {
                DateTime dtToDate = ConvertDateTime(Row["AppliedDate"].ToString());
                Row["App_Date"] = dtToDate;
            }
            DataRow[] Rows = null;
            DateTime App_Date = DateTime.ParseExact("01-12-2016", "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Rows = dtSource.Select("(App_Date  >='" + App_Date + "')");
            if (Rows.Length != 0)
            {
                dtVisa = Rows.CopyToDataTable();

            }
            return dtVisa;

        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }


        public void RefreshStatus()
        {
            DataTable dtResult_AppNo = new DataTable(), dtResult_VisaNo = new DataTable();
            DataSet dsResult = new DataSet();
            string[] _ByAppStatus = null, _ByAppLastDate = null, _ByVisaNoStatus = null, _ByVisaNoLastDate = null;
            string Path = @"C:\inetpub\wwwroot\lib";
            string Snap = @"C:\inetpub\wwwroot\VisaImages\StatusSnap";
            try
            {
                VisalistByAppList(out dtResult_AppNo, out dtResult_VisaNo);
                if (dtResult_AppNo.Rows.Count != 0)
                {
                    _ByAppStatus = new string[dtResult_AppNo.Rows.Count]; _ByVisaNoLastDate = new string[dtResult_AppNo.Rows.Count];
                    StatusByApp_No(dtResult_AppNo, Path, Snap, out _ByAppStatus, out _ByAppLastDate);
                    UpdateStatus(dtResult_AppNo, _ByAppStatus, _ByAppLastDate);
                }
                if (dtResult_AppNo.Rows.Count != 0)
                {
                    _ByVisaNoStatus = new string[dtResult_AppNo.Rows.Count]; _ByVisaNoLastDate = new string[dtResult_AppNo.Rows.Count];
                    StatusByVisa(dtResult_VisaNo, Path, Snap, out _ByVisaNoStatus, out _ByVisaNoLastDate);
                    UpdateStatus(dtResult_VisaNo, _ByVisaNoStatus, _ByVisaNoLastDate);
                }
            }
            catch
            {
            }

        }


        #region Update Status
        public void UpdateStatus(DataTable dtResult, string[] Status, string[] LastDate)
        {
            DBHelper.DBReturnCode retCode;
            string Vcode, OldStatus, OldDate; int k = 0, rowsAffected = 0;
            foreach (DataRow Row in dtResult.Rows)
            {
                if (Status[k] == null)
                {
                    k++;
                    continue;
                }
                Vcode = Row["Vcode"].ToString();
                OldStatus = Row["Status"].ToString();
                OldDate = Row["LastDay"].ToString();
                if (OldStatus != Status[k] || OldDate != LastDate[k].Replace("'", ""))
                {
                    SqlParameter[] sqlParams = new SqlParameter[3];
                    sqlParams[0] = new SqlParameter("@Status", Status[k]);
                    sqlParams[1] = new SqlParameter("@Vcode", Vcode);
                    sqlParams[2] = new SqlParameter("@LastDate", LastDate[k].Replace("'", ""));
                    retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByUpdateByGDRF", out rowsAffected, sqlParams);
                    k++;
                }
                else if (OldStatus != Status[k] && (Status[k] == "Application processing error" || Status[k] == "Documents Required"))
                {
                    SqlParameter[] sqlMailParams = new SqlParameter[2];
                    sqlMailParams[0] = new SqlParameter("@Status", Status[k]);
                    sqlMailParams[1] = new SqlParameter("@Vcode", Vcode);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tblVisaDeatilsByUpdateByIEMailStatus", out rowsAffected, sqlMailParams);
                }

                else if (OldStatus == Status[k] && OldStatus == "Approved")
                {
                    k++;
                }

                else
                {
                    k++;
                }
            }
        }

        #region Status Mapping
        public static string GetCutStatus(String NewStatus, out string LastDate)
        {
            try
            {
                LastDate = "";
                if (NewStatus.Contains(@"(UAE\Date\Time)"))
                {
                    LastDate = NewStatus.Split(' ')[NewStatus.Split(' ').Length - 1];
                    return "Approved";
                }
                else if (NewStatus.Contains("ALLOWED TO REMAIN"))
                {
                    LastDate = NewStatus.Split(' ')[NewStatus.Split(' ').Length - 1];
                    return "Entered in the country";
                }
                else if (NewStatus.Contains("NOT VALID"))
                {
                    return "Left the country";
                }
                else if (NewStatus.Contains("UNABLE TO PROCESS"))
                {
                    return "Application processing error";
                }
                else if (NewStatus.Contains("REQUEST IS REFUSED"))
                {
                    return "Document Required";
                }
                else
                {
                    return "";
                }
                //return arrStatus.All(x => x.EdnrdStatus.Contains(NewStatus)).ToString();
                //return "";
            }
            catch
            {
                LastDate = "";
                return "";
            }
        }
        #endregion

        #region Visa No Format
        public string GetVisaNo(string VisaNo)
        {
            string[] s = Regex.Split(VisaNo, "/");
            string a, b, c;
            if (s.Length == 3)
            {
                if (s[0].Length > 7)
                {

                    var v_fileno3 = s[0]; var v_mesg = "Entry Permit No.";
                    var v_appnum = v_fileno3;
                    var x = s[2];
                    var y = s[1];
                    var z = s[0];
                    y = y.ToString().Substring(s[1].Length - 2);
                    var w = v_appnum.Length;
                    if (w == 7)
                    {
                        z = "0" + z;
                    }
                    a = z.Substring(0, 2);
                    b = z.Substring(z.Length - 6);
                    return v_appnum = x + a + y + b;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }

        }
        #endregion

        #region Status By Visa No
        public void StatusByVisa(DataTable dtVisa, string Path, string Snap, out string[] ListStatus, out string[] LastDate)
        {
            CheckIfProcessAlreadyRunning();
            ListStatus = new string[dtVisa.Rows.Count];
            LastDate = new string[dtVisa.Rows.Count];
            //IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
            IWebDriver driver = new PhantomJSDriver(Path);
            string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
            Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot(); int k = 0;
            driver.Navigate().GoToUrl("http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity");
            while (driver.Url != "http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity") { }
            foreach (DataRow Row in dtVisa.Rows)
            {
                try
                {
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver; string OldUrl = driver.Url, NewUrl = driver.Url;
                    js = (IJavaScriptExecutor)driver;
                    string AppNo = GetVisaNo(Row["VisaNo"].ToString().Replace(" ", "")).Replace(" ", "");
                    if (AppNo.Length < 8)
                    {
                        ListStatus[k] = null;
                        k++;
                    }
                    string FirstName = Row["FirstName"].ToString().Split(' ')[0];
                    string Gender = Row["Gender"].ToString().Replace(" ", "1");
                    string PresentNationality = Row["PresentNationality"].ToString();
                    string Birth = Row["Birth"].ToString();
                    RemoteWebDriver r = (RemoteWebDriver)driver;
                    SeleniumManager.SubmitQuery(r, AppNo, "1", FirstName, Gender, PresentNationality, Birth);
                    driver.FindElement(By.Name("pform")).Submit();
                }
                catch
                {
                    k++;
                    continue;
                }
                try
                {
                    if (SeleniumManager.verify(driver, "/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"))
                    {
                        IWebElement Status = driver.FindElement(By.XPath("/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"));
                        ListStatus[k] = Status.Text;
                        ListStatus[k] = GetCutStatus(ListStatus[k], out LastDate[k]);
                    }
                }
                catch
                {

                    //k++;
                    //continue;
                }
                k++;
            }
            CheckIfProcessAlreadyRunning();
        }
        #endregion

        #region Status By App No
        public void StatusByApp_No(DataTable dtApplicationn, string Path, string Snap, out string[] ListStatus, out string[] LastDate)
        {
            CheckIfProcessAlreadyRunning();
            ListStatus = new string[dtApplicationn.Rows.Count];
            LastDate = new string[dtApplicationn.Rows.Count];
            //IWebDriver driver = new PhantomJSDriver(Path);
            //IWebDriver driver = new InternetExplorerDriver(@"E:\Kashif\IE Driver");
            IWebDriver driver = new PhantomJSDriver(Path);
            string root = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile); string downloadPath = root + "\\Downloads";
            Screenshot Is = ((ITakesScreenshot)driver).GetScreenshot(); int k = 0;
            driver.Navigate().GoToUrl("http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity");
            while (driver.Url != "http://ednrd.ae/portal/pls/portal/INIMM_DB.DBPK_VISAVALIDITY.Query_VisaValidity") { }
            foreach (DataRow Row in dtApplicationn.Rows)
            {
                try
                {
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver; string OldUrl = driver.Url, NewUrl = driver.Url;
                    js = (IJavaScriptExecutor)driver;
                    string AppNo = Row["TReference"].ToString().Replace(" ", "");
                    string FirstName = Row["FirstName"].ToString().Split(' ')[0];
                    string Gender = Row["Gender"].ToString();
                    string PresentNationality = Row["PresentNationality"].ToString();
                    string Birth = Row["Birth"].ToString();
                    RemoteWebDriver r = (RemoteWebDriver)driver;
                    SeleniumManager.SubmitQuery(r, AppNo, "3", FirstName, Gender, PresentNationality, Birth);
                    driver.FindElement(By.Name("pform")).Submit();
                    if (SeleniumManager.verify(driver, "/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"))
                    {
                        IWebElement Status = driver.FindElement(By.XPath("/html/body/form[@id='p_form']/table[@class='ClsTable']/tbody/tr[4]/td/table/tbody/tr[24]/td/div[@id='result_block']/table/tbody/tr[3]/td/table/tbody/tr[4]/td"));
                        ListStatus[k] = Status.Text;
                        ListStatus[k] = GetCutStatus(ListStatus[k], out LastDate[k]);
                    }
                }
                catch
                {
                    k++;
                    continue;
                }
                k++;
            }
            CheckIfProcessAlreadyRunning();
        }
        #endregion

        public static bool CheckIfProcessAlreadyRunning()
        {
            foreach (Process proc in Process.GetProcessesByName("phantomjs"))
            {
                proc.Kill();
            }
            return false;
        }
        #endregion
    }
}