﻿using CutAdmin.BL;
using CutAdmin.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class agencyDepositeManager
    {

        public static DBHelper.DBReturnCode GetBankDepositDetail(out DataTable dtResult)
        {
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            SqlParameter[] sqlParams = new SqlParameter[1];
            string Franchisee = objGlobalDefault.Franchisee;
            //sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.ParentId);
            sqlParams[0] = new SqlParameter("@ParentId", objGlobalDefault.sid);
            retCode = DBHelper.GetDataTable("Proc_tbl_BankDepositLoadMasterDataTable", out dtResult, sqlParams);
            return retCode;
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_BankDeposit", out dtResult);
            //return retCode;
        }

        public static DBHelper.DBReturnCode UserLogin(string sUserName, string sPassword)
        {
            HttpContext.Current.Session["LoginUser"] = null;
            DataTable dtResult;
            GlobalDefault objGlobalDefault;
            string sEncryptedPassword = Cryptography.EncryptText(sPassword);
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", sUserName);
            sqlParams[1] = new SqlParameter("@password", sEncryptedPassword);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadByKey", out dtResult, sqlParams);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                objGlobalDefault = new GlobalDefault();
                objGlobalDefault.sid = Convert.ToInt64(dtResult.Rows[0]["sid"]);
                objGlobalDefault.uid = dtResult.Rows[0]["uid"].ToString();
                objGlobalDefault.UserType = dtResult.Rows[0]["UserType"].ToString();
                objGlobalDefault.password = dtResult.Rows[0]["password"].ToString();
                objGlobalDefault.AgencyName = dtResult.Rows[0]["AgencyName"].ToString();
                objGlobalDefault.AgencyLogo = dtResult.Rows[0]["AgencyLogo"].ToString();
                objGlobalDefault.AgencyType = dtResult.Rows[0]["AgencyType"].ToString();
                objGlobalDefault.MerchantID = dtResult.Rows[0]["MerchantID"].ToString();
                objGlobalDefault.ContactPerson = dtResult.Rows[0]["ContactPerson"].ToString();
                objGlobalDefault.Last_Name = dtResult.Rows[0]["Last_Name"].ToString();
                objGlobalDefault.Designation = dtResult.Rows[0]["Designation"].ToString();
                objGlobalDefault.ContactID = Convert.ToInt64(dtResult.Rows[0]["ContactID"]);
                objGlobalDefault.RoleID = Convert.ToInt64(dtResult.Rows[0]["RoleID"]);
                HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
                //objGlobalDefault.Validity = Convert.ToDateTime(dtResult.Rows[0]["Validity"].ToString());
                //objGlobalDefault.dtLastAccess = Convert.ToDateTime(dtResult.Rows[0]["dtLastAccess"].ToString());
                //objGlobalDefault.Remarks = dtResult.Rows[0]["Remarks"].ToString();
                //objGlobalDefault.ValidationCode = dtResult.Rows[0]["ValidationCode"].ToString();
                //objGlobalDefault.LoginFlag = Convert.ToBoolean(dtResult.Rows[0]["LoginFlag"].ToString());
                //objGlobalDefault.RefAgency = dtResult.Rows[0]["RefAgency"].ToString();
                //objGlobalDefault.PANNo = dtResult.Rows[0]["PANNo"].ToString();
                //objGlobalDefault.ServiceTaxNO = dtResult.Rows[0]["ServiceTaxNO"].ToString();
                //objGlobalDefault.Donecarduser = dtResult.Rows[0]["Donecarduser"].ToString();
                //objGlobalDefault.MerchantURL = dtResult.Rows[0]["MerchantURL"].ToString();
                //objGlobalDefault.TDSFlag = Convert.ToBoolean(dtResult.Rows[0]["TDSFlag"].ToString());
                //objGlobalDefault.EnableDeposit = Convert.ToBoolean(dtResult.Rows[0]["EnableDeposit"].ToString());
                //objGlobalDefault.EnableCard = Convert.ToBoolean(dtResult.Rows[0]["EnableCard"].ToString());
                //objGlobalDefault.EnableCheckAccount = Convert.ToBoolean(dtResult.Rows[0]["EnableCheckAccount"].ToString());
                //objGlobalDefault.Agentuniquecode = dtResult.Rows[0]["Agentuniquecode"].ToString();
                //objGlobalDefault.Updatedate = Convert.ToDateTime(dtResult.Rows[0]["Updatedate"].ToString());
                //objGlobalDefault.FailedReports = dtResult.Rows[0]["FailedReports"].ToString();
                //objGlobalDefault.BookingDate = dtResult.Rows[0]["BookingDate"].ToString();
                //objGlobalDefault.Bankdetails = dtResult.Rows[0]["Bankdetails"].ToString();
                //objGlobalDefault.IATANumber = dtResult.Rows[0]["IATANumber"].ToString();
                //objGlobalDefault.DistAgencyName = dtResult.Rows[0]["DistAgencyName"].ToString();
                //objGlobalDefault.agentCategory = dtResult.Rows[0]["agentCategory"].ToString();
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode ApproveDeposit(int uid, decimal dDepositAmount, string sTypeofCash, string sTypeofcheque, string sComment, string sLastUpdatedDate, Int64 sId, out int rows)
        {
            //int isid = 0;
            Int64 sessionsid;
            string sCreditType;
            decimal dCreditAmount = 0;
            decimal dMaxCreditLimit = 0;
            decimal dAvailableCredit = 0;
            string securitycheqamt = "";
            string monthlybusiness = "";
            string securitycheqbank = "";
            string securitycheqdate = "";
            string ExecutiveName = "";
            int MinLimit = 0;
            decimal dAvailableCash = 0;
            decimal dReduce = 0;
            decimal Result = 0;
            if (sTypeofCash == "N/A" && sTypeofcheque != "N/A")
            {
                sCreditType = sTypeofcheque;
            }
            else if (sTypeofCash != "N/A" && sTypeofcheque == "N/A")
            {
                sCreditType = sTypeofCash;
            }
            else
                sCreditType = "OnlineTransfer";
            GlobalDefault sessionobj;
            sessionobj = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            sessionsid = sessionobj.sid;
            SqlParameter[] sqlParams = new SqlParameter[28];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@LimitUpdate", dDepositAmount);
            sqlParams[2] = new SqlParameter("@LastUpdateDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[3] = new SqlParameter("@Comments", sComment);
            sqlParams[4] = new SqlParameter("@UpdateByID", sessionsid);
            sqlParams[5] = new SqlParameter("@ApprovedByID", sessionsid);
            sqlParams[6] = new SqlParameter("@ApproveDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[7] = new SqlParameter("@ApproveFlag", 1);
            sqlParams[8] = new SqlParameter("@CreditType", sCreditType);
            sqlParams[9] = new SqlParameter("@ExecutiveName", ExecutiveName);
            sqlParams[10] = new SqlParameter("@DepositAmount", dDepositAmount);
            sqlParams[11] = new SqlParameter("@CreditAmount", dCreditAmount);
            sqlParams[12] = new SqlParameter("@MaxCreditLimit", dMaxCreditLimit);
            sqlParams[13] = new SqlParameter("@monthlybusiness", monthlybusiness);
            sqlParams[14] = new SqlParameter("@securitycheqamt", securitycheqamt);
            sqlParams[15] = new SqlParameter("@securitycheqbank", securitycheqbank);
            sqlParams[16] = new SqlParameter("@securitycheqdate", securitycheqdate);
            sqlParams[17] = new SqlParameter("@sid", sId);
            sqlParams[18] = new SqlParameter("@AvailableCredit", dDepositAmount);
            sqlParams[19] = new SqlParameter("@MinLimit", MinLimit);
            sqlParams[20] = new SqlParameter("@ActiveFlag", 1);
            sqlParams[21] = new SqlParameter("@StatementBalance", dAvailableCredit);
            sqlParams[22] = new SqlParameter("@AvailableCash", dAvailableCash);
            sqlParams[23] = new SqlParameter("@ReduceAmount", dReduce);
            sqlParams[24] = new SqlParameter("@Result", Result);
            sqlParams[25] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            sqlParams[26] = new SqlParameter("@MaxCreditAmmount", 0);
            sqlParams[27] = new SqlParameter("@OTCActive", 0);

            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimit_LogAddUpdate", out rows, sqlParams);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                using (var DB = new helperDataContext())
                {
                    var SuppDetails = (from obj in DB.tbl_AdminLogins where obj.sid == uid select obj).FirstOrDefault();
                    var sToEmail = SuppDetails.uid;
                    var Message = "";
                    var Subject = "";
                    //string Url = ConfigurationManager.AppSettings["URL"];
                    string Url = "http://hotels.trivo.in/AgencyLogo/SU-029373.jpg";
                    string Logo = HttpContext.Current.Session["logo"].ToString();

                    StringBuilder sb = new StringBuilder();

                    Message = "Your account is credited against your successful payment.<br> Amount of Rs." + dDepositAmount + " added in your account.";
                    Subject = "Account credited";

                    sb.Append("<!DOCTYPE html>");
                    sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                    sb.Append("<head>");
                    sb.Append("<meta charset=\"utf-8\" />");
                    sb.Append("</head>");
                    sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto; border:2px solid gray\">");
                    sb.Append("<div style=\"background-color: #FF9900; height: 5px\">");
                    sb.Append("</div>");
                    sb.Append("<div style=\"height:50px;width:auto\">");
                    sb.Append("    <br />");
                    sb.Append("<img src=\"" + Url + "\" alt=\"\" style=\"padding-left:10px;\" />  ");
                    //sb.Append("    <img src=\"http://www.clickurtrip.co.in/images/logosmal.png\"  style=\"padding-left:10px;\" />");
                    sb.Append("</div>");
                    //sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"padding-left:-2%;width:100%;height:auto\" />");
                    sb.Append("<div>");
                    sb.Append("    <br />");
                    sb.Append("    <br />");
                    sb.Append("<div style=\"margin-left:10%\">");
                    sb.Append("    <p> <span style=\"margin-left:2%;font-weight:400\">Hello " + SuppDetails.AgencyName + ",</span><br /></p>");

                    sb.Append("<p><span style=\"margin-left:2%;font-weight:400\">" + Message + "</span><br /></p>");



                    sb.Append("<p><span style=\"margin-left:2%;font-weight:400\">For any further clarification & query kindly feel free to contact us</span><br /></p>");

                    sb.Append("<span style=\"margin-left:2%\">");
                    sb.Append("<b>Thank You,</b><br />");
                    sb.Append("</span>");
                    sb.Append("<span style=\"margin-left:2%\">");
                    sb.Append("Administrator");
                    sb.Append("</span>");

                    sb.Append("   </div>");
                    sb.Append(" </div>");
                    //sb.Append("<img src=\"http://www.clickurtrip.co.in/images/unnamed.jpg\" style=\"width:100%;height:auto\" />");
                    sb.Append(" <div>");
                    sb.Append("    <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">");
                    sb.Append("       <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">");
                    sb.Append("            <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">clickurhotel.com</div></td>");
                    sb.Append("        </tr>");
                    sb.Append("    </table>");
                    sb.Append(" </div>");
                    sb.Append(" <div style=\"background-color: #FF9900; height: 5px\">");
                    sb.Append(" </div>");
                    sb.Append(" </body>");
                    sb.Append(" </html>");

                    try
                    {
                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                        List<string> DocLinksList = new List<string>();
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();

                        foreach (string mail in sToEmail.Split(','))
                        {
                            if (mail != "")
                            {
                                Email1List.Add(mail, mail);
                            }
                        }
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                        bool reponse = MailManager.SendMail(accessKey, Email1List, Subject, sb.ToString(), from, DocLinksList);
                        //return reponse;
                    }

                    catch
                    {
                        //return false;
                    }
                }
            }



            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{

            //    string sSubject = "Account credited";
            //    string sMessageTitle = "Account credited";
            //    string sMessage = "Your account is credited against your successful payment.<br> Amount of Rs." + dDepositAmount + " added in your account.";
            //    UpdateCreditManager.AccountingEmail(uid, sSubject, sMessageTitle, sMessage);
            //}

            return retCode;
        }

        public static DBHelper.DBReturnCode ValidateLimit(Int64 uid, out DataTable dtResult)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_AdminCreditLimt_Validation", out dtResult, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetLastUpdateCreditDetails(Int64 sid, out DataTable dtResult)
        {

            try
            {
                bool CreditLmit = false;
                DateTime LimitDate = DateTime.Now;
                DateTime Today = DateTime.Now;
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@uid", sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams);
                CreditLmit = Convert.ToBoolean(dtResult.Rows[0]["Credit_Flag"]);
                Int64 limitDays = 0;
                if (CreditLmit == true)
                {
                    LimitDate = ConvertDateTime(dtResult.Rows[0]["Spn_DateLimit"].ToString());
                    limitDays = Convert.ToInt64(dtResult.Rows[0]["Spn_NoDay"].ToString());
                    LimitDate = LimitDate.AddDays(limitDays);
                    if (LimitDate < Today)
                    {
                        retCode = ResetPriviouslimit(sid);
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                        {
                            SqlParameter[] sqlParams1 = new SqlParameter[1];
                            sqlParams1[0] = new SqlParameter("@uid", sid);
                            retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams1);
                        }

                    }
                }
                return retCode;
            }
            catch
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@uid", sid);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminCreditLimitLoadByKey", out dtResult, sqlParams);
                return retCode;
            }

        }

        #region ResetAvaillimit
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        public static DBHelper.DBReturnCode ResetPriviouslimit(Int64 Uid)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            int Rowseffected = 0;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@uid", Uid);
            sqlParams[1] = new SqlParameter("@LastUpdatedDate", DateTime.Now.ToString("dd-mm-yyyy"));
            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitUpdateByCreditLimit", out Rowseffected, sqlParams);
            return retCode;

        }
        #endregion ResetAvaillimit
    }

}