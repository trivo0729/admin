﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for DefaultHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DefaultHandler : System.Web.Services.WebService
    {


        public DefaultHandler()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        [WebMethod(true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            HttpContext.Current.Session["AgentDetailsOnAdmin"] = null;
            DBHelper.DBReturnCode retCode = DefaultManager.UserLogin(sUserName, sPassword);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Session["logo"] = objGlobalDefault.Agentuniquecode + ".jpg";
                Session["UserType"] = objGlobalDefault.UserType;
                AuthorizationManager.GetAgentFormList();
                if (objGlobalDefault.UserType == "SuperAdmin")
                {
                    DataSet ds;
                    float AvailableCedit = objGlobalDefault.AvailableCrdit;
                    bool IsFirstLogIn = objGlobalDefault.IsFirstLogIn;
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                }
                else if (objGlobalDefault.UserType == "Franchisee" && (objGlobalDefault.LoginFlag == true))
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Franchisee\"}";
                }
                else if (((objGlobalDefault.UserType == "Admin") || objGlobalDefault.UserType == "AdminStaff") && (objGlobalDefault.LoginFlag == true))
                {
                    List<Int64> UserID = new List<long> { objGlobalDefault.sid };
                    //CommissionReports.GenrateReports(UserID);
                    CommissionReports.GenrateCommision(AccountManager.GetSupplierByUser());
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Supplier\"}";
                }
                else if ((objGlobalDefault.UserType == "AdminStaff" || objGlobalDefault.UserType == "FranchiseeStaff") && (objGlobalDefault.LoginFlag == true))
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"" + objGlobalDefault.UserType + "\"}";
                }
                else if ((objGlobalDefault.UserType == "Agent" || objGlobalDefault.UserType == "AgentStaff") && (objGlobalDefault.LoginFlag == true))
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"" + objGlobalDefault.UserType + "\"}";
                }
                //else if (objGlobalDefault.UserType == "Sub-Agent" && (objGlobalDefault.LoginFlag == true))
                //{
                //    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Sub-Agent\"}";
                //}
                else if (objGlobalDefault.LoginFlag == false)
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Inactive\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";

            }
            else
            {
                DataTable dt;
                retCode = DefaultManager.GetAgentDetails(sUserName, out dt);
                if (dt.Rows.Count != 0)
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"AddAgent\"}";
                }
                else
                {
                    return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";
                }

            }

        }

        [WebMethod(EnableSession = true)]
        public string Logout()
        {

            Session["LoginUser"] = null;
            // Session["AgentDetailsOnAdmin"] = null;
            //Session.Remove("AGENTUSER");
            Session.Clear();
            Session.Abandon();
            //Admin.sUserName = string.Empty;
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }

        [WebMethod(EnableSession = true)]
        public string CheckSession()
        {
            if (Session["LoginUser"] != null)
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(true)]
        public string sendPassword(string sEmail)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            DataSet dsResult;
            DataTable dtFirstResult;
            DataTable dtSecondResult;
            DBHelper.DBReturnCode retCode = DefaultManager.isAgentValid(sEmail, out dsResult);
            dtFirstResult = dsResult.Tables[0];
            dtSecondResult = dsResult.Tables[1];
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtFirstResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Common.Cryptography.DecryptText(dtFirstResult.Rows[0]["password"].ToString());
                if (EmailManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtFirstResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtFirstResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            if (retCode == DBHelper.DBReturnCode.SUCCESS && dtSecondResult.Rows.Count > 0)
            {
                string sDecryptedPassword = Common.Cryptography.DecryptText(dtSecondResult.Rows[0]["password"].ToString());
                if (EmailManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtSecondResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtSecondResult.Rows[0]["Mobile"].ToString(), sDecryptedPassword) == true)//info@redhillindia.com
                {
                    sJsonString = "{\"retCode\":\"1\",}";
                }
            }
            return sJsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DefaultManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
    }
}
