﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        string json = "";
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string jsonString = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region MailSettings
        [WebMethod(EnableSession = true)]
        public string GetHotelActivityMails(string Type)
        {
            using (var DB = new helperDataContext())
            {
                string jsonString = "";
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    Uid = objGlobalDefault.ParentId;
                }
                //DBHelper.DBReturnCode retcode = VisaManager.GetVisaMails(Activity,Type, out dtResult);

                var HotelMailsList = (from obj in DB.tbl_ActivityMails
                                      where obj.Type == Type && obj.ParentID == 232
                                      select new
                                      {
                                          obj.Activity
                                      }).ToList();

                var MailsList = (from obj in DB.tbl_ActivityMails
                                 where obj.Type == Type && obj.ParentID == Uid
                                 select new
                                 {
                                     obj.sid,
                                     obj.Activity,
                                     //  obj.BCcMail,
                                     obj.CcMail,
                                     obj.Email,
                                     obj.ErroMessage
                                 }).ToList();

                if (HotelMailsList.Count > 0 && HotelMailsList != null)
                {

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = HotelMailsList, MailsList = MailsList });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail, string Message)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefault.sid;
            if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            try
            {
                using (var DB = new helperDataContext())
                {
                    var sActivity = (from obj in DB.tbl_ActivityMails where obj.Activity == Activity && obj.Type == Type && obj.ParentID == Uid select obj).FirstOrDefault();

                    if (sActivity == null)
                    {
                        tbl_ActivityMail activity = new tbl_ActivityMail();
                        activity.Type = "Hotel";
                        activity.Activity = Activity;
                        activity.ParentID = objGlobalDefault.sid;
                        activity.Email = MailsId;
                        activity.CcMail = CcMails;
                        activity.BCcMail = BCcMail;
                        activity.ErroMessage = Message;
                        DB.tbl_ActivityMails.InsertOnSubmit(activity);
                        DB.SubmitChanges();
                    }
                    else
                    {
                        sActivity.Email = MailsId;
                        sActivity.CcMail = CcMails;
                        sActivity.BCcMail = BCcMail;
                        sActivity.ErroMessage = Message;
                        DB.SubmitChanges();
                    }
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
        }
        #endregion

        #region CountryCityCode

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"City\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity1(string country)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
             helperDataContext db = new helperDataContext();
             var CityList = (from obj in db.tbl_HCities where obj.Countryname == country select obj).Distinct().ToList();

            if (CityList.Count > 0)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, CityList = CityList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCityCode(string Description)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCityCode(Description, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityCode\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion CountryCityCode

        #region GetCountryCity
        [WebMethod(EnableSession = true)]
        public string GetCountryCity(string country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";

            try
            {
                using (var DB = new helperDataContext())
                {
                    var City = (from obj in DB.tbl_HCities
                                where obj.Countryname == country

                                select new
                                {
                                    obj.Code,
                                    obj.Description

                                }).Distinct().ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CityList = City });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            return json;
        }
        #endregion

        #region Currency

        [WebMethod(EnableSession = true)]
        public string GetCurrency()
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                var CurrencyList = (from obj in DB.tbl_CommonCurrencies select obj).ToList();
                if (CurrencyList.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, CurrencyList = CurrencyList });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Get Country City Mapping
        [WebMethod(EnableSession = true)]
        public string GetCityCodeMapping(string Countryname, string CityName)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {

                DataSet dsResult;
                DataTable dtHotelBeds, dtDotw, dtExpedia, dtGrn;
                DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCountryCityMapping(Countryname, CityName, out dsResult);
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    dtHotelBeds = dsResult.Tables[0];
                    dtHotelBeds.Columns.Add("Supplier", typeof(String));
                    dtDotw = dsResult.Tables[1];
                    dtDotw.Columns.Add("Supplier", typeof(String));
                    dtExpedia = dsResult.Tables[2];
                    dtExpedia.Columns.Add("Supplier", typeof(String));
                    dtGrn = dsResult.Tables[3];
                    dtGrn.Columns.Add("Supplier", typeof(String));
                    jsonString = "";
                    int i = 0;
                    foreach (DataRow dr in dtHotelBeds.Rows)
                    {
                        dtHotelBeds.Rows[i]["Supplier"] = "HotelBeds";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtDotw.Rows)
                    {
                        dtDotw.Rows[i]["Supplier"] = "Dotw";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtExpedia.Rows)
                    {
                        dtExpedia.Rows[i]["Supplier"] = "Expedia";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtGrn.Rows)
                    {
                        dtGrn.Rows[i]["Supplier"] = "Grn";
                        i++;
                    }
                    // jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Staff\":[" + jsonString.Trim(',') + "]}";
                    //dtHotelBeds.Dispose();
                    List<Dictionary<string, object>> HotelBedsList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> DotwList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> ExpediaList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> GrnList = new List<Dictionary<string, object>>();

                    HotelBedsList = JsonStringManager.ConvertDataTable(dtHotelBeds);
                    DotwList = JsonStringManager.ConvertDataTable(dtDotw);
                    ExpediaList = JsonStringManager.ConvertDataTable(dtExpedia);
                    GrnList = JsonStringManager.ConvertDataTable(dtGrn);
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelBedsList = HotelBedsList, DotwList = DotwList, ExpediaList = ExpediaList, GrnList = GrnList });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception ex)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                string LineNo = ex.StackTrace.ToString();
            }
            return jsonString;

        }
        #endregion Get Country City Mapping

        [WebMethod(true)]
        public string AddMapping(string CityName, string CityCode, string CountryName, string CountryCode, string dotwCityCode, string ExpediaCityCode, string GrnCityCode)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            DBHelper.DBReturnCode retCode = CountryCityCodeManager.AddMapping(CityName, CityCode, CountryName, CountryCode, dotwCityCode, ExpediaCityCode, GrnCityCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedCities()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetMappedCities(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"MappedCitiesList\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #region Suppliers
        [WebMethod(EnableSession = true)]
        public string GetSuppliers()
        {
            using (var DB = new helperDataContext())
            {
                var SupplierList = (from obj in DB.tbl_APIDetails where obj.Hotel == true select obj).ToList();
                if (SupplierList.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, SupplierList = SupplierList });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Cancelation Policies
        [WebMethod(EnableSession = true)]
        public string GetCancelationPolicies()
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                Int64 Id = DataLayer.AccountManager.GetSupplierByUser();
                //var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicies where obj.SupplierID==Id select obj).Skip(1).ToList();
                var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicies where obj.SupplierID == Id select obj).ToList();
                if (CancelationPolicies.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, CancelationPolicies = CancelationPolicies });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region GetCancelation Policies
        [WebMethod(EnableSession = true)]
        public string GetCancelations(Int64 CancelId)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicies where obj.CancelationID == CancelId select obj).FirstOrDefault();
                var arrRelated = new List<tbl_CommonCancelationPolicy>();
                if (CancelationPolicies.PolicyType == "DaysPrior")
                {

                    arrRelated = (from obj in DB.tbl_CommonCancelationPolicies
                                  where obj.PolicyType == "DaysPrior" && obj.DaysPrior < CancelationPolicies.DaysPrior
                                  orderby obj.CancelationPolicy
                                  select obj).ToList();


                }
                else if (CancelationPolicies.PolicyType == "Date")
                {

                    var sDateCancellation = (from obj in DB.tbl_CommonCancelationPolicies
                                             where obj.PolicyType == "Date"
                                             orderby obj.CancelationPolicy
                                             select obj).ToList(); ;
                    foreach (var Cancellation in sDateCancellation)
                    {

                        if (DateTime.ParseExact(Cancellation.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) < DateTime.ParseExact(CancelationPolicies.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture))
                        {
                            arrRelated.Add(Cancellation);
                        }

                    }

                    //arrRelated = sDate.Select(d => d.CancelationID).ToList();
                    //arrRelated = (from obj in DB.tbl_CommonCancelationPolicies
                    //              where obj.PolicyType == "Date" && DateTime.ParseExact(obj.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) < DateTime.ParseExact(CancelationPolicies.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    //              orderby obj.CancelationPolicy
                    //              select obj).ToList();
                }
                else
                {
                    arrRelated = (from obj in DB.tbl_CommonCancelationPolicies
                                  where obj.PolicyType == "None"
                                  orderby obj.CancelationPolicy
                                  select obj).ToList();

                }
                if (arrRelated.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, NewCancelationPolicies = arrRelated });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Meal Plan
        [WebMethod(EnableSession = true)]
        public string GetMealPlans()
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                var MealPlanList = (from obj in DB.tbl_CommonMealPlans select obj).ToList();
                if (MealPlanList.Count > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, MealPlanList = MealPlanList });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region ListtoDataTable
        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public Int64 GetUserID(string UserType)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            Int64 Uid = 0;
            if (UserType == "Supplier")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Admin")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }

            return Uid;

        }

        [WebMethod(EnableSession = true)]
        public string GetALLOffer()
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetUserByLogin();
                var ALLOffer = (from obj in DB.tbl_CommonHotelOffers where obj.SupplierID == Uid select obj).ToList();
                if (ALLOffer.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, ALLOffer = ALLOffer });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #region AgencyDepositeDetails
        [WebMethod(EnableSession = true)]
        public string GetBankDepositDetail()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = agencyDepositeManager.GetBankDepositDetail(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> BankDeposit = new List<Dictionary<string, object>>();
                BankDeposit = JsonStringManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_BankDeposit = BankDeposit });
            }
            else
            {
                return jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        [WebMethod(true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            DBHelper.DBReturnCode retCode = agencyDepositeManager.UserLogin(sUserName, sPassword);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                }
                else if (objGlobalDefault.UserType == "Agent")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Agent\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";

            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string UnApproveDeposit(int uid, Int64 sId)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_BankDeposit List = DB.tbl_BankDeposits.Where(d => d.sId == sId).FirstOrDefault();
                    List.UnApproveFlag = true;
                    DB.SubmitChanges();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string ApproveDeposit(int uid, decimal dDepositAmount, string sTypeofCash, string sTypeofcheque, string sComment, string sLastUpdatedDate, Int64 sId)
        {
            int rows;
            string jsonString = "";
            DataTable dtResult = null;
            DBHelper.DBReturnCode retcode1 = agencyDepositeManager.ValidateLimit(uid, out dtResult);

            DBHelper.DBReturnCode retcode = agencyDepositeManager.ApproveDeposit(uid, dDepositAmount, sTypeofCash, sTypeofcheque, sComment, sLastUpdatedDate, sId, out rows);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion

        #region BankDetails
        [WebMethod(EnableSession = true)]
        public string AddBankDetails(string BankName, string AccountNo, string Branch, string SwiftCode, string Country)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    tbl_BankDetail Add = new tbl_BankDetail();
                    Add.ParentId = objGlobalDefault.sid;
                    Add.BankName = BankName;
                    Add.AccountNo = AccountNo;
                    Add.Branch = Branch;
                    Add.SwiftCode = SwiftCode;
                    Add.Country = Country;
                    DB.tbl_BankDetails.InsertOnSubmit(Add);
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetBankDetails()
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    var List = (from obj in DB.tbl_BankDetails where obj.ParentId == objGlobalDefault.sid select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BankDetails = List });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateBankDetail(string sid, string BankName, string AccountNo, string Branch, string SwiftCode, string Country)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    Int64 ID = Convert.ToInt64(sid);
                    tbl_BankDetail Update = DB.tbl_BankDetails.Single(x => x.sid == ID);
                    Update.BankName = BankName;
                    Update.AccountNo = AccountNo;
                    Update.Branch = Branch;
                    Update.SwiftCode = SwiftCode;
                    Update.Country = Country;
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteBankDetail(string sid)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    Int64 ID = Convert.ToInt64(sid);
                    tbl_BankDetail Delete = DB.tbl_BankDetails.Single(x => x.sid == ID);
                    DB.tbl_BankDetails.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion
    }
}

