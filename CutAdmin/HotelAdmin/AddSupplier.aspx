﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="AddSupplier.aspx.cs" Inherits="CutAdmin.HotelAdmin.AddSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script>
        $(function () {
            //debugger
            $(document).on({
                ajaxStart: function () {
                    $("#loders").css("display", "")
                }, ajaxStop: function () { $("#loders").css("display", "none") }
            });
        })
    </script>
    <script src="Scripts/AddSupplier.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Supplier</h1>
        </hgroup>
        <div class="with-padding">
            <form action="#" class="addagent">
                <h4>Personal details</h4>
                <hr>
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>First Name </label><span class="red">*</span><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_FirstName" value="" class="input-unstyled full-width" placeholder="First Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Last Name</label><span class="red">*</span><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_LastName" value="" class="input-unstyled full-width" placeholder="Last Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Designation</label><span class="red">*</span><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Designation" value="" class="input-unstyled full-width" placeholder="Designation" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet bold">
                        <label>Company Name</label><span class="red">*</span>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_AgencyName" value="" class="input-unstyled full-width" placeholder="Agency Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet	 bold">
                        <label>Company mail (Agency Mail)</label><span class="red">*</span>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Email" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Address</label><span class="red">*</span>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Address" value="" class="input-unstyled full-width" placeholder="Address" type="text">
                        </div>
                    </div>
                </div>

                <h4>Other Details</h4>
                <hr>
                <div class="columns">
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Country</label><span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="DivCountry">
                            <select id="selCountry" class="select country">
                                <option selected="selected" value="-">Select Any Country</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>City</label><span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="City">
                            <select id="selCity" class="select City">
                                <option value="-">-Select Any City-</option>
                            </select>
                            <%--<span class="select-value">-Select Any City-</span>
                                <span class="select-arrow"></span>
                                <span class="drop-down"></span>
                            </span>--%>
                        </div>
                    </div>

                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>IATA STATUS</label><span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="Div_IATA">
                            <select id="Select_IATA" name="validation-select" class="select" tabindex="-1">
                                <option selected="selected" value="0">Select Status</option>
                                <option value="1">Approved</option>
                                <option value="2">UnApproved</option>
                            </select>
                        </div>
                    </div>

                    <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                        <label>User type</label><span class="red">*</span>
                        <br>
                        <div class="full-width button-height" id="UserType">
                            <select id="Select_Usertype" class="select UsrTyp">
                                <option selected="selected" value="0">Select User Type</option>
                                <option value="Agent">Agent</option>
                                <option value="Corporate" id="Corporate">Corporate</option>
                                <option value="Franchisee" id="Franchisee">Franchisee</option>
                                <option value="Supplier" id="Supplier">Supplier</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="columns">
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Pin Code</label><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_PinCode" value="" class="input-unstyled full-width" placeholder="Pin Code" type="text">
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>IATA No</label>
                        <br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_IATA" value="" class="input-unstyled full-width" placeholder="IATA No" type="text">
                        </div>
                    </div>

                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Phone</label>
                        <br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Phone" value="" class="input-unstyled full-width" placeholder="Phone" type="text">
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Mobile</label><span class="red">*</span><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Mobile" value="" class="input-unstyled full-width" placeholder="Mobile" type="text">
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Fax</label>
                        <br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Fax" value="" class="input-unstyled full-width" placeholder="Fax" type="text">
                        </div>
                    </div>

                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>PAN No</label><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_PAN" value="" class="input-unstyled full-width" placeholder="PAN No" type="text">
                        </div>
                    </div>

                </div>

                <div class="columns">
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Service Tax Number</label><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_ReferenceNumber" value="" class="input-unstyled full-width" placeholder="Service Tax Number " type="text">
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Website URL</label><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Website" value="" class="input-unstyled full-width" placeholder="Website URL" type="text">
                        </div>
                    </div>

                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Group Markup</label>
                        <br>
                        <div class="full-width button-height">
                            <select id="selGroup" name="validation-select" class="select" tabindex="-1">
                                <option selected="selected" value="-">Select Any Group</option>
                            </select>
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Preferred Currency</label>
                        <br>
                        <div class="full-width button-height" id="Currency">
                            <select id="SelCurrency" name="validation-select currency" class="select" tabindex="-1">
                                <option selected="selected" value="0">Select Currency</option>
                                <option value="AED">AED</option>
                                <option value="INR">INR</option>
                                <option value="USD">USD</option>
                                <option value="PKR">PKR</option>
                                <option value="GBP">GBP</option>
                                <option value="EUR">EUR</option>
                                <option value="SAR">SAR</option>
                            </select>
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>Remarks</label><br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Remarks" value="" class="input-unstyled full-width" placeholder="Remarks" type="text">
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <label>GST NO</label>
                        <br>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_GSTNO" value="" class="input-unstyled full-width" placeholder="GST NO" type="text">
                        </div>
                    </div>
                </div>

                <div class="columns">
                </div>
                <p class="text-right">
                    <%--<button type="button" class="button anthracite-gradient UpdateMarkup">Register</button>--%>
                    <input id="btn_RegisterSupplier" type="button" value="Register" class="button anthracite-gradient UpdateMarkup" style="width: 6%;" onclick="ValidateLogin();" title="Submit Details" />
                    <a class="button" href="SupplierDetails.aspx">Back</a>
                </p>
            </form>
        </div>
    </section>
</asp:Content>
