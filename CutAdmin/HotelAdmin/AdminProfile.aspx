﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="AdminProfile.aspx.cs" Inherits="CutAdmin.HotelAdmin.AdminProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/AdminProfile.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <div class="with-padding">
            <hgroup id="main-title" class="thin">
                <h1 style="margin-left: -12px">Admin Profile</h1>
            </hgroup>
            <div class="with-padding">
                <form action="#">
                    <div class="columns">
                        <h4>Personal details</h4>
                        <hr>
                        <div class="three-columns twelve-columns-mobile six-columns-tablet">
                            <label>First Name</label>
                            <div class="input full-width">
                                <input type="text" id="txtFirstName" value="" class="input-unstyled full-width" placeholder="First Name" />
                            </div>
                        </div>

                        <div class="three-columns twelve-columns-mobile six-columns-tablet">
                            <label>Last Name</label>
                            <div class="input full-width">
                                <input type="text" id="txtLastName" value="" class="input-unstyled full-width" placeholder="Last Name" />
                            </div>
                        </div>

                        <div class="three-columns twelve-columns-mobile six-columns-tablet">
                            <label>Designation</label>
                            <div class="input full-width">
                                <input type="text" id="txtDesignation" value="" class="input-unstyled full-width" placeholder="Designation" />
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile six-columns-tablet">
                            <label>Mobile</label>
                            <div class="input full-width">
                                <input type="text" id="txtMobile" value="" class="input-unstyled full-width" placeholder="Mobile" />
                            </div>
                        </div>

                    </div>

                    <h4>Other Details</h4>
                    <hr>
                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Company Name</label>
                            <div class="input full-width">
                                <input type="text" id="txtCompanyName" value="" class="input-unstyled full-width" placeholder="Company Name" />
                            </div>
                        </div>

                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Company Mail</label>
                            <div class="input full-width">
                                <input type="text" id="txtCompanymail" value="" class="input-unstyled full-width" placeholder="Company Mail" />
                            </div>
                        </div>

                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Phone</label>
                            <div class="input full-width">
                                <input type="text" id="txtPhone" value="" class="input-unstyled full-width" placeholder="Phone" />
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="twelve-columns twelve-columns-mobile twelve-columns-tablet">
                            <label>Address</label>
                            <div class="input full-width">
                                <input type="text" id="txtAddress" value="" class="input-unstyled full-width" placeholder="Address" />
                            </div>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Nationality</label>
                            <div class="full-width button-height" id="drp_Nationality">
                                <select id="sel_Nationality" class="select full-width replacement select-styled-list tracked OfferType">
                                </select>
                            </div>
                        </div>
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>City</label>
                            <div class="full-width button-height" id="drp_City">
                                <select id="sel_City" class="select full-width replacement select-styled-list tracked OfferType">
                                </select>
                            </div>
                        </div>
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Pin Code</label>
                            <div class="input full-width">
                                <input type="text" id="txtPinCode" value="" class="input-unstyled full-width" placeholder="Pin Code" />
                            </div>
                        </div>
                    </div>

                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Pan No</label>
                            <div class="input full-width">
                                <input type="text" id="txtPanNo" value="" class="input-unstyled full-width" placeholder="Pan No" />
                            </div>
                        </div>
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Fax No</label>
                            <div class="input full-width">
                                <input type="text" id="txtFaxNo" value="" class="input-unstyled full-width" placeholder="Fax No" />
                            </div>
                        </div>
                        <div class="four-columns twelve-columns-mobile six-columns-tablet">
                            <label>Logo</label><br />
                            <input id="Logo" class="btn-search5 input" type="file" />
                            <br />
                        </div>
                    </div>
                    <div class="columns">
                        <%--<a class="button blue-gradient" onclick="Save()" style="float: right;">Update</a>--%>
                        <p class="text-right">
                            <input type="button" value="Update" class="button anthracite-gradient UpdateMarkup" style="width: 4%" onclick="Save()">
                        </p>
                    </div>
                </form>
            </div>

            <%--</div>--%>
        </div>

    </section>
</asp:Content>
