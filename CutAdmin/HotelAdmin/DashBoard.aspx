﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="CutAdmin.HotelAdmin.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/Dashboard.js?v=1.9"></script>
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Dashboard</h1>
        </hgroup>
        <div class="dashboard">
            <div class="columns">
                <div class="three-columns twelve-columns-mobile">
                </div>

                <div class="six-columns twelve-columns-mobile">
                </div>

                <div class="three-columns twelve-columns-mobile new-row-mobile">
                    <ul class="stats split-on-mobile">
                        <li>
                           <a href="AgentDetails.aspx" <strong>
                                <lable id="Agents"></lable>
                            </strong>
                            <br>
                            Suppliers
                        </li>
                        <li>
                            <strong>
                                <lable id="hotel_Count"></lable>
                            </strong>
                            <br>
                            Commisson Reports 
                        </li>
                    </ul>
                </div>

            </div>

        </div>

        <div class="with-padding">

            <div class="columns">
                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom">

                        <div class="block-title">
                            <h3><i class="icon-users icon-size2"></i>Group Request</h3>
                        </div>
                        <ul class="events">
                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Pending</span>
                                <a href="#" class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </a>
                            </li>
                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Confirmed</span>
                                <span class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </span>
                            </li>

                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Rejected</span>
                                <span class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </span>
                            </li>

                        </ul>

                    </div>
                </div>
                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom">

                        <div class="block-title">
                            <h3><i class="icon-pages icon-size2"></i>Reconfirmation</h3>
                        </div>
                        <ul class="events">
                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Pending</span>
                                <a href="#" class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </a>
                            </li>
                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Sent to Hotel</span>
                                <span class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </span>
                            </li>

                            <li>
                                <span class="event-date orange" style="font-size: 12px;">Reconfirmed</span>
                                <span class="event-description">
                                    <br />
                                    <h4>0</h4>
                                </span>
                            </li>

                        </ul>

                    </div>
                </div>
                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="BookingsRequests">
                    </div>
                </div>




                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="HotelListCount">
                    </div>
                </div>
                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">
                    <div class="block large-margin-bottom" id="RoomListCount">
                    </div>
                </div>
                <div class="new-row-mobile four-columns six-columns-tablet twelve-columns-mobile">

                    <div class="block large-margin-bottom" id="BookingOnHold">
                    </div>
                </div>

                <div class="eight-columns  twelve-columns-mobile six-columns-tablet">

                    <table class="calendar fluid large-margin-bottom" style="margin-top:-180px">

                        <!-- Month and scroll arrows -->
                        <caption>
                            <span class="cal-prev">◄</span>
                            <a href="#" class="cal-next">►</a>
                            September 2018
                        </caption>

                        <!-- Days names -->
                        <thead>
                            <tr>
                                <th scope="col">Sun</th>
                                <th scope="col">Mon</th>
                                <th scope="col">Tue</th>
                                <th scope="col">Wed</th>
                                <th scope="col">Thu</th>
                                <th scope="col">Fri</th>
                                <th scope="col">Sat</th>
                            </tr>
                        </thead>

                        <!-- Dates -->
                        <tbody>
                            <tr>
                                <td class="week-end prev-month"><span class="cal-day">30</span></td>
                                <td>
                                    <span class="cal-day">1</span>
                                    <ul class="cal-events">
                                        <li class="to-next-day"></li>
                                    </ul>
                                </td>
                                <td>
                                    <span class="cal-day">2</span>
                                    <ul class="cal-events">
                                        <li class="to-previous-day to-next-day"></li>
                                    </ul>
                                </td>
                                <td>
                                    <span class="cal-day">3</span>
                                    <ul class="cal-events">
                                        <li class="to-previous-day"></li>
                                    </ul>
                                </td>
                                <td><span class="cal-day">4</span></td>
                                <td>
                                    <span class="cal-day">5</span>
                                    <ul class="cal-events">
                                        <li class="important"></li>
                                    </ul>
                                </td>
                                <td class="week-end"><span class="cal-day">6</span></td>
                            </tr>
                            <tr>
                                <td class="week-end"><span class="cal-day">7</span></td>
                                <td><span class="cal-day">8</span></td>
                                <td class="today">
                                    <span class="cal-day">9</span>
                                    <ul class="cal-events">
                                        <li></li>
                                    </ul>
                                </td>
                                <td>
                                    <span class="cal-day">10</span>
                                    <ul class="cal-events">
                                        <li class="important"></li>
                                    </ul>
                                </td>
                                <td><span class="cal-day">11</span></td>
                                <td><span class="cal-day">12</span></td>
                                <td class="week-end"><span class="cal-day">13</span></td>
                            </tr>
                            <tr>
                                <td class="week-end"><span class="cal-day">14</span></td>
                                <td><span class="cal-day">15</span></td>
                                <td><span class="cal-day">16</span></td>
                                <td><span class="cal-day">17</span></td>
                                <td><span class="cal-day">18</span></td>
                                <td class="selected">
                                    <span class="cal-day">19</span>
                                    <ul class="cal-events">
                                        <li class="important"></li>
                                    </ul>
                                </td>
                                <td class="week-end"><span class="cal-day">20</span></td>
                            </tr>
                            <tr>
                                <td class="week-end">
                                    <span class="cal-day">21</span>
                                    <ul class="cal-events">
                                        <li></li>
                                    </ul>
                                </td>
                                <td><span class="cal-day">22</span></td>
                                <td><span class="cal-day">23</span></td>
                                <td>
                                    <span class="cal-day">24</span>
                                    <ul class="cal-events">
                                        <li></li>
                                        <li class="important"></li>
                                    </ul>
                                </td>
                                <td><span class="cal-day">25</span></td>
                                <td><span class="cal-day">26</span></td>
                                <td class="week-end"><span class="cal-day">27</span></td>
                            </tr>
                            <tr>
                                <td class="week-end"><span class="cal-day">28</span></td>
                                <td><span class="cal-day">29</span></td>
                                <td><span class="cal-day">30</span></td>
                                <td><span class="cal-day">31</span></td>
                                <td class="next-month"><span class="cal-day">1</span></td>
                                <td class="next-month"><span class="cal-day">2</span></td>
                                <td class="week-end next-month"><span class="cal-day">3</span></td>
                            </tr>
                        </tbody>
                        <!-- End dates -->

                    </table>
                </div>



            </div>
        </div>
    </section>
</asp:Content>
