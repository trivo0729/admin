﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;

namespace CutAdmin.HotelAdmin.DataLayer
{
    public class AdminDetailsManager
    {

        public static DBHelper.DBReturnCode AddUpdateAgent(Int64 sid, string sAgencyName, string sEmail, string sFirstName, string sLastName, string sDesignation, string sAddress, string sCity, string nPinCode, string nIATA, string sUsertype, string nPAN, string nPhone, string nMobile, string nFax, string sServiceTaxNumber, string sWebsite, string sGroupId, string sGroupName, string sAgencyLogo, string sRemarks, string hiddenAgentUniqueCode, string hiddenLoginFlag, string sbuttonvalue, string Currency, string FranchiseeId, string GSTNO)
        {
            int rowsAffected = 0;
            string UserCode = "";
            if (sUsertype == "Agent")
            {
                UserCode = "AG-";
            }

            else if (sUsertype == "Supplier")
            {
                UserCode = "SU-";
            }
            else if (sUsertype == "Franchisee")
            {
                UserCode = "FR-";
            }
            else if (sUsertype == "Corporate")
            {
                UserCode = "CO-";
            }
            DataTable dtResult;
            DataSet dsResult;
            string AgencyLogoPath = "";
            string AgencyType = "A";
            string sPassword = GenerateRandomString(8);
            string sEncryptedPassword = Common.Cryptography.EncryptText(sPassword);
            int RoleId;
            string MerchantID = "CUT-" + GenerateRandomString(4);
            DateTime Validity = DateTime.Now.AddYears(1);
            DateTime LastAccess = DateTime.Now;
            string ValidationCode = "VC-" + GenerateRandomNumber();
            string LoginFlag;
            string ReferenceNumber = "";
            string Donecarduser = "";
            int TDSFlag = 0;
            int EnableDeposite = 1;
            int EnableCard = 0;
            int EnableCheckAccount = 1;
            string AgentUniqueCode = UserCode + GenerateRandomString(4);
            DateTime? UpdateDate = null;
            string FailedReports = "";
            string BookingDate = "";
            string Bankdetails = "";
            string DistAgencyName = "";
            string CurrencyCode = "";
            string Referral = "";
            string Groups = sGroupName;//+ "-" + sVisaGroupName;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;
            Int64 nReceivedSid = 0;
            if (sid != 0)
            {
                MerchantID = "";
                ValidationCode = "";
                AgentUniqueCode = hiddenAgentUniqueCode;
                LoginFlag = hiddenLoginFlag;
            }
            else
                LoginFlag = "0";
            if (sUsertype == "Agent")
            {
                RoleId = 4;
                if (objGlobalDefault.UserType == "Franchisee")
                {
                    FranchiseeId = objGlobalDefault.Franchisee;
                }
            }
            else if (sUsertype == "Corporate")
            {
                RoleId = 5;
            }
            else if (sUsertype == "Franchisee" || sUsertype == "Supplier")
            {
                RoleId = 3;
                if (FranchiseeId == "")
                {
                    FranchiseeId = sAgencyName + "-" + GenerateRandomString(4);
                }
            }
            else
                RoleId = 7;
            if (objGlobalDefault.UserType == "AdminStaff" || objGlobalDefault.UserType == "FranchiseeStaff")
            {
                Int64 Uid;
                DataSet dsStaffResult = new DataSet();
                SqlParameter[] sqlParamsUniqueCode = new SqlParameter[1];
                sqlParamsUniqueCode[0] = new SqlParameter("@ParenId", ParentID);
                DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_StaffLoginLoadByParenId", out dsStaffResult, sqlParamsUniqueCode);
                ParentID = Convert.ToInt64(dsStaffResult.Tables[0].Rows[0]["ParentId"]);
                FranchiseeId = Convert.ToString(dsStaffResult.Tables[1].Rows[0]["FranchiseeId"]);
            }
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                var IsDelete = 0;
                SqlParameter[] sqlParams = new SqlParameter[48];
                sqlParams[0] = new SqlParameter("@uid", sEmail);
                sqlParams[1] = new SqlParameter("@UserType", sUsertype);
                sqlParams[2] = new SqlParameter("@password", sEncryptedPassword);
                sqlParams[3] = new SqlParameter("@AgencyName", sAgencyName);
                sqlParams[4] = new SqlParameter("@AgencyLogo", AgencyLogoPath);
                sqlParams[5] = new SqlParameter("@AgencyType", AgencyType);
                sqlParams[6] = new SqlParameter("@MerchantID", MerchantID);
                sqlParams[7] = new SqlParameter("@ContactPerson", sFirstName + " " + sLastName);
                sqlParams[8] = new SqlParameter("@Last_Name", sLastName);
                sqlParams[9] = new SqlParameter("@Designation", sDesignation);
                sqlParams[10] = new SqlParameter("@phone", nPhone);
                sqlParams[11] = new SqlParameter("@Mobile", nMobile);
                sqlParams[12] = new SqlParameter("@Fax", nFax);
                sqlParams[13] = new SqlParameter("@email", sEmail);
                sqlParams[14] = new SqlParameter("@Website", sWebsite);
                sqlParams[15] = new SqlParameter("@Address", sAddress);
                sqlParams[16] = new SqlParameter("@Code", sCity);
                sqlParams[17] = new SqlParameter("@PinCode", nPinCode);
                sqlParams[18] = new SqlParameter("@Validity", Validity);
                sqlParams[19] = new SqlParameter("@dtLastAccess", LastAccess);
                sqlParams[20] = new SqlParameter("@Remarks", sRemarks);
                sqlParams[21] = new SqlParameter("@ValidationCode", ValidationCode);
                sqlParams[22] = new SqlParameter("@LoginFlag", LoginFlag);
                sqlParams[23] = new SqlParameter("@RefAgency", ReferenceNumber);
                sqlParams[24] = new SqlParameter("@PANNo", nPAN);
                sqlParams[25] = new SqlParameter("@ServiceTaxNO", sServiceTaxNumber);
                sqlParams[26] = new SqlParameter("@Donecarduser", Donecarduser);
                sqlParams[27] = new SqlParameter("@MerchantURL", sWebsite);
                sqlParams[28] = new SqlParameter("@TDSFlag", TDSFlag);
                sqlParams[29] = new SqlParameter("@EnableDeposit", EnableDeposite);
                sqlParams[30] = new SqlParameter("@EnableCard", EnableCard);
                sqlParams[31] = new SqlParameter("@EnableCheckAccount", EnableCheckAccount);
                sqlParams[32] = new SqlParameter("@Agentuniquecode", AgentUniqueCode);
                sqlParams[33] = new SqlParameter("@Updatedate", UpdateDate);
                sqlParams[34] = new SqlParameter("@FailedReports", FailedReports);
                sqlParams[35] = new SqlParameter("@BookingDate", BookingDate);
                sqlParams[36] = new SqlParameter("@Bankdetails", Bankdetails);
                sqlParams[37] = new SqlParameter("@IATANumber", nIATA);
                sqlParams[38] = new SqlParameter("@DistAgencyName", DistAgencyName);
                sqlParams[39] = new SqlParameter("@agentCategory", sGroupId);
                sqlParams[40] = new SqlParameter("@sid", sid);
                sqlParams[41] = new SqlParameter("@RoleID", RoleId);
                sqlParams[42] = new SqlParameter("@CurrencyCode", Currency);
                sqlParams[43] = new SqlParameter("@Referral", Referral);
                sqlParams[44] = new SqlParameter("@ParentID", ParentID);
                //sqlParams[45] = new SqlParameter("@Currency", Currency);
                sqlParams[45] = new SqlParameter("@FranchiseeId", FranchiseeId);
                sqlParams[46] = new SqlParameter("@IsDelete", IsDelete);
                sqlParams[47] = new SqlParameter("@GSTNO", GSTNO);
                DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginAddUpdateByUser", out rowsAffected, sqlParams);


                if (retCode == DBHelper.DBReturnCode.SUCCESS || DBHelper.DBReturnCode.SUCCESSNOAFFECT == retCode)
                {
                    try
                    {
                        string UniqueCodeUpdate = "";
                        SqlParameter[] sqlParamsUniqueCode = new SqlParameter[1];
                        sqlParamsUniqueCode[0] = new SqlParameter("@Agentuniquecode", AgentUniqueCode);
                        retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadsid", out dtResult, sqlParamsUniqueCode);
                        nReceivedSid = Convert.ToInt64(dtResult.Rows[0]["sid"]);
                        // GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        Int64 uid = objGlobalDefault.sid;
                        if (sbuttonvalue == "REGISTER")
                        {
                            if (sUsertype == "Agent")
                            {
                                UserCode = "AG-";
                            }
                            else if (sUsertype == "Franchisee")
                            {
                                UserCode = "FR-";
                            }
                            else if (sUsertype == "Supplier")
                            {
                                UserCode = "SU-";
                            }
                            else if (sUsertype == "Corporate")
                            {
                                UserCode = "CO-";
                            }
                            Int64 UniqueCodeLength = nReceivedSid.ToString().Length;

                            if (UniqueCodeLength <= 4)
                            {
                                UniqueCodeUpdate = UserCode + nReceivedSid.ToString("D" + 4);
                            }
                            else
                                UniqueCodeUpdate = UserCode + nReceivedSid.ToString("D" + 6);
                            SqlParameter[] sqlParamsCodeUpdate = new SqlParameter[2];
                            sqlParamsCodeUpdate[0] = new SqlParameter("@sid", nReceivedSid);
                            sqlParamsCodeUpdate[1] = new SqlParameter("@Agentuniquecode", UniqueCodeUpdate);
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminLoginUpdateUniqueCode", out rowsAffected, sqlParamsCodeUpdate);
                            if (retCode != DBHelper.DBReturnCode.SUCCESS)
                            {
                                objTransactionScope.Dispose();
                                return DBHelper.DBReturnCode.EXCEPTION;
                            }

                            //AdmindbHotelhelperDataContext DB = new AdmindbHotelhelperDataContext();

                            //var List = (from obj in DB.tbl_AdminLogins where obj.sid == nReceivedSid select obj).ToList();

                            //if (List.Count!=0)
                            //{
                            //    tbl_AdminLogin Update = DB.tbl_AdminLogins.Single(x => x.sid == nReceivedSid);
                            //    Update.RoleID = 0;
                            //    DB.SubmitChanges();
                            //}
                        }
                        // string GroupSupplier ="HotelBeds";
                        SqlParameter[] sqlParamsGroup = new SqlParameter[3];
                        //sqlParamsGroup[0] = new SqlParameter("@GroupName", sGroupName);
                        //sqlParamsGroup[1] = new SqlParameter("@uid", nReceivedSid);
                        //sqlParamsGroup[2] = new SqlParameter("@UpdateDate", DateTime.Now);
                        //sqlParamsGroup[3] = new SqlParameter("@UpdatedBy", uid);
                        //sqlParamsGroup[4] = new SqlParameter("@GroupId", sGroupId);
                        //sqlParamsGroup[5] = new SqlParameter("@RoleId", RoleId);
                        //sqlParamsGroup[6] = new SqlParameter("@UserType", sUsertype);
                        //sqlParamsGroup[7] = new SqlParameter("@Supplier", GroupSupplier);
                        sqlParamsGroup[0] = new SqlParameter("@AgentId", nReceivedSid);
                        sqlParamsGroup[1] = new SqlParameter("@UpadteBy", uid);
                        sqlParamsGroup[2] = new SqlParameter("@GroupId", sGroupId);
                        if (sbuttonvalue == "REGISTER")
                        {
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingNewAdd", out rowsAffected, sqlParamsGroup);
                        }
                        else
                        {
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingNewUpdate", out rowsAffected, sqlParamsGroup);
                        }
                        //GroupSupplier = "ClickUrTrip";
                        //SqlParameter[] sqlParamsVisaGroup = new SqlParameter[8];
                        //sqlParamsVisaGroup[0] = new SqlParameter("@GroupName", sVisaGroupName);
                        //sqlParamsVisaGroup[1] = new SqlParameter("@uid", nReceivedSid);
                        //sqlParamsVisaGroup[2] = new SqlParameter("@UpdateDate", DateTime.Now);
                        //sqlParamsVisaGroup[3] = new SqlParameter("@UpdatedBy", uid);
                        //sqlParamsVisaGroup[4] = new SqlParameter("@GroupId", sVisaGroupId);
                        //sqlParamsVisaGroup[5] = new SqlParameter("@RoleId", RoleId);
                        //sqlParamsVisaGroup[6] = new SqlParameter("@UserType", sUsertype);
                        //sqlParamsVisaGroup[7] = new SqlParameter("@Supplier", GroupSupplier);
                        //if (sbuttonvalue == "REGISTER")
                        //{
                        //    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingAddVisaGroup", out rowsAffected, sqlParamsVisaGroup);
                        //}
                        //else
                        //{
                        //    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkupGroupMappingUpdate", out rowsAffected, sqlParamsVisaGroup);
                        //}
                        // retCode = HotelMarkupManager.SetGroup(AgentUniqueCode);
                        if (retCode != DBHelper.DBReturnCode.SUCCESS)
                        {
                            objTransactionScope.Dispose();
                            return DBHelper.DBReturnCode.EXCEPTION;
                        }
                        else
                        {
                            if (sbuttonvalue == "REGISTER")
                            {
                                SqlParameter[] sqlParamsCreditLimit = new SqlParameter[2];
                                sqlParamsCreditLimit[0] = new SqlParameter("@uid", nReceivedSid);
                                sqlParamsCreditLimit[1] = new SqlParameter("@LastUpdateDate", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
                                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitAdd", out rowsAffected, sqlParamsCreditLimit);
                                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                {
                                    int id = 0;
                                    SqlParameter[] sqlParamsMrkUpStfDefault = new SqlParameter[8];
                                    sqlParamsMrkUpStfDefault[0] = new SqlParameter("@sid", id);
                                    sqlParamsMrkUpStfDefault[1] = new SqlParameter("@GroupName", "Default");
                                    sqlParamsMrkUpStfDefault[2] = new SqlParameter("@Supplier", "HotelBeds");
                                    sqlParamsMrkUpStfDefault[3] = new SqlParameter("@MarkupPercentage", 0);
                                    sqlParamsMrkUpStfDefault[4] = new SqlParameter("@MarkupAmount", 0);
                                    sqlParamsMrkUpStfDefault[5] = new SqlParameter("@CommisionPercentage", 0);
                                    sqlParamsMrkUpStfDefault[6] = new SqlParameter("@CommisionAmount", 0);
                                    sqlParamsMrkUpStfDefault[7] = new SqlParameter("@ParentId", nReceivedSid);
                                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkUpGroupDetailsStaffAddUpdate", out rowsAffected, sqlParamsMrkUpStfDefault);
                                }
                                else
                                {
                                    objTransactionScope.Dispose();
                                    return DBHelper.DBReturnCode.EXCEPTION;
                                }
                            }
                            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            {
                                decimal MarkupPer = 0;
                                SqlParameter[] sqlParamsMarkupPer = new SqlParameter[2];
                                sqlParamsMarkupPer[0] = new SqlParameter("@uid", nReceivedSid);
                                sqlParamsMarkupPer[1] = new SqlParameter("@MarkupPer", MarkupPer);
                                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_MarkUpAgentAddUpdate", out rowsAffected, sqlParamsMarkupPer);
                                if (retCode == DBHelper.DBReturnCode.SUCCESS && sbuttonvalue == "REGISTER")
                                {
                                    DefaultManager.isAgentValid(sEmail, out dsResult);
                                    dtResult = dsResult.Tables[0];
                                    //if (DefaultManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtResult.Rows[0]["Mobile"].ToString(), sPassword) == false)//info@redhillindia.com
                                    //{
                                    //    objTransactionScope.Dispose();
                                    //    return DBHelper.DBReturnCode.EXCEPTION;
                                    //}
                                    if (EmailManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtResult.Rows[0]["Mobile"].ToString(), sPassword) == false)//info@redhillindia.com
                                    {
                                        objTransactionScope.Dispose();
                                        return DBHelper.DBReturnCode.EXCEPTION;
                                    }
                                    objTransactionScope.Complete();
                                    HttpContext.Current.Session["AgentUniqueCode"] = UniqueCodeUpdate;
                                    EmailManager.SendEmailAdmin(sAgencyName, UniqueCodeUpdate, nMobile, sEmail);
                                }
                                else if (retCode == DBHelper.DBReturnCode.SUCCESS && sbuttonvalue == "UPDATE")
                                {
                                    objTransactionScope.Complete();
                                    HttpContext.Current.Session["AgentUniqueCode"] = AgentUniqueCode;
                                }
                                else
                                {
                                    objTransactionScope.Dispose();
                                    return DBHelper.DBReturnCode.EXCEPTION;
                                }
                            }
                            else
                            {
                                objTransactionScope.Dispose();
                                return DBHelper.DBReturnCode.EXCEPTION;
                            }
                        }
                    }
                    catch
                    {
                        objTransactionScope.Dispose();
                        return DBHelper.DBReturnCode.EXCEPTION;
                    }
                }
                else
                {
                    objTransactionScope.Dispose();
                    return DBHelper.DBReturnCode.EXCEPTION;
                }
                //return retCode;
                return DBHelper.DBReturnCode.SUCCESS;
            }
        }



        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
    }

}