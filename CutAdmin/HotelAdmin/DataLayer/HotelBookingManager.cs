﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.HotelAdmin.DataLayer
{
    public class HotelBookingManager
    {
        public static DBHelper.DBReturnCode BookingList(Int64 Uid, out DataTable dtResult)
        {

            SqlParameter[] SQLParams = new SqlParameter[0];
            //SQLParams[0] = new SqlParameter("@uid", Uid);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadAll", out dtResult, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode CheckAvailableCash(out DataTable dtResult)
        {
            GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 uId = objGolobalDefaults.sid;
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_CheckAvailableCash", out dtResult, SQLParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetBookingDetailByFranchisee(out DataTable dtResult)
        {
            dtResult = null;
            DBHelper.DBReturnCode retCode;
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uId = objGolobalDefaults.sid;
                SqlParameter[] SQLParams = new SqlParameter[1];
                SQLParams[0] = new SqlParameter("@ParentId", uId);
                retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadAllByFranchisee", out dtResult, SQLParams);
                //HttpContext.Current.Session["InvoiceList"] = dtResult;

            }
            else
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode GetBookingDetails(out DataTable dtResult)
        {

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadAll", out dtResult);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetBookingDetailsByAgent(Int64 Uid, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Uid", Uid);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadAllAgent", out dtResult, sqlParams);
            return retCode;
        }


        public static DBHelper.DBReturnCode GetBookingDetailsByDate(string dFrom, string dTo, out DataTable dtResult)
        {

            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sid = objGlobalDefaults.sid;
            DateTime DtFrom = ConvertDateTime(dFrom);
            DateTime DtTo = ConvertDateTime(dTo);
            dFrom = DtFrom.ToString("dd/MM/yyy ", CultureInfo.CurrentCulture);  //this part Add
            dTo = DtTo.ToString("dd/MM/yyy", CultureInfo.CurrentCulture);       // this part Add
            SqlParameter[] sqlParams = new SqlParameter[2];

            sqlParams[0] = new SqlParameter("@dFrom", dFrom);                              //DtFrom.ToString("dd/MM/yyy", System.Globalization.CultureInfo.CurrentCulture));
            sqlParams[1] = new SqlParameter("@dTo", dTo);                                // DtTo.ToString("dd/MM/yyy", System.Globalization.CultureInfo.CurrentCulture));
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadByDateAdmin", out dtResult, sqlParams);
            return retCode;

        }


        public static DBHelper.DBReturnCode GetBookingDetailsAgentByDate(Int64 Uid, string dFrom, string dTo, out DataTable dtResult)
        {


            DateTime DtFrom = ConvertDateTime(dFrom);
            DateTime DtTo = ConvertDateTime(dTo);

            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Uid", Uid);
            sqlParams[1] = new SqlParameter("@dFrom", DtFrom.ToString("dd/MM/yyy", System.Globalization.CultureInfo.CurrentCulture));
            sqlParams[2] = new SqlParameter("@dTo", DtTo.ToString("dd/MM/yyy", System.Globalization.CultureInfo.CurrentCulture));
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadAllAgentByDate", out dtResult, sqlParams);
            return retCode;

        }

        public static DBHelper.DBReturnCode GetPolicyandHotel(string ReservationID, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ReservationId", ReservationID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_GetHotelNameAndCancellationPolicy", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode CancelBooking(Int64 sid, string Remark)
        {
            int rowsAffected = 0;
            string status = "Closed";
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Sid", sid);
            sqlParams[1] = new SqlParameter("@Remarks", Remark);
            sqlParams[2] = new SqlParameter("@Status", status);
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("usp_Proc_tbl_HotelReservationCancellation", out rowsAffected, sqlParams);
            return retCode;
        }



        public static DBHelper.DBReturnCode GetBookingVoucher(out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@ReservationId", ReservationID);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("usp_Proc_tbl_HotelReservationLoadVoucher", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode ConfirmationDetails(string ReservationID, out DataTable dtResult)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_ConfirmationDetails", out dtResult, SQLParams);
            return retCode;
        }


        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }


    }
}