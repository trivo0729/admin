﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.HotelAdmin.DataLayer
{
    public class StaffManager
    {
        public static DBHelper.DBReturnCode AddUpdateStaff(Int64 sid, string sFirstName, string sMiddleName, string sLastName, string sDesignation, string sMobile, string sEmail, string sPhone, string sAddress, string sCode, string sPinCode, string hiddenLoginFlag, string Department)
        {
            int rowsAffected = 0;
            DataTable dtResult;
            DataSet dsResult;
            Int64 nReceivedSid = 0;
            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                SqlParameter[] sqlParams = new SqlParameter[22];
                if (HttpContext.Current.Session["LoginUser"] != null)
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 ParentID = objGlobalDefault.sid;
                    string sUserType = "AdminStaff";
                    if (objGlobalDefault.UserType == "Admin")
                    {
                        sUserType = "AdminStaff";
                    }
                    else
                    {
                        sUserType = "SupplierStaff";
                    }

                    string StaffUniqueCode = "STF-" + GenerateRandomString(4);
                    string LoginFlag;
                    string sPassword = GenerateRandomString(8);
                    string sEncryptedPassword = Common.Cryptography.EncryptText(sPassword);
                    if (sid != 0)
                    {
                        LoginFlag = hiddenLoginFlag;
                    }
                    else
                        LoginFlag = "0";
                    sqlParams[0] = new SqlParameter("@sid", sid);
                    sqlParams[1] = new SqlParameter("@uid", sEmail);
                    sqlParams[2] = new SqlParameter("@UserType", sUserType);
                    sqlParams[3] = new SqlParameter("@password", sEncryptedPassword);
                    //sqlParams[4] = new SqlParameter("@ContactPerson", sFirstName + " " + "" + " " + sLastName);
                    sqlParams[4] = new SqlParameter("@ContactPerson", sFirstName);
                    sqlParams[5] = new SqlParameter("@Last_Name", sLastName);
                    sqlParams[6] = new SqlParameter("@Designation", sDesignation);
                    sqlParams[7] = new SqlParameter("@RoleID", 0);
                    sqlParams[8] = new SqlParameter("@phone", sPhone);
                    sqlParams[9] = new SqlParameter("@Mobile", sMobile);
                    sqlParams[10] = new SqlParameter("@email", sEmail);
                    sqlParams[11] = new SqlParameter("@Address", sAddress);
                    sqlParams[12] = new SqlParameter("@Code", sCode);
                    sqlParams[13] = new SqlParameter("@PinCode", sPinCode);
                    sqlParams[14] = new SqlParameter("@Validity", DateTime.Now.AddYears(1).ToString("yyyy-MM-dd"));
                    sqlParams[15] = new SqlParameter("@dtLastAccess", DateTime.Now.ToString("yyyy-MM-dd"));
                    sqlParams[16] = new SqlParameter("@LoginFlag", LoginFlag);
                    sqlParams[17] = new SqlParameter("@Staffuniquecode", StaffUniqueCode);
                    sqlParams[18] = new SqlParameter("@Updatedate", DateTime.Now.ToString("yyyy-MM-dd"));
                    sqlParams[19] = new SqlParameter("@ParentID", ParentID);
                    sqlParams[20] = new SqlParameter("@StaffCategory", "");
                    sqlParams[21] = new SqlParameter("@Department", Department);
                    DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_StaffLoginAddUpdate", out rowsAffected, sqlParams);
                    //return retCode;
                    if (retCode == DBHelper.DBReturnCode.SUCCESS && sid == 0)
                    {
                        SqlParameter[] sqlParamsUniqueCode = new SqlParameter[1];
                        sqlParamsUniqueCode[0] = new SqlParameter("@StaffUniqueCode", StaffUniqueCode);
                        retCode = DBHelper.GetDataTable("Proc_tbl_StaffLoginLoadsid", out dtResult, sqlParamsUniqueCode);
                        nReceivedSid = Convert.ToInt64(dtResult.Rows[0]["sid"]);
                        Int64 uid = objGlobalDefault.sid;
                        Int64 UniqueCodeLength = nReceivedSid.ToString().Length;
                        string UniqueCodeUpdate;
                        if (UniqueCodeLength <= 4)
                        {
                            UniqueCodeUpdate = "STF-" + nReceivedSid.ToString("D" + 4);
                        }
                        else
                            UniqueCodeUpdate = "STF-" + nReceivedSid.ToString("D" + 6);
                        SqlParameter[] sqlParamsCodeUpdate = new SqlParameter[2];
                        sqlParamsCodeUpdate[0] = new SqlParameter("@sid", nReceivedSid);
                        sqlParamsCodeUpdate[1] = new SqlParameter("@StaffuniqueCode", UniqueCodeUpdate);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_StaffLoginUpdateUniqueCode", out rowsAffected, sqlParamsCodeUpdate);
                        if (retCode != DBHelper.DBReturnCode.SUCCESS)
                        {
                            objTransactionScope.Dispose();
                            return DBHelper.DBReturnCode.EXCEPTION;
                        }

                        //dtResult = new DataSet();
                        DefaultManager.isAgentValid(sEmail, out dsResult);
                        dtResult = dsResult.Tables[1];
                        if (EmailManager.SendEmail(sEmail, "Your Password Detail", "Information Received", dtResult.Rows[0]["ContactPerson"].ToString(), sEmail, dtResult.Rows[0]["Mobile"].ToString(), sPassword) == false)//info@redhillindia.com
                        {
                            objTransactionScope.Dispose();
                            return DBHelper.DBReturnCode.EXCEPTION;
                        }
                    }
                    objTransactionScope.Complete();
                    return DBHelper.DBReturnCode.SUCCESS;
                }
                else
                    return DBHelper.DBReturnCode.EXCEPTION;
            }
        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
    }
}