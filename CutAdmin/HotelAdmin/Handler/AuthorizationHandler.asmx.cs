﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for AuthorizationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AuthorizationHandler : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string CheckUserAuthorities(string sFormName)
        {
            string jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            if (Session["AthorizedFormList"] != null)
            {
                Forms objForms = (Forms)Session["AthorizedFormList"];
                string[] arFormsName = objForms.strAuthorizedFormCollection;
                for (int i = 0; i < arFormsName.Length; i++)
                {
                    if (arFormsName[i] == sFormName)
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetFormList(string sFormName)
        {
            string jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            if (Session["AthorizedFormList"] != null)
            {
                Forms objForms = (Forms)Session["AthorizedFormList"];
                string[] arFormsName = objForms.strAuthorizedFormCollection;
                for (int i = 0; i < arFormsName.Length; i++)
                {
                    if (arFormsName[i] == sFormName)
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string[] GetAuthorizedFormList()
        {

            string[] arFormsName = new string[0];
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Role = (objGlobalDefault.RoleID).ToString();
            if (Role != "1" && Role != "2" && Role != "3" && Role != "0")
            {
                HttpContext.Current.Session["LoginUser"] = null;
                return arFormsName;

            }
            if (Session["AthorizedFormList"] != null)
            {
                Forms objForms = (Forms)Session["AthorizedFormList"];
                arFormsName = objForms.strAuthorizedFormCollection;
                return arFormsName;
            }
            else
                return arFormsName;
        }

        [WebMethod(EnableSession = true)]
        public string CheckFranchisee()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Role = (objGlobalDefault.RoleID).ToString();
            return "{\"Session\":\"1\",\"retCode\":\"1\",\"Role\":\"" + Role + "\"}";
        }


        [WebMethod(EnableSession = true)]
        public string DashBoard()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                string Agents = "";
                string Bookings = "";
                string Visa = "";
                string OTB = "";
                DataTable dtResult;
                DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_DashBoard", out dtResult);

                if (DBHelper.DBReturnCode.SUCCESS == retCode)
                {
                    if (dtResult.Rows.Count > 0)
                    {
                        Agents = dtResult.Rows[0]["Agents"].ToString();
                        Bookings = dtResult.Rows[0]["Bookings"].ToString();
                        Visa = dtResult.Rows[0]["Visa"].ToString();
                        OTB = dtResult.Rows[0]["OTB"].ToString();
                    }
                }

                return "{\"Session\":\"1\",\"retCode\":\"1\",\"Agents\":\"" + Agents + "\",\"Bookings\":\"" + Bookings + "\",\"Visa\":\"" + Visa + "\",\"OTB\":\"" + OTB + "\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }
}
