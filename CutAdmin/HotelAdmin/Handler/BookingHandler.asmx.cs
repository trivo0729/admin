﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
         static helperDataContext db = new helperDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }
        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }
        [WebMethod(EnableSession = true)]
        public string ValidateTransaction(List<Addons> arrAdons)
        {
            jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (Session["RatesDetails"] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                float BaseRate = Supplier[0].Details.Select(d => d.Rate.TotalCharge.TotalRate).ToList().Sum() + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                float AvailableCredit = 0;
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                var dtAvailableCredit = (from obj in db.tbl_AdminCreditLimits where obj.uid == objGlobalDefault.sid select obj).FirstOrDefault();
                if (dtAvailableCredit == null)
                    throw new Exception("Please Contact  Administrator and try Again");
                AvailableCredit = Convert.ToSingle(dtAvailableCredit.AvailableCredit);
                float @Creditlimit = Convert.ToSingle(dtAvailableCredit.CreditAmount);
                bool Credit_Flag = (bool)dtAvailableCredit.Credit_Flag;
                bool OTC = (bool)dtAvailableCredit.OTC;
                float @MAXCreditlimit = Convert.ToSingle(dtAvailableCredit.MaxCreditLimit);
                if (AvailableCredit >= 0 && AvailableCredit >= BaseRate)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @MAXCreditlimit >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit + @Creditlimit) >= BaseRate && @OTC == true && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit) >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }

                #endregion
                if (IsValid)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                else
                    throw new Exception("Your Balance is insufficient to Make this booking");
            }

            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }

    

        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            //int effected = 0;
            //SqlParameter[] sqlParams = new SqlParameter[5];
            //sqlParams[0] = new SqlParameter("@sTo", sTo);
            //sqlParams[1] = new SqlParameter("@sSubject", title);
            //sqlParams[2] = new SqlParameter("@VarBody", sMail);
            //sqlParams[3] = new SqlParameter("@Path", DocPath);
            //sqlParams[4] = new SqlParameter("@Cc", "online@clickurtrip.com");
            //DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);

            List<string> from = new List<string>();
            List<string> DocPathList = new List<string>();
            Dictionary<string, string> Email1List = new Dictionary<string, string>();
            Dictionary<string, string> BccList = new Dictionary<string, string>();
            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));

            foreach (string mail in sTo.Split(','))
            {
                if (mail != "")
                {
                    Email1List.Add(mail, mail);
                }
            }
            foreach (string mail in Cc.Split(';'))
            {
                if (mail != "")
                {
                    BccList.Add(mail, mail);
                }
            }
            foreach (string link in DocPath.Split(';'))
            {
                if (link != "")
                {
                    DocPathList.Add(link);
                }
            }

            //string URL = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
            string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

            MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);


        }

        
        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            try
            {
                DataTable dtResult = new DataTable();
                Session["SesBookingList"] = null;
                var BookingList = (from obj in db.tbl_HotelReservations
                                   join objPass in db.tbl_BookedPassengers on obj.ReservationID equals objPass.ReservationID
                                   join AgName in db.tbl_AdminLogins on obj.Uid equals AgName.sid
                                   where objPass.IsLeading == true
                                   select new
                                   {
                                       obj.Sid,
                                       obj.ReservationID,
                                       obj.ReservationDate,
                                       AgName.AgencyName,
                                       obj.Status,
                                       obj.TotalFare,
                                       obj.TotalRooms,
                                       obj.HotelName,
                                       obj.CheckIn,
                                       obj.CheckOut,
                                       obj.City,
                                       obj.Children,
                                       obj.BookingStatus,
                                       obj.NoOfAdults,
                                       obj.Source,
                                       obj.Uid,
                                       obj.LatitudeMGH,
                                       obj.LongitudeMGH,
                                       ParentID =obj.Uid,
                                       objPass.Name,
                                       objPass.LastName,
                                   }).Distinct().ToList();
                var BookingListNew = BookingList.OrderByDescending(s => s.Sid).ToList();
                var SupplierList = BookingList.Select(z => z.ParentID).Distinct().ToList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingListNew);
                Session["SesBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingListNew, SupplierList = SupplierList });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            try
            {

                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingPending });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingConfirmed });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "Cancelled").ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingRejected });
                }
                else if (status == "OnRequest" && type == "Bookings")
                {
                    var OnHoldBookings = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookings });
                }
                else if (status == "OnRequest" && type == "Requested")
                {
                    var OnHoldRequested = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }
                else if (status == "OnRequest" && type == "Confirmed")
                {
                    var OnHoldConfirmed = CutAdmin.HotelAdmin.DataLayer.BookingList.BookingListNew().Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldConfirmed });
                }

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Search(string Agency, string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string SupplierRef, string Supplier, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SesBookingList"];
            DataRow[] row = null;
            try
            {
                if (Agency != "")
                {
                    row = BookingList.Select("AgencyName like '%" + Agency + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();

                }
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("Name like '%" + Passenger + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                //if (SupplierRef != "")
                //{
                //    row = BookingList.Select("ReservationID like '%" + SupplierRef + "%'");
                //    if (row.Length != 0)
                //        BookingList = row.CopyToDataTable();
                //    else
                //        BookingList.Rows.Clear();
                //}
                if (Supplier != "" && Supplier != "All")
                {
                    row = BookingList.Select("AgencyName ='" + Supplier + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                  //  else
                    //    BookingList.Rows.Clear();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                    //else
                    //    BookingList.Rows.Clear();
                }
                Session["SearchBookingList"] = BookingList;
                List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                BookingList.Dispose();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string BookingCancle(string ReservationID, string Status)
        {
            try
            {

                bool response = InventoryManager.UpdateInvOnCancellation(ReservationID);

                if (response)
                {
                    List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                    using (var db = new dbHotelhelperDataContext())
                    {
                        for (int i = 0; i < Record.Count; i++)
                        {
                            tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                            Recordnew.BookingId = Record[i].BookingId;
                            Recordnew.InvSid = Record[i].InvSid;
                            Recordnew.SupplierId = Record[i].SupplierId;
                            Recordnew.HotelCode = Record[i].HotelCode;
                            Recordnew.RoomType = Record[i].RoomType;
                            Recordnew.RateType = Record[i].RateType;
                            Recordnew.InvType = Record[i].InvType;
                            Recordnew.Date = Record[i].Date;
                            Recordnew.Month = Record[i].Month;
                            Recordnew.Year = Record[i].Year;
                            Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                            Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                            Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                            Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                            Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                            Recordnew.UpdateOn = Record[i].UpdateOn;
                            AddRecord.Add(Recordnew);
                        }
                        db.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
                        db.SubmitChanges();
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        tbl_CommonHotelReservation Update = db.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID && x.Uid == objGlobalDefault.sid);
                        Update.Status = "Canceled";
                        db.SubmitChanges();

                        var CancleDetail = (from obj in db.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                        var BookingDetail = (from obj in db.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).ToList();
                        var Cancle = CancleDetail[0].CutCancellationDate.Split('|');
                        var Cancleamnt = CancleDetail[0].CancellationAmount.Split('|');
                        List<DateTime> CDate = new List<DateTime>();
                        DateTime CancleDate = DateTime.Now;
                        decimal RefundAmnt;
                        decimal CanclAmnt;
                        decimal Total;
                        //for (int i = 0; i < CancleDetail.Count; i++)
                        //{
                        //    CDate.Add(DateTime.ParseExact(CancleDetail[i].CutCancellationDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
                        //}

                        for (int i = 0; i < Cancle.Length - 1; i++)
                        {

                            if (CancleDate <= DateTime.ParseExact(Cancle[i], "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture))
                            {
                                CanclAmnt = Convert.ToDecimal(Cancleamnt[i]);
                                Total = Convert.ToDecimal(CancleDetail[0].RoomAmount);
                                RefundAmnt = Total - CanclAmnt;
                                break;
                            }
                        }

                        //Email Sending


                        Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                        // GlobalDefault objGlobalDefault = new GlobalDefault();
                        if (HttpContext.Current.Session["LoginUser"] != null)
                        {
                            objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                            // ParentID = objGlobalDefault.sid;
                        }

                        helperDataContext DBa = new helperDataContext();
                        var sReservation = (from obj in db.tbl_CommonHotelReservations
                                            from objAgent in DBa.tbl_AdminLogins
                                            where obj.ReservationID == ReservationID
                                            select new
                                            {
                                                customer_Email = objAgent.uid,
                                                customer_name = obj.AgencyName,
                                                VoucherNo = obj.VoucherID,
                                                Uid = obj.Uid,
                                                Email = obj.Uid,
                                                ContactPerson = objAgent.ContactPerson,
                                                CurrencyCode = objAgent.CurrencyCode,
                                                HotelName = obj.HotelName,
                                                City = obj.City,
                                                checkin = obj.CheckIn,
                                                checkout = obj.CheckOut,
                                                Ammount = obj.TotalFare,
                                                Nights = obj.NoOfDays,
                                                Passenger = obj.bookingname,
                                                InvoiceID = obj.InvoiceID,
                                                VoucherID = obj.VoucherID,
                                                bookingdate = obj.ReservationDate,
                                                Status = obj.Status,
                                            }).FirstOrDefault();

                        //  var sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();

                        var sMail = new tbl_ActivityMail();
                        if (objGlobalDefault.UserType != "SupplierStaff")
                            sMail = (from obj in DBa.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();
                        else
                            sMail = (from obj in DBa.tbl_ActivityMails where obj.ParentID == 232 && obj.Activity.Contains("Booking Cancelled") select obj).FirstOrDefault();

                        string BCcTeamMails = sMail.BCcMail;
                        string CcTeamMail = sMail.CcMail;
                        string Logo = HttpContext.Current.Session["logo"].ToString();
                        string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<!DOCTYPE html>");
                        sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">    ");
                        sb.Append("<head>    ");
                        sb.Append("<meta charset=\"utf-8\" />");
                        sb.Append("</head>");
                        sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">    ");
                        sb.Append("<div style=\"height:50px;width:auto\">    ");
                        sb.Append("<br />");
                        sb.Append("<img src=\"" + Url + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />    ");
                        sb.Append("</div>");
                        sb.Append("<br>");
                        sb.Append("<div style=\"margin-left:10%\">");
                        sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">Dear " + sReservation.ContactPerson + ",</span><br /></p>");
                        sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">We have received your request, your Booking Details.</span><br /></p>       ");
                        sb.Append("<style type=\"text/css\">");
                        sb.Append(".tg  {border-collapse:collapse;border-spacing:0;}");
                        sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                        sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                        sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                        sb.Append(".tg .tg-yw4l{vertical-align:top}");
                        sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>    ");
                        sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                        sb.Append("<tr>");
                        sb.Append("<td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + ReservationID + "</td>  ");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td class=\"tg-9hbo\"><b>Reservation Status</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:" + sReservation.Status + "</td>  ");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Reservation Date</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.bookingdate + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.HotelName + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Location</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.City + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Check-In</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkin + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Check-Out </b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkout + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Rate</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:" + sReservation.CurrencyCode + " " + Convert.ToInt32(sReservation.Ammount) / Convert.ToInt32(sReservation.Nights) + "</td>  ");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Total</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.CurrencyCode + "" + sReservation.Ammount + "</td>  ");
                        sb.Append("</tr>");
                        sb.Append("</table></div>                            ");
                        sb.Append("<p><span style=\\\"margin-left:2%;font-weight:400\\\">For any further clarification & query kindly feel free to contact us</span><br /></p>    ");
                        sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                        sb.Append("<b>Thank You,</b><br />    ");
                        sb.Append("</span>    ");
                        sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                        sb.Append("Administrator    ");
                        sb.Append("</span>                 ");
                        sb.Append("        </div>       ");
                        sb.Append("     <div>    ");
                        sb.Append("        <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">    ");
                        sb.Append("            <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">    ");
                        sb.Append("                <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  Trivo.in</div></td>    ");
                        sb.Append("            </tr>    ");
                        sb.Append("        </table>    ");
                        sb.Append("     </div>    ");
                        sb.Append("     </body>    ");
                        sb.Append("     </html>");

                        string Title = "Cancel Confirmation";
                        SendMail(sReservation.customer_Email, Title, sb.ToString(), "", CcTeamMail);
                    }
                    
               
                }
             

              

               

               

                string Message = EmailManager.GetErrorMessage("Booking Cancelled");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, Message = Message });
            }
            catch
            {
                string Message = EmailManager.GetErrorMessage("Booking Cancelled Error");
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            try
            {
                using (var  DB = new dbHotelhelperDataContext())
                {
                    var Detail = (from obj in DB.tbl_CommonHotelReservations
                                  join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                                  join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                  where objPass.IsLeading == true && obj.ReservationID == ReservationID
                                  select new
                                  {
                                      obj.Sid,
                                      obj.ReservationID,
                                      obj.ReservationDate,
                                      obj.Status,
                                      obj.TotalFare,
                                      obj.TotalRooms,
                                      obj.HotelName,
                                      obj.CheckIn,
                                      obj.CheckOut,
                                      obj.City,
                                      obj.Children,
                                      obj.BookingStatus,
                                      obj.NoOfAdults,
                                      obj.Source,
                                      obj.Uid,
                                      obj.NoOfDays,
                                      objPass.Name,
                                      objPass.LastName,
                                      objPass.RoomNumber,
                                      objroom.CancellationAmount,
                                      objroom.CutCancellationDate
                                  }).ToList().Distinct();
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 UserId = objGlobalDefault.sid;
                tbl_CommonHotelReconfirmationDetail Confirm = new tbl_CommonHotelReconfirmationDetail();
                using (var DB = new dbHotelhelperDataContext())
                {
                    Confirm.UserID = UserId;
                    Confirm.ReservationId = ReservationId;
                    Confirm.ReconfirmDate = ConfirmDate;
                    Confirm.Staff_Name = StaffName;
                    Confirm.ReconfirmThrough = ReconfirmThrough;
                    Confirm.HotelConfirmationNo = HotelConfirmationNo;
                    Confirm.Comment = Comment;
                    DB.tbl_CommonHotelReconfirmationDetails.InsertOnSubmit(Confirm);
                    DB.SubmitChanges();

                    tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
                    Bit.IsConfirm = true;
                    DB.SubmitChanges();

                    ReconfirmationMail(Confirm.sid, HotelName, ReservationId, StaffName, Comment); 
                }
              

                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        public static bool ReconfirmationMail(Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {

                using (var DB = new dbHotelhelperDataContext())
                {
                    using (var db = new helperDataContext())
                    {
                        var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                            from objAgent in db.tbl_AdminLogins
                                            where obj.ReservationID == ReservationId
                                            select new
                                            {
                                                customer_Email = objAgent.uid,
                                                customer_name = obj.AgencyName,
                                                VoucherNo = obj.VoucherID,

                                            }).FirstOrDefault();

                        var sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" select obj).FirstOrDefault();
                        string BCcTeamMails = sMail.BCcMail;
                        string CcTeamMail = sMail.CcMail;


                        StringBuilder sb = new StringBuilder();

                        sb.Append("<html>");
                        sb.Append("<head>");
                        sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                        sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                        sb.Append("<style>");
                        sb.Append("<!--");
                        sb.Append(" /* Font Definitions */");
                        sb.Append(" @font-face");
                        sb.Append("	{font-family:\"Cambria Math\";");
                        sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                        sb.Append("@font-face");
                        sb.Append("	{font-family:Calibri;");
                        sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                        sb.Append(" /* Style Definitions */");
                        sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                        sb.Append("	{margin-top:0cm;");
                        sb.Append("	margin-right:0cm;");
                        sb.Append("	margin-bottom:10.0pt;");
                        sb.Append("	margin-left:0cm;");
                        sb.Append("	line-height:115%;");
                        sb.Append("	font-size:11.0pt;");
                        sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                        sb.Append("a:link, span.MsoHyperlink");
                        sb.Append("	{color:blue;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append("a:visited, span.MsoHyperlinkFollowed");
                        sb.Append("	{color:purple;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append(".MsoPapDefault");
                        sb.Append("	{margin-bottom:10.0pt;");
                        sb.Append("	line-height:115%;}");
                        sb.Append("@page Section1");
                        sb.Append("	{size:595.3pt 841.9pt;");
                        sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                        sb.Append("div.Section1");
                        sb.Append("	{page:Section1;}");
                        sb.Append("-->");
                        sb.Append("</style>");
                        sb.Append("</head>");
                        sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                        sb.Append("<div class=Section1>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                        sb.Append("" + sReservation.customer_name + ",</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                        sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                        sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                        sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                        sb.Append(" style='border-collapse:collapse'>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                        sb.Append("  No.</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                        sb.Append("  Name</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                        sb.Append("  ID</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                        sb.Append("  by</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                        sb.Append("  / Note</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append("</table>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                        sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                        sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                        sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                        sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                        sb.Append("<p class=MsoNormal>&nbsp;</p>");
                        sb.Append("</div>");
                        sb.Append("</body>");
                        sb.Append("</html>");

                        string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                        List<string> attachmentList = new List<string>();

                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Dictionary<string, string> Email2List = new Dictionary<string, string>();
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                        Email1List.Add(sReservation.customer_Email, "");
                        Email1List.Add(BCcTeamMails, "");
                        Email2List.Add(CcTeamMail, "");

                        MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplier()
        {
            try
            {
                using (var db = new helperDataContext())
                {
                    var List = (from obj in db.tbl_AdminLogins
                                where obj.UserType == "Supplier"

                                select new
                                {
                                    obj.sid,
                                    obj.AgencyName
                                }).ToList();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
               
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingListSupplier(string[] SupplierList)
        {
            try
            {
                List<tbl_AdminLogin> ListData = new List<tbl_AdminLogin>();
                using (var DB = new helperDataContext())
                {
                    for (int i = 0; i < SupplierList.Length; i++)
                    {
                        if (SupplierList[i] != null)
                        {
                            var Liist = (from obj in DB.tbl_AdminLogins
                                         where obj.ParentID == Convert.ToInt32(SupplierList[i])

                                         select new
                                         {
                                             obj.sid,
                                             obj.AgencyName
                                         }).ToList();

                            for (int j = 0; j < Liist.Count; j++)
                            {
                                ListData.Add(new tbl_AdminLogin
                                {
                                    sid = Liist[j].sid,
                                    AgencyName = Liist[j].AgencyName
                                });
                            }

                        }

                    }
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListData });
                }
        
                
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }


    }
}
