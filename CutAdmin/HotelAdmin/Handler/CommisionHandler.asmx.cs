﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for CommisionHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CommisionHandler : System.Web.Services.WebService
    {
        string json = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        AdminDBHandlerDataContext db = new AdminDBHandlerDataContext();
        [WebMethod (EnableSession = true)]
        public string SaveCommision(List<Comm_Commission> ListCommision)
        {
            try
            {
                Int64 ParentID = 0;
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "AdminStaff")
                    ParentID = objGlobalDefault.ParentId;
                else
                    ParentID = objGlobalDefault.sid;
                ListCommision.ForEach(d => d.ParentID = ParentID);
                ListCommision.ForEach(d => d.UpdateBy = objGlobalDefault.sid.ToString());
                ListCommision.ForEach(d => d.UpdateDate =DateTime.Now.ToString("dd-MM-yyyy"));
                var arrCommission = (from obj in db.Comm_Commissions where obj.SupplierID == ListCommision.FirstOrDefault().SupplierID select obj).ToList();
                if (arrCommission.Count != 0)
                {
                    db.Comm_Commissions.DeleteAllOnSubmit(arrCommission);
                }
                db.Comm_Commissions.InsertAllOnSubmit(ListCommision);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json; ;

        }

        [WebMethod (EnableSession = true)]
        public string GetCommision(Int64 Supplier)
        {
            try
            {
                var SupplierList = (from obj in db.Comm_Commissions where obj.SupplierID == Supplier select obj).ToList();
                var sCycle = (from obj in DB.tbl_AdminLogins where obj.sid == Supplier select obj).FirstOrDefault();
                return objserialize.Serialize(new { Session = 1, retCode = 1, SupplierList = SupplierList, sCycle = sCycle });

            }
            catch (Exception ex)
            {

                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }

        }

    //    [WebMethod(EnableSession = true)]
    //    //public string _Seatting(Int64 Supplier , bool OnCheckIn, bool OnCancel ,int Cycle )
    //    //{
    //    //    try
    //    //    {
    //    //        var arrDetails = (from obj in DB.tbl_AdminLogins where obj.sid == Supplier select obj).FirstOrDefault();
    //    //        arrDetails.CommisionCycle = Cycle.ToString();
    //    //        arrDetails.CheckInCommission = OnCheckIn;
    //    //        arrDetails.RefundCancel = OnCancel;
    //    //        DB.SubmitChanges();
    //    //        return objserialize.Serialize(new { Session = 1, retCode = 1});

    //    //    }
    //    //    catch (Exception ex)
    //    //    {

    //    //        return objserialize.Serialize(new { Session = 1, retCode = 0 });
    //    //    }

    //    //}
    }
}
