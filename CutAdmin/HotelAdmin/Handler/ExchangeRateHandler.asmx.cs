﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for ExchangeRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ExchangeRateHandler : System.Web.Services.WebService
    {
         static helperDataContext db = new helperDataContext();
        [WebMethod(EnableSession = true)]
        public string GetExchangeLog()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataSet dtResult;
            DBHelper.DBReturnCode retcode = ExchangeRateManager.GeExchangLog(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataTable dtLogs, dtMarkups, dtLastLog;
                dtLogs = dtResult.Tables[0];

                DataView dv = dtResult.Tables[0].DefaultView;
                dv.Sort = "sid asc";
                dtLogs = dv.ToTable();
                dtMarkups = dtResult.Tables[1];
                DataRow[] row = null, Last_row = null;


                /* Last Log */
                dtLastLog = dtResult.Tables[2];
                dtLastLog = dtResult.Tables[2].Rows.Cast<System.Data.DataRow>().Skip((2 - 1) * 6).Take(6).CopyToDataTable();
                dtLastLog = dtLastLog.Select().OrderByDescending(u => u["sid"]).CopyToDataTable();
                dtLastLog.Columns.Add("MarkupAmt", typeof(decimal));
                dtLastLog.Columns.Add("MarkupPer", typeof(decimal));
                dtLastLog.Columns.Add("MarkupId", typeof(Int64));
                foreach (DataRow Row in dtLastLog.Rows)
                {
                    row = dtMarkups.Select("Currency = '" + Row["Currency"].ToString() + "'");
                    if (row.Length != 0)
                    {
                        Row["MarkupAmt"] = row[0]["Amount"];
                        Row["MarkupPer"] = row[0]["Percentage"];
                        Row["MarkupId"] = row[0]["sid"];
                    }
                    else
                    {
                        Row["MarkupAmt"] = 0;
                        Row["MarkupPer"] = 0;
                    }
                }
                List<Dictionary<string, object>> sLastLogs = new List<Dictionary<string, object>>();
                sLastLogs = JsonStringManager.ConvertDataTable(dtLastLog);
                /* End Last Log*/
                dtLogs.Columns.Add("MarkupAmt", typeof(decimal));
                dtLogs.Columns.Add("MarkupPer", typeof(decimal));
                dtLogs.Columns.Add("MarkupId", typeof(Int64));
                foreach (DataRow Row in dtLogs.Rows)
                {
                    row = dtMarkups.Select("Currency = '" + Row["Currency"].ToString() + "'");
                    if (row.Length != 0)
                    {
                        Row["MarkupAmt"] = row[0]["Amount"];
                        Row["MarkupPer"] = row[0]["Percentage"];
                        Row["MarkupId"] = row[0]["sid"];
                    }
                    else
                    {
                        Row["MarkupAmt"] = 0;
                        Row["MarkupPer"] = 0;
                    }
                }
                List<Dictionary<string, object>> sLogs = new List<Dictionary<string, object>>();
                sLogs = JsonStringManager.ConvertDataTable(dtLogs);
                dtLogs.Dispose();
                List<Dictionary<string, object>> sMarkups = new List<Dictionary<string, object>>();
                sMarkups = JsonStringManager.ConvertDataTable(dtMarkups);
                dtMarkups.Dispose();
                jsSerializer.MaxJsonLength = Int32.MaxValue;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, sLogs = sLogs, sMarkups = sMarkups, sLastLogs = sLastLogs });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        #region Search Exchange Rate
        [WebMethod(EnableSession = true)]
        public string SearchExchangeUpdate(string currency, string updatedBy, string updateDate)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            helperDataContext db = new helperDataContext();
            try
            {
                var data = (from obj in db.tbl_ExchangeRateLogs where obj.UpdateBy == updatedBy select obj).ToList();

                List<string> extractDate = new List<string>();

                for (var i = 0; i < data.Count; i++)
                {
                    extractDate.Add(data[i].UpdateDate.ToString().Split(' ')[0]);
                }

                List<object> exchangeRates = new List<object>();
                if (currency != "All Currency")
                {
                    for (var i = 0; i < extractDate.Count; i++)
                    {
                        if (updateDate == extractDate[i] && data[i].Currency == currency)
                        {
                            exchangeRates.Add(data[i]);
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < extractDate.Count; i++)
                    {
                        if (updateDate == extractDate[i])
                        {
                            exchangeRates.Add(data[i]);
                        }
                    }
                }

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, exchangeRate = exchangeRates });

            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #endregion

        #region Add Update
        [WebMethod(EnableSession = true)]
        public string GetExchangeRate(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            decimal[] Amt, TotalExchange;
            ExchangeRateManager.GetMarkupAmt(ExchangeRate, MarkupAmt, MarkupPer, out Amt, out TotalExchange);
            return jsSerializer.Serialize(new { retCode = 1, Session = 1, MarkupAmt = Amt, ExchangeRate = TotalExchange });
        }
        [WebMethod(EnableSession = true)]
        public string UpdateExchangeRate(Int64[] MarkupSid, Int64[] LogSid, string[] Currency, decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = ExchangeRateManager.UpdateExchangeRate(MarkupSid, LogSid, Currency, ExchangeRate, MarkupAmt, MarkupPer);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddExchangeRate(string MarkupSid, string Currency, string ExchangeRate, string MarkupAmt, string MarkupPer)
        {
            string json;
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new helperDataContext())
                {
                    tbl_ExchangeMarkup Add = new tbl_ExchangeMarkup();
                    //Add.Uid = Convert.ToInt64(MarkupSid);
                    Add.Currency = Currency;
                    Add.Preferred = "INR";
                    Add.Amount = Convert.ToDecimal(MarkupAmt);
                    Add.Percentage = Convert.ToDecimal(MarkupPer);
                    db.tbl_ExchangeMarkups.InsertOnSubmit(Add);
                    db.SubmitChanges();
                }
              

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion
            
        #region Update Excnage from Online
        [WebMethod(EnableSession = true)]
        public string GetOnlineRate()
        {
            JavaScriptSerializer ocjserialize = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = ExchangeRateManager.GetExchangeRate();
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                return ocjserialize.Serialize(new { retCode = 1, Session = 1 });
            else
                return ocjserialize.Serialize(new { retCode = 0, Session = 1 });
        }
        #endregion
    }
}
