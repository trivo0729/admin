﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for ExportToExcelHandler
    /// </summary>
    public class ExportToExcelHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            string requestedTable = context.Request["datatable"];
            string City = context.Request["City"];
            string Status = context.Request["Status"];
            string dFrom = context.Request["dFrom"];
            string dTo = context.Request["dTo"];
            string Agent = context.Request["uid"];
            string AgencyText = context.Request.QueryString["AgencyText"];
            string Agency = context.Request.QueryString["Agency"];
            string TransType = context.Request.QueryString["TransType"];
            string Type = context.Request.QueryString["Type"];
            string OnLoad = "True";
            string DocumentType = context.Request.QueryString["Document"];


            #region Agent Details

            if (requestedTable == "AgentDetails")
            {
                DataTable dtResult;
                //DBHelper.DBReturnCode retCode = AdminDetailsManager.GetAgentDetail(out dtResult);
                if (HttpContext.Current.Session["AgencySearch"] == null && HttpContext.Current.Session["AllAgency"] != null)
                {
                    dtResult = (DataTable)HttpContext.Current.Session["AllAgency"];
                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "ContactPerson", "uid", "LoginFlag", "Mobile", "Agentuniquecode", "Description", "Country", "CurrencyCode", "AvailableCredit");
                    ExporttoExcel(dtSelectedColumns, Type);
                    //context.Response.Write("<script language=\"javascript\">window.alert('No Record Found');window.location='AgencyStatement.aspx'</script>");
                }
                else if (HttpContext.Current.Session["AgencySearch"] != null)
                {
                    dtResult = (DataTable)HttpContext.Current.Session["AgencySearch"];
                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "ContactPerson", "uid", "LoginFlag", "Mobile", "Agentuniquecode", "Description", "Country", "CurrencyCode", "AvailableCredit");
                    ExporttoExcel(dtSelectedColumns, Type);
                }

            }
            #endregion Agent Details

            #region Package List

            if (requestedTable == "PackageList")
            {
                DataTable dtResultSession = null;
                if (Type == "All")
                {
                    dtResultSession = (DataTable)HttpContext.Current.Session["dtPackages"];
                }
                else
                {
                    dtResultSession = (DataTable)HttpContext.Current.Session["dtPackagesSearch"];
                }

                DataTable dtResult = dtResultSession;
                DataView myDataView = dtResult.DefaultView;
                // myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "Supplier", "TravelDate", "PaxName", "PackageName", "CatID", "Location", "StartDate", "EndDate", "Status");
                ExportPackage(dtSelectedColumns, DocumentType);

            }

            #endregion Booking List Invoice

            #region Staff Details
            else if (requestedTable == "StaffDetails")
            {
                DataTable dtSession = null;
                dtSession = (DataTable)HttpContext.Current.Session["StafflistSession"];
                DataTable dtResult = dtSession;
                DataView myDataView = dtResult.DefaultView;
                DataTable SorteddtResult = myDataView.ToTable();
                DataTable dtSelectedColumns = dtSession.DefaultView.ToTable(false, "ContactPerson", "uid", "Mobile", "Staffuniquecode", "Designation");
                ExporttoExcelStaff(dtSelectedColumns);
                //DBHelper.DBReturnCode retcode = AdminDetailsManager.GetStaffDetails(out dtResult);
                //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                //{
                //    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ContactPerson", "uid", "Mobile", "Staffuniquecode", "Designation");
                //    ExporttoExcelStaff(dtSelectedColumns);
                //}
                //else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                //{

                //    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ContactPerson", "uid", "Mobile", "Staffuniquecode", "Description", "Countryname");
                //    ExporttoExcelStaff(dtSelectedColumns);

                //    //context.Response.Write("<script language=\"javascript\">window.alert('No Record Found'); window.location='AgencyStatement.aspx';</script>");
                //}


            }
            #endregion Staff Details

            //#region Franchisee Details
            //else if (requestedTable == "FranchiseeDetails")
            //{
            //    DataTable dtResult;

            //    DBHelper.DBReturnCode retcode = FranchiseeManager.GetFranchiseeDetail(out dtResult);
            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "ContactPerson", "uid", "LoginFlag", "Mobile", "Agentuniquecode", "Description", "Countryname");
            //        ExporttoExcelFranchisee(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "ContactPerson", "uid", "LoginFlag", "Mobile", "Agentuniquecode", "Description", "Countryname");
            //        ExporttoExcelFranchisee(dtSelectedColumns);

            //        //context.Response.Write("<script language=\"javascript\">window.alert('No Record Found'); window.location='AgencyStatement.aspx';</script>");
            //    }

            //}

            //#endregion

            #region Agency Statement

            if (requestedTable == "AgencyStatement")
            {
                DataTable dtSession = null;

                if (Type == "All")
                {
                    dtSession = (DataTable)HttpContext.Current.Session["PaggingSession"];
                }
                else
                {
                    dtSession = (DataTable)HttpContext.Current.Session["PaggingSearch"];
                }

                DataTable dtResult = dtSession;
                DataView myDataView = dtResult.DefaultView;
                // myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "TransactionDate", "Particulars", "CreditedAmount", "DebitedAmount");
                ExporttoExcelAgencyStatement(dtSelectedColumns, DocumentType);


            }

            #endregion

            #region All Booking Details

            if (requestedTable == "AllBookingDetails")
            {
                DataTable dtSession = null;
                if (Type == "All")
                {
                    dtSession = (DataTable)HttpContext.Current.Session["SesBookingList"];

                }
                else
                {
                    dtSession = (DataTable)HttpContext.Current.Session["SearchBookingList"];
                }

                DataTable dtResult = dtSession;
                DataView myDataView = dtResult.DefaultView;
                // myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgencyName", "Name", "LastName", "HotelName", "City", "CheckIn", "CheckOut", "TotalRooms", "Status", "TotalFare");
                ExporttoExcelAllBookingDetails(dtSelectedColumns, DocumentType);


            }

            #endregion

            //#region Agency Statement
            //////onload
            ////if (requestedTable == "AgencyStatement" && (Agent == "---Select---") && (dFrom == "" && dTo == "") && OnLoad == "True" && TransType == "Both")
            ////{
            ////    DataTable dtResult;
            ////    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementAll(out dtResult);
            ////    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);

            ////    }
            ////    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);
            ////    }

            ////}

            //////only agent
            ////if (requestedTable == "AgencyStatement" && (Agent != "---Select---") && (dFrom == "" && dTo == "") && (TransType != "Both" || TransType == "Both"))
            ////{
            ////    DataTable dtResult;
            ////    Int64 uid = Convert.ToInt64(Agent);
            ////    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementByAgent(uid, out dtResult);
            ////    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "UpdatedBy", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);

            ////    }
            ////    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);
            ////    }

            ////}

            //////date with Credit, Debit and Both...

            ////if (requestedTable == "AgencyStatement" && (Agent == "---Select---") && (dFrom != "" && dTo != "") && (TransType != "Both" || TransType == "Both"))
            ////{
            ////    DataTable dtResult;

            ////    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementByDate(dFrom, dTo, TransType, out dtResult);
            ////    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);

            ////    }
            ////    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);
            ////    }

            ////}
            ////// for credit and debit from drop down
            ////else if (requestedTable == "AgencyStatement" && (Agent == "---Select---") && (dFrom == "" && dTo == "") && TransType != "Both")
            ////{
            ////    DataTable dtResult;

            ////    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementByType(TransType, out dtResult);
            ////    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);

            ////    }
            ////    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);
            ////    }

            ////}

            ////else if (requestedTable == "AgencyStatement" && (Agent != "---Select---") && (dFrom != "" && dTo != "") && (TransType != "Both" || TransType == "Both"))
            ////{
            ////    DataTable dtResult;
            ////    Int64 uid = Convert.ToInt64(Agent);
            ////    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementByDateAgent(uid, dFrom, dTo, TransType, out dtResult);
            ////    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);

            ////    }
            ////    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            ////    {
            ////        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "TransactionDate", "AgencyName", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");
            ////        ExporttoExcelAgencyStatement(dtSelectedColumns);
            ////    }
            ////}

            //if (requestedTable == "AgencyStatement")
            //{
            //    DataTable dtSession = null;


            //    dtSession = (DataTable)HttpContext.Current.Session["InvoiceList"];
            //    DataTable dtResult = dtSession;
            //    DataView myDataView = dtResult.DefaultView;
            //    myDataView.Sort = "Sid DESC";
            //    DataTable SorteddtResult = myDataView.ToTable();
            //    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "AgencyName", "Source", "ReservationDate", "InvoiceID", "bookingname", "HotelName", "CheckIn", "CheckOut", "Status", "DeadLine", "CurrencyCode", "TotalFare", "Servicecharge");
            //    ExporttoExcelAgencyStatement(dtSelectedColumns, DocumentType);


            //}

            //#endregion Agency Statement

            //#region Invoice
            //// Invoice Area Admin---------------------------------------------------------------------------------------



            ////on load
            //if (requestedTable == "Invoice" && (Agency == "---Select---" && dFrom == "" && dTo == ""))
            //{
            //    DataTable dtResult;
            //    DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetails(out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate");
            //        ExporttoExcelInvoice(dtSelectedColumns);


            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }

            //}

            ////with agent

            //if (requestedTable == "Invoice" && (Agency != "---Select---" && dFrom == "" && dTo == ""))
            //{
            //    DataTable dtResult;


            //    Int64 Uid = Convert.ToInt64(Agency);

            //    DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsByAgent(Uid, out dtResult);
            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }

            //}

            ////By Date

            //if (requestedTable == "Invoice" && (Agency == "---Select---" && dFrom != "" && dTo != ""))
            //{
            //    DataTable dtResult;


            //    //Int64 Uid = Convert.ToInt64(Agency);
            //    DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsByDate(dFrom, dTo, out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }

            //}


            //if (requestedTable == "Invoice" && (Agency != "---Select---" && dFrom != "" && dTo != ""))
            //{
            //    DataTable dtResult;


            //    Int64 Uid = Convert.ToInt64(Agency);
            //    DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsAgentByDate(Uid, dFrom, dTo, out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationDate", "ReservationID", "AgentRef", "City", "bookingname", "Name", "CheckIn", "CheckOut", "NoOfDays", "status", "TotalRooms", "TotalFare", "Servicecharge", "CancelDate", "terms");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }

            //}



            //#endregion Invoice

            //#region Markup Group Details

            //if (requestedTable == "MarkupGroup")
            //{
            //    DataTable dtResult;
            //    DBHelper.DBReturnCode retCode = HotelMarkupManager.GetMarkupGroup(out dtResult);
            //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "GroupName", "Supplier", "MarkUpPercentage", "MarkUpAmount", "CommissionPercentage", "CommissionAmount");
            //        ExporttoExcelGroup(dtSelectedColumns);
            //    }

            //}

            //#endregion Markup Group Details

            //#region VisaDetails
            //// Invoice Area Admin---------------------------------------------------------------------------------------
            //string UserId = context.Request.QueryString["sid"];
            //string From = context.Request.QueryString["dFrom"];
            //string To = context.Request.QueryString["dTo"];
            ////on load
            //if (requestedTable == "VisaDetails" && (UserId == "---Select---" && From == "" && To == ""))
            //{
            //    int sEcho = Convert.ToInt16(context.Request.Params["sEcho"]);
            //    Int64 iDisplayLength = Convert.ToInt64(context.Request.Params["iDisplayLength"]);
            //    Int64 iDisplayStart = Convert.ToInt64(context.Request.Params["iDisplayStart"]);
            //    string Search = context.Request.Params["sSearch"];
            //    DataTable dtResult;
            //    string participant = HttpContext.Current.Request.Params["iParticipant"];
            //    DBHelper.DBReturnCode retCode = VisaDetailsManager.Visa(iDisplayLength, iDisplayStart, Search, 1, "ASC", out dtResult);
            //    //DBHelper.DBReturnCode retcode = VisaDetailsManager.Visa(out dtResult);
            //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        //ExporttoExcelInvoice(dtSelectedColumns);
            //    }
            //    else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelInvoice(dtSelectedColumns);
            //    }

            //}

            ////with agent

            //if (requestedTable == "VisaDetails" && (UserId != "---Select---" && From == "" && To == ""))
            //{
            //    DataTable dtResult;
            //    Int64 Uid = Convert.ToInt64(UserId);
            //    DBHelper.DBReturnCode retcode = VisaDetailsManager.VisaLoadByUserId(out dtResult, Uid);
            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {
            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }

            //}

            ////By Date

            //if (requestedTable == "VisaDetails" && (UserId == "---Select---" && From != "" && To != ""))
            //{
            //    DataTable dtResult;
            //    //Int64 Uid = Convert.ToInt64(Agency);
            //    DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaDetailsByDate(dFrom, dTo, out dtResult);
            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }

            //}


            //if (requestedTable == "VisaDetails" && (UserId != "---Select---" && From != "" && To != ""))
            //{
            //    DataTable dtResult;
            //    Int64 Uid = Convert.ToInt64(UserId);
            //    DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaDetailsAgentByDate(Uid, dFrom, dTo, out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        DataView myDataView = dtResult.DefaultView;
            //        myDataView.Sort = "Sid DESC";
            //        DataTable SorteddtResult = myDataView.ToTable();
            //        DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }
            //    else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "FirstName", "MiddleName", "LastName", "FatherName", "MotherName", "HusbandMame", "Gender", "Birth", "BirthCountry", "PassportNo", "IssuingGovernment", "IssuingDate", "ExpDate", "AddressLine1", "AddressLine2");
            //        ExporttoExcelVisa(dtSelectedColumns);
            //    }

            //}



            //#endregion Invoice


            //#region New Agency Statement

            //if (requestedTable == "NewAgencyStatement")
            //{

            //    DataTable dtResult;
            //    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementAllAgent(out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {
            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "TransactionDate", "Particulars", "CreditedAmount", "DebitedAmount", "Balance");

            //        //(false, false, false, "TransactionDate",false,false,"CreditedAmount", "DebitedAmount", "Balance",false,false,"AgencyName");
            //        ExporttoExcelNewAgencyStatement(dtSelectedColumns, DocumentType);
            //    }

            //}

            //#endregion New Agency Statement


            //#region AgencyCredit

            //if (requestedTable == "AgencyCredit")
            //{

            //    DataTable dtResult;
            //    DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatementAll(out dtResult);

            //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "AgencyName", "AvailableCredit", "CreditAmount", "LastUpdateDate", "Credit_Flag", "OTC");

            //        //(false, false, false, "TransactionDate",false,false,"CreditedAmount", "DebitedAmount", "Balance",false,false,"AgencyName");
            //        ExporttoExcelAgencyCredit(dtSelectedColumns, DocumentType);
            //    }

            //}

            //#endregion AgencyCredit


        }


        public static void ExporttoExcelAgencyStatement(DataTable objdatatable, string DocumentType)
        {


            objdatatable.Columns["TransactionDate"].ColumnName = "Date";
            //objdatatable.Columns["AgencyName"].ColumnName = "AgencyName";
            objdatatable.Columns["Particulars"].ColumnName = "Particulars";
            objdatatable.Columns["CreditedAmount"].ColumnName = "Credited";
            objdatatable.Columns["DebitedAmount"].ColumnName = "Debited";
            //objdatatable.Columns["Balance"].ColumnName = "Balance";

            int columnscount = 0;
            int Rowscount = 0;

            if (DocumentType != "PDF")
            {

                #region Excel

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
                //sets font
                HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
                //HttpContext.Current.Response.Write("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("S.N");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {      //write in new column

                    HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    //HttpContext.Current.Response.Write("S.N");
                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    HttpContext.Current.Response.Write("<Td colspan=\"5\">");
                    HttpContext.Current.Response.Write("No Record Found");
                    HttpContext.Current.Response.Write("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        HttpContext.Current.Response.Write("<TR align=\"center\">");


                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                        HttpContext.Current.Response.Write("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            HttpContext.Current.Response.Write("<Td>");
                            //if (i != 2 && i != 3 && i != 4)
                            if (i != 3 && i != 4 && i != 5)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    HttpContext.Current.Response.Write("-");


                                }

                                else
                                {

                                    //HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    HttpContext.Current.Response.Write(row[i].ToString());


                                }
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("</TR>");
                    }
                    HttpContext.Current.Response.Write("</Table>");
                    HttpContext.Current.Response.Write("</font>");
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                }
                # endregion

            }
            else
            {
                #region PDF

                StringBuilder sb = new StringBuilder();
                sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");

                //sets font
                sb.Append("<span style='Text-align:center'><h1>My Statement Report</h1></span>");
                sb.Append("<br>");
                sb.Append("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                //sb.Append("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                sb.Append("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='width:100%;font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                sb.Append("<B>");
                sb.Append("S.N");
                sb.Append("</B>");
                sb.Append("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {

                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    sb.Append("<B>");
                    //sb.Append("S.N");
                    sb.Append(objdatatable.Columns[j].ToString());
                    sb.Append("</B>");
                    sb.Append("</Td>");
                }
                sb.Append("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    sb.Append("<Td colspan=\"5\">");
                    sb.Append("No Record Found");
                    sb.Append("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        sb.Append("<TR align=\"center\">");


                        sb.Append("<Td>");
                        sb.Append(objdatatable.Rows.IndexOf(row) + 1);
                        sb.Append("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            sb.Append("<Td>");
                            if (i != 2 && i != 3 && i != 4)
                            {
                                sb.Append(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    sb.Append("-");


                                }
                                else
                                {
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0.00"));
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    sb.Append(row[i].ToString());


                                }
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("</TR>");
                    }
                    sb.Append("</Table>");
                    sb.Append("</font>");
                    HttpContext context = System.Web.HttpContext.Current;
                    var htmlContent = String.Format(sb.ToString());
                    NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                    pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;

                    pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                    //pdfConverter.PdfToolPath = "E:\\_Projects_\\ClickUrTrip\\CUT\\Agent";
                    pdfConverter.PdfToolPath = System.Configuration.ConfigurationManager.AppSettings["rootPath"] + "\\Agent";
                    var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=MyStatement.pdf");
                    context.Response.BinaryWrite(pdfBytes);

                }
                # endregion

            }
        }

        public static void ExporttoExcelAllBookingDetails(DataTable objdatatable, string DocumentType)
        {
            objdatatable.Columns["ReservationDate"].ColumnName = "Date";
            objdatatable.Columns["ReservationID"].ColumnName = "Ref No.";
            objdatatable.Columns["AgencyName"].ColumnName = "Supplier";
            objdatatable.Columns["Name"].ColumnName = "First Name";
            objdatatable.Columns["LastName"].ColumnName = "Last Name";
            objdatatable.Columns["HotelName"].ColumnName = "Hotel Name";
            objdatatable.Columns["City"].ColumnName = "Location";
            objdatatable.Columns["CheckIn"].ColumnName = "Check In";
            objdatatable.Columns["CheckOut"].ColumnName = "Check Out";
            objdatatable.Columns["TotalRooms"].ColumnName = "Rooms";
            objdatatable.Columns["Status"].ColumnName = "Status";


            int columnscount = 0;
            int Rowscount = 0;

            if (DocumentType != "PDF")
            {

                #region Excel

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
                //sets font
                HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
                //HttpContext.Current.Response.Write("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("S.N");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {      //write in new column

                    HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    //HttpContext.Current.Response.Write("S.N");
                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    HttpContext.Current.Response.Write("<Td colspan=\"5\">");
                    HttpContext.Current.Response.Write("No Record Found");
                    HttpContext.Current.Response.Write("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        HttpContext.Current.Response.Write("<TR align=\"center\">");


                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                        HttpContext.Current.Response.Write("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            HttpContext.Current.Response.Write("<Td>");
                            //if (i != 2 && i != 3 && i != 4)
                            if (i != 3 && i != 4 && i != 5)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    HttpContext.Current.Response.Write("-");


                                }

                                else
                                {

                                    //HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    HttpContext.Current.Response.Write(row[i].ToString());


                                }
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("</TR>");
                    }
                    HttpContext.Current.Response.Write("</Table>");
                    HttpContext.Current.Response.Write("</font>");
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                }
                # endregion

            }
            else
            {
                #region PDF

                StringBuilder sb = new StringBuilder();
                sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");

                //sets font
                sb.Append("<span style='Text-align:center'><h1>Booking Statement Report</h1></span>");
                sb.Append("<br>");
                sb.Append("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                //sb.Append("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                sb.Append("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='width:100%;font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                sb.Append("<B>");
                sb.Append("S.N");
                sb.Append("</B>");
                sb.Append("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {

                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    sb.Append("<B>");
                    //sb.Append("S.N");
                    sb.Append(objdatatable.Columns[j].ToString());
                    sb.Append("</B>");
                    sb.Append("</Td>");
                }
                sb.Append("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    sb.Append("<Td colspan=\"5\">");
                    sb.Append("No Record Found");
                    sb.Append("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        sb.Append("<TR align=\"center\">");


                        sb.Append("<Td>");
                        sb.Append(objdatatable.Rows.IndexOf(row) + 1);
                        sb.Append("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            sb.Append("<Td>");
                            if (i != 2 && i != 3 && i != 4)
                            {
                                sb.Append(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    sb.Append("-");


                                }
                                else
                                {
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0.00"));
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    sb.Append(row[i].ToString());


                                }
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("</TR>");
                    }
                    sb.Append("</Table>");
                    sb.Append("</font>");
                    HttpContext context = System.Web.HttpContext.Current;
                    var htmlContent = String.Format(sb.ToString());
                    NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                    pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;

                    pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                    //pdfConverter.PdfToolPath = "E:\\_Projects_\\ClickUrTrip\\CUT\\Agent";
                    pdfConverter.PdfToolPath = System.Configuration.ConfigurationManager.AppSettings["rootPath"] + "\\Agent";
                    var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=AgencyStatement.pdf");
                    context.Response.BinaryWrite(pdfBytes);

                }
                # endregion

            }
        }

        public static void ExporttoExcelNewAgencyStatement(DataTable objdatatable, string DocumentType)
        {


            //objdatatable.Columns["TransactionDate"].ColumnName = "Date";
            //objdatatable.Columns["Particulars"].ColumnName = "Particulars";
            //objdatatable.Columns["CreditedAmount"].ColumnName = "Credited";
            //objdatatable.Columns["DebitedAmount"].ColumnName = "Debited";
            //objdatatable.Columns["Balance"].ColumnName = "Balance";

            int columnscount = 0;
            int Rowscount = 0;

            if (DocumentType != "PDF")
            {

                #region Excel

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
                //sets font
                HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
                //HttpContext.Current.Response.Write("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("S.N");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {      //write in new column

                    HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    //HttpContext.Current.Response.Write("S.N");
                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    HttpContext.Current.Response.Write("<Td colspan=\"5\">");
                    HttpContext.Current.Response.Write("No Record Found");
                    HttpContext.Current.Response.Write("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        HttpContext.Current.Response.Write("<TR align=\"center\">");


                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                        HttpContext.Current.Response.Write("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            HttpContext.Current.Response.Write("<Td>");
                            //if (i != 2 && i != 3 && i != 4)
                            if (i != 3 && i != 4 && i != 5)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    HttpContext.Current.Response.Write("-");


                                }

                                else
                                {

                                    //HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    HttpContext.Current.Response.Write(row[i].ToString());


                                }
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("</TR>");
                    }
                    HttpContext.Current.Response.Write("</Table>");
                    HttpContext.Current.Response.Write("</font>");
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                }
                # endregion

            }
            else
            {
                #region PDF

                StringBuilder sb = new StringBuilder();
                sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");

                //sets font
                sb.Append("<span style='Text-align:center'><h1>Agency Statement Report</h1></span>");
                sb.Append("<br>");
                sb.Append("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                //sb.Append("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                sb.Append("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='width:100%;font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                sb.Append("<B>");
                sb.Append("S.N");
                sb.Append("</B>");
                sb.Append("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {

                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    sb.Append("<B>");
                    //sb.Append("S.N");
                    sb.Append(objdatatable.Columns[j].ToString());
                    sb.Append("</B>");
                    sb.Append("</Td>");
                }
                sb.Append("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    sb.Append("<Td colspan=\"5\">");
                    sb.Append("No Record Found");
                    sb.Append("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        sb.Append("<TR align=\"center\">");


                        sb.Append("<Td>");
                        sb.Append(objdatatable.Rows.IndexOf(row) + 1);
                        sb.Append("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            sb.Append("<Td>");
                            if (i != 2 && i != 3 && i != 4)
                            {
                                sb.Append(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    sb.Append("-");


                                }
                                else
                                {
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0.00"));
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    sb.Append(row[i].ToString());


                                }
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("</TR>");
                    }
                    sb.Append("</Table>");
                    sb.Append("</font>");
                    HttpContext context = System.Web.HttpContext.Current;
                    var htmlContent = String.Format(sb.ToString());
                    NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                    pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;

                    pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                    //pdfConverter.PdfToolPath = "E:\\_Projects_\\ClickUrTrip\\CUT\\Agent";
                    pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\Agent";
                    var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=NewAgencyStatement.pdf");
                    context.Response.BinaryWrite(pdfBytes);

                }
                # endregion

            }
        }

        public static void ExporttoExcelAgencyCredit(DataTable objdatatable, string DocumentType)
        {


            //objdatatable.Columns["TransactionDate"].ColumnName = "Date";
            //objdatatable.Columns["Particulars"].ColumnName = "Particulars";
            //objdatatable.Columns["CreditedAmount"].ColumnName = "Credited";
            //objdatatable.Columns["DebitedAmount"].ColumnName = "Debited";
            //objdatatable.Columns["Balance"].ColumnName = "Balance";

            int columnscount = 0;
            int Rowscount = 0;

            if (DocumentType != "PDF")
            {

                #region Excel

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
                //sets font
                HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
                //HttpContext.Current.Response.Write("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("S.N");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {      //write in new column

                    HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    //HttpContext.Current.Response.Write("S.N");
                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }
                HttpContext.Current.Response.Write("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    HttpContext.Current.Response.Write("<Td colspan=\"5\">");
                    HttpContext.Current.Response.Write("No Record Found");
                    HttpContext.Current.Response.Write("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        HttpContext.Current.Response.Write("<TR align=\"center\">");


                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                        HttpContext.Current.Response.Write("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            HttpContext.Current.Response.Write("<Td>");
                            //if (i != 2 && i != 3 && i != 4)
                            if (i != 3 && i != 4 && i != 5)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    HttpContext.Current.Response.Write("-");


                                }

                                else
                                {

                                    //HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    HttpContext.Current.Response.Write(row[i].ToString());


                                }
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("</TR>");
                    }
                    HttpContext.Current.Response.Write("</Table>");
                    HttpContext.Current.Response.Write("</font>");
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                }
                # endregion

            }
            else
            {
                #region PDF

                StringBuilder sb = new StringBuilder();
                sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");

                //sets font
                sb.Append("<span style='Text-align:center'><h1>Agency Credit Report</h1></span>");
                sb.Append("<br>");
                sb.Append("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                //sb.Append("<BR><BR><BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                sb.Append("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='width:100%;font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                sb.Append("<B>");
                sb.Append("S.N");
                sb.Append("</B>");
                sb.Append("</Td>");

                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < columnscount; j++)
                {

                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    sb.Append("<B>");
                    //sb.Append("S.N");
                    sb.Append(objdatatable.Columns[j].ToString());
                    sb.Append("</B>");
                    sb.Append("</Td>");
                }
                sb.Append("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    sb.Append("<Td colspan=\"5\">");
                    sb.Append("No Record Found");
                    sb.Append("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row
                        sb.Append("<TR align=\"center\">");


                        sb.Append("<Td>");
                        sb.Append(objdatatable.Rows.IndexOf(row) + 1);
                        sb.Append("</Td>");


                        for (int i = 0; i < objdatatable.Columns.Count; i++)
                        {

                            sb.Append("<Td>");
                            if (i != 2 && i != 3 && i != 4)
                            {
                                sb.Append(row[i].ToString());
                            }
                            else
                            {

                                if (row[i].ToString() == "")
                                {
                                    sb.Append("-");


                                }
                                else
                                {
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0.00"));
                                    //sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                                    sb.Append(row[i].ToString());


                                }
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("</TR>");
                    }
                    sb.Append("</Table>");
                    sb.Append("</font>");
                    HttpContext context = System.Web.HttpContext.Current;
                    var htmlContent = String.Format(sb.ToString());
                    NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                    pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;

                    pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                    //pdfConverter.PdfToolPath = "E:\\_Projects_\\ClickUrTrip\\CUT\\Agent";
                    pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\Agent";
                    var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=AgencyCredit.pdf");
                    context.Response.BinaryWrite(pdfBytes);

                }
                # endregion

            }
        }

        public static void ExporttoExcel(DataTable objdatatable, string DocType)
        {
            StringBuilder sb = new StringBuilder();

            //objdatatable.Columns["Last_Name"].ColumnName = "Last Name";
            objdatatable.Columns["uid"].ColumnName = "Email";
            objdatatable.Columns["ContactPerson"].ColumnName = "Contact Person";
            objdatatable.Columns["Agentuniquecode"].ColumnName = "Agent Unique Code";
            objdatatable.Columns["AgencyName"].ColumnName = "Agency Name";
            //objdatatable.Columns["Last_Name"].ColumnName = "Contact Person";
            objdatatable.Columns["Description"].ColumnName = "City";
            objdatatable.Columns["Country"].ColumnName = "Country";
            objdatatable.Columns["LoginFlag"].ColumnName = "Status";

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            if (DocType != "PDF")
            {
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");
                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            }
            sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            //sets font
            sb.Append("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            sb.Append("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                sb.Append("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                //Get column headers  and make it as bold in excel columns
                sb.Append("<B>");
                sb.Append(objdatatable.Columns[j].ToString());
                sb.Append("</B>");
                sb.Append("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                sb.Append("<Td colspan=\"7\" align=\"left\">");
                sb.Append("No Record Found");
                sb.Append("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    sb.Append("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        sb.Append("<Td>");
                        sb.Append(row[i].ToString().Replace("False", "Inactive").Replace("True", "Active"));
                        sb.Append("</Td>");
                    }

                    sb.Append("</TR>");
                }
                sb.Append("</Table>");
                sb.Append("</font>");

            }

            if (DocType == "PDF")
            {
                var htmlContent = String.Format(sb.ToString());
                NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;

                pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                pdfConverter.PdfToolPath = System.Configuration.ConfigurationManager.AppSettings["rootPath"] + "\\Agent";
                var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=AgencyDetails.pdf");
                HttpContext.Current.Response.BinaryWrite(pdfBytes);
            }
            else
            {
                HttpContext.Current.Response.Write(sb.ToString());
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelStaff(DataTable objdatatable)
        {


            //objdatatable.Columns["Last_Name"].ColumnName = "Last Name";
            objdatatable.Columns["uid"].ColumnName = "Email";
            objdatatable.Columns["ContactPerson"].ColumnName = "Contact Person";
            objdatatable.Columns["Staffuniquecode"].ColumnName = "Staff Unique Code";

            //objdatatable.Columns["Last_Name"].ColumnName = "Contact Person";
            objdatatable.Columns["Designation"].ColumnName = "Designation";
            // objdatatable.Columns["Countryname"].ColumnName = "Country";


            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"6\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString());
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelFranchisee(DataTable objdatatable)
        {


            //objdatatable.Columns["Last_Name"].ColumnName = "Last Name";
            objdatatable.Columns["uid"].ColumnName = "Email";
            objdatatable.Columns["ContactPerson"].ColumnName = "Contact Person";
            objdatatable.Columns["Agentuniquecode"].ColumnName = "Unique Code";
            objdatatable.Columns["AgencyName"].ColumnName = "Franchisee Name";
            //objdatatable.Columns["Last_Name"].ColumnName = "Contact Person";
            objdatatable.Columns["Description"].ColumnName = "City";
            objdatatable.Columns["Countryname"].ColumnName = "Country";
            objdatatable.Columns["LoginFlag"].ColumnName = "Status";

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"7\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString().Replace("False", "Inactive").Replace("True", "Active"));
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelGroup(DataTable objdatatable)
        {
            objdatatable.Columns["GroupName"].ColumnName = "Group Name";
            objdatatable.Columns["Supplier"].ColumnName = "Supplier";
            objdatatable.Columns["MarkUpPercentage"].ColumnName = "MarkUp [%]";
            objdatatable.Columns["MarkUpAmount"].ColumnName = "MarkUp Amount";
            objdatatable.Columns["CommissionPercentage"].ColumnName = "Commission [%]";
            objdatatable.Columns["CommissionAmount"].ColumnName = "Commission Amount";


            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"6\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString());
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelInvoice(DataTable objdatatable)
        {
            objdatatable.Columns["ReservationID"].ColumnName = "Booking Number";
            objdatatable.Columns["AgentRef"].ColumnName = "Agent Ref";
            objdatatable.Columns["ReservationDate"].ColumnName = "Booking Date";
            objdatatable.Columns["bookingname"].ColumnName = "Leading Guest Name";
            objdatatable.Columns["TotalFare"].ColumnName = "SubTotal";
            objdatatable.Columns["Name"].ColumnName = "Hotel Name";
            objdatatable.Columns["CheckIn"].ColumnName = "Check-In";
            objdatatable.Columns["CheckOut"].ColumnName = "Check-Out";
            objdatatable.Columns["NoOfDays"].ColumnName = "Nights";
            objdatatable.Columns["TotalRooms"].ColumnName = "Rooms";

            objdatatable.Columns["Servicecharge"].ColumnName = "ServiceTax";
            //objdatatable.Columns["terms"].ColumnName = "Remarks";

            DataTable table = objdatatable;
            string SubTotal;

            object SumTotalFare;
            SumTotalFare = table.Compute("Sum(SubTotal)", "");
            string strTotalFare = SumTotalFare.ToString();

            if (strTotalFare == "")
            {
                SubTotal = "";
            }
            else
            {
                SubTotal = Convert.ToDecimal(SumTotalFare.ToString()).ToString("#,##0");
            }


            object SumServiceTax;
            SumServiceTax = table.Compute("Sum(ServiceTax)", "");

            string strServiceTax = SumServiceTax.ToString();
            string SerTax;
            if (strServiceTax == "")
            {
                SerTax = "";
            }
            else
            {
                SerTax = Convert.ToDecimal(SumServiceTax.ToString()).ToString("#,##0");
            }

            //Decimal TotalAmmount;
            string TotalAmmount;
            if (strTotalFare == "" && strServiceTax == "")
            {
                TotalAmmount = "";
            }
            else
            {
                TotalAmmount = ((Convert.ToDecimal(SumServiceTax) + Convert.ToDecimal(SumTotalFare)).ToString("#,##0"));
            }
            //HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("S.N");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");
            int columnscount = objdatatable.Columns.Count;
            int Rowscount = objdatatable.Rows.Count;


            for (int j = 0; j < 13; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");


                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());

                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }

            HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Total Fare");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            for (int j = 14; j < columnscount; j++)
            {
                HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");

                HttpContext.Current.Response.Write("<B>");


                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());

                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }

            HttpContext.Current.Response.Write("</TR>");
            if (objdatatable.Rows.Count == 0)
            {
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row

                    HttpContext.Current.Response.Write("<TR align=\"center\">");
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                    HttpContext.Current.Response.Write("</Td>");
                    for (int i = 0; i < 13; i++)
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        if (i != 0 && i != 6 && i != 7 && i != 11 && i != 12)
                        {
                            HttpContext.Current.Response.Write(row[i].ToString());
                        }
                        else if (i == 11 || i == 12)
                        {

                            HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                        }


                        else
                        {

                            HttpContext.Current.Response.Write(row[i].ToString().Replace("00:00", ""));
                        }

                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(((Convert.ToDecimal(row[11])) + (Convert.ToDecimal(row[12]))).ToString("#,##0"));
                    HttpContext.Current.Response.Write("</Td>");
                    for (int j = 0; j < columnscount; j++)
                    {
                        HttpContext.Current.Response.Write("<Td style='font-size:13.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");

                        HttpContext.Current.Response.Write("<B>");


                        HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());

                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    for (int i = 14; i < objdatatable.Columns.Count; i++)
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        if (i != 0 && i != 6 && i != 7)
                        {
                            HttpContext.Current.Response.Write(row[i].ToString());
                        }

                        else if (i == 11 && i == 12)
                        {

                            HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                        }

                        else
                        {
                            HttpContext.Current.Response.Write(row[i].ToString().Replace("00:00", ""));
                        }

                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Write("<Table>");
                HttpContext.Current.Response.Write("<TR align=\"center\">");
                HttpContext.Current.Response.Write("<Td colspan=\"12\">");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write(SubTotal);
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write(SerTax);
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write(TotalAmmount);
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelVisa(DataTable objdatatable)
        {
            objdatatable.Columns["FirstName"].ColumnName = "First Name";
            objdatatable.Columns["MiddleName"].ColumnName = "Middle Name";
            objdatatable.Columns["LastName"].ColumnName = "Last Name";
            objdatatable.Columns["FatherName"].ColumnName = "Father Name";
            objdatatable.Columns["MotherName"].ColumnName = "Mother Name";
            objdatatable.Columns["HusbandMame"].ColumnName = "HusbandMame";
            objdatatable.Columns["Gender"].ColumnName = "Gender";
            objdatatable.Columns["Birth"].ColumnName = "Birth";
            objdatatable.Columns["BirthCountry"].ColumnName = "Birth Country";
            objdatatable.Columns["PassportNo"].ColumnName = "PassportNo";
            objdatatable.Columns["IssuingGovernment"].ColumnName = "Issuing Government";
            objdatatable.Columns["IssuingDate"].ColumnName = "Issuing Date";
            objdatatable.Columns["ExpDate"].ColumnName = "Expire Date";
            objdatatable.Columns["AddressLine1"].ColumnName = "Address1";
            objdatatable.Columns["AddressLine2"].ColumnName = "Address2";
            int columnscount = objdatatable.Columns.Count;

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
            //am getting my grid's column headers
            //Int64 columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"6\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString());
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExportPackage(DataTable objdatatable, string DocumentType)
        {
            objdatatable.Columns["PackageName"].ColumnName = "Package Name";
            objdatatable.Columns["CatID"].ColumnName = "Package Type";
            objdatatable.Columns["Supplier"].ColumnName = "Agency";
            objdatatable.Columns["Location"].ColumnName = "Location";
            objdatatable.Columns["TravelDate"].ColumnName = "Travel Date";
            objdatatable.Columns["StartDate"].ColumnName = "Start Date";
            objdatatable.Columns["EndDate"].ColumnName = "End Date";
            objdatatable.Columns["PaxName"].ColumnName = "Leading Guest Name";
            objdatatable.Columns["Status"].ColumnName = "Status";


            DataTable table = objdatatable;

            int columnscount = 0;
            int Rowscount = 0;


            if (DocumentType != "PDF")
            {

                #region Excel
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=PackageList.xls");

                HttpContext.Current.Response.Charset = "utf-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
                //sets font
                HttpContext.Current.Response.Write("<span style='Text-align:center'><h1>Package List Report</h1></span>");
                HttpContext.Current.Response.Write("<br>");
                HttpContext.Current.Response.Write("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                HttpContext.Current.Response.Write("<Table border='1' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                HttpContext.Current.Response.Write("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("S.N");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < 9; j++)
                {      //write in new column
                    HttpContext.Current.Response.Write("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");


                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());

                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }



                for (int j = 10; j < columnscount; j++)
                {
                    HttpContext.Current.Response.Write("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");

                    HttpContext.Current.Response.Write("<B>");


                    HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());

                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write("No Record Found");
                    HttpContext.Current.Response.Write("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row

                        HttpContext.Current.Response.Write("<TR align=\"center\">");
                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(objdatatable.Rows.IndexOf(row) + 1);
                        HttpContext.Current.Response.Write("</Td>");
                        for (int i = 0; i < 8; i++)
                        {
                            HttpContext.Current.Response.Write("<Td>");
                            if (i != 0 && i != 6 && i != 7 && i != 11 && i != 12)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }

                            else if (i == 10 || i == 11)
                            {
                                HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                            }
                            else
                            {
                                HttpContext.Current.Response.Write(row[i].ToString().Replace("00:00", ""));
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("<Td>");


                        for (int i = 9; i < objdatatable.Columns.Count; i++)
                        {
                            HttpContext.Current.Response.Write("<Td>");
                            if (i != 0 && i != 6 && i != 7)
                            {
                                HttpContext.Current.Response.Write(row[i].ToString());
                            }

                            else if (i == 11 && i == 12)
                            {

                                HttpContext.Current.Response.Write(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                            }

                            else
                            {
                                HttpContext.Current.Response.Write(row[i].ToString().Replace("00:00", ""));
                            }

                            HttpContext.Current.Response.Write("</Td>");
                        }

                        HttpContext.Current.Response.Write("</TR>");
                    }
                    HttpContext.Current.Response.Write("</Table>");
                    HttpContext.Current.Response.Write("</font>");

                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }

                #endregion
            }
            else
            {
                #region PDF
                StringBuilder sb = new StringBuilder();
                sb.Append("<!DOCTYPE html>");
                sb.Append(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                sb.Append("<span style='Text-align:center'><h1>Package Booking List Report</h1></span>");
                sb.Append("<br>");
                //sets font
                sb.Append("<font style='font-size:16.0pt; font-family:Helvetica Neue;'>");
                sb.Append("<Table border='1' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'> <TR>");
                //am getting my grid's column headers
                sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                sb.Append("<B>");
                sb.Append("S.N");
                sb.Append("</B>");
                sb.Append("</Td>");
                columnscount = objdatatable.Columns.Count;
                Rowscount = objdatatable.Rows.Count;


                for (int j = 0; j < 9; j++)
                {      //write in new column
                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");
                    //Get column headers  and make it as bold in excel columns
                    sb.Append("<B>");


                    sb.Append(objdatatable.Columns[j].ToString());

                    sb.Append("</B>");
                    sb.Append("</Td>");
                }



                for (int j = 10; j < columnscount; j++)
                {
                    sb.Append("<Td style='font-size:16.0pt; font-family:Helvetica Neue; text-align:center; background:#006699; color:white'>");

                    sb.Append("<B>");


                    sb.Append(objdatatable.Columns[j].ToString());

                    sb.Append("</B>");
                    sb.Append("</Td>");
                }

                sb.Append("</TR>");
                if (objdatatable.Rows.Count == 0)
                {
                    sb.Append("<Td>");
                    sb.Append("No Record Found");
                    sb.Append("</Td>");
                }
                else
                {
                    foreach (DataRow row in objdatatable.Rows)
                    {//write in new row

                        sb.Append("<TR align=\"center\">");
                        sb.Append("<Td>");
                        sb.Append(objdatatable.Rows.IndexOf(row) + 1);
                        sb.Append("</Td>");
                        for (int i = 0; i < 9; i++)
                        {
                            sb.Append("<Td>");
                            if (i != 0 && i != 6 && i != 7 && i != 11 && i != 12)
                            {
                                sb.Append(row[i].ToString());
                            }

                            else if (i == 10 || i == 11)
                            {
                                sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                            }
                            else
                            {
                                sb.Append(row[i].ToString().Replace("00:00", ""));
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("<Td>");
                        //sb.Append(((Convert.ToDecimal(row[11])) + (Convert.ToDecimal(row[12]))).ToString("#,##0"));
                        sb.Append("</Td>");

                        for (int i = 9; i < objdatatable.Columns.Count; i++)
                        {
                            sb.Append("<Td>");
                            if (i != 0 && i != 6 && i != 7)
                            {
                                sb.Append(row[i].ToString());
                            }

                            else if (i == 11 && i == 12)
                            {

                                sb.Append(Convert.ToDecimal(row[i].ToString()).ToString("#,##0"));
                            }

                            else
                            {
                                sb.Append(row[i].ToString().Replace("00:00", ""));
                            }

                            sb.Append("</Td>");
                        }

                        sb.Append("</TR>");
                    }


                    sb.Append("</Table>");



                }

                HttpContext context = System.Web.HttpContext.Current;
                var htmlContent = String.Format(sb.ToString());
                NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
                pdfConverter.Size = NReco.PdfGenerator.PageSize.Default;
                pdfConverter.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                // pdfConverter.PdfToolPath = "E:\\ClickUrTrip\\CUT\\Agent";
                pdfConverter.PdfToolPath = "C:\\inetpub\\wwwroot\\Agent";
                var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
                context.Response.ContentType = "application/pdf";
                context.Response.AddHeader("Content-Disposition", "attachment;filename=BookingList.pdf");
                context.Response.BinaryWrite(pdfBytes);

                #endregion
            }

        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}