﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for MailsHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class MailsHandler : System.Web.Services.WebService
    {
         static helperDataContext db = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string GetHotelActivityMails(string Type)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            //DBHelper.DBReturnCode retcode = VisaManager.GetVisaMails(Activity,Type, out dtResult);

            var HotelMailsList = (from obj in db.tbl_ActivityMails
                                  where obj.Type == Type && obj.ParentID == Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"])
                                  select new
                                  {
                                      obj.sid,
                                      obj.Activity,
                                      //  obj.BCcMail,
                                      obj.CcMail,
                                      obj.Email,
                                      obj.ErroMessage
                                  }).ToList();

            if (HotelMailsList.Count > 0 && HotelMailsList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = HotelMailsList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        #region Mails Add Update
        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail, string Message)
        {
            try
            {
                var sActivity = (from obj in db.tbl_ActivityMails where obj.Activity == Activity && obj.Type == Type select obj).FirstOrDefault();
                sActivity.Email = MailsId;
                sActivity.CcMail = CcMails;
                sActivity.BCcMail = BCcMail;
                sActivity.ErroMessage = Message;
                db.SubmitChanges();
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
        }
        #endregion


    }
}
