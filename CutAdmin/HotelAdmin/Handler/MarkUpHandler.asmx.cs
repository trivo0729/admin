﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for MarkUpHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MarkUpHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        #region Group Markup

        [WebMethod(EnableSession = true)]
        public string GetGroup()
        {
            try
            {
                List<string> ListDetail = new List<string>();
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uId = AccountManager.GetUserByLogin();
               
                var List = (from obj in DB.tbl_GroupMarkups where obj.ParentID == uId select obj).ToList();
                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetGroupMarkup(Int64 Id)
        {
            try
            {
                var List = (from obj in DB.tbl_GroupMarkups
                            join mark in DB.tbl_GroupMarkupDetails on obj.sid equals mark.GroupId
                            where obj.sid == Id
                            select new
                            {
                                obj.sid,
                                obj.GroupName,
                                obj.ParentID,
                                mark.MarkupPercentage,
                                mark.MarkupAmmount,
                                mark.CommessionAmmount,
                                mark.CommessionPercentage,
                                mark.TaxApplicable

                            }).ToList();

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddGroupDetails(string GroupName, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uId = AccountManager.GetUserByLogin();
                tbl_GroupMarkup Mark = new tbl_GroupMarkup();
                Mark.GroupName = GroupName;
                Mark.ParentID = uId;
                //Mark.ParentID = 232;
                DB.tbl_GroupMarkups.InsertOnSubmit(Mark);
                DB.SubmitChanges();

                tbl_GroupMarkupDetail Det = new tbl_GroupMarkupDetail();
                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                Det.GroupId = Mark.sid;
                Det.TaxApplicable = false;
                DB.tbl_GroupMarkupDetails.InsertOnSubmit(Det);
                DB.SubmitChanges();


                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Update(Int64 GroupId, Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                tbl_GroupMarkupDetail Det = DB.tbl_GroupMarkupDetails.Where(d => d.GroupId == GroupId).FirstOrDefault();

                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


        #region Individual Markup
        [WebMethod(EnableSession = true)]
        public string GetIndividualMarkup(Int64 Id)
        {
            try
            {
                var List = (from obj in DB.tbl_IndividualMarkups where obj.AgentId == Id select obj).ToList();

                if (List.Count == 0)
                {
                    tbl_IndividualMarkup Det = new tbl_IndividualMarkup();
                    Det.Type = 1;
                    Det.MarkupPercentage = 0;
                    Det.MarkupAmmount = 0;
                    Det.CommessionAmmount = 0;
                    Det.CommessionPercentage = 0;
                    Det.AgentId = Id;
                    DB.tbl_IndividualMarkups.InsertOnSubmit(Det);
                    DB.SubmitChanges();
                }

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateIndividualMarkupDetails(Int64 Sid, Int64 IndMarkPercentage, Int64 IndMarkUpAmount, Int64 IndCommPercentage, Int64 IndCommAmount)
        {
            try
            {
                tbl_IndividualMarkup Det = DB.tbl_IndividualMarkups.Where(d => d.AgentId == Sid).FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = IndMarkPercentage;
                Det.MarkupAmmount = IndMarkUpAmount;
                Det.CommessionAmmount = IndCommAmount;
                Det.CommessionPercentage = IndCommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


        #region Global Markup

        [WebMethod(EnableSession = true)]
        public string GetGlobalMarkup()
        {
            try
            {
                var List = (from obj in DB.tbl_GlobalMarkups select obj).ToList();

                //if (List.Count == 0)
                //{
                //    tbl_GlobalMarkup Det = new tbl_GlobalMarkup();
                //    Det.Type = 1;
                //    Det.MarkupPercentage = 0;
                //    Det.MarkupAmmount = 0;
                //    Det.CommessionAmmount = 0;
                //    Det.CommessionPercentage = 0;
                //    DB.tbl_GlobalMarkups.InsertOnSubmit(Det);
                //    DB.SubmitChanges();
                //}

                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateGlobalMarkupDetails(Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                tbl_GlobalMarkup Det = DB.tbl_GlobalMarkups.FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        #endregion

    }
}
