﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for TransactionHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TransactionHandler : System.Web.Services.WebService
    {

        #region Bank / Cash
        [WebMethod(EnableSession = true)]
        public string InsertDeposit(Int64 Uid, string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string Date)
        {
            DBHelper.DBReturnCode retCode = UpdateCreditManager.AddDeposit(Uid, BankName, AccountNumber, AmountDeposit, Remarks, TypeOfDeposit, TypeOfCash, TypeOfCheque, EmployeeName, ReceiptNumber, ChequeDDNumber, ChequeDrawn, Date);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string Name = objGlobalDefaults.ContactPerson;
                string Email = objGlobalDefaults.uid;
                string Amount = AmountDeposit;
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #endregion

        #region Credit/Debit
        [WebMethod(true)]
        public string UpdateCredit(Int64 sid, Int64 uid, decimal DepositAmmount, decimal CreditAmmount, decimal MaxCreditAmmount, string Remark, string Invoice, decimal ReduceAmmount)
        {
            int rows;
            string jsonString = "";
            int bActiveFlag = 1;
            DBHelper.DBReturnCode retcode = UpdateCreditManager.UpdateCredit(sid, uid, bActiveFlag, DepositAmmount, CreditAmmount, MaxCreditAmmount, ReduceAmmount, Remark, Invoice, out rows);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Limit
        [WebMethod(true)]
        public string UpdateCreditLmit(Int64 sid, Int64 uid, decimal MaxCreditAmmount, decimal CreditLimit, string days, string Remark)
        {
            int rows;
            string jsonString = "";
            int bActiveFlag = 1;
            DataTable dtResult = null;
            DBHelper.DBReturnCode retcode1 = UpdateCreditManager.ValidateLimit(uid, out dtResult);
            if (retcode1 == DBHelper.DBReturnCode.SUCCESS)
            {
                decimal FixLimt = (decimal)(dtResult.Rows[0]["CreditAmount"]);
                decimal FixLimt_Cmp = (decimal)(dtResult.Rows[0]["CreditAmount_Cmp"]);
                if (FixLimt != FixLimt_Cmp && MaxCreditAmmount == 0)
                {
                    return jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                }
                decimal OneTimeLimt = (decimal)(dtResult.Rows[0]["MaxCreditLimit"]);
                decimal OneTime_Cmp = (decimal)(dtResult.Rows[0]["MaxCreditLimit_Cmp"]);
                if (OneTimeLimt != OneTime_Cmp && CreditLimit == 0)
                {
                    return jsonString = "{\"Session\":\"1\",\"retCode\":\"3\"}";
                }
            }


            DBHelper.DBReturnCode retcode = UpdateCreditManager.UpdateCreditLmit(sid, uid, bActiveFlag, MaxCreditAmmount, CreditLimit, days, Remark, out rows);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion
        [WebMethod(true)]
        public string ValidateLimit(Int64 uId)
        {
            string jsonString = "";
            DataTable dtResult;


            DBHelper.DBReturnCode retcode = UpdateCreditManager.ValidateLimit(uId, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                //string messege = "";
                decimal AvailableBalance = (decimal)(dtResult.Rows[0]["AvailableCredit"]);
                decimal FixLimt = (decimal)(dtResult.Rows[0]["CreditAmount"]);
                decimal FixLimt_Cmp = (decimal)(dtResult.Rows[0]["CreditAmount_Cmp"]);
                bool FixLimit_Flag = Convert.ToBoolean(dtResult.Rows[0]["Credit_Flag"]);
                string FixLimt_GivenDt = dtResult.Rows[0]["Spn_DateLimit"].ToString();
                string FixLimt_Span = (dtResult.Rows[0]["Spn_NoDay"].ToString());
                var FixLimtdate = DateTime.ParseExact(FixLimt_GivenDt, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                decimal OneTimeLimt = (decimal)(dtResult.Rows[0]["MaxCreditLimit"]);
                decimal OneTime_Cmp = (decimal)(dtResult.Rows[0]["MaxCreditLimit_Cmp"]);
                bool OneTime_Flag = Convert.ToBoolean(dtResult.Rows[0]["OTC"]);
                string OneTime_GivenDt = dtResult.Rows[0]["Spn_OtcLimit"].ToString();
                string OneTime_Span = dtResult.Rows[0]["Spn_OtcDay"].ToString();
                var OneTimeLimtdate = DateTime.ParseExact(FixLimt_GivenDt, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //// for Fix Limt
                //if ((FixLimt != FixLimt_Cmp) && (FixLimtdate.AddDays(3) <= DateTime.Now))
                //{
                //    messege = "Your credit limit span is expired. Kindly credit your account for further bookings.";
                //}

                //if ((OneTimeLimt != OneTime_Cmp) && (OneTimeLimtdate.AddDays(3) <= DateTime.Now))
                //{
                //    messege = "Your One Time limit span is expired. Kindly credit your account for further bookings.";
                //}


                return jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                //return jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"FixLimt\":\"" + FixLimt + "\",\"FixLimt_Cmp\":\"" + FixLimt_Cmp + "\",\"OneTimeLimt\":\"" + OneTimeLimt + "\",\"OneTimeLimt_Cmp\":\"" + OneTime_Cmp + "\"}";

            }
            else
            {
                return jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
    }
}
