﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for UserHanler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UserHanler : System.Web.Services.WebService
    {

        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        [WebMethod(EnableSession = true)]
        public string GetAgentDetail()
        {
            objSerializer.MaxJsonLength = int.MaxValue;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                DataTable dtResult = new DataTable();
                string GroupName = "";
                Session["AgencySearch"] = null;
                if (objGlobalDefault.UserType.Contains("Admin"))
                {
                    var arrAgency = (from obj in DB.tbl_AdminLogins
                                     from objContact in DB.tbl_Contacts
                                     from objCredit in DB.tbl_AdminCreditLimits
                                     where obj.ContactID == objContact.ContactID &&
                                     obj.sid == objCredit.uid && obj.UserType=="Supplier"
                                     select new
                                     {
                                         obj.sid,
                                         obj.AgencyName,
                                         obj.Agentuniquecode,
                                         obj.uid,
                                         obj.password,
                                         objCredit.AvailableCredit,
                                         GroupName = (from objGroupMarkupMapping in DB.tbl_AgentGroupMarkupMappings
                                                      from objGroup in DB.tbl_GroupMarkups
                                                      where objGroupMarkupMapping.GroupId == Convert.ToInt64(objGroup.sid) &&
                                                      objGroupMarkupMapping.AgentId == obj.sid
                                                      select objGroup).FirstOrDefault().GroupName == null ? GroupName : "Group A",
                                         Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                         Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                         ContactPerson = obj.ContactPerson,
                                         Mobile = objContact.Mobile,
                                         obj.dtLastAccess,
                                         obj.EnableCheckAccount,
                                         obj.LoginFlag,
                                         obj.CurrencyCode,
                                         obj.ParentID,
                                         obj.agentCategory,
                                         obj.UserType
                                     }).ToList();
                    GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                    dtResult = lsttodt.ToDataTable(arrAgency);

                }
                else
                {
                    var arrAgency = (from obj in DB.tbl_AdminLogins
                                     from objContact in DB.tbl_Contacts
                                     from objCredit in DB.tbl_AdminCreditLimits
                                     where obj.ContactID == objContact.ContactID &&
                                     obj.sid == objCredit.uid && obj.ParentID == objGlobalDefault.ParentId
                                     select new
                                     {
                                         obj.sid,
                                         obj.AgencyName,
                                         obj.Agentuniquecode,
                                         obj.uid,
                                         obj.password,
                                         objCredit.AvailableCredit,
                                         GroupName = (from objGroupMarkupMapping in DB.tbl_AgentGroupMarkupMappings
                                                      from objGroup in DB.tbl_GroupMarkups
                                                      where objGroupMarkupMapping.GroupId == Convert.ToInt64(objGroup.sid) &&
                                                      objGroupMarkupMapping.AgentId == obj.sid
                                                      select objGroup).FirstOrDefault().GroupName == null ? GroupName : "Group A",
                                         Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                         Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                         ContactPerson = obj.ContactPerson,
                                         Mobile = objContact.Mobile,
                                         obj.dtLastAccess,
                                         obj.EnableCheckAccount,
                                         obj.LoginFlag,
                                         obj.CurrencyCode,
                                         obj.ParentID,
                                         obj.agentCategory,
                                         obj.UserType
                                     }).ToList().OrderBy(d => d.sid).ToList();
                    GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                    dtResult = lsttodt.ToDataTable(arrAgency);
                }
                List<Dictionary<string, object>> arrAgents = JsonStringManager.ConvertDataTable(dtResult);
                Session["AllAgency"] = dtResult;
                dtResult.Dispose();
                return objSerializer.Serialize(new { retCode = 1, List_Agent = arrAgents });
            }
            catch
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetAgencyDetails(Int64 AgentID)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                var arrAgency = new object();
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                     arrAgency = (from obj in DB.tbl_AdminLogins
                                     from objContact in DB.tbl_Contacts
                                     from objCredit in DB.tbl_AdminCreditLimits
                                     where obj.ContactID == objContact.ContactID &&
                                     obj.sid == objCredit.uid && obj.sid == AgentID
                                     select new
                                     {
                                         obj.sid,
                                         obj.AgencyName,
                                         obj.Agentuniquecode,
                                         obj.ContactPerson,
                                         obj.Designation,
                                         objContact.Address,
                                         Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                         Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                         objContact.PinCode,
                                         obj.uid,
                                         obj.password,
                                         objCredit.AvailableCredit,
                                         objContact.phone,
                                         objContact.email,
                                         objContact.Mobile,
                                         objContact.Fax,
                                         objContact.Website,
                                         obj.IATANumber,
                                         obj.UserType,
                                         obj.dtLastAccess,
                                         obj.EnableCheckAccount,
                                         obj.Updatedate,
                                         obj.LoginFlag,
                                         obj.CurrencyCode,
                                         obj.ParentID,
                                         obj.agentCategory,
                                         obj.PANNo,
                                         obj.GSTNumber,

                                     }).FirstOrDefault();
                  
                }
                return objSerializer.Serialize(new { retCode = 1, arrAgency = arrAgency });
            }
            catch
            {
                return objSerializer.Serialize(new { });
            }
        }


        [WebMethod(EnableSession = true)]
        public string Search(string Name, string Type, string Code, string Group, string Status, string Country, string City, string MinBalance)
        {
            DataTable AgencyList = (DataTable)Session["AllAgency"];
            DataRow[] row = null;
            try
            {
                if (Name != "")
                {
                    row = AgencyList.Select("uid like '%" + Name + "%'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();

                }
                if (Type != "")
                {
                    row = AgencyList.Select("UserType like '%" + Type + "%'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (Code != "")
                {
                    row = AgencyList.Select("Agentuniquecode like '%" + Code + "%'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (Group != "" && Group != "Select Any Group")
                {
                    row = AgencyList.Select("GroupName like '%" + Group + "%'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (Status != "")
                {
                    bool Active = true;
                    if (Status == "0")
                        Active = false;
                    row = AgencyList.Select("LoginFlag like '%" + Active + "%'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (MinBalance != "")
                {
                    row = AgencyList.Select("AvailableCredit >='" + MinBalance + "'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (Country != "" && Country != "Select Any Country")
                {
                    row = AgencyList.Select("Country ='" + Country + "'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                if (City != "" && City != "Select Any City")
                {
                    row = AgencyList.Select("Description ='" + City + "'");
                    if (row.Length != 0)
                        AgencyList = row.CopyToDataTable();
                    else
                        AgencyList.Rows.Clear();
                }
                Session["AgencySearch"] = AgencyList;
                List<Dictionary<string, object>> arrAgents = JsonStringManager.ConvertDataTable(AgencyList);
                AgencyList.Dispose();
                return objSerializer.Serialize(new { retCode = 1, List_Agent = arrAgents });
            }
            catch
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }

        }
    }
}
