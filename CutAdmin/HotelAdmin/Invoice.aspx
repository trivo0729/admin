﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="CutAdmin.HotelAdmin.Invoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="../js/libs/jquery-1.10.2.min.js"></script>
   --%>
    <script src="Scripts/BookingList.js?v=1.2"></script>
    <script src="Scripts/Booking.js"></script>
        <style>
        #ConfirmDate {
            z-index: 99999999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotel Booking Report</h1>
            <hr />
            <h2><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Agency </label>
                            <div class="input full-width">
                                <input type="text" id="txt_Agency" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile">
                            <label>Check-In</label>
                             <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Check-In" class="input-unstyled" value="">
                                </span>
                        </div>
                          <div class="three-columns twelve-columns-mobile">
                            <label>Check-In</label>
                             <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Check-Out" class="input-unstyled" value="">
                                </span>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Passenger Name </label>
                            <div class="input full-width">
                                <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                      <div class="three-columns twelve-columns-mobile">
                            <label>Booking Date</label>
                             <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Bookingdate" class="input-unstyled" value="">
                                </span>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Reference No.</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Reference" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Supplier Reference No.</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Supplier" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Supplier</label>
                            <div class="full-width button-height typeboth">
                                <select id="selSupplier" class="select">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Hotel Name</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Hotel" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Location</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Location" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="two-columns twelve-columns-mobile four-columns-tablet">
                            <label>Reservation Status</label>
                            <div class="full-width button-height typeboth">
                                <select id="selReservation" class="select">
                                    <option selected="selected">All</option>
                                    <option>Vouchered</option>
                                    <option>Cancelled</option>
                                    <option>OnRequest</option>
                                    <option>Reconfirmed</option>
                                    <%--<option>Incomplete</option>--%>
                                </select>
                            </div>
                        </div>
                        <div class="two-columns twelve-columns-mobile formBTn searhbtn">
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                        </div>
                        <div class="two-columns twelve-columns-mobile bold">
                            <br />
                            <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_BookingList">
                    <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Ref No.</th>
                            <th scope="col" class="align-center">Supplier</th>
                            <th scope="col" class="align-center">Passenger</th>
                            <th scope="col" class="align-center">Hotel &amp; Location</th>
                            <th scope="col" class="align-center">Check-In</th>
                            <th scope="col" class="align-center">Check-Out</th>
                            <th scope="col" class="align-center">Rooms</th>
                            <th scope="col" class="align-center">Status</th>
                            <th scope="col" class="align-center">Amount</th>
                            <th scope="col" class="align-center">Invoice</th>
                            <th scope="col" class="align-center">Voucher</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
