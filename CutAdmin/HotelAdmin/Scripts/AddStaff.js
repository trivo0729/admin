﻿$(document).ready(function () {
    GetGroups();
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            Success("An error occured while loading countries")
        }
    });

    if (location.href.indexOf('?') != -1) {
        $("#btnAddStaff").val('Update');
        hiddenID = GetQueryStringParamsForStaffAddUpdate('sid');
        var name = GetQueryStringParamsForStaffAddUpdate('sName').replace(/%20/g, ' ');
        //var fullnamesplit = fullname.split(" ");
        var lastname = GetQueryStringParamsForStaffAddUpdate('sLastName').replace(/%20/g, ' ');
        $("#txtFirstName").val(name);
        //$("#txtMiddleName").val(fullnamesplit[1]);
        $("#txtLastName").val(lastname);
        $("#txtDesignation").val(GetQueryStringParamsForStaffAddUpdate('sDesignation').replace(/%20/g, ' '));
        $("#txtAddress").val(GetQueryStringParamsForStaffAddUpdate('sAddress').replace(/%20/g, ' '));
        $("#txtPinCode").val(GetQueryStringParamsForStaffAddUpdate('nPinCode').replace(/%20/g, ' '));
        $("#txtEmail").val(GetQueryStringParamsForStaffAddUpdate('sEmail').replace(/%20/g, ' '));
        $("#txtPhone").val(GetQueryStringParamsForStaffAddUpdate('nPhone').replace(/%20/g, ' '));
        $("#txtMobileNumber").val(GetQueryStringParamsForStaffAddUpdate('nMobile').replace(/%20/g, ' '));
        $("#txtDepartment").val(GetQueryStringParamsForStaffAddUpdate('sDepartment').replace(/%20/g, ' '));
        hiddenLoginFlag = GetQueryStringParamsForStaffAddUpdate('bLoginFlag').replace(/%20/g, ' ');
        setTimeout(function () { $('#selGroup option').filter(function () { return $.trim($(this).text()) == GetQueryStringParamsForStaffAddUpdate('sGroup').replace(/%20/g, ' '); }).attr('selected', true); }, 1000);
        //setTimeout(function () { $("#selGroup").val($("#selGroup").text(GetQueryStringParamsForStaffAddUpdate('sGroup').replace(/%20/g, ' '))); }, 1000);
        setTimeout(function () { $("#selCountry").val(GetQueryStringParamsForStaffAddUpdate('sCountry').replace(/%20/g, ' ')); }, 1500);
        setTimeout(function () { GetCity($("#selCountry").val()); }, 2100);
        var tempCode = GetQueryStringParamsForStaffAddUpdate('sCity').replace(/%20/g, ' ');
        setTimeout(function () { $("#selCity").val(tempCode); }, 2800);
        $("#txtEmail").attr("readonly", true);
    }
    else if (location.href.indexOf('?') == -1) {
        $("#btnAddStaff").val('Add');
        setTimeout(function () { $('#selCountry option').filter(function () { return $.trim($(this).text()) == 'INDIA'; }).attr('selected', true); }, 1500);
        setTimeout(function () {
            GetCity($("#selCountry").val());
        }, 2000);
    }

    function GetCity(recCountry) {
        $.ajax({
            type: "POST",
            url: "Handler/GenralHandler.asmx/GetCity",
            data: '{"country":"' + recCountry + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCity = result.City;
                    if (arrCity.length > 0) {
                        $("#selCity").empty();
                        var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                        for (i = 0; i < arrCity.length; i++) {
                            ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                        }
                        $("#selCity").append(ddlRequest);
                    }
                }
                if (result.retCode == 0) {
                    $("#selCity").empty();
                }
            },
            error: function () {
                Success("An error occured while loading cities")
            }
        });
        //$('#selCity option:selected').val(tempcitycode);
    }

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });
});
var hiddenID;
var hiddenLoginFlag;
var bvalid;
var sFirstName;
var sMiddleName;
var sLastName;
var sDesignation;
var sCountry;
var sCode;
var sPinCode;
var sAddress;
var sPhone;
var sMobile;
var sEmail;
var sGroupId;
var sGroupName;
var sDepartment;

function AddStaff() {
    $("#formStaff label").css("display", "none");
    $("#formStaff label").html("* This field is required");
    debugger;
    sFirstName = $('#txtFirstName').val();
    sMiddleName = $('#txtMiddleName').val();
    sLastName = $('#txtLastName').val();
    sDesignation = $('#txtDesignation').val();
    sCountry = $('#selCountry').val();
    sCode = $('#selCity').val();
    sPinCode = $('#txtPinCode').val();
    sAddress = $('#txtAddress').val();
    sPhone = $('#txtPhone').val();
    sMobile = $('#txtMobileNumber').val();
    sEmail = $('#txtEmail').val();
    sDepartment = $("#txtDepartment").val();
    //sGroupId = $("#selGroup").val();
    //sGroupName = $("#selGroup option:selected").text();
    //sPassword = $('#txtPassword').val();
    //sPassword = "justatest";
    bvalid = Validate_Staff();
    if (bvalid == true) {
        if ($("#btnAddStaff").val() == "Update") {
            debugger;
            $.ajax({
                type: "POST",
                url: "Handler/GenralHandler.asmx/UpdateStaff",
                //data: '{"sid":"' + hiddenID + '","sFirstName":"' + sFirstName + '","sMiddleName":"' + sMiddleName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sMobile":"' + sMobile + '","sEmail":"' + sEmail + '","sPhone":"' + sPhone + '","sAddress":"' + sAddress + '","sCode":"' + sCode + '","sPinCode":"' + sPinCode + '","hiddenLoginFlag":"' + hiddenLoginFlag + '","sGroup":"' + sGroupName + '"}',
                data: '{"sid":"' + hiddenID + '","sFirstName":"' + sFirstName + '","sMiddleName":"' + sMiddleName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sMobile":"' + sMobile + '","sEmail":"' + sEmail + '","sPhone":"' + sPhone + '","sAddress":"' + sAddress + '","sCode":"' + sCode + '","sPinCode":"' + sPinCode + '","hiddenLoginFlag":"' + hiddenLoginFlag + '","Department":"' + sDepartment + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        Success("Something went wrong!");
                        //window.location.href = "StaffDetails.aspx";
                        //                                window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Staff details updated successfully.");
                        setTimeout(function () {
                            window.location.href = "StaffDetails.aspx";
                        }, 2000);
                        
                        
                        return false;
                    }
                    else {
                    }
                },
                error: function () {
                    Success("An error occured while updating Staff details");
                }
            });
        }
        else if ($("#btnAddStaff").val() == 'Add') {
            //.............................
            $.ajax({
                type: "POST",
                url: "Handler/GenralHandler.asmx/CheckForUniqueAgent",
                data: '{"sEmail":"' + sEmail + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Login.aspx";
                        //window.location.href = "Default.aspx";
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        //Success("This email/username is already taken, Please choose another.");
                        Success("This email/username is already taken, Please choose another.")
                        $("#txtEmail").css("border-color", "brown");
                        $("#txtEmail").focus();
                        //$("#txtMobileNumber").css("border-color", "brown");
                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Their is some error in processing your request, Please try again")
                        //Success("Their is some error in processing your request, Please try again");
                        return false;
                    }
                    else if (result.retCode == 2) {
                        //.............................
                        $.ajax({
                            type: "POST",
                            url: "Handler/GenralHandler.asmx/AddNewStaff",
                            //data: '{"sFirstName":"' + sFirstName + '","sMiddleName":"' + sMiddleName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sMobile":"' + sMobile + '","sEmail":"' + sEmail + '","sPhone":"' + sPhone + '","sAddress":"' + sAddress + '","sCode":"' + sCode + '","sPinCode":"' + sPinCode + '","sGroup":"' + sGroupName + '"}',
                            data: '{"sFirstName":"' + sFirstName + '","sMiddleName":"' + "" + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sMobile":"' + sMobile + '","sEmail":"' + sEmail + '","sPhone":"' + "" + '","sAddress":"' + "" + '","sCode":"' + "" + '","sPinCode":"' + "" + '","Department":"' + sDepartment + '" , "hiddenLoginFlag":"' + "" + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.retCode == 1) {
                                    //Success("Staff details added successfully. A mail containing password has been sent to registered mail. Now you can activate the staff");
                                    Success("Staff details added successfully. A mail containing password has been sent to registered mail. Now you can activate the staff")
                                    setTimeout(function () {
                                        window.location.href = "StaffDetails.aspx";
                                    }, 2000);
                                }
                                if (result.retCode == 0) {
                                    Success("Something went wrong!");
                                }
                            },
                            error: function () {
                                Success("An error occured while adding Staff details");
                            }
                        });
                    }
                },
                error: function () {
                }
            });
        }
    }
}

function Validate_Staff() {
    bvalid = true;
    var reg = new RegExp('[0-9]$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    if (sFirstName == "") {
        bvalid = false;
        Success("Enter First Name");
        return
        //$("#lbl_txtFirstName").css("display", "");
    }
    if (sFirstName != "" && reg.test(sFirstName)) {
        bValid = false;
        Success("FirstName must not be numeric.");
        return
        //$("#lbl_txtFirstName").html("* FirstName must not be numeric.");
        //$("#lbl_txtFirstName").css("display", "");
    }
    //if (sMiddleName == "") {
    //    bvalid = false;
    //    $("#lbl_txtMiddleName").css("display", "");
    //}
    //if (sMiddleName != "" && reg.test(sMiddleName)) {
    //    bValid = false;
    //    $("#lbl_txtMiddleName").html("* MiddleName must not be numeric.");
    //    $("#lbl_txtMiddleName").css("display", "");
    //}
    if (sLastName == "") {
        bvalid = false;
        Success("Enter Last Name");
        return
        //$("#lbl_txtLastName").css("display", "");
    }
    if (sLastName != "" && reg.test(sLastName)) {
        bValid = false;
        Success("LastName must not be numeric.");
        return
        //$("#lbl_txtLastName").html("* LastName must not be numeric.");
        //$("#lbl_txtLastName").css("display", "");
    }
    if (sDesignation == "") {
        bvalid = false;
        Success("Enter Designation");
        return
        //$("#lbl_txtDesignation").css("display", "");
    }
    if (sDesignation != "" && reg.test(sDesignation)) {
        bvalid = false;
        Success("Designation must not be numeric at end.");
        return
        //$("#lbl_txtDesignation").html("* Designation must not be numeric at end.");
        //$("#lbl_txtDesignation").css("display", "");
    }
    if (sDepartment == "") {
        bvalid = false;
        Success("Enter Department");
        return
        //$("#lbl_txtDepartment").css("display", "");
    }
    if (sDepartment != "" && reg.test(sDepartment)) {
        bvalid = false;
        Success("Department must not be numeric at end.");
        return
        //$("#lbl_txtDepartment").html("* Department must not be numeric at end.");
        //$("#lbl_txtDepartment").css("display", "");
    }
    //if (sCountry == "-") {
    //    bvalid = false;
    //    $("#lbl_selCountry").css("display", "");
    //}
    //if (sCode == "-") {
    //    bvalid = false;
    //    $("#lbl_selCity").css("display", "");
    //}
    //if (sAddress == "") {
    //    bvalid = false;
    //    $("#lbl_txtAddress").css("display", "");
    //}
    //if (sAddress != "" && reg.test(sAddress)) {
    //    bvalid = false;
    //    $("#lbl_txtAddress").html("* Address must not be numeric at end.");
    //    $("#lbl_txtAddress").css("display", "");
    //}
    //if (sPhone != "" && !(reg.test(sPhone))) {
    //    bvalid = false;
    //    $("#lbl_txtPhoneNumber").html("* Phone Number must be numeric.");
    //    $("#lbl_txtPhoneNumber").css("display", "");
    //}
    if (sMobile == "") {
        bvalid = false;
        Success("Enter Mobile No");
        return
        // $("#lbl_txtMobileNumber").css("display", "");
    }
    if (sMobile != "" && !(reg.test(sMobile))) {
        bvalid = false;
        Success("Mobile Number must be numeric.");
        return
        //$("#lbl_txtMobileNumber").html("* Mobile Number must be numeric.");
        //$("#lbl_txtMobileNumber").css("display", "");
    }
    //if (sGroupId == "-") {
    //    bValid = false;
    //    $("#lbl_Group").css("display", "");
    //}
    if (sEmail == "") {
        bvalid = false;
        Success("Enter Email ID");
        return
        //$("#lbl_txtEmail").html("* This field is required");
        //$("#lbl_txtEmail").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bvalid = false;
            Success("Wrong email format.");
            return
            //$("#lbl_txtEmail").html("* Wrong email format.");
            //$("#lbl_txtEmail").css("display", "");
        }
    }
    // if (sUserName == "") {
    //    $("#lbl_txtUserName").css("display", "");
    //    return false;
    //}
    //if (sPassword == "") {
    //    bvalid = false;
    //    $("#lbl_txtPassword").css("display", "");
    //}
    //else if ($('#txtConfirmPassword').val() == "") {
    //    bvalid = false;
    //    $("#lbl_txtConfirmPassword").css("display", "");
    //    //Success("Please confirm password!")
    //    $('#txtConfirmPassword').focus();
    //}
    //else if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
    //    bvalid = false;
    //    $("#lbl_txtConfirmPassword").css("display", "");
    //    $("#lbl_txtConfirmPassword").html("The passwords you have entered did not matched!");
    //    //Success("The passwords you have entered did not matched!")
    //    //$('#txtConfirmPassword').val('');
    //    $('#txtConfirmPassword').focus();
    //}
    return bvalid;
}

var arrGroup = new Array();
function GetGroups() {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetGroups",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGroup = result.Group;
                if (arrGroup.length > 0) {
                    $('#selGroup').empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Group</option>';
                    for (i = 0; i < arrGroup.length; i++) {
                        ddlRequest += '<option value="' + arrGroup[i].sid + '">' + arrGroup[i].GroupName + '</option>';
                    }
                    $("#selGroup").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetQueryStringParamsForStaffAddUpdate(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

//function GetPassword(encryptpass) {
//    debugger;
//    $.ajax({
//        type: "POST",
//        url: "GenralHandler.asmx/GetPassword",
//        data: '{"password":"' + encryptpass + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                $("#txtPassword").val(result.password);
//            }
//        },
//        error: function () {
//            Success("An error occured while loading password.")
//        }
//    });
//}