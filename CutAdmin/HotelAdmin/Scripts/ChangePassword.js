﻿$(function () {
    //$('[data-toggle="popover"]').popover();
    //$.ajax({
    //    type: "POST",
    //    url: "Handler/GenralHandler.asmx/GetPassword",
    //    data: '{"password":"O0g3AK/TLbnDwQj6V72d0w=="}',
    //    contentType: "application/json; charset=utf-8",
    //    datatype: "json",
    //    success: function (response) {
    //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
    //        debugger;
    //        if (result.retCode == 1) {
    //            pass = result.password;
    //            $("#txt_Password").val(pass);
    //            $("#hddn_Password").val(pass);
    //            $("#txt_SendPassword").val(pass);
    //        }
    //    },
    //    error: function () {
    //        alert("An error occured while loading password.");
    //    }
    //});

    //$.ajax({
    //    type: "POST",
    //    url: "Handler/GenralHandler.asmx/GetAdminProfile",
    //    data: '',
    //    contentType: "application/json; charset=utf-8",
    //    datatype: "json",
    //    success: function (response) {
    //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
    //        if (result.Session == 0 && result.retCode == 0) {
    //            alert("Your session has been expired!")
    //            window.location.href = "../Login.aspx";
    //            //window.location.href = "../Default.aspx";
    //        }
    //        else if (result.retCode == 1) {
    //            arrAgentDetails = result.AgentDetails;
    //            if (arrAgentDetails.length > 0) {
    //                hiddensid = arrAgentDetails[0].sid;
    //                var p = arrAgentDetails[0].password;
    //                setTimeout(function () { GetPassword(arrAgentDetails[0].password) }, 1500);
    //                //$('#hdnpassword').val(arrAgentDetails[0].password);
    //            }

    //        }
    //        else if (result.Session == 1 && result.retCode == 0) {
    //            alert("Something went wrong!")
    //        }
    //    },
    //    error: function () {
    //        // alert("An error occured while loading password")
    //        alert("An error occured while loading password");
    //    }
    //});

});
var hiddensid;

//function GetPassword(encryptpass) {
//    $("#txt_Password").val('');
//    $.ajax({
//        type: "POST",
//        url: "Handler/GenralHandler.asmx/GetPassword",
//        data: '{"password":"' + encryptpass + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            debugger;
//            if (result.retCode == 1) {
//                pass = result.password;
//                $('#hdnpassword').val(pass);
//            }
//        },
//        error: function () {
//            Success("An error occured while loading password.")
//            //alert("An error occured while loading password.")
//        }
//    });
//}

function hidealert() {
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
    $("#alPError").css("display", "none");
}

function ChangePassword() {
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
    $("#alPError").css("display", "none");
    $("#lbl_OldPassword").css("display", "none");
    $("#lbl_OldPassword").html("* This field is required");
    $("#lbl_NewPassword").css("display", "none");
    $("#lbl_ConfirmNewPassword").css("display", "none");
    $("#lbl_ConfirmNewPassword").html("* This field is required");
    var bValid = true;
    var OldPassword = $("#txtOldPassword").val();
    var NewPassword = $("#txtNewPassword").val();
    var ConfirmNewPassword = $("#txtConfirmNewPassword").val();

    if (OldPassword == "") {
        Success("Please Insert Old Password");
        bValid = false;
    }
    if (NewPassword == "") {
        Success("Please Insert New Password");
        bValid = false;
    }
    if (ConfirmNewPassword == "") {
        Success("Please Insert Confirm New Password");
        bValid = false;
    }
    if (ConfirmNewPassword != "" && (NewPassword != ConfirmNewPassword)) {
        bValid = false;
        Success("Confirm Password did not match!");
        bValid = false;
    }
    if (bValid == true) {
        $.modal.confirm('Are you sure you want to change password?', function () {
            $.ajax({
                type: "POST",
                url: "../HotelAdmin/Handler/GenralHandler.asmx/ChangePassword",
                data: '{"sOldPassword":"' + OldPassword + '","sNewPassword":"' + NewPassword + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1 && result.Status == 1) {
                        Success("Your password has been changed successfully!");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                    else if (result.retCode == 0 && result.Status == 0) {
                        Success("The current password you have entered is wrong!");
                    }
                    else if (result.retCode == 0 && result.Status == -1) {
                        Success("Something went wrong! Please contact administrator.");
                    }
                    else if (result.Session == 0 && result.retCode == 0 && result.Status == -1) {
                        Success("Your session has been expired!!")
                    }
                },
            });
        }, function () {
            $('#modals').remove();
        });
    }
}
function changePass(OldPassword, NewPassword) {
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/GenralHandler.asmx/ChangePassword",
        data: '{"sOldPassword":"' + OldPassword + '","sNewPassword":"' + NewPassword + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.Status == 1) {
                Success("Your password has been changed successfully!");
                setTimeout(function () {
                    window.location.reload();
                }, 500)
            }
            else if (result.retCode == 0 && result.Status == 0) {
                Success("The current password you have entered is wrong!");
            }
            else if (result.retCode == 0 && result.Status == -1) {
                Success("Something went wrong! Please contact administrator.");
            }
            else if (result.Session == 0 && result.retCode == 0 && result.Status == -1) {
                Success("Your session has been expired!!")
            }
        },
    });
}
function cleartextbox() {
    $("#txtOldPassword").val('');
    $("#txtNewPassword").val('');
    $("#txtConfirmNewPassword").val('');
}

