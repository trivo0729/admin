﻿
$(document).ready(function () {
    GetLogo();
    CheckSession();
});

function CheckSession() {
    $.ajax({
        url: "../DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0) {
                window.location.href = "../Default.aspx";
            }
        },
        error: function () {
            Success('Error occured while authenticating user!');
            setTimeout(function () {
                window.location.href = "../Default.aspx";
            }, 2000);
            
        }
    });
}


function GetLogo() {
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetLogo",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            AdminName = obj.AdminName;
            AdminCode = obj.AdminCode;
            var ul = '';
            if (obj.retCode == 1) {
                $("#AdminName").text(AdminName);
                $('#AdminLogo').append('<img src="Logo/' + AdminCode + '.jpg" alt="Logo not found" class="logo" />');
            }

            else {

            }
        }
    });

}
