﻿
$(document).ready(function () {
    GetAgentDetail();

});

function GetAgentDetail() {
    $("#tbl_CommInvoiceDetails").dataTable().fnClearTable();
    $("#tbl_CommInvoiceDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/GetAgentDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                var div = "";
                for (var i = 0; i < List_AgentDetails.length; i++) {
                    div += '<option value="' + List_AgentDetails[i].sid + '">' + List_AgentDetails[i].AgencyName + '</option>'
                }

                $("#selSupplier").append(div);
                $("#tbl_CommInvoiceDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_CommInvoiceDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_CommInvoiceDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_CommInvoiceDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

function Search() {
    var Id = $("#selSupplier option:selected").val();
    if (Id == "" || Id == "-") {
        Success("Please Select Supplier.");
        return false;
    }

    var Month = $("#selMonth option:selected").val();
    if (Month == "" || Month == "-") {
        Success("Please Select Month.");
        return false;
    }

    var Year = $("#selYear option:selected").val();
    if (Year == "" || Year == "-") {
        Success("Please Select Year.");
        return false;
    }

    $("#tbl_CommInvoiceDetails").dataTable().fnClearTable();
    $("#tbl_CommInvoiceDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/CommissionReportHandler.asmx/GetCommissionInvoiceReport",
        data: JSON.stringify({ Id: Id, Month: Month, Year: Year }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            $("#UnPaidAmt").empty();
            if (result.retCode == 1) {
                var List_Details = result.Arr; 
                var List_Reser = result.ArrReser;
                var div = "";
                var Amount = 0;
                var Currency = "";
                for (var i = 0; i < List_Details.length; i++) {
                    Amount += List_Details[i].Commission;
                    Currency = List_Details[i].Currency;
                    div += '<tr>';
                    div += '<td class="align-center hide-on-mobile">' + (i+1) + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceDate + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InvoiceNo + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Reser[i].CheckIn + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Reser[i].CheckOut + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Reser[i].NoOfDays + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + Currency+ " " + List_Details[i].InvoiceAmount + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Commission + '</td>';
                   // div += '<td class="align-center hide-on-mobile"><a href="#" class="button" title="Edit" onclick="UpdateModal(\'' + List_Details[i].ID + '\',\'' + List_Details[i].SupplierID + '\',\'' + List_Details[i].Commission + '\',\'' + List_Details[i].UnPaidAmunt + '\')"><span class="icon-pencil"></span></a> </td>';
                    div += '</tr>';
                }
                var AmountCurrency = Currency + " " + Amount;
                $("#UnPaidAmt").text(AmountCurrency);
                $("#tbl_CommInvoiceDetails").append(div);
                $("#tbl_CommInvoiceDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_CommInvoiceDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_CommInvoiceDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_CommInvoiceDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

function ExportCommReportToExcel(Document) {

    var Type = "All";

    window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=CommissionReportAdmin&Type=" + Type + "&Document=" + Document;

}