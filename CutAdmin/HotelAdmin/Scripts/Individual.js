﻿var HiddenId;

function UpdateIndiviualMarkupModal(sid) {
    debugger;
    HiddenId = sid;
    GetIndividualMarkup(sid)

    $.modal({
        content:

                '<div class="standard-tabs margin-bottom" id="add-tabs">' +
               '<div class="respTable">' +
                 '<table class="table responsive-table" id="sorting-advanced">                                                                                                   ' +
                    ' <thead>                                                                                                                                                    ' +
                        ' <tr>                                                                                                                                                   ' +
                         '    <th scope="col">MarkUp Percentage </th>                                                                                                            ' +
                            ' <th scope="col" class="align-center hide-on-mobile">MarkUp Amount</th>                                                                            ' +
                            ' <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage</th>                                                           ' +
                            ' <th scope="col" class="align-center">Commision Amount</th>                                                                                        ' +
                        ' </tr>                                                                                                                                                  ' +
                   '  </thead>                                                                                                                                                   ' +
                    ' <tbody>                                                                                                                                                    ' +
                    '     <tr>                                                                                                                                                   ' +
                    '         <td>                                                                                                                                               ' +
                    '             <div class="input full-width">                                                                                                                 ' +
                    '                 <input name="prompt-value" id="txt_IndMarkPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>        ' +
                    '         </td>                                                                                                                                              ' +
                    '         <td>                                                                                                                                               ' +
                    '             <div class="input full-width">                                                                                                                 ' +
                    '                 <input name="prompt-value" id="txt_IndMarkUpAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>            ' +
                    '         </td>                                                                                                                                              ' +
                    '         <td>                                                                                                                                               ' +
                    '             <div class="input full-width">                                                                                                                 ' +
                    '                 <input name="prompt-value" id="txt_IndCommPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>     ' +
                    '         </td>                                                                                                                                              ' +
                    '         <td>                                                                                                                                               ' +
                    '             <div class="input full-width">                                                                                                                 ' +
                    '                 <input name="prompt-value" id="txt_IndCommAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>         ' +
                    '         </td>                                                                                                                                              ' +
                    '     </tr>                                                                                                                                                  ' +
                    ' </tbody>                                                                                                                                                   ' +
                    '</table><br>' +
                    ' <button style="float:right" type="button" onclick="UpdateIndividualMarkupDetails()" class="button anthracite-gradient UpdateMarkup">Update</button>' +
                  '</div>' +
                  '</div>'
             ,

        title: 'Update Individual Markup',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });

}

function GetIndividualMarkup(sid) {
    $.ajax({
        type: "POST",
        url: "HotelAdmin/handler/MarkUpHandler.asmx/GetIndividualMarkup",
        data: JSON.stringify({ Id: sid }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                var List = result.Arr;

                $("#txt_IndMarkPercentage").val(List[0].MarkupPercentage);
                $("#txt_IndMarkUpAmount").val(List[0].MarkupAmmount);
                $("#txt_IndCommPercentage").val(List[0].CommessionPercentage);
                $("#txt_IndCommAmount").val(List[0].CommessionAmmount);

            }
            else if (result.retCode == 0) {
                $("#txt_IndMarkPercentage").val("0");
                $("#txt_IndMarkUpAmount").val("0");
                $("#txt_IndCommPercentage").val("0");
                $("#txt_IndCommAmount").val("0");
            }
        },
        error: function () {
        }

    });
}

function UpdateIndividualMarkupDetails() {

    var IndMarkPercentage = $("#txt_IndMarkPercentage").val();
    if (IndMarkPercentage == "") {
        Success("Please enter Markup Percentage");
        return false;
    }
    var IndMarkUpAmount = $("#txt_IndMarkUpAmount").val();
    if (IndMarkUpAmount == "") {
        Success("Please enter MarkUp Amount");
        return false;
    }
    var IndCommPercentage = $("#txt_IndCommPercentage").val();
    if (IndCommPercentage == "") {
        Success("Please enter Commission Percentage");
        return false;
    }
    var IndCommAmount = $("#txt_IndCommAmount").val();
    if (IndCommAmount == "") {
        Success("Please enter Commission Amount");
        return false;
    }

    var data = {
        Sid: HiddenId,
        IndMarkPercentage: IndMarkPercentage,
        IndMarkUpAmount: IndMarkUpAmount,
        IndCommPercentage: IndCommPercentage,
        IndCommAmount: IndCommAmount,
    }

    $.ajax({
        type: "POST",
        url: "HotelAdmin/handler/MarkUpHandler.asmx/UpdateIndividualMarkupDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Detail Update successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
              
            }
            else if (result.retCode == 0) {
                Success("Something went wrong.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        },
        error: function () {
        }

    });
}





