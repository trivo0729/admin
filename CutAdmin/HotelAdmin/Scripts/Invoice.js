﻿//$(document).ready(function () {

// //   GetBookingDetails();
//});

var hiddensid;
var ReservationId;
var Latitude;
var Longitude;
var sid;

function IncompleteBookingDetail(InvoiceID) {
    var Data = { InvoiceID: InvoiceID };
    $("#spn_InvoiceNo").empty();
    $("#spn_AgentName").empty();
    $("#spn_HotelName").empty();
    $("#spn_Nationality").empty();
    $("#spn_RoomPrice").empty();
    $("#spn_RoomCount").empty();
    $("#spn_CancellationDate").empty();
    $("#spn_CancellationPrice").empty();
    $("#spn_CheckIn").empty();
    $("#spn_CheckOut").empty();
    $("#spn_AdultCount").empty();
    $("#spn_ChildCount").empty();
    $("#spn_Supplier").empty();
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/GetIncompleteBookingDetail",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = JSON.parse(response.d);
            if (result.retCode == 1) {
                var List = result.List;

                $("#spn_InvoiceNo").append(List[0].ReservationId);
                $("#spn_AgentName").append(result.AgentName);
                $("#spn_HotelName").append(List[0].HotelName);
                $("#spn_Nationality").append(List[0].Nationality);
                $("#spn_RoomPrice").append(List[0].CUTRoomPrice);
                $("#spn_RoomCount").append(List[List.length - 1].RoomNo);
                $("#spn_CancellationDate").append(List[0].CancellationDate);
                $("#spn_CancellationPrice").append(List[0].CancellationPrice);
                $("#spn_CheckIn").append(List[0].CheckIn);
                $("#spn_CheckOut").append(List[0].CheckOut);
                $("#spn_AdultCount").append(List[0].AdultCount);
                $("#spn_ChildCount").append(List[0].ChildCount);
                $("#spn_Supplier").append(List[0].Supplier);
                for (var i = 0; i < List.length; i++) {

                }
                $("#IncompleteBookingDetailModal").modal("show");
            }
            else
                Success("No Record Found");
        },
    });
}

function Reset() {
    $('#txt_AgencyList').val('');
    $('#datepicker3').val('');
    $('#datepicker4').val('');
    $('#Sel_Type').val('');
    $('#txt_PassengerName').val('');
    $('#datepicker5').val('');
    $('#txt_RefNo').val('');
}


function BookingListByDate() {
    debugger;
    $("#tbl_Invoice tbody").empty();
    //$("#tbl_Invoice").dataTable().fnClearTable();
    //$("#tbl_Invoice").dataTable().fnDestroy();
    //var Supp = "MGH"
    var dFrom;
    var dTo;
    var tRow;
    var Type;
    var Passenger;
    var BookingDt;
    var RefNo;
    var SuppRefNo;
    var Supplier;
    var HotelName;
    var Destination;
    SearchBit = true;

    sid = $('#txt_AgencyList').val();
    var From = $('#datepicker3').val();
    var To = $('#datepicker4').val();
    Type = $('#Sel_Type').val();
    Passenger = $('#txt_PName').val();
    BookingDt = $('#datepicker5').val();
    RefNo = $('#txt_Reference').val();
    SuppRefNo = $('#txt_SupRefNo').val();
    Supplier = $('#ddl_suppliers option:selected').val();
    HotelName = $('#txt_hotel').val();
    Destination = $('#txt_location').val();
    var data = {
        Agent: sid,
        From: From,
        To: To,
        Type: Type,
        Passenger: Passenger,
        ResDt: BookingDt,
        RefNo: RefNo,
        SuppRefNo: SuppRefNo,
        Supplier: Supplier,
        HotelName: HotelName,
        Destination: Destination

    }

    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/BookingListByDate",
        //data: '{"Agent":"' + sid + '","From":"' + From + '","To":"' + To + '","Type":"' + Type + '","Passenger":"' + Passenger + '","ResDt":"' + BookingDt + '","RefNo":"' + RefNo + '","SuppRefNo":"' + SuppRefNo + '","Supplier":"' + Supplier + '","HotelName":"' + HotelName + '","Destination":"' + Destination + '"}',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var arrAgentList = result.ReservationDetails;

                if (arrAgentList != 0) {
                    $('#lblStatus').css("display", "none");
                    var tRow = '';

                    for (var i = 0; i < arrAgentList.length; i++) {
                        UID = arrAgentList[i].Uid;
                        var strCin = arrAgentList[i].CheckIn;
                        var ret1 = strCin.split(" ");
                        var CheckIn = ret1[0];

                        var strCout = arrAgentList[i].CheckOut;
                        var ret2 = strCout.split(" ");
                        var CheOut = ret2[0];

                        var Reservation = arrAgentList[i].ReservationDate;
                        var ret3 = Reservation.split(" ");
                        var ReservationDate = ret3[0];

                        var DeadlineDate = arrAgentList[i].DeadLine;
                        var DD = DeadlineDate.split(" ");

                        var HoldTime = arrAgentList[i].HoldTime;
                        var HT = HoldTime.split(" ");

                        var sts = arrAgentList[i].Status;
                        sts.replace("Booking", "On Hold")

                        tRow += '<tr>';
                        tRow += '<td align="center" style = "width:5%">' + (i + 1) + '</td>';
                        tRow += '<td>' + ReservationDate + '</td>';
                        tRow += '<td style="width:10%">' + arrAgentList[i].InvoiceID + '</td>';
                        tRow += '<td align="center">' + arrAgentList[i].AgencyName + '</td>';

                        if (arrAgentList[i].IsConfirm == "True") {
                            tRow += '<td ><a style="cursor:pointer">' + arrAgentList[i].Source + '<br/> ' + arrAgentList[i].ReservationID + '</a></td>';
                        }
                        else {
                            tRow += '<td ><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBookingDetail(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\',\'' + arrAgentList[i].AgencyName + '\')">' + arrAgentList[i].Source + '<br/> ' + arrAgentList[i].ReservationID + '</a></td>';
                        }
                        //  align="center" onclick="ConfirmHoldBookingDetail(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\',\'' + DD[0] + '\')">' + arrAgentList[i].ReservationID + '</td>';


                        tRow += '<td>' + arrAgentList[i].bookingname + '</td>';
                        tRow += '<td>' + arrAgentList[i].HotelName + "/" + arrAgentList[i].City + '</td>';
                        tRow += '<td style="width:10%">' + CheckIn + '</td>';
                        tRow += '<td style="width:10%">' + CheOut + '</td>';
                        tRow += '<td style="width:10%">' + " " + '</td>';

                        ////////// edited for hold date update modal ///////
                        //if (arrAgentList[i].Status == "Booking") {
                        //    tRow += '<td><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBooking(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\')">On Hold</a></td>';
                        //    tRow += '<td>' + HT[0] + '</td>';
                        //}
                        //else {
                        //    tRow += '<td><a style="cursor:pointer" title="Update Date" onclick="UpdateHoldBookingDate(\'' + arrAgentList[i].ReservationID + '\',\'' + DeadlineDate + '\',\'' + HoldTime + '\')">' + arrAgentList[i].Status + ' </a></td>';
                        //    tRow += '<td>' + DD[0] + '</td>';
                        //}
                        /////////////  End ////////////////////

                        //if (arrAgentList[i].Status == "Booking") {
                        //    tRow += '<td><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBooking(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\')">On Hold</a></td>';
                        //    tRow += '<td>' + HT[0] + '</td>';
                        //}
                        //else if (arrAgentList[i].Status == "Vouchered") {
                        //    tRow += '<td><a style="cursor:pointer" title="Update Date" onclick="UpdateHoldBookingDate(\'' + arrAgentList[i].ReservationID + '\',\'' + DeadlineDate + '\',\'' + HoldTime + '\')">' + arrAgentList[i].Status + ' </a></td>';
                        //    tRow += '<td>' + DD[0] + '</td>';
                        //}
                        //else {
                        //    tRow += '<td><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBooking(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\')">On Hold</a></td>';
                        //    tRow += '<td>' + HT[0] + '</td>';
                        //}

                        if (arrAgentList[i].Status == "Hold") {
                            //   tRow += '<td><a style="cursor:pointer" title="Update Date" onclick="UpdateHoldBookingDate(\'' + arrAgentList[i].ReservationID + '\',\'' + DeadlineDate + '\',\'' + HoldTime + '\')">' + arrAgentList[i].Status + ' </a></td>';
                            tRow += '<td><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBooking(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\',\'' + arrAgentList[i].AffilateCode + '\',\'' + DeadlineDate + '\',\'' + HoldTime + '\')">On Hold</a></td>';

                            tRow += '<td>' + DD[0] + '</td>';
                        }
                        else {
                            tRow += '<td><a style="cursor:pointer">' + arrAgentList[i].Status + ' </a></td>';
                            tRow += '<td>' + HT[0] + '</td>';
                        }


                        //tRow += '<td>' + sts.replace("Booking", "On Hold") + '</td>';
                        //tRow += '<td>' + DD[0] + '</td>';
                        tRow += '<td><span><i class="' + arrAgentList[i].Currency + '"></i>&nbsp' + numberWithCommas((parseFloat(arrAgentList[i].TotalFare))) + '</span></td>';

                        //--For Invoice
                        tRow += '<td align="center" style="width:8%"><a style="cursor:pointer" title="Invoice" onclick="GetPrintInvoice(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\')">View</a></td>';
                        //-- For Voucher
                        tRow += '<td align="center" style="width:8%"><a style="cursor:pointer" title="Voucher" onclick="GetPrintVoucher(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\')">View</a></td>';
                        //if (arrAgentList[i].Status == "Cancelled") {
                        //    tRow += '<td style="width:10%"><a style="cursor:pointer" title="Voucher"  onclick="VoucherDetails(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\',\'' + Supp + '\')"><span class="glyphicon glyphicon-envelope">&nbsp</span></a><a style="cursor:pointer" title="Print" onclick="GetPrintVoucher(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\')"><span class="glyphicon glyphicon-print">&nbsp</span></a><a style="cursor:pointer" title="PDF"  onclick="VoucherDetails(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Latitude + '\',\'' + arrAgentList[i].Longitude + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\')"><span class="glyphicon glyphicon-file"></span></a></td>';
                        //}
                        //else if (arrAgentList[i].Status == "Booking") {

                        //    tRow += '<td style="width:10%"><a style="cursor:pointer" title="Voucher"  onclick="VoucherDetails(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\',\'' + Supp + '\')"><span class="glyphicon glyphicon-envelope">&nbsp</span></a><a style="cursor:pointer" title="Print" onclick="GetPrintVoucher(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\')"><span class="glyphicon glyphicon-print">&nbsp</span></a><a style="cursor:pointer" title="PDF"  onclick="VoucherDetails(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].Latitude + '\',\'' + arrAgentList[i].Longitude + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\')"><span class="glyphicon glyphicon-file"></span></a></td>';
                        //}

                        //else {
                        //    var l = arrAgentList[i].LatitudeMGH;
                        //    var v = arrAgentList[i].LatitudeMGH;
                        //    tRow += '<td style="width:10%"><a style="cursor:pointer" title="Voucher" data-toggle="modal" data-target="#VoucherModal" onclick="VoucherDetails(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + l + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\',\'' + Supp + '\')"><span class="glyphicon glyphicon-envelope">&nbsp</span></a><a style="cursor:pointer" title="Print" onclick="GetPrintVoucher(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].Status + '\',\'' + arrAgentList[i].Source + '\')"><span class="glyphicon glyphicon-print">&nbsp</span></a><a style="cursor:pointer" title="PDF"  onclick="GetPDFVoucher(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Uid + '\',\'' + arrAgentList[i].LatitudeMGH + '\',\'' + arrAgentList[i].LongitudeMGH + '\',\'' + arrAgentList[i].NoOfDays + '\',\'' + arrAgentList[i].Status + '\',\'' + Supp + '\')"><span class="glyphicon glyphicon-file"></span></a></td>';
                        //}

                        //-- For Cancellation
                        if (arrAgentList[i].Status == "Cancelled")
                            tRow += '<td><a style="cursor:pointer" title="Cancelled" onclick="CancelledBooking()"><span class="glyphicon glyphicon-remove-sign">&nbsp</span></a></td>';
                        else if (arrAgentList[i].Status == "Vouchered")
                            tRow += '<td><a style="cursor:pointer" title="Cancel" onclick="OpenCancellationPopup(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Status + '\')"><span class="glyphicon glyphicon-remove-sign">&nbsp</span></a></td>';
                        else if (arrAgentList[i].Status == "Booking")
                            tRow += '<td><a style="cursor:pointer" title="Confirm" onclick="OpenConfirmationPopup(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Status + '\')"><span class="glyphicon glyphicon-saved">&nbsp</span></a><a style="cursor:pointer" title="Cancel" onclick="OpenCancellationPopup(\'' + arrAgentList[i].ReservationID + '\',\'' + arrAgentList[i].Status + '\')"><span class="glyphicon glyphicon-remove-sign">&nbsp</span></a></td>';
                        tRow += '</tr>';
                    }


                    $("#tbl_Invoice").append(tRow);
                    //$("#tbl_Invoice tbody").html(tRow);
                    //$("#tbl_Invoice").dataTable({
                    //     bSort: false, sPaginationType: 'full_numbers',
                    //});
                }
                else {
                    $('#lblStatus').css("display", "block");
                }
            }
        },
        error: function () {
        }
    });


}

function GetPrintInvoice(ReservationID, Uid, Status, Type) {
    //if (Status == 'Vouchered' || Status == 'Cancelled') {

    if (Status == Cancelled)

        if (Type == "HotelBeds") {
            Type = "A"
        }

    if (Type == "MGH") {
        Type = "B"
    }

    if (Type == "DoTW") {
        Type = "C"
    }

    //var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
    var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '&Supplier=' + Type, '_blank');
    //window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    //}
    //else {
    //    //$('#SpnMessege').text('Please Confirm Your Booking!')
    //    //$('#ModelMessege').modal('show')
    //    alert('Please Confirm Your Booking!')
    //}
}

function GetPrintVoucher(ReservationID, Uid, Latitude, Longitude, Status, Type) {
    debugger;
    if (Status == 'Vouchered') {

        var win = window.open('ViewVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
        //window.open('../Agent/Voucher.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else if (Status == 'Booking') {
        Success('Please Confirm Your Booking!')
    }
    else
        Success('You cannot get voucher for cancelled booking!')
}

var Ammount;
var arrHotelReservation = new Array();
var arrBookedPassenger = new Array();
var arrBookedRoom = new Array();
var arrBookingTransactions = new Array();
var arrAgentDetails = new Array();

//function GetPrintInvoice(ReservationID, Uid, Status) {
//    debugger;
//    if (Status == 'Vouchered' || Status == 'Cancelled') {
//        window.open('../Agent/Invoice.html?ReservationId=' + ReservationID + '&Uid=' + Uid, 'tester', 'left=50000,top=50000,width=800,height=600');
//    }
//    else {
//        alert('Please Confirm Your Booking!')
//    }
//}

function InvoicePrint(ReservationID, Uid) {
    debugger;

    $("#tblCreditDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/BookingDetails",
        data: '{"ReservationID":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelReservation = result.HotelReservation;
                arrBookedPassenger = result.BookedPassenger;
                arrBookedRoom = result.BookedRoom;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                debugger;
                var strCin = arrHotelReservation[0].CheckIn;
                var ret1 = strCin.split(" ");
                var CheckInDate = ret1[0];
                //var CRET1 = CheckInDate.split("-");
                //CheckInDate = CRET1[2] + '/' + CRET1[1] + '/' + CRET1[0]


                var strCout = arrHotelReservation[0].CheckOut;
                var ret2 = strCout.split(" ");
                var CheckOutDate = ret2[0];
                //var CRET2 = CheckOutDate.split("-");
                //CheckOutDate = CRET2[2] + '/' + CRET2[1] + '/' + CRET2[0]

                var Reservation = arrHotelReservation[0].ReservationDate;
                var ret3 = Reservation.split(" ");
                var ReservationDateBooking = ret3[0];
                //var CRET3 = ReservationDateBooking.split("-");
                //ReservationDateBooking = CRET3[2] + '/' + CRET3[1] + '/' + CRET3[0]

                var Nights = (arrHotelReservation[0].NoOfDays < 10) ? '0' + arrHotelReservation[0].NoOfDays : '' + arrHotelReservation[0].NoOfDays;

                $('#spnInvoice').text(arrHotelReservation[0].InvoiceID);
                $('#spnVoucher').text(arrHotelReservation[0].VoucherID);
                $('#spnChkIn').text(CheckInDate);
                $('#spnChkOut').text(CheckOutDate);
                $('#spnHotelName').text(arrHotelReservation[0].HotelName);
                $('#spnNights').text(Nights);
                $('#spnStatus').text(arrHotelReservation[0].Status);
                $('#spnDate').text(ReservationDateBooking);
                $('#spnAgentRef').text(arrHotelReservation[0].AgentRef);
                $('#spnHotelDestination').text(arrHotelReservation[0].City);



                $('#spnName').text(arrAgentDetails[0].AgencyName);
                $('#spnAdd').text(arrAgentDetails[0].Address);
                $('#spnCity').text(arrAgentDetails[0].Description);
                $('#spnCountry').text(arrAgentDetails[0].Countryname);
                $('#spnPhone').text(arrAgentDetails[0].phone);
                $('#spnemail').text(arrAgentDetails[0].email);


                var Night = arrHotelReservation[0].NoOfDays;
                var SalesTax = arrBookingTransactions[0].SeviceTax;
                SalesTax = parseFloat(SalesTax).toFixed(2);
                Ammount = arrBookingTransactions[0].BookingAmtWithTax;
                Ammount = parseFloat(Ammount).toFixed(2);


                // Convert numbers to words
                // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
                // permission to use this Javascript on your web page is granted
                // provided that all of the code (including this copyright notice) is
                // used exactly as shown (you can change the numbering system if you wish)
                var words;
                // American Numbering System
                var th = ['', 'thousand', 'million', 'billion', 'trillion'];
                // uncomment this line for English Number System
                // var th = ['','thousand','million', 'milliard','billion'];

                var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']; var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']; var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']; function toWords(s) { s = s.toString(); s = s.replace(/[\, ]/g, ''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i = 0; i < x; i++) { if ((x - i) % 3 == 2) { if (n[i] == '1') { str += tn[Number(n[i + 1])] + ' '; i++; sk = 1; } else if (n[i] != 0) { str += tw[n[i] - 2] + ' '; sk = 1; } } else if (n[i] != 0) { str += dg[n[i]] + ' '; if ((x - i) % 3 == 0) str += 'hundred '; sk = 1; } if ((x - i) % 3 == 1) { if (sk) str += th[(x - i - 1) / 3] + ' '; sk = 0; } } if (x != s.length) { var y = s.length; str += 'point '; for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' '; } return str.replace(/\s+/g, ' '); }

                words = toWords(Ammount);

                var tRow = '';
                tRow += '<tr>' +
                    '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px ">' +
                    '<span>No.</span>' +
                    '</td>' +
                    '<td style="width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">' +
                    '<span>Room Type</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center">' +
                    '<span>Board</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center">' +
                    '<span>Rooms</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Nights</span>' +
                    '</td>' +
                    '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Rate</span>' +
                    '</td>' +
                    '<td style="width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">' +
                    '<span>Total</span>' +
                    '</td>' +
                    '</tr>';
                for (var i = 0; i < arrBookedRoom.length; i++) {

                    tRow += '<tr style="border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A">';
                    tRow += '<td align="center" style="width:30px; border: none; text-align: left; padding-left: 15px;">' + (i + 1) + '</td>';
                    tRow += '<td style="border: none">' + arrBookedRoom[i].RoomType + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + arrBookedRoom[i].BoardText + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + arrBookedRoom[i].RoomNumber + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;" >' + Night + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + (parseFloat(arrBookedRoom[i].RoomAmount).toFixed(2)) + '</td>';
                    tRow += '<td align="center" style="border: none; padding-bottom: 8px; padding-top: 8px;">' + (Night) * (parseFloat(arrBookedRoom[i].RoomAmount).toFixed(2)) + '</td>';
                    tRow += '</tr>';

                }
                tRow += '<tr style="border: none; background-color: #E6E7E9; text-align: right">' +
                        '<td align="right" colspan="6" style="border: none; width: 20px; padding-right: 25px; color: #57585A; ">' +
                        '<span><b> Service Tax (1.4%)</b></span>' +
                        '</td>' +
                        '<td style="border: none; width: 20px; text-align: center; color: #57585A"><b>' + SalesTax + '</b></td>' +
                        '</tr>';

                tRow += '<tr border="1" style="border-spacing:0px">' +
                       '<td colspan="5" align="left" style="height: 35px;  background-color: #00AEEF; color: white; font-size: 15px; padding-left: 10px;  border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                       '<b> In Words:</b> <span>: ' + words + '</span>' +
                       '</td>' +
                       '<td align="center" style="height: 35px; background-color: #27B4E8; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                       '<span>Total Amount</span>' +
                       '</td>' +
                      '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 18px; padding-left: 10px; font-weight: 700; border-width: 3px 0px 0px 0px; border-top-color: #E6DCDC;">' +
                      '<span>₹' + Ammount + ' </span>' +
                      '</td>' +
                      '</tr>';
                $("#tbl_Rate tbody").append(tRow);
                $("#tbl_Rate tbody").html(tRow);
                //--------------Cancellation Table----------------


                var TRow = '';
                TRow = '<tr style="border: none">' +

                '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px ">' +
                    '<span>No.</span>' +
                '</td>' +
                '<td style="width:190px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">' +
                    '<span>Room Type</span>' +
                '</td>' +

                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Rooms</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Cancellation After</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Charge/Unit</span>' +
                '</td>' +
                '<td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700; ">' +
                    '<span>Total Charge</span>' +
                '</td>' +
            '</tr>';
                for (var i = 0; i < arrBookedRoom.length; i++) {

                    var CancellationAmt = arrBookedRoom[0].CanAmtWithTax;

                    if (CancellationAmt.indexOf('|') == true) {
                        var CanAmt = CancellationAmt.split('|');
                        CanAmt[0];
                        CanAmt[1];

                        CancellationAmt = (parseFloat(CanAmt[0]).toFixed(2)) + '|' + (parseFloat(CanAmt[1]).toFixed(2));
                        var CancellationAmtWNight = ((parseFloat(CanAmt[0]).toFixed(2)) * Night) + '|' + ((parseFloat(CanAmt[1]).toFixed(2)) * Night);
                    }
                    else {
                        CancellationAmtWNight = (parseFloat(CancellationAmt).toFixed(2) * Night);
                    }

                    TRow += '<tr style="border: none; background-color: #E6E7E9; text-align: center; color: #57585A">';
                    TRow += '<td align="center" style="width:30px; border: none; text-align: left; padding-left: 15px;">' + (i + 1) + ' </td>';
                    TRow += '<td style="border: none;text-align: left"><span> ' + arrBookedRoom[i].RoomType + '</span> </td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; ">  <span>' + arrBookedRoom[i].RoomNumber + ' </span></td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "> <span>' + arrBookedRoom[i].CutCancellationDate + '</span> </td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "><span>₹' + (parseFloat(CancellationAmt).toFixed(2)) + '</span></td>';
                    //TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "><span>₹' + (parseFloat(arrBookedRoom[i].CanAmtWithTax).toFixed(2)) + '</span></td>';
                    TRow += '<td style="border: none; padding-bottom: 8px; padding-top: 8px; "> <span> ₹' + CancellationAmtWNight + '</span></td>';
                    TRow += '</tr>';


                }
                TRow += '<tr style="border: none; background-color: #E6E7E9; text-align: left;">' +
                       '<td colspan="7" style="border: none; width: 20px; padding: 0px 15px 15px 25px; color: #ECA236; ">' +
                       '*Dates & timing will calculated based on local timing </td>' +
                       '</tr>';

                $("#tbl_Cancellation tbody").append(TRow);
                $("#tbl_Cancellation tbody").html(TRow);






            }
        },
        error: function () {
            Success("An error occured while loading credit information")
        }
    });

}



function InvoiceDetails(Resevationid, Uid, Nights, status) {
    debugger;
    if (status == 'Booking') {
        Success('Please Confirm Your Booking! ')
    }
    UID = Uid;
    ReservationId = Resevationid;
    Status = status;
    Night = (Nights < 10) ? '0' + Nights : '' + Nights;
}


function MailInvoice() {
    ReservationId = GetQueryStringParams('ReservationId');
    Status = GetQueryStringParams('Status');
    UID = GetQueryStringParams('Uid');
    //if (Status == 'Vouchered' || Status == 'Cancelled') {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;
    var sEmail = $("#txt_sendInvoice").val();
    if (sEmail == "") {
        bValid = false;
        $("#lbl_txt_sendInvoice").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            $("#lbl_txt_sendInvoice").html("* Wrong email format.");
            $("#lbl_txt_sendInvoice").css("display", "");
        }
    }

    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../EmailHandler.asmx/SendInvoice",
            data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '","Night":"' + Night + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Some error occured, Please try again in some time.");
                    return false;
                }
                if (result.retCode == 1) {
                    Success("Email has been sent successfully.");
                    setTimeout(function () {
                        window.location.href = "Invoice.aspx";
                    }, 2000);
                   
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
                else if (result.retCode == 0) {
                    Success("Sorry Please Try Later.");
                    setTimeout(function () {
                        window.location.href = "Invoice.aspx";
                    }, 2000);
                  
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
                else if (result.retCode == 2) {
                    Success("Sorry, No account found with this email id.");
                    setTimeout(function () {
                        window.location.href = "Invoice.aspx";
                    }, 2000);
                  
                    //                    window.location.href = "../CUT/Default.aspx";
                    return false;
                }
            },
            error: function () {
                Success('Something Went Wrong');
            }

        });
    }
    //}
    //else {
    //    alert('Please Confirm Your Booking!!!!!');
    //    window.location.href = "Invoice.aspx";
    //}
}

function MailCommissionInvoice() {
    ReservationId = GetQueryStringParams('ReservationId');
    Status = GetQueryStringParams('Status');
    UID = GetQueryStringParams('Uid');
    //if (Status == 'Vouchered' || Status == 'Cancelled') {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    var bValid = true;
    var sEmail = $("#txt_sendInvoice").val();
    if (sEmail == "") {
        bValid = false;
        $("#lbl_txt_sendInvoice").css("display", "");
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            $("#lbl_txt_sendInvoice").html("* Wrong email format.");
            $("#lbl_txt_sendInvoice").css("display", "");
        }
    }


    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../EmailHandler.asmx/SendCommissionInvoice",
            data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '","Night":"' + Night + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Some error occured, Please try again in some time.");
                    return false;
                }
                if (result.retCode == 1) {
                    Success("Email has been sent successfully.");
                    setTimeout(function () {
                        window.location.href = "CommissionStatement.aspx";
                    }, 2000);
                }
                else if (result.retCode == 0) {
                    Success("Sorry Please Try Later.");
                    setTimeout(function () {
                        window.location.href = "CommissionStatement.aspx";
                    }, 2000);
                }
                else if (result.retCode == 2) {
                    Success("Sorry, No account found with this email id.");
                    setTimeout(function () {
                        window.location.href = "CommissionStatement.aspx";
                    }, 2000);
                }
            },
            error: function () {
                Success('Something Went Wrong');
            }

        });
    }
    //}
    //else {
    //    alert('Please Confirm Your Booking!!!!!');
    //    window.location.href = "Invoice.aspx";
    //}
}


var Ammount;



function VoucherPrint(ReservationID, Uid) {
    debugger;
    var arrHotelReservation = new Array();
    var arrBookedPassenger = new Array();
    var arrBookedRoom = new Array();
    var arrBookingTransactions = new Array();
    var arrAgentDetails = new Array();

    //var ReservationID;
    $("#tblCreditDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/BookingDetails",
        data: '{"ReservationID":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelReservation = result.HotelReservation;
                arrBookedPassenger = result.BookedPassenger;
                arrBookedRoom = result.BookedRoom;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                debugger;
                var strCin = arrHotelReservation[0].CheckIn;
                var ret1 = strCin.split(" ");
                var CheckInDate = ret1[0];
                //var CRET1 = CheckInDate.split("-");
                //CheckInDate = CRET1[2] + '/' + CRET1[1] + '/' + CRET1[0]


                var strCout = arrHotelReservation[0].CheckOut;
                var ret2 = strCout.split(" ");
                var CheckOutDate = ret2[0];
                //var CRET2 = CheckOutDate.split("-");
                //CheckOutDate = CRET2[2] + '/' + CRET2[1] + '/' + CRET2[0]

                var Reservation = arrHotelReservation[0].ReservationDate;
                var ret3 = Reservation.split(" ");
                var ReservationDateBooking = ret3[0];
                //var CRET3 = ReservationDateBooking.split("-");
                //ReservationDateBooking = CRET3[2] + '/' + CRET3[1] + '/' + CRET3[0]
                var Nights = (arrHotelReservation[0].NoOfDays < 10) ? '0' + arrHotelReservation[0].NoOfDays : '' + arrHotelReservation[0].NoOfDays;

                var agentcode = arrAgentDetails[0].Agentuniquecode;

                $('#spnVoucher').text(arrHotelReservation[0].VoucherID);
                $('#spnChkIn').text(CheckInDate);
                $('#spnChkOut').text(CheckOutDate);
                $('#spnHotelName').text(arrHotelReservation[0].HotelName);
                $('#spnNights').text(Nights);
                //$('#spnStatus').text(arrHotelReservation[0].Status);
                $('#spnDate').text(ReservationDateBooking);
                $('#spnAgentRef').text(arrHotelReservation[0].AgentRef);

                //$('#spnName').text(arrAgentDetails[0].AgencyName);
                $('#spnAdd').text(arrAgentDetails[0].Address);
                $('#spnCity').text(arrAgentDetails[0].Description);
                $('#spnCountry').text(arrAgentDetails[0].Countryname);
                $('#spnPhone').text(arrAgentDetails[0].phone);
                $('#AgentLogo').append('<img src="../AgencyLogos/' + agentcode + '.png" alt="Logo not found" height="auto" width="auto" class="logo" />');
                //$('#spnemail').text(arrAgentDetails[0].email);
                var tRow = '';

                for (var i = 0; i < arrBookedRoom.length; i++) {
                    debugger;

                    //if ((arrBookedPassenger.length) > 1) {

                    for (var j = 0; j < arrBookedPassenger.length; j++) {

                        if (arrBookedRoom[i].RoomCode == arrBookedPassenger[j].RoomCode) {

                            //var v1 = arrBookedPassenger[j + 1].RoomCode;
                            //var v2 = arrBookedPassenger[j + 2].RoomCode;

                            tRow += '<tr style="border-spacing: 0px;">';
                            tRow += '<td style="border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700">' + arrBookedPassenger[j].RoomNumber + '</td>';
                            tRow += '<td colspan="7" style="border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700; ">';
                            tRow += 'Guest Name :&nbsp;&nbsp;&nbsp; <span> ' + arrBookedPassenger[j].Name + ' ' + ' ' + arrBookedPassenger[j].LastName + '</span></td>';
                            tRow += '</tr>';

                            try {
                                if (typeof arrBookedPassenger[j + 1].RoomCode !== 'undefined' && arrBookedRoom[i].RoomCode == arrBookedPassenger[j + 1].RoomCode == arrBookedPassenger[j].RoomCode) {
                                    tRow += '<tr style="border-spacing: 0px;">';
                                    tRow += '<td style="border-left: none; border-right: none; border-top: none; background-color: white; color: white; font-size: 18px;  padding-left:10px; font-weight:700"></td>';
                                    tRow += '<td colspan="7" style="border-left: none; border-top: none; border-right: none;  height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700; ">';
                                    tRow += 'Guest Name :&nbsp;&nbsp;&nbsp; <span> ' + arrBookedPassenger[j + 1].Name + ' ' + ' ' + arrBookedPassenger[j + 1].LastName + '</span></td>';
                                    tRow += '</tr>';


                                    //-----------------------------------------------------------------------------------------
                                    try {

                                        if (typeof arrBookedPassenger[j + 2].RoomCode !== 'undefined' && arrBookedRoom[i].RoomCode == arrBookedPassenger[j + 2].RoomCode) {
                                            tRow += '<tr style="border-spacing: 0px;">';
                                            tRow += '<td style="border-left: none; border-right: none; border-top: none; background-color: white; color: white; font-size: 18px;  padding-left:10px; font-weight:700"></td>';
                                            tRow += '<td colspan="7" style="border-left: none; border-top: none; border-right: none;  height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700; ">';
                                            tRow += 'Guest Name :&nbsp;&nbsp;&nbsp; <span> ' + arrBookedPassenger[j + 2].Name + ' ' + ' ' + arrBookedPassenger[j + 2].LastName + '</span></td>';
                                            tRow += '</tr>';

                                        }

                                        //else {
                                        //    j = j + 1;
                                        //    break;

                                        //}

                                    }
                                    catch (error) {

                                        break;
                                        //continue;

                                    }
                                }

                                //else {
                                //    j = j + 1;
                                //    break;

                                //}
                            }
                            //-----------------------------------------------------------------------------------------


                            catch (error) {
                                break;
                            }


                        }

                        //else {
                        //    j = j + 1;
                        //    break;


                        //}
                    }

                    tRow += '<tr style="border: none; text-align: center; color: #57585A; font-weight:600">';
                    tRow += '<td align="left" style="border: none; padding-top: 15px; padding: 15px 0px 0px 10px; ">Room Type</td>';
                    tRow += '<td style="border: none; padding-top: 15px"> Board </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Total Rooms</td>';
                    tRow += '<td style="border: none; padding-top: 15px"> Adults </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Child </td>';
                    tRow += '<td style="border: none; padding-top: 15px">Child Age</td>';
                    tRow += '<td align="left" style="border: none; padding-top: 15px">Remark</td>';
                    tRow += '</tr>';
                    tRow += '<tr style="color: #B0B0B0; border: none; text-align: center; color: #57585A;">';
                    tRow += '<td align="left" style="border: none; padding:12px 0px 15px 10px; "> ' + arrBookedRoom[i].RoomType + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].BoardText + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].TotalRooms + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Adults + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Child + '</td>';
                    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].ChildAge + '</td>';
                    tRow += '<td align="left" style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Remark + '</td>';
                    tRow += '</tr>';




                }
                $("#tbl_RoomDetails tbody").append(tRow);
                $("#tbl_RoomDetails tbody").html(tRow);




                //for (var i = 0; i < arrBookedRoom.length; i++) {
                //    debugger;

                //    tRow += '<tr style="border-spacing: 0px;">';
                //    tRow += '<td style="border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: #00AEEF; color: white; font-size: 18px;  padding-left:10px; font-weight:700">' + arrBookedRoom[i].RoomNumber + '</td>';
                //    tRow += '<td colspan="7" style="border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: #35C2F1; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700; ">';
                //    tRow += 'Guest Name :&nbsp;&nbsp;&nbsp; <span> ' + arrBookedRoom[i].LeadingGuest + '</span></td>';
                //    tRow += '</tr>';
                //    tRow += '<tr style="border: none; text-align: center; color: #57585A; font-weight:600">';
                //    tRow += '<td align="left" style="border: none; padding-top: 15px; padding: 15px 0px 0px 10px; ">Room Type</td>';
                //    tRow += '<td style="border: none; padding-top: 15px"> Board </td>';
                //    tRow += '<td style="border: none; padding-top: 15px">Total Rooms</td>';
                //    tRow += '<td style="border: none; padding-top: 15px"> Adults </td>';
                //    tRow += '<td style="border: none; padding-top: 15px">Child </td>';
                //    tRow += '<td style="border: none; padding-top: 15px">Child Age</td>';
                //    tRow += '<td align="left" style="border: none; padding-top: 15px">Remark</td>';
                //    tRow += '</tr>';
                //    tRow += '<tr style="color: #B0B0B0; border: none; text-align: center; color: #57585A;">';
                //    tRow += '<td align="left" style="border: none; padding:12px 0px 15px 10px; "> ' + arrBookedRoom[i].RoomType + '</td>';
                //    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].BoardText + '</td>';
                //    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].TotalRooms + '</td>';
                //    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Adults + '</td>';
                //    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Child + '</td>';
                //    tRow += '<td style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].ChildAge + '</td>';
                //    tRow += '<td align="left" style="border: none; padding-top: 12px; padding-bottom: 15px; ">' + arrBookedRoom[i].Remark + '</td>';
                //    tRow += '</tr>';

                //    $("#tbl_RoomDetails tbody").append(tRow);
                //    $("#tbl_RoomDetails tbody").html(tRow);
                //}
            }
        },
        error: function () {
            Success("An error occured while loading credit information")
        }
    });

}

function BookingDetails(HotelName, CheckIn, CheckOut, BookingName, City, Nights, GTAId, ReservationDate, TotalFare, Servicecharge) {

    debugger;
    var strCin = CheckIn;
    var ret1 = strCin.split(" ");
    var CheckInDate = ret1[0];

    var strCout = CheckOut;
    var ret2 = strCout.split(" ");
    var CheckOutDate = ret2[0];

    var Amt = parseFloat(TotalFare) + parseFloat(Servicecharge);

    if (HotelName.length > 17) {
        HotelName = HotelName.substr(0, 17) + '..';
    }
    if (BookingName.length > 18) {
        BookingName = BookingName.substr(0, 18) + '..';
    }
    if (City.length > 18) {
        City = City.substr(0, 18) + '..';
    }


    $("#dspHotelName1").text(HotelName);
    $("#dspCheckin1").text(CheckInDate);
    $("#dspCheckout1").text(CheckOutDate);
    $("#dspPasssengerName1").text(BookingName);
    $("#dspLocation1").text(City);
    $("#dspNights1").text(Nights)
    $("#dspBookingID1").text(GTAId);
    $("#dspBookingDate1").text(ReservationDate);
    $("#dspBookingAmount1").text(Amt);



    //dspNights
}



//function GetPrintVoucher(ReservationID, Uid, Latitude, Longitude, Status) {
//    debugger;
//    if (Status == 'Vouchered') {
//        window.open('../Agent/Voucher.html?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude, 'tester', 'left=50000,top=50000,width=800,height=600');
//    }
//    else if (Status == 'Booking') {
//        alert('Please Confirm Your Booking!')
//    }
//    else
//        alert('You cannot get voucher for cancelled booking!')
//}

var Night;
var Status;
function VoucherDetails(ReservationID, Uid, latitude, longitude, Nights, status) {
    debugger;
    if (status == 'Booking') {
        Success('Please Confirm Your Booking! ')
    }
    else if (status == 'Cancelled') {
        Success('You cannot get voucher for cancelled booking!')
    }

    UID = Uid;
    ReservationId = ReservationID;
    Latitude = latitude;
    Longitude = longitude;
    Night = (Nights < 10) ? '0' + Nights : '' + Nights;
    Status = status;
}


function MailVoucher() {
    debugger;

    Status = GetQueryStringParams('Status');
    ReservationId = GetQueryStringParams('ReservationId');
    var UID = GetQueryStringParams('Uid');
    var Latitude = "";
    var Longitude = "";
    var Night = "";


    if (Status == 'Vouchered') {
        //var Email;
        //Email = $("#txt_sendInvoice").val();
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        var bValid = true;
        var sEmail = $("#txt_sendVoucher").val();
        if (sEmail == "") {
            bValid = false;
            $("#lbl_txt_sendVoucher").css("display", "");
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                $("#lbl_txt_sendVoucher").html("* Wrong email format.");
                $("#lbl_txt_sendVoucher").css("display", "");
            }
        }

        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "../EmailHandler.asmx/SendVoucher",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '","UID":"' + UID + '","Latitude":"' + Latitude + '","Longitude":"' + Longitude + '","Night":"' + Night + '","Supp":"","Status":"' + Status + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        Success("Some error occured, Please try again in some time.");
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Email has been sent successfully.");
                        setTimeout(function () {
                            window.location.href = "BookingList.aspx";
                        }, 2000);
                       
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Sorry Please Try Later.");
                        setTimeout(function () {
                            window.location.href = "BookingList.aspx";
                        }, 2000);
                       
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    else if (result.retCode == 2) {
                        Success("Sorry, No account found with this email id.");
                        setTimeout(function () {
                            window.location.href = "BookingList.aspx";
                        }, 2000);
                     
                        //                    window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                },
                error: function () {
                    Success('Something Went Wrong');
                }

            });
        }
    }
    else {
        Success('Please Confirm Your Booking!!!!');
        setTimeout(function () {
            window.location.href = "Invoice.aspx";
        }, 2000);
     
    }
}

function CancelDetails(conformation, ReservationDate, paxname, city, checkin, checkout, bookingid, amount, Sid) {
    debugger;

    var strCin = checkin;
    var ret1 = strCin.split(" ");
    var CheckInDate = ret1[0];

    var strCout = checkout;
    var ret2 = strCout.split(" ");
    var CheckOutDate = ret2[0];

    var Reservation = ReservationDate;
    var ret3 = Reservation.split(" ");
    var Reservationdate = ret3[0];



    $("#spnCConformation").text(conformation);
    $("#spnCResDate").text(Reservationdate);
    $("#spnCPaxName").text(paxname);
    $("#spnCCity").text(city);
    $("#spnCCheckin").text(CheckInDate);
    $("#spnCCheckout").text(CheckOutDate);
    $("#spnCBookingId").text(bookingid);
    $("#spnCAmount").text(amount);
    var Remark = $("#txtRemark");

    hiddensid = Sid;
    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/GetPolicyandHotel",
        data: '{"ReservationID":"' + conformation + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgentList = result.ReservationDetails;
                if (arrAgentList != 0) {


                    var policy = arrAgentList[0].CancellationPolicy;
                    var hotel = arrAgentList[0].HotelName;

                    $("#spnCCancellation").text(policy);
                    $("#spnCHotel").text(hotel);
                }
                else {

                }
            }
        },
        error: function () {
            Success('Something Went Wrong');
        }
    });
}


function CancelBooking() {
    debugger;
    var Remark = $("#txtRemark").val();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelCancelBooking",
        data: '{"sid":"' + hiddensid + '","Remark":"' + Remark + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {


            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Booking Cancelled");

            }
        },
        error: function () {
            Success("something went wrong");
        }
    });
}

function GetPDFInvoice(ReservationID, Uid, Status) {
    debugger;
    var win = window.open('InvoicePDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');


}

function GetPDFVoucher(ReservationID, Uid, Status) {
    debugger;
    var win = window.open('VoucherPDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');


}


function ExportInvoiceToExcel() {
    debugger;
    Uid = $('#hdnDCode').val();
    var Agency = $('#hdnDCode').val();
    dFrom = $('#datepicker3').val();
    dTo = $('#datepicker4').val();;

    window.location.href = "ExportToExcelHandler.ashx?datatable=Invoice&sid=" + Uid + "&dFrom=" + dFrom + "&dTo=" + dTo + "&Agency=" + Agency;

}
var SearchBit = false;
function ExportAgencyStatement(Document) {
    debugger;

    var Type = $('#TransactionType').val();
    var RefNo = $('#hdnDCode').val();
    var From = $('#datepicker3').val();
    var To = $('#datepicker4').val();

    if (Type == "Both" && From == "" && To == "" && RefNo == "") {
        SearchBit = false;
    }

    if (SearchBit == false) {
        window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&uid=" + RefNo + "&dFrom=" + From + "&dTo=" + To + "&TransType=" + Type + "&Document=" + Document;
    }
    else {
        window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&uid=" + RefNo + "&dFrom=" + From + "&dTo=" + To + "&TransType=" + Type + "&Document=" + Document;
    }

    //   window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&sid=" + sid + "&dFrom=" + dFrom + "&dTo=" + dTo + "&TransType=" + TransType;

}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source, AffilateCode, DeadLineDate, HoldDate) {
    //OpenCancellationPopup(ReservationID, Status);
    $("#hdn_Supplier").val(Source);
    $("#hdn_AffiliateCode").val(AffilateCode);
    OpenHoldPopup(ReservationID, Status);

    $("#hdn_HoldDate").val(HoldDate);
    $("#hdn_DeadLineDate").val(DeadLineDate);

    $('#ConfirmAlertForOnHoldModel').modal('show');
}

function OpenUpdateHoldDateModal() {
    $("#HoldDate").text($("#hdn_HoldDate").val());
    $("#holdReservatonID").val($("#hndReservatonID").val());
    $("#holdDeadLineDate").val($("#hdn_DeadLineDate").val());
    $("#DeadLineDate").text($("#hdn_DeadLineDate").val());

    $('#UpdateHoldBookingDateModel').modal('show');
}

function UpdateHoldDate() {
    var OldDate = $("#HoldDate").text();
    var ReservationId = $("#holdReservatonID").val();
    var Date = $("#datepickerHold").val();
    if (Date == "") {
        $('#SpnMessege').text('Please Enter Date.');
        $('#ModelMessege').modal('show');
        return false;
    }
    var DeadLineDate = $("#holdDeadLineDate").val();


    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/UpdateHoldDate",
        data: '{"ReservationId":"' + ReservationId + '","OldDate":"' + OldDate + '","Date":"' + Date + '","DeadLineDate":"' + DeadLineDate + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Hotel Hold Date Updated.");
                $("#HoldDate").text(' ');
                $("#datepickerHold").val(' ');
                $("#holdReservatonID").val(' ');
                $('#UpdateHoldBookingDateModel').modal('hide');
                window.location.href = "Invoice.aspx";
            }
            else if (result.retCode == 2) {
                Success("Hold Date Must Be Lesser Than Deadline Date.");
                $("#HoldDate").text(' ');
                $("#datepickerHold").val(' ');
                $("#holdReservatonID").val(' ');
                $('#UpdateHoldBookingDateModel').modal('hide');
                window.location.href = "Invoice.aspx";
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });
}

function ConfirmHoldBookingDetail(ReservationID, Uid, Status, Source, AffilateCode, AgencyName) {

    $("#ReservatonID").val(ReservationID);
    OpenHoldPopupp(ReservationID, Status);

    $('#ConfirmHoldBookingDetailModel').modal('show');
}

function numberWithCommas(n) {
    var parts = n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function OpenHoldPopupp(ReservationID, Status) {
    debugger;
    ReserID = ReservationID;
    var arrCancellationDetails = new Array();
    var arrCancellationPolicy = new Array();
    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHoldingDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#divRemark1").css("display", "block");
                $("#divRemark2").css("display", "block");
                $("#btnCancelBooking").val("Cancel Booking");
                $("#dlgLoader").css("display", "none");
                $("#txtRemark").val("");
                $("#dspAlertMessage").css("display", "none");

                $("#hndReservatonID").val(ReservationID);
                $("#hndIsCancelable").val(result.IsCancelable);
                $("#hndStatus").val(Status);
                $("#hdn_AffiliateCode").val(result.HotelReservation[0].AffilateCode)
                $("#hdn_Supplier").val(result.HotelReservation[0].Source)
                $("#hdn_TotalFare").val(result.TotalFare);
                $("#hdn_ServiceCharge").val(result.ServiceCharge);
                $("#hdn_Total").val(result.Total);


                TimeGap = result.TimeGap;
                totalamount = result.TotalFare + result.ServiceCharge;
                checkin = result.HotelReservation[0].CheckIn;
                checkout = result.HotelReservation[0].CheckOut;
                bookingdate = result.HotelReservation[0].ReservationDate;

                HotelName = result.HotelReservation[0].HotelName;
                PassengerName = result.HotelReservation[0].bookingname;
                City = result.HotelReservation[0].City;
                if (HotelName.length > 17) {
                    HotelName = HotelName.substr(0, 17) + '..';
                }
                if (PassengerName.length > 18) {
                    PassengerName = PassengerName.substr(0, 18) + '..';
                }
                if (City.length > 18) {
                    City = City.substr(0, 18) + '..';
                }
                $("#HotelName").text(HotelName);
                $("#Checkin").text(checkin);
                $("#Checkout").text(checkout);
                $("#PasssengerName").text(PassengerName);
                $("#Location").text(City);
                // $("#Nights").text(arrCancellationDetails[0].NoOfDays);
                $("#BookingID").text(ReservationID);
                $("#BookingDate").text(bookingdate);
                $("#BookingAmount").text(numberWithCommas(parseFloat(result.TotalFare)));
                //$("#dspBookingAmount").text(numberWithCommas(parseFloat(arrCancellationDetails[0].TotalFare) + parseFloat(arrCancellationDetails[0].ServiceCharge)));



            }
            else if (result.retCode == 0) {
                $('#ConfirmHoldBookingDetailModel').modal('hide');
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#ConfirmHoldBookingDetailModel').modal('hide')
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}

function SaveConfirmDetail() {
    var HotelName = $("#HotelName").text();
    var ReservationId = $("#ReservatonID").val();

    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").text();
    if (ReconfirmThrough == "Select Reconfirm Through") {
        Success('Please Select Reconfirm Through.');
        return false;
    }



    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationId,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,

    }

    $.ajax({
        type: "POST",
        url: "HotelBookingHandler.asmx/SaveConfirmDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Confirm Detail Save.");
                $("#ReservatonID").val('');
                $("#ConfirmDate").val('');
                $("#HotelConfirmationNo").val('');
                $("#StaffName").val('');
                $("#ReconfirmThrough option:selected").val('-');
                $("#Comment").val('');
                $('#ConfirmHoldBookingDetailModel').modal('hide');
                window.location.href = "Invoice.aspx";
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });

}

function InvoiceMailModal() {

    $.modal({
        content:
      //'<div class="modal-header">'+
      //'<button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>'+
      //'<h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>'+
      //'</div>'+
      '<div class="modal-body">' +
      '' +
      '<table class="table table-hover table-responsive" id="tbl_SendInvoice" style="width: 100%">' +
      '<tr>' +
      '<td style="border: none;">' +
      '<span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;' +
      '' +
      '<input type="text" id="txt_sendInvoice" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 100%;" />' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td style="border: none;">' +
      '<input id="btn_sendInvoice" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailInvoice();" />' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '' +
      '</div>',
        title: 'Email',
        width: 400,
        height: 200,
        scrolling: true,
        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        // buttonsLowPadding: true
    });

}

function InvoiceCommissionMailModal() {

    $.modal({
        content:
      //'<div class="modal-header">'+
      //'<button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>'+
      //'<h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>'+
      //'</div>'+
      '<div class="modal-body">' +
      '' +
      '<table class="table table-hover table-responsive" id="tbl_SendInvoice" style="width: 100%">' +
      '<tr>' +
      '<td style="border: none;">' +
      '<span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;' +
      '' +
      '<input type="text" id="txt_sendInvoice" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 100%;" />' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td style="border: none;">' +
      '<input id="btn_sendInvoice" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailCommissionInvoice();" />' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '' +
      '</div>',
        title: 'Email',
        width: 400,
        height: 200,
        scrolling: true,
        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        // buttonsLowPadding: true
    });

}

function VoucherMailModal() {

    $.modal({
        content:
      //'<div class="modal-header">'+
      //'<button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>'+
      //'<h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>'+
      //'</div>'+
      '<div class="modal-body">' +
      '' +
      '<table class="table table-hover table-responsive" id="tbl_SendVoucher" style="width: 100%">' +
      '<tr>' +
      '<td style="border: none;">' +
      '<span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;' +
      '' +
      '<input type="text" id="txt_sendVoucher" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 100%;" />' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td style="border: none;">' +
      '<input id="btn_sendVoucher" type="button" value="Send Voucher" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailVoucher();" />' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '' +
      '</div>',
        title: 'Email',
        width: 400,
        height: 200,
        scrolling: true,
        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        // buttonsLowPadding: true
    });

}

function OpenCancellationModel(ReservationID, Status) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;
                for (var j = 0; j < Detail.length; j++) {
                    Cancle += Detail[j].CutCancellationDate.split('|');
                    Cancleamnt += Detail[j].CancellationAmount.split('|');
                }
                if (Cancle.split(',').length >= 2) {
                    var Cancle = Cancle.split(',');
                    var Cancleamnt = Cancleamnt.split(',');
                }
                for (var i = 0; i < Cancle.length - 1; i++) {
                    Policy += "<p><b>Room " + (i + 1) + "</b></p><p style='margin-left:5px'>In the event of cancellation after " + Cancle[i] + ",  Rs. " + Cancleamnt[i] + " will be applicable.<p>"
                }

                $.modal({
                    content:
                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td style="border: none;">' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td style="border: none;">' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].Name + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td style="border: none;">' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td style="border: none;">' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Cancellation Policy</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td style="border: none;">' +
                  '<span class="text-left">' + Policy + '</span>&nbsp;&nbsp;' +
                  '' +

                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Confirm</h4>' +
                  '</tr>' +
                  '<td style="border: none;">' +
                  '<span class="text-left"><center>Do you want to Cancel Booking?</center></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td style="border: none;">' +
                  '<input id="btn_CancleBooking" type="button" value="Yes" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="BookingCancle(\'' + ReservationID + '\',\'' + Status + '\');" />' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +
                  '' +
                  '</div>',
                    title: 'Cancel Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'No': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    // buttonsLowPadding: true
                });
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });




}

function BookingCancle(ReservationID, Status) {
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/BookingCancle",
        data: '{"ReservationID":"' + ReservationID + '","Status":"' + Status + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $('#btn_Cancel').hide();
                Success("Booking Cancelled");
                setTimeout(function () {
                    window.location.href = "BookingList.aspx"
                }, 2000);
           
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });
}