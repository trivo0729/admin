﻿var reg = new RegExp('[0-9]$');
var floatRegex = new RegExp('[-+]?([0-9]*.[0-9]+|[0-9]+)');
var availablelimit;
var maxlimit;
var CreditAmount;
var availablecash = 0.00;
var LimitUpdate;

var Add;
var reduce;
var chequenumber;
var bankname;
var urlParamDecoded;
var hiddensid;
var sid;
var activeflag;
var type;
var deposit;
var credit;

$(document).ready(function () {
    urlParamDecoded = GetQueryStringParamsForUpdateCredit('UniqueCode');
    hiddensid = GetQueryStringParamsForUpdateCredit('sid');
    var agency;
    var Name = GetQueryStringParamsForUpdateCredit('Name');
    agency = Name.split('%20');
    debugger;
    for (var i = 0; i < agency.length; i++) {
        if (Name.indexOf("&") != -1) {
            Name = Name.replace("&", "");
        }

        if (Name.indexOf("%") != -1) {
            Name = Name.replace("%20", " ");
        }
    }
    GetBank(); //For supplier
    GetBankDetails();
    GetLastUpdateCreditDetails();
    // GetAccountDetails(hiddensid);
    $("#txt_CreditLimit").prop("disabled", true);
    $("#txt_MaximumLimit").prop("disabled", true);
    $("#spUniqueCode").text(urlParamDecoded);
    $("#spName").text(Name);
    // $("#txt_UniqueCode").val(urlParamDecoded);
    $('#ddl_Type').change(function () {
        //$("#lblerr_txt_AvailableLimit").css("display", "none");
        $("#lblerr_txt_MaximumLimit").css("display", "none");
        $("#lblerr_txt_AddCredit").css("display", "none");
        //$("#lblerr_txt_ReduceCredit").css("display", "none");
        $("#lblerr_txt_Comments").css("display", "none");

        if ($('#ddl_Type').val() == 'Refund') {
            $("#spaddcredit").text("Reduce Amount:");
            $("#txt_AddCredit").val('');
            $("#txt_AddCredit").prop("disabled", false);
            $("#txt_CreditLimit").prop("disabled", true);
            $("#txt_MaximumLimit").prop("disabled", true);
            $("#lblerr_txt_AddCredit").css("display", "none");
        }
        if ($('#ddl_Type').val() == 'Incentive') {
            $("#spaddcredit").text("Add Incentive:");
            $("#txt_AddCredit").val('');
            $("#txt_AddCredit").prop("disabled", false);
            $("#txt_CreditLimit").prop("disabled", true);
            $("#txt_MaximumLimit").prop("disabled", true);
            $("#lblerr_txt_AddCredit").css("display", "none");
        }
        if ($('#ddl_Type').val() == 'Cash') {
            $("#spaddcredit").text("Add Cash Amount:");
            $("#txt_AddCredit").val('');
            $("#txt_AddCredit").prop("disabled", false);
            $("#txt_CreditLimit").prop("disabled", true);
            $("#txt_MaximumLimit").prop("disabled", true);
            $("#lblerr_txt_AddCredit").css("display", "none");
        }
        if ($('#ddl_Type').val() == 'Deposit') {
            $("#spaddcredit").text("Add Deposit:");
            //$("#txt_ReduceCredit").val('');
            $("#txt_AddCredit").prop("disabled", false);
            $("#txt_CreditLimit").prop("disabled", true);
            $("#txt_MaximumLimit").prop("disabled", true);
            $("#lblerr_txt_AddCredit").css("display", "none");
        }
        if ($('#ddl_Type').val() == 'Credit') {
            $("#txt_AddCredit").prop("disabled", true);
            $("#txt_CreditLimit").prop("disabled", false);
            $("#txt_MaximumLimit").prop("disabled", true);
        }
        if ($('#ddl_Type').val() == 'OTC') {
            $("#txt_AddCredit").prop("disabled", true);
            $("#txt_CreditLimit").prop("disabled", true);
            $("#txt_MaximumLimit").prop("disabled", false);
        }
    });
});

var arrGetCreditDetails = new Array();
function GetLastUpdateCreditDetails() {
    debugger;
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/GenralHandler.asmx/GetLastUpdateCreditDetails",
        data: '{"uid":"' + hiddensid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGetCreditDetails = result.tbl_AdminCreditLimit;
                if (arrGetCreditDetails.length > 0) {
                    for (i = 0; i < arrGetCreditDetails.length; i++) {
                        $('#ddl_Type').val(arrGetCreditDetails[i].AdminCreditType);
                        sid = arrGetCreditDetails[i].sid;
                        // $("#txt_AvailableLimit").val(arrGetCreditDetails[i].AvailableCredit);
                        $("#txt_MaximumLimit").val("");
                        $("#txt_Comments").val("");
                        $("#txt_CreditLimit").val("");
                        $("#txt_AddCredit").val("");
                        //$("#txt_ReduceCredit").val("");
                        $("#spAvailableLimit").text(arrGetCreditDetails[i].AvailableCredit);
                        $("#spCreditLimit").text(arrGetCreditDetails[i].CreditAmount);

                        $("#spCreditLimit_Cmp").text(arrGetCreditDetails[i].CreditAmount_Cmp);

                        $("#spOneTimeCreditLimit").text(arrGetCreditDetails[i].MaxCreditLimit);
                        $("#spOneTimeCreditLimit_Cmp").text(arrGetCreditDetails[i].MaxCreditLimit_Cmp);
                        AgentDetails(hiddensid)
                    }
                }
            }
        },
        error: function () {
        }
    });
}

function ValidateCredit() {
    //if ($("#txt_CreditLimit")==prop("disabled", false)) {
    //    $("#lblerr_txt_CreditLimit").html("<b>*This field is required</b>");
    //}
    //if ($("#txt_MaximumLimit")==prop("disabled", false)) {
    //    $("#lblerr_txt_MaximumLimit").html("<b>*This field is required</b>");
    //}
    //if ($("#txt_AddCredit")==prop("disabled", false)) {
    //    $("#lblerr_txt_AddCredit").html("<b>*This field is required</b>");
    //}
    //$("#lblerr_txt_Comments").html("<b>*This field is required</b>");
    var isValid = true;

    //availablelimit = $("#txt_AvailableLimit").val();
    maxlimit = $("#txt_MaximumLimit").val();
    add = $('#txt_AddCredit').val();
    if ($("#spaddcredit").text == "Reduce Amount:") {
        reduce = $('#txt_AddCredit').val();
    }
    CreditAmount = $('#txt_CreditLimit').val();
    var comments = $('#txt_Comments').val();

    //if (availablelimit == "") {
    //    isValid = false;
    //    $("#lblerr_txt_AvailableLimit").css("display", "");
    //}
    //if (availablelimit != "" && !(reg.test(availablelimit))) {
    //    isValid = false;
    //    $("#lblerr_txt_AvailableLimit").html("<b>*Only numbers allowed</b>");
    //    $("#lblerr_txt_AvailableLimit").css("display", "");
    //}
    //if (maxlimit == "") {
    //    isValid = false;
    //    $("#lblerr_txt_MaximumLimit").css("display", "");
    //}
    //if (maxlimit != "" && !(reg.test(maxlimit))) {
    //    isValid = false;
    //    $("#lblerr_txt_MaximumLimit").html("<b>*Only numbers allowed</b>");
    //    $("#lblerr_txt_MaximumLimit").css("display", "");
    //}
    //if (comments == "") {
    //    isValid = false;
    //    $("#lblerr_txt_Comments").css("display", "");
    //}
    if ($('#txt_AddCredit').prop("disabled") == false && add == "") {
        isValid = false;
        $("#lblerr_txt_AddCredit").css("display", "");
    }
    if ($('#txt_AddCredit').prop("disabled") == false && (add != "" && !(floatRegex.test(add)))) {
        isValid = false;
        $("#lblerr_txt_AddCredit").html("<b>*Only numbers allowed</b>");
        $("#lblerr_txt_AddCredit").css("display", "");
    }
    //if ($('#txt_ReduceCredit').prop("disabled") == false && reduce == "") {
    //    isValid = false;
    //    $("#lblerr_txt_ReduceCredit").css("display", "");
    //}
    //if ($('#txt_ReduceCredit').prop("disabled") == false && (reduce != "" && !(floatRegex.test(reduce)))) {
    //    isValid = false;
    //    $("#lblerr_txt_ReduceCredit").html("<b>*Only numbers allowed</b>");
    //    $("#lblerr_txt_ReduceCredit").css("display", "");
    //}
    //if (parseFloat(reduce) > parseFloat(availablelimit)) {
    //    if ($('#txt_ReduceCredit').prop("disabled") == false && (reduce != "" && (floatRegex.test(reduce)))) {
    //        isValid = false;
    //        $("#lblerr_txt_ReduceCredit").html("<b>Cannot reduce Available limit less than nill.</b>");
    //        $("#lblerr_txt_ReduceCredit").css("display", "");
    //    }
    //}
    return isValid;
}

var arrBankName = new Array();
function GetBankDetails() {
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/GenralHandler.asmx/GetBankNames",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrBankName = result.Arr;
                if (arrBankName.length > 0) {
                    $("#Select_Bank").empty();
                    var ddlRequest = '';
                    ddlRequest += '<option selected="selected" value="-">Select Bank</option>';
                    for (i = 0; i < arrBankName.length; i++) {
                        ddlRequest += '<option value="' + arrBankName[i].sid + '">' + arrBankName[i].BankName + '</option>';
                    }
                    $("#Select_Bank").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetBank() {
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetBankDetails",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var BankDetails = result.BankDetails;
                if (BankDetails.length > 0) {
                    $("#Select_BankAccount").empty();
                    var ddlRequest = '';
                    ddlRequest += '<option selected="selected" value="-">Select Bank</option>';
                    for (i = 0; i < BankDetails.length; i++) {
                        ddlRequest += '<option value="' + BankDetails[i].sid + '">' + BankDetails[i].BankName + '</option>';
                    }
                    $("#Select_BankAccount").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetAccountDetails(id) {
    $("#tbl_UserCreditLogList").dataTable().fnClearTable();
    $("#tbl_UserCreditLogList").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/GenralHandler.asmx/GetAccountDetails",
        data: '{"id":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_AccountDetails = result.AccountDetails;
                var tRow = "";
                for (var i = 0; i < List_AccountDetails.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + List_AccountDetails[i].LimitUpdate + '</td>';
                    tRow += '<td>' + List_AccountDetails[i].DepositAmount + '</td>';
                    tRow += '<td>' + List_AccountDetails[i].CreditAmount + '</td>';
                    tRow += '<td>' + List_AccountDetails[i].LastUpdateDate + '</a></td>';
                    tRow += '<td>' + List_AccountDetails[i].Comments + '</td>';
                    tRow += '<td>' + isActive(List_AccountDetails[i].ApproveFlag) + '</a></td>';
                    tRow += '<td>' + List_AccountDetails[i].CreditType + '</td>';
                    tRow += '<td>' + List_AccountDetails[i].ExecutiveName + '</td>';
                    tRow += '</tr>';
                }
                $("#tbl_UserCreditLogList tbody").html(tRow);
                $("#tbl_UserCreditLogList").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }
    });
}

function backtoAgentDetails() {
    window.location.href = 'SupplierDetails.aspx';
}

function isActive(status) {
    debugger;
    if (status == '1') {
        return 'Active';
    }
    else {
        return 'InActive';
    }
}

function GetQueryStringParamsForUpdateCredit(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function AgentDetails(Agent) {
    $("#tbl_AgencyStatement").dataTable().fnClearTable();
    $("#tbl_AgencyStatement").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/handler/AgencyStatementHandler.asmx/GetAgencyStatementByAgent",
        data: '{"uid":"' + Agent + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //$("#tbl_AgencyStatement tbody tr").remove();
                var arrAgencyStatement = result.AgentStatement;
                //var sAgencyName = result.AgencyName;
                var tRow;
                if (arrAgencyStatement != 0) {
                    $('#lblStatus').css("display", "none");
                    //var tRow = '<tr align=center><td><b>S.N</b></td><td><b>Date</b></td><td><b>Particulars</b></td><td><b>Credit</b></td><td><b>Debit</b></td><td><b>Balance</b></td></tr>';
                    for (var i = 0; i < arrAgencyStatement.length; i++) {
                        var Debited;
                        var Credit;
                        if ((arrAgencyStatement[i].DebitedAmount) == "" || (arrAgencyStatement[i].DebitedAmount) == null) {
                            Debited = "";
                        }
                        else {
                            Debited = arrAgencyStatement[i].DebitedAmount;
                        }

                        if ((arrAgencyStatement[i].CreditedAmount) == "") {
                            Credit = "";
                        }
                        else {
                            Credit = arrAgencyStatement[i].CreditedAmount;
                        }
                        tRow += '<tr>';
                        tRow += '<td align=center style="width:5%;">' + (i + 1) + '</td>';
                        tRow += '<td align=center>' + arrAgencyStatement[i].TransactionDate + '</td>';
                        tRow += '<td style="width:40%;">' + arrAgencyStatement[i].Particulars + '</td>';
                        //tRow += '<td style="width:40%;">' + arrAgencyStatement[i].Status + '</td>';
                        tRow += '<td align=center>' + numberWithCommas(Credit) + '</td>';
                        tRow += '<td align=center>' + numberWithCommas(Debited) + '</td>';
                        tRow += '<td align=center>' + numberWithCommas(arrAgencyStatement[i].Balance) + '</td>';
                        tRow += '</tr>';
                    }
                    $("#tbl_AgencyStatement tbody").html(tRow);
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
                else {
                    $("#tbl_AgencyStatement").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
            }
            else {
                $("#tbl_AgencyStatement").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }
    });
}
function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}