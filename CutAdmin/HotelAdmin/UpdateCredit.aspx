﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="UpdateCredit.aspx.cs" Inherits="CutAdmin.HotelAdmin.UpdateCredit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script>
        $(function () {
            //debugger
            $(document).on({
                ajaxStart: function () {
                    $("#loders").css("display", "")
                }, ajaxStop: function () { $("#loders").css("display", "none") }
            });
        })
    </script>
    <script src="Scripts/UpdateCredit.js"></script>
    <script src="Scripts/AgentStatment.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#dteBankDeposite").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                //onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                //minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dteCashDeposit").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                //onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                //minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Update Credit</h1>
        </hgroup>
        <div class="with-padding">
            <span class="lato size14 grey" style="float: right; padding-bottom: 10px;">Name:<span class="orange" id="spName"></span>,
            Agent Code:<span class="orange" id="spUniqueCode"></span>, 
            Available limit:<span class="orange" id="spAvailableLimit"></span>,
             Credit Limit:[<span class="orange" id="spCreditLimit"></span>/<span class="orange" id="spCreditLimit_Cmp">0</span>]
            One Time Limit:[<span class="orange" id="spOneTimeCreditLimit"></span>/<span class="orange" id="spOneTimeCreditLimit_Cmp"></span>]
            </span>
            <div class="clear"></div>
            <div class="standard-tabs margin-bottom" id="add-tabs">
                <ul class="tabs">
                    <li class="active"><a href="#Bank">Bank</a></li>
                    <li><a href="#Cash">Cash</a></li>
                    <li><a href="#Credit">Credit / Debit</a></li>
                    <li><a href="#Limit">Credit Limit</a></li>
                </ul>
                <div class="tabs-content">
                    <div id="Bank" class="with-padding">
                        <div class="columns">
                            <div class="six-columns  twelve-columns-mobile six-columns-tablet">
                                <label>Type of Transaction</label>
                                <div class="full-width button-height">
                                    <select id="Select_TypeOfCash" name="validation-select" class="select">
                                        <option selected="selected" value="-">Select Transaction</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="NEFT">NEFT</option>
                                        <option value="IMPS">IMPS</option>
                                        <option value="RTGS">RTGS</option>
                                        <option value="NetBanking">Net Banking</option>
                                        <option value="Draft">Draft</option>
                                        <option value="TT">TT</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_TypeOfCash">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="six-columns  twelve-columns-mobile six-columns-tablet">
                                <label>Bank Account</label>
                                <div class="full-width button-height">
                                    <select id="Select_BankAccount" name="validation-select" class="select">
                                        <option selected="selected" value="-">Select Bank</option>
                                        <option value="ICICI Bank -  023105002994">ICICI Bank -  023105002994</option>
                                        <option value="Axis Bank - 914020021370944">Axis Bank - 914020021370944</option>
                                        <option value="Bank of India - 870120110000456">Bank of India - 870120110000456</option>
                                        <option value="ICICI Bank - 023105500640">ICICI Bank - 023105500640</option>
                                        <option value="EMIRATES ISLAMIC BANK - 370-73941042-01">EMIRATES ISLAMIC BANK - 370-73941042-01</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_BankAccount">
                                    <b>* This field is required</b></label>
                            </div>

                            <div class="four-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_AmountGiven" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_AmountGiven">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <label>Deposit Date</label><div class="input full-width">
                                    <%--<input name="prompt-value" id="dteBankDeposite" value="" class="input-unstyled full-width" placeholder="dd/mm/yy" type="text">--%>
                                    <%--<input type="text" name="prompt-value" placeholder="dd/mm/yy" id="dteBankDeposite" class="input-unstyled datepicker" value="">--%>
                                    <input type="text" placeholder="dd/mm/yy" id="dteBankDeposite" class="input full-width mySelectCalendar" readonly="readonly" value="" style="margin-left: -8px; width: 106%">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_dteBankDeposite">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile">
                                <label>Receipt Number/Transaction ID</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_BankTransactionNumber" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_BankTransactionNumber">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_BankRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="saveBack">
                            <button id="btnBankDeposite" type="button" onclick="Submit()" class="button anthracite-gradient">Submit</button>
                            <button type="button" onclick="window.location = 'SupplierDetails.aspx'" class="button glossy">
                                Back
                            </button>
                        </div>
                    </div>

                    <div id="Cash" class="with-padding">
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_Ammount" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_Ammount">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Deposit Date</label><div class="input full-width">
                                    <%--<input name="prompt-value" id="dteCashDeposit" value="" class="input-unstyled datepicker" placeholder="dd/mm/yy" type="text">--%>
                                    <input name="prompt-value" id="dteCashDeposit" value="" class="input full-width mySelectCalendar" placeholder="dd/mm/yy" type="text" style="margin-left: -8px; width: 108%">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_dteCashDeposit">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Given To</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_Given_To" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_Given_To">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Receipt Number</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_ReceiptNumberCheque" value="" class="input-unstyled full-width" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_ReceiptNumberCheque">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_CashRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="saveBack">
                            <button id="btnSubmitCash" type="button" onclick="SaveCash()" class="button anthracite-gradient">Submit</button>
                        </div>
                    </div>

                    <div id="Credit" class="with-padding">
                        <div class="columns">
                            <div class="three-columns   twelve-columns-mobile">
                                <label>Type</label>
                                <div class="full-width button-height">
                                    <select id="Select_CreditType" name="validation-select" class="select" onchange="ChangeTransaction()">
                                        <option selected="selected" value="Credit">Credit</option>
                                        <option value="Debit">Debit</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_CreditTypet">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns   twelve-columns-mobile ">
                                <label>Type of Transaction</label>
                                <div class="full-width button-height" id="dll_Credit">
                                    <select id="Select_CreditTransaction" name="validation-select" class="select">
                                        <option selected="selected" value="-">Select Transaction</option>
                                        <option value="Refund">Refund</option>
                                        <option value="Incentive">Incentive</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_CreditTransaction">
                                    <b>* This field is required</b></label>
                                <div class="full-width button-height" id="dll_Debit" style="display: none">
                                    <select id="Select_DebitTransaction" name="validation-select" class="select" onchange="ValidateInvoice()">
                                        <option selected="selected" value="-">Select Transaction</option>
                                        <option value="Against invoice">Against Invoice</option>
                                        <option value="Against invoice">Against Visa</option>
                                        <option value="Against invoice">Against OTB</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_Select_DebitTransaction">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_CreditAmmount" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Invoice No </label><div class="input full-width">
                                    <input name="prompt-value" id="txt_InvoiceNo" value="" class="input-unstyled full-width" placeholder="Invoice No" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_InvoiceNo">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_CreditRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_CreditRemarks">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="saveBack">
                            <button id="btnSubmitCredit" type="button" onclick="DebitCredit()" class="button anthracite-gradient">Submit</button>
                        </div>
                    </div>

                    <div id="Limit" class="with-padding">
                        <div class="columns">
                            <div class="six-columns   twelve-columns-mobile">
                                <label>Type of Transaction</label>
                                <div class="full-width button-height">
                                    <select id="Select_Limit" name="validation-select" class="select" onchange="SelectLimit()">
                                        <option selected="selected" value="-">Select Transaction</option>
                                        <option value="Fix Limit">Fix Limit</option>
                                        <option value="One Time Limit">One Time Limit</option>
                                    </select>
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Select_Limit">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="six-columns twelve-columns-mobile">
                                <label>Amount</label><div class="input full-width">
                                    <input name="prompt-value" id="txt_LimitAmountGiven" value="" class="input-unstyled full-width" placeholder="0.00" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_LimitAmountGiven">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns" id="LimitGiven" style="display: none">
                            <div class="twelve-columns bold">
                                <label>Limit Validity :(No of days from first use) </label>
                                <div class="input full-width">
                                    <input name="prompt-value" id="txt_LimitGiven" value="" class="input-unstyled full-width" placeholder="No of days from first use" type="text">
                                </div>
                                <label style="color: red; margin-top: 3px; display: none" id="lblerr_txt_LimitGiven">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="twelve-columns bold">
                                <label>Remarks</label><textarea id="txt_LimtRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="saveBack">
                            <button id="btnUpdateHotel" type="button" onclick="UpdateCreditLmit()" class="button anthracite-gradient">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="respTable">
                <table class="table responsive-table" id="tbl_AgencyStatement">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center hide-on-mobile-portrait">S.N</th>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Particulars</th>
                            <th scope="col" class="align-center">Credit</th>
                            <th scope="col" class="align-center">Debit</th>
                            <th scope="col" class="align-center">Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
