﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewInvoice.aspx.cs" Inherits="CutAdmin.HotelAdmin.ViewInvoice" %>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>View Invoice</title>
    
    <!-- Picker -->
    <%--<link href="css/css/font-awesome.min.css" rel="stylesheet" />--%>

    <script type="text/javascript" src="scripts/Invoice.js"></script>
    <%--<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>--%>



    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="../css/2x.css?v=1"/>

    <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="../js/libs/glDatePicker/developr.fixed.css?v=1">

    <!-- jQuery Form Validation -->
    <%--	<link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">--%>

    <!-- Additional styles -->

    <link rel="stylesheet" href="../css/styles/modal.css?v=1">


    <!-- JavaScript at bottom except for Modernizr -->
    <script src="../js/libs/modernizr.custom.js"></script>
    <link href="../css/font-awesome.min.css" rel="stylesheet" />

    <!-- iOS web-app metas -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- iPhone ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png" sizes="57x57">
    <!-- iPad ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad.png" sizes="72x72">
    <!-- iPhone (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-retina.png" sizes="114x114">
    <!-- iPad (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad-retina.png" sizes="144x144">

    <!-- iPhone SPLASHSCREEN (320x460) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="(device-width: 320px)">
    <!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone-retina.png" media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPhone 5 SPLASHSCREEN (640×1096) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone5.png" media="(device-height: 568px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="(device-width: 768px) and (orientation: portrait)">
    <!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="(device-width: 768px) and (orientation: landscape)">
    <!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait-retina.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)">
    <!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape-retina.png" media="(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)">

    <link rel="stylesheet" href="../css/styles/table.css?v=1">
    <link href="../css/custom.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="../js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="../css/ScrollingTable.css" rel="stylesheet" />
    <script src="../Scripts/moments.js"></script>
    <script src="Scripts/Invoice.js"></script>
    <script type="text/javascript">
        var ReservationID;
        var Status;
        $(document).ready(function () {
            Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
            if (Status == "Cancelled") {
                document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
            }
            ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
            var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid');

            //$("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));
            //InvoicePrint(ReservationID, Uid);
            document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationModel('" + ReservationID + "', '" + Status + "')");
            document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFInvoice('" + ReservationID + "', '" + Uid + "','" + Status + "')");

        });


        function ProceedToCancellation() {
            var Supplier = GetQueryStringParamsForAgentRegistrationUpdate('Supplier')
            debugger;
            if ($("#btnCancelBooking").val() == "Cancel Booking") {
                if ($("#hndIsCancelable").val() == "0") {

                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("Sorry! You cannot cancel this booking.")
                    $('#ModelMessege').modal('show')


                    // alert("Sorry! You cannot cancel this booking.")
                }
                else {
                    var data = {
                        ReservationID: $("#hndReservatonID").val(),
                        ReferenceCode: $("#hndReferenceCode").val(),
                        CancellationAmount: $("#hndCancellationAmount").val(),
                        BookingStatus: $("#hndStatus").val(),
                        Remark: $("#txtRemark").val(),
                        TotalFare: $("#hdn_TotalFare").val(),
                        ServiceCharge: $("#hdn_ServiceCharge").val(),
                        Total: $("#hdn_Total").val(),
                        Type: Supplier
                    }
                    $("#dlgLoader").css("display", "initial");
                    $.ajax({
                        type: "POST",
                        url: "../HotelHandler.asmx/HotelCancelBooking",
                        data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                alert("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
                                //document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
                                $("#AgencyBookingCancelModal").modal('hide');
                                setTimeout(function () { window.location.href = "ViewInvoice.aspx?datatable=ReservationId=" + ReservationID + "&Uid=" + Uid + "&Status=Cancelled&Supplier=" + Supplier; }, 2000);
                                //location.reload();
                                //$.ajax({
                                //    type: "POST",
                                //    url: "../EmailHandler.asmx/CancellationEmail",
                                //    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                                //    contentType: "application/json; charset=utf-8",
                                //    datatype: "json",
                                //    success: function (response) {
                                //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                //        if (result.Session == 0) {
                                //            alert("Some error occured, Please try again in some time.");

                                //        }
                                //        if (result.retCode == 1) {
                                //            //alert("Email has been sent successfully.");


                                //        }
                                //        else if (result.retCode == 0) {
                                //            alert("Sorry Please Try Later.");


                                //        }

                                //    },
                                //    error: function () {
                                //        alert('Something Went Wrong');
                                //    }

                                //});

                                ///// email ends here...


                            }
                            else if (result.retCode == 0) {
                                $('#AgencyBookingCancelModal').modal('hide')
                                $('#SpnMessege').text(result.Message);
                                $('#ModelMessege').modal('show')

                                //alert(result.Message);


                            }
                        },
                        error: function () {
                            $('#AgencyBookingCancelModal').modal('hide')
                            $('#SpnMessege').text('Something Went Wrong');
                            $('#ModelMessege').modal('show')
                            // alert("something went wrong");
                        },
                        complete: function () {
                            $("#dlgLoader").css("display", "none");
                        }
                    });
                }
            } else if ($("#btnCancelBooking").val() == "Confirm Booking") {
                debugger;
                if ($("#hndIsConfirmable").val() == "0") {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("Sorry! You cannot confirm this booking.")
                    $('#ModelMessege').modal('show')
                    // alert("Sorry! You cannot confirm this booking.")
                }
                else {
                    var data = {
                        ReservationID: $("#hndReservatonID").val()
                    }
                    $("#dlgLoader").css("display", "initial");
                    $.ajax({
                        type: "POST",
                        url: "../HotelHandler.asmx/ConfirmHoldBooking",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                $('#SpnMessege').text("Your booking has been confirmed.Confirmation email has been sent.!");
                                $('#ModelMessege').modal('show')
                                //  alert("Your booking has been confirmed.Confirmation email has been sent.!");
                                $("#AgencyBookingCancelModal").modal('hide');
                                location.reload();

                                //here mail goes for hold relaese
                                debugger;
                                $.ajax({
                                    type: "POST",
                                    url: "../EmailHandler.asmx/HoldReleaseEmail",
                                    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (response) {
                                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                        if (result.Session == 0) {
                                            $('#SpnMessege').text("Some error occured, Please try again in some time.");
                                            $('#ModelMessege').modal('show')
                                            // alert("Some error occured, Please try again in some time.");

                                        }
                                        if (result.retCode == 1) {
                                            //alert("Email has been sent successfully.");


                                        }
                                        else if (result.retCode == 0) {
                                            $('#AgencyBookingCancelModal').modal('hide')
                                            $('#SpnMessege').text("Sorry Please Try Later.");
                                            $('#ModelMessege').modal('show')
                                            // alert("Sorry Please Try Later.");


                                        }

                                    },
                                    error: function () {
                                        $('#AgencyBookingCancelModal').modal('hide')
                                        $('#SpnMessege').text('Something Went Wrong');
                                        $('#ModelMessege').modal('show')
                                        //alert('Something Went Wrong');
                                    }

                                });
                                // Mail Ends Here....
                            }
                            else if (result.retCode == 0) {
                                $('#AgencyBookingCancelModal').modal('hide')
                                $('#SpnMessege').text(result.Message);
                                //   $('#ModelMessege').modal('show')
                                alert(result.Message);
                            }

                            else if (result.retCode == 2) {
                                //alert(result.Message);
                                $('#AgencyBookingCancelModal').modal('hide')
                                $("#dspAlertMessage").html("Your Balance is Insufficient to confirm this booking.");
                                $("#dspAlertMessage").css("display", "block");
                                //$("#AgencyBookingCancelModal").modal('hide');
                            }
                        },
                        error: function () {
                            $('#AgencyBookingCancelModal').modal('hide')
                            $('#SpnMessege').text("something went wrong during hold confirmation");
                            $('#ModelMessege').modal('show')
                            // alert("something went wrong during hold confirmation");
                        },
                        complete: function () {
                            $("#dlgLoader").css("display", "none");
                        }
                    });
                }
            }
        }

        function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    </script>
</head>

<body>

    <div style="margin-left: 35%;" id="BtnDiv">
        <input type="button" class="btn btn-search" value="Email Invoice" style="cursor: pointer" title="Invoice" data-toggle="modal" data-target="#InvoiceModal" onclick="InvoiceMailModal()" />
        <input type="button" class="btn btn-search" value="Download" id="btn_VoucherPDF" />
        <input type="button" class="btn btn-search" value="Cancel Booking" id="btn_Cancel" />
    </div>
    <br />
    <div id="maincontainer" runat="server" class="print-portrait-a4" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray;">
    </div>

    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="../js/setup.js"></script>
    <script src="../Scripts/moments.js"></script>
    <!-- Template functions -->
    <script src="../js/developr.input.js"></script>
    <script src="../js/developr.message.js"></script>
    <script src="../js/developr.modal.js"></script>
    <script src="../js/developr.navigable.js"></script>
    <script src="../js/developr.notify.js"></script>
    <script src="../js/developr.scroll.js"></script>
    <script src="../js/developr.progress-slider.js"></script>
    <script src="../js/developr.tooltip.js"></script>
    <script src="../js/developr.confirm.js"></script>
    <script src="../js/developr.agenda.js"></script>
    <script src="../js/developr.tabs.js"></script>
    <!-- Must be loaded last -->
    <!-- Tinycon -->
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../js/libs/tinycon.min.js"></script>
    <script src="../js/libs/DataTables/jquery.dataTables.min.js"></script>
    <link href="../js/libs/DataTables/jquery.dataTables.css" rel="stylesheet" />
    <script src="../js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
    <script src="http://www.google.com/jsapi"></script>
</body>
</html>
