﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
using CutAdmin.Models;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Destination

        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {
            jsSerializer = new JavaScriptSerializer();
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier(); /*Check  Authorized Supplier*/
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    foreach (var objSupplier in arrSupplier)
                    {
                        var arrHotels = (from obj in DB.tbl_CommonHotelMasters
                                         where obj.ParentID == objSupplier && obj.CityId.Contains(name)
                                         select new Models.AutoComplete
                                         {
                                             id = obj.CityId,
                                             value = obj.CityId + "," + obj.CountryId,
                                         }).Distinct().ToList();
                        foreach (var objLocation in arrHotels)
                        {
                            if (list_autocomplete.Where(d => d.id == objLocation.id).FirstOrDefault() == null)
                            {
                                list_autocomplete.Add(objLocation);
                            }
                        }
                    }
                }

                return jsSerializer.Serialize(list_autocomplete.Distinct());
            }
            catch (Exception)
            {

                return jsSerializer.Serialize(new { id = "", value = "No Data found" });
            }
            ////string jsonString = "";
            ////DataTable dtResult;
            ////DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            ////JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            ////if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////{
            ////    list_autocomplete = dtResult.AsEnumerable()
            ////    .Select(data => new Models.AutoComplete
            ////    {
            ////        id = data.Field<String>("DestinationCode"),
            ////        value = data.Field<String>("Destination")
            ////    }).ToList();

            ////    jsonString = objSerlizer.Serialize(list_autocomplete);
            ////    dtResult.Dispose();
            ////}
            ////else
            ////{
            ////    jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            ////}
            ////return jsonString;
        }


        [WebMethod(true)]
        public string GetNationalityCOR()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DestinationManager.GetNationalityCOR(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"NationalityMaster\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion Destination

        #region Mapped Hotel list

        //[WebMethod(EnableSession = true)]
        //public string GetMappedHotels()
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            string jsonString = "";
        //            JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //            DataTable dtResult;
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            string sid = Convert.ToString(objGlobalDefault.sid);
        //            if (objGlobalDefault.UserType == "SupplierStaff")
        //                sid = objGlobalDefault.ParentId.ToString();
        //            //DBHelper.DBReturnCode retcode = HotelMappingManager.GetMappedHotels(sid, out dtResult);

        //            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //            //{
        //            //List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
        //            //objHotelList = JsonStringManager.ConvertDataTable(dtResult);

        //            //if (objGlobalDefault.UserType == "Admin")
        //            //{
        //            //    var objHotelList = (from obj in DB.tbl_CommonHotelMasters orderby obj.sid ascending select obj).ToList();
        //            //}
        //            //else
        //            //{
        //                var objHotelList = (from obj in DB.tbl_CommonHotelMasters where obj.ParentID == Convert.ToInt64(sid) orderby obj.sid ascending select obj).ToList();
        //            //}

        //            var OfferList = (from Offer in DB.tbl_HotelOfferUtilities
        //                             join OfferDay in DB.tbl_OfferDayUtilities on Offer.HotelOfferId equals OfferDay.Offer_Id
        //                             join Room in DB.tbl_RoomDetailUtilities on Offer.Room_Id equals Room.Room_Id
        //                             where OfferDay.DateType == "Season Date"
        //                             select new
        //                             {
        //                                 OfferDay.BookBefore,
        //                                 OfferDay.DaysPrior,
        //                                 OfferDay.OfferType,
        //                                 OfferDay.DiscountAmount,
        //                                 OfferDay.DiscountPer,
        //                                 OfferDay.FreebiItem,
        //                                 OfferDay.FreebiItemDetail,
        //                                 OfferDay.HotelOfferCode,
        //                                 OfferDay.MinNights,
        //                                 OfferDay.MinRooms,
        //                                 OfferDay.NewRate,
        //                                 OfferDay.OfferCode,
        //                                 OfferDay.OfferNote,
        //                                 OfferDay.OfferOn,
        //                                 OfferDay.OfferTerms,
        //                                 OfferDay.ValidFrom,
        //                                 OfferDay.ValidTo,
        //                                 Offer.Hotel_Id,
        //                                 Offer.HotelOfferId,
        //                                 Offer.Room_Id,
        //                                 Room.RoomCategory
        //                             }).ToList();

        //            var RoomList = (from Room in DB.tbl_commonRoomDetails
        //                            join Hotel in DB.tbl_CommonHotelMasters on Room.HotelId equals Hotel.sid
        //                            join RoomType in DB.tbl_commonRoomTypes on Room.RoomTypeId equals RoomType.RoomTypeID
        //                            select new
        //                            {
        //                                Hotel.sid,
        //                                RoomType.RoomType,
        //                                Room.RoomId,


        //                            }).ToList();
        //            //dtResult.Dispose();
        //            return objserialize.Serialize(new { Session = 1, retCode = 1, MappedHotelList = objHotelList, OfferList = OfferList, RoomList = RoomList });
        //        }
        //        //}
        //        //else
        //        //{

        //        //}
        //    }
        //    catch (Exception)
        //    {

        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        [WebMethod(EnableSession = true)]
        public string GetMappedHotels()
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    string jsonString = "";
                    JavaScriptSerializer objserialize = new JavaScriptSerializer();
                    DataTable dtResult;
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string sid = Convert.ToString(objGlobalDefault.sid);
                    if (objGlobalDefault.UserType == "SupplierStaff")
                        sid = objGlobalDefault.ParentId.ToString();
                    //DBHelper.DBReturnCode retcode = HotelMappingManager.GetMappedHotels(sid, out dtResult);

                    //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    //{
                    //List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                    //objHotelList = JsonStringManager.ConvertDataTable(dtResult);

                    List<tbl_CommonHotelMaster> objHotelList = new List<tbl_CommonHotelMaster>();

                    //if (objGlobalDefault.UserType == "Admin")
                    //{
                    //    objHotelList = (from obj in DB.tbl_CommonHotelMasters orderby obj.sid ascending select obj).ToList();
                    //}
                    //else
                    //{
                    //    objHotelList = (from obj in DB.tbl_CommonHotelMasters where obj.ParentID == Convert.ToInt64(sid) orderby obj.sid ascending select obj).ToList();
                    //}

                    objHotelList = (from obj in DB.tbl_CommonHotelMasters orderby obj.sid ascending select obj).ToList();

                    var OfferList = new object();
                    var RoomList = (from Room in DB.tbl_commonRoomDetails
                                    join Hotel in DB.tbl_CommonHotelMasters on Room.HotelId equals Hotel.sid
                                    join RoomType in DB.tbl_commonRoomTypes on Room.RoomTypeId equals RoomType.RoomTypeID
                                    where Room.Approved != false
                                    select new
                                    {
                                        Hotel.sid,
                                        RoomType.RoomType,
                                        Room.RoomId,
                                    }).ToList();
                    //dtResult.Dispose();
                    return objserialize.Serialize(new { Session = 1, retCode = 1, MappedHotelList = objHotelList, OfferList = OfferList, RoomList = RoomList,SupplierId=objGlobalDefault.sid });
                }
                //}
                //else
                //{

                //}
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        //[WebMethod(EnableSession = true)]
        //public string GetBookingListSupplier(string[] SupplierList)
        //{
        //    try
        //    {
        //        List<tbl_AdminLogin> ListData = new List<tbl_AdminLogin>();
        //        for (int i = 0; i < SupplierList.Length; i++)
        //        {
        //            if (SupplierList[i] != null)
        //            {
        //                var Liist = (from obj in DB.tbl_AdminLogins
        //                             where obj.ParentID == Convert.ToInt32(SupplierList[i])

        //                             select new
        //                             {
        //                                 obj.sid,
        //                                 obj.AgencyName
        //                             }).ToList();

        //                for (int j = 0; j < Liist.Count; j++)
        //                {
        //                    ListData.Add(new tbl_AdminLogin
        //                    {
        //                        sid = Liist[j].sid,
        //                        AgencyName = Liist[j].AgencyName
        //                    });
        //                }

        //            }

        //        }
        //        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListData });

        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        [WebMethod(EnableSession = true)]
        public string GetMappedHotelstoUpdate(string Country, string City)
        {
            Int64 UID = AccountManager.GetSupplierByUser();
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            try
            {
                List<HotelInfo> objHotelList = new List<HotelInfo>();
                var arrSupplier = AuthorizationManager.GetAuthorizedSupplier();
                using(var DB  = new dbHotelhelperDataContext())
	            {
                    using (var db = new helperDataContext())
                    {
                        foreach (var SupplierID in arrSupplier)
                        {
                            var arrHotel = (from obj in DB.tbl_CommonHotelMasters
                                            join
                                                objSupplier in db.tbl_AdminLogins on obj.ParentID equals objSupplier.sid
                                            where obj.ParentID == SupplierID && obj.CityId.Contains(City) && obj.CountryId.Contains(Country)
                                            select new HotelInfo
                                            {
                                                HotelName = obj.HotelName,
                                                sid = obj.sid,
                                                SupplierName = objSupplier.AgencyName
                                            }).ToList();
                            foreach (var objHotel in arrHotel)
                            {
                                objHotelList.Add(objHotel);
                            }
                        }
                    }
		          
	            }
                if (objHotelList.Count !=0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, HotelListbyCity = objHotelList.OrderBy(d=>d.HotelName).ToList() });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0});
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = "No Hotel found for this Location." });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelByHotelId(Int64 HotelCode)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                string jsonString = "";
                JavaScriptSerializer objserialize = new JavaScriptSerializer();
                DataTable dtResult;
                DBHelper.DBReturnCode retcode = HotelMappingManager.GetHotelByHotelId(HotelCode, out dtResult);
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                    List<string> objFacilities = new List<string>();
                    objHotelList = JsonStringManager.ConvertDataTable(dtResult);
                    //var ContactList = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == HotelCode select obj).ToList();
                    var ContactList = (from obj in DB.Comm_HotelContacts where obj.HotelCode == HotelCode select obj).ToList();
                    var FacilityList = (from obj in DB.tbl_commonFacilities select obj).ToList();
                    dtResult.Dispose();
                    return objserialize.Serialize(new { Session = 1, retCode = 1, HotelListbyId = objHotelList, ContactList = ContactList, FacilityList = FacilityList });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }




        [WebMethod(true)]
        public string GetFacilities()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelMappingManager.GetFacilities(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelFacilities\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;

        }

        [WebMethod(true)]
        public string GetHalal()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelMappingManager.GetHalal(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelFacilities\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;

        }

        #endregion


        #region update Hotel Mapped Table

        //[WebMethod(true)]
        //public string SaveFacilities(string sFacilities,Int64 sHotelId)
        //{
        //    retCode = HotelMappingManager.MappedHotelSearch(sHotelId, out dtResult);

        //    DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    Int64 sUserID = objUser.sid;
        //    string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        try
        //        {
        //            tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
        //            Hotel.HotelFacilities = sFacilities;

        //            DB.SubmitChanges();
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //        catch (Exception ex)
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }
        //    }
        //    else
        //    {
        //        try
        //        {
        //            tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
        //            Hotel.HotelFacilities = sFacilities;

        //            DB.tbl_CommonHotelMasters.InsertOnSubmit(Hotel);
        //            DB.SubmitChanges();
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //        catch (Exception ex)
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }
        //    }

        //    return json;

        //}

        [WebMethod(true)]
        public string AddSupplier(Int64 srno, string SupplierName, string SupplierCode)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.AddSupplier(srno, SupplierName, SupplierCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        [WebMethod(true)]
        public string SaveFacilities(string sFacilities, Int64 sHotelId)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveFacilities(sFacilities, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string SaveRatings(string sRatings, Int64 sHotelId)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveRatings(sRatings, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string SaveImageUrl(string addedUrl, Int64 sHotelId)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveImageUrl(addedUrl, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion



        #region Rooms List
        [WebMethod(EnableSession = true)]
        public string GetRoomType()
        {
            DBHelper.DBReturnCode retcode = RoomsManager.GetRoomType(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomType\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddRoomAmenity(string RoomAmenityText, Int64 HotelId)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetUserByLogin();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                tbl_commonRoomDetail RoomList = DB.tbl_commonRoomDetails.Where(x => x.HotelId == HotelId).FirstOrDefault();
                var RoomAmenityList = (from obj in DB.tbl_commonAmunities where obj.RoomAmunityName == RoomAmenityText select obj).ToList();

                if (RoomAmenityList.Count() > 0)
                {
                    try
                    {
                        tbl_commonAmunity RoomAmenity = DB.tbl_commonAmunities.Single(x => x.RoomAmunityName == RoomAmenityText);

                        RoomAmenity.RoomAmunityName = RoomAmenityText;

                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomAmenityID\":" + RoomAmenity.RoomAmunityID + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }



                }
                else
                {
                    try
                    {
                        tbl_commonAmunity RoomAmenity = new tbl_commonAmunity();
                        RoomAmenity.RoomAmunityName = RoomAmenityText;


                        DB.tbl_commonAmunities.InsertOnSubmit(RoomAmenity);
                        DB.SubmitChanges();
                        Int64 RoomAmenityID = RoomAmenity.RoomAmunityID;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomAmenityID\":" + RoomAmenityID + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                return json;
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetRoomAmenities()
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var arrAminity = (from obj in DB.tbl_commonAmunities select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrRoomAmenities = arrAminity });
                }
            }
            catch
            {

            }
            return jsonString;
        }

        [WebMethod(true)]
        //public string AddHotelRoom(Int64 RoomID, Int64 HotelID, Int64 RoomTypeID, string RoomAmenitiesID, int MaxOccupacy, int MaxChilds, int AdultsWithChilds, int AdultsWithoutChilds, string RoomDescription, string RoomNotes, int MaxExtrabedAllowed, string RoomSize, string BeddingType, string SmokingAllowed)
        //{
        public string AddHotelRoom(Int64 RoomID, Int64 HotelID, Int64 RoomTypeID, string RoomAmenitiesID, int MaxOccupacy, int AdultsWithoutChilds, string RoomDescription, string RoomNotes, int MaxExtrabedAllowed, string RoomSize, string BeddingType, string SmokingAllowed)
        {
            //retCode = HotelMappingManager.SearchHotelMapp(sHotelName, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, out dtResult);
            using (var DB = new dbHotelhelperDataContext())
            {
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetSupplierByUser();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                tbl_commonRoomDetail RoomList = DB.tbl_commonRoomDetails.Where(x => x.HotelId == HotelID && x.RoomTypeId == RoomTypeID).FirstOrDefault();

                bool Approved = true;
                if (objUser.UserType == "Supplier")
                {    
                    Approved = false;
                }
                else if (objUser.UserType == "SupplierStaff")
                {
                    Approved = false;
                }
                else if (objUser.UserType == "Admin")
                {
                    Approved = true;
                }

                if (RoomList != null)
                {
                    RoomID = RoomList.RoomId;
                    try
                    {
                        tbl_commonRoomDetail Room = DB.tbl_commonRoomDetails.Single(x => x.RoomId == RoomID);

                        Room.RoomAmenitiesId = RoomAmenitiesID;
                        Room.HotelId = HotelID;
                        Room.RoomTypeId = RoomTypeID;
                        Room.RoomOccupancy = MaxOccupacy;
                        Room.MaxExtrabedAllowed = MaxExtrabedAllowed;
                        //Room.MaxChildAllowed = MaxChilds;
                        Room.NoOfChildWithoutBed = AdultsWithoutChilds;
                        // Room.AdultsWithChildAllowed = AdultsWithChilds;
                        Room.RoomSize = RoomSize;
                        Room.BeddingType = BeddingType;
                        Room.RoomDescription = RoomDescription;
                        Room.RoomNote = RoomNotes;
                        Room.SmokingAllowed = SmokingAllowed;
                        //Room.Interconnection = Interconnection;
                        //Room.RoomImage = MainImage;
                        //Room.SubImages = SubImages;
                        Room.UpdatedBy = sUserID;
                        Room.UpdatedDate = DateTimes;
                        //Room.CreatedBy = sUserID;
                        //Room.CreatedDate = DateTimes;
                        //Room.Approved = Approved;

                        DB.SubmitChanges();

                        var id = Room.RoomId;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"id\":\"" + Room.RoomTypeId + "\",\"UserType\":\"" + objUser.UserType + "\"}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }



                }
                else
                {
                    try
                    {
                        tbl_commonRoomDetail Room = new tbl_commonRoomDetail();
                        Room.RoomAmenitiesId = RoomAmenitiesID;
                        Room.HotelId = HotelID;
                        Room.RoomTypeId = RoomTypeID;
                        Room.RoomOccupancy = MaxOccupacy;
                        Room.MaxExtrabedAllowed = MaxExtrabedAllowed;
                        //  Room.MaxChildAllowed = MaxChilds;
                        Room.NoOfChildWithoutBed = AdultsWithoutChilds;
                        //  Room.AdultsWithChildAllowed = AdultsWithChilds;
                        Room.RoomSize = RoomSize;
                        Room.BeddingType = BeddingType;
                        Room.RoomDescription = RoomDescription;
                        Room.RoomNote = RoomNotes;
                        Room.SmokingAllowed = SmokingAllowed;
                        //Room.Interconnection = Interconnection;
                        //Room.RoomImage = MainImage;
                        //Room.SubImages = SubImages;
                        Room.UpdatedBy = sUserID;
                        Room.UpdatedDate = DateTimes;
                        Room.CreatedBy = sUserID;
                        Room.CreatedDate = DateTimes;
                        Room.Approved = Approved;

                        DB.tbl_commonRoomDetails.InsertOnSubmit(Room);
                        DB.SubmitChanges();
                        var id = Room.RoomId;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"id\":\"" + Room.RoomId + "\",\"UserType\":\"" + objUser.UserType + "\"}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                return json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddHotelAppprovedRoom(Int64 RoomID,Int64 ParentId, Int64 HotelID, Int64 RoomTypeID, string RoomAmenitiesID, int MaxOccupacy, int AdultsWithoutChilds, string RoomDescription, string RoomNotes, int MaxExtrabedAllowed, string RoomSize, string BeddingType, string SmokingAllowed)
        {
            //retCode = HotelMappingManager.SearchHotelMapp(sHotelName, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, out dtResult);
            using (var DB = new dbHotelhelperDataContext())
            {
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                tbl_commonRoomDetail RoomList = DB.tbl_commonRoomDetails.Where(x => x.HotelId == HotelID && x.RoomTypeId == RoomTypeID).FirstOrDefault();

                bool Approved = true;
                if (objUser.UserType == "Supplier")
                {
                    Approved = false;
                }
                else if (objUser.UserType == "SupplierStaff")
                {
                    Approved = false;
                }
                else if (objUser.UserType == "Admin")
                {
                    Approved = true;
                }

                if (RoomList != null)
                {
                    RoomID = RoomList.RoomId;
                    try
                    {
                        tbl_commonRoomDetail Room = DB.tbl_commonRoomDetails.Single(x => x.RoomId == RoomID);

                        Room.RoomAmenitiesId = RoomAmenitiesID;
                        Room.HotelId = HotelID;
                        Room.RoomTypeId = RoomTypeID;
                        Room.RoomOccupancy = MaxOccupacy;
                        Room.MaxExtrabedAllowed = MaxExtrabedAllowed;
                        //Room.MaxChildAllowed = MaxChilds;
                        Room.NoOfChildWithoutBed = AdultsWithoutChilds;
                        // Room.AdultsWithChildAllowed = AdultsWithChilds;
                        Room.RoomSize = RoomSize;
                        Room.BeddingType = BeddingType;
                        Room.RoomDescription = RoomDescription;
                        Room.RoomNote = RoomNotes;
                        Room.SmokingAllowed = SmokingAllowed;
                        //Room.Interconnection = Interconnection;
                        //Room.RoomImage = MainImage;
                        //Room.SubImages = SubImages;
                        Room.UpdatedBy = ParentId;
                        Room.UpdatedDate = DateTimes;
                        //Room.CreatedBy = ParentId;
                        //Room.CreatedDate = DateTimes;
                        Room.Approved = Approved;

                        DB.SubmitChanges();

                        var id = Room.RoomId;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"id\":\"" + Room.RoomTypeId + "\",\"UserType\":\"" + objUser.UserType + "\"}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }



                }
                else
                {
                    try
                    {
                        tbl_commonRoomDetail Room = new tbl_commonRoomDetail();
                        Room.RoomAmenitiesId = RoomAmenitiesID;
                        Room.HotelId = HotelID;
                        Room.RoomTypeId = RoomTypeID;
                        Room.RoomOccupancy = MaxOccupacy;
                        Room.MaxExtrabedAllowed = MaxExtrabedAllowed;
                        //  Room.MaxChildAllowed = MaxChilds;
                        Room.NoOfChildWithoutBed = AdultsWithoutChilds;
                        //  Room.AdultsWithChildAllowed = AdultsWithChilds;
                        Room.RoomSize = RoomSize;
                        Room.BeddingType = BeddingType;
                        Room.RoomDescription = RoomDescription;
                        Room.RoomNote = RoomNotes;
                        Room.SmokingAllowed = SmokingAllowed;
                        //Room.Interconnection = Interconnection;
                        //Room.RoomImage = MainImage;
                        //Room.SubImages = SubImages;
                        Room.UpdatedBy = ParentId;
                        Room.UpdatedDate = DateTimes;
                        Room.CreatedBy = ParentId;
                        Room.CreatedDate = DateTimes;
                        Room.Approved = Approved;

                        DB.tbl_commonRoomDetails.InsertOnSubmit(Room);
                        DB.SubmitChanges();
                        var id = Room.RoomId;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"id\":\"" + Room.RoomId + "\",\"UserType\":\"" + objUser.UserType + "\"}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                return json;
            }
        }

        [WebMethod(true)]
        public string AddRoomType(string RoomTypeText, Int64 HotelId)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = objUser.sid;
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                tbl_commonRoomDetail RoomList = DB.tbl_commonRoomDetails.Where(x => x.HotelId == HotelId).FirstOrDefault();
                var RoomTypeList = (from obj in DB.tbl_commonRoomTypes where obj.RoomType == RoomTypeText select obj).ToList();

                if (RoomTypeList.Count() > 0)
                {
                    try
                    {
                        tbl_commonRoomType RoomType = DB.tbl_commonRoomTypes.Single(x => x.RoomType == RoomTypeText);

                        RoomType.RoomType = RoomTypeText;

                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomTypeId\":" + RoomType.RoomTypeID + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }



                }
                else
                {
                    try
                    {
                        tbl_commonRoomType RoomType = new tbl_commonRoomType();
                        RoomType.RoomType = RoomTypeText;


                        DB.tbl_commonRoomTypes.InsertOnSubmit(RoomType);
                        DB.SubmitChanges();
                        Int64 RoomTypeId = RoomType.RoomTypeID;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomTypeId\":" + RoomTypeId + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                return json;
            }
        }


        #endregion

        #region Hotel List

        [WebMethod(EnableSession = true)]
        public string HotelActivation(Int64 HotelId, string Status)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMasters.Single(x => x.sid == HotelId);
                    Hotel.Active = Status;
                    DB.SubmitChanges();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, ActiveHotel = Hotel.Active });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        #endregion

        #region Hotel Images
        [WebMethod(true)]
        public string UpdateImage(Int64 sHotelId, string addedUrl)
        {
            retCode = HotelMappingManager.SaveImageUrl(addedUrl, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        //[WebMethod(EnableSession = true)]
        //public string DeleteSubImage(string FileNo, string noFiles, string sid)
        //{
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    try
        //    {
        //        string Path = System.Web.HttpContext.Current.Server.MapPath("~/HotelImages/");
        //        string FileName = FileNo;
        //        DirectoryInfo dir = new DirectoryInfo(Path);
        //        FileInfo[] files = null;

        //        files = dir.GetFiles();

        //        foreach (FileInfo file in files)
        //        {
        //            if (file.Name == FileName)
        //            {
        //                file.Delete();
        //                break;
        //            }
        //            else
        //            {
        //                continue;
        //            }

        //        }
        //        DataManager.DBReturnCode retCode = HotelMappingManager.SubImages(noFiles, sid);
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch (Exception ex)
        //    {
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //}
        #endregion


        #region Search Add
        [WebMethod(true)]
        public string AddSearch(string CityName, string CheckIn, string CheckOut, string HotelCode, string TotalNights, string StarRating, string Nationality, string NumberOfRooms, string EntryDate, string AddSearchsession)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            string Franchisee = objGlobalDefault.Franchisee;
            DBHelper.DBReturnCode retCode = HotelManager.AddHotelSearch(CityName, CheckIn, CheckOut, HotelCode, TotalNights, StarRating, Nationality, NumberOfRooms, EntryDate, UserId, AddSearchsession);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        #region Counts

        [WebMethod(EnableSession = true)]
        public string GetCount()
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Hotel = 0, Agent = 0;
                    Int64 Uid = AccountManager.GetUserByLogin();
                    using (var  db = new dbml.helperDataContext() )
                    {
                        Hotel = (from obj in DB.tbl_CommonHotelMasters where obj.ParentID == Uid select obj).ToList().Count;
                        Agent = (from obj in db.tbl_AdminLogins where obj.ParentID == Uid && obj.UserType == "Agent" select obj).ToList().Count;
                    }
                    var arrBookings = Reservations.BookingList();
                    List<Reservation> arrBuyerRevt = new List<Reservation>();
                    List<Reservation> arrSupplierRevt = new List<Reservation>();
                    List<Reservation> arrDestinationsRevt = new List<Reservation>();
                    List<Reservation> arrHotelsRevt = new List<Reservation>();

                    arrBookings.Where(d => d.Source.Contains("HOTELBEDS")).ToList().ForEach(r => r.Source = "HOTELBEDS");
                    foreach (Int64 AgentID in arrBookings.Select(D=>D.Uid).ToList().Distinct())
                    {
                        arrBuyerRevt.Add(new Reservation
                        {
                            Buyer = arrBookings.Where(d => d.Uid == AgentID).FirstOrDefault().AgencyName,
                            Count = arrBookings.Where(d => d.Uid == AgentID).Count()
                        });
                    }
                    foreach (string  Supplier in arrBookings.Select(D => D.Source).ToList().Distinct())
                    {
                        arrSupplierRevt.Add(new Reservation
                        {
                            Supplier = arrBookings.Where(d => d.Source == Supplier).FirstOrDefault().Source,
                            Count = arrBookings.Where(d => d.Source == Supplier).Count()
                        });
                    }
                    foreach (string Country in arrBookings.Select(D => D.City).ToList().Distinct())
                    {
                        arrDestinationsRevt.Add(new Reservation
                        {
                            Destination = arrBookings.Where(d => d.City == Country).FirstOrDefault().City,
                            Count = arrBookings.Where(d => d.City == Country).Count()
                        });
                    }
                    foreach (string HotelName in arrBookings.Select(D => D.HotelName).ToList().Distinct())
                    {
                        arrHotelsRevt.Add(new Reservation
                        {
                            HotelName = HotelName,
                            Count = arrBookings.Where(d => d.HotelName == HotelName).Count()
                        });
                    }

                    var arrOnRequest = new 
                    {
                        OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                        OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                        OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrOnHold = new
                    {
                       OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                       OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                       OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrReconfirmed = new
                    {
                        ReconfirmPending = arrBookings.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().Count,
                        ReconfirmRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().Count,
                        ReconfirmReject = arrBookings.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrGroupRequest = new
                    {
                        GroupRequestPending = arrBookings.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestReject = arrBookings.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().Count,
                    };

                    json = jsSerializer.Serialize(new {
                        Session = 1, retCode = 1, 
                        HotelList = Hotel,
                        AgentCount = Agent,
                        arrBuyerRervation = arrBuyerRevt.OrderByDescending(d=>d.Count).Take(5),
                        arrSupplierRevt = arrSupplierRevt.OrderByDescending(d => d.Count).Take(5),
                        arrDestinationsRevt = arrDestinationsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrHotelsRevt = arrHotelsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrOnRequest = arrOnRequest,
                        arrOnHold = arrOnHold,
                        arrReconfirmed = arrReconfirmed,
                        arrGroupRequest = arrGroupRequest,
                        arrBookings=arrBookings
                    });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 ,ex= ex.Message});
            }
            return json;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string SetMarkup(string HotelCode, Int64 Per, Int64 Amt)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 sUserID = AccountManager.GetUserByLogin();

                    var MrkCOunt = (from obj in DB.tbl_CommHotelMarkups where obj.HotelID == Convert.ToInt32(HotelCode) && obj.SupplierId == sUserID select obj).ToList();
                    if (MrkCOunt.Count != 0)
                    {
                        DB.tbl_CommHotelMarkups.DeleteAllOnSubmit(MrkCOunt);
                        DB.SubmitChanges();
                    }
                    tbl_CommHotelMarkup Add = new tbl_CommHotelMarkup();
                    Add.HotelID = Convert.ToInt32(HotelCode);
                    Add.SupplierId = sUserID;
                    Add.Per = Per;
                    Add.Amt = Amt;
                    DB.tbl_CommHotelMarkups.InsertOnSubmit(Add);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteHote(Int64 Id)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    tbl_CommonHotelMaster Delete = DB.tbl_CommonHotelMasters.Single(x => x.sid == Id);
                    DB.tbl_CommonHotelMasters.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }
}
