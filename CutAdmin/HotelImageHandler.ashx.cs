﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelImageHandler
    /// </summary>
    public class HotelImageHandler : IHttpHandler
    {
        string ImageNames;
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        public void ProcessRequest(HttpContext context)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
                string sid = System.Convert.ToString(context.Request.QueryString["sid"]);
                string ImgPath = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
                string[] tokens = ImgPath.Split('^');
                int TotalImgs = tokens.Count() - 1;
                string[] newFiles;

                Int64 srno = Convert.ToInt64(sid);
                string OldImages = (from obj in DB.tbl_CommonHotelMasters where obj.sid == srno select obj.SubImages).FirstOrDefault();
                //string[] OldTokens = OldImages.Split('^');
                string NewImages = "";
                string SavedImages = "";


                for (int img = 0; img < tokens.Count(); img++)
                {
                    if (tokens[img] != "")
                    {
                        string token1 = tokens[img].Split('.')[0];
                        string ext1 = tokens[img].Split('.')[1];
                        string ext = ext1.Split(',')[0];
                        if (OldImages != null && OldImages.Contains(token1))
                        {
                            SavedImages = OldImages;
                        }
                        else
                        {
                            NewImages += sid + "-" + token1 + '.' + ext + "^";
                        }
                    }

                }
                string[] New = NewImages.Split('^');
                ImageNames = SavedImages + "^" + NewImages;

                if (context.Request.Files.Count > 0)
                {

                    HttpFileCollection files = context.Request.Files;
                    if (TotalImgs == files.Count)
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];
                            string token1 = tokens[i].Split('.')[0];
                            string myFilePath = sid + "-" + token1;
                            string ext1 = tokens[i].Split('.')[1];
                            string ext = "jpg";
                            if (ext != ".pdf")
                            {
                                string FileName = myFilePath;
                                FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath + '.' + ext);
                                file.SaveAs(FileName);
                                //retCode = ActivityManager.UpdateImages(sFileName, sid);
                                context.Response.Write("1");
                            }
                            else
                            {
                                context.Response.Write("0");

                            }
                        }
                        if (ImageNames != null)
                        {
                            ImgPath = "";
                            ImgPath = ImageNames;
                            retCode = HotelMappingManager.HotelImages(ImgPath, sid);
                        }

                    }
                    else
                    {
                        string Name;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];
                            string myFilePath = New[i];
                            string ext = Path.GetExtension(myFilePath);
                            if (ext != ".pdf")
                            {
                                string FileName = myFilePath;
                                FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath);
                                file.SaveAs(FileName);
                                //retCode = ActivityManager.UpdateImages(sFileName, sid);
                                context.Response.Write("1");
                            }
                            else
                            {
                                context.Response.Write("0");

                            }
                        }
                        if (ImageNames != null)
                        {
                            ImgPath = "";
                            ImgPath = ImageNames;
                            retCode = HotelMappingManager.HotelImages(ImgPath, sid);
                        }
                    }


                }
                else
                {
                    //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                    if (retCode == DataManager.DBReturnCode.SUCCESS)
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}