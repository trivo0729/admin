﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using CutAdmin.Common;
using CutAdmin.BL;
using System.Data;
using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelMappingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelMappingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Hotel Parsing

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string HotelList(string session)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = int.MaxValue;
            try
            {
                if (Session["session" + session] != null && session == Session["session" + session].ToString() && Session["newCommonHotels" + session] != null)
                {
                    List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                    List_CommonHotelDetails = (List<CommonLib.Response.CommonHotelDetails>)Session["newCommonHotels" + session];
                    //List_CommonHotelDetails = HotelMappingManager.skipMappedHotels(List_CommonHotelDetails);
                    Session["newCommonHotels" + session] = List_CommonHotelDetails;
                    string htlname = session.Split('_')[7];
                    if (htlname != "")
                    {

                        List_CommonHotelDetails = List_CommonHotelDetails.Where(c => c.HotelName.Contains(htlname)).ToList();
                    }
                    return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, List_CommonHotelDetails = List_CommonHotelDetails });
                }
                else
                {
                    Session["FiterHistory"] = null;
                    Session["List_HotelDetail_Filter"] = null;
                    GlobalDefault objGlobalDefault = null;
                    string jsonString = "";
                    string Response = "";
                    bool bResponse_MGH = false;
                    Session["session"] = session;
                    Session["session" + session] = session;
                    objSerlizer.MaxJsonLength = Int32.MaxValue;

                    List<CommonLib.Response.Facility> lstFacility = new List<CommonLib.Response.Facility>();
                    List<CommonLib.Response.Category> lstCategory = new List<CommonLib.Response.Category>();
                    List<CommonLib.Response.Location> lstLocation = new List<CommonLib.Response.Location>();

                    if (CutAdmin.Common.Common.Session(out objGlobalDefault))
                    {
                        // 

                        DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
                        if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
                        {
                            objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                        }
                        List<HotelLib.Response.HotelDetail> List_HotelDetail;
                        float min_price;
                        float max_price;
                        int m_counthotel;
                        float min_price_Common;
                        float max_price_Common;
                        int m_counthotel_Common;
                        Models.Hotel objHotel = new Models.Hotel { };
                        Models.MGHHotel objMGHHotel = new Models.MGHHotel { };
                        Models.DoTWHotel objDoTWHotel = new Models.DoTWHotel { };
                        Models.EANHotels objEANHotels = new Models.EANHotels { };
                        Models.GRNHotels ObjGRNHotel = new Models.GRNHotels { };
                        //Models.GRNAvailReq


                        HttpContext context = HttpContext.Current;
                        //var parentThreadCulture = System.Globalization.CultureInfo.InvariantCulture;
                        var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                        //................................HotelBeds..........B..............................................//
                        Models.StatusCode objStatusCode;
                        Common.XMLHelper objXmlHelper = new XMLHelper();
                        objXmlHelper.session = session;
                        Common.XMLHelper.context = context;


                        objXmlHelper.objGlobalDefault = objGlobalDefault;
                        objXmlHelper.CultureInfo = parentThreadCulture;
                        objXmlHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;

                        Models.MGHStatusCode objMGHStatusCode;
                        Common.MghXMLHelper objMghXMLHelper = new MghXMLHelper();
                        objMghXMLHelper.session = session;
                        Common.MghXMLHelper.context = context;
                        objMghXMLHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;

                        HttpContext ctx = HttpContext.Current;
                        Models.DOTWStatusCode objDOTWStatusCode = new Models.DOTWStatusCode();
                        Common.DOTWXMLHelper objDOTWXMLHelper = new DOTWXMLHelper();
                        objDOTWXMLHelper.session = session;
                        objDOTWXMLHelper.context = context;
                        objDOTWXMLHelper.objGlobalDefault = objGlobalDefault;
                        objDOTWXMLHelper.objAgentDetailsOnAdmin = objAgentDetailsOnAdmin;
                        objDOTWXMLHelper.CultureInfo = parentThreadCulture;
                        // Thread DotwTh = new Thread(new ThreadStart(objDOTWXMLHelper.AvailRequest));
                        //  DotwTh.Priority = ThreadPriority.Lowest;
                        // bool bResponse_Dotw = false; ;
                        // bResponse_Dotw = Common.DOTWXMLHelper.AvailRequest(session, out objDOTWStatusCode);
                        //LogManager.Add(0, objDOTWStatusCode.RequestHeader, objDOTWStatusCode.Request, objDOTWStatusCode.ResponseHeader, objDOTWStatusCode.Response, objGlobalDefault.sid, objDOTWStatusCode.Status);
                        // end respons/
                        var Inverient = System.Globalization.CultureInfo.InvariantCulture;
                        #region Expidia Request & Respons
                        List<EANLib.Response.HotelSummary> List_EANHotels;
                        Models.EANStatusCode objEANStatusCode;
                        //List<EANLib.Response.ValueAdd> Category = new List<EANLib.Response.ValueAdd>();
                        Common.EANXMLhelper objEANXMLhelper = new EANXMLhelper();
                        EANXMLhelper.Context = context;
                        objEANXMLhelper.session = session;
                        //GRN
                        JsonHelper ObjGRNHelper = new JsonHelper();
                        JsonHelper.Context = context;
                        ObjGRNHelper.session = session;

                        GRNHotelList ObjGRNHotelList = new GRNHotelList();
                        GtaHotelList ObjGtaHoteList = new GtaHotelList();
                        HotelBedsList ObjHotelBedsList = new HotelBedsList();
                        ExpediaHotelList ObjExpediaHotelList = new ExpediaHotelList();

                        JsonHelper.Context = context;
                        ObjGtaHoteList.Session = session;
                        ObjGRNHotelList.Session = session;
                        ObjHotelBedsList.Session = session;
                        ObjExpediaHotelList.Session = session;
                        //while (objDOTWXMLHelper.Response == false) { objDOTWXMLHelper.AvailRequest(true); }
                        // objXmlHelper.AvailRequest();
                        Parallel.For(0, 7, i =>
                        {
                            Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                            if (i == 0)
                                objXmlHelper.AvailRequest();

                            else if (i == 1)
                                objMghXMLHelper.AvailRequest();
                            else if (i == 2)
                                objDOTWXMLHelper.AvailRequest(true);
                            else if (i == 3)
                                //ObjGRNHelper.AvailReq();.
                                ObjGRNHotelList.GetGRNHotels();
                            else if (i == 4)
                                ObjGtaHoteList.GetGtaHotels();
                            ////////else if( i== 5)
                            ////////   ObjGRNHelper.AvailReq();
                            //else if (i == 5)
                               // ObjHotelBedsList.GetHotelBedsList();
                            else if (i == 6)
                                ObjExpediaHotelList.GetExpediaHotelList();

                        });

                        //for (int i = 0; i < 4;i++ )
                        //{
                        //    if (i == 0)
                        //        objXmlHelper.AvailRequest();
                        //    else if (i == 1)
                        //        objMghXMLHelper.AvailRequest();
                        //    else if (i == 2)
                        //        objDOTWXMLHelper.AvailRequest(true);
                        //    else if (i == 3)
                        //        ObjGRNHelper.AvailReq();
                        //}

                        if (objEANXMLhelper.bResponse)
                        {
                            Session["EanAvail"] = objEANXMLhelper.objEANHotels;
                            objEANHotels = objEANXMLhelper.objEANHotels;
                        }


                        #endregion Expidia Request & Respons



                        // Parallel.Invoke(() => objXmlHelper.AvailRequest(), () => objMghXMLHelper.AvailRequest(), () => objDOTWXMLHelper.AvailRequest(true));

                        //HotelBedsTh.Start(); 
                        //MghTh.Start(); 
                        //DotwTh.Start();
                        Boolean HotelBedsIsAlive = true;
                        Boolean MghIsAlive = true;
                        Boolean DotwIsAlive = true;
                        Models.CommonStatusCode objCommonStatusCode;
                        bool bResponse_Common = Common.CommonXMLHelper.AvailRequest(session, out objCommonStatusCode);
                        //objStatusCode = new Models.CommonStatusCode {  DisplayRequest = objMGHAvail, FDate = fDate, TDate = tDate, Occupancy = m_List_Hotel_Occupancy };
                        int count;
                        //int countCommon;





                        if (objMghXMLHelper.Response)
                        {
                            Session["MGHStatus"] = objMghXMLHelper.objStatusCode;
                            Session["MGHAvail"] = objMghXMLHelper.objMGHHotel;
                            Session["MGHRooms"] = objMghXMLHelper.objStatusCode.DisplayRequest.Room;
                            objMGHHotel = objMghXMLHelper.objMGHHotel;
                            // LogManager.Add(0, objMghXMLHelper.objStatusCode.RequestHeader, objMghXMLHelper.objStatusCode.Request, objMghXMLHelper.objStatusCode.ResponseHeader, objMghXMLHelper.objStatusCode.Response, objXmlHelper.objGlobalDefault.sid, objMghXMLHelper.objStatusCode.Status);
                            //}
                        }
                        if (objDOTWXMLHelper.Response)
                        {
                            Session["DoTW_Night"] = objDOTWXMLHelper.objStatusCode.DisplayRequest.Night;
                            Session["DoTW_Occupancy"] = objDOTWXMLHelper.objStatusCode.Occupancy;
                            Session["DoTW_NumberOfRoom"] = objDOTWXMLHelper.objStatusCode.DisplayRequest.room.Count;
                            Session["DoTWAvail"] = objDOTWXMLHelper.objDoTWHotel;
                            objDoTWHotel = objDOTWXMLHelper.objDoTWHotel;
                            // LogManager.Add(0, objDOTWXMLHelper.objStatusCode.RequestHeader, objDOTWXMLHelper.objStatusCode.Request, objDOTWXMLHelper.objStatusCode.ResponseHeader, objDOTWXMLHelper.objStatusCode.Response, objXmlHelper.objGlobalDefault.sid, objDOTWXMLHelper.objStatusCode.Status);
                        }
                        if (ObjGRNHelper.isResponse)
                        {
                            Session["GRN_Night"] = ObjGRNHelper.objStatusCode.DisplayRequest.Night;
                            Session["GRN_Occupancy"] = ObjGRNHelper.objStatusCode.Occupancy;
                            Session["GRN_NumberOfRoom"] = ObjGRNHelper.objStatusCode.NoofRoom;
                            Session["GRNAvail"] = ObjGRNHelper.objHotel;
                            ObjGRNHotel = ObjGRNHelper.objHotel;
                        }
                        List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                        if (objMghXMLHelper.Response == true || objDOTWXMLHelper.Response == true)
                        {
                            // int countCommon = objXmlHelper.objStatusCode.DisplayRequest.Night;
                            ParseCommonResponse objParseCommonResponse = new ParseCommonResponse();
                            List_CommonHotelDetails = objParseCommonResponse.GetCommon(objHotel, objMGHHotel, objDoTWHotel, objEANHotels, ObjGRNHotel, out lstFacility, out lstCategory, out lstLocation);
                            List_CommonHotelDetails = List_CommonHotelDetails.OrderBy(d => d.HotelName).ToList();
                        }
                        if (ObjGtaHoteList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjGtaHoteList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        //GRN Chnages
                        if (ObjGRNHotelList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjGRNHotelList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        if (ObjHotelBedsList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjHotelBedsList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        if (ObjExpediaHotelList.Hotels != null)
                        {
                            foreach (CommonLib.Response.CommonHotelDetails Hotel in ObjExpediaHotelList.Hotels)
                            {
                                List_CommonHotelDetails.Add(Hotel);
                            }

                        }
                        List_CommonHotelDetails = HotelMappingManager.skipMappedHotels(List_CommonHotelDetails);
                        if (List_CommonHotelDetails != null)
                            Session["newCommonHotels" + session] = List_CommonHotelDetails;
                        else
                            return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                        string htlname = session.Split('_')[7];
                        if (htlname != "")
                        {
                            List_CommonHotelDetails = List_CommonHotelDetails.Where(c => c.HotelName.Contains(htlname)).ToList();
                        }

                        return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, List_CommonHotelDetails = List_CommonHotelDetails, objUser = objUser });
                    }
                    else
                        return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });

                }
            }
            catch (Exception ex)
            {
                return jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0, error = ex.Message });
            }


        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMappedHotel(string HotelCode, string session)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            if (Session["newCommonHotels" + session] != null)
            {
                List<CommonLib.Response.CommonHotelDetails> List_CommonHotelDetails = new List<CommonLib.Response.CommonHotelDetails>();
                List_CommonHotelDetails = (List<CommonLib.Response.CommonHotelDetails>)Session["newCommonHotels" + session];
                List<CommonLib.Response.CommonHotelDetails> objMapped = new List<CommonLib.Response.CommonHotelDetails>();


                foreach (string HotelId in HotelCode.Split(','))
                {
                    objMapped.Add(List_CommonHotelDetails.Single(d => d.HotelId == HotelId));
                }
                return objSerialize.Serialize(new { Session = 1, retCode = 1, CommonHotel = objMapped });

            }
            else
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetSelectedHotels(string sHotelDescription, string sSubImages, string sHotelFacilitiesText)
        {
          using (var DB = new dbHotelhelperDataContext())
            {
                //string[] arrFacilities;
                //char[] splitchar = { ',' };
                //arrFacilities = sHotelFacilitiesText.Split(splitchar);

                JavaScriptSerializer objSerialize = new JavaScriptSerializer();
                List<CommonLib.Response.CommonHotelDetails> SelectedHotels = new List<CommonLib.Response.CommonHotelDetails>();
                Session["SelectedHotelDetails"] = sHotelDescription + "|||" + sSubImages;
                if (sHotelFacilitiesText != "")
                {
                    foreach (string Facility in sHotelFacilitiesText.Split(','))
                    {
                        try
                        {
                            var facility = (from obj in DB.tbl_commonFacilities where obj.HotelFacilityName == Facility select obj).ToList();
                            if (facility.Count() > 0)
                            {
                                facility[0].HotelFacilityName = Facility;
                                DB.SubmitChanges();

                                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                            }
                            else
                            {
                                tbl_commonFacility tblFacility = new tbl_commonFacility();

                                tblFacility.HotelFacilityName = Facility;

                                DB.tbl_commonFacilities.InsertOnSubmit(tblFacility);

                                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                                DB.SubmitChanges();

                            }


                        }
                        catch (Exception ex)
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                        //return json;

                    }
                }



                return objSerialize.Serialize(new { Session = 1, retCode = 1 });
            }
        }

        #endregion Hotel Parsing

        #region Add Mapping with sp

        //[WebMethod(true)]
        //public string AddHotelMapping(string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sMainImage, string sSubImages, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sCountryCode, string sCityCode, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime)
        //{
        //    //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

        //    DBHelper.DBReturnCode retCode = HotelMappingManager.AddHotelMapping(sHotelName, sHotelAddress, sDescription, sRatings, sFacilities, sLangitude, sLatitude, sMainImage, sSubImages, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sCountryCode, sCityCode, sZipCode, sPatesAllowed, sLiquorPolicy, sTripAdviserLink, sHotelGroup, sAdultMinAge, sChildAgeFrom,sChildAgeTo, sCheckinTime, sCheckoutTime);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    else
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //}


        #endregion

        #region Add Mapping with linq
        [WebMethod(true)]
        public string AddHotelMapping(Int64 sHotelCode, string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sGTACode, string sCountryCode, string sCityCode, Int64 LocationID, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string Smooking, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime, int TotalRooms, int TotalFloors, string BuildYear, string RenovationYear, string HotelPhone, string HotelMobile, string HotelFax, string HotelPerson, string HotelEmail, string ReservationPhone, string ReservationMobile, string ReservationFax, string ReservationPerson, string ReservationEmail, string AccountsPhone, string AccountsMobile, string AccountsFax, string AccountsPerson, string AccountsEmail, List<Comm_TaxMapping> RatesDetails, List<Comm_TaxMapping> MealRates)
        {
            DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sUserID = 0;
            bool Approved = true;
            if (objUser.UserType=="Supplier")
            {
                sUserID = objUser.sid;
                Approved = false;
            }
            else if (objUser.UserType=="SupplierStaff")
            {
                sUserID = objUser.ParentId;
                Approved = false;
            }
            else if (objUser.UserType == "Admin")
            {
                sUserID = objUser.sid;
                Approved = true;
            }
            
            retCode = HotelMappingManager.SearchHotelMapp(sHotelName,sUserID, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sGTACode, out dtResult);

           
           
            string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                  using (var DB = new dbHotelhelperDataContext())
                    {
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        if (MealRates.Count != 0)
                        {
                            MealRates.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(MealRates);
                        }

                        tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMasters.Single(x => x.sid == sHotelCode);
                        List<tbl_CommonHotelContact> Contacts = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        List<Comm_HotelContact> Contacts1 = (from obj in DB.Comm_HotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        if (sHotelName != "")
                        {
                            if (objUser.UserType == "Supplier")
                                Hotel.HotelName = Hotel.HotelName;
                            else if (objUser.UserType != "SupplierStaff")
                                Hotel.HotelName = Hotel.HotelName;
                            else
                                Hotel.HotelName = sHotelName;
                        }
                        if (sHotelAddress != "")
                        {
                            Hotel.HotelAddress = sHotelAddress;
                        }
                        if (sFacilities != "")
                        {
                            Hotel.HotelFacilities = sFacilities;
                        }
                        if (sCheckinTime != "")
                        {
                            Hotel.CheckinTime = sCheckinTime;
                        }
                        if (sCheckoutTime != "")
                        {
                            Hotel.CheckoutTime = sCheckoutTime;
                        }
                        if (sDescription != "")
                        {
                            Hotel.HotelDescription = sDescription;
                        }
                        if (sAdultMinAge != 0)
                        {
                            Hotel.AdultMinAge = sAdultMinAge;
                        }
                        if (sChildAgeFrom != 0)
                        {
                            Hotel.ChildAgeFrom = sChildAgeFrom;
                        }
                        if (sChildAgeTo != 0)
                        {
                            Hotel.ChildAgeTo = sChildAgeTo;
                        }
                        if (TotalRooms != 0)
                        {
                            Hotel.TotalRooms = TotalRooms;
                        }
                        if (TotalFloors != 0)
                        {
                            Hotel.TotalFloors = TotalFloors;
                        }
                        if (BuildYear != "")
                        {
                            Hotel.BuildYear = BuildYear;
                        }
                        if (RenovationYear != "")
                        {
                            Hotel.RenovationYear = RenovationYear;
                        }

                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        // Hotel.ParentID = sUserID;
                       // Hotel.Approved = Approved;
                        //Hotel.HotelImage = sMainImage;
                        //Hotel.SubImages = sSubImages;
                        if (sDotwCode != "")
                        {
                            Hotel.DotwCode = sDotwCode;
                        }
                        if (sHotelBedsCode != "")
                        {
                            Hotel.HotelBedsCode = sHotelBedsCode;
                        }
                        if (sMGHCode != "")
                        {
                            Hotel.MGHCode = sMGHCode;
                        }
                        if (sGRNCode != "")
                        {
                            Hotel.GRNCode = sGRNCode;
                        }
                        if (sExpediaCode != "")
                        {
                            Hotel.ExpediaCode = sExpediaCode;
                        }
                        if (sGTACode != "")
                        {
                            Hotel.GTACode = sGTACode;
                        }
                        if (Hotel.BaseSupplier != "")
                        {
                            Hotel.BaseSupplier = "ClickUrtrip";
                        }
                        //Hotel.UpdatedBy = sUserID;
                        Hotel.UpdatedDate = DateTimes;
                        if (sHotelGroup != "")
                        {
                            Hotel.HotelGroup = sHotelGroup;
                        }
                        if (sPatesAllowed != "")
                        {
                            Hotel.PatesAllowed = sPatesAllowed;
                        }
                        if (sTripAdviserLink != "")
                        {
                            Hotel.TripAdviserLink = sTripAdviserLink;
                        }
                        if (sLiquorPolicy != "")
                        {
                            Hotel.LiquorPolicy = sLiquorPolicy;
                        }
                        if (Smooking != "")
                        {
                            Hotel.Smooking = Smooking;
                        }
                        //Hotel.HalalId = HalalId;
                        //Hotel.HotelDetails = HotelDetails;
                        // Hotel.BaseSupplier = BaseSupplier;
                        // Hotel.AmenitiesCode = AmenitiesCode;
                        //  Hotel.Smooking = Smooking;
                        // Hotel.BuildYear = BuildYear;
                        // Hotel.RenovationYear = RenovationYear;
                        // Hotel.TotalFloors = TotalFloors;
                        // Hotel.TotalRooms = TotalRooms;
                        // Hotel.HotelNote = HotelNote;
                        // Hotel.Smooking = Smooking;
                        //if (Contacts.Count() > 0)
                        //{
                        //    if (HotelPhone != "")
                        //    {
                        //        Contacts[0].HotelPhone = HotelPhone;
                        //    }
                        //    if (HotelMobile != "")
                        //    {
                        //        Contacts[0].HotelMobile = HotelMobile;
                        //    }
                        //    if (HotelFax != "")
                        //    {
                        //        Contacts[0].HotelFax = HotelFax;
                        //    }
                        //    if (HotelPerson != "")
                        //    {
                        //        Contacts[0].HotelContactPerson = HotelPerson;
                        //    }
                        //    if (HotelEmail != "")
                        //    {
                        //        Contacts[0].HotelEmail = HotelEmail;
                        //    }
                        //    //
                        //    if (ReservationPhone != "")
                        //    {
                        //        Contacts[0].ReservationPhone = ReservationPhone;
                        //    }
                        //    if (ReservationMobile != "")
                        //    {
                        //        Contacts[0].ReservationMobile = ReservationMobile;
                        //    }
                        //    if (ReservationFax != "")
                        //    {
                        //        Contacts[0].ReservationFax = ReservationFax;
                        //    }
                        //    if (ReservationPerson != "")
                        //    {
                        //        Contacts[0].ReservationContactPerson = ReservationPerson;
                        //    }
                        //    if (ReservationEmail != "")
                        //    {
                        //        Contacts[0].ReservationEmail = ReservationEmail;
                        //    }

                        //    //
                        //    if (AccountsPhone != "")
                        //    {
                        //        Contacts[0].AccountsPhone = AccountsPhone;
                        //    }
                        //    if (AccountsMobile != "")
                        //    {
                        //        Contacts[0].AccountsMobile = AccountsMobile;
                        //    }
                        //    if (AccountsFax != "")
                        //    {
                        //        Contacts[0].AccountsFax = AccountsFax;
                        //    }
                        //    if (AccountsPerson != "")
                        //    {
                        //        Contacts[0].AccountsContactPerson = AccountsPerson;
                        //    }
                        //    if (AccountsEmail != "")
                        //    {
                        //        Contacts[0].AccountsEmail = AccountsEmail;
                        //    }

                        //    Contacts[0].HotelCode = Hotel.sid;
                        //    DB.SubmitChanges();
                        //}
                        //else
                        //{
                        //    tbl_CommonHotelContact Contact = new tbl_CommonHotelContact();

                        //    if (HotelPhone != "")
                        //    {
                        //        Contact.HotelPhone = HotelPhone;
                        //    }
                        //    if (HotelMobile != "")
                        //    {
                        //        Contact.HotelMobile = HotelMobile;
                        //    }
                        //    if (HotelFax != "")
                        //    {
                        //        Contact.HotelFax = HotelFax;
                        //    }
                        //    if (HotelPerson != "")
                        //    {
                        //        Contact.HotelContactPerson = HotelPerson;
                        //    }
                        //    if (HotelEmail != "")
                        //    {
                        //        Contact.HotelEmail = HotelEmail;
                        //    }
                        //    //
                        //    if (ReservationPhone != "")
                        //    {
                        //        Contact.ReservationPhone = ReservationPhone;
                        //    }
                        //    if (ReservationMobile != "")
                        //    {
                        //        Contact.ReservationMobile = ReservationMobile;
                        //    }
                        //    if (ReservationFax != "")
                        //    {
                        //        Contact.ReservationFax = ReservationFax;
                        //    }
                        //    if (ReservationPerson != "")
                        //    {
                        //        Contact.ReservationContactPerson = ReservationPerson;
                        //    }
                        //    if (ReservationEmail != "")
                        //    {
                        //        Contact.ReservationEmail = ReservationEmail;
                        //    }

                        //    //
                        //    if (AccountsPhone != "")
                        //    {
                        //        Contact.AccountsPhone = AccountsPhone;
                        //    }
                        //    if (AccountsMobile != "")
                        //    {
                        //        Contact.AccountsMobile = AccountsMobile;
                        //    }
                        //    if (AccountsFax != "")
                        //    {
                        //        Contact.AccountsFax = AccountsFax;
                        //    }
                        //    if (AccountsPerson != "")
                        //    {
                        //        Contact.AccountsContactPerson = AccountsPerson;
                        //    }
                        //    if (AccountsEmail != "")
                        //    {
                        //        Contact.AccountsEmail = AccountsEmail;
                        //    }

                        //    Contact.HotelCode = Hotel.sid;
                        //    DB.tbl_CommonHotelContacts.InsertOnSubmit(Contact);
                        //    DB.SubmitChanges();
                        //}

                        if (Contacts1.Count() > 0)                                  //update contact details in table 'Comm_HotelContacts'
                        {
                            Contacts1[0].Type = "HotelContact";
                            if (HotelPhone != "")
                            {
                                Contacts1[0].Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contacts1[0].MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contacts1[0].Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contacts1[0].ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contacts1[0].email = HotelEmail;
                            }

                            Contacts1[0].HotelCode = Hotel.sid;

                            DB.SubmitChanges();

                            //
                            Contacts1[1].Type = "ReservationContact";
                            if (ReservationPhone != "")
                            {
                                Contacts1[1].Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contacts1[1].MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contacts1[1].Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contacts1[1].ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contacts1[1].email = ReservationEmail;
                            }

                            Contacts1[1].HotelCode = Hotel.sid;
                            DB.SubmitChanges();

                            //
                            Contacts1[2].Type = "AccountsContact";
                            if (AccountsPhone != "")
                            {
                                Contacts1[2].Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contacts1[2].MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contacts1[2].Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contacts1[2].ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contacts1[2].email = AccountsEmail;
                            }

                            Contacts1[2].HotelCode = Hotel.sid;

                            DB.SubmitChanges();
                        }
                        else
                        {
                            Comm_HotelContact Contact1 = new Comm_HotelContact();
                            Comm_HotelContact Contact2 = new Comm_HotelContact();
                            Comm_HotelContact Contact3 = new Comm_HotelContact();

                            Contact1.Type = "HotelContact";
                            Contact1.SupplierCode = sUserID;
                            if (HotelPhone != "")
                            {
                                Contact1.Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contact1.MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contact1.Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contact1.ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contact1.email = HotelEmail;
                            }

                            Contact1.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact1);
                            DB.SubmitChanges();

                            //
                            Contact2.Type = "ReservationContact";
                            Contact2.SupplierCode = sUserID;
                            if (ReservationPhone != "")
                            {
                                Contact2.Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contact2.MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contact2.Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contact2.ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contact2.email = ReservationEmail;
                            }

                            Contact2.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact2);
                            DB.SubmitChanges();

                            //
                            Contact3.Type = "AccountsContact";
                            Contact3.SupplierCode = sUserID;
                            if (AccountsPhone != "")
                            {
                                Contact3.Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contact3.MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contact3.Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contact3.ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contact3.email = AccountsEmail;
                            }

                            Contact3.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact3);
                            DB.SubmitChanges();
                        }

                        //DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"Update\"}";
                    }
                }
                catch (Exception ex)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }



            }
            else
            {
                try
                {
                  using (var DB = new dbHotelhelperDataContext())
                    {
                        tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
                        tbl_CommonHotelContact Contacts = new tbl_CommonHotelContact();
                        Comm_HotelContact Contact1 = new Comm_HotelContact();
                        Comm_HotelContact Contact2 = new Comm_HotelContact();
                        Comm_HotelContact Contact3 = new Comm_HotelContact();

                        Hotel.HotelName = sHotelName;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.HotelDescription = sDescription;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelFacilities = sFacilities;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        Hotel.CheckinTime = sCheckinTime;
                        Hotel.CheckoutTime = sCheckoutTime;
                        Hotel.AdultMinAge = sAdultMinAge;
                        Hotel.ChildAgeFrom = sChildAgeFrom;
                        Hotel.ChildAgeTo = sChildAgeTo;
                        Hotel.TotalRooms = TotalRooms;
                        Hotel.TotalFloors = TotalFloors;
                        Hotel.BuildYear = BuildYear;
                        Hotel.RenovationYear = RenovationYear;
                        Hotel.DotwCode = sDotwCode;
                        Hotel.HotelBedsCode = sHotelBedsCode;
                        Hotel.MGHCode = sMGHCode;
                        Hotel.GRNCode = sGRNCode;
                        Hotel.ExpediaCode = sExpediaCode;
                        Hotel.GTACode = sGTACode;
                        Hotel.CreatedBy = sUserID;
                        Hotel.ParentID = sUserID;
                        Hotel.CreatedDate = DateTimes;
                        Hotel.HotelGroup = sHotelGroup;
                        Hotel.TripAdviserLink = sTripAdviserLink;
                        Hotel.PatesAllowed = sPatesAllowed;
                        Hotel.LiquorPolicy = sLiquorPolicy;
                        Hotel.Smooking = Smooking;
                        Hotel.Approved = Approved;

                        DB.tbl_CommonHotelMasters.InsertOnSubmit(Hotel);
                        DB.SubmitChanges();
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.Select(c => { c.HotelID = sHotelCode; return c; });
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        //1
                        //Contacts.HotelPhone = HotelPhone;
                        //Contacts.HotelMobile = HotelMobile;
                        //Contacts.HotelFax = HotelFax;
                        //Contacts.HotelContactPerson = HotelPerson;
                        //Contacts.HotelEmail = HotelEmail;

                        //Contacts.ReservationPhone = ReservationPhone;
                        //Contacts.ReservationMobile = ReservationMobile;
                        //Contacts.ReservationFax = ReservationFax;
                        //Contacts.ReservationContactPerson = ReservationPerson;
                        //Contacts.ReservationEmail = ReservationEmail;

                        //Contacts.AccountsPhone = AccountsPhone;
                        //Contacts.AccountsMobile = AccountsMobile;
                        //Contacts.AccountsFax = AccountsFax;
                        //Contacts.AccountsContactPerson = AccountsPerson;
                        //Contacts.AccountsEmail = AccountsEmail;
                        //Contacts.HotelCode = Hotel.sid;

                        //DB.tbl_CommonHotelContacts.InsertOnSubmit(Contacts);


                        //List<tbl_CommonLocation> Location = (from obj in DB.tbl_CommonLocations where obj.Lid == LocationID select obj).ToList();
                        //if (Location.Count != 0 )
                        //{
                        //    tbl_CommonLocation Locationn = new tbl_CommonLocation();

                        //    Locationn.LocationName = "";
                        //    Locationn.Country = sCountryCode;
                        //    Locationn.CityCode=sCityCode;

                        //    DB.tbl_CommonLocations.InsertOnSubmit(Locationn);
                        //}

                        //DB.SubmitChanges();
                        //2

                        Contact1.Type = "HotelContact";
                        Contact1.SupplierCode = sUserID;
                        if (HotelPhone != "")
                        {
                            Contact1.Phone = HotelPhone;
                        }
                        if (HotelMobile != "")
                        {
                            Contact1.MobileNo = HotelMobile;
                        }
                        if (HotelFax != "")
                        {
                            Contact1.Fax = HotelFax;
                        }
                        if (HotelPerson != "")
                        {
                            Contact1.ContactPerson = HotelPerson;
                        }
                        if (HotelEmail != "")
                        {
                            Contact1.email = HotelEmail;
                        }

                        Contact1.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact1);
                        DB.SubmitChanges();

                        //
                        Contact2.Type = "ReservationContact";
                        Contact2.SupplierCode = sUserID;
                        if (ReservationPhone != "")
                        {
                            Contact2.Phone = ReservationPhone;
                        }
                        if (ReservationMobile != "")
                        {
                            Contact2.MobileNo = ReservationMobile;
                        }
                        if (ReservationFax != "")
                        {
                            Contact2.Fax = ReservationFax;
                        }
                        if (ReservationPerson != "")
                        {
                            Contact2.ContactPerson = ReservationPerson;
                        }
                        if (ReservationEmail != "")
                        {
                            Contact2.email = ReservationEmail;
                        }

                        Contact2.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact2);
                        DB.SubmitChanges();

                        //
                        Contact3.Type = "AccountsContact";
                        Contact3.SupplierCode = sUserID;
                        if (AccountsPhone != "")
                        {
                            Contact3.Phone = AccountsPhone;
                        }
                        if (AccountsMobile != "")
                        {
                            Contact3.MobileNo = AccountsMobile;
                        }
                        if (AccountsFax != "")
                        {
                            Contact3.Fax = AccountsFax;
                        }
                        if (AccountsPerson != "")
                        {
                            Contact3.ContactPerson = AccountsPerson;
                        }
                        if (AccountsEmail != "")
                        {
                            Contact3.email = AccountsEmail;
                        }

                        Contact3.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact3);
                        DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelCode\":\"" + Contact3.HotelCode + "\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"Add\" }";
                        
                    }
                }
                catch (Exception ex)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;

        }



        [WebMethod(true)]
        public string AddHotelApproved(Int64 sHotelCode,Int64 sParent, string sHotelName, string sHotelAddress, string sDescription, string sRatings, string sFacilities, string sLangitude, string sLatitude, string sHotelBedsCode, string sDotwCode, string sMGHCode, string sGRNCode, string sExpediaCode, string sGTACode, string sCountryCode, string sCityCode, Int64 LocationID, string sZipCode, string sPatesAllowed, string sLiquorPolicy, string Smooking, string sTripAdviserLink, string sHotelGroup, int sAdultMinAge, int sChildAgeFrom, int sChildAgeTo, string sCheckinTime, string sCheckoutTime, int TotalRooms, int TotalFloors, string BuildYear, string RenovationYear, string HotelPhone, string HotelMobile, string HotelFax, string HotelPerson, string HotelEmail, string ReservationPhone, string ReservationMobile, string ReservationFax, string ReservationPerson, string ReservationEmail, string AccountsPhone, string AccountsMobile, string AccountsFax, string AccountsPerson, string AccountsEmail, List<Comm_TaxMapping> RatesDetails, List<Comm_TaxMapping> MealRates)
        {
            DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 sUserID = 0;
            bool Approved = true;
  
            retCode = HotelMappingManager.SearchHotelMapp(sHotelName, sParent, sHotelBedsCode, sDotwCode, sMGHCode, sGRNCode, sExpediaCode, sGTACode, out dtResult);



            string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                  using (var DB = new dbHotelhelperDataContext())
                    {
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        if (MealRates.Count != 0)
                        {
                            MealRates.ForEach(c => c.HotelID = sHotelCode);
                            TaxManager.SaveTaxDetails(MealRates);
                        }

                        tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMasters.Single(x => x.sid == sHotelCode);
                        List<tbl_CommonHotelContact> Contacts = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        List<Comm_HotelContact> Contacts1 = (from obj in DB.Comm_HotelContacts where obj.HotelCode == sHotelCode select obj).ToList();
                        if (sHotelName != "")
                        {
                            Hotel.HotelName = sHotelName;
                        }
                        if (sHotelAddress != "")
                        {
                            Hotel.HotelAddress = sHotelAddress;
                        }
                        if (sFacilities != "")
                        {
                            Hotel.HotelFacilities = sFacilities;
                        }
                        if (sCheckinTime != "")
                        {
                            Hotel.CheckinTime = sCheckinTime;
                        }
                        if (sCheckoutTime != "")
                        {
                            Hotel.CheckoutTime = sCheckoutTime;
                        }
                        if (sDescription != "")
                        {
                            Hotel.HotelDescription = sDescription;
                        }
                        if (sAdultMinAge != 0)
                        {
                            Hotel.AdultMinAge = sAdultMinAge;
                        }
                        if (sChildAgeFrom != 0)
                        {
                            Hotel.ChildAgeFrom = sChildAgeFrom;
                        }
                        if (sChildAgeTo != 0)
                        {
                            Hotel.ChildAgeTo = sChildAgeTo;
                        }
                        if (TotalRooms != 0)
                        {
                            Hotel.TotalRooms = TotalRooms;
                        }
                        if (TotalFloors != 0)
                        {
                            Hotel.TotalFloors = TotalFloors;
                        }
                        if (BuildYear != "")
                        {
                            Hotel.BuildYear = BuildYear;
                        }
                        if (RenovationYear != "")
                        {
                            Hotel.RenovationYear = RenovationYear;
                        }

                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                       // Hotel.ParentID = sUserID;
                        Hotel.Approved = Approved;
                        //Hotel.HotelImage = sMainImage;
                        //Hotel.SubImages = sSubImages;
                        if (sDotwCode != "")
                        {
                            Hotel.DotwCode = sDotwCode;
                        }
                        if (sHotelBedsCode != "")
                        {
                            Hotel.HotelBedsCode = sHotelBedsCode;
                        }
                        if (sMGHCode != "")
                        {
                            Hotel.MGHCode = sMGHCode;
                        }
                        if (sGRNCode != "")
                        {
                            Hotel.GRNCode = sGRNCode;
                        }
                        if (sExpediaCode != "")
                        {
                            Hotel.ExpediaCode = sExpediaCode;
                        }
                        if (sGTACode != "")
                        {
                            Hotel.GTACode = sGTACode;
                        }
                        if (Hotel.BaseSupplier != "")
                        {
                            Hotel.BaseSupplier = "ClickUrtrip";
                        }
                        Hotel.UpdatedBy = sUserID;
                        Hotel.UpdatedDate = DateTimes;
                        if (sHotelGroup != "")
                        {
                            Hotel.HotelGroup = sHotelGroup;
                        }
                        if (sPatesAllowed != "")
                        {
                            Hotel.PatesAllowed = sPatesAllowed;
                        }
                        if (sTripAdviserLink != "")
                        {
                            Hotel.TripAdviserLink = sTripAdviserLink;
                        }
                        if (sLiquorPolicy != "")
                        {
                            Hotel.LiquorPolicy = sLiquorPolicy;
                        }
                        if (Smooking != "")
                        {
                            Hotel.Smooking = Smooking;
                        }
                        //Hotel.HalalId = HalalId;
                        //Hotel.HotelDetails = HotelDetails;
                        // Hotel.BaseSupplier = BaseSupplier;
                        // Hotel.AmenitiesCode = AmenitiesCode;
                        //  Hotel.Smooking = Smooking;
                        // Hotel.BuildYear = BuildYear;
                        // Hotel.RenovationYear = RenovationYear;
                        // Hotel.TotalFloors = TotalFloors;
                        // Hotel.TotalRooms = TotalRooms;
                        // Hotel.HotelNote = HotelNote;
                        // Hotel.Smooking = Smooking;
                        //if (Contacts.Count() > 0)
                        //{
                        //    if (HotelPhone != "")
                        //    {
                        //        Contacts[0].HotelPhone = HotelPhone;
                        //    }
                        //    if (HotelMobile != "")
                        //    {
                        //        Contacts[0].HotelMobile = HotelMobile;
                        //    }
                        //    if (HotelFax != "")
                        //    {
                        //        Contacts[0].HotelFax = HotelFax;
                        //    }
                        //    if (HotelPerson != "")
                        //    {
                        //        Contacts[0].HotelContactPerson = HotelPerson;
                        //    }
                        //    if (HotelEmail != "")
                        //    {
                        //        Contacts[0].HotelEmail = HotelEmail;
                        //    }
                        //    //
                        //    if (ReservationPhone != "")
                        //    {
                        //        Contacts[0].ReservationPhone = ReservationPhone;
                        //    }
                        //    if (ReservationMobile != "")
                        //    {
                        //        Contacts[0].ReservationMobile = ReservationMobile;
                        //    }
                        //    if (ReservationFax != "")
                        //    {
                        //        Contacts[0].ReservationFax = ReservationFax;
                        //    }
                        //    if (ReservationPerson != "")
                        //    {
                        //        Contacts[0].ReservationContactPerson = ReservationPerson;
                        //    }
                        //    if (ReservationEmail != "")
                        //    {
                        //        Contacts[0].ReservationEmail = ReservationEmail;
                        //    }

                        //    //
                        //    if (AccountsPhone != "")
                        //    {
                        //        Contacts[0].AccountsPhone = AccountsPhone;
                        //    }
                        //    if (AccountsMobile != "")
                        //    {
                        //        Contacts[0].AccountsMobile = AccountsMobile;
                        //    }
                        //    if (AccountsFax != "")
                        //    {
                        //        Contacts[0].AccountsFax = AccountsFax;
                        //    }
                        //    if (AccountsPerson != "")
                        //    {
                        //        Contacts[0].AccountsContactPerson = AccountsPerson;
                        //    }
                        //    if (AccountsEmail != "")
                        //    {
                        //        Contacts[0].AccountsEmail = AccountsEmail;
                        //    }

                        //    Contacts[0].HotelCode = Hotel.sid;
                        //    DB.SubmitChanges();
                        //}
                        //else
                        //{
                        //    tbl_CommonHotelContact Contact = new tbl_CommonHotelContact();

                        //    if (HotelPhone != "")
                        //    {
                        //        Contact.HotelPhone = HotelPhone;
                        //    }
                        //    if (HotelMobile != "")
                        //    {
                        //        Contact.HotelMobile = HotelMobile;
                        //    }
                        //    if (HotelFax != "")
                        //    {
                        //        Contact.HotelFax = HotelFax;
                        //    }
                        //    if (HotelPerson != "")
                        //    {
                        //        Contact.HotelContactPerson = HotelPerson;
                        //    }
                        //    if (HotelEmail != "")
                        //    {
                        //        Contact.HotelEmail = HotelEmail;
                        //    }
                        //    //
                        //    if (ReservationPhone != "")
                        //    {
                        //        Contact.ReservationPhone = ReservationPhone;
                        //    }
                        //    if (ReservationMobile != "")
                        //    {
                        //        Contact.ReservationMobile = ReservationMobile;
                        //    }
                        //    if (ReservationFax != "")
                        //    {
                        //        Contact.ReservationFax = ReservationFax;
                        //    }
                        //    if (ReservationPerson != "")
                        //    {
                        //        Contact.ReservationContactPerson = ReservationPerson;
                        //    }
                        //    if (ReservationEmail != "")
                        //    {
                        //        Contact.ReservationEmail = ReservationEmail;
                        //    }

                        //    //
                        //    if (AccountsPhone != "")
                        //    {
                        //        Contact.AccountsPhone = AccountsPhone;
                        //    }
                        //    if (AccountsMobile != "")
                        //    {
                        //        Contact.AccountsMobile = AccountsMobile;
                        //    }
                        //    if (AccountsFax != "")
                        //    {
                        //        Contact.AccountsFax = AccountsFax;
                        //    }
                        //    if (AccountsPerson != "")
                        //    {
                        //        Contact.AccountsContactPerson = AccountsPerson;
                        //    }
                        //    if (AccountsEmail != "")
                        //    {
                        //        Contact.AccountsEmail = AccountsEmail;
                        //    }

                        //    Contact.HotelCode = Hotel.sid;
                        //    DB.tbl_CommonHotelContacts.InsertOnSubmit(Contact);
                        //    DB.SubmitChanges();
                        //}

                        if (Contacts1.Count() > 0)                                  //update contact details in table 'Comm_HotelContacts'
                        {
                            Contacts1[0].Type = "HotelContact";
                            if (HotelPhone != "")
                            {
                                Contacts1[0].Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contacts1[0].MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contacts1[0].Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contacts1[0].ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contacts1[0].email = HotelEmail;
                            }

                            Contacts1[0].HotelCode = Hotel.sid;

                            DB.SubmitChanges();

                            //
                            Contacts1[1].Type = "ReservationContact";
                            if (ReservationPhone != "")
                            {
                                Contacts1[1].Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contacts1[1].MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contacts1[1].Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contacts1[1].ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contacts1[1].email = ReservationEmail;
                            }

                            Contacts1[1].HotelCode = Hotel.sid;
                            DB.SubmitChanges();

                            //
                            Contacts1[2].Type = "AccountsContact";
                            if (AccountsPhone != "")
                            {
                                Contacts1[2].Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contacts1[2].MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contacts1[2].Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contacts1[2].ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contacts1[2].email = AccountsEmail;
                            }

                            Contacts1[2].HotelCode = Hotel.sid;

                            DB.SubmitChanges();
                        }
                        else
                        {
                            Comm_HotelContact Contact1 = new Comm_HotelContact();
                            Comm_HotelContact Contact2 = new Comm_HotelContact();
                            Comm_HotelContact Contact3 = new Comm_HotelContact();

                            Contact1.Type = "HotelContact";
                            Contact1.SupplierCode = sUserID;
                            if (HotelPhone != "")
                            {
                                Contact1.Phone = HotelPhone;
                            }
                            if (HotelMobile != "")
                            {
                                Contact1.MobileNo = HotelMobile;
                            }
                            if (HotelFax != "")
                            {
                                Contact1.Fax = HotelFax;
                            }
                            if (HotelPerson != "")
                            {
                                Contact1.ContactPerson = HotelPerson;
                            }
                            if (HotelEmail != "")
                            {
                                Contact1.email = HotelEmail;
                            }

                            Contact1.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact1);
                            DB.SubmitChanges();

                            //
                            Contact2.Type = "ReservationContact";
                            Contact2.SupplierCode = sUserID;
                            if (ReservationPhone != "")
                            {
                                Contact2.Phone = ReservationPhone;
                            }
                            if (ReservationMobile != "")
                            {
                                Contact2.MobileNo = ReservationMobile;
                            }
                            if (ReservationFax != "")
                            {
                                Contact2.Fax = ReservationFax;
                            }
                            if (ReservationPerson != "")
                            {
                                Contact2.ContactPerson = ReservationPerson;
                            }
                            if (ReservationEmail != "")
                            {
                                Contact2.email = ReservationEmail;
                            }

                            Contact2.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact2);
                            DB.SubmitChanges();

                            //
                            Contact3.Type = "AccountsContact";
                            Contact3.SupplierCode = sUserID;
                            if (AccountsPhone != "")
                            {
                                Contact3.Phone = AccountsPhone;
                            }
                            if (AccountsMobile != "")
                            {
                                Contact3.MobileNo = AccountsMobile;
                            }
                            if (AccountsFax != "")
                            {
                                Contact3.Fax = AccountsFax;
                            }
                            if (AccountsPerson != "")
                            {
                                Contact3.ContactPerson = AccountsPerson;
                            }
                            if (AccountsEmail != "")
                            {
                                Contact3.email = AccountsEmail;
                            }

                            Contact3.HotelCode = Hotel.sid;
                            DB.Comm_HotelContacts.InsertOnSubmit(Contact3);
                            DB.SubmitChanges();
                        }

                        //DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"UserType\":\"" + objUser.UserType + "\",\"value\":\"AddApproved\" }";
                    }
                }
                catch (Exception ex)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }



            }
            else
            {
                try
                {
                  using (var DB = new dbHotelhelperDataContext())
                    {
                        tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
                        tbl_CommonHotelContact Contacts = new tbl_CommonHotelContact();
                        Comm_HotelContact Contact1 = new Comm_HotelContact();
                        Comm_HotelContact Contact2 = new Comm_HotelContact();
                        Comm_HotelContact Contact3 = new Comm_HotelContact();

                        Hotel.HotelName = sHotelName;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.CountryId = sCountryCode;
                        Hotel.CityId = sCityCode;
                        Hotel.LocationId = LocationID;
                        Hotel.HotelAddress = sHotelAddress;
                        Hotel.HotelDescription = sDescription;
                        Hotel.HotelCategory = sRatings;
                        Hotel.HotelFacilities = sFacilities;
                        Hotel.HotelZipCode = sZipCode;
                        Hotel.HotelLangitude = sLangitude;
                        Hotel.HotelLatitude = sLatitude;
                        Hotel.CheckinTime = sCheckinTime;
                        Hotel.CheckoutTime = sCheckoutTime;
                        Hotel.AdultMinAge = sAdultMinAge;
                        Hotel.ChildAgeFrom = sChildAgeFrom;
                        Hotel.ChildAgeTo = sChildAgeTo;
                        Hotel.TotalRooms = TotalRooms;
                        Hotel.TotalFloors = TotalFloors;
                        Hotel.BuildYear = BuildYear;
                        Hotel.RenovationYear = RenovationYear;
                        Hotel.DotwCode = sDotwCode;
                        Hotel.HotelBedsCode = sHotelBedsCode;
                        Hotel.MGHCode = sMGHCode;
                        Hotel.GRNCode = sGRNCode;
                        Hotel.ExpediaCode = sExpediaCode;
                        Hotel.GTACode = sGTACode;
                       // Hotel.CreatedBy = sUserID;
                       // Hotel.ParentID = sUserID;
                        Hotel.CreatedDate = DateTimes;
                        Hotel.HotelGroup = sHotelGroup;
                        Hotel.TripAdviserLink = sTripAdviserLink;
                        Hotel.PatesAllowed = sPatesAllowed;
                        Hotel.LiquorPolicy = sLiquorPolicy;
                        Hotel.Smooking = Smooking;
                        Hotel.Approved = Approved;

                        DB.tbl_CommonHotelMasters.InsertOnSubmit(Hotel);
                        DB.SubmitChanges();
                        if (RatesDetails.Count != 0)
                        {
                            RatesDetails.Select(c => { c.HotelID = sHotelCode; return c; });
                            TaxManager.SaveTaxDetails(RatesDetails);
                        }
                        //Contacts.HotelPhone = HotelPhone;
                        //Contacts.HotelMobile = HotelMobile;
                        //Contacts.HotelFax = HotelFax;
                        //Contacts.HotelContactPerson = HotelPerson;
                        //Contacts.HotelEmail = HotelEmail;

                        //Contacts.ReservationPhone = ReservationPhone;
                        //Contacts.ReservationMobile = ReservationMobile;
                        //Contacts.ReservationFax = ReservationFax;
                        //Contacts.ReservationContactPerson = ReservationPerson;
                        //Contacts.ReservationEmail = ReservationEmail;

                        //Contacts.AccountsPhone = AccountsPhone;
                        //Contacts.AccountsMobile = AccountsMobile;
                        //Contacts.AccountsFax = AccountsFax;
                        //Contacts.AccountsContactPerson = AccountsPerson;
                        //Contacts.AccountsEmail = AccountsEmail;
                        //Contacts.HotelCode = Hotel.sid;

                        //DB.tbl_CommonHotelContacts.InsertOnSubmit(Contacts);

                        //DB.SubmitChanges();

                        //List<tbl_CommonLocation> Location = (from obj in DB.tbl_CommonLocations where obj.Lid == LocationID select obj).ToList();
                        //if (Location.Count != 0 )
                        //{
                        //    tbl_CommonLocation Locationn = new tbl_CommonLocation();

                        //    Locationn.LocationName = "";
                        //    Locationn.Country = sCountryCode;
                        //    Locationn.CityCode=sCityCode;

                        //    DB.tbl_CommonLocations.InsertOnSubmit(Locationn);
                        //}
                        

                        Contact1.Type = "HotelContact";
                        Contact1.SupplierCode = sUserID;
                        if (HotelPhone != "")
                        {
                            Contact1.Phone = HotelPhone;
                        }
                        if (HotelMobile != "")
                        {
                            Contact1.MobileNo = HotelMobile;
                        }
                        if (HotelFax != "")
                        {
                            Contact1.Fax = HotelFax;
                        }
                        if (HotelPerson != "")
                        {
                            Contact1.ContactPerson = HotelPerson;
                        }
                        if (HotelEmail != "")
                        {
                            Contact1.email = HotelEmail;
                        }

                        Contact1.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact1);
                        DB.SubmitChanges();

                        //
                        Contact2.Type = "ReservationContact";
                        Contact2.SupplierCode = sUserID;
                        if (ReservationPhone != "")
                        {
                            Contact2.Phone = ReservationPhone;
                        }
                        if (ReservationMobile != "")
                        {
                            Contact2.MobileNo = ReservationMobile;
                        }
                        if (ReservationFax != "")
                        {
                            Contact2.Fax = ReservationFax;
                        }
                        if (ReservationPerson != "")
                        {
                            Contact2.ContactPerson = ReservationPerson;
                        }
                        if (ReservationEmail != "")
                        {
                            Contact2.email = ReservationEmail;
                        }

                        Contact2.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact2);
                        DB.SubmitChanges();

                        //
                        Contact3.Type = "AccountsContact";
                        Contact3.SupplierCode = sUserID;
                        if (AccountsPhone != "")
                        {
                            Contact3.Phone = AccountsPhone;
                        }
                        if (AccountsMobile != "")
                        {
                            Contact3.MobileNo = AccountsMobile;
                        }
                        if (AccountsFax != "")
                        {
                            Contact3.Fax = AccountsFax;
                        }
                        if (AccountsPerson != "")
                        {
                            Contact3.ContactPerson = AccountsPerson;
                        }
                        if (AccountsEmail != "")
                        {
                            Contact3.email = AccountsEmail;
                        }

                        Contact3.HotelCode = Hotel.sid;
                        DB.Comm_HotelContacts.InsertOnSubmit(Contact3);
                        DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelCode\":\"" + Contact3.HotelCode + "\",\"UserType\":\"Admin\",\"value\":\"UpdateApproved\" }";
                    }
                }
                catch (Exception ex)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string AddLocation(string val, string sCountryCode, string sCityCode)
        {
            try
            {
              using (var DB = new dbHotelhelperDataContext())
                {
                    var arrLocation = DB.tbl_CommonLocations.Where(d => d.LocationName == val && d.City == sCityCode && d.Country == sCountryCode).FirstOrDefault();
                    if (arrLocation != null)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Lid\":\"" + arrLocation.Lid + "\"}";
                    }
                    else
                    {
                        tbl_CommonLocation Locationn = new tbl_CommonLocation();
                        Locationn.LocationName = val;
                        Locationn.Country = sCountryCode;
                        Locationn.City = sCityCode;
                        DB.tbl_CommonLocations.InsertOnSubmit(Locationn);
                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Lid\":\"" + Locationn.Lid + "\"}";
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion

        #region CountryCity
        [WebMethod(true)]
        public string GetCountry()
        {

            DBHelper.DBReturnCode retcode = HotelMappingManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelMappingManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityList\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string GetCityCountry(string Location)
        {
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
           try
            {
              using (var DB = new dbHotelhelperDataContext())
                {
                    var LocationName = (from obj in DB.tbl_CommonLocations where obj.LocationName == Location select obj).FirstOrDefault();
                    return objSerialize.Serialize(new { Session = 1, retCode = 1, LocationName = LocationName });
                }
            }
            catch
            {
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }
        #endregion


    }

    
}

