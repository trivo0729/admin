﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Net;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Reflection;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class ImageHandler : System.Web.Services.WebService
    {

        public ImageHandler()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        [WebMethod(EnableSession = true)]
        public string GetRandomNo()
        {
            string VisaCode = "CUT-" + GenerateRandomNumber();
            string logoFileName = VisaCode;
            Session["RandomNo"] = logoFileName;

            return logoFileName;

        }

        [WebMethod(EnableSession = true)]
        public string GetvideoRandomNo()
        {
            string VisaCode = "MFvid-" + GenerateRandomNumber();
            string logoFileName = VisaCode;
            Session["vidRandomNo"] = logoFileName;

            return logoFileName;

        }

        [WebMethod(EnableSession = true)]
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();


            return rndnumber;
            //string rndstring = "";
            //bool IsRndlength = false;
            //Random rnd = new Random((int)DateTime.Now.Ticks);
            //if (IsRndlength)
            //    length = rnd.Next(4, length);
            //for (int i = 0; i < length; i++)
            //{
            //    int toss = rnd.Next(1, 10);
            //    if (toss > 5)
            //        rndstring += (char)rnd.Next((int)'A', (int)'Z');
            //    else
            //        rndstring += rnd.Next(0, 9).ToString();
            //}
            //return rndstring;
        }
    }
}
