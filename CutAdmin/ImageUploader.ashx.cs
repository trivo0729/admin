﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for ImageUploader1
    /// </summary>
    public class ImageUploader1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            string id = System.Convert.ToString(context.Request.QueryString["id"]);
            string Title = System.Convert.ToString(context.Request.QueryString["Title"]);
            string ShortDescription = System.Convert.ToString(context.Request.QueryString["ShortDescription"]);
            string Description = System.Convert.ToString(context.Request.QueryString["Description"]);
            string Project = System.Convert.ToString(context.Request.QueryString["Project"]);
            string myFilePath="";
            if (context.Request.Files.Count > 0)
            {

                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                     myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    if (ext != ".pdf")
                    {
                        string FileName = myFilePath;
                        FileName = Path.Combine(context.Server.MapPath("BlogImages/"), myFilePath);
                        file.SaveAs(FileName);
                        //retCode = ActivityManager.UpdateImages(sFileName, sid);
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");

                    }
                }
                retCode = ActivityManager.AddBlogDetail(Title, ShortDescription, Description,Project, myFilePath,id);
            }
            else
            {
                //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}