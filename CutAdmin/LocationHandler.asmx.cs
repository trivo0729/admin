﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for LocationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        string json = "";
        private string escapejsondata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }


        [WebMethod(EnableSession = true)]
        public string SaveLocation(string Location, string Country, string City)
        {
            using (var DB = new dbHotelhelperDataContext())
            {
                var RoomList = (from obj in DB.tbl_CommonLocations where (obj.LocationName.Contains(Location) && obj.City == City) select obj).ToList();
                if (RoomList.Count == 0)
                {
                    try
                    {
                        tbl_CommonLocation Locatn = new tbl_CommonLocation();
                        Locatn.LocationName = Location;
                        Locatn.City = City;
                        Locatn.Country = Country;

                        DB.tbl_CommonLocations.InsertOnSubmit(Locatn);
                        DB.SubmitChanges();

                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Locat\":\"1\"}";

                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }


                return json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetLocation()
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var LocationList = ((from Loct in DB.tbl_CommonLocations select Loct).Distinct()).ToList();

                    if (LocationList.Count > 0)
                    {
                        json = jsSerializer.Serialize(LocationList);
                        json = json.TrimEnd(']');
                        json = json.TrimStart('[');
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"LocationList\":[" + json + "]}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string DeleteLocation(Int64 LocationId)
        {
            string jsonString = "";
            DBHelper.DBReturnCode retcode = ActivityManager.DeleteLocation(LocationId);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

    }
}
