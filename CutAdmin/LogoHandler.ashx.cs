﻿using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for LogoHandler
    /// </summary>
    public class LogoHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string ImgPath = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
            //  int TotalImgs = ImgPath.Count();
            if (context.Request.Files.Count > 0)
            {

                HttpFileCollection files = context.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = ImgPath.Split('.')[0];
                    string ext = "jpg";

                    if (ext != ".pdf")
                    {
                        string FileName = myFilePath;
                        //FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath + '.' + ext);
                        FileName = Path.Combine(context.Server.MapPath("AgencyLogo/"), objGlobalDefaults.Agentuniquecode + '.' + ext);
                        file.SaveAs(FileName);
                        //retCode = ActivityManager.UpdateImages(sFileName, sid);
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");

                    }
                }

            }
            else
            {
                //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                if (retCode == DataManager.DBReturnCode.SUCCESS)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            //if (ImgPath != "")
            //{
            //    //retCode = RoomManager.RoomImages(ImgPath, RoomId, HotelId);
            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}