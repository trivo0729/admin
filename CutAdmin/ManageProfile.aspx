﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ManageProfile.aspx.cs" Inherits="CutAdmin.ManageProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Additional styles -->
    <script src="Scripts/ManageProfile.js?v=1.1"></script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <link href="css/dynamicDiv.css" rel="stylesheet" />

    <script src="Scripts/moments.js"></script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
        ul li {
            list-style: none;
            cursor: pointer;
        }

        .deleteMe {
            float: right;
            background: #bb3131;
            color: white;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <div class="with-padding">
            <div class="large-box-shadow white-gradient with-border">

                <div class="blue-gradient panel-control">
                    <h3 style="color: white">Manage Profile</h3>

                </div>

                <div class="with-padding" style="height:475px">
                    <div class="columns">
                        <h5 style="padding-left: 1%">Personal Detail</h5>
                        <hr />
                        <div class="three-columns">
                            <small class="input-info">First Name:</small><br />
                            <input type="text" id="txtFirstName" class="input" style="width: 92%" autocomplete="off" />
                        </div>

                        <div class="three-columns">
                            <small class="input-info">Last Name:</small><br />
                            <input type="text" id="txtLastName" class="input" style="width: 92%" autocomplete="off" />
                        </div>

                        <div class="three-columns">
                            <small class="input-info">Designation:</small><br />
                            <input type="text" id="txtDesignation" class="input" style="width: 92%" autocomplete="off" />
                        </div>
                        <div class="three-columns">
                            <small class="input-info">Mobile:</small><br />
                            <input type="text" id="txtMobile" class="input" style="width: 92%" autocomplete="off" />
                        </div>

                    </div>


                    <div class="columns">
                        <h5 style="padding-left: 1%">Other Detail</h5>
                        <hr />
                        <div class="four-columns">
                            <small class="input-info">Company Name:</small><br />
                            <input type="text" id="txtCompanyName" class="input" style="width: 92%" autocomplete="off" />
                        </div>

                        <div class="four-columns">
                            <small class="input-info">Company mail:</small><br />
                            <input type="text" id="txtCompanymail" class="input" style="width: 92%" autocomplete="off" readonly />
                        </div>

                        <div class="four-columns">
                            <small class="input-info">Phone:</small><br />
                            <input type="text" id="txtPhone" class="input" style="width: 92%" autocomplete="off" />
                        </div>
                    </div>
                    <div class="columns">
                        <div class="twelve-columns full-width">
                            <small class="input-info">Address:</small><br />
                            <input type="text" id="txtAddress" class="input" style="width: 92%" autocomplete="off" />
                        </div>
                    </div>

                    <div class="columns">
                        <div class="four-columns" id="drp_Nationality">
                            <small class="input-info">Nationality</small>
                            <p class="button-height">
                                <select id="sel_Nationality" class="select full-width replacement select-styled-list tracked OfferType">
                                </select>
                        </div>
                        <div class="four-columns" id="drp_City">
                            <small class="input-info">City</small>
                            <p class="button-height">
                                <select id="sel_City" class="select full-width replacement select-styled-list tracked OfferType">
                                </select>
                        </div>

                        <div class="four-columns">
                            <small class="input-info">Pin Code:</small><br />
                            <input type="text" id="txtPinCode" class="input" style="width: 92%" autocomplete="off" />
                        </div>
                    </div>

                    <div class="columns">
                        <div class="four-columns">
                            <small class="input-info">Pan No:</small><br />
                            <input type="text" id="txtPanNo" class="input" style="width: 92%" autocomplete="off" />
                        </div>

                        <div class="four-columns">
                            <small class="input-info">Fax No:</small><br />
                            <input type="text" id="txtFaxNo" class="input" style="width: 92%" autocomplete="off" />
                        </div>
                        <div class="four-columns">
                            <small class="input-info">Logo:</small><br />
                            <input id="Logo" class="btn-search5 input" type="file" />
                            <br />
                        </div>
                    </div>
                    <div class="columns">
                        <a class="button blue-gradient" onclick="Save()" style="float: right;">Update</a>
                    </div>

                    </div>

                </div>

            </div>
      

    </section>
    <!-- End main content -->
    <script>
        // Create a "close" button and append it to each list item
        var myNodelist = document.getElementsByTagName("LI");
        var i;
        for (i = 0; i < myNodelist.length; i++) {
            var span = document.createElement("SPAN");
            var txt = document.createTextNode("\u00D7");
            span.className = "closeX";
            span.appendChild(txt);
            myNodelist[i].appendChild(span);
        }

        // Click on a close button to hide the current list item
        var close = document.getElementsByClassName("closeX");
        var i;
        for (i = 0; i < close.length; i++) {
            close[i].onclick = function () {
                var div = this.parentElement;
                div.style.display = "none";
            }
        }

        // Add a "checked" symbol when clicking on a list item
        var list = document.querySelector('ul');
        list.addEventListener('click', function (ev) {
            if (ev.target.tagName === 'LI') {
                ev.target.classList.toggle('checked');
            }
        }, false);

        // Create a new list item when clicking on the "Add" button
        function newElement() {
            var li = document.createElement("li");
            var inputValue = document.getElementById("myInput").value;
            var t = document.createTextNode(inputValue);
            $(li).addClass("cInclutions");
            li.appendChild(t);
            if (inputValue === '') {
                alert("You must write something!");
            } else {
                document.getElementById("myUL").appendChild(li);
            }
            document.getElementById("myInput").value = "";

            var span = document.createElement("SPAN");
            var txt = document.createTextNode("\u00D7");
            span.className = "closeX";
            span.appendChild(txt);
            li.appendChild(span);

            for (i = 0; i < close.length; i++) {
                close[i].onclick = function () {
                    var div = this.parentElement;
                    div.style.display = "none";
                }
            }
        }

        // Create a new list item when clicking on the "Add" button
        function newElement1() {
            var li = document.createElement("li");
            var inputValue = document.getElementById("myInput1").value;
            var t = document.createTextNode(inputValue);
            $(li).addClass("cExclutions");
            li.appendChild(t);
            if (inputValue === '') {
                alert("You must write something!");
            } else {
                document.getElementById("myUL1").appendChild(li);
            }
            document.getElementById("myInput1").value = "";

            var span = document.createElement("SPAN");
            var txt = document.createTextNode("\u00D7");
            span.className = "closeX";
            span.appendChild(txt);
            li.appendChild(span);

            for (i = 0; i < close.length; i++) {
                close[i].onclick = function () {
                    var div = this.parentElement;
                    div.style.display = "none";
                }
            }
        }


    </script>



    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
    <script src="js/developr.modal.js"></script>

</asp:Content>
