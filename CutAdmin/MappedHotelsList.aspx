﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MappedHotelsList.aspx.cs" Inherits="CutAdmin.MappedHotelsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/MappedHotelsList.js"></script>

    <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3>Hotels List</h3><hr />
		</hgroup>
        
		<div class="with-padding">
            <table class="table responsive-table"  id="tbl_HotelList">

				<thead>
					<tr>
						<th scope="col" width="35%">Hotels</th>
						<th scope="col" width="10%">Dotw Code</th>
						<th scope="col" width="10%">GRN Code</th>
                        <th scope="col" width="10%">HotelBeds Code</th>
                        <th scope="col" width="10%">Expedia Code</th>
                       
					</tr>
				</thead>
				

				<tbody>
				</tbody>

			</table>

		</div>

	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>
     <link rel="stylesheet" href="css/styles/modal.css?v=1">
    <script src="js/developr.modal.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

	
</asp:Content>
