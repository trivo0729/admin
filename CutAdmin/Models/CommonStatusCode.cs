﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class CommonStatusCode
    {
        public string Request { get; set; }
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }
        public AvailCommonRequest DisplayRequest { get; set; }
        public DateTime FDate { get; set; }
        public DateTime TDate { get; set; }
        public List<CommonLib.Request.Occupancy> Occupancy { get; set; }
    }
}