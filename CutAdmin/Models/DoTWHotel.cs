﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class DoTWHotel
    {
        public float MinPrice { get; set; }
        public int CountHotel { get; set; }
        public List<DOTWLib.Response.Facility> Facility { get; set; }
        public List<DOTWLib.Response.DOTWHotelDetails> HotelDetails { get; set; }
        public List<DOTWLib.Response.Location> Location { get; set; }
        public List<DOTWLib.Response.Category> Category { get; set; }
        public float MaxPrice { get; set; }
        public DoTWAvailRequest DisplayRequest { get; set; }
    }
}