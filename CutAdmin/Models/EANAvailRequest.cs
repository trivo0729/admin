﻿using HotelLib.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class EANAvailRequest
    {
        public string city { get; set; }
        public string countryCode { get; set; }
        public string arrivalDate { get; set; }
        public string departureDate { get; set; }
        public List<Room> room { get; set; }


    }
}