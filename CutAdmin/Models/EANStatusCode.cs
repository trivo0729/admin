﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class EANStatusCode
    {

        public int noRooms { get; set; }
        public Int64 noNights { get; set; }
        public string Request { get; set; }
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }
        //public EANAvailRequest AvailRequest { get; set; }
        public string FDate { get; set; }
        public string TDate { get; set; }
        public List<EANLib.Request.Room> Occupancy { get; set; }
    }
}