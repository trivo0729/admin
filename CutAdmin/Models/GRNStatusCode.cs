﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class GRNStatusCode
    {
        public int Night { get; set; }
        public int NoofRoom { get; set; }
        public string Request { get; set; }
        
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }
        public GRNAvailRequest DisplayRequest { get; set; }
        public DateTime FDate { get; set; }
        public DateTime TDate { get; set; }
        public List<GRNLib.Request.Availability.Occupancy> Occupancy { get; set; }
    }
}