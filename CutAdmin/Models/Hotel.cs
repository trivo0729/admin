﻿using HBTransLib.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class Hotel
    {
        public float MinPrice { get; set; }
        public int CountHotel { get; set; }
        public List<HotelLib.Response.Facility> Facility { get; set; }
        public List<HotelLib.Response.HotelDetail> HotelDetail { get; set; }
        public List<HotelLib.Response.Location> Location { get; set; }
        public List<HotelLib.Response.Category> Category { get; set; }
        public float MaxPrice { get; set; }
        public AvailRequest DisplayRequest { get; set; }
    }
}