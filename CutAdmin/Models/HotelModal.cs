﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Models
{
    public class HotelModal
    {

    }
    public class Reservation
    {
        public string Name { get; set; }
        public string Supplier { get; set; }
        public string Buyer { get; set; }
        public string HotelName { get; set; }
        public string Destination { get; set; }
        public int Count { get; set; }
    }
}