﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class Location
    {
        public string HotelCode { get; set; }
        public string Name { get; set; }
    }
}