﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class MGHHotel
    {
        public float MinPrice { get; set; }
        public int CountHotel { get; set; }
        public List<MGHLib.Response.Facilities> Facility { get; set; }
        public List<MGHLib.Response.MGHHotelDetails> HotelDetail { get; set; }
        public List<MGHLib.Response.Location> Location { get; set; }
        public List<MGHLib.Response.Category> Category { get; set; }
        public float MaxPrice { get; set; }
        public MGHAvailRequest DisplayRequest { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
    }
}