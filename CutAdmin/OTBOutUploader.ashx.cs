﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for OTBOutUploader
    /// </summary>
    public class OTBOutUploader : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    string RefrenceNo = System.Convert.ToString(context.Request.QueryString["Otb_id"]);
                    string FileNo = System.Convert.ToString(context.Request.QueryString["FileType"]);
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    if (ext == ".pdf" || ext == ".jpg" || ext == "jpeg")
                    {
                        string FileName = RefrenceNo + "_" + FileNo + ext;
                        FileName = Path.Combine(context.Server.MapPath("~/OTBDocument/"), FileName);
                        file.SaveAs(FileName);
                        string FN = FileName.Replace(myFilePath, "");
                        context.Session["OutboundFileName"] = ext;
                        string fileName = Path.GetFileName(file.FileName);
                        var attachment = file.InputStream;

                    }
                    else
                    {
                        context.Response.Write("Only PDF/jpg file is allowed");

                    }
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}