﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for OnlineHotelBookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OnlineHotelBookingHandler : System.Web.Services.WebService
    {
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
         static helperDataContext db = new helperDataContext();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string CheckAvailableCash()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retCode = HotelBookingManager.CheckAvailableCash(out dtResult);
            if (DBHelper.DBReturnCode.SUCCESS == retCode)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"AvailableCash\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
                return jsonString;
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }


        //public string GetBookingDetails(Int64 sid, DateTime dFrom, DateTime dTo)
        [WebMethod(EnableSession = true)]
        public string GetBookingDetails()
        {
            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            //DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatement(sid, sFrom, sTo, out dtResult);
            DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetails(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                SorteddtResult.Columns.Add("Currency", typeof(string));
                string CurrencyCode, CurrencyClass;
                for (int i = 0; i < SorteddtResult.Rows.Count; i++)
                {
                    CurrencyCode = SorteddtResult.Rows[i]["CurrencyCode"].ToString();
                    CurrencyClass = GetCurrencyClass(CurrencyCode);
                    SorteddtResult.Rows[i]["Currency"] = CurrencyClass;
                }
                jsonString = "";
                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                HttpContext.Current.Session["InvoiceList"] = SorteddtResult;
                SorteddtResult.Dispose();
            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelResDetails(string ReserId)
        {
            try
            {
                using (var DB= new helperDataContext())
                {
                    var List = (from obj in DB.tbl_HotelReservations
                                //join Room in DB.tbl_BookedRooms on obj.ReservationID equals Room.ReservationID

                                where obj.ReservationID == ReserId
                                select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
               
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelRoomDetails(string ReserId)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_BookedRooms
                                where obj.ReservationID == ReserId
                                select obj).Distinct().ToList();
                    var aarPaxes = (from obj in DB.tbl_BookedPassengers where obj.ReservationID == ReserId select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List, aarPaxes = aarPaxes });
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        public string GetCurrencyClass(string CurrencyCode)
        {
            string CurrencyClass = "";
            switch (CurrencyCode)
            {
                case "AED":
                    CurrencyClass = "Currency-AED";

                    break;
                case "SAR":
                    CurrencyClass = "Currency-SAR";
                    break;
                case "EUR":
                    CurrencyClass = "fa fa-eur;";
                    break;
                case "GBP":
                    CurrencyClass = "fa fa-gbp";
                    break;
                case "USD":
                    CurrencyClass = "fa fa-dollar";
                    break;
                case "INR":
                    CurrencyClass = "fa fa-inr";
                    break;
            }
            return CurrencyClass;
        }

        [WebMethod(EnableSession = true)]
        public string BookingListByDate(string Agent, string From, string To, string Type, string Passenger, string ResDt, string RefNo, string SuppRefNo, string Supplier, string HotelName, string Destination)
        {

            DateTime dFrom = DateTime.Now;
            DateTime dTo = DateTime.Now;
            DateTime BookingDt = DateTime.Now;
            if (From != "" || To != "" || ResDt != "")
            {
                dFrom = ConvertDateTime(From);
                dTo = ConvertDateTime(To);
                BookingDt = ConvertDateTime(ResDt);
            }

            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            Int64 Uid = objGlobalDefaults.sid;
            // DBHelper.DBReturnCode retcode = HotelBookingManager.BookingListByDate(sFrom, sTo, Type, Passenger, sBookingDt, RefNo, HotelName, Destination, out dtResult);

            DBHelper.DBReturnCode retcode = HotelBookingManager.BookingList(Uid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {

                #region DateRowsAdd
                dtResult.Columns.Add("ChkIn_Dt", typeof(DateTime));
                dtResult.Columns.Add("ChkOut_Dt", typeof(DateTime));
                dtResult.Columns.Add("ReservationDate_Dt", typeof(DateTime));
                string ChkIn_Dt = "";
                //DateTime ChkIn_Dt_Converted = DateTime.Now;
                string ChkIn_Dt_Converted;
                string CheckOut_Dt = "";
                //DateTime CheckOut_Dt_Converted = DateTime.Now;
                string CheckOut_Dt_Converted;
                string ReservationDate_Dt = "";
                DateTime ReservationDate_Dt_Converted = DateTime.Now;
                for (int i = 0; i < dtResult.Rows.Count; i++)
                {

                    ChkIn_Dt = (dtResult.Rows[i]["CheckIn"]).ToString();
                    ChkIn_Dt_Converted = ConvertDateTime(ChkIn_Dt).ToString();
                    dtResult.Rows[i]["ChkIn_Dt"] = ChkIn_Dt_Converted;

                    CheckOut_Dt = (dtResult.Rows[i]["CheckOut"]).ToString();
                    CheckOut_Dt_Converted = ConvertDateTime(CheckOut_Dt).ToString(); ;
                    dtResult.Rows[i]["ChkOut_Dt"] = CheckOut_Dt_Converted;

                    ReservationDate_Dt = (dtResult.Rows[i]["ReservationDate"]).ToString();
                    ReservationDate_Dt_Converted = ConvertDateTime(ReservationDate_Dt);
                    dtResult.Rows[i]["ReservationDate_Dt"] = ReservationDate_Dt_Converted;


                }

                #endregion

                DataRow[] rows = null;


                #region old Condition
                //if (From == "" && To == "" && Type == "All")
                //{
                //    rows = dtResult.Select("(bookingname like '%" + Passenger + "%') AND (HotelName like '%" + HotelName + "%') AND (InvoiceID like'%" + RefNo + "%') AND (City like'%" + Destination + "%')");
                //}

                //if (From == "" && To == "" && Type != "All")
                //{
                //    rows = dtResult.Select("(Status like '%" + Type + "%') AND (bookingname like '%" + Passenger + "%') AND (HotelName like '%" + HotelName + "%') AND (InvoiceID like'%" + RefNo + "%') AND (City like'%" + Destination + "%')");
                //}

                //if (From != "" && To != "" && Type != "All")
                //{
                //    rows = dtResult.Select("(ChkIn_Dt  >= #" + dFrom + "# AND ChkIn_Dt <= #" + dTo + "#) AND (Status like '%" + Type + "%') AND (bookingname like '%" + Passenger + "%') AND (HotelName like '%" + HotelName + "%') AND (InvoiceID like'%" + RefNo + "%') AND (City like'%" + Destination + "%')");
                //}

                //if (From != "" && To != "" && Type == "All")
                //{
                //    rows = dtResult.Select("(ChkIn_Dt  >= #" + dFrom + "# AND ChkIn_Dt <= #" + dTo + "#)  AND (bookingname like '%" + Passenger + "%') AND (HotelName like '%" + HotelName + "%') AND (InvoiceID like'%" + RefNo + "%') AND (City like'%" + Destination + "%')");
                //}
                #endregion



                #region Search Query
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                SorteddtResult.Columns.Add("Currency", typeof(string));
                string CurrencyCode, CurrencyClass;
                for (int i = 0; i < SorteddtResult.Rows.Count; i++)
                {
                    CurrencyCode = SorteddtResult.Rows[i]["CurrencyCode"].ToString();
                    CurrencyClass = GetCurrencyClass(CurrencyCode);
                    SorteddtResult.Rows[i]["Currency"] = CurrencyClass;
                }
                //if (rows.Length == 0)
                //{

                //    SorteddtResult = null;

                //    SorteddtResult = dtResult.Clone();
                //}
                //else
                //{
                //    SorteddtResult = rows.CopyToDataTable();
                //}
                DataRow[] Bookingrows = null;
                if (Agent != "")
                {

                    Bookingrows = SorteddtResult.Select("(AgencyName like '%" + Agent + "%')");

                    if (Bookingrows.Length == 0)
                    {

                        SorteddtResult = null;

                        SorteddtResult = dtResult.Clone();
                    }
                    else
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }

                }
                if (Type != "All")
                {
                    Bookingrows = SorteddtResult.Select("(Status like '%" + Type + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                }
                if (RefNo != "")
                {
                    Bookingrows = SorteddtResult.Select("(InvoiceId like '%" + RefNo + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                if (From != "")
                {
                    From = dFrom.ToString();
                    Bookingrows = SorteddtResult.Select("(ChkIn_Dt = '" + From + "')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                //if (To != "")
                //{
                //    To = dTo.ToString();
                //    Bookingrows = SorteddtResult.Select("(ChkOut_Dt = '" + To + "')");
                //    if (Bookingrows.Length != 0)
                //    {
                //        SorteddtResult = Bookingrows.CopyToDataTable();
                //    }
                //    else
                //    {
                //        SorteddtResult = null;
                //        SorteddtResult = dtResult.Clone();
                //    }
                //}
                if (To != "")
                {
                    To = dTo.ToString();
                    Bookingrows = SorteddtResult.Select("(ChkOut_Dt = '" + To + "')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                if (ResDt != "")
                {
                    ResDt = BookingDt.ToString();
                    Bookingrows = SorteddtResult.Select("(ReservationDate_Dt = '" + ResDt + "')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                if (HotelName != "")
                {
                    Bookingrows = SorteddtResult.Select("(HotelName like '%" + HotelName + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                if (Destination != "")
                {
                    Bookingrows = SorteddtResult.Select("(City like'%" + Destination + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                if (Passenger != "")
                {
                    Bookingrows = SorteddtResult.Select("(bookingname like'%" + Passenger + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }


                if (SuppRefNo != "")
                {
                    Bookingrows = SorteddtResult.Select("(ReservationID like'%" + SuppRefNo + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }

                if (Supplier != "All")
                {
                    Bookingrows = SorteddtResult.Select("(Source like'%" + Supplier + "%')");
                    if (Bookingrows.Length != 0)
                    {
                        SorteddtResult = Bookingrows.CopyToDataTable();
                    }
                    else
                    {
                        SorteddtResult = null;
                        SorteddtResult = dtResult.Clone();
                    }
                }
                #endregion


                //SorteddtResult = rows.CopyToDataTable();
                jsonString = "";
                HttpContext.Current.Session["InvoiceList"] = SorteddtResult;
                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetIncompleteBookingDetail(string InvoiceID)
        {
            string jsonString = "";
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            try
            {
                helperDataContext DB = new helperDataContext();
                var List = (from Obj in DB.tbl_RoomLogs where Obj.ReservationId == InvoiceID select Obj).ToList();
                if (List.Count > 0)
                {
                    var MyAgent = (from Obj in DB.tbl_AdminLogins where Obj.sid == List[0].uid select Obj).ToList();
                    string AgentName = "";
                    if (MyAgent.Count > 0)
                        AgentName = MyAgent[0].ContactPerson;
                    jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, List = List, AgentName = AgentName });
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"List\":\"" + List + "\",\"AgentName\":\"" + AgentName + "\"}";
                }
                else
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            catch (Exception ex)
            {
                jsonString = "";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetBookingDetailsByAgent(Int64 Uid)
        {

            string jsonString = "";
            DataTable dtResult;

            DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsByAgent(Uid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();

                jsonString = "";
                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();
            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingDetailsByDate(string dFrom, string dTo)
        {

            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsByDate(dFrom, dTo, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                jsonString = "";



                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetailsByDate\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetBookingDetailsAgentByDate(Int64 Uid, string dFrom, string dTo)
        {

            string jsonString = "";
            DataTable dtResult;

            DBHelper.DBReturnCode retcode = HotelBookingManager.GetBookingDetailsAgentByDate(Uid, dFrom, dTo, out dtResult);


            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                jsonString = "";



                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetailsAgentByDate\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }



        [WebMethod(EnableSession = true)]
        public string CancelBooking(Int64 sid, string Remark)
        {
            DBHelper.DBReturnCode retCode = HotelBookingManager.CancelBooking(sid, Remark);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetPolicyandHotel(string ReservationID)
        {
            DataTable dtResult;
            string jsonString = "";
            DBHelper.DBReturnCode retCode = HotelBookingManager.GetPolicyandHotel(ReservationID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingVoucher()
        {
            DataTable dtResult;
            string jsonString = "";
            DBHelper.DBReturnCode retCode = HotelBookingManager.GetBookingVoucher(out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string ConfirmationDetails(string ReservationID)
        {

            string jsonString = "";
            DataTable dtResult;

            //DBHelper.DBReturnCode retcode = AgencyStatementManager.GetAgencyStatement(sid, sFrom, sTo, out dtResult);
            DBHelper.DBReturnCode retcode = HotelBookingManager.ConfirmationDetails(ReservationID, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateHoldDate(string ReservationId, string OldDate, string Date, string DeadLineDate)
        {
            try
            {
                var Time = OldDate.Split(' ');
                var Datte = Date + ' ' + Time[1] + ":00";

                helperDataContext db = new helperDataContext();
                DateTime DeadLineDatee = DateTime.ParseExact(DeadLineDate, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime Datee = DateTime.ParseExact(Datte, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                if (DeadLineDatee.AddMinutes(-2) > Datee)
                {

                    tbl_HotelReservation Data = db.tbl_HotelReservations.Single(x => x.ReservationID == ReservationId);
                    Data.HoldTime = Datee.ToString();
                    db.SubmitChanges();
                    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else
                {
                    return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                }


            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {
                 helperDataContext db = new helperDataContext();
                GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uId = objGolobalDefaults.sid;

                tbl_HotelReconfirmationDetail Confirm = new tbl_HotelReconfirmationDetail();
                Confirm.UserID = uId;
                Confirm.ReservationId = ReservationId;
                Confirm.ReconfirmDate = ConfirmDate;
                Confirm.Staff_Name = StaffName;
                Confirm.ReconfirmThrough = ReconfirmThrough;
                Confirm.HotelConfirmationNo = HotelConfirmationNo;
                Confirm.Comment = Comment;
                db.tbl_HotelReconfirmationDetails.InsertOnSubmit(Confirm);
                db.SubmitChanges();

                tbl_HotelReservation Bit = db.tbl_HotelReservations.Single(x => x.ReservationID == ReservationId);
                Bit.IsConfirm = true;
                db.SubmitChanges();

                ReconfirmationMail(Confirm.sid, HotelName, ReservationId, StaffName, Comment);


                return "{\"Session\":\"1\",\"retCode\":\"1\"}";

            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        public static bool ReconfirmationMail(Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                helperDataContext db = new helperDataContext();
                var sReservation = (from obj in db.tbl_HotelReservations
                                    from objAgent in db.tbl_AdminLogins
                                    where obj.ReservationID == ReservationId
                                    select new
                                    {
                                        customer_Email = objAgent.uid,
                                        customer_name = obj.AgencyName,
                                        VoucherNo = obj.VoucherID,

                                    }).FirstOrDefault();

                var sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" select obj).FirstOrDefault();
                string BCcTeamMails = sMail.BCcMail;
                string CcTeamMail = sMail.CcMail;


                StringBuilder sb = new StringBuilder();

                sb.Append("<html>");
                sb.Append("<head>");
                sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                sb.Append("<style>");
                sb.Append("<!--");
                sb.Append(" /* Font Definitions */");
                sb.Append(" @font-face");
                sb.Append("	{font-family:\"Cambria Math\";");
                sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                sb.Append("@font-face");
                sb.Append("	{font-family:Calibri;");
                sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                sb.Append(" /* Style Definitions */");
                sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                sb.Append("	{margin-top:0cm;");
                sb.Append("	margin-right:0cm;");
                sb.Append("	margin-bottom:10.0pt;");
                sb.Append("	margin-left:0cm;");
                sb.Append("	line-height:115%;");
                sb.Append("	font-size:11.0pt;");
                sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                sb.Append("a:link, span.MsoHyperlink");
                sb.Append("	{color:blue;");
                sb.Append("	text-decoration:underline;}");
                sb.Append("a:visited, span.MsoHyperlinkFollowed");
                sb.Append("	{color:purple;");
                sb.Append("	text-decoration:underline;}");
                sb.Append(".MsoPapDefault");
                sb.Append("	{margin-bottom:10.0pt;");
                sb.Append("	line-height:115%;}");
                sb.Append("@page Section1");
                sb.Append("	{size:595.3pt 841.9pt;");
                sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                sb.Append("div.Section1");
                sb.Append("	{page:Section1;}");
                sb.Append("-->");
                sb.Append("</style>");
                sb.Append("</head>");
                sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                sb.Append("<div class=Section1>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                sb.Append("" + sReservation.customer_name + ",</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                sb.Append(" style='border-collapse:collapse'>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                sb.Append("  No.</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Sid + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                sb.Append("  Name</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                sb.Append("  ID</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                sb.Append("  by</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append(" <tr>");
                sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                sb.Append("  / Note</span></p>");
                sb.Append("  </td>");
                sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                sb.Append("  </td>");
                sb.Append(" </tr>");
                sb.Append("</table>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                sb.Append("<p class=MsoNormal>&nbsp;</p>");
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");


                string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";


                // SendMail(sReservation.customer_Email, Title, sb.ToString(), "", "online@clickurtrip.com");
                //EmailManager.SendMail(sReservation.customer_Email, Title, sb.ToString(), "", CcTeamMail);
                return true;

            }
            catch
            {
                return false;
            }
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                    //NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }

        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {
            int effected = 0;
            SqlParameter[] sqlParams = new SqlParameter[5];
            sqlParams[0] = new SqlParameter("@sTo", sTo);
            sqlParams[1] = new SqlParameter("@sSubject", title);
            sqlParams[2] = new SqlParameter("@VarBody", sMail);
            sqlParams[3] = new SqlParameter("@Path", DocPath);
            sqlParams[4] = new SqlParameter("@Cc", Cc);
            DBHelper.DBReturnCode retcode = DBHelper.ExecuteNonQuery("Proc_InvoiceAttachmentMail", out effected, sqlParams);
        }
    }
}
