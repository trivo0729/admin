﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="OtbCharges.aspx.cs" Inherits="CutAdmin.OtbCharges" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/OtbCharges.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>OtbCharges</h1>
            <div class="Groupmark" id="DivGroupmark" style="float: right">
                     
            <button type="button" id="btn_RegiterAgent" class="button anthracite-gradient Updategroup  " onclick="MailDilog();" title="Update Mail">Update Mail</button>

        </div>
        </hgroup>

        <div class="with-padding">

           <%-- <h4>Otb Service</h4>--%>
            <hr />

            <div class="columns">
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>AirLines<span style="color:red;">*</span></label>
                    <div class="full-width button-height Groupmark">
                        <select id="Sel_AirLineIn" class="select" onchange="GetAirportsList()">
                            <option value="-" selected="selected">-Select AirLine-</option>
                                <option value="G9">Air Arabia</option>
                                <option value="AI">Air India</option>
                                <option value="Ix">Air India Express</option>
                                <option value="9w" id="OptJet">Jet Airways Airlines</option>
                                <option value="6E">Indigo Airlines </option>
                                <option value="Wy">Oman Airways</option>
                                <option value="SG">Spicejet Airlines</option>
                                <option value="EK" id="OptEmirates">Emirates Airlines</option>
                                <option value="EY">Etihad Airways</option>
                                <option value="FZ">Fly Dubai</option>
                        </select>
                    </div>
                </div>
                <div class="four-columns four-columns-tablet twelve-columns-mobile Groupmark">
                    <label>From<span style="color:red;">*</span></label>
                    <div class="full-width button-height">
                        <select id="Sel_ArivalFrom" class="select" onchange="GetOtbCharges()">
                            <option value="-" selected="selected">--Select AirLine First--</option>
                        </select>
                    </div>
                </div>
            </div>

            <h4>Otb Charges</h4>
            <hr />

            <div class="columns">
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Currency<span style="color:red;">*</span></label>
                    <div class="full-width button-height Groupmark" id="DivCurrency">
                        <select id="selCurrencyType" onchange="Notification()" class="select">
                            <option value="-" selected="selected">--Salect OTB Currency--</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                            <option value="SAR">SAR</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="USD">USD</option>
                        </select>
                    </div>
                </div>

                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Otb Fee<span style="color:red;">*</span></label>
                    <div class="input full-width"><input type="text" class="input-unstyled full-width" id="txtOtbFee" onkeyup="ADD()" placeholder="0.00"></div>
                </div>
                <div class="four-columns four-columns-tablet twelve-columns-mobile">
                    <label>Urgent Fee</label>
                    <div class="input full-width"><input type="text" class="input-unstyled full-width" id="txtOtherFee" onkeyup="ADD()" placeholder="0.00"></div>
                </div>
            </div>

            <p class="text-alignright">
                <button type="button" id="btnAddStaff" onclick="UpdateCharges();" title="Submit Details" class="button anthracite-gradient UpdateMarkup">Update Charges</button>
                 &nbsp; <button type="reset" value="Reset" onclick="ResetControls()" class="button anthracite-gradient">Reset</button>

            </p>
        </div>

    </section>
    <!-- End main content -->
</asp:Content>
