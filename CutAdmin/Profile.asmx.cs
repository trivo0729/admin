﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for Profile
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Profile : System.Web.Services.WebService
    {

        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
         static helperDataContext db = new helperDataContext();
        DBHelper.DBReturnCode retCode;

        [WebMethod(EnableSession = true)]
        public string Save(string FistNAme, string LastNAme, string Designation, string Mobile, string CompanyName, string Companymail, string Phone, 
            string Address, string Country, string City, string PinCode, string PanNo, string FaxNo)
        {
        
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    tbl_AdminLogin add = DB.tbl_AdminLogins.Single(x => x.uid == objGlobalDefaults.uid);
                    add.AgencyName = CompanyName;
                    add.ContactPerson = FistNAme;
                    add.Last_Name = LastNAme;
                    add.Designation = Designation;
                    add.PANNo = PanNo;
                    DB.SubmitChanges();

                    tbl_Contact cont = DB.tbl_Contacts.Single(x => x.ContactID == objGlobalDefaults.ContactID);
                    cont.Address = Address;
                    cont.email = Companymail;
                    cont.Fax = FaxNo;
                    cont.Mobile = Mobile;
                    cont.phone = Phone;
                    cont.PinCode = PinCode;
                    cont.Code = City;

                    DB.SubmitChanges();

                    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                
                 return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }

        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }

         [WebMethod(EnableSession = true)]
        public string GetUserDetail()
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    //var ParentID = 

                    var UserDetail = (from obj in DB.tbl_AdminLogins
                                      join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                      join objh in DB.tbl_HCities on objc.Code equals objh.Code
                                      where obj.sid == objGlobalDefaults.sid
                                      select new
                                      {
                                          obj.ContactPerson,
                                          obj.Last_Name,
                                          obj.Designation,
                                          obj.AgencyName,
                                          obj.AgencyType,
                                          obj.PANNo,
                                          obj.Agentuniquecode,
                                          obj.ContactID,
                                          obj.uid,
                                          objc.Address,
                                          objc.email,
                                          objc.Mobile,
                                          objc.phone,
                                          objc.PinCode,
                                          objc.Fax,
                                          objc.sCountry,
                                          objc.Website,
                                          objc.StateID,
                                          objh.Countryname,
                                          objh.Description
                                      }).FirstOrDefault();
                    if (UserDetail != null)
                      return jsSerializer.Serialize(new { retCode = 1, Session = 1, UserDetail = UserDetail });
                    else
                        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
                }
            }
            catch (Exception)
            {

                return jsSerializer.Serialize(new { retCode = 0, Session = 1});
            }
        }
    }
}
