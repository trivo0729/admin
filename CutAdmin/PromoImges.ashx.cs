﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for PromoImges
    /// </summary>
    public class PromoImges : IHttpHandler
    {
        string json = "";
        public void ProcessRequest(HttpContext context)
        {
            helperDataContext db = new helperDataContext();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            jsSerializer.MaxJsonLength = Int32.MaxValue;
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

            Int64 sid = System.Convert.ToInt64(context.Request.QueryString["sid"]);
            string sFileName = System.Convert.ToString(context.Request.QueryString["sFileName"]);
            string facility = System.Convert.ToString(context.Request.QueryString["facility"]);
            string service = System.Convert.ToString(context.Request.QueryString["service"]);
            string start = System.Convert.ToString(context.Request.QueryString["start"]);
            string details = System.Convert.ToString(context.Request.QueryString["details"]);
            string Notes = System.Convert.ToString(context.Request.QueryString["Notes"]);
            if (context.Request.Files.Count > 0)
            {
                int rowsAffected = 0;
                bool ShowFlag = true;
                var VirtualPath = "";
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    if (ext != ".pdf")
                    {
                        string FileName = file.FileName;
                        FileName = Path.Combine(context.Server.MapPath("~/PromoImages/"), FileName);
                        file.SaveAs(FileName);
                        VirtualPath = "~/PromoImages/" + file.FileName;
                        if (sid != 0)
                        {
                            var data = (from obj in db.tbl_PromoImages where obj.sid == sid select obj).First();
                            data.ImageName = sFileName;
                            DateTime now = DateTime.Now;
                            data.UpdatedDate = now;
                            data.ShowFlag = ShowFlag;
                            data.facility = facility;
                            data.service = service;
                            data.start = start;
                            data.details = details;
                            data.Notes = Notes;
                            data.Path = VirtualPath;
                            db.SubmitChanges();
                            context.Response.Write("1");

                        }
                        else
                        {
                            CutAdmin.dbml.tbl_PromoImage Add = new CutAdmin.dbml.tbl_PromoImage();

                            Add.ImageName = sFileName;
                            Add.UpdatedDate = DateTime.Now;
                            Add.ShowFlag = ShowFlag;
                            Add.facility = facility;
                            Add.service = service;
                            Add.start = start;
                            Add.details = details;
                            Add.Notes = Notes;
                            Add.Path = VirtualPath;
                            db.tbl_PromoImages.InsertOnSubmit(Add);
                            db.SubmitChanges();

                            var data2 = (from obj in db.tbl_PromoImages where obj.sid != Add.sid select obj).ToList();
                            for (i = 0; i < data2.Count; i++)
                            {
                                data2[i].ShowFlag = false;
                                db.SubmitChanges();
                            }
                            context.Response.Write("1");
                        }
                       
                    }
                        
                    else
                    {
                        context.Response.Write("0");

                    }
                    //return json;
                }
              
            }
                
            else
            {

            }
            //return json;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}