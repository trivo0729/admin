﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="QuotationDetails.aspx.cs" Inherits="CutAdmin.QuotationDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/QuotationDetails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Quotation Details</h1>
            <h2><a href="AddQuotation.aspx" class="addnew"><i class="fa fa-user-plus"></i></a><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Company Name: </label>
                            <div class="input full-width">
                                <input type="text" id="txt_Name" class="input-unstyled full-width"><input type="hidden" id="hdnDCode" />
                            </div>
                        </div>
                        <div class="five-columns twelve-columns-mobile formBTn">
                            </br>
                            <button type="button" class="button anthracite-gradient" onclick="Search();">Search</button>
                            <button type="button" onclick="reset()" class="button anthracite-gradient">Reset</button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_QuotationDetails">

                    <thead>
                        <tr>
                            <th scope="col" width="5%">S.N</th>
                            <th scope="col" class="align-center">Quotation No </th>
                            <th scope="col" class="align-center">Quotation Date </th>
                            <th scope="col" width="10%" class="align-center">Company Name</th>
                            <th scope="col" width="10%" class="align-center">Leading passenger</th>
                            <th scope="col" width="10%" class="align-center">No of Passenger</th>
                            <th scope="col" width="10%" class="align-center">View Quotation</th>
                            <th scope="col" width="10%" class="align-center">Update</th>
                            <th scope="col" width="10%" class="align-center">Delete</th>
                        </tr>
                    </thead>


                    <tbody>
                        <%--<tr>
                            <td>1</td>
                            <td>Sumit Nanda</td>
                            <td><a style="cursor: pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal()" title="Click to edit Password">sumit@clickurtrip.com</a></td>
                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#TerritoryModel" onclick="TerritoryModel()" title="State">State</a></td>
                            <td class="align-center"><a href="AddSalesPerson.html" class="button" title="Update"><span class="icon-publish "></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="Trash" onclick="deletTrush();"><span class="icon-trash"></span></a></span></td>
                        </tr>--%>

                    </tbody>

                </table>
            </div>
        </div>

    </section>
    <!-- End main content -->
    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Favicon count
        Tinycon.setBubble(2);

        // If the browser support the Notification API, ask user for permission (with a little delay)
        if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet()) {
            setTimeout(function () {
                notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function () {
                    // Confirmation message
                    if (notify.hasNotificationPermission()) {
                        notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
                            icon: 'img/demo/icon.png',
                            system: true
                        });
                    }
                    else {
                        notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
                            icon: 'img/demo/icon.png'
                        });
                    }
                });

            }, 2000);
        }

        /*
		 * Handling of 'other actions' menu
		 */

        var otherActions = $('#otherActions'),
			current = false;

        // Other actions
        $('.list .button-group a:nth-child(2)').menuTooltip('Loading...', {

            classes: ['with-mid-padding'],
            ajax: 'ajax-demo/tooltip-content.html',

            onShow: function (target) {
                // Remove auto-hide class
                target.parent().removeClass('show-on-parent-hover');
            },

            onRemove: function (target) {
                // Restore auto-hide class
                target.parent().addClass('show-on-parent-hover');
            }
        });

        // Delete button
        $('.list .button-group a:last-child').data('confirm-options', {

            onShow: function () {
                // Remove auto-hide class
                $(this).parent().removeClass('show-on-parent-hover');
            },

            onConfirm: function () {
                // Remove element
                $(this).closest('li').fadeAndRemove();

                // Prevent default link behavior
                return false;
            },

            onRemove: function () {
                // Restore auto-hide class
                $(this).parent().addClass('show-on-parent-hover');
            }

        });

        // Demo modal
        function TerritoryModel() {
            $.modal({
                content: '<div class="respTable"><table class="table responsive-table" id="sorting-advanced">' +
'<tr><th class="align-center bold">S.N </th><th class="align-center bold">Name </th></tr>' +
'<tr><td>1</td><td>Gujarat</td></tr>' +
'<tr><td>2</td><td>Chandigarh</td></tr>' +
'<tr><td>3</td><td>Maharashtra</td></tr>' +
'</table></div>',
                title: 'State',
                width: 450,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };
        // Password modal
        function PasswordModal() {
            $.modal({
                content: '<div class="modal-body">' +
'<div class="scrollingDiv">' +
'<div class="columns">' +
'<div class="four-columns bold">User ID</div>' +
'<div class="eight-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="" class="input-unstyled full-width" type="text"></div></div></div> ' +
'<div class="columns">' +
'<div class="four-columns bold">Password</div>' +
'<div class="eight-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password value=" class="input-unstyled full-width" type="text"></div></div> ' +
'</div>' +
'<div class="columns">' +
'<div class="four-columns bold">&nbsp;</div>' +
'<div class="eight-columns bold"><button type="button" class="button compact marright anthracite-gradient">Email</button>' +
'<button type="button" class="button compact Changepass anthracite-gradient">Change&nbsp;Password</button></div>' +
'</div></div></div>',

                title: 'Edit Password',
                width: 350,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };

        // deletTrush alert
        function deletTrush() {
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to Delete this package</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="submit" class="button anthracite-gradient">OK</button></p>',


                width: 250,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        };


    </script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
