﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for RoomHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoomHandlerO : System.Web.Services.WebService
    {
        //string json = ""; string Texts = "";
        //string jsonString = "";
        //DataTable dtResult;
        //JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        //DBHelper.DBReturnCode retCode;

        //[WebMethod(EnableSession = true)]
        //public string GetRooms(Int64 sHotelId)
        //{
        //    string jsonString = "";
        //    JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //     static helperDataContext db = new helperDataContext();

        //    if (sHotelId > 0)
        //    {
        //        List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
        //        //objHotelList = JsonStringManager.ConvertDataTable(dtResult);          
        //        var RoomList = (from obj in DB.tbl_commonRoomDetails where obj.HotelId == sHotelId select obj).ToList();
        //        var RoomTypeList = (from obj in DB.tbl_commonRoomTypes select obj).ToList();
        //        var RoomAmenityList = (from obj in DB.tbl_commonAmunities select obj).ToList();

        //        return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomAmenityList = RoomAmenityList, RoomTypeList = RoomTypeList });

        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        //[WebMethod(EnableSession = true)]
        //public string getRoomwithId(Int64 RoomId, Int64 HotelCode)
        //{
        //    string jsonString = "";
        //    JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //     static helperDataContext db = new helperDataContext();

        //    if (RoomId > 0)
        //    {
        //        var RoomList = (from Room in DB.tbl_commonRoomDetails where Room.RoomId == RoomId select Room).FirstOrDefault();
        //        var RoomType = (from RType in DB.tbl_commonRoomTypes where RType.RoomTypeID == RoomList.RoomTypeId select RType).FirstOrDefault();
        //        List<string> AmenityList = new List<string>();
        //        foreach (string AmenityId in RoomList.RoomAmenitiesId.Split(','))
        //        {
        //            var Amenity = (from obj in DB.tbl_commonAmunities where obj.RoomAmunityID == Convert.ToInt64(AmenityId) select obj.RoomAmunityName).FirstOrDefault();
        //            AmenityList.Add(Amenity);
        //        }


        //        return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomType = RoomType, AmenityList = AmenityList });

        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}


        //[WebMethod(EnableSession = true)]
        //public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, int MinStay, int MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday)
        //{
        //    DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    Int64 sUserID = objUser.sid;
        //    string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
        //    //RoomTypeID, CheckinR/CheckoutR,      
        //    int[] arr = new int[5];
        //    string[] status = new string[2];
        //    status[0] = "Active";
        //    status[1] = "Deactive";
           
        //    for (int i = 0; i < RoomTypeID.Count(); i++)
        //    {
        //        List<tbl_commonRoomRate> ListRoomID = new List<tbl_commonRoomRate>();
        //        ListRoomID = (from obj in DB.tbl_commonRoomRates where (obj.HotelID==HotelCode && obj.RoomId == Convert.ToInt64(RoomTypeID[i])) select obj).ToList();
        //        string[] Nationalities = Country.Split('^');

        //        if (ListRoomID.Count == 0)
        //        {
        //            #region New Rate
        //            // If No Entries present
        //            for (int j = 0; j < CheckinR.Count; j++)
        //            {
        //                tbl_commonRoomRate Rate = new tbl_commonRoomRate();
        //                Rate.RoomId = Convert.ToInt64(RoomTypeID[i]);
        //                Rate.HotelID = HotelCode;
        //                Rate.SupplierId = Supplier;
        //                Rate.ValidNationality = Country;
        //                Rate.CurrencyCode = Currency;
        //                Rate.MealPlan = MealPlan;
        //                Rate.HotelContractId = 0;
        //                Rate.ValidNationality = Country;
        //                Rate.RateType = RateTypeCode[i];
        //                Rate.Checkin = CheckinR[j];
        //                Rate.Checkout = CheckoutR[j];
        //                Rate.RateInclude = RateInclude;
        //                Rate.RateExclude = RateExclude;
        //                Rate.MinStay = Convert.ToInt16(MinStay);
        //                Rate.MaxStay = Convert.ToInt16(MaxStay);
        //                Rate.TaxIncluded = TaxIncluded;
        //                Rate.TaxType = TaxType;
        //                Rate.TaxOn = TaxOn;
        //                Rate.TaxValue = Convert.ToInt16(TaxValue);
        //                Rate.TaxDetails = TaxDetails;
        //                Rate.BookingCode = BookingCode[i];
        //                Rate.CancellationPolicyId = CancelPolicy[i];
        //                //Rate.OfferId = Offers[i];
        //                Rate.TariffNote = RateNote[i];
        //                Rate.DayWise = Daywise[i];
        //                if (Daywise[i] == "Yes")
        //                {
        //                    if (Sunday[i] != "0")
        //                    {
        //                        Rate.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
        //                       // Rate.SunEB =Convert.ToDecimal( Sunday[i].Split(',')[1]);
        //                    }
        //                    if (Monday[i] != "0")
        //                    {
        //                        Rate.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
        //                       // Rate.MonEB =Convert.ToDecimal( Monday[i].Split(',')[1]);
        //                    }
        //                    if (Tuesday[i] != "0")
        //                    {
        //                        Rate.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
        //                       // Rate.TueEB = Convert.ToDecimal(Tuesday[i].Split(',')[1]);
        //                    }
        //                    if (Wednesday[i] != "0")
        //                    {
        //                        Rate.WedRR =Convert.ToDecimal( Wednesday[i].Split(',')[0]);
        //                      //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
        //                    }
        //                    if (Thursday[i] != "0")
        //                    {
        //                        Rate.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
        //                      //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
        //                    }
        //                    if (Friday[i] != "0")
        //                    {
        //                        Rate.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
        //                       // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
        //                    }
        //                    if (Saturday[i] != "0")
        //                    {
        //                        Rate.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
        //                       // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
        //                    }

        //                }
        //                else
        //                {
        //                    Rate.RR = Convert.ToDecimal(RoomRates[i]);
        //                    Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
        //                    Rate.CWB = Convert.ToDecimal(CWB[i]);
        //                    Rate.CNB = Convert.ToDecimal(CWN[i]);
        //                }
        //                Rate.Status = status[0];
        //                DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
        //                DB.SubmitChanges();
        //                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //                DB.SubmitChanges();
        //            }
        //            #endregion
        //        }
        //        else
        //        {
        //            //if Already Exists
        //            bool AvlGenData, AvlCancelation = false, AvlOffer = false, AvlNationality=false, AvlDates = false;

        //            var dGeneralData = ListRoomID.Where(d => d.MealPlan == MealPlan && d.CurrencyCode==Currency && d.SupplierId==Supplier && d.MinStay == MinStay && d.MaxStay==MaxStay).ToList();
        //            if (dGeneralData.Count() > 0) { AvlGenData = true; }
        //            else { AvlGenData = false; }

                   
        //            if (AvlGenData == true)
        //            {
        //              //var dDates = ListRoomID.Where(d => (Convert.ToDateTime(d.Checkin) <= Convert.ToDateTime(CheckinR[i])) && (Convert.ToDateTime(d.Checkout) >= Convert.ToDateTime(CheckoutR[i]))).ToList();
        //              var Checkins = dGeneralData.Select(d => DateTime.Parse(d.Checkin)).ToList();
        //              var Checkouts = dGeneralData.Select(d => DateTime.Parse(d.Checkout)).ToList();

        //              //for (int chk; chk < Checkins.Count;chk++ )
        //              //{
        //              //    DateTime chkIN = Checkins[chk];
        //              //    DateTime chkOUT = Checkouts[chk];
        //              //    if(Convert.ToDateTime(CheckinR[i])>chkIN || Convert.ToDateTime(CheckoutR[i])<chkOUT)
        //              //    {

        //              //    }
        //              //}



        //                //  if (dDates.Count > 0)
        //                //  {
        //                //      var RateID = dDates.Select(d => d.HotelRateID).ToList();
        //                //      foreach (Int64 ID in RateID)
        //                //      {
        //                //          tbl_commonRoomRate OldRate = DB.tbl_commonRoomRates.Single(x => x.HotelRateID == ID);
        //                //          OldRate.Status = status[1];
        //                //          DB.SubmitChanges();

        //                //          #region New Rate
        //                //          for (int j = 0; j < CheckinR.Count; j++)
        //                //          {
        //                //              tbl_commonRoomRate Rate = new tbl_commonRoomRate();
        //                //              Rate.RoomId = Convert.ToInt64(RoomTypeID[i]);
        //                //              Rate.HotelID = HotelCode;
        //                //              Rate.SupplierId = Supplier;
        //                //              Rate.ValidNationality = Country;
        //                //              Rate.CurrencyCode = Currency;
        //                //              Rate.MealPlan = MealPlan;
        //                //              Rate.HotelContractId = 0;
        //                //              Rate.ValidNationality = Country;
        //                //              Rate.RateType = RateTypeCode[i];
        //                //              Rate.Checkin = CheckinR[j];
        //                //              Rate.Checkout = CheckoutR[j];
        //                //              Rate.RateInclude = RateInclude;
        //                //              Rate.RateExclude = RateExclude;
        //                //              Rate.MinStay = Convert.ToInt16(MinStay);
        //                //              Rate.MaxStay = Convert.ToInt16(MaxStay);
        //                //              Rate.TaxIncluded = TaxIncluded;
        //                //              Rate.TaxType = TaxType;
        //                //              Rate.TaxOn = TaxOn;
        //                //              Rate.TaxValue = Convert.ToInt16(TaxValue);
        //                //              Rate.TaxDetails = TaxDetails;
        //                //              Rate.BookingCode = BookingCode[i];
        //                //              Rate.CancellationPolicyId = CancelPolicy[i];
        //                //              //Rate.OfferId = Offers[i];
        //                //              Rate.TariffNote = RateNote[i];
        //                //              Rate.DayWise = Daywise[i];
        //                //              if (Daywise[i] == "Yes")
        //                //              {
        //                //                  if (Sunday[i] != "0")
        //                //                  {
        //                //                      Rate.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
        //                //                      // Rate.SunEB =Convert.ToDecimal( Sunday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Monday[i] != "0")
        //                //                  {
        //                //                      Rate.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
        //                //                      // Rate.MonEB =Convert.ToDecimal( Monday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Tuesday[i] != "0")
        //                //                  {
        //                //                      Rate.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
        //                //                      // Rate.TueEB = Convert.ToDecimal(Tuesday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Wednesday[i] != "0")
        //                //                  {
        //                //                      Rate.WedRR = Convert.ToDecimal(Wednesday[i].Split(',')[0]);
        //                //                      //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Thursday[i] != "0")
        //                //                  {
        //                //                      Rate.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
        //                //                      //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Friday[i] != "0")
        //                //                  {
        //                //                      Rate.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
        //                //                      // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
        //                //                  }
        //                //                  if (Saturday[i] != "0")
        //                //                  {
        //                //                      Rate.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
        //                //                      // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
        //                //                  }

        //                //              }
        //                //              else
        //                //              {
        //                //                  Rate.RR = Convert.ToDecimal(RoomRates[i]);
        //                //                  Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
        //                //                  Rate.CWB = Convert.ToDecimal(CWB[i]);
        //                //                  Rate.CNB = Convert.ToDecimal(CWN[i]);
        //                //              }
        //                //              Rate.Status = status[0];
        //                //              DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
        //                //              DB.SubmitChanges();
        //                //              json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //                //              DB.SubmitChanges();
        //                //          }
        //                //          #endregion
        //                //      }

        //                //  }

        //                //foreach (string nation in Nationalities)
        //                //{
        //                //    var dNation = dGeneralData.Where(d => d.ValidNationality.Split('^').Contains(nation)).ToList();
        //                //    if (dNation.Count() == 0)
        //                //    {
        //                //        var RateID = dGeneralData.Select(d => d.HotelRateID).ToList();
        //                //        foreach (Int64 ID in RateID)
        //                //        {
        //                //            tbl_commonRoomRate Rate = DB.tbl_commonRoomRates.Single(x => x.HotelRateID == ID);
        //                //            string ExistingNationalities = Rate.ValidNationality;
        //                //            Rate.ValidNationality = ExistingNationalities + nation + "^";
        //                //            //DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
        //                //            DB.SubmitChanges();
        //                //        }

        //                //    }
        //                //}

        //            }
                    

        //            //for (int j = 0; j < CheckinR.Count; j++)
        //            //{
        //            //    var dCancelation = ListRoomID.Where(d => d.CancellationPolicyId.Split('^').Contains(CancelPolicy[i]));
        //            //    //var Room = (from obj in DB.tbl_commonRoomRates where (obj.Checkin == CheckinR[i]) select obj).ToList();

        //            //        tbl_commonRoomRate Rate = new tbl_commonRoomRate();
        //            //        Rate.RoomId = Convert.ToInt64(RoomTypeID[i]);
        //            //        Rate.HotelID = HotelCode;
        //            //        Rate.SupplierId = Supplier;
        //            //        Rate.ValidNationality = Country;
        //            //        Rate.CurrencyCode = Currency;
        //            //        Rate.MealPlan = MealPlan;
        //            //        Rate.HotelContractId = 0;
        //            //        Rate.ValidNationality = Country;
        //            //        Rate.RateType = RateTypeCode[i];
        //            //        Rate.Checkin = CheckinR[j];
        //            //        Rate.Checkout = CheckoutR[j];
        //            //        Rate.RateInclude = RateInclude;
        //            //        Rate.RateExclude = RateExclude;
        //            //        Rate.MinStay = Convert.ToInt16(MinStay);
        //            //        Rate.MaxStay = Convert.ToInt16(MaxStay);
        //            //        Rate.TaxIncluded = TaxIncluded;
        //            //        Rate.TaxType = TaxType;
        //            //        Rate.TaxOn = TaxOn;
        //            //        Rate.TaxValue = Convert.ToInt16(TaxValue);
        //            //        Rate.TaxDetails = TaxDetails;
        //            //        Rate.BookingCode = BookingCode[i];
        //            //        Rate.CancellationPolicyId = CancelPolicy[i];
        //            //        //Rate.OfferId = Offers[i];
        //            //        Rate.TariffNote = RateNote[i];
        //            //        Rate.DayWise = Daywise[i];
        //            //        if (Daywise[i] == "Yes")
        //            //        {
        //            //            if (Sunday[i] != "0")
        //            //            {
        //            //                Rate.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
        //            //                // Rate.SunEB = Convert.ToDecimal(Sunday[i].Split(',')[1]);
        //            //            }
        //            //            if (Monday[i] != "0")
        //            //            {
        //            //                Rate.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
        //            //                // Rate.MonEB = Convert.ToDecimal(Monday[i].Split(',')[1]);
        //            //            }
        //            //            if (Tuesday[i] != "0")
        //            //            {
        //            //                Rate.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
        //            //                // Rate.TueEB = Convert.ToDecimal(Tuesday[i].Split(',')[1]);
        //            //            }
        //            //            if (Wednesday[i] != "0")
        //            //            {
        //            //                Rate.WedRR = Convert.ToDecimal(Wednesday[i].Split(',')[0]);
        //            //                // Rate.WedEB = Convert.ToDecimal(Wednesday[i].Split(',')[1]);
        //            //            }
        //            //            if (Thursday[i] != "0")
        //            //            {
        //            //                Rate.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
        //            //                //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
        //            //            }
        //            //            if (Friday[i] != "0")
        //            //            {
        //            //                Rate.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
        //            //                // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
        //            //            }
        //            //            if (Saturday[i] != "0")
        //            //            {
        //            //                if (Saturday[i].Split(',')[0] != "undefined")
        //            //                {
        //            //                    Rate.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
        //            //                    //  Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
        //            //                }
        //            //            }

        //            //        }
        //            //        else
        //            //        {
        //            //            Rate.RR = Convert.ToDecimal(RoomRates[i]);
        //            //            Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
        //            //            Rate.CWB = Convert.ToDecimal(CWB[i]);
        //            //            Rate.CNB = Convert.ToDecimal(CWN[i]);
        //            //        }
        //            //        Rate.Status = status[0];
        //            //        DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
        //            //        DB.SubmitChanges();
        //            //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //            //        DB.SubmitChanges();
        //            //    }
        //        }
        //    }

        //    return json;
        //}

        //public class RatePrice
        //{
        //    public string Date { get; set; }           
        //    public string Daywise { get; set; }
        //    public string RRRate { get; set; }
        //    public string EBRate { get; set; }
        //    public string CWBRate { get; set; }
        //    public string CNBRate { get; set; }
        //    public string MonRate { get; set; }
        //    public string TueRate { get; set; }
        //    public string WedRate { get; set; }
        //    public string ThuRate { get; set; }
        //    public string FriRate { get; set; }
        //    public string SatRate { get; set; }
        //    public string SunRate { get; set; }
        //}
        //public class Rates
        //{
        //    public string RateType { get; set; }
        //    public string RoomName{ get; set; }
        //    public List<RatePrice> PriceList { get; set; }
        //}
        //[WebMethod(EnableSession = true)]
        //public string GetRates(Int64 HotelCode, string Destination, string Checkin, string Checkout, string nationality, int Nights, int Adults, int Childs)
        //{
        //    string jsonString = "";
        //    JavaScriptSerializer objserialize = new JavaScriptSerializer();

        //    if (HotelCode > 0)
        //    {
        //        List<Dictionary<string, object>> RatesList = new List<Dictionary<string, object>>();
        //        List<Dictionary<string, object>> HotelRatesList = new List<Dictionary<string, object>>();
        //        var HotelDetails = (from obj in DB.tbl_CommonHotelMasters where obj.sid == HotelCode select obj).FirstOrDefault();
        //        var AllRooms = (from Room in DB.tbl_commonRoomDetails where Room.HotelId == HotelCode select Room).ToList();
        //        if (Adults > 0)
        //            AllRooms = AllRooms.Where(e => e.AdultsWithoutChildAllowed >= Adults).ToList();
        //        if (Childs > 0)
        //            AllRooms = AllRooms.Where(e => e.AdultsWithChildAllowed >= Childs ).ToList();
        //        var RoomTypes = (from RType in DB.tbl_commonRoomTypes select RType).ToList();
        //        var RoomRates = (from rates in DB.tbl_commonRoomRates where (rates.HotelID == HotelCode) select rates).ToList();
        //        if (nationality!="")
        //            RoomRates = RoomRates.Where(e => e.ValidNationality.Split('^').Contains(nationality)).ToList();
               

        //        #region Result1
        //        DataTable dtResult = new DataTable();
        //        dtResult.Columns.Add("RoomName", typeof(string));
        //        dtResult.Columns.Add("Supplier", typeof(string));
        //        dtResult.Columns.Add("Nationality", typeof(string));
        //        dtResult.Columns.Add("RateType", typeof(string));
        //        dtResult.Columns.Add("Checkin", typeof(string));
        //        dtResult.Columns.Add("Checkout", typeof(string));
        //        dtResult.Columns.Add("MinStay", typeof(string));
        //        dtResult.Columns.Add("MaxStay", typeof(string));
        //        dtResult.Columns.Add("CurrencyCode", typeof(string));
        //        dtResult.Columns.Add("DayWise", typeof(string));
        //        dtResult.Columns.Add("RR", typeof(string));
        //        dtResult.Columns.Add("EB", typeof(string));
        //        dtResult.Columns.Add("CWB", typeof(string));
        //        dtResult.Columns.Add("CNB", typeof(string));
        //        dtResult.Columns.Add("MonRR", typeof(string));
        //        dtResult.Columns.Add("MonEB", typeof(string));
        //        dtResult.Columns.Add("TueRR", typeof(string));
        //        dtResult.Columns.Add("TueEB", typeof(string));
        //        dtResult.Columns.Add("WedRR", typeof(string));
        //        dtResult.Columns.Add("WedEB", typeof(string));
        //        dtResult.Columns.Add("ThuRR", typeof(string));
        //        dtResult.Columns.Add("ThuEB", typeof(string));
        //        dtResult.Columns.Add("FriRR", typeof(string));
        //        dtResult.Columns.Add("FriEB", typeof(string));
        //        dtResult.Columns.Add("SatRR", typeof(string));
        //        dtResult.Columns.Add("SatEB", typeof(string));
        //        dtResult.Columns.Add("SunRR", typeof(string));
        //        dtResult.Columns.Add("SunEB", typeof(string));
        //        dtResult.Columns.Add("MealPlan", typeof(string));
        //        dtResult.Columns.Add("TaxIncluded", typeof(string));
        //        dtResult.Columns.Add("TaxType", typeof(string));
        //        dtResult.Columns.Add("TaxOn", typeof(string));
        //        dtResult.Columns.Add("TaxValue", typeof(string));
        //        dtResult.Columns.Add("TaxDetails", typeof(string));
        //        dtResult.Columns.Add("BookingCode", typeof(string));
        //        dtResult.Columns.Add("RateInclude", typeof(string));
        //        dtResult.Columns.Add("RateExclude", typeof(string));
        //        dtResult.Columns.Add("OfferId", typeof(string));
        //        dtResult.Columns.Add("Refundable", typeof(string));
        //        dtResult.Columns.Add("CancellationPolicy", typeof(string));
        //        dtResult.Columns.Add("TariffNote", typeof(string));
        //        dtResult.Columns.Add("Status", typeof(string));


        //        for (int i = 0; i < AllRooms.Count; i++)
        //        {
        //            List<string> objRateList = new List<string>();
        //            objRateList.DefaultIfEmpty();
        //            var roomtype = RoomTypes.SingleOrDefault(e => e.RoomTypeID == AllRooms[i].RoomTypeId);

        //            for (int rts = 0; rts < RoomRates.Count; rts++)
        //            {
        //                if (RoomRates[rts].RoomId == roomtype.RoomTypeID)
        //                {
        //                    var roomrate = RoomRates[rts];
        //                    if (roomrate != null)
        //                    {
        //                        objRateList.Add(roomtype.RoomType);
        //                        objRateList.Add(roomrate.SupplierId.ToString());
        //                        objRateList.Add(roomrate.ValidNationality);
        //                        objRateList.Add(roomrate.RateType);
        //                        objRateList.Add(roomrate.Checkin);
        //                        objRateList.Add(roomrate.Checkout);
        //                        objRateList.Add(roomrate.MinStay.ToString());
        //                        objRateList.Add(roomrate.MaxStay.ToString());
        //                        objRateList.Add(roomrate.CurrencyCode);
        //                        objRateList.Add(roomrate.DayWise);
        //                        if (roomrate.RR == null)
        //                            roomrate.RR = 0;
        //                        objRateList.Add(roomrate.RR.ToString());
        //                        if (roomrate.EB == null)
        //                            roomrate.EB = 0;
        //                        objRateList.Add(roomrate.EB.ToString());
        //                        if (roomrate.CWB == null)
        //                            roomrate.CWB = 0;
        //                        objRateList.Add(roomrate.CWB.ToString());
        //                        if (roomrate.CNB == null)
        //                            roomrate.CNB = 0;
        //                        objRateList.Add(roomrate.CNB.ToString());
        //                        if (roomrate.MonRR == null)
        //                            roomrate.MonRR = 0;
        //                        objRateList.Add(roomrate.MonRR.ToString());
        //                        if (roomrate.MonEB == null)
        //                            roomrate.MonEB = 0;
        //                        objRateList.Add(roomrate.MonEB.ToString());
        //                        if (roomrate.TueRR == null)
        //                            roomrate.TueRR = 0;
        //                        objRateList.Add(roomrate.TueRR.ToString());
        //                        if (roomrate.TueEB == null)
        //                            roomrate.TueEB = 0;
        //                        objRateList.Add(roomrate.TueEB.ToString());
        //                        if (roomrate.WedRR == null)
        //                            roomrate.WedRR = 0;
        //                        objRateList.Add(roomrate.WedRR.ToString());
        //                        if (roomrate.WedEB == null)
        //                            roomrate.WedEB = 0;
        //                        objRateList.Add(roomrate.WedEB.ToString());
        //                        if (roomrate.ThuRR == null)
        //                            roomrate.ThuRR = 0;
        //                        objRateList.Add(roomrate.ThuRR.ToString());
        //                        if (roomrate.ThuEB == null)
        //                            roomrate.ThuEB = 0;
        //                        objRateList.Add(roomrate.ThuEB.ToString());
        //                        if (roomrate.FriRR == null)
        //                            roomrate.FriRR = 0;
        //                        objRateList.Add(roomrate.FriRR.ToString());
        //                        if (roomrate.FriEB == null)
        //                            roomrate.FriEB = 0;
        //                        objRateList.Add(roomrate.FriEB.ToString());
        //                        if (roomrate.SatRR == null)
        //                            roomrate.SatRR = 0;
        //                        objRateList.Add(roomrate.SatRR.ToString());
        //                        if (roomrate.SatEB == null)
        //                            roomrate.SatEB = 0;
        //                        objRateList.Add(roomrate.SatEB.ToString());
        //                        if (roomrate.SunRR == null)
        //                            roomrate.SunRR = 0;
        //                        objRateList.Add(roomrate.SunRR.ToString());
        //                        if (roomrate.SunEB == null)
        //                            roomrate.SunEB = 0;
        //                        objRateList.Add(roomrate.SunEB.ToString());
        //                        objRateList.Add(roomrate.MealPlan);
        //                        objRateList.Add(roomrate.TaxIncluded);
        //                        objRateList.Add(roomrate.TaxType);
        //                        objRateList.Add(roomrate.TaxOn);
        //                        objRateList.Add(roomrate.TaxValue.ToString());
        //                        objRateList.Add(roomrate.TaxDetails);
        //                        objRateList.Add(roomrate.BookingCode);
        //                        objRateList.Add(roomrate.RateInclude);
        //                        objRateList.Add(roomrate.RateExclude);
        //                        if (roomrate.OfferId == null)
        //                            roomrate.OfferId = "";
        //                        objRateList.Add(roomrate.OfferId.ToString());
        //                        if (roomrate.Refundable == null)
        //                            roomrate.Refundable = "";
        //                        objRateList.Add(roomrate.Refundable);
        //                        if (roomrate.CancellationPolicyId == null)
        //                            roomrate.CancellationPolicyId = "";
        //                        objRateList.Add(roomrate.CancellationPolicyId.ToString());
        //                        objRateList.Add(roomrate.TariffNote);
        //                        objRateList.Add(roomrate.Status);
        //                    }

        //                    DataRow row = dtResult.NewRow();
        //                    row.ItemArray = objRateList.ToArray();
        //                    dtResult.Rows.Add(row);
        //                    objRateList.Clear();
        //                }

        //            }


        //        }
        //        RatesList = JsonStringManager.ConvertDataTable(dtResult);
        //        #endregion

        //        #region User Dates
        //        List<string> UserDates = new List<string>();
        //        var UserRange = SplitDateRange(Convert.ToDateTime(Checkin).Date, Convert.ToDateTime(Checkout).Date, dayChunkSize: 15).ToList();

        //        string dateAlready = "";
        //        string charsToRemove = " 00:00:00";
        //        for (int ndates = 0; ndates < UserRange.Count; ndates++)
        //        {
        //            for (DateTime Dt = UserRange[ndates].Item1.Date; Dt <= UserRange[ndates].Item2.Date; Dt = Dt.AddDays(1))
        //            {
        //                if (Dt.ToString() != dateAlready)
        //                {
        //                    string tempdate = Dt.ToString();
        //                    UserDates.Add(tempdate.Replace(charsToRemove, string.Empty) + "^" + Dt.DayOfWeek);
        //                    dateAlready = tempdate;
        //                }
        //            }
        //        }

        //        #endregion


        //        #region Result2
        //        DataTable dtResult1 = new DataTable();
        //        dtResult1.Columns.Add("Checkin", typeof(string));
        //        dtResult1.Columns.Add("RoomName", typeof(string));
        //        dtResult1.Columns.Add("Supplier", typeof(string));
        //        dtResult1.Columns.Add("Nationality", typeof(string));
        //        dtResult1.Columns.Add("RateType", typeof(string));
        //        dtResult1.Columns.Add("Checkout", typeof(string));
        //        dtResult1.Columns.Add("MinStay", typeof(string));
        //        dtResult1.Columns.Add("MaxStay", typeof(string));
        //        dtResult1.Columns.Add("CurrencyCode", typeof(string));
        //        dtResult1.Columns.Add("DayWise", typeof(string));
        //        dtResult1.Columns.Add("RR", typeof(string));
        //        dtResult1.Columns.Add("EB", typeof(string));
        //        dtResult1.Columns.Add("CWB", typeof(string));
        //        dtResult1.Columns.Add("CNB", typeof(string));
        //        dtResult1.Columns.Add("MonRR", typeof(string));
        //        dtResult1.Columns.Add("MonEB", typeof(string));
        //        dtResult1.Columns.Add("TueRR", typeof(string));
        //        dtResult1.Columns.Add("TueEB", typeof(string));
        //        dtResult1.Columns.Add("WedRR", typeof(string));
        //        dtResult1.Columns.Add("WedEB", typeof(string));
        //        dtResult1.Columns.Add("ThuRR", typeof(string));
        //        dtResult1.Columns.Add("ThuEB", typeof(string));
        //        dtResult1.Columns.Add("FriRR", typeof(string));
        //        dtResult1.Columns.Add("FriEB", typeof(string));
        //        dtResult1.Columns.Add("SatRR", typeof(string));
        //        dtResult1.Columns.Add("SatEB", typeof(string));
        //        dtResult1.Columns.Add("SunRR", typeof(string));
        //        dtResult1.Columns.Add("SunEB", typeof(string));
        //        dtResult1.Columns.Add("MealPlan", typeof(string));
        //        dtResult1.Columns.Add("TaxIncluded", typeof(string));
        //        dtResult1.Columns.Add("TaxType", typeof(string));
        //        dtResult1.Columns.Add("TaxOn", typeof(string));
        //        dtResult1.Columns.Add("TaxValue", typeof(string));
        //        dtResult1.Columns.Add("TaxDetails", typeof(string));
        //        dtResult1.Columns.Add("BookingCode", typeof(string));
        //        dtResult1.Columns.Add("RateInclude", typeof(string));
        //        dtResult1.Columns.Add("RateExclude", typeof(string));
        //        dtResult1.Columns.Add("OfferId", typeof(string));
        //        dtResult1.Columns.Add("Refundable", typeof(string));
        //        dtResult1.Columns.Add("CancellationPolicy", typeof(string));
        //        dtResult1.Columns.Add("TariffNote", typeof(string));
        //        dtResult1.Columns.Add("Status", typeof(string));

               

        //        List<string> HotelDates = new List<string>();
        //        for (int rl = 0; rl < RatesList.Count; rl++)
        //        {
        //            var HotelRange = SplitDateRange(Convert.ToDateTime(RatesList[rl]["Checkin"]).Date, Convert.ToDateTime(RatesList[rl]["Checkout"]).Date, dayChunkSize: 15).ToList();
        //            string dateAlready1 = "";
        //            for (int hdates = 0; hdates < HotelRange.Count; hdates++)
        //            {
        //                for (DateTime Dt = HotelRange[hdates].Item1.Date; Dt <= HotelRange[hdates].Item2.Date; Dt = Dt.AddDays(1))
        //                {
        //                    if (Dt.ToString() != dateAlready1)
        //                    {
        //                        string tempdate = Dt.ToString();
        //                        HotelDates.Add(tempdate.Replace(charsToRemove, string.Empty) + "^" + Dt.DayOfWeek);
        //                        HotelDates.Add(RatesList[rl]["RoomName"].ToString());
        //                        HotelDates.Add(RatesList[rl]["Supplier"].ToString());
        //                        HotelDates.Add(RatesList[rl]["Nationality"].ToString());
        //                        HotelDates.Add(RatesList[rl]["RateType"].ToString());
        //                        HotelDates.Add(RatesList[rl]["Checkout"].ToString());
        //                        HotelDates.Add(RatesList[rl]["MinStay"].ToString());
        //                        HotelDates.Add(RatesList[rl]["MaxStay"].ToString());
        //                        HotelDates.Add(RatesList[rl]["CurrencyCode"].ToString());
        //                        HotelDates.Add(RatesList[rl]["DayWise"].ToString());
        //                        HotelDates.Add(RatesList[rl]["RR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["EB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["CWB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["CNB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["MonRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["MonEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TueRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TueEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["WedRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["WedEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["ThuRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["ThuEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["FriRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["FriEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["SatRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["SatEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["SunRR"].ToString());
        //                        HotelDates.Add(RatesList[rl]["SunEB"].ToString());
        //                        HotelDates.Add(RatesList[rl]["MealPlan"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TaxIncluded"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TaxType"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TaxOn"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TaxValue"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TaxDetails"].ToString());
        //                        HotelDates.Add(RatesList[rl]["BookingCode"].ToString());
        //                        HotelDates.Add(RatesList[rl]["RateInclude"].ToString());
        //                        HotelDates.Add(RatesList[rl]["RateExclude"].ToString());
        //                        HotelDates.Add(RatesList[rl]["OfferId"].ToString());
        //                        HotelDates.Add(RatesList[rl]["Refundable"].ToString());
        //                        HotelDates.Add(RatesList[rl]["CancellationPolicy"].ToString());
        //                        HotelDates.Add(RatesList[rl]["TariffNote"].ToString());
        //                        HotelDates.Add(RatesList[rl]["Status"].ToString());
        //                        dateAlready1 = tempdate;

        //                        DataRow row = dtResult1.NewRow();
        //                        row.ItemArray = HotelDates.ToArray();
        //                        dtResult1.Rows.Add(row);
        //                        HotelDates.Clear();
        //                    }

        //                }
        //            }

        //        }

        //        HotelRatesList = JsonStringManager.ConvertDataTable(dtResult1);

        //        #endregion

        //        #region User and Hotel Data Merge
        //        List<Dictionary<string, object>> objRatesList = new List<Dictionary<string, object>>();

        //        for (int i = 0; i < HotelRatesList.Count; i++)
        //        {
        //            var Hotel = HotelRatesList[i]["Checkin"];
        //            for (int j = 0; j < UserDates.Count; j++)
        //            {
        //                if (Hotel.ToString() == UserDates[j].ToString())
        //                {
        //                    objRatesList.Add(HotelRatesList[i]);
        //                }
        //            }
        //        }

        //        #endregion

        //        objRatesList.ForEach(x =>
        //        {

        //            x.Where(y => y.Value == null).ToList().ForEach(z =>
        //            {
        //                if (objRatesList.All(l => !l.ContainsKey(z.Key) || l[z.Key] == null))
        //                    x.Remove(z.Key);
        //            });
        //        });

        //        #region Rates List
        //        List<Rates> ListRate = new List<Rates>();
               
        //        foreach (Dictionary<string, object> objRate in objRatesList)
        //        {
        //            if (!ListRate.Exists(d => d.RateType == objRate["RateType"].ToString() && d.RoomName == objRate["RoomName"].ToString()))
        //            {
        //                List<RatePrice> ListPrice = GetRate(objRate["RoomName"].ToString(), objRate["RateType"].ToString(), objRatesList);
        //                ListRate.Add(new Rates
        //                {
        //                    RoomName = objRate["RoomName"].ToString(),
        //                    RateType = objRate["RateType"].ToString(),
        //                    PriceList = ListPrice,
                            
        //                });
                        
                       
        //            }
        //        }
        //        #region Special Dates with Rates
        //        var spDateRates = ListRate.Where(d => d.RateType == "SL").ToList();
        //        var gnRates = ListRate.Where(d => d.RateType == "GN").ToList();
        //        RatePrice newList;
        //        foreach (Rates objRate in gnRates)
        //        {
        //            foreach (RatePrice objPrice in objRate.PriceList)
        //            {
        //                foreach (Rates objSpRate in spDateRates)
        //                {
        //                    try
        //                    {
        //                        RatePrice obj = objSpRate.PriceList.Where(d => d.Date == objPrice.Date).FirstOrDefault();
        //                        if (obj != null)
        //                        {
        //                            objPrice.RRRate = obj.RRRate;
        //                            objPrice.EBRate = obj.EBRate;
        //                            objPrice.CWBRate = obj.CWBRate;
        //                            objPrice.CNBRate = obj.CNBRate;
        //                            objPrice.Daywise = obj.Daywise;
        //                            objPrice.MonRate = obj.MonRate;
        //                            objPrice.TueRate = obj.TueRate;
        //                            objPrice.WedRate = obj.WedRate;
        //                            objPrice.ThuRate = obj.ThuRate;
        //                            objPrice.FriRate = obj.FriRate;
        //                            objPrice.SatRate = obj.SatRate;
        //                            objPrice.SunRate = obj.SunRate;
        //                        }
        //                    }
        //                    catch(Exception ex)  
        //                    {
                                
        //                    }                  
                          

        //                }                        
        //            }
                    
        //        }
        //        ListRate = ListRate.Where(d => d.RateType == "GN").ToList();
        //        #endregion




        //        #endregion

        //        return objserialize.Serialize(new { Session = 1, retCode = 1, RatesList = ListRate, RoomTypes = RoomTypes, AllRooms = AllRooms });
        //       // jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"RatesList\":\"objRatesList\"}";
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}
        //public List<RatePrice> GetRate(string RoomName, string RateType, List<Dictionary<string, object>> Rates)
        //{
           
        //    List<RatePrice> List_Price = new List<RatePrice>();
        //    foreach (Dictionary<string, object> objRate in Rates)
        //    {
        //        if (objRate["RateType"].ToString() == RateType && objRate["RoomName"].ToString() == RoomName)
        //        {
        //            List_Price.Add(new RatePrice
        //            {
        //                Date = objRate["Checkin"].ToString(),
        //                RRRate = objRate["RR"].ToString(),
        //                EBRate = objRate["EB"].ToString(),
        //                CWBRate = objRate["CWB"].ToString(),
        //                CNBRate = objRate["CNB"].ToString(),
        //                Daywise = objRate["DayWise"].ToString(),
        //                MonRate = objRate["MonRR"].ToString(),
        //                TueRate = objRate["TueRR"].ToString(),
        //                WedRate = objRate["WedRR"].ToString(),
        //                ThuRate = objRate["ThuRR"].ToString(),
        //                FriRate = objRate["FriRR"].ToString(),
        //                SatRate = objRate["SatRR"].ToString(),
        //                SunRate = objRate["SunRR"].ToString(),
        //            });
        //        }
               
               
        //    }

           
        //    return List_Price;
        //}
        //public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        //{
        //    DateTime chunkEnd;
        //    while ((chunkEnd = start.AddDays(dayChunkSize)) < end)
        //    {
        //        yield return Tuple.Create(start, chunkEnd);
        //        start = chunkEnd;
        //    }
        //    yield return Tuple.Create(start, end);
        //}


    }
}



//if (
//    Room[da].SupplierId == Supplier &&
//    Room[da].HotelContractId == "0" &&
//    Room[da].ValidNationality == Country &&
//    Room[da].RateType == RateTypeCode[i] &&
//    Room[da].Checkin == CheckinR[i] &&
//    Room[da].Checkout == CheckoutR[i] &&
//    Room[da].MinStay == Convert.ToInt16(MinStay) &&
//    Room[da].MaxStay == Convert.ToInt16(MaxStay) &&
//    Room[da].CurrencyCode == Currency &&
//    Room[da].DayWise == Daywise[i] &&
//    Room[da].RR == Convert.ToDecimal(RoomRates[i]) &&
//    Room[da].EB == Convert.ToDecimal(ExtraBeds[i]) &&
//    Room[da].CWB == Convert.ToDecimal(CWB[i]) &&
//    Room[da].CNB == Convert.ToDecimal(CWN[i]) &&

//    Room[da].MonRR == Monday[i].Split(',')[0] &&
//    Room[da].MonEB == Monday[i].Split(',')[1] &&

//    Room[da].TueRR == Tuesday[i].Split(',')[0] &&
//    Room[da].TueEB == Tuesday[i].Split(',')[1] &&

//    Room[da].WedRR == Wednesday[i].Split(',')[0] &&
//    Room[da].WedEB == Wednesday[i].Split(',')[1] &&

//    Room[da].ThuRR == Thursday[i].Split(',')[0] &&
//    Room[da].ThuEB == Thursday[i].Split(',')[1] &&

//    Room[da].FriRR == Friday[i].Split(',')[0] &&
//    Room[da].FriEB == Friday[i].Split(',')[1] &&

//    Room[da].SatRR == Saturday[i].Split(',')[0] &&
//    Room[da].SatEB == Saturday[i].Split(',')[1] &&

//    Room[da].SunRR == Sunday[i].Split(',')[0] &&
//    Room[da].SunEB == Sunday[i].Split(',')[1]
//    )
//{ }
//if (Room[da].Checkin != CheckinR[i] && Room[da].Checkout == CheckoutR[i])
//{ }