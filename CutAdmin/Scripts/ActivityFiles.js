﻿/// <reference path="D:\CUTAE\CUTAE\activities-orange_files/Thumnail.aspx" />


function Save() {
    if ($("#btn_Supplier2").val() == "Update") {
        Update_Priority()
    }
    else {
        AddPriority();
    }
}

function SaveMode() {

    if ($("#btn_Supplier").val() == "Update") {

        Update_Mode()
    }
    else {
        AddMode();
    }
}

$(function () {
    GetActivitylist()

    $("#Button1").click(function (evt) {
        debugger
        var fileUpload = $("#Imges").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
            FileName += files[i].name + "^";
        }
        //Success(FileName)
        $.ajax({
            url: "ActImagesUploder.ashx?sid=" + sid + "&ImagePath=" + FileName,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                Success("Imgaes Uploded Successfully")
                $("#result").empty();
                $("#Imges").val("");
                $("#AddUpdateDialogBox").modal("hide")
                GetActivitylist()
            },
            error: function (err) {
            }
        });
    });

})

function OpenPopUp() {
    GetMode();
    var newId = Date.now().toString().substr(8); // or use any method that you want to achieve this string
    $("#Password").val(newId)
    $("#AddCustomer").modal("show")
}

function OpenPopUp1() {
    Getpriority();
    var newId = Date.now().toString().substr(8); // or use any method that you want to achieve this string
    $("#Password").val(newId)
    $("#AddCustomer2").modal("show")
}

function AddMode() {
    debugger;
    var fName = $("#Fname").val()
    var param = { fName: fName }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/AddMode",
        data: JSON.stringify(param),
        contentType: "application/json",
        datatype: "json",
        success: function (data) {
            var obj = JSON.parse(data.d);
            if (obj.retCode == 1) {
                $('#SpnMessege').text("Tour Type is Added");
                $('#ModelMessege').modal('show');
                GetMode();
                $("#Fname").val('');
            }
            else {
                Success("Something Went Wrog");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        }
    });
}


function GetMode() {
    $("#tbl_Getmode").dataTable().fnClearTable();
    $("#tbl_Getmode").dataTable().fnDestroy();
    $.ajax({
        url: "ActivityHandller.asmx/GetMode",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_MatchSummary = result.dtresult;

                for (var i = 0; i < arr_MatchSummary.length; i++) {
                    var html = '';
                    var sid = arr_MatchSummary[i].Sid;
                    html += '<tr><td>' + (i + 1) + '</td>'
                    html += '<td align="center">' + arr_MatchSummary[i].TourType + ' </td>'
                    html += '<td align="center"><a style="cursor:pointer" onclick="GetModetoupdate(\'' + sid + '\',\'' + arr_MatchSummary[i].TourType + '\')"><span class="fa fa-edit" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update Details"></span></a> | <a style="cursor:pointer" onclick="DeleteMode(\'' + sid + '\')"><span class="fa fa-trash-o" data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete"></span></a></td>'
                    html += '</tr>'
                    $("#tbl_Getmode tbody").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_Getmode").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
                $("#tbl_Getmode").css("width", "100%")
            }
        }
    })
}

function AddPriority() {
    debugger;

    var fName = $("#fname").val()

    var param = { fName: fName }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/AddPriority",
        data: JSON.stringify(param),
        contentType: "application/json",
        datatype: "json",
        success: function (data) {
            var obj = JSON.parse(data.d);
            if (obj.retCode == 1) {
                $('#SpnMessege').text("Priority Type is Added");
                $('#ModelMessege').modal('show');
                $("#fname").val('')
                Getpriority();
            }
            else {
                Success("Something Went Wrog");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
              
            }
        }
    });
}

function Getpriority() {
    $("#tbl_GetPriority").dataTable().fnClearTable();
    $("#tbl_GetPriority").dataTable().fnDestroy();

    $.ajax({
        url: "ActivityHandller.asmx/Getpriority",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_MatchSummary = result.dtresult;

                for (var i = 0; i < arr_MatchSummary.length; i++) {
                    var html = '';
                    var sid = arr_MatchSummary[i].Sid;
                    html += '<tr><td>' + (i + 1) + '</td>'
                    html += '<td align="center">' + arr_MatchSummary[i].PriorityType + '</td>'

                    html += '<td align="center"><a style="cursor:pointer" onclick="GetPriorityttoupdate(\'' + sid + '\',\'' + arr_MatchSummary[i].PriorityType + '\')"><span class="fa fa-edit" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update Details"></span></a> | <a style="cursor:pointer" onclick="DeletePriority(\'' + sid + '\')"><span class="fa fa-trash-o" data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete"></span></a></td>'
                    html += '</tr>'
                    $("#tbl_GetPriority tbody").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()
                $("#tbl_GetPriority").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
                $("#tbl_GetPriority").css("width", "100%")
            }
        }
    })
}

var Pid;

function GetPriorityttoupdate(sid, name) {
    $("#btn_Supplier2").val("Update");
    $("#fname").val(name);
    Pid = sid;
    //Update_Priority();
}

function GetModetoupdate(sid, name) {
    $("#btn_Supplier").val("Update");
    $("#Fname").val(name);
    Pid = sid;
}

function Update_Priority() {

    var fName = $("#fname").val()

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/Update_Priority",
        data: '{"fName":"' + fName + '","Pid":"' + Pid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Getpriority();
                $('#SpnMessege').text("Priority Type is Updated");
                $('#ModelMessege').modal('show');
                $("#fname").val('');
                $("#btn_Supplier2").val("Save");
            }
            if (result.retCode == 0) {
                Success("Something went wrong!")
            }
        },
        error: function () {
            Success("An error occured while updating details");
        }
    });
}

function Update_Mode() {

    var fName = $("#Fname").val()

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/Update_Mode",
        data: '{"fName":"' + fName + '","Pid":"' + Pid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                GetMode();
                $('#SpnMessege').text("Tour Type is Updated");
                $('#ModelMessege').modal('show');
                $("#Fname").val('');
                $("#btn_Supplier").val("Save");
            }
            if (result.retCode == 0) {
                Success("Something went wrong!")
            }
        },
        error: function () {
            Success("An error occured while updating details");
        }
    });
}

function DeletePriority(sid) {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/DeletePriority",
        data: '{"sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {

                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                $("#Dialog_Deletecandidate").dialog("close");
                $('#SpnMessege').text("Priority Type is Deleted");
                $('#ModelMessege').modal('show');
                Getpriority();
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}

function DeleteMode(sid) {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/DeleteMode",
        data: '{"sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                // window.location.href = "login.aspx"; // Session end
                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                $("#Dialog_Deletecandidate").dialog("close");
                $('#SpnMessege').text("Tour Type is Deleted");
                $('#ModelMessege').modal('show');
                GetMode();
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}

function GetActivitylist() {
    $("#tbl_Actlist").dataTable().fnClearTable();
    $("#tbl_Actlist").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();
    $.ajax({
        url: "ActivityHandller.asmx/ActList",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrActivity = result.arrActivity
                for (var i = 0; i < arrActivity.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:3%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrActivity[i].Name + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrActivity[i].City + ' ,' + arrActivity[i].Country + ' </td>'
                    html += '<td style="width:30%"  align="center">'
                    for (var j = 0; j < arrActivity[i].TourType.length; j++) {
                        html += arrActivity[i].TourType[j] + " ";
                    }
                    if (arrActivity[i].Mode.length == 0)
                        html += '-'
                    html += '</td>'

                    html += '<td style="width:10%" align="center">'
                    for (var j = 0; j < arrActivity[i].Mode.length; j++) {
                        html += arrActivity[i].Mode[j].Type.replace('TKT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-67-tags.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Ticket Only"> ').replace('SIC', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-44-group.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Sharing"> ').replace('PVT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-6-car.png" alt="" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Private"> ');
                    }
                    if (arrActivity[i].Mode.length == 0)
                        html += '-'
                    html += '</td>'
                    if (arrActivity[i].Status == "True")
                    {
                        html += '<td style="width:15%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',this.value)">On</label><label for="chk_Off' + i + '" class="button red-active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',this.value)">Off</label></span></td>';
                      
                    }
                       
                    else
                    {
                        html += '<td style="width:15%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',this.value)">On</label><label for="chk_Off' + i + '" class="button red-active active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',this.value)">Off</label></span></td>';
                       
                    }
                       
                    

                    // html += '<td style="width:10%" align="center"><a style="cursor:pointer" <a href="AddActivity.aspx?id=' + arrActivity[i].Aid + '"><span class="icon-extract" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update Details"></span></a>  |  <a style="cursor:pointer" onclick="UpdateImage(\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].sImages + '\')"><span class="icon-tools" data-toggle="tooltip" data-placement="right" title="" data-original-title="Add Images"></span></a></td></tr>'
                    html += '<td style="width:25%" align="center"><a style="cursor:pointer" href="AddActivity.aspx?id=' + arrActivity[i].Aid + '"><span class="icon-pencil icon-size2" title="Edit"></span></a> &nbsp; | &nbsp; <a style="cursor:pointer" onclick="Delete(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\')"><span class="icon-trash" title="Delete"></span></a></td>'

                    html += '<td style="width:10%" align="center"><a style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td></tr>'

                    $("#tbl_Actlist").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_Actlist").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function Delete(Id) {

    if (confirm("Are You want to delete this activity.")) {
        Deletee(Id);
    }
    else {

    }
}

function Deletee(Id) {
    debugger;
    $.ajax({
        url: "ActivityHandller.asmx/DeleteActivity",
        type: "post",
        data: '{"id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Activity deleted.");
                GetActivitylist();
                //$("#" + sid + '_' + No).remove();
                //$('[data-toggle="tooltip"]').tooltip()
            }
        }
    })
}

var sid = ''; var FileName = '';
function UpdateImage(ActName, Sr_No, Act_Images) {
    FileName = ''
    FileName = Act_Images + "^";
    sid = Sr_No;
    GetFiles(Sr_No, Act_Images)
    $("#result").empty();
    $("#ActName").text(ActName);
    $("#Imges").val("");
    $("#AddUpdateDialogBox").modal("show")
}

function imgError(Image) {
    Image.src = "https://2.bp.blogspot.com/-ex3V86fj4dQ/UrCQQa4cLsI/AAAAAAAAFdA/j2FCTmGOrog/s1600/no-thumbnail.png";

}

function GetFiles(Sr_No, No) {
    $("#old_Img").empty();
    No = No.split('^')
    var html = ''
    var Url = "";
    for (var i = 0; i < No.length; i++) {
        if (No[i] != "") {
            html = ''
            Url = '../activities-orange_files/Thumnail.aspx?fn=' + No[i] + '&w=192&h=192&rf=';
            html += '<div id="' + sid + '_' + (i) + '"><tabel><tbody><tr><td><img class="thumbnail" style="heigh:100%;width:50%" src="' + Url + '" style="heigh:100px;width:150px" title="' + No[i] + '" onError="imgError(this)"  /></td><td><a class="black fa fa-pencil-square-o" style="padding-right: 50px;padding-top: 10px;padding-bottom: 5px;" onclick="SetFile(\'' + No[i] + '\',\'' + i + '\',\'' + sid + '\')"> Set Start</a><td><td><a class="icon-remove-sign" style="color:red" onclick="DeleteFile(\'' + No[i] + '\',\'' + i + '\',\'' + sid + '\')"> Delete</a></td></tr></tbody></tabel></div>'
            $("#old_Img").append(html);
        }
    }
}

function SetFile(FileNo, No, sid) {
    var Images = '';
    var SetImage = FileName;
    var Files = SetImage.split('^');
    for (var i = 0; i < Files.length; i++) {
        Files[i] = Files[i].replace(FileNo, "")
        if (i == 0) {
            Images += FileNo + "^";
        }
        if (Files[i] != "") {
            Images += Files[i] + "^";
        }
    }

    $.ajax({
        url: "ActivityHandller.asmx/SetFiles",
        type: "post",
        data: '{"sImage":"' + Images + '","sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                GetActivitylist()
                GetFiles(sid, Images)
                $('[data-toggle="tooltip"]').tooltip()
            }
        }
    })
}

function DeleteFile(FileNo, No, sid) {
    FileName = FileName.replace(FileNo + "^", "")
    $.ajax({
        url: "ActivityHandller.asmx/DeleteFile",
        type: "post",
        data: '{"FileNo":"' + FileNo + '","noFiles":"' + FileName + '","sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                GetActivitylist()
                $("#" + sid + '_' + No).remove();
                $('[data-toggle="tooltip"]').tooltip()
            }
        }
    })
}


// Demo complex modal

function Rates(aid, name, Location, city, country) {
    location.href = "Activityratelist.aspx?id=" + aid + "&name=" + name + "&location=" + Location + "&pCities=" + city + "&country=" + country + "&Type=Update";
}

function RatesModal(aid, name) {

    $.modal({
        //content: '<table class="table responsive-table" id="tbl_GetHotel">' +
        //           '<thead>' +

        //           '<tr style="background-color: #494747; color: white">' +

        //           '<th>' +
        //            '<span class="text-left">Supplier</span>' +
        //            '</th>' +

        //             '<th>' +
        //            '<span class="text-left">Hotel Name</span>' +
        //             '</th>' +

        //              '<th>' +
        //             '<span class="text-left">Address</span>' +
        //             '</th>' +

        //            '<th>' +
        //             '<span class="text-left">Description</span>' +
        //              '</th>' +

        //              '<th>' +
        //             '<span class="text-left">Catagory</span>' +
        //              '</th>' +

        //            '</tr>' +
        //        '</thead>' +
        //          ' <tbody>' +
        //          '</tbody>' +
        //         '</table>',

        content: ' <div id="Locations">' +
                '</div>',

        title: 'Rates',
        width: 600,
        height: 100,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            },
            //'Center': {
            //    color: 'green',
            //    click: function (win) { win.centerModal(true); }
            //},
            //'Other action': {
            //    color: 'blue',
            //    click: function (win) { win.closeModal(); }
            //},
            //'Last action': {
            //    color: 'orange',
            //    click: function (win) { win.closeModal(); }
            //}
        },
        buttons: {
            'Update Rates': {
                classes: 'blue-gradient glossy full-width',
                click: function () { UpdateRates(aid, name); }
            }
        },
        buttonsLowPadding: true
    });

    // 
    // 
    // 
    // 
    // 

    // 
    // 
    // 
    // 


    //var Label1 = arrActivity

    for (var i = 0; i < arrActivity.length; i++) {
        if (arrActivity[i].Aid == aid) {

            var lbl = arrActivity[i].City;
            var labl = lbl.split(',');
            var label; var html = '';
            if (labl == "null") {
                html += ''
            }
            else if (labl.length == 1) {
                for (var j = 0; j < labl.length; j++) {
                    label = labl[j];
                    html += '<div class="columns">'
                    html += '       <div class="new-row four-columns">'
                    html += '              <input type="radio" name="radio" id="rd_Label' + (i + 1) + '" class="mid-margin-left PPCity" value="' + label + '">'
                    html += '             <label class="label">' + label + '</label>'
                    html += '       </div>'
                    html += '</div>'
                }
            }
            else {
                for (var j = 0; j < labl.length - 1; j++) {
                    label = labl[j];
                    html += '<div class="columns">'
                    html += '       <div class="new-row four-columns">'
                    html += '              <input type="radio" name="radio" id="rd_Label' + (i + 1) + '" class="mid-margin-left PPCity" value="' + label + '">'
                    html += '             <label class="label">' + label + '</label>'
                    html += '       </div>'
                    html += '</div>'
                }
            }



        }

    }
    $("#Locations").append(html);

};

var Cities; var pCities = "";
function UpdateRates(aid, name) {
    var lsss = document.getElementsByClassName('PPCity');
    for (var i = 0; i < lsss.length; i++) {
        Cities = lsss[i];
        //Cities[i] = document.getElementById('rd_Label' + i).value;

        // Cities[i] = $("input[name=radio]:checked")[i].val();

        if (Cities.checked == true) {
            pCities = Cities.value;
            // pCities = selectedCities;
        }

    }


    window.location.href = "AddActivityTariff.aspx?id=" + aid + "&pCities=" + pCities + "&name=" + name;





}

//window.onload = function () {

//    //Check File API support
//    if (window.File && window.FileList && window.FileReader) {
//        $("#old_Img").empty();
//        var filesInput = document.getElementById("Imges");

//        filesInput.addEventListener("change", function (event) {

//            var files = event.target.files; //FileList object
//            var output = document.getElementById("result");

//            for (var i = 0; i < files.length; i++) {
//                var file = files[i];

//                //Only pics
//                if (!file.type.match('image'))
//                    continue;

//                var picReader = new FileReader();

//                picReader.addEventListener("load", function (event) {

//                    var picFile = event.target;

//                    var div = document.createElement("div");

//                    div.innerHTML = "<img class='thumbnail' style='heigh:100%;width:50%' src='" + picFile.result + "'" +
//                            "title='" + picFile.name + "'/>";

//                    output.insertBefore(div, null);
//                });
//                picReader.readAsDataURL(file);
//            }
//        });
//    }
//    else {
//        console.log("Your browser does not support File API");
//    }
//}


function RedirectStatus(aid, value) {
    var data = {
        Actid: aid,
        Status: value
    }
    $.ajax({
        url: "ActivityHandller.asmx/RedirectStatus",
        type: "post",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                if (value == "On")
                {
                    Success("Status Updated");
                    $("#chk_On" + i).removeClass("button red-active active");
                    $("#chk_On" + i).addClass("button blue-active active");
                }
                else {
                    Success("Status Updated");
                    $("#chk_Off" + i).removeClass("button blue-active active");
                    $("#chk_Off" + i).addClass("button red-active active");
                }
               
                //GetActivitylist()
            }
            else
                Success("Something Went Wrong");
        }
    })
}