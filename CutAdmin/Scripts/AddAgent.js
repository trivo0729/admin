﻿var s;
var FrachiseId;
var arrCountry;
var arrCity;
$(document).ready(function () {
    //$(function () {
    //    $('[data-toggle="popover"]').popover();
    //});

    GetGroups();
    $('#txt_AgencyName').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            Success("An error occured while loading countries")
        }
    });
    $('#chkTermsAndConditions').change(function () {
        if ($('#chkTermsAndConditions').is(':checked')) {
            $('#btn_RegisterSupplier').attr("disabled", false);
        }
        else
            $('#btn_RegisterSupplier').attr("disabled", true);
        $("#tbl_AgentRegistration tr td label").css("display", "none");
        $("#tbl_AgentRegistration tr td label").html("* This field is required");
    });
    $('#selCountry').change(function () {
        if ($('#selCountry option:selected').val() == 'IN') {
            $('#txt_PAN').attr("disabled", false);
            $('#txt_ReferenceNumber').attr("disabled", false);
        }
        else {
            $('#txt_PAN').attr("disabled", true);
            $('#txt_ReferenceNumber').attr("disabled", true);
        }
    });
    $('#Select_IATA').change(function () {
        $("#lbl_IATA").css("display", "none");
        $('#txt_IATA').val('');
        if ($('#Select_IATA option:selected').val() == '1') {
            $('#txt_IATA').attr("disabled", false);
        }
        else {
            $('#txt_IATA').attr("disabled", true);
        }
    });
    if (location.href.indexOf('?') != -1) {
        FrachiseId = GetQueryStringParamsForAgentRegistrationUpdate('s');
        if (FrachiseId == undefined) {
            $("#btn_RegisterSupplier").val('Update');
            GetGroups();
            hiddenID = GetQueryStringParamsForAgentRegistrationUpdate('sid');
            $("#txt_AgencyName").val(GetQueryStringParamsForAgentRegistrationUpdate('sAgencyName').replace(/%20/g, ' '));
            var fullname = GetQueryStringParamsForAgentRegistrationUpdate('sContactPerson').replace(/%20/g, ' ');
            var fullnamesplit = fullname.split(' ');
            $("#txt_FirstName").val(fullnamesplit[0]);
            $("#txt_LastName").val(fullnamesplit[1]);
            debugger;
            //$("#txt_FirstName").val(GetQueryStringParamsForAgentRegistrationUpdate('sFirstName').replace(/%20/g, ' '));
            //$("#txt_LastName").val(GetQueryStringParamsForAgentRegistrationUpdate('sLastName').replace(/%20/g, ' '));
            $("#txt_Designation").val(GetQueryStringParamsForAgentRegistrationUpdate('sDesignation').replace(/%20/g, ' '));
            $("#txt_Address").val(GetQueryStringParamsForAgentRegistrationUpdate('sAddress').replace(/%20/g, ' '));
            $("#txt_IATA").val(GetQueryStringParamsForAgentRegistrationUpdate('IATANumber').replace(/%20/g, ' '));
            setTimeout(function () {
                if ($("#txt_IATA").val() != '') {
                    $("#Select_IATA").val('1');
                    $('#txt_IATA').attr("disabled", false);
                    $("#Div_IATA .select span")[0].textContent = "Approved";
                }
                else {
                    $("#Select_IATA").val('2');
                    $('#txt_IATA').attr("disabled", true);
                    $("#Div_IATA .select span")[0].textContent = "UnApproved";
                }
            }, 1500);

            //$("#selCountry").val(GetQueryStringParamsForAgentRegistrationUpdate('sCountry').replace(/%20/g, ' '));

            $("#txt_PinCode").val(GetQueryStringParamsForAgentRegistrationUpdate('nPinCode').replace(/%20/g, ' '));
            $("#txt_Email").val(GetQueryStringParamsForAgentRegistrationUpdate('sEmail').replace(/%20/g, ' '));
            //$("#txt_Password").val(GetQueryStringParamsForAgentRegistrationUpdate('sPassword').replace(/%20/g, ' '));
            $("#txt_Phone").val(GetQueryStringParamsForAgentRegistrationUpdate('nPhone').replace(/%20/g, ' '));
            $("#txt_Mobile").val(GetQueryStringParamsForAgentRegistrationUpdate('nMobile').replace(/%20/g, ' '));
            $("#txt_Fax").val(GetQueryStringParamsForAgentRegistrationUpdate('nFax').replace(/%20/g, ' '));
            $("#Select_Usertype").val(GetQueryStringParamsForAgentRegistrationUpdate('sUsertype').replace(/%20/g, ' '));
            //$("#txt_ReferenceNumber").val(GetQueryStringParamsForAgentRegistrationUpdate('nServiceTaxNumber').replace(/%20/g, ' '));
            $("#txt_PAN").val(GetQueryStringParamsForAgentRegistrationUpdate('nPAN').replace(/%20/g, ' '));
            $("#txt_Website").val(GetQueryStringParamsForAgentRegistrationUpdate('sWebsite').replace(/%20/g, ' '));
            //setTimeout(function () { $("#SelCurrency").val(GetQueryStringParamsForAgentRegistrationUpdate('sPreferredCurrency').replace(/%20/g, ' ')); }, 1500);
            //$("#SelCurrency").val(GetQueryStringParamsForAgentRegistrationUpdate('sPreferredCurrency').replace(/%20/g, ' '));
            if (GetQueryStringParamsForAgentRegistrationUpdate('GSTNumber').replace(/%20/g, ' ') != "null") {
                $("#txt_GSTNO").val(GetQueryStringParamsForAgentRegistrationUpdate('GSTNumber').replace(/%20/g, ' '));
            }
            else {
                $("#txt_GSTNO").val("");
            }
            //$("#txt_Remarks").val(GetQueryStringParamsForAgentRegistrationUpdate('sRemarks').replace(/%20/g, ' '));
            hiddenAgentUniqueCode = GetQueryStringParamsForAgentRegistrationUpdate('sUniqueCode');
            hiddenLoginFlag = GetQueryStringParamsForAgentRegistrationUpdate('bLoginFlag').replace(/%20/g, ' ');
            //$("#Select_Usertype").prop("disabled", true);
            $('#divLogo').css("display", "");
            $('#txt_Email').css("display", true);
            //$('#txt_Email').attr("readonly", true);
            $.ajax({
                url: "../AgencyLogos/" + hiddenAgentUniqueCode + ".png",
                type: "HEAD",
                async: false,
                dataType: "json",
                success: function (data) {
                    $('#divLogoEdit').append("<img src='../AgencyLogos/" + hiddenAgentUniqueCode + ".png' style='width:50%' alt='Logo not found' />");
                },
                error: function () {
                    $.ajax({
                        url: "../AgencyLogos/" + hiddenAgentUniqueCode + ".jpg",
                        type: "HEAD",
                        async: false,
                        dataType: "json",
                        success: function (data) {
                            $('#divLogoEdit').append("<img src='../AgencyLogos/" + hiddenAgentUniqueCode + ".jpg' style='width:50%' alt='Logo not found' />");
                        },
                        error: function () {
                            $.ajax({
                                url: "../AgencyLogos/" + hiddenAgentUniqueCode + ".jpeg",
                                type: "HEAD",
                                async: false,
                                dataType: "json",
                                success: function (data) {
                                    $('#divLogoEdit').append("<img src='../AgencyLogos/" + hiddenAgentUniqueCode + ".jpeg' style='width:50%' alt='Logo not found' />");
                                },
                                error: function () {
                                    $.ajax({
                                        url: "../AgencyLogos/" + hiddenAgentUniqueCode + ".bmp",
                                        type: "HEAD",
                                        async: false,
                                        dataType: "json",
                                        success: function (data) {
                                            $('#divLogoEdit').append("<img src='../AgencyLogos/" + hiddenAgentUniqueCode + ".bmp' style='width:50%' alt='Logo not found' />");
                                        },
                                        error: function () {
                                            $.ajax({
                                                url: "../AgencyLogos/" + hiddenAgentUniqueCode + ".gif",
                                                type: "HEAD",
                                                async: false,
                                                dataType: "json",
                                                success: function (data) {
                                                    $('#divLogoEdit').append("<img src='../AgencyLogos/" + hiddenAgentUniqueCode + ".gif' style='width:50%' alt='Logo not found' />");
                                                },
                                                error: function () {
                                                    $('#divLogoEdit').append("<img src='../AgencyLogos/123.png' style='width:50%' alt='Logo not found' />");
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });



            //$("#selGroup").val(GetQueryStringParamsForAgentRegistrationUpdate('sGroup').replace(/%20/g, ' '));
            // $('#imgLogo').attr("src",'<img src="../AgencyLogos/' + hiddenAgentUniqueCode + '.png" style="width:50%" alt="Logo not found" />');
            //setTimeout(function () { $("#selGroup option:selected").text(GetQueryStringParamsForAgentRegistrationUpdate('sGroup').replace(/%20/g, ' ')); }, 1500);
            setTimeout(function () {
                AppendGroup(GetQueryStringParamsForAgentRegistrationUpdate('sGroup').replace(/%20/g, ' '));
            }, 4000);
            var cntr = '';
            setTimeout(function () {
                cntr = GetQueryStringParamsForAgentRegistrationUpdate('sCountry').replace(/%20/g, ' ');
                $("#selCountry").val(GetQueryStringParamsForAgentRegistrationUpdate('sCountry').replace(/%20/g, ' '));
            }, 1500);
            setTimeout(function () {

                //GetCity($("#selCountry").val());
                GetCountryCity(cntr);
                AppendUserType(GetQueryStringParamsForAgentRegistrationUpdate('sUsertype').replace(/%20/g, ' '));
                AppendCurrency(GetQueryStringParamsForAgentRegistrationUpdate('CurrencyCode').replace(/%20/g, ' '));
                setTimeout(function () {
                    AppendCity(GetQueryStringParamsForAgentRegistrationUpdate('City').replace(/%20/g, ' '));
                }, 3000);
                //AppendStatus(GetQueryStringParamsForAgentRegistrationUpdate('Status').replace(/%20/g, ' '));
                //AppendGroup(GetQueryStringParamsForAgentRegistrationUpdate('Group').replace(/%20/g, ' '));
                GetNationality(cntr);
                if ($("#selCountry").val() == 'IN') {
                    $('#txt_PAN').attr("disabled", false);
                    $('#txt_ReferenceNumber').attr("disabled", false);
                }
                else {
                    $('#txt_PAN').val('');
                    $('#txt_ReferenceNumber').val('');
                }
            }, 2000);

            //var tempCode = GetQueryStringParamsForAgentRegistrationUpdate('sCity').replace(/%20/g, ' ');

            //setTimeout(function () { $("#selCity").val(tempCode); }, 4000);

            //s = GetQueryStringParamsForAgentRegistrationUpdate('sGroup').replace(/%20/g, ' ');

            var refreh = setTimeout(GetGroups(), 2000);
            //var HotelGroup = s.split(/-/);
            //var GROUP = HotelGroup[0];
            //var Markup = HotelGroup[1];
            //setTimeout(function () { $('#selGroup option').filter(function () { return $.trim($(this).text()) == GROUP; }).attr('selected', true); }, 1000);
            //setTimeout(function () { $('#selVisaGroup option').filter(function () { return $.trim($(this).text()) == Markup; }).attr('selected', true); }, 1000);
            //setTimeout(function () { $("#selGroup option:selected").text(GetQueryStringParamsForAgentRegistrationUpdate('sGroup').replace(/%20/g, ' ')); }, 3000);

        }
        else {
            FrachiseId = GetQueryStringParamsForAgentRegistrationUpdate('UniqueId').replace(/%20/g, ' ').replace(null, "")
            $("#txt_AgencyName").val(GetQueryStringParamsForAgentRegistrationUpdate('Copmpany').replace(/%20/g, ' ').replace(null, ""));
            $("#txt_FirstName").val(GetQueryStringParamsForAgentRegistrationUpdate('Firstname').replace(/%20/g, ' ').replace(null, ""));
            $("#txt_LastName").val(GetQueryStringParamsForAgentRegistrationUpdate('LastName').replace(/%20/g, ' ').replace(null, ""));
            $("#txt_Designation").val(GetQueryStringParamsForAgentRegistrationUpdate('Designation').replace(/%20/g, ' ').replace(null, ""));
            $("#Select_Usertype").val("Franchisee")

        }

    }
    else if (location.href.indexOf('?') == -1) {
        $("#btn_RegisterSupplier").val('Register');
        setTimeout(function () { $('#selCountry option').filter(function () { return $.trim($(this).text()) == 'INDIA'; }).attr('selected', true); }, 1000);
        setTimeout(function () {
            GetCountryCity($("#selCountry").val());
        }, 2000);
    }



    function GetCountryCity(recCountry) {
        $.ajax({
            type: "POST",
            //url: "../DefaultHandler.asmx/GetCity",
            url: "GenralHandler.asmx/GetCountryCity",
            data: '{"country":"' + recCountry + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCity = result.CityList;
                    if (arrCity.length > 0) {
                        $("#selCity").empty();
                        var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                        for (i = 0; i < arrCity.length; i++) {
                            ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                        }
                        $("#selCity").append(ddlRequest);
                    }
                }
                if (result.retCode == 0) {
                    $("#selCity").empty();
                }
            },
            error: function () {
                Success("An error occured while loading cities")
            }
        });
        //$('#selCity option:selected').val(tempcitycode);
    }

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCountryCity(sndcountry);
    });
});

function CountryCity() {
    GetCountryCity($("#selCountry").val());
}

var hiddenID;
var hiddenMerchantID;
var hiddenValidity;
var hiddenValidationCode;
var hiddenAgentUniqueCode;
var hiddenParentID;
var hiddenLoginFlag;
function ValidateLogin() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    //var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');
    $("#formAgent label").css("display", "none");
    $("#formAgent label").html("* This field is required");
    //$("#txt_Email").css("border-color", "#ebebeb");
    //$("#txt_Mobile").css("border-color", "#ebebeb");
    var bValid = true;
    var sAgencyName = $("#txt_AgencyName").val();
    var sFirstName = $("#txt_FirstName").val();
    var sLastName = $("#txt_LastName").val();
    var sDesignation = $("#txt_Designation").val();
    var sAddress = $("#txt_Address").val();
    var bIATAStatus = $("#Select_IATA").val();
    var nIATA = $("#txt_IATA").val();
    var sCity = $("#selCity").val();
    var sCountry = $("#selCountry").val();
    var nPinCode = $("#txt_PinCode").val();
    var sEmail = $("#txt_Email").val();
    //var sPassword = $("#txt_Password").val();
    //var sConfirmPassword = $("#txt_ConfirmPassword").val();
    var nPhone = $("#txt_Phone").val();
    var nMobile = $("#txt_Mobile").val();
    var nFax = $("#txt_Fax").val();
    var sUsertype = "Agent";
    //var nRole = $("#Select_Usertype").val();
    var sServiceTaxNumber = $("#txt_ReferenceNumber").val();
    var nPAN = $("#txt_PAN").val();
    var sWebsite = $("#txt_Website").val();
    var sGroupId = $("#selGroup").val();
    var sGroupName = $("#selGroup option:selected").text();
    //var sVisaGroupId = $("#selVisaGroup").val();
    //var sVisaGroupName = $("#selVisaGroup option:selected").text();
    //var IsActive = $("#selIsActive").val();
    var sAgencyLogo = "";
    var Currency = $("#SelCurrency").val();
    var sRemarks = $("#txt_Remarks").val();
    var GSTNO = $("#txt_GSTNO").val();

    if (sAgencyName == "") {
        bValid = false;
        Success("Please Enter Agency Name");
        return;
    }

    if (sFirstName == "") {
        bValid = false;
        Success("Please Enter First Name");
        return;
    }
    if (sFirstName != "" && reg.test(sFirstName)) {
        bValid = false;
        Success("* First Name must not be numeric.");
        return;
    }
    if (sLastName == "") {
        bValid = false;
        Success("Please Enter Last Name");
        return;
    }
    if (sLastName != "" && reg.test(sLastName)) {
        bValid = false;
        Success("* Last Name must not be numeric.");
        return;
    }
    if (sDesignation == "") {
        bValid = false;
        Success("Please Enter Designation");
        return;
    }
    if (sDesignation != "" && reg.test(sDesignation)) {
        bValid = false;
        Success("* Designation must not be numeric at end.");
        return;
    }
    if (sEmail == "") {
        bValid = false;
        Success("Please Enter Email");
        return;
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            Success("* Wrong email format.");
            return;
        }
    }
    //if (sPassword == "") {
    //    bValid = false;
    //    $("#lbl_Password").css("display", "");
    //}
    //if (sConfirmPassword == "") {
    //    bValid = false;
    //    $("#lbl_ConfirmPassword").css("display", "");
    //}
    if (sAddress == "") {
        bValid = false;
        Success("Please Enter Address");
        return;
    }
    if (sAddress != "" && reg.test(sAddress)) {
        bValid = false;
        Success("* Address must not be numeric at end.");
        return;
    }
    if (bIATAStatus == "0") {
        bValid = false;
        Success("Please Select IATA STATUS");
        return;
    }
    if (bIATAStatus == "1" && nIATA == "") {
        bValid = false;
        Success("Please Enter IATA No");
        return;
    }
    if (bIATAStatus == "1" && nIATA != "" && !reg.test(nIATA)) {
        bValid = false;
        Success("* IATA no. must be numeric and valid.");
        return;
    }
    if (sCity == "-") {
        bValid = false;
        Success("Please Select City");
        return;
    }
    if (sCountry == "-") {
        bValid = false;
        Success("Please Select Country");
        return;
    }
    if (nMobile == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }
    //if (sUsertype == "") {
    //    bValid = false;
    //    Success("Please Select User Type");
    //}
    if (nFax == "") {
        nFax = '0';
    }
    else {
        if (!(reg.test(nFax))) {
            bValid = false;
            Success("* Fax no. must be numeric.");
            return;
        }
    }
    if (nPhone == "") {
        nPhone = '0';
    }
    else {
        if (!(reg.test(nPhone))) {
            bValid = false;
            Success("* Phone no must be numeric.");
            return;
        }
    }
    if (nPinCode == "") {
        nPinCode = '0';
    }
    else {
        if (!(reg.test(nPinCode))) {
            bValid = false;
            Success("* Pincode must be numeric.");
            return;
        }
    }
    if (nPAN == "") {
        nPAN = '0';
    }
    else {
        if (!(regPan.test(nPAN))) {
            bValid = false;
            Success("* PAN no must be correct.");
            return;
        }
    }
    //if (sServiceTaxNumber != "" && !reg.test(sServiceTaxNumber)) {
    //    bValid = false;
    //    $("#lbl_IATA").html("* Service Tax no. must be numeric.");
    //    $("#lbl_IATA").css("display", "");
    //}
    //if (sPassword != sConfirmPassword) {
    //    bValid = false;
    //    $("#lbl_ConfirmPassword").html("Password & Confirm password<br /> must be same");
    //    $("#lbl_ConfirmPassword").css("display", "");
    //}
    if (sGroupId == "-" || sGroupName == "") {
        bValid = false;
        Success("Please Select Group");
        return;
    }
    if (Currency == "0") {
        bValid = false;
        Success("Please Select Currency");
        return;
    }
    if (FrachiseId == undefined) {
        FrachiseId = "";
    }
    //if (sVisaGroupId == "-")
    //{
    //    bValid = false;
    //    $("#lbl_VisaGroup").css("display", "");

    //}
    if (bValid == true) {
        if (bIATAStatus != "1") {
            nIATA = "";
        }
        if ($("#btn_RegisterSupplier").val() == "Update") {
            $.ajax({
                type: "POST",
                url: "HotelAdmin/Handler/GenralHandler.asmx/UpdateRegisteredAgent",
                //data: '{"sid":"' + hiddenID + '","sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sPassword":"' + sPassword + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","hiddenAgentUniqueCode":"' + hiddenAgentUniqueCode + '","hiddenLoginFlag":"' + hiddenLoginFlag + '","sbuttonvalue":"UPDATE"}',
                // data: '{"sid":"' + hiddenID + '","sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sVisaGroupId":"' + sVisaGroupId + '","sVisaGroupName":"' + sVisaGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","hiddenAgentUniqueCode":"' + hiddenAgentUniqueCode + '","hiddenLoginFlag":"' + hiddenLoginFlag + '","sbuttonvalue":"UPDATE"}',
                data: '{"sid":"' + hiddenID + '","sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","hiddenAgentUniqueCode":"' + hiddenAgentUniqueCode + '","hiddenLoginFlag":"' + hiddenLoginFlag + '","sbuttonvalue":"UPDATE","Currency":"' + Currency + '","FranchiseeId":"' + FrachiseId + '","GSTNO":"' + GSTNO + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "AgentDetails.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Agent Updated successfully")
                        setTimeout(function () {
                            window.location.href = "AgentDetails.aspx";
                        }, 2000);
                    }
                    else {
                        Success("Something went wrong!")
                    }
                },
                error: function () {
                }
            });
        }
        else if ($("#btn_RegisterSupplier").val() == "Register") {
            //Success(sEmail)
            $.ajax({
                type: "POST",
                url: "HotelAdmin/Handler/GenralHandler.asmx/CheckForUniqueAgent",
                data: '{"sEmail":"' + sEmail + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Login.aspx";
                        //  window.location.href = "Default.aspx";
                        //  window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("This email/username is already taken, Please choose another.")
                        //Success("This email/username is already taken, Please choose another.");
                        $("#txt_Email").css("border-color", "brown");
                        $("#txt_Email").focus();
                        //$("#txt_Mobile").css("border-color", "brown");
                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Their is some error in processing your request, Please try again")
                        //Success("Their is some error in processing your request, Please try again");
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $.ajax({
                            type: "POST",
                            url: "HotelAdmin/Handler/GenralHandler.asmx/AddAgent",
                            //data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sPassword":"' + sPassword + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","bIATAStatus":"' + bIATAStatus + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER"}',
                            // data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sVisaGroupId":"' + sVisaGroupId + '","sVisaGroupName":"' + sVisaGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER"}',
                            data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER","Currency":"' + Currency + '","FranchiseeId":"' + FrachiseId + '","GSTNO":"' + GSTNO + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session == 0) {
                                    window.location.href = "HotelAdmin/Dashboard.aspx";
                                    //                                window.location.href = "../CUT/Default.aspx";
                                    return false;
                                }
                                if (result.retCode == 1) {
                                    //Success("Agent registered successfully. A mail containing password has been sent to registered mail. Now you can activate the agent/Upload the logo.");
                                    //Success("Agent registered successfully. A mail containing password has been sent to registered mail. Now you can activate the agent/Upload the logo.")
                                    Success("Agent registered successfully. A mail containing password has been sent to registered mail.")
                                    setTimeout(function () {
                                        window.location.href = "AgentDetails.aspx";
                                    }, 2000);
                                }
                                else if (result.retCode == 0) {
                                    //Success("Something went wrong! Please contact administrator.");
                                    Success("Something went wrong!")
                                }
                            },
                            error: function () {
                                Success("An error occured during registration!")
                                //Success("An error occured during registration! Please contact administrator.");
                            }
                        });
                    }
                },
                error: function () {
                }
            });
        }
    }
}
function LogoUpload() {
    $("#btn_UploadImage").css("display", "")
    $("#ConformModal").modal("hide")
    //window.location.href = "Registration2.aspx";
}

function ShowImageModal() {
    $("#LogoUploadModal").modal("show")
}

var arrGroup = new Array();

function GetGroups() {
    $.ajax({
        type: "POST",
        //url: "HotelAdmin/Handler/GenralHandler.asmx/GetGroups",
        url: "../handler/MarkUpHandler.asmx/GetGroup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGroup = result.Arr;
                if (arrGroup.length > 0) {
                    $('#selGroup').empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Group</option>';
                    for (i = 0; i < arrGroup.length; i++) {
                        ddlRequest += '<option value="' + arrGroup[i].sid + '">' + arrGroup[i].GroupName + '</option>';
                    }
                    $("#selGroup").append(ddlRequest);
                    if (s != "") {
                        $("#selGroup").val(s)
                    }
                }
            }
        },
        error: function () {
        }
    });
}

function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#selCountry").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function AppendUserType(ut) {
    try {
        var checkclass = document.getElementsByClassName('check');
        $("#UserType .select span")[0].textContent = ut;
    }
    catch (ex)
    { }
}

function AppendCurrency(ut) {
    try {
        var checkclass = document.getElementsByClassName('check');
        $("#Currency .select span")[0].textContent = ut;
        for (var i = 0; i <= ut.length; i++) {
            $('input[value="' + ut + '"][class="currency"]').prop("selected", true);
            $("#SelCurrency").val(ut);
        }
    }
    catch (ex)
    { }
}

function AppendCity(ut) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var CityCode = $.grep(arrCity, function (p) { return p.Description == ut; }).map(function (p) { return p.Code; });

        $("#City .select span")[0].textContent = ut;
        for (var i = 0; i <= CityCode.length; i++) {
            $('input[value="' + CityCode + '"][class="country"]').prop("selected", true);
            $("#selCity").val(CityCode);
        }
    }
    catch (ex)
    { }
}

function AppendGroup(ut) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var CityCode = $.grep(arrGroup, function (p) { return p.sid == ut; }).map(function (p) { return p.GroupName; });

        $("#Divgroup .select span")[0].textContent = CityCode;
        for (var i = 0; i <= ut.length; i++) {
            $('input[value="' + ut + '"][class="Divgroup"]').prop("selected", true);
            $("#selGroup").val(ut);
        }
    }
    catch (ex)
    { }
}