﻿/// <reference path="E:\Rumana\ClickUrHotel\CutAdmin\CutAdmin\handler/CityAddUpdateHandler.asmx" />
$(document).ready(function () {
    SearchCity();
});

// newCity modal
function newCity() {
    GetCountryCity();
    $.modal({
        content: '<div class="columns">' +
'            <div class="twelve-columns  twelve-columns-mobile six-columns-tablet"  id="DivCountry">   ' +
'                        <label>Country<span style="color:red;">*</span> </label>                      ' +
'<br>                                                                          ' +
'<div class="full-width button-height">                                        ' +
'    <select id="selCountrynew" class="select OfferType">                      ' +
'        <option selected="selected" value="-">Select Any Country</option>     ' +
'    </select>                                                                 ' +
'</div>                                                                        ' +
'</div>                                                                            ' +
'<div class="twelve-columns twelve-columns-mobile"><label>City: </label><br/> <div class="input full-width">' +
'<input name="prompt-value"  id="txt_City" value="" class="input-unstyled full-width"  placeholder="City Name" type="text">' +
'</div>' +
'</br><p class="text-alignright"><button type="submit" onclick="addCity()" id="btn_AddCity" class="button anthracite-gradient">ADD</button></p>',

        title: 'Add City',
        width: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
var arrCountry, arrCountryCity;
function GetCountryCity() {
    //SearchCity();
    //$("#tbl_MasterCity").dataTable().fnClearTable();
    //$("#tbl_MasterCity").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/CityAddUpdateHandler.asmx/GetCountryCity",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                //var tr = '';
                //for (i = 0; i < arrCountry.length; i++) {
                //    tr += '<tr>';

                //    tr += '<td style="width:5%">' + (i + 1) + '</td>';
                //    tr += '<th scope="row">' + List_City[i].Description + '</th>';
                //    tr += '<th scope="row">' + List_City[i].Countryname + '</th>';
                //    tr += '</tr>'; 
                //}
                //$("#tbl_MasterCity tbody").append(tr);
                //$("#tbl_MasterCity").dataTable({
                //    bSort: false, sPaginationType: 'full_numbers',
                //});
                //document.getElementById("tbl_MasterCity").removeAttribute("style")
                if (arrCountry.length > 0) {
                    $("#selCountrynew").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountrynew").append(ddlRequest);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}
function OpenDilogBox() {
    $("#selCountrynew").val("");
    $("#selCountrynew").val("-");
    $("#AddCityModal").modal("show")
}
function addCity() {
    var bValid = true;
    var CountryCode = $("#selCountrynew").val();
    var CountryName = $("#selCountrynew option:selected").text();
    var CityName = $("#txt_City").val();
    if (CityName == "") {
        Success("Please Insert City Name")
        bValid = false;
        $("#txt_City").focus()
    }
    if (CountryCode == "-") {
        Success("Please Select Country")
        bValid = false;
        $("#selCountrynew").focus()
    }
   
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/CityAddUpdateHandler.asmx/AddCity",
            data: '{"CountryCode":"' + CountryCode + '","CountryName":"' + CountryName + '","CityName":"' + CityName + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("City Added Successfully")
                  
                    setTimeout(function () {
                        window.location.reload();
                        GetCountryCity()
                    }, 2000);
                }
                if (result.retCode == 0) {
                    Success("An error occured while adding City.")

                }
            }
        })
    }

}
function SearchCity() {
    
    $.ajax({
        type: "POST",
        url: "../Handler/CityAddUpdateHandler.asmx/SearchCity",
        data: '{"SearchCity":"' + $("#txt_SeachCity").val() + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            $("#tbl_MasterCity").dataTable().fnClearTable();
            $("#tbl_MasterCity").dataTable().fnDestroy();
            if (result.retCode == 1) {
                List_City = result.Arr;
                //$("#tbl_MasterCity").empty();
                var tRow = "";

                var input = "\/Date(1458845940000)\/";
                for (var i = 0; i < List_City.length; i++) {

                    tRow += '<tr>';

                    tRow += '<td style="width:5%">' + (i + 1) + '</td>';
                    tRow += '<th scope="row">' + List_City[i].Description + '</th>';
                    tRow += '<th scope="row">' + List_City[i].Countryname + '</th>';
                    tRow += '</tr>';
                }
                $("#tbl_MasterCity tbody").html(tRow);
                setTimeout(function () {
                    $("#tbl_MasterCity").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                },1000);
                
            }
            else if (result.retCode == 0) {
                $("#tbl_MasterCity tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_MasterCity").append(tRow);

                $("#tbl_MasterCity").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }

        },
        error: function () {
        }

    });
}

function SearchCity2() {
    $("#tbl_MasterCity").dataTable().fnClearTable();
    $("#tbl_MasterCity").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/CityAddUpdateHandler.asmx/SearchCity",
        data: '{"SearchCity":"' + $("#txt_SeachCity").val() + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_City = result.Arr;
                //$("#tbl_MasterCity").empty();
                var tRow = "";

                var input = "\/Date(1458845940000)\/";
                for (var i = 0; i < List_City.length; i++) {

                    tRow += '<tr>';

                    tRow += '<td style="width:5%">' + (i + 1) + '</td>';
                    tRow += '<th scope="row">' + List_City[i].Description + '</th>';
                    tRow += '<th scope="row">' + List_City[i].Countryname + '</th>';
                    tRow += '</tr>';
                }
                $("#tbl_MasterCity tbody").html(tRow);
                setTimeout(function () {
                    $("#tbl_MasterCity").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }, 1000);

            }
            else if (result.retCode == 0) {
                $("#tbl_MasterCity tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_MasterCity").append(tRow);

                $("#tbl_MasterCity").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }

        },
        error: function () {
        }

    });
}
//function SearchCity() {
//    $("#tbl_MasterCity tbody tr").remove();
//    $.ajax({
//        type: "POST",
//        url: "CityAddUpdateHandler.asmx/SearchCity",
//        data: '{"SearchCity":"' + $("#txt_SeachCity").val() + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                arrCountryCity = result.tbl_HCity;
//                if (arrCountryCity.length > 0) {

//                    var tr = '';
//                    for (i = 0; i < arrCountryCity.length; i++) {
//                        tr += '<tr><td style="width: 5%">' + (i + 1) + '</td>'
//                        tr += '<td>' + arrCountryCity[i].Description + '</td>'
//                        tr += '<td>' + arrCountryCity[i].Countryname + '</td>'
//                        //tr += '<td><a style="">Edit</a></td></tr>'
//                    }
//                    $("#tbl_MasterCity tbody").append(tr);
//                }
//            }
//        }
//    })
//}