﻿var HotelCode = 0, HotelName = '', RoomId = 0, RoomType = '';
var RoomDetails = [], arrRoomTypes = [], arrRoomList = [], arrSuppliers = [], arrCancelationPolicies = [], arrMasterOffer = [];

$('.onlyNumbersText').find('input').on('keypress', function () {
    return event.charCode >= 48 && event.charCode <= 57;
})

$(function () {
    HotelCode = getParameterByName('sHotelID');
    HotelName = getParameterByName('HotelName');
    $("#lblHotel").text(HotelName);
    GetSuppliers();
    GetCountry();

    GetCurrencyy();
    GetMealPlans();
    DateRateValidity(0);
    getRooms();
    document.getElementById('chkAllDays0').click();
    document.getElementById('chkAllSD0').click();
})

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}

function GetALLOffer() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetALLOffer",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var ALLOffer = result.ALLOffer;
                if (ALLOffer.length > 0) {
                    for (i = 0; i < ALLOffer.length; i++) {
                        ddlRequest += '<option value="' + ALLOffer[i].OfferID + '">' + ALLOffer[i].SeasonName + '</option>';
                    }
                    $(".sel_OfferTyp select").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function CheckNationality(currentval) {
    if ($("#sel_Nationality option[value='All']").is(':selected')) {
        selectAll();
    }
    else if ($("#sel_Nationality option[value='Unselect']").is(':selected')) {
        UnselectAll();
    }
}

function selectAll() {
    $('#sel_Nationality option').prop('selected', true);
    $("#sel_Nationality option[value='All']").click();
}

function UnselectAll() {
    $('#sel_Nationality option').prop('selected', false);
    $("#sel_Nationality option[value='All']").click();
}

function GetCancelationPolicies() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelationPolicies",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.CancelationPolicies;
                // $(".cCancelationPolicies select").empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {
                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }
                    $(".cCancelationPolicies select").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}


function DateRateValidity(id) {
    if (id == 0) {
        $("#txtFromRV" + id).datepicker($.extend({
            minDate: 0,
            onSelect: function () {
                var minDate = $(this).datepicker('getDate');
                minDate.setDate(minDate.getDate() + 1); //add One days
                $("#txtToRV" + id).datepicker("option", "minDate", minDate);
                //UpdateFlag = false;
                ValidateCommon(id);
            }
        }, datepickersOpt));

        $("#txtToRV" + id).datepicker($.extend({
            onSelect: function () {
                var maxDate = $(this).datepicker('getDate');
                maxDate.setDate(maxDate.getDate());
                UpdateFlag = false;
                ChkAvlDate("GN", id);
            }
        }, datepickersOpt));
    }
    else {
        var previous = moment($("#txtToRV" + (id - 1)).val(), "DD-MM-YYYY");
        $("#txtFromRV" + id).datepicker($.extend({
            minDate: previous._i,
            onSelect: function () {
                var minDate = $(this).datepicker('getDate');
                minDate.setDate(minDate.getDate() + 1); //add One days
                $("#txtToRV" + id).datepicker("option", "minDate", minDate);
                //UpdateFlag = false;
                ValidateCommon(id);
            }
        }, datepickersOpt));
        $("#txtToRV" + id).datepicker($.extend({
            onSelect: function () {
                var maxDate = $(this).datepicker('getDate');
                maxDate.setDate(maxDate.getDate());
                UpdateFlag = false;
                ChkAvlDate("GN", id);
            }
        }, datepickersOpt));
    }
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function DateSpecialDates(id) {
    var cFromRVDate = $(".cRateValidFrom");
    var cToRVDate = $(".cRateValidTo");

    count = cFromRVDate.length - 1;
    var previousFrom = moment(cFromRVDate[0].value, "DD-MM-YYYY");

    count1 = cToRVDate.length - 1;
    var previousTo = moment(cToRVDate[count1].value, "DD-MM-YYYY");

    $("#txtFromSD" + id).datepicker($.extend({
        minDate: previousFrom._i,
        maxDate: previousTo._i,
        onSelect: function () {
            var minDate = $(this).datepicker('getDate');
            minDate.setDate(minDate.getDate() + 1);
            $("#txtToSD" + id).datepicker("option", "minDate", minDate);
            //UpdateFlag = false;
        }
    }, datepickersOpt));

    $("#txtToSD" + id).datepicker($.extend({
        maxDate: previousTo._i,
        onSelect: function () {
            var maxDate = $(this).datepicker('getDate');
            maxDate.setDate(maxDate.getDate());
            UpdateFlag = false;
            ChkAvlDate("SL", id)
        }
    }, datepickersOpt));
}

var arrMealPlans = [];
function GetMealPlans() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetMealPlans",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            arrMealPlans = result.MealPlanList;
            if (result.retCode == 1) {
                for (i = 0; i < arrMealPlans.length; i++) {
                    ddlRequest += '<option value="' + arrMealPlans[i].MealPlanShort + '">' + arrMealPlans[i].MealPlanName + '</option>';
                }
            }
            else {
                ddlRequest += '<option value=""  disabled>Not Available</option>';
            }
            $("#sel_MealPlan").append(ddlRequest);
        },
        error: function () {
        }
    });
}

function GetCurrencyy() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCurrency",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCurrency = result.CurrencyList;
                if (arrCurrency.length > 0) {
                    for (i = 0; i < arrCurrency.length; i++) {
                        if (arrCurrency[i].CurrencyCode == "SAR") {
                            ddlRequest += '<option selected="selected" value="' + arrCurrency[i].CurrencyCode + '">' + arrCurrency[i].CurrencyCode + '</option>';
                            $("#Currency .select span")[0].textContent = "SAR";
                        } else {
                            ddlRequest += '<option value="' + arrCurrency[i].CurrencyCode + '">' + arrCurrency[i].CurrencyCode + '</option>';
                        }
                    }
                    $("#sel_CurrencyCode").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetCountry() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    ddlRequest += ' <option class="optCountry" value="AllCountry">For All Nationality</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        if (arrCountry[i].Country != "AllCountry")
                            ddlRequest += '<option class="optCountry" value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    ddlRequest += ' <option value="Unselect" style="display:none"inline-block;" disabled>Unselect All</option>';
                    $("#sel_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetSuppliers() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetSuppliers",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSuppliers = result.SupplierList;
                if (arrSuppliers.length > 0) {
                    //ddlRequest += ' <option value="25" selected="selected">From Hotel</option>';
                    for (i = 0; i < arrSuppliers.length; i++) {
                        if (arrSuppliers[i].sid != '25')
                            ddlRequest += '<option value="' + arrSuppliers[i].sid + '">' + arrSuppliers[i].Supplier + '</option>';
                    }
                    $("#sel_Supplier").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function getRooms() {
    var sHotelId = HotelCode

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + sHotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;

            }

        }

    });
}

function AddDates() {

    $("#TabRooms").empty();
    $("#TabRoomsSpl").empty();
    $("#TabContent").empty();
    $("#TabContentSpl").empty();


    if (UpdateFlag == true) {
        ConfirmModel(Rate);
    }
    else {
        $("#DivDates")[0].style.cssText = "display:";
        //$(".maintab").empty();
        //$("#TabRoomsSpl").empty();
        var tabs = '';
        var tabContent = '';
        //tabs += '<ul class="tabs">';
        //tabContent += '<div class="tabs-content" id="tabContentID">';
        for (var i = 0; i < arrRoomList.length; i++) {
            for (var j = 0; j < arrRoomTypes.length; j++) {
                if (arrRoomList[i].RoomTypeID == arrRoomTypes[j].RoomTypeID) {
                    if (i == 0) {

                        tabs += '<li class="active"><a href="#tabRoomtype' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';

                    }
                    else {

                        tabs += '<li><a href="#tabRoomtype' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';
                    }
                }
            }
        }
        // tabs += '</ul>';
        $("#TabRooms").append(tabs);


        var ChkSD = $("#chkSpecialDate");
        if (ChkSD.is(":checked")) {
            var SDtabs = '';
            //SDtabs += '<ul class="tabs">';
            for (var i = 0; i < arrRoomList.length; i++) {
                for (var j = 0; j < arrRoomTypes.length; j++) {
                    if (arrRoomList[i].RoomTypeID == arrRoomTypes[j].RoomTypeID) {
                        if (i == 0) {

                            SDtabs += '<li class="active"><a href="#tabRoomtypeSD' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';

                        }
                        else {

                            SDtabs += '<li><a href="#tabRoomtypeSD' + i + '">' + arrRoomTypes[j].RoomType + '</a></li>';
                        }
                    }
                }
            }
            //SDtabs += '</ul>';
            $("#TabRoomsSpl").append(SDtabs);
        }



        //var supl = $("#lbl_Supplier").text();

        $(".divContents").empty();
        $(".divContentsSD").empty();
        $(".NestesDiv").empty();
        $(".NestesDivSD").empty();


        AddRVDates();
        setTimeout(function () {
            AddSDDates();
        }, 1000);

        var ValidFromRV = $('.cRateValidFrom')
        var ValidFromSD = $('.ctxtfromSD')

        var ValidToRV = $('.cRateValidTo')
        var ValidToSD = $('.ctxttoSD')


        setTimeout(function () {
            if (DateRangeRV.length == 0) {
                Success("Please Select Date Range");
                return false
            }
            else {

                var trRoomsRV = '';
                var tabContent = "";
                ///////////////////NEW WORK //////////////////

                for (var l = 0; l < arrRoomList.length; l++) {

                    tabContent += '<div id="tabRoomtype' + l + '" class="tabs-content with-padding">';
                    tabContent += '<div id="maintab-1">';
                    tabContent += '<div class="standard-tabs  margin-bottom" id="add-tabs">';
                    tabContent += '<ul class="tabs">';
                    for (var m = 0; m < DateRangeRV.length; m++) {
                        if (m == 0) {
                            tabContent += '<li class="active"><a href="#tabDate' + l + '_' + m + '" id="ClicktabDate' + l + '_' + m + '">' + DateRangeRV[m] + '</a></li>';
                        }
                        else {
                            tabContent += '<li><a href="#tabDate' + l + '_' + m + '" id="ClicktabDate' + l + '_' + m + '">' + DateRangeRV[m] + '</a></li>';
                        }
                    }
                    tabContent += '</ul>';

                    tabContent += '<div class="tabs-content">';
                    for (var n = 0; n < DateRangeRV.length; n++) {
                        tabContent += '<div id="tabDate' + l + '_' + n + '" class="tab-active divContents">';
                        tabContent += '</div>';
                    }
                    tabContent += '</div>';  //tabs-content

                    tabContent += '</div>';/// UP
                    tabContent += '</div>';/// UP
                    tabContent += '</div>';

                }
                $("#TabContent").append(tabContent);

                for (var j = 0; j < arrRoomList.length; j++) {
                    for (var i = 0; i < DateRangeRV.length; i++) {

                        trRoomsRV = '';
                        trRoomsRV += '<div class="tabs-content">';
                        trRoomsRV += '<div class="with-padding">';
                        trRoomsRV += '<div class="row block table-head margin-bottom">';

                        trRoomsRV += '<span class="block-title anthracite-gradient glossy"><h3>ROOM VALIDITY</h3>';
                        //trRoomsRV += '<small>' + DateRangeRV[i] + '(' + DaysRV + ')&nbsp;<strong>|</strong>&nbsp;</small>';
                        //  trRoomsRV += '<label><strong>(</strong>' + DateRangeRV[i] + '<strong>)</strong>,</small>';
                        trRoomsRV += '  <div class="clear"></div>';
                        trRoomsRV += '    </span>';

                        trRoomsRV += '<div class="with-padding withpad tabwhite" id="RoomContentRV' + j + '_' + i + '_' + '0' + '"  >';
                        //Inputs in RV 
                        trRoomsRV += ' <div class="columns roomrate" id="RoomRatesByRoom' + j + '_' + i + '_' + '0' + '">';
                        trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Room Rate(RR):<span class="red">*</span></label><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtRoomRate' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"> </div>';
                        trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Extra Bed(EB):<span class="red">*</span></label> <input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtExtraBed' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"> </div>';
                        trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CWB:<span class="red">*</span></label><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildWithBed' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div>';
                        trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CNB:<span class="red">*</span></label><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildNoBed' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div>';
                        trRoomsRV += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Booking Code:</label><input type="text" id="txtBookingCode' + j + '_' + i + '_' + '0' + '" class="input full-width"></div>';
                        // trRoomsRV += '  <div class="new-row three-columns"><small class="input-info">Cancellation Policy:</small> <select id="sel_CancellationPolicyId' + j + '" class="select cCancelationPolicies" onchange="AddCancelations(this.id,' + j + ');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPolicies' + j + '" class="full-width sCanceltionPolicies"> </ul></div>';
                        trRoomsRV += '  <div class="four-columns four-columns-tablet six-columns-mobile"><input type="checkbox" id="chkRVforDaywise' + j + '_' + i + '_' + '0' + '" onchange="CheckRVforDaywise(' + j + ',' + i + ',' + '0' + ')" /><label for="chkRVforDaywise' + j + '_' + i + '_' + '0' + '" class="label">Day Wise Rates</label></div>';
                        //trRoomsRV += ' <div class="two-columns twelve-columns-mobile"><label>Min Stay Rate:<span class="red">*</span></label></div>';
                        //trRoomsRV += '<div class="two-columns twelve-columns-mobile"><div class="full-width button-height SelBox"><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtMinRate' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div></div>'
                        // trRoomsRV += '<div class="two-columns twelve-columns-mobile"><div class="full-width button-height SelBox"><select id="NightsRates' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_NightsRates"><option value="0" selected="selected">0</option><option value="1" >1Night</option><option value="2">2Nights</option><option value="3">3Nights</option><option value="4">4Nights</option><option value="5">5Nights</option><option value="6">6Nights</option><option value="7">7Nights</option><option value="8">8Nights</option><option value="9">9Nights</option><option value="10">10Nights</option></select></div></div>'
                        trRoomsRV += '</div>';
                        trRoomsRV += '   <div class="columns" id="RoomRatesByDayRV' + j + '_' + i + '_' + '0' + '" style="display:none">';
                        //Day wise Rates         

                        for (var wr = 0; wr < WeekRateType.length; wr++) {

                            // trRoomsRV += '<div  class="columns">';
                            trRoomsRV += '<div class="one-columns"><label>' + WeekRateType[wr] + '</label></div>';
                            trRoomsRV += ''
                            for (var wd = 0; wd < WeekDaysRV.length; wd++) {
                                trRoomsRV += '<div class="one-columns"><small>' + WeekDaysRV[wd] + '</small><br/><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtRV' + WeekDaysRV[wd] + j + '_' + i + '_' + '0' + wr + '" class="input cWeekDaysRV" placeholder="' + WeekDaysRV[wd] + '" style="width:40px;"></div>';
                            }

                            //trRoomsRV += '</div>';
                        }
                        trRoomsRV += '</div>';


                        trRoomsRV += '<div  class="columns">';

                        trRoomsRV += '  <div class="three-columns  twelve-columns-mobile"><label>Offer:</label><div class="full-width button-height SelBox"><select id="sel_OfferType' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_OfferTyp"  onchange="AddOffer(this.id,\'' + j + '_' + i + '_' + '0' + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iOffer' + j + '_' + i + '_' + '0' + '" class="full-width sel_OfferTyp"></ul></div></div>';

                        trRoomsRV += ' <div class="two-columns twelve-columns-mobile"><label>Offer Nights:</label><div class="full-width button-height SelBox"><select id="OfferNights' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_Offernight"><option value="0" selected="selected">0</option><option value="1" >1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option></select></div></div>';

                        trRoomsRV += '  <div class="four-columns  twelve-columns-mobile"><label>Cancellation Policy:</label> <div id="ButtonSearch" style="height: 20px">  <span class="button-group">'
                        trRoomsRV += ' <input type="button" id="chkCancellationpolicyNonRefundable' + j + '_' + i + '_' + '0' + '" class="button black-gradient" value="NonRefundable"  onclick="CancellationpolicyRV(\'' + 'NonRefundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')">'
                        trRoomsRV += ' <input type="button"  id="chkCancellationpolicyRefundable' + j + '_' + i + '_' + '0' + '" class="button white-gradient" value="Refundable" onclick="CancellationpolicyRV(\'' + 'Refundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')"></span></div>';
                        trRoomsRV += ' </div>';
                        //trRoomsRV += '  <label for="SearchType1" class="button grey-active">'
                        //trRoomsRV += '      <input type="radio" id="chkCancellationpolicyNonRefundable' + j + '_' + i + '_' + '0' + '" checked name="button-radio SearchType" value="General" onclick="CancellationpolicyRV(\'' + 'NonRefundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')">'
                        //trRoomsRV += '      NonRefundable'
                        //trRoomsRV += '  </label>'
                        //trRoomsRV += ' <label for="SearchType2" class="button grey-active">'
                        //trRoomsRV += '     <input type="radio" name="button-radio SearchType" id="chkCancellationpolicyRefundable' + j + '_' + i + '_' + '0' + '" value="Advance" onclick="CancellationpolicyRV(\'' + 'Refundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')">'
                        //trRoomsRV += '    Refundable'
                        //trRoomsRV += ' </label>'

                        trRoomsRV += '  <div id="divTaxRV' + j + '_' + i + '_' + '0' + '" class="three-columns twelve-columns-mobile" style="display:none" none"><label>Select Policy:</label><div class="full-width button-height SelBox"> <select id="sel_CancellationPolicyId' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select cCancelationPolicies" onchange="AddCancelations(this.id,\'' + j + '_' + i + '_' + '0' + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPolicies' + j + '_' + i + '_' + '0' + '" class="full-width sCanceltionPolicies"></ul></div></div>';
                        trRoomsRV += ' </div>';


                        trRoomsRV += '<div  class="columns">';

                        trRoomsRV += '  <div id="Type' + j + '_' + i + '_' + '0' + '" class="four-columns twelve-columns-mobile"><label>Rate Type:</label><div class="full-width button-height SelBox"><select id="sel_Type' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width"><option value="Contracted" selected="selected">Contracted</option><option value="Promo">Promo</option><option value="Tactical">Tactical</option></select></div></div>';

                        trRoomsRV += '  <div class="eight-columns twelve-columns-mobile"><label>Rate Note:</label><textarea type="text" id="txtRateNote' + j + '_' + i + '_' + '0' + '" col="6" class="input full-width autoexpanding"> </textarea></div>';

                        trRoomsRV += '</div>';


                        /*Meal Plan*/
                        //trRoomsRV += GenrateStaticMeal('Nor_' + i + '_' + arrRoomList[j].RoomTypeID);
                        debugger
                        trRoomsRV += '<div  class="columns">';
                        trRoomsRV += '  <div class="twelve-columns">';
                        trRoomsRV += ' <label class="input-info">Meals Rate</label><hr/>'
                        trRoomsRV += '</div>'
                        trRoomsRV += '</div>'

                        trRoomsRV += '<div  class="columns">';
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">Meal</label></div>'
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">Adult Rate</label></div>'
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">CWB Rate</label></div>'
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">CNB Rate</label></div>'
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">Type</label></div>'
                        trRoomsRV += '  <div class="two-columns"><label class="input-info">Compulsary</label></div>'
                        trRoomsRV += '</div>'


                        trRoomsRV += '<div class="AddMeal' + 'Nor_' + i + '_' + arrRoomList[j].RoomTypeID + '"></div>';
                        //End Inputs in RV      
                        var arrDays = [];
                        arrDays = DaysRV;

                        trRoomsRV += '<div  class="columns">';
                        trRoomsRV += '<div class="twelve-columns">'
                        trRoomsRV += '<label class="input-info">AddOns Rate</label><hr/>'
                        trRoomsRV += '</div>'
                        trRoomsRV += '</div>'

                        trRoomsRV += '<div id="AddOns' + j + '_' + i + '_' + '0' + arrRoomList[j].RoomTypeID + '">';
                        GetAddDetails(j, "Nor_" + i + '_' + arrRoomList[j].RoomTypeID, 'AddOns' + j + '_' + i + '_' + '0' + arrRoomList[j].RoomTypeID)
                        trRoomsRV += '</div>';

                        trRoomsRV += '</div>';
                        trRoomsRV += '</div>';
                        trRoomsRV += ' </div>';
                        trRoomsRV += ' </div>';


                        $("#tabDate" + j + '_' + i).append(trRoomsRV);

                        $("#ClicktabDate" + j + '_' + i).click();


                    }

                }

            }



            /////// Tab For Special Dates////////
            var ChkSD = $("#chkSpecialDate");

            if (ChkSD.is(":checked")) {
                if (DateRangeSD.length == 0) {
                    Success("Please Select Special Date Range");
                    return false
                }
                else {
                    var trRoomsSD = '';
                    var tabContentSD = "";
                    for (var l = 0; l < arrRoomList.length; l++) {

                        tabContentSD += '<div id="tabRoomtypeSD' + l + '" class="with-padding" >';
                        tabContentSD += '<div class="standard-tabs same-height inner-tabs">';
                        tabContentSD += '<ul class="tabs">';
                        for (var m = 0; m < DateRangeSD.length; m++) {
                            if (m == 0) {
                                tabContentSD += '<li class="active"><a href="#tabDateSD' + l + '_' + m + '" id="ClicktabDateSD' + l + '_' + m + '">' + DateRangeSD[m] + '</a></li>';
                            }
                            else {
                                tabContentSD += '<li><a href="#tabDateSD' + l + '_' + m + '" id="ClicktabDateSD' + l + '_' + m + '">' + DateRangeSD[m] + '</a></li>';
                            }
                        }
                        tabContentSD += '</ul>';

                        tabContentSD += '<div class="tabs-content">';
                        for (var n = 0; n < DateRangeSD.length; n++) {

                            tabContentSD += '<div id="tabDateSD' + l + '_' + n + '" class="tab-active divContentsSD" >';
                            tabContentSD += '</div>';

                        }
                        tabContentSD += '</div>';  //tabs-content

                        tabContentSD += '</div>';/// UP
                        tabContentSD += '</div>';/// UP

                    }
                    $("#TabContentSpl").append(tabContentSD);

                    trRoomsSD = '';
                    //trRoomsSD += '<div class="tabs-content">';
                    //trRoomsSD += '<div class="with-padding">';
                    //trRoomsSD += '<div class="row block table-head margin-bottom">';
                    //trRoomsSD += '<span class="block-title anthracite-gradient glossy"><h3>SPECIAL DATES</h3>';
                    for (var j = 0; j < arrRoomList.length; j++) {
                        for (var i = 0; i < DateRangeSD.length; i++) {
                            trRoomsSD = '';
                            trRoomsSD += '<div class="tabs-content">';
                            trRoomsSD += '<div class="with-padding">';
                            trRoomsSD += '<div class="row block table-head margin-bottom">';
                            trRoomsSD += '<span class="block-title anthracite-gradient glossy"><h3>SPECIAL DATES</h3>';
                            // trRoomsSD += '<small><strong>(</strong>' + DateRangeSD[i] + '<strong>)</strong>,</small>';
                            trRoomsSD += '  <div class="clear"></div>';
                            trRoomsSD += '    </span>';


                            trRoomsSD += '<div class="with-padding withpad tabwhite" id="RoomContentSD' + j + '_' + i + '_' + '0' + '" >';
                            //Inputs in SD          
                            trRoomsSD += ' <div class="columns">';
                            trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Room Rate(RR):</label><input type="text" min="0"  onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtRoomRateSD' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"> </div>';
                            trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Extra Bed(EB):</label> <input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtExtraBedSD' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"> </div>';
                            trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CWB:</label><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildWithBedSD' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div>';
                            trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>CNB:</label><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtChildNoBedSD' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div>';
                            trRoomsSD += '  <div class="two-columns four-columns-tablet six-columns-mobile"><label>Booking Code:</label><input type="text"  id="txtBookingCodeSD' + j + '_' + i + '_' + '0' + '" class="input full-width"></div>';
                            //trRoomsSD += '  <div class="new-row three-columns"><small class="input-info">Cancellation Policy:</small> <select id="sel_CancellationPolicyIdSD' + j + '" name="validation-select" class="select full-width cCancelationPolicies" onchange="AddCancelationsSD(this.id,' + j + ');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPoliciesSD' + j + '" class="full-width sCanceltionPoliciesSD"> </ul> </div>';
                            trRoomsSD += '  <div class="four-columns four-columns-tablet six-columns-mobile"><input type="checkbox" id="chkSDforDaywise' + j + '_' + i + '_' + '0' + '" onchange="CheckSDforDaywise(' + j + ',' + i + ',' + '0' + ')" /><label for="chkSDforDaywise' + j + '_' + i + '_' + '0' + '" class="label">Day Wise Rates</label></h6></div>';
                            //trRoomsSD += ' <div class="two-columns twelve-columns-mobile"><label>Min Stay Rate:</label></div>';
                            //trRoomsSD += '<div class="two-columns twelve-columns-mobile"><div class="full-width button-height SelBox"><input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57"  id="txtMinRateSD' + j + '_' + i + '_' + '0' + '" class="input full-width cInputs"></div></div>'
                            //trRoomsSD += '<div class="two-columns twelve-columns-mobile"><div class="full-width button-height SelBox"><select id="NightsRatesSD' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_NightsRates"><option value="0" selected="selected">0</option><option value="1" >1Night</option><option value="2">2Nights</option><option value="3">3Nights</option><option value="4">4Nights</option><option value="5">5Nights</option><option value="6">6Nights</option><option value="7">7Nights</option><option value="8">8Nights</option><option value="9">9Nights</option><option value="10">10Nights</option></select></div></div>'
                            trRoomsSD += '  </div>';

                            trRoomsSD += '<div class="columns" id="RoomRatesByDaySD' + j + '_' + i + '_' + '0' + '" style="display:none">';
                            //Day wise Rates SD        
                            // trRoomsSD += '<div  class="columns">';
                            for (var wr = 0; wr < WeekRateType.length; wr++) {

                                trRoomsSD += '<div class="one-columns"><label>' + WeekRateType[wr] + '</label></div>';
                                for (var wd = 0; wd < WeekDaysSD.length; wd++) {
                                    trRoomsSD += '<div class="one-columns"><small>' + WeekDaysSD[wd] + '</small><br/><input type="number" min="0" value="0" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtSD' + WeekDaysSD[wd] + j + '_' + i + '_' + '0' + wr + '" class="input cWeekDaysSD" placeholder="' + WeekDaysSD[wd] + '" style="width:40px;"></div>';
                                }

                            }
                            trRoomsSD += '</div>';


                            trRoomsSD += '  <div  class="columns">';

                            trRoomsSD += '  <div class="three-columns  twelve-columns-mobile"><label>Offer:</label><div class="full-width button-height SelBox"> <select id="sel_OfferTypeSD' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_OfferTyp"  onchange="AddOfferSD(this.id,\'' + j + '_' + i + '_' + '0' + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iOfferSD' + j + '_' + i + '_' + '0' + '" class="full-width sel_OfferTyp"></ul></div></div>';

                            trRoomsSD += ' <div class="two-columns twelve-columns-mobile"><label>Offer Nights:</label><div class="full-width button-height SelBox"><select id="OfferNightsSD' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width sel_Offernight"><option value="0" selected="selected">0</option><option value="1" >1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option></select></div></div>';

                            trRoomsSD += '  <div class="four-columns  twelve-columns-mobile"><label>Cancellation Policy:</label><div id="ButtonSearch" style="height: 20px"><span class="button-group">'
                            trRoomsSD += ' <input type="button" id="chkCancellationpolicyNonRefundableSD' + j + '_' + i + '_' + '0' + '" class="button black-gradient" value="NonRefundable"  onclick="CancellationpolicySD(\'' + 'NonRefundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')">'
                            trRoomsSD += ' <input type="button"  id="chkCancellationpolicyRefundableSD' + j + '_' + i + '_' + '0' + '" class="button white-gradient" value="Refundable" onclick="CancellationpolicySD(\'' + 'Refundable' + '\',\'' + j + '_' + i + '_' + '0' + '\')"></span></div>';
                            trRoomsSD += ' </div>';

                            trRoomsSD += '  <div id="divTaxSD' + j + '_' + i + '_' + '0' + '" class="three-columns twelve-columns-mobile" style="display:none" none"><label>Select Policy:</label><div class="full-width button-height SelBox"> <select id="sel_CancellationPolicyIdSD' + j + '_' + i + '_' + '0' + '"name="validation-select" class="select cCancelationPolicies" onchange="AddCancelationsSD(this.id,\'' + j + '_' + i + '_' + '0' + '\');"><option value="" selected="selected" disabled>Please select</option></select><ul id="iCancelationPoliciesSD' + j + '_' + i + '_' + '0' + '" class="full-width sCanceltionPoliciesSD"> </ul></div></div>';
                            trRoomsSD += ' </div>';

                            trRoomsSD += '  <div  class="columns">';

                            trRoomsSD += '  <div id="TypeSD' + j + '_' + i + '_' + '0' + '" class="four-columns twelve-columns-mobile"><label>Rate Type:</label><div class="full-width button-height SelBox"><select id="sel_TypeSD' + j + '_' + i + '_' + '0' + '" name="validation-select" class="select full-width"><option value="Contracted" selected="selected">Contracted</option><option value="Promo">Promo</option><option value="Tactical">Tactical</option></select></div></div>';
                            trRoomsSD += '  <div class="eight-columns twelve-columns-mobile"><label>Rate Note:</label><textarea  id="txtRateNoteSD' + j + '_' + i + '_' + '0' + '" class="input full-width autoexpanding"></textarea></div>';
                            trRoomsSD += '</div>';

                            // trRoomsSD += ' </div>';

                            trRoomsSD += '<div  class="columns">';
                            trRoomsSD += '  <div class="twelve-columns">';
                            trRoomsSD += ' <label class="input-info">Meals Rate</label><hr/>'
                            trRoomsSD += '</div>'
                            trRoomsSD += '</div>'

                            trRoomsSD += '<div  class="columns">';
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">Meal</label></div>'
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">Adult Rate</label></div>'
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">CWB Rate</label></div>'
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">CNB Rate</label></div>'
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">Type</label></div>'
                            trRoomsSD += '  <div class="two-columns"><label class="input-info">Compulsary</label></div>'
                            trRoomsSD += '</div>'

                            trRoomsSD += '<div class="AddMeal' + 'sp_' + i + '_' + arrRoomList[j].RoomTypeID + '"></div>';

                            //End Inputs in SD    

                            trRoomsSD += '<div  class="columns">';
                            trRoomsSD += '<div class="twelve-columns">'
                            trRoomsSD += '<label class="input-info">AddOns Rate</label><hr/>'
                            trRoomsSD += '</div>'
                            trRoomsSD += '</div>'

                            trRoomsSD += '<div   id="AddSPOns' + j + '_' + i + '_' + '0' + arrRoomList[j].RoomTypeID + '">';
                            GetAddDetails(j, "sp_" + i + '_' + arrRoomList[j].RoomTypeID, 'AddSPOns' + j + '_' + i + '_' + '0' + arrRoomList[j].RoomTypeID);
                            trRoomsSD += '</div>';


                            trRoomsSD += '  </div>';
                            trRoomsSD += '  </div>';
                            trRoomsSD += '  </div>';
                            trRoomsSD += '  </div>';

                            //End Data Special Dates
                            $("#tabDateSD" + j + '_' + i).append(trRoomsSD);
                            $("#ClicktabDateSD" + j + '_' + i).click()
                        }
                    }
                    /////// End Special Dates Tab ///////////////

                    //////////////////////////////////////////End Tausif Work //////////////////////////////////

                }
            }


            GetCancelationPolicies();
            GetMasterOffer();
            //  GetALLOffer();
        }, 1000);
    }



}

function ValidateCommon(id) {
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;


    Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    if (Supplier == "-") {
        Success("Please Select Supplier");
        $("#txtFromRV" + id + "").val("");
        return false;
    }
    if (Country == "") {
        Success("Please Select Country")
        $("#txtFromRV" + id + "").val("");
        return false;
    }
    if (Currency == "-") {
        Success("Please Select Currency")
        $("#txtFromRV" + id + "").val("");
        return false;
    }
    if (MealPlan == "-") {
        Success("Please Select MealPlan")
        $("#txtFromRV" + id + "").val("");
        return false;
    }
}

var UpdateFlag = false;
var Rate = "";
function ChkAvlDate(RateType, id) {
    //Success("hello world");


    var cRateFormRV = $(".cRateValidFrom");
    var cRateToRV = $(".cRateValidTo");


    for (var i = 0; i < cRateFormRV.length; i++) {

        var FromRV = cRateFormRV[i].value;
        var ToRV = cRateToRV[i].value;

        if (FromRV != "" && ToRV !== "") {
            CheckinRV[i] = FromRV;
            CheckoutRV[i] = ToRV;
            DateRangeRV[i] = FromRV + " TO " + ToRV;
            RateValidityData += DateRangeRV[i] + ' | ' + DaysRV + '^';
        }


    }
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    //var Country = Ctry.options[Ctry.selectedIndex].value;
    Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;
    var CheckinR = [], CheckoutR = [], RateTypeCode = [];

    for (var fr = 0; fr < CheckinRV.length; fr++) {
        CheckinR.push(CheckinRV[fr]);
        CheckoutR.push(CheckoutRV[fr]);
    }

    var data =
      {
          HotelCode: HotelCode,
          Supplier: 0,
          Country: Country,
          Currency: Currency,
          MealPlan: MealPlan,
          CheckinR: CheckinR,
          CheckoutR: CheckoutR,
          RateType: RateType
      }

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/ChekAvailRates",
        //url: "RoomHandler.asmx/GetRatelist",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Rate = result.sRate;
                //Rate = result.RateList;
                UpdateFlag = true;
                ConfirmModel(Rate);
                $("#txtToRV" + id).val("");
            }
        }
    });

}


//div Rate Validity
var clickRV = 0;
var arrRateValidities = [];
function AddRateValidity(id) {

    if (UpdateFlag == true) {
        ConfirmModel(Rate);
    }
    else {
        var divRequest = '';
        if (($('#txtFromRV' + id).val() == "") || ($('#txtToRV' + id).val() == "")) {
            Success('Please Fill Mandatory Fields')
            return false;
        }
        var ChkSD = $("#chkSpecialDate");
        if (ChkSD.is(":checked")) {
            if (($('#txtFromRV' + id).val() == "") || ($('#txtToRV' + id).val() == "")) {
                Success('Please Fill Dates')
                $("#chkSpecialDate").attr("checked", false);
                return false;
            }
        }
        var DateRV = $('#txtFromRV' + id).val() + "&nbsp;TO&nbsp;" + $('#txtToRV' + id).val();

        if (id == clickRV) {
            document.getElementById("lnkAddRV" + id).style.display = "none";
            clickRV += 1;

            document.getElementById("clickRV").innerHTML = clickRV;
            divRequest += ' <div class="columns" id="divRV' + clickRV + '">';
            divRequest += '  <div class="three-columns"><small class="input-info">From:</small><br/><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtFromRV' + clickRV + '" class="input-unstyled cRateValidFrom" value="" ></span></div>';
            divRequest += '  <div class="three-columns"><small class="input-info">To:</small><br/><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtToRV' + clickRV + '" class="input-unstyled cRateValidTo" value=""> </span></div>';
            //divRequest += ' <div class="four-columns cRVDays" id="iRVDays' + clickRV + '"><small class="input-info">Days:</small><br />';
            //divRequest += '   <label for="chkAllDays' + clickRV + '" class="label"><input type="checkbox" class="cDayRV" id="chkAllDays' + clickRV + '" value="All"  onchange="CheckRVAllDays(' + clickRV + ');">All</label>';
            //divRequest += ' <label for="chkSun' + clickRV + '" class="label"><input type="checkbox" id="chkSun' + clickRV + '" class="cDayRV" value="Sun" onchange="CheckRVSingle(' + clickRV + ');">Sun</label>';
            //divRequest += ' <label for="chkMon' + clickRV + '" class="label"><input type="checkbox" id="chkMon' + clickRV + '" class="cDayRV" value="Mon" onchange="CheckRVSingle(' + clickRV + ');">Mon</label>';
            //divRequest += ' <label for="chkTue' + clickRV + '" class="label"><input type="checkbox" id="chkTue' + clickRV + '" class="cDayRV" value="Tue" onchange="CheckRVSingle(' + clickRV + ');">Tue</label><br />';
            //divRequest += ' <label for="chkWed' + clickRV + '" class="label"><input type="checkbox" id="chkWed' + clickRV + '" class="cDayRV" value="Wed" onchange="CheckRVSingle(' + clickRV + ');">Wed</label>';
            //divRequest += ' <label for="chkThu' + clickRV + '" class="label"><input type="checkbox" id="chkThu' + clickRV + '" class="cDayRV" value="Thu" onchange="CheckRVSingle(' + clickRV + ');">Thu</label>';
            //divRequest += ' <label for="chkFri' + clickRV + '" class="label"><input type="checkbox" id="chkFri' + clickRV + '" class="cDayRV" value="Fri" onchange="CheckRVSingle(' + clickRV + ');">Fri</label>';
            //divRequest += ' <label for="chkSat' + clickRV + '" class="label"><input type="checkbox" id="chkSat' + clickRV + '" class="cDayRV" value="Sat" onchange="CheckRVSingle(' + clickRV + ');">Sat</label>';
            //divRequest += ' </div>';
            divRequest += ' <div class="one-columns"><br />';
            divRequest += ' <a id="lnkAddRV' + clickRV + '"  onclick="AddRateValidity(' + clickRV + ');" class="button"><span class="icon-plus-round blue"></span></a>';
            divRequest += ' <a  onclick="RemoveRateValidity(' + clickRV + ');" class="button"><span class="icon-minus-round red"></span></a>';
            divRequest += ' </div>';


            divRequest += ' </div></div><br />';
            $('#idRateValidity').append(divRequest);
            DateRateValidity(clickRV);
            //document.getElementById('chkAllDays' + clickRV).click();
        }
    }
}


function CheckRVAllDays(id) {
    var AllDays = $("#chkAllDays" + id)[0];
    var Sunday = $("#chkSun" + id)[0];
    var Monday = $("#chkMon" + id)[0];
    var Tuesday = $("#chkTue" + id)[0];
    var Wednesday = $("#chkWed" + id)[0];
    var Thursday = $("#chkThu" + id)[0];
    var Friday = $("#chkFri" + id)[0];
    var Saturday = $("#chkSat" + id)[0];

    if (AllDays.checked == true) {
        Sunday.checked = true;
        Monday.checked = true;
        Tuesday.checked = true;
        Wednesday.checked = true;
        Thursday.checked = true;
        Friday.checked = true;
        Saturday.checked = true;
    }
    else {
        Sunday.checked = false;
        Monday.checked = false;
        Tuesday.checked = false;
        Wednesday.checked = false;
        Thursday.checked = false;
        Friday.checked = false;
        Saturday.checked = false;
    }

}

function CheckRVSingle(id) {
    var AllDays = $("#chkAllDays" + id)[0];
    var Sunday = $("#chkSun" + id)[0];
    var Monday = $("#chkMon" + id)[0];
    var Tuesday = $("#chkTue" + id)[0];
    var Wednesday = $("#chkWed" + id)[0];
    var Thursday = $("#chkThu" + id)[0];
    var Friday = $("#chkFri" + id)[0];
    var Saturday = $("#chkSat" + id)[0];
    if (Sunday.checked == false || Monday.checked == false || Tuesday.checked == false || Wednesday.checked == false || Thursday.checked == false || Friday.checked == false || Saturday.checked == false) {
        AllDays.checked = false;
    }
    if (Sunday.checked == true && Monday.checked == true && Tuesday.checked == true && Wednesday.checked == true && Thursday.checked == true && Friday.checked == true && Saturday.checked == true) {
        AllDays.checked = true;
    }

}

function RemoveRateValidity(valRemove) {
    if (clickRV > 0) {
        document.getElementById('divRV' + valRemove).remove();
        clickRV -= 1;
        document.getElementById("clickRV").innerHTML = clickRV;
        document.getElementById("lnkAddRV" + clickRV).style.display = "";

    }
}

var DateRangeRV = [];
var CheckinRV = [];
var CheckoutRV = [];

var DaysRV = [];
var RateValidityData = ''

function AddRVDates() {
    var trRooms = '';
    var tempDays = '';
    var cRateFormRV = $(".cRateValidFrom");
    var cRateToRV = $(".cRateValidTo");
    var ChkDay = $("#iRVDays0").children('label').children('input')
    ChkDay.each(function () {
        if (this.checked == true) {
            if (this.value != 'All') {
                tempDays += this.value + ',';
            }
            else {
                tempDays = 'All' + ',';
            }

        }
    })
    tempDays = tempDays.replace(/,\/|\/$/g, '');

    DaysRV = tempDays.split(',');
    var index = DaysRV.indexOf("All");    // <-- Not supported in <IE9
    if (index !== -1) {
        DaysRV.splice(index, 1);
    }

    for (var i = 0; i < cRateFormRV.length; i++) {

        var FromRV = cRateFormRV[i].value;
        var ToRV = cRateToRV[i].value;

        if (FromRV != "" && ToRV !== "") {
            CheckinRV[i] = FromRV;
            CheckoutRV[i] = ToRV;
            DateRangeRV[i] = FromRV + " TO " + ToRV;
            RateValidityData += DateRangeRV[i] + ' | ' + DaysRV + '^';
        }


    }





}
//End div Rate Validity

function CheckSpecialDate() {

    if (UpdateFlag == true) {
        $("#chkSpecialDate").attr("checked", false);
        ConfirmModel(Rate);

    }
    else {
        var ChkSD = $("#chkSpecialDate");
        var cRVFrom = $(".cRateValidFrom");
        var cRVTo = $(".cRateValidTo");

        if (ChkSD.is(":checked")) {
            for (var id = 0; id < cRVFrom.length; id++) {
                if (cRVFrom[id].value == "" || cRVTo[id].value == "") {
                    Success("Please select Date Range");
                    $("#chkSpecialDate").attr("checked", false);
                    return false;
                }
            }
            $("#idSpecialDate")[0].style.cssText = "display:";
            $("#spldtheading")[0].style.cssText = "display:";
            $("#TabRoomsSpl")[0].style.cssText = "display:";
            //$("#dispSD")[0].style.cssText = "display:";
            DateSpecialDates(0);
        }
        else {
            $("#idSpecialDate")[0].style.cssText = "display: none;";
            $("#dispSD")[0].style.cssText = "display:none";
            $("#spldtheading")[0].style.cssText = "display:none";
            $("#TabRoomsSpl")[0].style.cssText = "display:none";
        }
    }

}



function CheckSDAllDays(id) {
    if ($("#chkAllSD" + id)[0].checked == true) {
        $("#chkSunSD" + id)[0].checked = true;
        $("#chkMonSD" + id)[0].checked = true;
        $("#chkTueSD" + id)[0].checked = true;
        $("#chkWedSD" + id)[0].checked = true;
        $("#chkThuSD" + id)[0].checked = true;
        $("#chkFriSD" + id)[0].checked = true;
        $("#chkSatSD" + id)[0].checked = true;
    }
    else {
        $("#chkSunSD" + id)[0].checked = false;
        $("#chkMonSD" + id)[0].checked = false;
        $("#chkTueSD" + id)[0].checked = false;
        $("#chkWedSD" + id)[0].checked = false;
        $("#chkThuSD" + id)[0].checked = false;
        $("#chkFriSD" + id)[0].checked = false;
        $("#chkSatSD" + id)[0].checked = false;
    }
}

function CheckSDSingle(id) {
    var AllDays = $("#chkAllSD" + id)[0];
    var Sunday = $("#chkSunSD" + id)[0];
    var Monday = $("#chkMonSD" + id)[0];
    var Tuesday = $("#chkTueSD" + id)[0];
    var Wednesday = $("#chkWedSD" + id)[0];
    var Thursday = $("#chkThuSD" + id)[0];
    var Friday = $("#chkFriSD" + id)[0];
    var Saturday = $("#chkSatSD" + id)[0];
    if (Sunday.checked == false || Monday.checked == false || Tuesday.checked == false || Wednesday.checked == false || Thursday.checked == false || Friday.checked == false || Saturday.checked == false) {
        AllDays.checked = false;
    }
    if (Sunday.checked == true && Monday.checked == true && Tuesday.checked == true && Wednesday.checked == true && Thursday.checked == true && Friday.checked == true && Saturday.checked == true) {
        AllDays.checked = true;
    }

}

var clickSD = 0;
function AddSpecialDates(id) {
    var divRequest = '';
    if (($('#txtFromSD' + id).val() == "") || ($('#txtToSD' + id).val() == "")) {
        Success('Please Fill Mandatory Fields')
        return false;
    }
    if (id == clickSD) {
        document.getElementById("lnkAddSD" + id).style.display = "none";
        clickSD += 1;
        document.getElementById("clickSD").innerHTML = clickSD;
        divRequest += ' <div class="columns" id="divSD' + clickSD + '">';
        divRequest += '  <div class="three-columns"><small class="input-info">From:</small><br><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtFromSD' + clickSD + '" class="input-unstyled ctxtfromSD" value=""></span></div>';
        divRequest += '  <div class="three-columns"><small class="input-info">To:</small><br><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtToSD' + clickSD + '" class="input-unstyled ctxttoSD" value="" > </span></div>';
        divRequest += ' <div class="one-columns"><br />';
        divRequest += ' <a id="lnkAddSD' + clickSD + '"  onclick="AddSpecialDates(' + clickSD + ');" class="button"><span class="icon-plus-round blue"></span></a>';
        divRequest += ' <a  onclick="RemoveSpecialDate(' + clickSD + ');" class="button"><span class="icon-minus-round red"></span></a>';
        divRequest += ' </div>';


        divRequest += ' </div></div><br />';
        $('#idSpecialDate').append(divRequest);
        DateSpecialDates(clickSD);
        //document.getElementById('chkAllSD' + clickSD).click();
    }
}

function RemoveSpecialDate(valRemove) {
    if (clickSD > 0) {
        document.getElementById('divSD' + valRemove).remove();
        clickSD -= 1;
        document.getElementById("clickSD").innerHTML = clickSD;
        document.getElementById("lnkAddSD" + clickSD).style.display = "";

    }
}

var DateRangeSD = [];
var CheckinSD = [];
var CheckoutSD = [];
var DaysSD = [];
var SpecialDatesData = '';

function AddSDDates() {
    var trRooms = '';
    var cRateFromSD = $(".ctxtfromSD");
    var cRateToSD = $(".ctxttoSD");

    for (var i = 0; i < cRateFromSD.length; i++) {
        var tempDays = '';
        var FromSD = cRateFromSD[i].value;
        var ToSD = cRateToSD[i].value;

        var ChkSDDay = $("#iSDDays" + i).children('label').children('input')

        ChkSDDay.each(function () {
            if (this.checked == true) {
                if (this.value != 'All') {
                    tempDays += this.value + ',';
                }
                else {
                    tempDays = 'All';
                    return false;
                }

            }
        })
        tempDays = tempDays.replace(/,\/|\/$/g, '');
        DaysSD[i] = tempDays;
        if (FromSD != "" && ToSD != "") {
            DateRangeSD[i] = FromSD + " TO " + ToSD;
            CheckinSD[i] = FromSD;
            CheckoutSD[i] = ToSD;
            SpecialDatesData += DateRangeSD[i] + ' | ' + DaysSD[i] + '^';
        }

    }
}
//End div Special Dates

var WeekDaysRV = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
var WeekDaysSD = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
//var WeekRateType = ["RR", "EB", "CWB", "CNB"];
//var WeekRateType = ["RR", "EB"];
var WeekRateType = ["Room Rate"];


function GetMasterOffer() {
    var Country = [];
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();
    //var Country;
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All") {
            Country += '';
        }
        else {
            Country += ChkList[i] + ",";
        }

    }

    var ddlRequest = '';
    var data = {
        DateRangeRV: DateRangeRV,
        CheckinRV: CheckinRV,
        CheckoutRV: CheckoutRV,
        Country: Country
    }
    $.ajax({
        type: "POST",
        url: "MasterHotelOfferHandler.asmx/GetMasterOffer",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $(".sel_OfferTyp select").empty();
                arrMasterOffer = result.arrOffer;
                // $(".cCancelationPolicies select").empty();
                if (arrMasterOffer.length > 0) {
                    ddlRequest += '<option value="" selected="selected" disabled>Please select</option>';
                    for (i = 0; i < arrMasterOffer.length; i++) {
                        ddlRequest += '<option value="' + arrMasterOffer[i].Sid + '">' + arrMasterOffer[i].SeasonName + '</option>';
                    }
                    // $(".sel_OfferTypeSD").append(ddlRequest);
                    $(".sel_OfferTyp select").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function AddCancelations(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    CancelPoliciesValue = value;
    CancelPoliciesText = text;
    row += '<li id="' + value + '">' + CancelPoliciesText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iCancelationPolicies" + count).append(row);

    // $("#txtCancellationRV" + count).append(row);
    // GetCancelationsRV(value, count);
}
function GetCancelationsRV(CancelId, count) {
    var ddlRequest = '';

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelations",
        data: '{"CancelId":"' + CancelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.NewCancelationPolicies;
                $("#sel_CancellationPolicyId" + count).empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {

                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }
                    $("#sel_CancellationPolicyId" + count).append(ddlRequest);

                }
            }
        },
        error: function () {
        }
    });
}

function GetCancelationsSD(CancelId, count) {
    var ddlRequest = '';

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCancelations",
        data: '{"CancelId":"' + CancelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCancelationPolicies = result.NewCancelationPolicies;
                $("#sel_CancellationPolicyIdSD" + count).empty();
                if (arrCancelationPolicies.length > 0) {
                    for (i = 0; i < arrCancelationPolicies.length; i++) {

                        ddlRequest += '<option value="' + arrCancelationPolicies[i].CancelationID + '">' + arrCancelationPolicies[i].CancelationPolicy + '</option>';
                    }

                    $("#sel_CancellationPolicyIdSD" + count).append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function AddCancelationsSD(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    CancelPoliciesValue = value;
    CancelPoliciesText = text;
    row += '<li id="' + value + '">' + CancelPoliciesText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iCancelationPoliciesSD" + count).append(row);
    GetCancelationsSD(value, count);
}

function AddOffer(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    OfferValue = value;
    OfferText = text;
    row += '<li id="' + value + '">' + OfferText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iOffer" + count).append(row);
    // GetCancelationsSD(value, count);
}

function AddOfferSD(name, count) {
    var row = "";
    var CancelPoliciesValue = "";
    var CancelPoliciesText = "";

    var e = document.getElementById(name);
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    OfferValue = value;
    OfferText = text;
    row += '<li id="' + value + '">' + OfferText + ' <span class="deleteMe" onclick="DeleteLi();">X</span></li>'

    $("#iOfferSD" + count).append(row);
    // GetCancelationsSD(value, count);
}

function DeleteLi() {
    $(".deleteMe").on("click", function () {
        $(this).closest("li").remove();
    });
}
function CheckRVforDaywise(id, idd, iddd) {
    var ChkSD = $("#chkRVforDaywise" + id + '_' + idd + '_' + iddd);

    if (ChkSD.is(":checked")) {
        $("#RoomRatesByDayRV" + id + '_' + idd + '_' + iddd)[0].style.display = "";
        for (var rt = 0; rt < arrRoomTypes.length; rt++) {
            $("#txtRV" + WeekDaysRV[0] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[1] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[2] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[3] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[4] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[5] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[6] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRate" + id + '_' + idd + '_' + iddd).val());

            $("#txtRV" + WeekDaysRV[0] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[1] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[2] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[3] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[4] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[5] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[6] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBed" + id + '_' + idd + '_' + iddd).val());
        }
    }
    else {
        $("#RoomRatesByDayRV" + id + '_' + idd + '_' + iddd)[0].style.display = "none";
    }
}

function CheckSDforDaywise(id, idd, iddd) {
    var ChkSD = $("#chkSDforDaywise" + id + '_' + idd + '_' + iddd);

    if (ChkSD.is(":checked")) {
        $("#RoomRatesByDaySD" + id + '_' + idd + '_' + iddd)[0].style.display = "";
        for (var rt = 0; rt < arrRoomTypes.length; rt++) {
            $("#txtSD" + WeekDaysRV[0] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[1] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[2] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[3] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[4] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[5] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[6] + id + '_' + idd + '_' + iddd + 0).val($("#txtRoomRateSD" + id + '_' + idd + '_' + iddd).val());

            $("#txtSD" + WeekDaysRV[0] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtRV" + WeekDaysRV[1] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[2] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[3] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[4] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[5] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
            $("#txtSD" + WeekDaysRV[6] + id + '_' + idd + '_' + iddd + 1).val($("#txtExtraBedSD" + id + '_' + idd + '_' + iddd).val());
        }
    }
    else {
        $("#RoomRatesByDaySD" + id + '_' + idd + '_' + iddd)[0].style.display = "none";
    }
}

function checkTaxApllied(val) {
    if (val == "NO") {
        $("#div_UpdateTaxDetails")[0].style.display = "";
    }
    else {
        $("#div_UpdateTaxDetails")[0].style.display = "none";
    }
}


function CancellationpolicyRV(val, ID) {

    if (val == "Refundable") {
        $("#divTaxRV" + ID)[0].style.display = "";
        //$('#chkCancellationpolicyNonRefundable' + ID).prop('disabled', true);
        //$('#chkCancellationpolicyRefundable' + ID).prop('disabled', false);
        // $('#chkCancellationpolicyNonRefundable' + ID).addClass("white");
        $("#chkCancellationpolicyRefundable" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyRefundable" + ID).addClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).addClass("button white-gradient");

    }
    else {
        $("#divTaxRV" + ID)[0].style.display = "none";
        //$('#chkCancellationpolicyRefundable' + ID).prop('disabled', true);
        //$('#chkCancellationpolicyNonRefundable' + ID).prop('disabled', false);
        $("#chkCancellationpolicyRefundable" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyRefundable" + ID).addClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundable" + ID).addClass("button black-gradient");
    }
}

function CancellationpolicySD(val, ID) {
    if (val == "Refundable") {
        $("#divTaxSD" + ID)[0].style.display = "";
        $("#chkCancellationpolicyRefundableSD" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyRefundableSD" + ID).addClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).addClass("button white-gradient");
    }
    else {
        $("#divTaxSD" + ID)[0].style.display = "none";
        $("#chkCancellationpolicyRefundableSD" + ID).removeClass("button black-gradient");
        $("#chkCancellationpolicyRefundableSD" + ID).addClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).removeClass("button white-gradient");
        $("#chkCancellationpolicyNonRefundableSD" + ID).addClass("button black-gradient");
    }
}

var RateTypes = ["GN", "SL"];
function SaveRates() {

    //  SaveInventory();
    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    var Sunday = '', Monday = '', Tuesday = '', Wednesday = '', Thursday = '', Friday = '', Saturday = '';
    var RVCheckin = '', RVCheckout = '';


    //Common Data
    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    //var Country = Ctry.options[Ctry.selectedIndex].value;
    Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    var TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails;
    //var RateInclude = $("#txtRateInclude").val();
    //var RateExclude = $("#txtRateExclude").val();
    var RtInc = $('.cInclutions');
    var RateInclude = '';
    for (var ri = 0; ri < RtInc.length; ri++) {
        RateInclude += RtInc[ri].innerHTML.split('<')[0] + "^";
    }

    var RtExc = $('.cExclutions');
    var RateExclude = '';
    for (var re = 0; re < RtExc.length; re++) {
        RateExclude += RtExc[re].innerHTML.split('<')[0] + "^";
    }

    if (Supplier == "-") {
        Success("Please Select Supplier")
        return false;
    }
    if (Country == "") {
        Success("Please Select Country")
        return false;
    }
    if (Currency == "-") {
        Success("Please Select Currency")
        return false;
    }
    if (MealPlan == "-") {
        Success("Please Select MealPlan")
        return false;
    }

    var MinStay = $("#txtminStay").val();
    var MaxStay = $("#txtmaxStay").val();
    var MinRoom = $("#txtminRoom").val();

    var inputs = $(".cInputs")
    var AllInputs = "";
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value == "") {
            AllInputs = "Blank";
        }
    }
    if (AllInputs == "Blank") {
        Success("Please Fill Rates And Rate Type of All Rooms");
        return false;
    }


    //var RateType = $(".cInputsRateType")
    //var AllInputsRateType = "";
    //for (var i = 0; i < RateType.length; i++) {
    //    if (RateType[i].value == "") {
    //        AllInputsRateType = "Blank";
    //    }
    //}
    //if (AllInputsRateType == "Blank") {
    //    Success("Please Select Rate Type of All Rooms");
    //    return false;
    //}


    if ($("#chkTaxIncluded1").is(":checked")) {
        TaxIncluded = "YES";
        TaxType = 0;
        TaxOn = 0;
        TaxValue = 0;
        TaxDetails = 0;

    }
    else {
        TaxIncluded = "NO";

        var TxType = document.getElementById("sel_TaxType");
        TaxType = TxType.options[TxType.selectedIndex].value;

        var TxOn = document.getElementById("sel_TaxOn");
        TaxOn = TxOn.options[TxOn.selectedIndex].value;

        TaxValue = $("#txtTaxValue").val();
        TaxDetails = $("#txtTaxDetails").val();
    }
    var CheckinR = [], CheckoutR = [], RateTypeCode = [];

    for (var fr = 0; fr < CheckinRV.length; fr++) {
        CheckinR.push(CheckinRV[fr]);
        CheckoutR.push(CheckoutRV[fr]);
    }
    var minRate = [], minNights = []; /*Min Stay Rates*/

    var RoomTypeID = [], RoomTypeName = [];
    var RoomRates = [], ExtraBeds = [], CWB = [], CWN = [], BookingCode = [], CancelPolicy = [], Offers = [], RateNote = [], RateType = [], CWN = [], CWN = [], InventoryType = [], NoOfInventoryRoom = [], OfferNight = [];
    var Daywise = [], Sunday = [], Monday = [], Tuesday = [], Wednesday = [], Thursday = [], Friday = [], Saturday = [];

    var ListAddOnsRates = new Array();
    var divData = "";
    var ListAddOnsRates = new Array();
    var Roomdivs = $(".divContents")
    for (var c = 0; c < arrRoomList.length; c++) {
        RoomTypeID.push(arrRoomList[c].RoomTypeID);
        for (var i = 0; i < DateRangeRV.length; i++) {

            RoomTypeName.push(arrRoomList[c].RoomType);
            RateTypeCode.push(RateTypes[0]);
            RoomRates.push($("#txtRoomRate" + c + '_' + i + '_' + '0').val());
            ExtraBeds.push($("#txtExtraBed" + c + '_' + i + '_' + '0').val());
            CWB.push($("#txtChildWithBed" + c + '_' + i + '_' + '0').val());
            CWN.push($("#txtChildNoBed" + c + '_' + i + '_' + '0').val());
            BookingCode.push($("#txtBookingCode" + c + '_' + i + '_' + '0').val());
            RateNote.push($("#txtRateNote" + c + '_' + i + '_' + '0').val());
            RateType.push($("#sel_Type" + c + '_' + i + '_' + '0').val());
            ListAddOnsRates.push(GenrateAddOnsRates('divRow_' + c + "Nor_" + i + '_' + arrRoomList[c].RoomTypeID, arrRoomList[c].RoomTypeID, "Nor_" + i + '_' + arrRoomList[c].RoomTypeID));
            InventoryType.push($("#sel_InventoryRV" + c + '_' + i + '_' + '0').val());
            NoOfInventoryRoom.push($("#sel_divinventoryRoomRV" + c + '_' + i + '_' + '0').val());
            //if ($("#txtMinRate" + c + '_' + i + '_' + '0').val() == 0)
            minRate.push(0);
            //  else
            // minRate.push($("#txtMinRate" + c + '_' + i + '_' + '0').val());


            // minNights.push($("#NightsRates" + c + '_' + i + '_' + '0').val());
            minNights.push(0);
            OfferNight.push($("#OfferNights" + c + '_' + i + '_' + '0').val());

            var cpolicies = "";
            var cppolicy = $('#iCancelationPolicies' + c + '_' + i + '_' + '0')[0].children
            for (var cp = 0; cp < cppolicy.length; cp++) {
                cpolicies += cppolicy[cp].id + ",";
            }
            cpolicies.replace(/^,|,$/g, "");
            //var ClPolicy = document.getElementById("sel_CancellationPolicyId" + c);
            if (cpolicies != null)
                //CancelPolicy.push(ClPolicy.options[ClPolicy.selectedIndex].value);
                CancelPolicy.push(cpolicies);
            else
                divData = "NotApplied"

            var offers = "";
            var coffer = $('#iOffer' + c + '_' + i + '_' + '0')[0].children
            for (var of = 0; of < coffer.length; of++) {
                offers += coffer[of].id + ",";
            }
            offers.replace(/^,|,$/g, "");
            if (offers != null)
                Offers.push(offers);
            else
                divData = "NotApplied"
            //var ofrs = document.getElementById("sel_OfferType" + c);
            //if (ofrs != null)
            //    Offers.push(ofrs.options[ofrs.selectedIndex].value);
            //else
            //divData = "NotApplied"

            var Day = '', sun = '', mon = '', tue = '', wed = '', thu = '', fri = '', sat = '';
            var sunRR = '', monRR = '', tueRR = '', wedRR = '', thuRR = '', friRR = '', satRR = '';
            var sunEB = '', monEB = '', tueEB = '', wedEB = '', thuEB = '', friEB = '', satEB = '';
            if ($("#chkRVforDaywise" + c + '_' + i + '_' + '0').is(":checked")) {
                Day = "Yes";
                sunRR = $("#txtRV" + WeekDaysRV[0] + c + '_' + i + '_' + '0' + 0).val();
                monRR = $("#txtRV" + WeekDaysRV[1] + c + '_' + i + '_' + '0' + 0).val();
                tueRR = $("#txtRV" + WeekDaysRV[2] + c + '_' + i + '_' + '0' + 0).val();
                wedRR = $("#txtRV" + WeekDaysRV[3] + c + '_' + i + '_' + '0' + 0).val();
                thuRR = $("#txtRV" + WeekDaysRV[4] + c + '_' + i + '_' + '0' + 0).val();
                friRR = $("#txtRV" + WeekDaysRV[5] + c + '_' + i + '_' + '0' + 0).val();
                satRR = $("#txtRV" + WeekDaysRV[6] + c + '_' + i + '_' + '0' + 0).val();

                sunEB = $("#txtRV" + WeekDaysRV[0] + c + '_' + i + '_' + '0' + 1).val();
                monEB = $("#txtRV" + WeekDaysRV[1] + c + '_' + i + '_' + '0' + 1).val();
                tueEB = $("#txtRV" + WeekDaysRV[2] + c + '_' + i + '_' + '0' + 1).val();
                wedEB = $("#txtRV" + WeekDaysRV[3] + c + '_' + i + '_' + '0' + 1).val();
                thuEB = $("#txtRV" + WeekDaysRV[4] + c + '_' + i + '_' + '0' + 1).val();
                friEB = $("#txtRV" + WeekDaysRV[5] + c + '_' + i + '_' + '0' + 1).val();
                satEB = $("#txtRV" + WeekDaysRV[6] + c + '_' + i + '_' + '0' + 1).val();

                sun = sunRR + ',' + sunEB;
                mon = monRR + ',' + monEB;
                tue = tueRR + ',' + tueEB;
                wed = wedRR + ',' + wedEB;
                thu = thuRR + ',' + thuEB;
                fri = friRR + ',' + friEB;
                sat = satRR + ',' + satEB;
            }
            else {
                Day = "No";
                sun = 0;
                mon = 0;
                tue = 0;
                wed = 0;
                thu = 0;
                fri = 0;
                sat = 0;
            }
            Daywise.push(Day);
            Sunday.push(sun);
            Monday.push(mon);
            Tuesday.push(tue);
            Wednesday.push(wed);
            Thursday.push(thu);
            Friday.push(fri);
            Saturday.push(sat);
        }

    }
    if (divData == "NotApplied") {
        Success("Please Select Cancelation Poliies or Offers");
        return false;
    }
    if (MinStay == "") {
        MinStay = 1;
    }
    if (MaxStay == "") {
        MaxStay = 0;
    }
    if (MinRoom == "") {
        MinRoom = 1;
    }

    //if (MaxStay == "") {
    //    Success("Please Enter Max Stay")
    //    return false;
    //}
    var data =
         {
             HotelCode: HotelCode,
             Supplier: 0,
             Country: Country,
             Currency: Currency,
             MealPlan: MealPlan,
             CheckinR: CheckinR,
             CheckoutR: CheckoutR,
             RateInclude: RateInclude,
             RateExclude: RateExclude,
             MinStay: MinStay,
             MaxStay: MaxStay,
             MinRoom: MinRoom,
             //MaxStay: 0,
             TaxIncluded: TaxIncluded,
             TaxType: TaxType,
             TaxOn: TaxOn,
             TaxValue: TaxValue,
             TaxDetails: TaxDetails,
             RoomTypeID: RoomTypeID,
             RateTypeCode: RateTypeCode,
             RoomRates: RoomRates,
             ExtraBeds: ExtraBeds,
             CWB: CWB,
             CWN: CWN,
             BookingCode: BookingCode,
             CancelPolicy: CancelPolicy,
             Offers: Offers,
             RateNote: RateNote,
             RateType: RateType,
             Daywise: Daywise,
             Sunday: Sunday,
             Monday: Monday,
             Tuesday: Tuesday,
             Wednesday: Wednesday,
             Thursday: Thursday,
             Friday: Friday,
             Saturday: Saturday,
             ListAddOnsRates: ListAddOnsRates,
             OfferNight: OfferNight,
             Nights: minNights,
             MinRate: minRate,
             //InventoryType: InventoryType,
             //NoOfInventoryRoom: NoOfInventoryRoom
         }
    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/AddRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //  Success("General Rates Added Sucessfully")
                if ($("#chkSpecialDate").is(":checked")) {
                    SaveSLRates();
                }
                else {
                    Success("Rates Added Sucessfully");
                    setTimeout(function () {
                        window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + GetQueryStringParams("HotelName")
                    }, 500);

                }

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

function SaveSLRates() {
    var Sunday = '', Monday = '', Tuesday = '', Wednesday = '', Thursday = '', Friday = '', Saturday = '';

    var ChkList = $('.chkNationality option:checked').map(function () {
        return this.value;
    }).get();

    var Nationalities = '';
    for (var i = 0; i < ChkList.length; i++) {
        if (ChkList[i] == "All" || ChkList[i] == "Unselect") {
            Nationalities += '';
        }
        else {
            Nationalities += ChkList[i] + "^";
        }

    }

    //Common Data
    var supl = document.getElementById("sel_Supplier");
    var Supplier = supl.options[supl.selectedIndex].value;

    //var Ctry = document.getElementById("sel_Nationality");
    var Country = Nationalities;

    var Crency = document.getElementById("sel_CurrencyCode");
    var Currency = Crency.options[Crency.selectedIndex].value;

    var Mealp = document.getElementById("sel_MealPlan");
    var MealPlan = Mealp.options[Mealp.selectedIndex].value;

    var TaxIncluded, TaxType, TaxOn, TaxValue, TaxDetails;
    //var RateInclude = $("#txtRateInclude").val();
    //var RateExclude = $("#txtRateExclude").val();
    var RtInc = $('.cInclutions');
    var RateInclude = '';
    for (var ri = 0; ri < RtInc.length; ri++) {
        RateInclude += RtInc[ri].innerHTML.split('<')[0] + "^";
    }

    var RtExc = $('.cExclutions');
    var RateExclude = '';
    for (var re = 0; re < RtExc.length; re++) {
        RateExclude += RtExc[re].innerHTML.split('<')[0] + "^";
    }

    var MinStay = $("#txtminStay").val();
    var MaxStay = $("#txtmaxStay").val();
    var MinRoom = $("#txtminRoom").val();


    if ($("#chkTaxIncluded1").is(":checked")) {
        TaxIncluded = "YES";
        TaxType = 0;
        TaxOn = 0;
        TaxValue = 0;
        TaxDetails = 0;

    }
    else {
        TaxIncluded = "NO";
        var TxType = document.getElementById("sel_TaxType");
        TaxType = TxType.options[TxType.selectedIndex].value;

        var TxOn = document.getElementById("sel_TaxOn");
        TaxOn = TxOn.options[TxOn.selectedIndex].value;

        TaxValue = $("#txtTaxValue").val();
        TaxDetails = $("#txtTaxDetails").val();
    }
    var CheckinR = [], CheckoutR = [], RateTypeCode = [];

    for (var fr = 0; fr < CheckinSD.length; fr++) {
        CheckinR.push(CheckinSD[fr]);
        CheckoutR.push(CheckoutSD[fr]);
    }

    var minRate = [], minNights = [];
    var RoomTypeID = [], RoomTypeName = [];
    var RoomRates = [], ExtraBeds = [], CWB = [], CWN = [], BookingCode = [], CancelPolicy = [], Offers = [], RateNote = [], RateType = [], CWN = [], CWN = [], InventoryType = [], NoOfInventoryRoom = [], OfferNight = [];
    var Daywise = [], Sunday = [], Monday = [], Tuesday = [], Wednesday = [], Thursday = [], Friday = [], Saturday = [];

    var ListAddOnsRates = new Array();
    var divData = "";
    var Roomdivs = $(".divContents")
    for (var c = 0; c < arrRoomList.length; c++) {
        RoomTypeID.push(arrRoomList[c].RoomTypeID);
        for (var i = 0; i < DateRangeSD.length; i++) {
            RoomTypeName.push(arrRoomTypes[c].RoomType);
            RateTypeCode.push(RateTypes[1]);
            RoomRates.push($("#txtRoomRateSD" + c + '_' + i + '_' + '0').val());
            ExtraBeds.push($("#txtExtraBedSD" + c + '_' + i + '_' + '0').val());
            CWB.push($("#txtChildWithBedSD" + c + '_' + i + '_' + '0').val());
            CWN.push($("#txtChildNoBedSD" + c + '_' + i + '_' + '0').val());
            BookingCode.push($("#txtBookingCodeSD" + c + '_' + i + '_' + '0').val());
            RateNote.push($("#txtRateNoteSD" + c + '_' + i + '_' + '0').val());
            RateType.push($("#sel_TypeSD" + c + '_' + i + '_' + '0').val());
            ListAddOnsRates.push(GenrateAddOnsRates('divRow_' + c + 'sp_' + i + '_' + arrRoomList[c].RoomTypeID, arrRoomList[c].RoomTypeID, "sp_" + i + '_' + arrRoomList[c].RoomTypeID));
            InventoryType.push($("#sel_InventorySD" + c + '_' + i + '_' + '0').val());
            NoOfInventoryRoom.push($("#sel_divinventoryRoomSD" + c + '_' + i + '_' + '0').val());
            // if ($("#txtMinRateSD" + c + '_' + i + '_' + '0').val() == 0)
            minRate.push(0);
            // else
            // minRate.push($("#txtMinRateSD" + c + '_' + i + '_' + '0').val());

            //  minNights.push($("#NightsRatesSD" + c + '_' + i + '_' + '0').val());
            minNights.push(0);
            OfferNight.push($("#OfferNightsSD" + c + '_' + i + '_' + '0').val());

            var cpolicies = "";
            var cppolicy = $('#iCancelationPoliciesSD' + c + '_' + i + '_' + '0')[0].children;
            for (var cp = 0; cp < cppolicy.length; cp++) {
                cpolicies += cppolicy[cp].id + ",";
            }
            cpolicies.replace(/^,|,$/g, "");;
            //var ClPolicy = document.getElementById("sel_CancellationPolicyIdSD" + c);
            if (cpolicies != null)
                //CancelPolicy.push(ClPolicy.options[ClPolicy.selectedIndex].value);
                CancelPolicy.push(cpolicies);
            else
                divData = "NotApplied"


            var offers = "";
            var coffer = $('#iOfferSD' + c + '_' + i + '_' + '0')[0].children;
            for (var of = 0; of < coffer.length; of++) {
                offers += coffer[of].id + ",";
            }
            offers.replace(/^,|,$/g, "");
            if (offers != null)
                Offers.push(offers);
            else
                divData = "NotApplied"

            //var ofrs = document.getElementById("sel_OfferTypeSD" + c);
            //if (ofrs != null)
            //    Offers.push(ofrs.options[ofrs.selectedIndex].value);
            //else
            //divData = "NotApplied"


            var Day = '', sun = '', mon = '', tue = '', wed = '', thu = '', fri = '', sat = '';
            var sunRR = '', monRR = '', tueRR = '', wedRR = '', thuRR = '', friRR = '', satRR = '';
            var sunEB = '', monEB = '', tueEB = '', wedEB = '', thuEB = '', friEB = '', satEB = '';
            if ($("#chkSDforDaywise" + c + '_' + i + '_' + '0').is(":checked")) {
                Day = "Yes";
                sunRR = $("#txtSD" + WeekDaysSD[0] + c + '_' + i + '_' + '0' + 0).val();
                monRR = $("#txtSD" + WeekDaysSD[1] + c + '_' + i + '_' + '0' + 0).val();
                tueRR = $("#txtSD" + WeekDaysSD[2] + c + '_' + i + '_' + '0' + 0).val();
                wedRR = $("#txtSD" + WeekDaysSD[3] + c + '_' + i + '_' + '0' + 0).val();
                thuRR = $("#txtSD" + WeekDaysSD[4] + c + '_' + i + '_' + '0' + 0).val();
                friRR = $("#txtSD" + WeekDaysSD[5] + c + '_' + i + '_' + '0' + 0).val();
                satRR = $("#txtSD" + WeekDaysSD[6] + c + '_' + i + '_' + '0' + 0).val();

                sunEB = $("#txtSD" + WeekDaysSD[0] + c + '_' + i + '_' + '0' + 1).val();
                monEB = $("#txtSD" + WeekDaysSD[1] + c + '_' + i + '_' + '0' + 1).val();
                tueEB = $("#txtSD" + WeekDaysSD[2] + c + '_' + i + '_' + '0' + 1).val();
                wedEB = $("#txtSD" + WeekDaysSD[3] + c + '_' + i + '_' + '0' + 1).val();
                thuEB = $("#txtSD" + WeekDaysSD[4] + c + '_' + i + '_' + '0' + 1).val();
                friEB = $("#txtSD" + WeekDaysSD[5] + c + '_' + i + '_' + '0' + 1).val();
                satEB = $("#txtSD" + WeekDaysSD[6] + c + '_' + i + '_' + '0' + 1).val();

                sun = sunRR + ',' + sunEB;
                mon = monRR + ',' + monEB;
                tue = tueRR + ',' + tueEB;
                wed = wedRR + ',' + wedEB;
                thu = thuRR + ',' + thuEB;
                fri = friRR + ',' + friEB;
                sat = satRR + ',' + satEB;
            }
            else {
                Day = "No";
                sun = 0;
                mon = 0;
                tue = 0;
                wed = 0;
                thu = 0;
                fri = 0;
                sat = 0;
            }
            Daywise.push(Day);
            Sunday.push(sun);
            Monday.push(mon);
            Tuesday.push(tue);
            Wednesday.push(wed);
            Thursday.push(thu);
            Friday.push(fri);
            Saturday.push(sat);
        }
    }
    if (divData == "NotApplied") {
        Success("Please Select Cancelation Poliies or Offers");
        return false;
    }
    if (MinStay == "") {
        MinStay = 1;
    }
    if (MaxStay == "") {
        MaxStay = 1;
    }
    if (MinRoom == "") {
        MinRoom = 1;
    }
    var data =
         {
             HotelCode: HotelCode,
             Supplier: 0,
             Country: Country,
             Currency: Currency,
             MealPlan: MealPlan,
             CheckinR: CheckinR,
             CheckoutR: CheckoutR,
             RateInclude: RateInclude,
             RateExclude: RateExclude,
             MinStay: MinStay,
             MaxStay: MaxStay,
             MinRoom: MinRoom,
             //MaxStay: 0,
             TaxIncluded: TaxIncluded,
             TaxType: TaxType,
             TaxOn: TaxOn,
             TaxValue: TaxValue,
             TaxDetails: TaxDetails,
             RoomTypeID: RoomTypeID,
             RateTypeCode: RateTypeCode,
             RoomRates: RoomRates,
             ExtraBeds: ExtraBeds,
             CWB: CWB,
             CWN: CWN,
             BookingCode: BookingCode,
             CancelPolicy: CancelPolicy,
             Offers: Offers,
             RateNote: RateNote,
             RateType: RateType,
             Daywise: Daywise,
             Sunday: Sunday,
             Monday: Monday,
             Tuesday: Tuesday,
             Wednesday: Wednesday,
             Thursday: Thursday,
             Friday: Friday,
             Saturday: Saturday,
             ListAddOnsRates: ListAddOnsRates,
             OfferNight: OfferNight,
             Nights: minNights,
             MinRate: minRate,
             //InventoryType: InventoryType,
             // NoOfInventoryRoom: NoOfInventoryRoom

         }
    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/AddRates",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Rates Added Sucessfully");
                setTimeout(function () {
                    window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + GetQueryStringParams("HotelName")
                }, 500);
            }
        }
    })
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function SaveInventory() {


    var inventoryType = "";
    var cInventoryType = $('#sel_InventoryRV' + c)[0].children
    for (var Iv = 0; Iv < cInventoryType.length; Iv++) {
        inventoryType += cInventoryType[Iv].id + ",";
    }
    inventoryType.replace(/^,|,$/g, "");
    if (inventoryType != null)
        InventoryType.push(inventoryType);
    else
        divData = "NotApplied"


    var noOfRoom = "";
    var cNoOfRoom = $('#sel_divinventoryRoomR' + c)[0].children
    for (var Nr = 0; Nr < cNoOfRoom.length; Nr++) {
        noOfRoom += cNoOfRoom[Nr].id + ",";
    }
    noOfRoom.replace(/^,|,$/g, "");
    if (noOfRoom != null)
        NoOfRoom.push(noOfRoom);
    else
        divData = "NotApplied"
    var InventoryType = $('#sel_InventoryRV' + c).val();
    var NoOfRoom = $('#sel_divinventoryRoomR' + c).val();

}

function ConfirmModel(Rate) {

    $.modal({
        content:
                        '<div class="columns" style="padding-left: 10px">' +
                        'Rates already exist in selected dates kindly edit existing rates or select new dates ' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns">' +
                        '<a href="Ratelist.aspx?sHotelID=' + HotelCode + '&HName=' + HotelName + '"">' +
                        //'<button type="button" style="margin-left: 10px;margin-top: 15px" class="button glossy blue-gradient">Yes</button></a>' +
                        '</div>' +
                        ' </div>' +
                        '<br>' +
                        '<div class="twelve-columns" id="div_Edit">' +
                        '</div>' +
                        ' </div>',


        title: 'Rates',
        width: 400,
        height: 300,
        scrolling: true,
        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        // buttonsLowPadding: true
    });
    if (Rate.length != 0)
        EditModal(Rate);
}
var ArrRateID = [];
function EditModal(Rate) {
    tab = +' <div class="table-wrap"> <table id="tbl_EditRate">';
    tab += '<thead>';
    tab += '<tr>';
    tab += '<th style="width:200px;height:30px;text-align: center">CheckIn</th>';
    tab += '<th style="width:200px;height:30px;text-align: center">CheckOut</th>';
    tab += '<th style="width:200px;height:30px;text-align: center">Rate Type</th>';
    tab += '<th style="width:200px;height:30px;text-align: center">Edit</th>';
    tab += '</tr>';
    tab += '</thead>';
    tab += '<tbody>'


    for (var i = 0; i < Rate.length; i++) {
        if (i == 0) {
            tab += '<tr>';
            tab += '<td style="width:200px;background-color:white;height:32px;text-align: center">' + Rate[i].Checkin + '</td>';
            tab += '<td style="width:200px;background-color:white;height:32px;text-align: center">' + Rate[i].Checkout + '</td>';
            tab += '<td style="width:200px;background-color:white;height:32px;text-align: center">' + Rate[i].RateType + '</td>';
            tab += '<td style="width:200px;background-color:white;height:32px;text-align: center"><a href="#" onclick=RedirectEditRate(\'' + HotelCode + '\',\'' + HotelName + '\',\'' + Rate[i].HotelRateID + '\',\'' + Rate[i].RateType + '\')><span class="icon-pencil icon-size2" title="Edit Rates"></a></td>';
            tab += '</tr>';
        }

        ArrRateID.push(Rate[i].HotelRateID);

    }

    tab += '</tbody>';
    tab += '</table></div></div>';

    $("#div_Edit").append(tab);
}

function RedirectEditRate(HotelCode, HotelName, HotelRateID, RateType) {
    UpdateFlag = false;
    window.location.href = "EditRate.aspx?HotelCode=" + HotelCode + "&HotelName=" + HotelName + "&RateID=" + HotelRateID + "&RateType=" + RateType + "&ArrRateID=" + ArrRateID + "";
}