﻿var Arr_Country = new Array();
var arrCity = new Array();
var id;
$(document).ready(function () {

    GetCountry();

    id = GetQueryStringParams('ID');
    if (id != undefined) {
        GetSalesPersonByID(id);
        $("#btn_RegiterSales").val('Update');
    }

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });

    //$('#selTeritoryCountry').change(function () {
    //    var sndcountry = $('#selTeritoryCountry').val();
    //    GetTeritoryCity(sndcountry);
    //});


    $('#selTerCityCountry').change(function () {
        var sndcountry = $('#selTerCityCountry').val();
        GetTeritoryCity(sndcountry);
    })
    $("#txt_DOB").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });



});


function GetCountry() {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                Arr_Country = result.CountryList;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    $("#selTerCityCountry").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                    $("#selTerCityCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

function GetCity(recCountry) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCity",
        data: '{"country":"' + recCountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.CityList;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}




function ValidateLogin() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    $("#formAgent label").css("display", "none");
    $("#formAgent label").html("* This field is required");

    var bValid = true;

    var sFirstName = $("#txt_FirstName").val();
    var sLastName = $("#txt_LastName").val();
    var Gender = $("#selGender option:selected").val();

    var nMobile = "";
    for (var i = 0; i < $(".txtmob").length; i++) {

        var Mob = $(".txtmob")[i].value;
        nMobile += Mob + "^";

    }
    var DOB = "";
    var sEmail = $("#txt_Email").val();
    DOB = $("#txt_DOB").val();
    var sAddress = $("#txt_Address").val();
    var sCountry = $("#selCountry").val();
    var State = "";
    if (sCountry == "IN") {
        State = $("#selState").val();

        if (State == "") {
            bValid = false;
            Success("* Please select State.");
            return;
        }
    }
    var sCity = $("#selCity").val();
    var nPinCode = $("#txt_PinCode").val();

    //var sThemes = $("#select_Themes").val();


    var SalesTerritory = $("#selTerritory").val();
    var TerCountry = "";
    var TerState = "";
    var TerCityCountry = "";
    var TerCity = "";
    var TerAgency = "";
    if (SalesTerritory == "Country") {

        TerCountry = $("#select_TerCountry").val();
        if (TerCountry == "-") {
            bValid = false;
            Success("* Please select Territory Country.");
            return;
        }
    }
    else if (SalesTerritory == "State") {

        TerState = $("#select_TerState").val();
        if (TerState == "-") {
            bValid = false;
            Success("* Please select Territory State.");
            return;
        }
    }
    else if (SalesTerritory == "City") {
        TerCityCountry = $("#selTerCityCountry").val();
        TerCity = $("#select_TerCity").val();
        if (TerCityCountry == "") {
            bValid = false;
            Success("* Please select Territory Country.");
            return;
        }
        if (TerCity == "") {
            bValid = false;
            Success("Please select Territory City.");
            return;
        }
    }
    else if (SalesTerritory == "Agency") {
        TerAgency = $("#select_TerAgency").val();
        if (TerAgency == "-") {
            bValid = false;
            Success("Please select Territory Agency.");
            return;
        }
    }


    if (sFirstName == "") {
        bValid = false;
        Success("Please Enter First Name");
        return;
    }
    if (sFirstName != "" && reg.test(sFirstName)) {
        bValid = false;
        Success("* First Name must not be numeric.");
        return;
    }
    if (sLastName == "") {
        bValid = false;
        Success("Please Enter Last Name");
        return;
    }
    if (sLastName != "" && reg.test(sLastName)) {
        bValid = false;
        Success("Last Name must not be numeric.");
        return;
    }
    if (nMobile == "^") {
        bValid = false;
        $("#lbl_Mobile").css("display", "");
    }


    if (sEmail == "") {
        bValid = false;
        Success("Please Enter Email");
        return;
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            Success("Wrong email format.");
            return;
        }
    }

    if (DOB == "") {
        bValid = false;
        Success("Please select DOB");
        return;
    }

    if (sAddress == "") {
        bValid = false;
        Success("Please Enter Address");
        return;
    }
    if (sAddress != "" && reg.test(sAddress)) {
        bValid = false;
        Success("Address must not be numeric at end.");
        return;
    }
    if (sCountry == "-") {
        bValid = false;
        Success("Please Select Country");
        return;
    }

    if (sCity == "-") {
        bValid = false;
        Success("Please Select City");
        return;
    }



    if (nPinCode == "") {
        nPinCode = '0';
    }
    else {
        if (!(reg.test(nPinCode))) {
            bValid = false;
            Success("Pin code must be correct.");
            return;
        }
    }

    if (SalesTerritory == "") {
        bValid = false;
        Success("Please select Territory.");
        return;
    }



    if (bValid == true) {

        if ($("#btn_RegiterSales").val() == "Update") {
            $.ajax({
                type: "POST",
                url: "../Handler/SalesHandler.asmx/UpdateSalesPerson",
                data: '{"id":"' + id + '","contactID":"' + contactID + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","Gender":"' + Gender + '","nMobile":"' + nMobile + '","sEmail":"' + sEmail + '","DOB":"' + DOB + '","sAddress":"' + sAddress + '","sCountry":"' + sCountry + '","State":"' + State + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","SalesTerritory":"' + SalesTerritory + '","TerCountry":"' + TerCountry + '","TerState":"' + TerState + '","TerCityCountry":"' + TerCityCountry + '","TerCity":"' + TerCity + '","TerAgency":"' + TerAgency + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "SalesPersonDetails.aspx";

                        return false;
                    }
                    if (result.retCode == 1) {

                        //Ok("Sales Person Details Updated successfully")
                        Success("Sales Person Details Updated successfully")
                        setTimeout(function () { window.location.href = "SalesPersonDetails.aspx" }, 2000)
                    }
                    else {
                        Success("Something went wrong! Please contact administrator.")

                    }
                },
                error: function () {
                }
            });
        }
        else if ($("#btn_RegiterSales").val() == "Register") {

            $.ajax({
                type: "POST",
                url: "../Handler/SalesHandler.asmx/CheckForUniqueSales",
                data: '{"sEmail":"' + sEmail + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Login.aspx";
                        window.location.href = "../Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("This email/username is already taken, Please choose another.")

                        $("#txt_Email").css("border-color", "brown");
                        $("#txt_Email").focus();

                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Their is some error in processing your request, Please try again")

                        return false;
                    }
                    else if (result.retCode == 2) {
                        $.ajax({
                            type: "POST",
                            url: "../Handler/SalesHandler.asmx/AddSalesPerson",

                            data: '{"sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","Gender":"' + Gender + '","nMobile":"' + nMobile + '","sEmail":"' + sEmail + '","DOB":"' + DOB + '","sAddress":"' + sAddress + '","sCountry":"' + sCountry + '","State":"' + State + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","SalesTerritory":"' + SalesTerritory + '","TerCountry":"' + TerCountry + '","TerState":"' + TerState + '","TerCityCountry":"' + TerCityCountry + '","TerCity":"' + TerCity + '","TerAgency":"' + TerAgency + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session == 0) {
                                    window.location.href = "AdminDashboard.htm";
                                    //                                
                                    return false;
                                }
                                if (result.retCode == 1) {

                                    //Ok("Sales Person registered successfully. A mail containing password has been sent to registered mail. Now you can activate the agent/Upload the logo.", "LogoUpload", null)
                                    Success("Sales Person registered successfully")
                                    setTimeout(function () { window.location.href = "AddSalesPerson.aspx" }, 2000)

                                }
                                else if (result.retCode == 0) {

                                    Success("Something went wrong! Please contact administrator.")
                                }
                            },
                            error: function () {
                                Success("An error occured during registration! Please contact administrator.")

                            }
                        });
                    }
                },
                error: function () {
                }
            });
        }
    }
}

var contactID = "";
function GetSalesPersonByID(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetSalesPersonByID",
        data: '{"Id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var arrDetails = result.Arr;
                var BrrDetails = result.Brr;
                var Mobile = [];
                Mobile = arrDetails[0].Mobile.split('^');

                if (arrDetails.length > 0) {
                    contactID = arrDetails[0].ContactID;
                    $("#txt_FirstName").val(arrDetails[0].ContactPerson);
                    $("#txt_LastName").val(arrDetails[0].Last_Name);
                    $("#selGender").val(arrDetails[0].Gender);
                    $("#Gender .select span")[0].textContent = arrDetails[0].Gender;
                    $("#txt_Address").val(arrDetails[0].Address);
                    $("#txt_PinCode").val(arrDetails[0].PinCode);
                    $("#txt_Email").val(arrDetails[0].email);
                    $("#txt_DOB").val(arrDetails[0].DOB);


                    for (var i = 0; i < Mobile.length; i++) {
                        //var Countt = 0;
                        //var cnt = parseInt(Count - 1);
                        if (i == 0) {
                            $("#txt_Mobile" + i + "").val(Mobile[i]);
                        }
                        else {
                            var Div = "";
                            Div += '<div class="columns" id="MobDiv' + i + '">'
                            Div += '<div class="four-columns">'
                            Div += '<input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57"  id="txt_Mobile' + i + '" placeholder="Mobile" class="input full-width txtmob" />'

                            Div += '</div>'
                            Div += '<div class="two-columns">'
                            Div += '<i Onclick="AddNumber()" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'
                            //Div += '<button class="btn" title="Remove Mob No" style="margin-left: 5px" onclick="RemoveNumber(\'' + i + '\')"><i class="fa fa-minus"></i></button>'
                            Div += '<i onclick="RemoveNumber(\'' + i + '\')" aria-hidden="true" style="margin-left:2px"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'

                            Div += '</div>'
                            Div += '</div>'
                            Div += '<br />'
                            $("#moreNo").append(Div);
                            $("#moreNo").show();
                            $("#txt_Mobile" + i + "").val(Mobile[i]);
                        }
                    }
                    countrId = arrDetails[0].sCountry
                    GetCountryname(countrId);

                    GetCity(countrId);
                    var Code = arrDetails[0].Code
                    setTimeout(function () { Appendcityforsp(Code) }, 1000);
                    if (arrDetails[0].sCountry == "IN") {
                        GetStateDiv();
                        setTimeout(function () {
                            //stateid = arrDetails[0].state
                            //GetStateforsp(stateid);
                            //GetState();
                            setTimeout(function () { AppendState(arrDetails[0].state) }, 1000);

                        }, 2000)
                    }

                    //Appendcityforsp(Code);
                    //setTimeout(function () {
                    //    $("#selCity").val(arrDetails[0].CountryID)
                    //    $("#City .select span")[0].textContent = arrDetails[0].Description;
                    //}, 3000)



                    //GetCity(arrDetails[0].sCountry);
                    //setTimeout(function () {
                    //    $("#selCity").val(arrDetails[0].Code)
                    //    $("#City .select span")[0].textContent = arrDetails[0].Description;
                    //}, 3000)

                    $("#selTerritory").val(arrDetails[0].TerritoryType);
                    $("#Territory .select span")[0].textContent = arrDetails[0].TerritoryType;

                    if (arrDetails[0].TerritoryType == "Country") {
                        $("#TeriDiv").show();
                        GetTeritoryCountry();
                        setTimeout(function () {

                            Countries = arrDetails[0].CountryID.split(',');
                            var CountriesArray = [];
                            for (var i = 0; i < Countries.length; i++) {
                                CountriesArray.push(Countries[i]);

                            }

                            BindSpCounty(Countries);

                            //$("#select_TerCountry").multipleSelect("setSelects", CountriesArray);

                        }, 3000)

                    }
                    else if (arrDetails[0].TerritoryType == "State") {
                        $("#TeriDivState").show();
                        GetTerState();
                        setTimeout(function () {

                            States = arrDetails[0].StateID.split(',');
                            var StatesArray = [];
                            for (var i = 0; i < States.length; i++) {
                                StatesArray.push(States[i]);

                            }

                            $("#select_TerState").multipleSelect("setSelects", StatesArray);

                        }, 3000)
                    }
                    else if (arrDetails[0].TerritoryType == "City") {
                        $("#TeriDivCity").show();
                        $("#TeriDivCityCountry").show();
                        GetTeritoryCountry();
                        GetTeritoryCity(arrDetails[0].CountryID);
                        $("#selTerCityCountry").val(arrDetails[0].CountryID)
                        setTimeout(function () {

                            Cities = arrDetails[0].CitiesID.split(',');
                            var CitiesArray = [];
                            for (var i = 0; i < Cities.length; i++) {
                                CitiesArray.push(Cities[i]);

                            }

                            $("#select_TerCity").multipleSelect("setSelects", CitiesArray);

                        }, 3000)

                    }
                    else {
                        $("#TeriDivAgency").show();
                        GetTeritoryAgency();
                        setTimeout(function () {

                            Agencies = arrDetails[0].Agents.split(',');
                            var AgenciesArray = [];
                            for (var i = 0; i < Agencies.length; i++) {
                                AgenciesArray.push(Agencies[i]);

                            }

                            $("#select_TerAgency").multipleSelect("setSelects", AgenciesArray);

                        }, 4000)
                    }




                }
            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
}

var Count = 1;
function AddNumber() {
    var cnt = parseInt(Count - 1);

    if ($("#txt_Mobile" + cnt + "").val() != "") {
        var Div = "";
        Div += '<div class="columns" id="MobDiv' + Count + '">'
        Div += '<div class="four-columns">'
        //Div += '<div class="input full-width">'
        //Div += '<input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Mobile' + Count + '" placeholder="Mobile" class="input-unstyled full-width />'
        Div += '<input name="prompt-value" type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="input full-width txtmob" id="txt_Mobile' + Count + '" placeholder="Mobile">'
        //Div += '</div>'
        Div += '</div>'
        Div += '<div class="one-columns">'
        Div += '<i Onclick="AddNumber()" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'
        Div += '</div>'
        Div += '<div class="two-columns">'
        Div += '<i Onclick="RemoveNumber(\'' + Count + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'

        Div += '</div>'
        Div += '</div>'
        Div += '<br />'
        $("#moreNo").append(Div);
        $("#moreNo").show();
        Count++;
    }
    else {
        alert("Enter " + Count + " Mobile No.")
    }

}

function RemoveNumber(Cnt) {
    $("#MobDiv" + Cnt + "").remove();
}

var Country = "";
function GetStateDiv() {
    Country = $("#selCountry option:selected").val();
    if (Country == "IN") {
        $("#DivState").show();
        GetState();
    }
    else {
        $("#DivState").hide();
    }
}

function GetTerritory() {
    var Territory = $("#selTerritory option:selected").val();
    if (Territory == "Country") {
        $("#TeriDiv").show();
        $("#TeriDivCity").hide();
        $("#TeriDivCityCountry").hide();
        $("#TeriDivState").hide();
        $("#TeriDivAgency").hide();
        GetTeritoryCountry();
        //$("#select_TerCountry").show();

    }
    else if (Territory == "State") {

        $("#TeriDivState").show();
        $("#TeriDiv").hide();
        $("#TeriDivAgency").hide();
        $("#TeriDivCity").hide();
        $("#TeriDivCityCountry").hide();

        GetTerState();
    }
    else if (Territory == "City") {
        $("#TeriDivCity").show();
        $("#TeriDivCityCountry").show();
        $("#TeriDivState").hide();
        $("#TeriDiv").hide();
        $("#TeriDivAgency").hide();
    }
    else if (Territory == "Agency") {
        $("#TeriDivCity").hide();
        $("#TeriDivCityCountry").hide();
        $("#TeriDivState").hide();
        $("#TeriDiv").hide();
        $("#TeriDivAgency").show();
        GetTeritoryAgency();
    }
}


function GetTeritoryCountry() {

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                if (arrCountry.length > 0) {
                    $("#select_TerCountry").empty();
                    $(".ms-select-all").empty();
                    var ddlRequest = "";
                    var ddlRequestt = "";
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#select_TerCountry").append(ddlRequest);
                    $('#select_TerCountry').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

function GetState() {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetState",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrState = result.List;
                if (arrState.length > 0) {
                    $("#selState").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any State</option>';
                    for (i = 0; i < arrState.length; i++) {
                        ddlRequest += '<option value="' + arrState[i].StateID + '">' + arrState[i].SateName + '</option>';
                    }
                    $("#selState").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selState").empty();
            }
        },
        error: function () {
            alert("An error occured while loading States")
        }
    });
}

function GetTerState() {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetState",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrState = result.List;
                if (arrState.length > 0) {
                    $("#select_TerState").empty();
                    $(".ms-select-all").empty();
                    var ddlRequest = "";
                    var ddlRequestt = "";
                    for (i = 0; i < arrState.length; i++) {
                        ddlRequest += '<option value="' + arrState[i].StateID + '">' + arrState[i].SateName + '</option>';

                    }
                    $("#select_TerState").append(ddlRequest);
                    $('#select_TerState').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }
            }
            if (result.retCode == 0) {
                $("#select_TerState").empty();
            }
        },
        error: function () {
            alert("An error occured while loading States")
        }
    });
}

function GetTeritoryCity(country) {

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetTeritoryCity",
        data: '{"country":"' + country + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#select_TerCity").empty();
                    $(".ms-select-all").empty();
                    var ddlRequestt = "";
                    var ddlRequest = "";
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';

                    }
                    $("#select_TerCity").append(ddlRequest);
                    $('#select_TerCity').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }
            }
            if (result.retCode == 0) {
                $("#select_TerCity").empty();
            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
} /**/


function GetTeritoryAgency() {

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetTeritoryAgency",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAgency = result.Arr;

                if (arrAgency.length > 0) {
                    $("#select_TerAgency").empty();
                    $(".ms-select-all").empty();
                    //var ddlRequest = '<option selected="selected" value="-">Select Any Agency</option>';
                    var ddlRequest = "";
                    var ddlRequestt = "";
                    for (i = 0; i < arrAgency.length; i++) {

                        ddlRequest += '<option value="' + arrAgency[i].sid + '">' + arrAgency[i].AgencyName + '</option>';
                    }

                    $("#select_TerAgency").append(ddlRequest);
                    $('#select_TerAgency').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}


function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(Arr_Country, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#selCountry").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function BindSpCounty(Countries) {


    var checkclass = document.getElementsByClassName('check');
    var Cityy = Countries.split(",")

    $("#TeriDiv .select span")[0].textContent = Cityy;
    for (var i = 0; i < Cityy.length - 1; i++) {
        $('input[value="' + Cityy[i] + '"][class="Country"]').prop("selected", true);
        $("#select_TerCountry").val(Cityy);
        pCities += Cityy[i] + "^";
        var selected = [];
    }
}


function GetCountryname(countrId) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        // var Country = Country.split(",")
        var Countryname = $.grep(arrCountry, function (p) { return p.Country == countrId; })
           .map(function (p) { return p.Countryname; });


        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i < arrCountry.length; i++) {
            if (arrCountry[i].Country == countrId) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#DivCountry .select span")[0].textContent = Countryname;
                $('input[value="' + countrId + '"][class="OfferType"]').prop("selected", true);

                $("#selCountry").val(countrId);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}

function Appendcityforsp(Code) {
    debugger;
    try {
        var checkclass = document.getelementsbyclassname('check');
        var Description = $.grep(arrCity, function (p) { return p.Code == Code; })
           .map(function (p) { return p.Description; });
        $("#DivCity .select span")[0].textcontent = Description;
        for (var i = 0; i < arrCity.length; i++) {
            if (arrCity[i].Code == Code) {
                $("#DivCity .select span")[0].textcontent = Description;
                $('input[value="' + Code + '"][class="OfferType"]').prop("selected", true);
                $("#selCity").val(Code);
                //  pgta += gta[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}
function GetStateforsp(stateid) {
    debugger;
    try {
        // var checkclass = document.getelementsbyclassname('check');
        var SateName = $.grep(arrState, function (p) { return p.StateID == stateid; })
           .map(function (p) { return p.SateName; });
        $("#State .select span")[0].textcontent = SateName;
        for (var i = 0; i < arrState.length; i++) {
            if (arrState[i].StateID == stateid) {
                //// $('input[value="' + tours[i].trim() + '"][class="chk_tourtype"]').addclass('checked');     
                $("#State .select span")[0].textcontent = SateName;
                $('input[value="' + stateid + '"][class="offertype"]').prop("selected", true);

                $("#selState").val(stateid);
                //  pgta += gta[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}
//$("#selState").val(arrDetails[0].state)
//$("#State .select span")[0].textContent = arrDetails[0].state;
//function AppendState(statename) {
//    try {
//        var checkclass = document.getElementsByClassName('check');
//        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

//        $("#State .select span")[0].textContent = statename;
//        //for (var j = 0; j <= Description.length; j++) {
//        $('input[value="' + statename + '"][class="OfferType"]').prop("selected", true);
//        $("#selState").val(statename);
//        //}
//    }
//    catch (ex)
//    { }
//}
function AppendState(stateid) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        // var Country = Country.split(",")
        var statename = $.grep(arrState, function (p) { return p.StateID == stateid; })
           .map(function (p) { return p.SateName; });


        $("#State .select span")[0].textContent = statename;
        for (var i = 0; i < arrState.length; i++) {
            if (arrState[i].StateID == stateid) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#State .select span")[0].textContent = statename;
                $('input[value="' + stateid + '"][class="OfferType"]').prop("selected", true);

                $("#selState").val(stateid);
                //  pGTA += GTA[i] + ",";
                var selected = [];
            }


        }
    }
    catch (ex)
    { }
}