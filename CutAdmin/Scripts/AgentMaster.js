﻿$(document).ready(function () {

    GetAgentInfo();

});


function GetAgentInfo() {
    $(window).unbind();
    debugger;
    $.ajax({
        type: "POST",
        url: "../handler/AgentHandler.asmx/GetAgentInfo",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#Agent_Info").empty();
            if (result.retCode == 1) {
                var CurrencyClass = result.CurrencyClass;
                switch (CurrencyClass) {
                    case "Currency-AED":
                        CurrencyClass = "AED";
                        break;
                    case "Currency-SAR":
                        CurrencyClass = "SAR";
                        break;
                    case "fa fa-eur":
                        CurrencyClass = "EUR";
                        break;
                    case "fa fa-gbp":
                        CurrencyClass = "GBP";
                        break;
                    case "fa fa-dollar":
                        CurrencyClass = "USD";
                        break;
                    case "fa fa-inr":
                        CurrencyClass = "INR";
                        break;
                }
                var html = "";
                html += '<li class="title-menu"><b>Hi, ' + result.AgentName + '</b></li>                           '
                html += '<li>                                                               '
                html += '   <ul class="message-menu">                                       '
                html += '       <li>                                                        '
                html += '           <a href="#" title="Available Balance">                       '
                html += '               <span class="message-status">                       '
                html += '                   <span class="starred">Not starred</span>      '
                html += '                   <span class="new-message">New</span>            '
                html += '               </span>                                             '
                //html += '               <span class="message-info">                         '
                //html += '                   <span class="blue">15:47</span>                 '
                //html += '               </span>                                             '
                html += '               <strong class="blue">Available Balance</strong><br>        '
                html += '               <strong>' + CurrencyClass + '  ' + result.AvailableCredit + '</strong>          '
                html += '           </a>                                                    '
                html += '       </li>                                                       '
                html += '       <li>                                                        '
                html += '           <a href="#" title="Credit Limit">                       '
                html += '               <span class="message-status">                       '
                html += '                   <span class="starred">Not starred</span>      '
                html += '                   <span class="new-message">New</span>            '
                html += '               </span>                                             '
                //html += '               <span class="message-info">                         '
                //html += '                   <span class="blue">15:47</span>                 '
                //html += '               </span>                                             '
                html += '               <strong class="blue">Credit Limit</strong><br>        '
                html += '               <strong>' + CurrencyClass + '  ' + result.CreditLimit + '</strong>          '
                html += '           </a>                                                    '
                html += '       </li>                                                       '
                html += '    </ul>                                                          '
                html += '</li>                                                              '
                $("#Agent_Info").append(html);
            }
            else if (esult.retCode == 0) {
                window.location.href = "../Default.aspx";
            }
        },
        error: function () {
            Success("An error occured while loading agent info")
        }
    });
}

