﻿var arAgentRoleList = new Array();
var arFormList = new Array();
var arAPIList = new Array();
var arFormListForRole = new Array();
var arAPIListForRole = new Array();
var arrayToSubmit = new Array();
var sSelectedRoleValue
$(function () {
    GetAuthority()
    $("#spName").text(GetQueryStringParams('Name').replace(/%20/g, ' '))
})
function GetAuthority() {
    $("#hdnDCode").val(GetQueryStringParams('uid'));
    $('#lbl_ErrServiceTaxPercent').css("display", "none");
    $('#lbl_ErrServiceTaxPercent').html("* This field is required");
    $.ajax({
        type: "POST",
        url: "../handler/ChannelHandler.asmx/GetAuthority",
        data: '{"Sid":"' + $("#hdnDCode").val() + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.sForms;
                if (arFormList.length > 0) {
                    var trForms = '';
                    for (i = 0; i < arFormList.length; i = i + 3) {
                        if (i < arFormList.length) {
                            //trForms += '<div class="row">';
                            //trForms += '<div class="col-md-4">' + arFormList[i].sDisplayName + ': <label style="float:right" for="chk' + arFormList[i].sFormName + '"><input id="chk' + arFormList[i].sFormName + '" type="checkbox" value="' + arFormList[i].sFormName + '"  class="switch tiny"/> </label></div>';
                            //if ((i + 1) < arFormList.length)
                            //    trForms += '<div class="col-md-4">' + arFormList[i + 1].sDisplayName + ': <label style="float:right" for="chk' + arFormList[i + 1].sFormName + '"><input id="chk' + arFormList[i + 1].sFormName + '" type="checkbox" value="' + arFormList[i + 1].sFormName + '"  /> </label></div>';
                            //if ((i + 2) < arFormList.length)
                            //    trForms += '<div class="col-md-4">' + arFormList[i + 2].sDisplayName + ': <label style="float:right" for="chk' + arFormList[i + 2].sFormName + '"><input id="chk' + arFormList[i + 2].sFormName + '" type="checkbox" value="' + arFormList[i + 2].sFormName + '"  class="switch tiny"/> </label></div>';
                            trForms += '<div class="columns">';
                            trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arFormList[i].sFormName + '">' + arFormList[i].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += ' <input type="checkbox" name="switch-medium-1" id="chk' + arFormList[i].sFormName + '" class="switch tiny" value="1">';
                            trForms += ' </p>';
                            trForms += '</div>';

                            if ((i + 1) < arFormList.length){
                                trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arFormList[i + 1].sFormName + '">' + arFormList[i + 1].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += '<input type="checkbox" name="switch-medium-1" id="chk' + arFormList[i + 1].sFormName + '" class="switch tiny" value="1">';
                            trForms += '</p>';
                            trForms += '</div>';
                            }
                            if ((i + 2) < arFormList.length){
                                trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arFormList[i + 2].sFormName + '">' + arFormList[i + 2].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += '<input type="checkbox" name="switch-medium-1" id="chk' + arFormList[i + 2].sFormName + '" class="switch tiny" value="1">';
                            trForms += '</p>';
                            trForms += '</div>';
                            }

                            if ((i) == arFormList.length - 1)
                                trForms += '<div class="two-columns four-columns-mobile">Visa Cropping: <label style="float:right" for="chkCropping"><input id="chkCropping" type="checkbox" value="GRN" class="switch tiny"></label></div>';
                            trForms += '</div>';
                        }
                    }
                    $("#tblForms").append(trForms);
                    if ($("#hdnDCode").val() == "") {
                        $('input[type=checkbox]').attr("disabled", true);
                    }

                }
                arFormListForRole = result.AssigndForms;
                for (j = 0; j < arFormList.length; j++) {
                    var checked = $.grep(arFormListForRole, function (p) { return p.sFormName == arFormList[j].sFormName; })
                              .map(function (p) { return p.sFormName; });
                    if (checked.length != 0)
                        $('#chk' + checked).click();
                    //else
                    //    $('#chk' + arFormList[j].sFormName).bootstrapToggle('off')
                }
                arAPIList = result.sAPI;
                trForms = '';
                if (arAPIList.length > 0) {
                    trForms = '';
                    for (i = 0; i < arAPIList.length; i = i + 3) {
                        if (i < arAPIList.length) {
                            trForms += '<div class="columns">';
                            trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arAPIList[i].sFormName + '">' + arAPIList[i].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += ' <input type="checkbox" name="switch-medium-1" id="chk' + arAPIList[i].sFormName + '" class="switch tiny" value="1">';
                            trForms += ' </p>';
                            trForms += '</div>';
                           
                            if ((i + 1) < arAPIList.length){
                            trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arAPIList[i + 1].sFormName + '">' + arAPIList[i + 1].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += '<input type="checkbox" name="switch-medium-1" id="chk' + arAPIList[i + 1].sFormName + '" class="switch tiny" value="1">';
                            trForms += '</p>';
                            trForms += '</div>';
                            }
                           if ((i + 2) < arAPIList.length){
                            trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arAPIList[i + 1].sFormName + '">' + arAPIList[i + 2].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += '<input type="checkbox" name="switch-medium-1" id="chk' + arAPIList[i + 2].sFormName + '" class="switch tiny" value="1">';
                            trForms += '</p>';
                            trForms += '</div>';
                            trForms += '</div>';
                           }
                            //    trForms += '<div class="col-md-4">' + arAPIList[i + 1].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 1].sFormName + '"><input id="chk' + arAPIList[i + 1].sFormName + '" type="checkbox" value="' + arAPIList[i + 1].sFormName + '" class="switch tiny"/> </label></div>';
                            //if ((i + 2) < arAPIList.length)
                            //    trForms += '<div class="col-md-4">' + arAPIList[i + 2].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 2].sFormName + '"><input id="chk' + arAPIList[i + 2].sFormName + '" type="checkbox" value="' + arAPIList[i + 2].sFormName + '" class="switch tiny"/> </label></div>';
                            //trForms += '</div>';
                        }
                    }
                    trForms += '';
                    $("#tblAPI").append(trForms);
                    if ($("#hdnDCode").val() == "") {
                        $('input[type=checkbox]').attr("disabled", true);
                    }

                }
                arAPIListForRole = result.AssigndAPI;
                for (j = 0; j < arAPIList.length; j++) {
                    var checked = $.grep(arAPIListForRole, function (p) { return p.sFormName == arAPIList[j].sFormName; })
                  .map(function (p) { return p.sFormName; });
                    //alert(checked)
                    if (checked.length != 0)
                        $('#chk' + checked).click()
                    else
                        $('#chk' + arAPIList[j].sFormName)
                }

                if (result.sHold == true)
                    $('#chkOnHold').click()
                if (result.SupplierVisible == true)
                    $('#chk_Supplier').click()
                $('#chkCropping')
            }
        },
        error: function () {
        }
    });
}

function SubmitAgentRole(Type) {
    debugger;
    var j = 0;
    arrayToSubmit = [];
    if (Type == 'Form') {
        for (var i = 0; i < arFormList.length; i++) {
            if ($('#chk' + arFormList[i].sFormName).prop('checked')) {
                arrayToSubmit[j] = arFormList[i].nId;
                j++;
            }
        }
    }
    else if (Type == 'Hotel') {
        for (var i = 0; i < arAPIList.length; i++) {
            if ($('#chk' + arAPIList[i].sFormName).prop('checked')) {
                arrayToSubmit[j] = arAPIList[i].nId;
                j++;
            }
        }
    }

    var arrayJson = JSON.stringify(arrayToSubmit);
    var SupplierVisible = false;
    var OnHold = false;
    var Cropping = false;
    if ($('#chk_Supplier').prop('checked')) {
        SupplierVisible = true;
    }
    if ($('#chkOnHold').prop('checked')) {
        OnHold = true;
    }
    if ($('#chkCropping').prop('checked')) {
        Cropping = true;
    }
    //if (chk_Supplier.checked == true) {
    //    SupplierVisible = true;
    //}
    //if (chkOnHold.checked == true) {
    //    OnHold = true;
    //}
    //if (chkCropping.checked == true) {
    //    Cropping = true;
    //}
    if ($("#hdnDCode").val() != "0") {
        $.ajax({
            type: "POST",
            url: "../handler/ChannelHandler.asmx/SetFormsForAgentRole",
            data: '{"nUid":"' + $("#hdnDCode").val() + '","SupplierVisible":"' + SupplierVisible + '","OnHold":"' + OnHold + '","Cropping":"' + Cropping + '","Type":"' + Type + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                }
                else if (result.retCode == 0) {
                    Success("Something goes wrong!");
                }
            },
            error: function () {
                Success("error occured while submitting checked forms");
            }
        });
    }
    else {

        Success('Please select a Role!');
        $("#hdnDCode").focus()
    }
}
function CheckAssigned() {

}