﻿$(document).ready(function () {
    GetCount();
});

function GetCount() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetCount",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelList = result.HotelList;
                var AgentCount = result.AgentCount;
                $('#hotel_Count').text(HotelList);
                $('#Agents').text(AgentCount);
                var arrBuyer = result.arrBuyerRervation;
                for (var i = 0; i < arrBuyer.length; i++) {
                    $("#div_Buyer").append('<div class="columns"> <div class="nine-columns mid-margin-bottom">' + arrBuyer[i].Buyer + '</div><div class="three-columns mid-margin-bottom align-right">' + arrBuyer[i].Count + '</div></div>')
                }

                var arrSupplier = result.arrSupplierRevt;
                for (var i = 0; i < arrSupplier.length; i++) {
                    $("#div_Supplier").append('<div class="columns"> <div class="nine-columns mid-margin-bottom">' + arrSupplier[i].Supplier + '</div><div class="three-columns mid-margin-bottom align-right">' + arrSupplier[i].Count + '</div></div>')
                }

                var arrDestinations = result.arrDestinationsRevt;
                for (var i = 0; i < arrDestinations.length; i++) {
                    $("#div_Destination").append('<div class="columns"> <div class="nine-columns mid-margin-bottom">' + arrDestinations[i].Destination + '</div><div class="three-columns mid-margin-bottom align-right">' + arrDestinations[i].Count + '</div></div>')
                }

                var arrHotelsRevt = result.arrHotelsRevt;
                for (var i = 0; i < arrHotelsRevt.length; i++) {
                    $("#div_Hotels").append('<div class="columns"> <div class="nine-columns mid-margin-bottom">' + arrHotelsRevt[i].HotelName + '</div><div class="three-columns mid-margin-bottom align-right">' + arrHotelsRevt[i].Count + '</div></div>')
                }
                BookingReconfirmation(result.arrReconfirmed);
                BookingRequests(result.arrOnRequest);
                BookingOnHold(result.arrOnHold)
                BookingGroupRequest(result.arrGroupRequest)
                GetCalender(result.arrBookings);
            }
        },

    });
}


function BookingRequests(result) {
    try {

        var Pending = result.OnHoldBookings;
        var Confirmed = result.OnHoldConfirmed;
        var Rejected = result.OnHoldRequested;
        var html = "";
        html += '<div class="with-mid-padding" style="border-bottom: 2px solid #212db8; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);">'
        html += '<h4 class="icon-mailbox icon-size2"> On Request Bookings</h4>'
        html += '</div>'
        html += '<div class="with-mid-padding align-center">'
        html += '<a href="bookinglist.aspx?Type=BookingPending&Status=Vouchered" >'
        html += '<h2 class="aline-right no-margin-bottom">' + Pending
        html += '<span class="thin font14">On Request</span>'
        html += '</h2>'
        html += '</a>'
        html += '</div>    '
        html += '<div class="columns">'
        html += '<div class="six-columns align-center" style="background-color: cornflowerblue; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=BookingConfirmed&Status=Vouchered" >'
        html += '<h4 class="">' + Confirmed + '</h4>'
        html += '<span class="silver font12">Confirmed</span>'
        html += '</a>'
        html += '</div>'
        html += '<div class="six-columns align-center" style="background-color: steelblue; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=BookingRejected&Status=Vouchered" >'
        html += '<h4>' + Rejected + '</h4>'
        html += '<span class="silver font12">Rejected</span>'
        html += '</a>'
        html += '</div>'
        html += '</div>'
        $("#BookingsRequests").append(html);
    } catch (e) {

    }
}

function BookingOnHold(result) {
    try {
        var Bookings = result.OnHoldBookings;
        var Requested = result.OnHoldRequested;
        var Confirmed = result.OnHoldConfirmed;
        var html = "";
        html += '<div class="with-mid-padding" style="border-bottom: 2px solid #72b648; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);">'
        html += '<h4 class="icon-hourglass icon-size2"> Bookings On Hold</h4>'
        html += '</div>'
        html += '<div class="with-mid-padding align-center">'
        html += '<a href="bookinglist.aspx?Type=Bookings&Status=OnRequest" >'
        html += '<h2 class="aline-right no-margin-bottom">' + Bookings
        html += '<span class="thin font14">On Hold</span>'
        html += '</h2>'
        html += '</a>'
        html += '</div> '
        html += '<div class="columns">'
        html += '<div class="six-columns align-center" style="background-color: darkolivegreen; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=Requested&Status=OnRequest" >'
        html += '<h4 class="">' + Confirmed + '</h4>'
        html += '<span class="silver font12">Confirmed</span>'
        html += '        </a>'
        html += '</div>'
        html += '<div class="six-columns align-center" style="background-color: forestgreen; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=Confirmed&Status=OnRequest" >'
        html += '<h4>' + Requested + '</h4>'
        html += '<span class="silver font12">Released</span>'
        html += '        </a>'
        html += '</div>'
        html += '</div>'
        $("#BookingOnHold").append(html);
    } catch (e) {

    }
}

function BookingReconfirmation(result) {
    try {
        var Pending = result.ReconfirmPending;
        var Confirmed = result.ReconfirmRequested;
        var Rejected = result.ReconfirmReject;
        var html = "";
        html += '<div class="with-mid-padding" style="border-bottom: 2px solid #e2a213; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);">'
        html += '<h4 class="icon-feather icon-size2">Booking Reconfirmations</h4>'
        html += '</div>'
        html += '<div class="with-mid-padding align-center">'
        html += '<a href="bookinglist.aspx?Type=ReconfirmPending&Status=Vouchered" >'
        html += '<h2 class="aline-right no-margin-bottom">' + Pending
        html += '<span class="thin font14">Need Action</span>'
        html += '</h2>'
        html += '</a>'
        html += '</div>'
        html += '<div class="columns" style="">'
        html += '<div class="six-columns align-center" style="background-color: peru; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=ReconfirmRequested&Status=Vouchered" >'
        html += '<h4 class="">' + Confirmed + '</h4>'
        html += '<span class="silver font12">Reconfirmed</span>'
        html += '</a>'
        html += '</div>'
        html += '<div class="six-columns align-center" style="background-color: saddlebrown; padding-top: 5px;">'
        html += '<a href="bookinglist.aspx?Type=ReconfirmReject&Status=Vouchered"   >'
        html += '<h4>' + Rejected + '</h4>'
        html += '<span class="silver font12">Rejected</span>'
        html += '</a>'
        html += '</div>'
        html += '</div>'
        $("#BookingsReconfirm").append(html);
    } catch (e) {

    }
}

function BookingGroupRequest(result) {
    try {
        var Pending = result.GroupRequestPending;
        var Confirmed = result.GroupRequestRequested;
        var Rejected = result.GroupRequestReject;
        var html = "";
        html += '<div class="with-mid-padding" style="border-bottom: 2px solid #939393; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);">'
        html += '<h4 class="icon-users icon-size2"> Group Request</h4>'
        html += '</div>'
        html += '<div class="with-mid-padding align-center">'
        html += '<a href="groupbookinglist.aspx?Type=GroupRequestPending&Status=GroupRequest">'
        html += '<h2 class="aline-right no-margin-bottom">' + Pending
        html += '<span class="thin font14">Pending</span>'
        html += '</h2>'
        html += '</a>'
        html += '</div>    '
        html += '<div class="columns">'
        html += '<div class="six-columns align-center" style="background-color: darkslategray; padding-top: 5px;">'
        html += '<a href="groupbookinglist.aspx?Type=GroupRequestRequested&Status=GroupRequest" >'
        html += '<h4 class="">' + Confirmed + '</h4>'
        html += '<span class="silver font12">Confirmed</span>'
        html += '</a>'
        html += '</div>'
        html += '<div class="six-columns align-center" style="background-color: slategrey; padding-top: 5px;">'
        html += '<a href="groupbookinglist.aspx?Type=GroupRequestReject&Status=GroupRequest" >'
        html += '<h4>' + Rejected + '</h4>'
        html += '<span class="silver font12">Rejected</span>'
        html += '        </a>'
        html += '</div>'
        html += '</div>'
        $("#GroupRequest").append(html);

    } catch (e) {

    }
}

function GetCalender(arrBookings) {
    try {
        var arrEvents = [];
        for (var i = 0; i < arrBookings.length; i++) {
            arrBookings[i].sCheckIn = arrBookings[i].sCheckIn + "T22:30"
            var Event = {
                id: arrBookings[i].ReservationID,
                title: "<a  onclick='GetDetails(\"" + arrBookings[i].ReservationID + "\")'><h5 class='green underline'>" + arrBookings[i].HotelName + "<br/> <small class=''>(" + arrBookings[i].AgencyName + ")</small></h5></a>",
                start: arrBookings[i].sCheckIn,
                end: arrBookings[i].sCheckIn,
                html: true,
                color: '#FFF',
                textEscape: false,
            }
            arrEvents.push(Event)
        }
        var calendar = $('.datepicker').fullCalendar({
            editable: true,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            events: arrEvents,
            // Convert the allDay from string to boolean
            eventRender: function (event, element, view) {
                element.find('.fc-title').html("<b>" + event.title + "</b>");
            },
            selectable: true,
            selectHelper: true,
            editable: true,
            eventAfterAllRender: function (view) {
                //$(".fc-event-container").append("<span class='closon'>X</span>");
            },
            eventDrop: function (event, delta) {
            },
            eventResize: function (event) {
            },
            eventClick: function (event) {
                //$(".closon").click(function () {
                //    $('#fullCalendar').fullCalendar('removeEvents', event._id);
                //});
            },
        });
        $(".fc-toolbar").css({
            "width": "90%",
            "margin-left": "6%",
        })
    }
    catch (e) {
        alert(e)
    }
}


function GetDetails(ReservationIID) {

   

    var data = {
        ReservationID: ReservationIID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                try {
                    var Detail = result.Detail;

                    $.modal({
                        content:

                      '<div class="modal-body">' +
                      '' +
                      '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 90%">' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Hotel:&nbsp;&nbsp;<b>' + Detail[0].HotelName + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckIn:&nbsp;&nbsp;<b>' + Detail[0].CheckIn + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">CheckOut:&nbsp;&nbsp;<b>' + Detail[0].CheckOut + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Passenger: &nbsp;&nbsp;<b>' + Detail[0].Name + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Location:&nbsp;&nbsp;<b> ' + Detail[0].City + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Nights:&nbsp;&nbsp; <b>' + Detail[0].NoOfDays + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td>' +
                      '<span class="text-left">Booking Id:&nbsp;&nbsp; <b>' + Detail[0].ReservationID + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Booiking Date:&nbsp;&nbsp;<b> ' + Detail[0].ReservationDate + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                       '<td>' +
                      '<span class="text-left">Amount:&nbsp;&nbsp;<b>' + Detail[0].TotalFare + '</b></span>&nbsp;&nbsp;' +
                      '' +
                      '</td>' +
                      '</tr>' +
                      '</table>' +



                       '<br/>' +
                      '</div>',
                        title: 'Booking Details',
                        width: 600,
                        height: 200,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'huge anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true

                    });
                }
                catch (ex) {

                }


            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}