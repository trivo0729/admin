﻿$(function () {
    GetExchangeMarkup();
    //GetCurrentDate();
})

var arrExchangeLog, arrExchangeMarkup, today1;
var MarkupSid = [];

function GetExchangeMarkup() {
    $("#tbl_log").dataTable().fnClearTable();
    $("#tbl_log").dataTable().fnDestroy();
    debugger;
    $.ajax({
        type: "POST",
        url: "../Handler/ExchangeRateHandler.asmx/GetExchangeLog",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrExchangeLog = result.sLogs;
                arrExchangeLog = result.sLastLogs;
                var option = '';
                if (arrExchangeLog.length > 0) {
                    $("#tbl_log tbody").remove();
                    $("#sel_Currency").empty()
                    var Html = '<tbody>';
                    for (i = 0; i < arrExchangeLog.length; i++) {
                        option += '<option value="' + arrExchangeLog[i].MarkupId + '">' + arrExchangeLog[i].Currency + '</option>'
                        Html += '<tr>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].Currency + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].ExchangeRate + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].MarkupRate + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].TotalExchange + '</td>';
                        //Html += '<td style="width:15%" class="align-center">' + arrExchangeLog[i].UpdateDate + '</td>';
                        Html += '<td style="width:10%" class="align-center">' + arrExchangeLog[i].UpdateBy + '</td>';
                        Html += '<td style="width:10%" class="align-center"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update  Exchange"  style="cursor:pointer" onclick="FCYEdit(\'' + arrExchangeLog[i].sid + '\',\'' + arrExchangeLog[i].MarkupId + '\',\'' + arrExchangeLog[i].Currency + '\',\'' + arrExchangeLog[i].ExchangeRate + '\',\'' + arrExchangeLog[i].MarkupAmt + '\',\'' + arrExchangeLog[i].MarkupPer + '\',\'' + arrExchangeLog[i].MarkupRate + '\',\'' + arrExchangeLog[i].TotalExchange + '\')"></span></a></td>';
                        Html += '</tr>';
                    }
                    Html += '</tbody>';
                    $("#tbl_log").append(Html);
                    $("#sel_Currency").append(option);
                    $("#SpanUdtDate").append(arrExchangeLog[0].UpdateDate);
                    $("#tbl_log").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
            }

        },
        error: function () {
        }
    });
}
function GetMarkup() {
    $("#FCYEditModal").modal("show")
}
var Currency, ExchangeRate, MarkupAmt, MarkupPer, LogSid;
//function Submit() {
//    Ok("Are You Sure you want to Update Exchange Markup ??", "Update", null)
//}

function Submit() {

    $.modal({
        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to Update</p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Update()">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function Update() {
    MarkupSid = []; Currency = []; ExchangeRate = []; MarkupAmt = []; MarkupPer = []; LogSid = [];
    LogSid.push($("#Hdn_FCY").val())
    MarkupSid.push($("#Hdn_Id").val())
    ExchangeRate.push($("#txt_FCY").val())
    Currency.push($("#sel_Currency option:selected").text())
    MarkupAmt.push($("#txt_SetMarkupAmt").val())
    MarkupPer.push($("#txt_SetMarkupPer").val())
    var data = {
        MarkupSid: MarkupSid,
        LogSid: LogSid,
        Currency: Currency,
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }
    $.ajax({
        type: "POST",
        url: "../Handler/ExchangeRateHandler.asmx/UpdateExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Markup Updated")
                $("#demo").hide(1000);
                window.location.reload();
            }
            // Cancel()
        },
        error: function () {
        }
    });
}
function FCYEdit(SID, MarkupId, Curruncy, Rate, MarkupAmt, MarkupPer, MarkupRate, Total) {

    $("#Hdn_FCY").val(SID)

    $("#Hdn_Id").val(MarkupId)
    $("#sel_Currency").val(MarkupId)
    $("#div_Currency .select span")[0].textContent = Curruncy;
    $("#txt_FCY").val(Rate)
    $("#txt_SetMarkupAmt").val(MarkupAmt)
    $("#txt_SetMarkupPer").val(MarkupPer)
    $("#txt_MarkupAmt").val(MarkupRate)
    $("#txt_TotalFCY").val(Total)
    if (MarkupAmt == 0) {
        $("#td_Amt").css("display", "none")
        $("#td_Per").css("display", "")
    }

    else {
        $("#td_Amt").css("display", "")
        $("#td_Per").css("display", "none")
    }
    $("#demo").show(1000);
}
function FCYHide() {

    $("#demo").hide(1000);
}

function GetMarkupAmt(id) {
    ExchangeRate = []; MarkupAmt = []; MarkupPer = [];
    ExchangeRate.push($("#txt_FCY").val())
    if (id == "txt_SetMarkupAmt") {
        MarkupAmt.push($("#txt_SetMarkupAmt").val())
        MarkupPer.push(0)
        $("#txt_SetMarkupPer").val(0)
    }
    else {
        $("#txt_SetMarkupAmt").val(0)
        MarkupAmt.push(0)
        MarkupPer.push($("#txt_SetMarkupPer").val())
    }

    var data = {
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }
    $.ajax({
        type: "POST",
        url: "../Handler/ExchangeRateHandler.asmx/GetExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#txt_TotalFCY").val(result.ExchangeRate[0])
                $("#txt_MarkupAmt").val(result.MarkupAmt[0])

            }

        },
        error: function () {
        }
    });

}
function GetCurrencyValue(id) {
    var OnlineRate = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
             .map(function (p) { return p.ExchangeRate; });
    var MarkupAmount = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
           .map(function (p) { return p.MarkupAmt; });
    var MarkupPercent = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.MarkupPer; });
    var sid = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.sid; });
    var Total = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
         .map(function (p) { return p.TotalExchange; });
    var Curruncy = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.Currency; });
    var MarkupAmt = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.MarkupAmt; });
    var MarkupPer = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
        .map(function (p) { return p.MarkupPer; });
    var MarkupRate = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
     .map(function (p) { return p.MarkupRate; });
    FCYEdit(sid, id, Curruncy, OnlineRate, MarkupAmt, MarkupPer, MarkupRate, Total)

}

function GetOnlineRate() {
    $.ajax({
        type: "POST",
        url: "../Handler/ExchangeRateHandler.asmx/GetOnlineRate",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Online Rates Updated")
                GetExchangeMarkup()
            }

        },
        error: function () {
        }
    });

}

function ShowSearchForm() {
    $("#searchResult").remove();
    $("#currId").val('');
    $("#updtBy").val('');
    var div = "";
    div += '<div id="searchResult" style="display:none" class="col-md-12">'
    div += '<table id="searchRslt" class="table table-bordered" style="margin-bottom:20px">'
    div += '<thead>'
    div += '<tr>'
    div += '<th align="center">Currency</th>'
    div += '<th align="center">Exchange Rate</th>'
    div += '<th align="center">Markup</th>'
    div += '<th align="center">Total</th>'
    div += '<th align="center">Update By</th>'
    div += '<th align="center">Update Time</th>'
    div += '</tr>'
    div += '</thead>'
    div += '<tbody>'
    div += '</tbody>'
    div += '</table>'
    div += '</div>'
    $("#rowId").append(div);
    $("#searchDiv").show(1000);
}

function HideSearchForm() {
    $("#searchDiv").hide(1000);
}

//function GetCurrentDate() {
//    var today = new Date();
//    var dd = today.getDate();
//    var mm = today.getMonth() + 1; //January is 0!

//    var yyyy = today.getFullYear();
//    if (dd < 10) {
//        dd = '0' + dd;
//    }
//    if (mm < 10) {
//        mm = '0' + mm;
//    }
//    today1 = dd + '/' + mm + '/' + yyyy;
//    document.getElementById("dateId").value = today1;
//}

function searchER() {
    var curr = $("#currId").val();
    var updatedBy = $("#updtBy").val();
    var updateDt = $("#dateId").val();
    var updateDate = updateDt.replace(/[/]/g, '-');

    if (curr == "") {
        Success("Please select 'currency'.");
        return false;
    }

    if (updatedBy == "") {
        Success("Please select 'Updated By'.");
        return false;
    }
    if (updateDt == "") {
        Success("Please enter date.");
        return false;
    }

    var data = {
        currency: curr,
        updatedBy: updatedBy,
        updateDate: updateDate
    }

    $.ajax({
        type: "POST",
        url: "../Handler/ExchangeRateHandler.asmx/SearchExchangeUpdate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var searchResult = result.exchangeRate;

                if (searchResult.length == 0) {
                    return false;
                }

                var tr = "";
                for (var i = 0; i < searchResult.length; i++) {
                    tr += "<tr><td>" + searchResult[i].Currency + "</td>"
                    tr += "<td>" + searchResult[i].ExchangeRate + "</td>"
                    tr += "<td>" + searchResult[i].MarkupRate + "</td>"
                    tr += "<td>" + searchResult[i].TotalExchange + "</td>"
                    tr += "<td>" + searchResult[i].UpdateBy + "</td>"
                    tr += "<td>" + searchResult[i].UpdateDate.substr(11, 5) + "</td></tr>"
                }
                $("#searchRslt tbody").empty();
                $("#searchRslt tbody").append(tr);
            }
        },
        error: function () {
        }
    });

    $("#searchResult").show(500);
}