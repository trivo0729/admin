﻿var arrDate = new Array();
var arrInventory = new Array();
var Startdt;
var Enddt;
var HotelCode = 0;
function GetInventory() {
    if (location.href.indexOf('?') != -1) {
        HotelCode = GetQueryStringParams('sHotelID');
    }
    Startdt = $('#datepicker_start').val();
    Enddt = $('#datepicker_end').val();
    var InventoryType = "FreeSale";
    if (Freesale_Id.checked) {
        InventoryType = "FreeSale";
        $("#AmM").hide();
        $("#btn_US").hide();
        $("#btn_St").show();
        $("#btn_SS").show();
    }
    if (Allocation_Id.checked) {
        InventoryType = "Allocation";
        $("#btn_US").show();
        $("#btn_SS").show();
        $("#btn_St").hide();
    }

    if (Allotmnet_Id.checked) {
        InventoryType = "Allotment";
        $("#btn_US").show();
        $("#btn_SS").hide();
        $("#btn_St").hide();

    }
    $.ajax({
        type: "Post",
        url: "../Inventoryhandler.asmx/GetInventory",
        data: JSON.stringify({ Startdt: Startdt, Enddt: Enddt, InventoryType: InventoryType, HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrDate = result.ListDates;
                arrInventory = result.arrInventory;
                GenrateTable();
            }
        },
        error: function () {

        },
    });
}

function GenrateTable() {
    $("#div_tbl_InvList").empty();
    var html = '';
    html += '<table class="table responsive-table  colorTable responsive-table-on" id="tbl_HotelList"><thead>';
    html += '<tr>';
    html += '<th >Hotel Name <input type="hidden" id="txt_HotelCode"></th>';
    for (var i = 0; i < arrDate.length; i++) {
        html += '<th class="align-center header" class="align-center header">' + arrDate[i] + '</th>';
    }
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';
    for (var i = 0; i < arrInventory.length; i++) {
        html += '<tr>';
        html += '<td style="width:20%"><div class=" SelBox"><label>' + arrInventory[i].HotelName + '</label><input type="hidden" class="HotelCode" value="' + arrInventory[i].HotelCode + '" /></div></td>';
        for (var d = 0; d < arrDate.length; d++) {
            var Count = $.grep(arrInventory[i].Dates, function (h) { var yr = h.datetime.split('-')[2]; debugger; return h.datetime == arrDate[d] + "-" + yr })
                        .map(function (h) { return h.NoOfCount; })[0];
            var bCount = $.grep(arrInventory[i].Dates, function (h) { var yr = h.datetime.split('-')[2]; debugger; return h.datetime == arrDate[d] + "-" + yr })
                      .map(function (h) { return h.Type; })[0];
            html += '<td class="align-center header" class="align-center header"><input type="checkbox" class="checkbox Date"   value="' + arrDate[d] + '" onclick="SelectByDate(' + arrDate[d] + ')"/>' + bCount + '/' + Count + '</td>';
        }
        html += '</tr>';
    }
    html += '</tbody>';
    html += '</table>';
    $("#div_tbl_InvList").append(html)
    $("#tbl_HotelList").dataTable({
        bSort: false,
        sPaginationType: 'full_numbers',
        sSearch: false
    });
    $("#tbl_HotelList_wrapper").addClass("respTable");
    // Call template init (optional, but faster if called manually)
    $.template.init();
    $('#tbl_HotelList').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        var drophotelId = this.id;
        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }
        // If there is already a special row
        if (row.length > 0) {
            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');
            // Remove row
            row.remove();
            $(".row-drop").remove();
            return;
        }
        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');
            // Remove rows
            rows.remove();

        }
        // Style row
        tr.children().addClass('anthracite-gradient glossy');
        // Add fake row
        var HotelCode = $($(tr).find(".HotelCode")[0]).val();
        $("#txt_HotelCode").val(HotelCode)
        var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == HotelCode; })
               .map(function (p) { return p.ListRoom; })[0];
        $('<tr class="row-drop"><td colspan="' + tr.children().length + '"><div class="columns" style="overflow:scroll height:300px;margin-left:auto;margin-right:auto;"><div id="div_Dates0" class="twelve-columns  twelve-columns-mobile"> <div class="new-row coloumns"><div class="three-row"><b>Note:</b>  Availability  <label style=""><span class="wrapped green-gradient " style="margin-bottom: -10px;"></span></label>  No Availability <label style="margin-bottom: -22px;"><span class="margin-bottom wrapped red-gradient with-mid-padding align-center"></span></label></div> </div></div></div></td></tr>').insertAfter(tr);
        for (var i = 0; i < arrRoom.length; i++) {
            $('<tr class="row-drop RateDetails"  id="div_Class' + i + HotelCode + '" ></tr>').insertAfter(tr);
            GetRoomDetails(arrRoom[i], i + HotelCode);
        }


    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
    });
}

function GetRoomDetails(arrRoom, HotelCode) {
    try {
        var html = '';
        html += '<th style="padding:9px 13px;" ><div class="SelBox"><div class=" SelBox"> <label><input type="checkbox" class="checkbox ChkRoom" name="ChkTotal' + i + '" id="chk' + HotelCode + '" value="' + arrRoom.RoomTypeId + '" onclick="ChakedByRoom("' + HotelCode + '")"> ' + arrRoom.RoomTypeName + '</label></div></th>'
        for (var dt = 0; dt < arrRoom.Dates.length; dt++) {
            var cssClass = "";
            if (arrRoom.Dates[dt].InventoryType == "0")
                cssClass = "noinventory disabled";
            else
                cssClass = "GreenTD";
            if (cssClass != "") {
                var date = arrRoom.Dates[dt].datetime.split('-')[0] + '-' + arrRoom.Dates[dt].datetime.split('-')[1];
                if (arrRoom.Dates[dt].InventoryType == "FreeSale")
                    html += '<td class="' + cssClass + '"><span class="AddTxt"><span><input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.Dates[dt].datetime + '-' + arrRoom.Dates[dt].RateTypeId + '" class="checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1"> <span> ' + arrRoom.Dates[dt].Type + '/FS </span></span></span></span></span></span></td>'
                else
                    html += '<td class="' + cssClass + '"><span class="AddTxt"><span><input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.Dates[dt].datetime + '-' + arrRoom.Dates[dt].RateTypeId + '" class="checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1"> <span> ' + arrRoom.Dates[dt].Type + '/' + arrRoom.Dates[dt].NoOfCount + '  </span></span></span></span></span></span></td>'
            }

            else
                html += '<td>-</td> '
        }
        $("#div_Class" + HotelCode).append(html)

        $(".ChkRoom").click(function () {
            var ndRoom = $(this).hasClass("checked");
            var arrRate = $($(this)[0].childNodes[1]).val();
            if (ndRoom == false)
                $(".Room" + arrRate).addClass("checked");
            else
                $(".Room" + arrRate).removeClass("checked");
            debugger
        });
        $(".Date").click(function () {
            var ndRoom = $(this).hasClass("checked");
            var arrRate = $($(this)[0].childNodes[1]).val().split('-')[0] + '-' + $($(this)[0].childNodes[1]).val().split('-')[1];
            if (ndRoom == false)
                $("." + arrRate).addClass("checked");
            else
                $("." + arrRate).removeClass("checked");
            debugger
        });

    } catch (e) {

    }
}

function ChakedByRoom(HotelCode) {
    $(".Room" + HotelCode).click();
}

function SelectByDate(Date) {
    $("." + Date).click();
}


function Next() {
    var days = 10;
    var StartDate = $("#datepicker_end").val();
    $("#datepicker_start").val(StartDate);
    var dateObject = $("#datepicker_end").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_end").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_end").val();
    GetInventory()
}

function Previous() {
    var days = -10;
    var StartDate = $("#datepicker_start").val();
    $("#datepicker_end").val(StartDate);
    var dateObject = $("#datepicker_start").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_start").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_start").val();
    GetInventory()

}




var HotelName;

function StartSaleModal() {
    ListRoom = new Array();
    var Room = $('.RateDetails');
    HotelName = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.HotelName; })[0];
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0];
            var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.ListRoom; })[0];
            var RoomName = $.grep(arrRoom, function (p) { return p.RoomTypeId == sRoom; })
                  .map(function (p) { return p.RoomTypeName; })[0];
            for (var i = 1; i < ndListDate.length; i++) {
                var sID = $($(ndListDate[i]).find("input:checkbox")).val().split('-')[3];
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked) {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    sDate = sDate.split('-')[0] + '-' + sDate.split('-')[1] + '-' + sDate.split('-')[2];
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length != 0)
                        ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
                    arrDate = new Array();
                }

            }
            if (arrDate.length != 0)
                ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
        }
    }

    if (ListRoom.length == 0) {
        Success("Please Select  Dates");
        return false;
    }
    $.modal.confirm(EditModal(), function () {
        UpdateInventory("Start Sale")

    }, function () {
        $('#modals').remove();
    });
}

function StopSaleModal() {
    ListRoom = new Array();
    var Room = $('.RateDetails');
    HotelName = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.HotelName; })[0];
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0];
            var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.ListRoom; })[0];
            var RoomName = $.grep(arrRoom, function (p) { return p.RoomTypeId == sRoom; })
                  .map(function (p) { return p.RoomTypeName; })[0];
            for (var i = 1; i < ndListDate.length; i++) {
                var sID = $($(ndListDate[i]).find("input:checkbox")).val().split('-')[3];
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked) {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    sDate = sDate.split('-')[0] + '-' + sDate.split('-')[1] + '-' + sDate.split('-')[2];
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length != 0)
                        ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
                    arrDate = new Array();
                }

            }
            if (arrDate.length != 0)
                ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
        }
    }
    $.modal.confirm(EditModalStop(), function () {
        UpdateInventory("Stop Sale")

    }, function () {
        $('#modals').remove();
    });
}

function EditModalStop() {
    arrRate = new Array();
    var tab = '';
    tab += 'Please Confirm Selected Dates & Room For Stop Sale<br/><br/>' + HotelName + '<br/><br/>';
    tab += '<div class="respTable">'
    tab += '<table class="table responsive-table evenodd" id="sorting-advanced">'
    for (var i = 0; i < ListRoom.length; i++) {
        tab += '<tr><td class="even">' + ListRoom[i].Name + '</td><td>' + ListRoom[i].Date[0] + '  to ' + ListRoom[i].Date[ListRoom[i].Date.length - 1] + '</td></tr>'
    }
    tab += '</tr>'
    '</table>'
    tab += '<div>'

    return tab;
}

function EditModal() {
    arrRate = new Array();
    var tab = '';
    tab += 'Please Confirm Selected Dates & Room For Start Sale<br/><br/>' + HotelName + '<br/><br/>';
    tab += '<div class="respTable">'
    tab += '<table class="table responsive-table evenodd" id="sorting-advanced">'
    for (var i = 0; i < ListRoom.length; i++) {
        tab += '<tr><td class="even">' + ListRoom[i].Name + '</td><td>' + ListRoom[i].Date[0] + '  to ' + ListRoom[i].Date[ListRoom[i].Date.length - 1] + '</td></tr>'
    }
    tab += '</tr>'
    '</table>'
    tab += '<div>'

    return tab;
}

function UpdateInventory(value) {

    $.ajax({
        type: "POST",
        url: "Inventoryhandler.asmx/UpdateInventory",
        data: JSON.stringify({ ListRoomRate: ListRoom, status: value }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Inventory Updated Successfully");
                GetInventory();

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })

}