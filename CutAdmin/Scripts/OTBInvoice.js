﻿$(document).ready(function () {
    debugger;
    var ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('RefNo').replace(/\+/g, '%20');
    var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid').replace(/\+/g, '%20');
    InvoicePrint(ReservationID, Uid);
});

function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function Print() {
    window.print();
}
var arrOTBDetails
var arrBookingTransactions;
var arrAgentDetails;
function InvoicePrint(ReservationID, Uid) {

    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/PrintInvoice",
        data: '{"RefNo":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.roleID == "Admin") {
                var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var Year = now.getFullYear()
                var today = (day) + "-" + (month) + "-" + (Year);
                $("#spnDate").text(today);
                arrOTBDetails = result.VisaDetails;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                $('#spnStatus').text(arrOTBDetails[0].OTBNo);
                for (var i = 0 ; i < arrAgentDetails.length; i++) {
                    $('#spnName').text(arrAgentDetails[0].AgencyName);
                    $('#spnAdd').text(arrAgentDetails[0].Address);
                    $('#spnCity').text(arrAgentDetails[0].Description);
                    $('#spnCountry').text(arrAgentDetails[0].Countryname);
                    $('#spnPhone').text(arrAgentDetails[0].phone);
                    $('#spnemail').text(arrAgentDetails[0].email);
                    $('#spnVoucher').text(arrOTBDetails[0].OTBNo)
                    $('#spnAgentRef').text(arrAgentDetails[0].Agentuniquecode)

                }
                $('#UrgenCharges').text(Math.round(arrOTBDetails[0].UrgentFee));
                $('#OtherCharges').text(0);
                $('#ServiceCharges').text(Math.round(arrOTBDetails[0].ServiceCharge * arrOTBDetails.length));
                $('#netAmmount').text(Math.round(arrOTBDetails[0].Total));
                var tr;
                $("#tbl_passenger").empty()
                tr += '<table id="tbl_Rate" border="1" style="margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">';
                tr += '<tbody>'
                tr += '<tr style="border: none;  ">';
                tr += '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px "><span>Service Details</span></td>'
                tr += ' <td style="width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left"><span>Passenger Name</span></td>'
                tr += '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center"><span>Document Number</span></td>'
                tr += '<td style="width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span>Fees</span></td>'
                tr += '</tr>'
                for (var i = 0 ; i < arrOTBDetails.length; i++) {
                    debugger;
                    tr += '<tr>'
                    tr += '<td style="height: 35px; font-size: 15px; border: none;font-weight: 700;/* text-align:center; */"><span id="Airline" style="margin-left: 0.5em;color: #57585A;">Otb ' + arrOTBDetails[i].In_AirLine + '</span></td>';
                    tr += '<td style="height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center"><span id="PassengerName" style="color: #57585A;">' + arrOTBDetails[i].Name + " " + arrOTBDetails[i].LastName + '</span></td>'
                    tr += '<td style="height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span id="docno" style="color: #57585A;">' + arrOTBDetails[i].PassportNo + '</span></td>'
                    tr += '<td style="width: 100px; height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span id="Fess" style="color: #57585A;">' + Math.round(arrOTBDetails[i].VisaFee) + '</span></td>'
                    tr += '</tr>'
                }
                tr += '</tbody>'
                tr += '</table>'
                var s = tr.split('undefined')
                $("#tbl_passenger").html(s[1]);
                var NetAmmount = Math.round(arrOTBDetails[0].Total * 100) / 100//returns 28.5
                NumToWord(Math.round(arrOTBDetails[0].Total));

                //}


            }
            if (result.retCode == 1 && result.roleID == "Agent") {
                var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var Year = now.getFullYear()
                var today = (day) + "-" + (month) + "-" + (Year);
                $("#spnDate").text(today);
                arrOTBDetails = result.VisaDetails;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                $('#spnStatus').text(arrBookingTransactions[0].ReservationID)
                $('#spnReference').text(arrBookingTransactions[0].ReservationID)
                for (var i = 0 ; i < arrAgentDetails.length; i++) {
                    $('#spnName').text(arrAgentDetails[0].AgencyName);
                    $('#spnAdd').text(arrAgentDetails[0].Address);
                    $('#spnCity').text(arrAgentDetails[0].Description);
                    $('#spnCountry').text(arrAgentDetails[0].Countryname);
                    $('#spnPhone').text(arrAgentDetails[0].phone);
                    $('#spnemail').text(arrAgentDetails[0].email);
                    $('#spnVoucher').text(arrOTBDetails[0].OTBNo)
                    $('#spnAgentRef').text(arrAgentDetails[0].Agentuniquecode)
                }
                var TotalAmount = Math.round(result.Markup);
                var VisaFee = Math.round(TotalAmount * 100) / 100//returns 28.5
                var ServiceCharges = Math.round(parseFloat(parseFloat(TotalAmount) - parseFloat(arrOTBDetails[0].Total)));
                var AgentMarkup = result.AgentMarkup;
                var OtherFee = parseFloat(parseFloat(TotalAmount) - parseFloat(AgentMarkup));
                $('#spnServiceNo').text(arrOTBDetails[0].OTBNo);
                $('#spnStatus').text(arrOTBDetails[0].OTBNo);

                //$('#Airline').text("OTB" + " " + arrOTBDetails[0].In_AirLine)
                //$('#PassengerName').text(arrOTBDetails[0].Name);
                //$('#docno').text(arrOTBDetails[0].VisaNo);
                //$('#Fess').text(Math.round(arrOTBDetails[0].Total));
                $('#UrgenCharges').text(Math.round(arrOTBDetails[0].UrgentFee));
                $('#OtherCharges').text(Math.round(TotalAmount - arrOTBDetails[0].Total));
                $('#ServiceCharges').text(Math.round(arrOTBDetails[0].ServiceCharge * arrOTBDetails.length));
                $('#netAmmount').text(Math.round(TotalAmount));
                var tr;
                $("#tbl_passenger").empty()
                tr += '<table id="tbl_Rate" border="1" style="margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">';
                tr += '<tbody>'
                tr += '<tr style="border: none;  ">';
                tr += '<td style="background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:left; padding-left:10px "><span>Service Details</span></td>'
                tr += ' <td style="width:170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left"><span>Passenger Name</span></td>'
                tr += '<td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none;  font-weight: 700; text-align:center"><span>Document Number</span></td>'
                tr += '<td style="width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span>Fees</span></td>'
                tr += '</tr>'
                for (var i = 0 ; i < arrOTBDetails.length; i++) {
                    debugger;
                    tr += '<tr>'
                    tr += '<td style="height: 35px; font-size: 15px; border: none;font-weight: 700;/* text-align:center; */"><span id="Airline" style="margin-left: 0.5em;color: #57585A;">Otb ' + arrOTBDetails[i].In_AirLine + '</span></td>';
                    tr += '<td style="height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center"><span id="PassengerName" style="color: #57585A;">' + arrOTBDetails[i].Name + " " + arrOTBDetails[i].LastName + '</span></td>'
                    tr += '<td style="height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span id="docno" style="color: #57585A;">' + arrOTBDetails[i].PassportNo + '</span></td>'
                    tr += '<td style="width: 100px; height: 35px; font-size: 15px; border: none; font-weight: 700; text-align: center;"><span id="Fess" style="color: #57585A;">' + Math.round(arrOTBDetails[i].VisaFee) + '</span></td>'
                    tr += '</tr>'
                }
                tr += '</tbody>'
                tr += '</table>'
                var s = tr.split('undefined')
                $("#tbl_passenger").html(s[1]);
                var NetAmmount = Math.round(arrOTBDetails[0].Total * 100) / 100//returns 28.5
                NumToWord(Math.round(TotalAmount));
                //}
            }

        },
        error: function () {
            $('#SpnMessege').text('error')
            $('#ModelMessege').modal('show')
            // alert('error')
        }
    });
}

var NUMBER2TEXT = {
    ones: ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'],
    tens: ['', '', 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'],
    sep: ['', ' thousand ', ' million ', ' billion ', ' trillion ', ' quadrillion ', ' quintillion ', ' sextillion ']
};



function onlyNumbers(evt) {
    var e = event || evt; // For trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function NumToWord(inputNumber) {
    var VisaFee = Math.round(inputNumber * 100) / 100//returns 28.5
    //alert(VisaFee)
    var str = new String(VisaFee)
    var splt = str.split("");
    var rev = splt.reverse();
    var once = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
    var twos = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
    var tens = ['', 'Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];

    numLength = rev.length;
    var word = new Array();
    var j = 0;

    for (i = 0; i < numLength; i++) {
        switch (i) {

            case 0:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = 'and ' + once[rev[i]];
                }
                word[j] = word[j];
                break;

            case 1:
                aboveTens();
                break;

            case 2:
                if (rev[i] == 0) {
                    word[j] = '';
                }
                else if ((rev[i - 1] == 0) || (rev[i - 2] == 0)) {
                    word[j] = once[rev[i]] + " Hundred ";
                }
                else {
                    word[j] = once[rev[i]] + " Hundred and";
                }
                break;

            case 3:
                if (rev[i] == 0 || rev[i + 1] == 1) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if ((rev[i + 1] != 0) || (rev[i] > 0)) {
                    word[j] = word[j] + " Thousand";
                }
                break;


            case 4:
                aboveTens();
                break;

            case 5:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if (rev[i + 1] !== '0' || rev[i] > '0') {
                    word[j] = word[j] + " Lakh";
                }

                break;

            case 6:
                aboveTens();
                break;

            case 7:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if (rev[i + 1] !== '0' || rev[i] > '0') {
                    word[j] = word[j] + " Crore";
                }
                break;

            case 8:
                aboveTens();
                break;

                //            This is optional. 

                //            case 9:
                //                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                //                    word[j] = '';
                //                }
                //                else {
                //                    word[j] = once[rev[i]];
                //                }
                //                if (rev[i + 1] !== '0' || rev[i] > '0') {
                //                    word[j] = word[j] + " Arab";
                //                }
                //                break;

                //            case 10:
                //                aboveTens();
                //                break;

            default: break;
        }
        j++;
    }

    function aboveTens() {
        if (rev[i] == 0) { word[j] = ''; }
        else if (rev[i] == 1) { word[j] = twos[rev[i - 1]]; }
        else { word[j] = tens[rev[i]]; }
    }

    word.reverse();
    var finalOutput = '';
    for (i = 0; i < numLength; i++) {
        finalOutput = finalOutput + word[i];
    }
    document.getElementById('lblAmmountinwords').innerHTML = finalOutput;
}