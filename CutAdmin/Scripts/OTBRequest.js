﻿var ArrivalAirLine;
var DepartingAirline;
var Arrivalfrom;
var DepartingFrom;
var ArrivalDate;
var DepartingDate;
var ArrivingFlightNo;
var ArrivingPnr;
var DerartingPnr;
var DerartingFlightNo;
var ArrivalTicketCopy;
var DepartingTicketCopy;
var VisaFee;
var UrgentCharges;
var otherCharges;
var TotalPerPass;
var NoOfPax;
var TotalAmmount;
var PassengerName = new Array();
var PassengerLast = new Array();
var VisaType = new Array();
var VisaNo = new Array();
var dataIssue = new Array();
var PassportNo = new Array();
var Upload = new Array();
var EmailFormat = "";
var Email1;
var CCEmailId;
var Email2;
var arrVisa;
var otbType;
var OtbRefNo;
$(document).ready(function () {
    GetVisaByOtb(0)
});
function GetAgentDetails(uid) {
    if (Currency != undefined) { $(".Currency").removeClass(Currency); }
    var result;
    $.ajax({
        type: "POST",
        url: "../Handler/VisaHandler.asmx/GetAgenDetails",
        data: '{"uid":"' + uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
             result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //$("#spUniqueCode").text(result.Agentuniquecode)
                //$("#spAvailableLimit").html(result.AvailableCredit)
                //$("#spOneTimeCreditLimit").html(result.OTC)
                //$("#spAgent").text(result.ContactPerson)
                if (result.Balance <= 0) {
                    Success("Agent Does not have Sufficient Balance !!")
                    $("#txt_Agent").val("");
                    $("#selVisaCountry").val("-")
                    $("#selService").val("-")
                }
                else {
                    $("#spUniqueCode").text(result.Agentuniquecode)
                    $("#spAvailableLimit").html(result.AvailableCredit)
                    $("#spOneTimeCreditLimit").html(result.OTC)
                    $("#spAgent").text(result.ContactPerson)
                    $("#spCreditLimit").html(result.CreditGiven)
                    $("#txt_Avail").val(result.Balance);
                    $("#selVisaCountry").val("-")
                    $("#selService").val("-")
                    GetCurrency(result.CurrencyImage);

                }
                $('#sp_VisaFee').text("0.00")
                $('#sp_UrgentFee').text("0.00");
                $('#OtherFee').text("0.00");
                $('#sp_TotalPer').text("0.00");
                $('#sp_Total').text("0.00")
            }
        },
        error: function () {
            $("#spUniqueCode").text("No Record Found")
            $("#spAvailableLimit").text(0)
            $("#spOneTimeCreditLimit").text(0)
            $("#spAgent").text("No Record Found")
        }
    });
}
function GetVisaByOtb(uid) {
    $("#tbl_PassengerDetails tbody tr").remove()
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/GetVisaByOTB",
        data: '{"AgentId":"' + uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            OtbRefNo = result.OtbRefNo;
            var trow;
            if (result.retCode == 1) {
                var arrVisa = result.tbl_Visa;
                $("#tbl_PassengerDetails tbody tr").remove();


                for (var i = 0; i < arrVisa.length; i++) {
                    var Service = arrVisa[i].IeService
                    if (Service == "96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)")
                        Service = "96 hours Visa transit Single";
                    var Pnr = arrVisa[i].ArrivalflightNo;
                    if (Pnr == "")
                        Pnr = "";
                    trow += '<tr>'
                    trow += '<td style="width: 5%;"><input type="checkbox" id="' + arrVisa[i].sid + '" value="' + arrVisa[i].Vcode + '" name="type" onclick="GenratePass()" class="mycheck"></td>';
                    trow += '<td>' + arrVisa[i].FirstName + " " + arrVisa[i].LastName + '</td>';
                    trow += '<td>' + arrVisa[i].VisaNo + '</td>';
                    trow += '<td style="align:center" id="' + (i + 1) + '"> </td>';
                    trow += '<td>' + Service + '</td>';
                    trow += '<td>' + arrVisa[i].PassportNo + '</td>';
                    trow += '</tr>'
                    GetPresentNationality(arrVisa[i].PresentNationality, (i + 1))
                }
                $("#tbl_PassengerDetails").append(trow)
            }
            else {
                $("#tbl_PassengerDetails").append('<tr><td style="align:center;text-align: center" colspan="6"> No Record Found </td></tr>')
            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
            Success("An error occured while loading Visa")
        }
    });
}
function GetPresentNationality(PresentNationality, I) {
    $.ajax({
        type: "POST",
        url: "../Handler/VisaDetails.asmx/GetCountry",
        data: '{"Code":"' + PresentNationality + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Country = result.tbl_VisaCountry;
                $('#' + (I)).append(Country[0].Country);
            }
        },
        error: function () {
            //$('#SpnMessege').text("An error occured")
            Success("An error occured")
        }
    });
}
function GetAirportsList() {
    var Airline = $("#Sel_AirLineIn").val();
    $("#DivDOB").css("display", "none")
    $("#DivPassportNo").css("display", "none")
    $("#DivIssue").css("display", "none")
    $("#DivCountry").css("display", "none")
    $("#Divtxt_Contact").css("display", "none")

    if (Airline == "6E"){
        $("#DivDOB").css("display", "")
        $("#DivPassportNo").css("display", "")
        $("#DivIssue").css("display", "")
        $("#DivCountry").css("display", "")
        $("#Divtxt_Contact").css("display", "")
    }
    if (Airline != "EK") {
        $("#Sel_ArivalFrom").empty();
        var Airports = $("#Sel_AirLineIn option:selected").text();
        $.ajax({
            type: "POST",
            url: "../Handler/OTBHandler.asmx/GetAirports",
            data: '{ "Airlines": "' + Airports + '" }',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var arrAirports = result.tbl_Airports;

                    var DeprtureFromOption;
                    DeprtureFromOption += ' <option selected="selected" value="-">---Select Airports----</option>'
                    for (var i = 0; i < arrAirports.length; i++) {
                        DeprtureFromOption += '<option value="' + arrAirports[i].CityCode + '">' + arrAirports[i].Origin + '</option>'
                    }
                    $("#Sel_ArivalFrom").append(DeprtureFromOption);
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });

    }
    else {
        if (confirm("Are You want to Make Emirates Otb? ") == true) {
            GetOtbCharges();
        }
    }

}

function SelectOtbType() {
    if ($("#rdb_Cut").is(":checked")) {
        $("#tbl_VisaList").css("display", "")
        $("#AddPassenge1rDetails").css("display", "none")
        $("#lblNopix").css("display", "none")
        $("#DivSelect_PassNo").css("display", "none")
        $("#lblPnr").css("display", "none")
        $("#PassengerDetails").css("display", "none")
        $("#OptJet").css("display", "")
        $("#OptEmirates").css("display", "")
    }
    else {
        $("#tbl_VisaList").css("display", "none")
        $("#lblNopix").css("display", "")
        $("#AddPassenge1rDetails").css("display", "")
        $("#lblPnr").css("display", "")
        $("#PassengerDetails").css("display", "")
        $('input:checkbox').removeAttr('checked');
        $("#OptJet").css("display", "none")
        $("#OptEmirates").css("display", "none")
        $("#DivSelect_PassNo").css("display", "")
        
    }
}

//function SelectOtbType(id) {
//    if (id == "rdb_Cut") {
//        $("#tbl_VisaList").css("display", "")
//        $("#lblNopix").css("display", "none")
//        $("#Select_PassNo").css("display", "none")
//        $("#lblPnr").css("display", "none")
//        $("#PassengerDetails").css("display", "none")
//        $("#OptJet").css("display", "")
//        $("#OptEmirates").css("display", "")
//    }
//    else {
//        $("#tbl_VisaList").css("display", "none")
//        $("#lblNopix").css("display", "")
//        $("#Select_PassNo").css("display", "")
//        $("#lblPnr").css("display", "")
//        $("#PassengerDetails").css("display", "")
//        $('input:checkbox').removeAttr('checked');
//        $("#OptJet").css("display", "none")
//        $("#OptEmirates").css("display", "none")
//    }

//}
var NoOfPass;
function GenratePassengerDetails(Value) {
    NoOfPass = Value;
    $("#sp_NoofPax").text(NoOfPass)
    $("#sp_Total").text(Math.round(NoOfPass * TotalPerPass))
    var Details;
    $("#AddPassengerDetails").empty();
    if (Value != 1) {
        for (var i = 1; i < Value; i++) {
            Details += '<h4>Passenger ' + (i + 1) + '</h4>'
                Details += '<hr />'
                Details += '<div class="columns">'
                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>First Name:</label>'
                Details += '<div class="input full-width">'
                Details += '<input type="text" id="txtFirstName' + (i + 1) + '" placeholder="First Name" class="input-unstyled full-width">'
                Details += '</div>'
                Details += '</div>'
                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>Last Name:</label>'
                Details += '<div class="input full-width">'
                Details += '<input type="text" id="txtLastName' + (i + 1) + '" placeholder="Last Name" class="input-unstyled full-width">'
                Details += '</div>'
                Details += '</div>'

            //  For Indiogo 
                var Airline = $("#Sel_AirLineIn").val();
                if (Airline == "6E") {
                    Details += '<div class="three-columns twelve-columns-mobile four-columns-tablet bold" id="DivDOB" >                '
                    Details += '<label>Date of Birth*:</label>                                                                         '
                    Details += '<span class="input full-width">                                                                        '
                    Details += '<span class="icon-calendar"></span>                                                                    '
                    Details += '<input type="text" name="datepicker" id="td_datepickerDob' + (i + 1) + '" class="input-unstyled datepicker" value="">'
                    Details += '</span>                                                                                                '
                    Details += '</div>                                                                                                 '
                    Details += '<div class="three-columns twelve-columns-mobile four-columns-tablet bold" id="DivPassportNo" >         '
                    Details += '<label>Passport No.*:</label>                                                                          '
                    Details += '<span class="input full-width">                                                                        '
                    Details += '<input type="text" " id="txt_Passport' + (i + 1) + '" class="input-unstyled " value="">                              '
                    Details += '</span>                                                                                                '
                    Details += '</div>                                                                                                 '
                    Details += '<div class="three-columns twelve-columns-mobile four-columns-tablet bold" id="DivIssue">               '
                    Details += '<label>Place of Issue:</label>                                                                         '
                    Details += '<span class="input full-width">                                                                        '
                    Details += '<input type="text"  id="txt_PlaceIssue' + (i + 1) + '" class="input-unstyled" value="">                              '
                    Details += '</span>                                                                                                '
                    Details += '</div>                                                                                                 '
                    Details += '<div class="three-columns twelve-columns-mobile four-columns-tablet bold" id="DivCountry">             '
                    Details += '<label>Issuing Country.*</label>                                                                       '
                    Details += '<span class="input full-width">                                                                        '
                    Details += '<input type="text"  id="txt_IssuinCon' + (i + 1) + '" class="input-unstyled" value="">                               '
                    Details += '</span>                                                                                                '
                    Details += '</div>                                                                                                 '
                }
          
               

            //    Details += '<br /> <div class="row indigo"  >'
            //    Details += '<div class="col-md-2">'
            //    Details += '<br>'
            //    Details += '<span class="text-left">Date of Birth* :</span>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-4">'
            //    Details += '<input class="form-control mySelectCalendar" type="text" style="margin-top: 3%; cursor: pointer" id="td_datepickerDob' + (i + 1) + '" placeholder="dd-mm-yyyy">'
            //    Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_td_datepickerDob' + (i + 1) + '">'
            //    Details += '<b>* This field is required</b></label>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-2">'
            //    Details += '<br /> '
            //    Details += '<span class="text-left"> Passport No.*:</span>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-4">'
            //    Details += '<input class="form-control" type="text" id="txt_Passport' + (i + 1) + '"> '
            //    Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Passport' + (i + 1) + '"> '
            //    Details += '<b>* This field is required</b></label> '
            //    Details += '</div> '
            //    Details += '</div><br>'

            //    Details += '<div class="row indigo"  >'
            //    Details += '<div class="col-md-2">'
            //    Details += '<br /> '
            //    Details += '<span class="text-left">Place of Issue* :</span>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-4">'
            //    Details += '<input class="form-control" type="text" id="txt_PlaceIssue' + (i + 1) + '">'
            //    Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_PlaceIssue' + (i + 1) + '">'
            //    Details += '<b>* This field is required</b></label>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-2">'
            //    Details += '<br /> '
            //    Details += '<span class="text-left"> Issuing Country.*:</span>'
            //    Details += '</div>'
            //    Details += '<div class="col-md-4">'
            //// Details += '<br>'
            //    Details += '<input class="form-control" type="text" id="txt_IssuinCon' + (i + 1) + '">'
            //    Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_IssuinCon' + (i + 1) + '">'
            //    Details += '<b>* This field is required</b></label>'
            //    Details += '</div>'
            //    Details += '</div>'

                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>Visa Type:</label>'
                Details += '<div class="full-width button-height Groupmark">'
                Details += '<select id="Sel_VisaType' + (i + 1) + '" class="select">'
                Details += '<option value="-" selected="selected">--Salect Visa Type--</option>'
                Details += '<option value="6">96 hours Visa transit Single Entery</option>'
                Details += '<option value="1">14 days Service VISA</option>'
                Details += '<option value="2">30 days Tourist Single Entry</option>'
                Details += '<option value="3">30 days Tourist Multiple Entry</option>'
                Details += '<option value="4">90 days Tourist Single Entry</option>'
                Details += '<option value="5">90 days Tourist Multiple Entry</option>'
                Details += '</select>'
                Details += '</div>'
                Details += '</div>'
                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>Visa No:</label>'
                Details += '<div class="input full-width">'
                Details += '<input type="text" id="txt_VisaNo' + (i + 1) + '" class="input-unstyled full-width">'
                Details += '</div>'
                Details += '</div>'
                Details += '</div>'
                Details += '<div class="columns">'
                Details += '<div class="three-columns twelve-columns-mobile four-columns-tablet bold">'
                Details += '<label>Date of Issue:</label>'
                Details += '<span class="input full-width">'
                Details += '<span class="icon-calendar"></span>'
                Details += '<input type="text" name="datepicker" id="td_datepickerDI' + (i + 1) + '" class="input-unstyled datepicker" value="">'
                Details += '</span>'
                Details += '</div>'
                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>Sponsor Name:</label>'
                Details += '<div class="input full-width">'
                Details += '<input type="text" id="txt_Sponsor' + (i + 1) + '" class="input-unstyled full-width">'
                Details += '</div>'
                Details += '</div>'
                Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile">'
                Details += '<label>Upload Ticket Copy:</label><input type="file" name="special-input-1" id="UploadVisa' + (i + 1) + '" Details += onchange="sendfiles(this.id);" title="Only .pdf file allwed" value="" class="file" multiple>'
                Details += '</div>'
                Details += '<img style="-webkit-user-select: none; margin-left: 10%; display: none" id="img_UploadVisa' + (i + 1) + '" src="../images/loader.gif">'
                Details += '</div>'
                var Airline = $("#Sel_AirLineIn").val();
                if (Airline == "6E") {
                    Details += '<div class="three-columns four-columns-tablet twelve-columns-mobile" id="Divtxt_Contact">'
                    Details += ' <label>Contact No:</label>'
                    Details += ' <div class="input full-width">'
                    Details += ' <input type="text" id="txt_Contact' + (i + 1) + '" class="input-unstyled full-width">'
                    Details += ' </div>'
                    Details += '</div>'
                }
              

            //Details += '<br/><hr style="width: 80%; border: 1px solid #D3D3D3;/">'
            //Details += '<div class="tab-content5">'
            //Details += '<div class="tab-pane padding20 active">'
            //Details += '<div class="col-md-12 offset-0">'
            //Details += '<div class="row">'
            //Details += '<div class="col-md-12"><p class="lato size20  lblue">Passenger' + (i + 1) + '</p><div class="line2"></div> </div>'
            //Details += '</div>'
            //Details += '<br/><div class="row">'
            //Details += '<div class="col-md-2"><br/><span class="text-left">First Name *  :</span></div>'
            //Details += '<div class="col-md-4"><input class="form-control" type="text" style="margin-top: 3%; cursor: pointer" id="txtFirstName' + (i + 1) + '" placeholder="First Name"><label style="color: red; margin-top: 3px; display: none" id="lbl_txtFirstName' + (i + 1) + '"><b>* This field is required</b></label> </div>'
            //Details += '<div class="col-md-2"><br/><span class="text-left">Last Name * :</span></div>'
            //Details += '<div class="col-md-4"><input class="form-control" type="text" style="margin-top: 3%; cursor: pointer" id="txtLastName' + (i + 1) + '" placeholder="Last Name"><label style="color: red; margin-top: 3px; display: none" id="lbl_txtLastName' + (i + 1) + '"><b>* This field is required</b></label></div>'
            //Details += ' </div>'


            ////  For Indiogo 
            //Details += '<br /> <div class="row indigo"  >'
            //Details += '<div class="col-md-2">'
            //Details += '<br>'
            //Details += '<span class="text-left">Date of Birth* :</span>'
            //Details += '</div>'
            //Details += '<div class="col-md-4">'
            //Details += '<input class="form-control mySelectCalendar" type="text" style="margin-top: 3%; cursor: pointer" id="td_datepickerDob' + (i + 1) + '" placeholder="dd-mm-yyyy">'
            //Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_td_datepickerDob' + (i + 1) + '">'
            //Details += '<b>* This field is required</b></label>'
            //Details += '</div>'
            //Details += '<div class="col-md-2">'
            //Details += '<br /> '
            //Details += '<span class="text-left"> Passport No.*:</span>'
            //Details += '</div>'
            //Details += '<div class="col-md-4">'
            //Details += '<input class="form-control" type="text" id="txt_Passport' + (i + 1) + '"> '
            //Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Passport' + (i + 1) + '"> '
            //Details += '<b>* This field is required</b></label> '
            //Details += '</div> '
            //Details += '</div><br>'

            //Details += '<div class="row indigo"  >'
            //Details += '<div class="col-md-2">'
            //Details += '<br /> '
            //Details += '<span class="text-left">Place of Issue* :</span>'
            //Details += '</div>'
            //Details += '<div class="col-md-4">'
            //Details += '<input class="form-control" type="text" id="txt_PlaceIssue' + (i + 1) + '">'
            //Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_PlaceIssue' + (i + 1) + '">'
            //Details += '<b>* This field is required</b></label>'
            //Details += '</div>'
            //Details += '<div class="col-md-2">'
            //Details += '<br /> '
            //Details += '<span class="text-left"> Issuing Country.*:</span>'
            //Details += '</div>'
            //Details += '<div class="col-md-4">'
            //// Details += '<br>'
            //Details += '<input class="form-control" type="text" id="txt_IssuinCon' + (i + 1) + '">'
            //Details += '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_IssuinCon' + (i + 1) + '">'
            //Details += '<b>* This field is required</b></label>'
            //Details += '</div>'
            //Details += '</div>'


            //Details += '<div class="row">'
            //Details += '<div class="col-md-2"><br /> <span class="text-left">Visa Type* :</span></div>'
            //Details += '<div class="col-md-4"><select class="form-control" id="Sel_VisaType' + (i + 1) + '" style="margin-top: 2.5%; cursor: pointer"><option value="-" selected="selected">--Salect Visa Type--</option><option value="6">96 hours Visa transit Single Entery</option><option value="1">14 days Service VISA</option><option value="2">30 days Tourist Single Entry</option><option value="3">30 days Tourist Multiple Entry</option><option value="4">90 days Tourist Single Entry</option><option value="5">90 days Tourist Multiple Entry</option></select><label style="color: red; margin-top: 3px; display: none" id="lbl_Sel_VisaType' + (i + 1) + '"><b>* This field is required</b></label></div>'
            //Details += '<div class="col-md-2"><br /> <span class="text-left">Visa No* :</span></div>'
            //Details += '<div class="col-md-4"><input class="form-control" type="text" id="txt_VisaNo' + (i + 1) + '" style="margin-top: 10px"><label style="color: red; margin-top: 3px; display: none" id="lbl_txt_VisaNo' + (i + 1) + '"><b>* This field is required</b></label></div>'
            //Details += '</div><br>'
            //Details += '<div class="row">'
            //Details += '<div class="col-md-2"><br /> <span class="text-left">Date of Issue* :</span></div>'
            //Details += '<div class="col-md-4"><input class="form-control mySelectCalendar" type="text" style="margin-top: 3%; cursor: pointer" id="td_datepickerDI' + (i + 1) + '" placeholder="dd-mm-yyyy"><label style="color: red; margin-top: 3px; display: none" id="lbl_td_datepickerDIn' + (i + 1) + '"><b>* This field is required</b></label></div>'
            //Details += '<div class="col-md-2"><br><span class="text-left">Sponsor Name* :</span></div>'
            //Details += '<div class="col-md-4"><input class="form-control" type="text" style="margin-top: 3%; cursor: pointer" id="txt_Sponsor' + (i + 1) + '" placeholder=""><label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Sponsor' + (i + 1) + '"><b>* This field is required</b></label></div>'

            //Details += '</div> <br/>'
            //Details += '<div class="row">'
            //Details += '<div class="col-md-2"><br><span class="text-left opensans size15 orange">Upload Visa Copy* :</span></div>'
            //Details += '<div class="col-md-4"><input type="file" id="UploadVisa' + (i + 1) + '" onchange="sendfiles(this.id);" class="btn-search5 right" data-toggle="tooltip" data-placement="bottom"  title="Only .pdf file allwed" style="width: 100%;" accept="application/pdf" /><label style="color: red; margin-top: 3px; display: none" id="lbl_UploadVisa' + (i + 1) + '"><b>* This field is required</b></label><img style="-webkit-user-select: none;MARGIN-LEFT: 10%;display: none" id="img_UploadVisa' + (i + 1) + '" src="../images/loader.gif"></div>'

            //Details += ' <div class="indigo"> <div class="col-md-2 " style="padding-top: 1%">'
            //Details += ' <span class="text-left">Contact No * :</span> </div>'
            //Details += '<div class="col-md-4"> <input type="text" id="txt_Contact' + (i + 1) + '"   class="form-control" /><label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Contact' + (i + 1) + '"><b>* This field is required</b></label></div></div>'
            //Details += '</div>'
            //Details += '<div class="clearfix"></div>'
            //Details += '</div>'
        }
        var S = Details.split('undefined')
        // For Indigo
        $(".indigo").css("display", "none");
        if ($("#Sel_AirLineIn").val() == "6E")
            $(".indigo").css("display", "");
        $("#AddPassengerDetails").append(S[1])
        $('[data-toggle="tooltip"]').tooltip();
        for (var j = 0; j < Value; j++) {
            $('#td_datepickerDI' + (j + 2)).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                maxDate: new Date(),
            });
            $('#td_datepickerDob' + (j + 2)).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                maxDate: new Date(),
            });
        }
    }
}
///Check For Unique Pnr //
function GenratePass() {
    debugger;
    var selchbox = [];
    var inpfields = document.getElementsByTagName('input');
    var nr_inpfields = inpfields.length;
    for (var i = 0; i < nr_inpfields; i++) {
        if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true && inpfields[i].name == 'type') {
            selchbox.push(inpfields[i].value);
            //selchbox[i] = inpfields[i].value
        }
    }
    $("#sp_NoofPax").text(selchbox.length)
    $("#sp_Total").text(Math.round(selchbox.length * TotalPerPass))
}
function CheckPnr() {
    var ArivalPnr = $("#txt_PNRIn").val();
    var DepartPnr = $("#txt_PNROut").val();
    if (ArivalPnr != "" && DepartPnr != "") {
        if (DepartPnr == ArivalPnr) {
            $("#spn_UploadCopy").css("display", "none")
            $("#UploadOutbound").css("display", "none")
            $("#lbl_UploadOutbound").css("display", "none")
        }
        else {
            $("#spn_UploadCopy").css("display", "")
            $("#UploadOutbound").css("display", "")

        }
    }

}

function GetOtbCharges() {
    $("#sp_VisaFee").text(0);
    $("#sp_UrgentFee").text(0);
    $("#OtherFee").text(0);
    $("#sp_TotalPer").text(0);
    $("#sp_Total").text(0);
    var selchbox = [];
    var Airline = $("#Sel_AirLineIn").val();
    var Supplier = $("#Sel_AirLineIn  option:selected").text();
    var Airports = $("#Sel_ArivalFrom").val();
    var type = document.getElementById('rdb_Cut')
    var OtbType;
    if ($("#rdb_Normal").prop("checked")) {
        OtbType = "Normal"

    }
    else {
        OtbType = "Urgent"
    }
    var inpfields = document.getElementsByTagName('input');
    var nr_inpfields = inpfields.length;
    for (var i = 0; i < nr_inpfields; i++) {
        if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true) {
            selchbox.push(inpfields[i].value);
            //selchbox[i] = inpfields[i].value
        }
    }
    if (selchbox.length != 0 || type.checked == false && $('#hdnDCode').val() != "") {
        if (Airline != "-" && Airports != "-") {
            var data = {
                AgentId: $('#hdnDCode').val(),
                Airline: Airline,
                Airports: Airports,
                RefrenceNo: selchbox,
                OtbType: OtbType,
                Supplier: Supplier
            }
            var Jason = JSON.stringify(data);
            $.ajax({
                type: "POST",
                url: "../Handler/OTBHandler.asmx/GetAirlinesOtbCharges",
                data: Jason,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1 && result.Emirates == 0) {
                        VisaFee = result.VisaCharges;
                        UrgentCharges = result.UrgentCharges;
                        otherCharges = result.OtherCarges;
                        TotalPerPass = result.Chargesper;
                        NoOfPass = $("#sp_NoofPax").text()
                        $("#sp_VisaFee").text(VisaFee)
                        $("#sp_UrgentFee").text(UrgentCharges)
                        $("#OtherFee").text(result.OtherCarges)
                        $("#sp_TotalPer").text(TotalPerPass)
                        $("#sp_Total").text(Math.round((TotalPerPass * NoOfPass) * 1000) / 1000)
                        EmailFormat = result.EmailFormat
                        Email1 = result.Email1;
                        CCEmailId = result.CCEmailId;
                        Email2 = result.Email2;
                        GetCurrency(result.AgentCurrency);

                    }
                    else if (result.retCode == 1 && result.Emirates == 1) {
                        if (Airline == "EK") {
                            window.open("http://www.emirates.com/english/help/contact-emirates/ptok/planning-travel/ok-to-board")
                        }
                        else {
                            Success("Otb Not Required for Jet Airways!!")

                            // alert ("Otb Not Required for Jet Airways!!")
                        }
                    }
                    if (result.retCode == 0) {

                    }
                },
                error: function () {
                    Success("An error occured while Geting Otb Charges")

                    // alert("An error occured while Geting Otb Charges")
                }
            });

        }
    }
    else if (selchbox.length == 0 || type.checked == true) {
        Success("Please Select Passenger From Given List");

        // alert("Please Select Passenger From Given List");
        $("#Sel_ArivalFrom").val('-');
        window.location.href = "#cutotbList";

    }
    else {
        $('#txt_Agent').focus()
        Success("Please select Agent First")
    }


}
// Aplly for Otb //
var bValid; var IsIndigo = false;
function ApplyForOtb() {

    var sRdbType;
    var selchbox = [];
    if ($("#rdb_Cut").prop("checked")) {
        bValid = ValidateOther();
        if (bValid == true) {
            $("#image").css("display", "")
            var inpfields = document.getElementsByTagName('input');
            var nr_inpfields = inpfields.length;
            for (var i = 0; i < nr_inpfields; i++) {
                if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true && inpfields[i].name == 'type') {
                    selchbox.push(inpfields[i].value);
                    //selchbox[i] = inpfields[i].value
                }
            }
            if (selchbox.length != 0) {
                if ($("#rdb_Normal").prop("checked"))
                    sRdbType = "Normal";
                else
                    sRdbType = "Urgent";
                var arrayJson1 = JSON.stringify(selchbox);
                ArrivalAirLine = $("#Sel_AirLineIn option:selected").text();
                DepartingAirline = $("#Sel_DeprtingAirLineIn option:selected").text();
                Arrivalfrom = $("#Sel_ArivalFrom option:selected").text();
                DepartingFrom = $("#txt_DeprtureFrom").val();
                DepartingDate = $("#td_datepickerDdateOut").val();
                ArrivalDate = $("#dt_datepickerDdateIn").val();
                //ArrivingFlightNo = $("#txt_FlightNoIn").val();
                ArrivingFlightNo = "";
                //DerartingFlightNo = $("#txt_FlightNoOut").val();
                DerartingFlightNo = "";
                ArrivingPnr = $("#txt_PNRIn").val();
                DerartingPnr = $("#txt_PNROut").val();
                ArrivalTicketCopy = $("#UploadInbound").val();
                DepartingTicketCopy = $("#UploadOutbound").val();
                VisaFee = $("#sp_VisaFee").text();
                UrgentCharges = $("#sp_UrgentFee").text();
                otherCharges = $("#OtherFee").text();
                TotalPerPass = $("#sp_TotalPer").text();
                NoOfPax = $("#sp_NoofPax").text();
                TotalAmmount = $("#sp_Total").text();
                if (otherAirlines.checked == false) {
                    DepartingAirline = $("#Sel_DeprtingAirLineIn option:selected").text();
                }
                else {
                    DepartingAirline = ArrivalAirLine;
                }
                if (otherpnr.checked == false) {
                    DerartingPnr = $("#txt_PNROut").val();
                    DepartingTicketCopy = $("#UploadOutbound").val();
                }
                else {
                    DerartingPnr = $("#txt_PNRIn").val();
                    DepartingTicketCopy = "";
                }
                var data = {
                    Uid: $('#hdnDCode').val(),
                    ArrivalAirLine: ArrivalAirLine,
                    DepartingAirline: DepartingAirline,
                    Arrivalfrom: Arrivalfrom,
                    DepartingFrom: DepartingFrom,
                    DepartingDate: DepartingDate,
                    ArrivalDate: ArrivalDate,
                    ArrivingFlightNo: ArrivingFlightNo,
                    DerartingFlightNo: DerartingFlightNo,
                    ArrivingPnr: ArrivingPnr,
                    DerartingPnr: DerartingPnr,
                    ArrivalTicketCopy: ArrivalTicketCopy,
                    DepartingTicketCopy: DepartingTicketCopy,
                    VisaFee: VisaFee,
                    UrgentCharges: UrgentCharges,
                    OtherCharges: otherCharges,
                    TotalPerPass: TotalPerPass,
                    PaxNo: NoOfPax,
                    TotalAmmount: TotalAmmount,
                    RefNo: selchbox,
                    sRdbType: sRdbType,
                    EmailFormat: EmailFormat,
                    Email1: Email1,
                    Email2: Email2,
                    CCEmailId: CCEmailId,
                    OtbRefNo: OtbRefNo
                }
                var arrayJson2 = JSON.stringify(data);
                var arrayJson = arrayJson1 + arrayJson2;
                $.ajax({
                    type: "POST",
                    url: "../Handler/OTBHandler.asmx/ApplyOtbCut",
                    data: arrayJson2,
                    // data: '{"sVRefNo":"' + sVRefNo + '","sFName":"' + sFName + '","sVisaNo":"' + sVisaNo + '","sVType":"' + sVType + '","sDateIssue":"' + sDateIssue + '","sVisaFee":"' + sVisaFee + '","sUrgFee":"' + sUrgFee + '","sServiceCh":"' + sServiceCh + '","sTotal":"' + sTotal + '","sInAirline":"' + sInAirline + '","sInDepDate":"' + sInDepDate + '","sInFlightNo":"' + sInFlightNo + '","sInPNR":"' + sInPNR + '","sOutAirline":"' + sOutAirline + '","sOutDepDate":"' + sOutDepDate + '","sOutFlightNo":"' + sOutFlightNo + '","sOutPNR":"' + sOutPNR + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1 && result.Mail == 1) {
                            $("#image").css("display", "none")
                            Success("Add OTB Record SuccessFully.");
                            //$('#ModelMessege').modal('show')
                            // alert("Add OTB Record SuccessFully.");
                            Ok("Otb Request Accepted", "Location", null)
                        }
                        else if (result.retCode == 1 && result.Mail == 0) {
                            Success("Your Application is Proceed after Admin Approval..")

                            // alert("Your Application is Proceed after Admin Approval..")
                        }
                        else if (result.retCode == 0 && result.Insufficent == 0) {
                            Success("Your does not have Sufficent balance to fill this Application..")

                            //alert("Your does not have Sufficent balance to fill this Application..")
                        }
                        else if (result.retCode == 0 && result.Session == 0) {
                            Success("Please Login Again to fill this Application..")

                            // alert("Please Login Again to fill this Application..")
                        }
                    },
                    error: function () {
                        Success("An error occured while Applying OTB Service")

                        // alert("An error occured while Applying OTB Service")
                    }
                });

            }
            else {
                Success("Please Select Passenger From Given List");

                //  alert("Please Select Passenger From Given List");
                window.location.href = "#cutotbList";
            }

        }

    }
    else {
        bValid = ValidateOther()
        if (bValid == true) {
            $("#image").css("display", "")
            var FirstName = [];
            var LastName = [];
            var DOB = [];
            var OtherPassportNo = [];
            var PlaceIssue = [];
            var IssueCountry = [];
            var VisaNo = [];
            var VisaType = [];
            var Issue = [];
            var PassportNo = [];
            var VisaCopy = [];
            var Sponsor = [];
            var SponsorNo = [];
            var ContactNo = [];
            var j;
            var type;
            for (var i = 0; i < NoOfPass; i++) {
                j = (i + 1);
                FirstName[i] = $("#txtFirstName" + (j)).val();
                LastName[i] = $("#txtLastName" + (j)).val();
                if (IsIndigo == true) {
                    DOB[i] = $("#td_datepickerDob" + (j)).val();
                    OtherPassportNo[i] = $("#txt_Passport" + (j)).val();
                    PlaceIssue[i] = $("#txt_PlaceIssue" + (j)).val();
                    IssueCountry[i] = $("#txt_IssuinCon" + (j)).val();
                    ContactNo[i] = $("#txt_Contact" + (j)).val();
                    PassportNo[i] = $("#txt_Passport" + (j)).val();
                }
                else {
                    DOB[i] = "";
                    OtherPassportNo[i] = "";
                    PlaceIssue[i] = "";
                    IssueCountry[i] = "";
                    ContactNo[i] = "";
                    PassportNo[i] = "";
                }
                type = "#Sel_VisaType" + (j) + " " + "option:selected";
                VisaType[i] = $(type).text();
                VisaNo[i] = $("#txt_VisaNo" + (j)).val();
                Issue[i] = $("#td_datepickerDI" + (j)).val();
                //PassportNo[i] = $("#txt_PassportNo" + (j)).val();

                VisaCopy[i] = $("#UploadVisa" + (j)).val();
                Sponsor[i] = $("#txt_Sponsor" + (j)).val();
                //SponsorNo[i] = $("#txt_Contact" + (j)).val();
                SponsorNo[i] = "";
            }
            if ($("#rdb_Normal").prop("checked"))
                otbType = "Normal";
            else
                otbType = "Urgent";
            sRdbType = "Other";
            var arrayJson1 = JSON.stringify(selchbox);
            ArrivalAirLine = $("#Sel_AirLineIn option:selected").text();
            Arrivalfrom = $("#Sel_ArivalFrom option:selected").text();
            ArrivalDate = $("#dt_datepickerDdateIn").val();
            ArrivingPnr = $("#txt_PNRIn").val();
            ArrivalTicketCopy = $("#UploadInbound").val();
            DepartingFrom = $("#txt_DeprtureFrom").val();
            DepartingDate = $("#td_datepickerDdateOut").val();
            if (otherAirlines.checked == false) {
                DepartingAirline = $("#Sel_DeprtingAirLineIn option:selected").text();
            }
            else {
                DepartingAirline = ArrivalAirLine;
            }
            if (otherpnr.checked == false) {
                DerartingPnr = $("#txt_PNROut").val();
                DepartingTicketCopy = $("#UploadOutbound").val();
            }
            else {
                DerartingPnr = $("#txt_PNRIn").val();
                DepartingTicketCopy = "";
            }
            //ArrivingFlightNo = $("#txt_FlightNoIn").val();
            ArrivingFlightNo = "";
            //DerartingFlightNo = $("#txt_FlightNoOut").val();
            DerartingFlightNo = "";
            VisaFee = $("#sp_VisaFee").text();
            UrgentCharges = $("#sp_UrgentFee").text();
            otherCharges = $("#OtherFee").text();
            TotalPerPass = $("#sp_TotalPer").text();
            NoOfPax = $("#sp_NoofPax").text();
            TotalAmmount = $("#sp_Total").text();
            var data = {
                Uid: $('#hdnDCode').val(),
                ArrivalAirLine: ArrivalAirLine,
                DepartingAirline: DepartingAirline,
                Arrivalfrom: Arrivalfrom,
                DepartingFrom: DepartingFrom,
                DepartingDate: DepartingDate,
                ArrivalDate: ArrivalDate,
                ArrivingFlightNo: ArrivingFlightNo,
                DerartingFlightNo: DerartingFlightNo,
                ArrivingPnr: ArrivingPnr,
                DerartingPnr: DerartingPnr,
                ArrivalTicketCopy: ArrivalTicketCopy,
                DepartingTicketCopy: DepartingTicketCopy,
                VisaFee: VisaFee,
                UrgentCharges: UrgentCharges,
                OtherCharges: otherCharges,
                TotalPerPass: TotalPerPass,
                PaxNo: NoOfPax,
                TotalAmmount: TotalAmmount,
                FirstName: FirstName,
                LastName: LastName,
                VisaType: VisaType,
                VisaNo: VisaNo,
                Issue: Issue,
                PassportNo: PassportNo,
                VisaCopy: VisaCopy,
                sRdbType: sRdbType,
                otbType: otbType,
                EmailFormat: EmailFormat,
                Email1: Email1,
                Email2: Email2,
                CCEmailId: CCEmailId,
                Sponsor: Sponsor,
                SponsorNo: SponsorNo,
                OtbRefNo: OtbRefNo,
                // Indigo Airlines //
                DOB: DOB,
                OtherPassportNo: OtherPassportNo,
                PlaceIssue: PlaceIssue,
                IssueCountry: IssueCountry,
                ContactNo: ContactNo,
            }
            var arrayJson = JSON.stringify(data);
            $.ajax({
                type: "POST",
                url: "../Handler/OTBHandler.asmx/ApplyOtbOther",
                data: arrayJson,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1 && result.Mail == 1) {
                        $("#image").css("display", "none")
                        Ok("Otb Request Accepted", "Location", null)
                    }
                    else if (result.retCode == 1 && result.Mail == 0) {
                        $("#image").css("display", "none")
                        Success("Your Application is Proceed after Admin Approval..")
                    }
                    else if (result.retCode == 0 && result.Insufficent == 0) {
                        $("#image").css("display", "none")
                        Success("Your does not have Sufficent balance to fill this Application..")
                    }
                    else if (result.retCode == 0 && result.Session == 0) {
                        $("#image").css("display", "none")
                        Success("Please Login Again to fill this Application..")
                    }
                },
                error: function () {
                    $("#image").css("display", "none")
                    Success("An error occured while Applying OTB Service")
                }
            });

        }

    }

}
function Location() {
    $(window).unbind();
    window.location.reload();
}
// End Aplly for Otb //

function ValidateOther() {
    IsIndigo = false;
    if ($('.indigo').is(':visible'))
        IsIndigo = true;
    debugger;
    if ($("#Sel_AirLineIn").val() == "-") {
        $("#Sel_AirLineIn").focus();
        Success("Please select Airlines");
        return bValid = false;
    }
   
    if ($("#Sel_ArivalFrom").val() == "-") {
        $("#Sel_ArivalFrom").focus();
        Success("Please select ArivalFrom");
        return bValid = false;
    }
   
    if ($("#dt_datepickerDdateIn").val() == "") {
        $("#dt_datepickerDdateIn").focus();
        Success("Please select Departure Date");
        return bValid = false;
    }
   
    if ($("#txt_PNRIn").val() == "") {
        $("#txt_PNRIn").focus();
        Success("Please enter PNR");
        return bValid = false;
    }
  
    if ($("#UploadInbound").val() == "") {
        $("#UploadInbound").focus();
        Success("Please Upload Ticket Copy");
        return bValid = false;
    }
  
    if (otherAirlines.checked == false) {
        if ($("#Sel_DeprtingAirLineIn").val() == "-") {
            $("#Sel_DeprtingAirLineIn").focus();
            Success("Please select Departing Airlines");
            return bValid = false;
        }
       
    }
    if ($("#txt_DeprtureFrom").val() == "") {
        $("#txt_DeprtureFrom").focus();
        Success("Please select DeprtureFrom");
        return bValid = false;
    }
   
    if ($("#td_datepickerDdateOut").val() == "") {
        $("#td_datepickerDdateOut").focus();
        Success("Please select Departure Date");
        return bValid = false;
    }
 

    if (otherpnr.checked == false) {
        if ($("#txt_PNROut").val() == "") {
            $("#txt_PNROut").focus();
            Success("Please enter PNR");
            return bValid = false;
        }
       
        if ($("#txt_PNROut").val() == "") {
            $("#txt_PNROut").focus();
            Success("Please enter PNR");
            return bValid = false;
        }

        if ($("#txt_PNROut").val() != $("#txt_PNRIn").val()) {
            if ($("#UploadOutbound").val() == "") {
                $("#UploadOutbound").focus();
                Success("Please Upload Ticket Copy");
                return bValid = false;
            }
        }
       
    }

    bValid = true;
    if ($("#rdb_Cut").prop("checked") && bValid == true) {
        return bValid = true;
    }
    else if (bValid == true) {
        NoOfPass = $("#Select_PassNo").val();
        for (var i = 0; i < NoOfPass; i++) {
            if ($("#txtFirstName" + (i + 1)).val() == "") {
                $("#txtFirstName" + (i + 1)).focus();
                Success("Please enter First Name");
                return bValid = false;
            }
           
            if ($("#txtLastName" + (i + 1)).val() == "") {
                $("#txtLastName" + (i + 1)).focus();
                Success("Please enter Last Name");
                return bValid = false;
            }
          
            if ($("#Sel_VisaType" + (i + 1)).val() == "-") {
                $("#Sel_VisaType" + (i + 1)).focus();
                Success("Please select Visa Type");
                return bValid = false;
            }
            if (IsIndigo == true) {
                if ($("#td_datepickerDob" + (i + 1)).val() == "") {
                    $("#td_datepickerDob" + (i + 1)).focus();
                    Success("Please select DOB");
                    return bValid = false;
                }
                if ($("#txt_Passport" + (i + 1)).val() == "") {
                    $("#txt_Passport" + (i + 1)).focus();
                    Success("Please enter Passport No");
                    return bValid = false;
                }
                if ($("#txt_PlaceIssue" + (i + 1)).val() == "") {
                    $("#txt_PlaceIssue" + (i + 1)).focus();
                    Success("Please enter Place of Issue");
                    return bValid = false;
                }
                if ($("#txt_IssuinCon" + (i + 1)).val() == "") {
                    $("#txt_IssuinCon" + (i + 1)).focus();
                    Success("Please enter Issuing Country");
                    return bValid = false;
                }
            }
            $("#lbl_txt_VisaNo" + (i + 1)).css("display", "none");
            if ($("#txt_VisaNo" + (i + 1)).val() == "") {
                $("#txt_VisaNo" + (i + 1)).focus();
                Success("Please enter Visa No");
                return bValid = false;
            }
            if ($("#td_datepickerDI" + (i + 1)).val() == "") {
                $("#td_datepickerDI" + (i + 1)).focus();
                Success("Please enter Date of Issue");
                return bValid = false;
            }
            if ($("#txt_Sponsor" + (i + 1)).val() == "") {
                $("#txt_Sponsor" + (i + 1)).focus()
                Success("Please select DOB");
                return bValid = false;
            }
            if ($("#UploadVisa" + (i + 1)).val() == "") {
                $("#UploadVisa" + (i + 1)).focus();
                Success("Please select Upload Ticket Copy");
                return bValid = false;
            }
            if (IsIndigo == true) {
                if ($("#txt_Contact" + (i + 1)).val() == "") {
                    $("#txt_Contact" + (i + 1)).focus();
                    Success("Please enter Contact No");
                    return bValid = false;
                }
            }
        }
        return bValid = true;
    }


}
function GetCurrency(Currency) {
    debugger;
    switch (Currency) {
        case "AED":
            CurrencyClass = "Currency-AED";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);

            break;
        case "SAR":
            CurrencyClass = "Currency-SAR";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);
            break;
        case "EUR":
            CurrencyClass = "fa fa-eur";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);
            break;
        case "GBP":
            CurrencyClass = "fa fa-gbp";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);
            break;
        case "USD":
            CurrencyClass = "fa fa-dollar";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);
            break;
        case "INR":
            CurrencyClass = "fa fa-inr";
            $("#iconCharges").addClass(CurrencyClass);
            $("#UFee").addClass(CurrencyClass);
            $("#other").addClass(CurrencyClass);
            $("#per").addClass(CurrencyClass);
            $("#Total").addClass(CurrencyClass);
            break;
    }

}
//$(document).ready(function () {

//    var ref = setTimeout("GetCurrency('INR')", 1000)
//})

//function OtherPnrAirlines() {
//    if ($("#otherpnr").is(":checked")) {
//        //alert("hiiii");
//        $("#divtxt_PNROut").css("display", "none")
//    }
//    else
//    {
//        $("#divtxt_PNROut").css("display", "")
//    }
//}
//function abcd() {
//    alert("hiiiiii");
//}
function OtherPnrAirlines(id) {
    if (id.checked == true) {

        var Departure = document.getElementsByClassName('Departure-PNR'), i;
        for (i = 0; i < Departure.length; i += 1) {
            Departure[i].style.display = 'none';
        }
    }
    else {
        var Departure = document.getElementsByClassName('Departure-PNR'), i;
        for (i = 0; i < Departure.length; i += 1) {
            Departure[i].style.display = '';
        }

    }
}

function other(id) {
    if (id.checked == true) {

        var Departure = document.getElementsByClassName('Departure'), i;
        for (i = 0; i < Departure.length; i += 1) {
            Departure[i].style.display = 'none';
        }
    }
    else {
        var Departure = document.getElementsByClassName('Departure'), i;
        for (i = 0; i < Departure.length; i += 1) {
            Departure[i].style.display = '';
        }

    }
}