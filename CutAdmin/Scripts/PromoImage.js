﻿$(document).ready(function () {
    GetPromoImageDetail();
});
var hiddensid = 0;
var check;
function GetPromoImageDetail() {
    //$("#tbl_PromoImageDetails").dataTable().fnClearTable();
    //$("#tbl_PromoImageDetails").dataTable().fnDestroy();
    $("#tbl_PromoImageDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "../Handler/PromoImageHandler.asmx/GetPromoImageDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_PromoImageDetails = result.tbl_PromoImage;
                var tRow = "";
                for (var i = 0; i < List_PromoImageDetails.length; i++) {
                    tRow += '<tr>';
                    moment.locale('India Standard Time');
                    List_PromoImageDetails[i].UpdatedDate = moment(List_PromoImageDetails[i].UpdatedDate).format("L");
                    tRow += '<td>' + List_PromoImageDetails[i].ImageName + '</td>                                                                                      '
                    tRow += '<td>' + List_PromoImageDetails[i].UpdatedDate + '</td>                                                                                      '
                    tRow += '<td> '
                    if (List_PromoImageDetails[i].ShowFlag!=false){
                    tRow += '<input type="radio" name="radio" id="rdb' + List_PromoImageDetails[i].ImageName + '" onchange="ChangePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName +'\')" class="radio mid-margin-left checked"></td>              '
                    }
                    else
                    {
                        tRow += '<input type="radio" name="radio" id="rdb' + List_PromoImageDetails[i].ImageName + '" onchange="ChangePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName +'\')" class="radio mid-margin-left"></td> '
                    }
                        //tRow += '<input type="radio" name="radio" id="rdb' + List_PromoImageDetails[i].ImageName + '" onclick="ChangePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName + '\')" value="' + List_PromoImageDetails[i].ShowFlag + '" class="radio mid-margin-left"></td>              '
                    tRow += '<td class="align-center"><span class="button-group children-tooltip">                          '
                    tRow += '<a href="#" class="button" title="Image" onclick="ViewPromoImage(\'' + List_PromoImageDetails[i].ImageName + '\',\'' + List_PromoImageDetails[i].Path + '\')"><span class="fa fa-picture-o"></span></a>'
                    tRow += '<a href="#" class="button" title="Edit" onclick="UpdatePromoImages(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName + '\',\'' + List_PromoImageDetails[i].facility + '\',\'' + List_PromoImageDetails[i].service + '\',\'' + List_PromoImageDetails[i].start + '\',\'' + List_PromoImageDetails[i].details + '\',\'' + List_PromoImageDetails[i].Notes + '\',\'' + List_PromoImageDetails[i].Path + '\')"><span class="icon-pencil"></span></a>   '
                    tRow += '<a href="#" class="button" title="trash" onclick="DeletePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName + '\')"><span class="icon-trash"></span></a>    '
                    tRow += '</span></td>                                                                                             '


                    //tRow += '<td>' + List_PromoImageDetails[i].ImageName.replace(".jpg", "").replace(".bmp", "").replace(".png", "").replace(".jpeg", "").replace(".gif", "") + '</td>';
                    //tRow += '<td>' + List_PromoImageDetails[i].UpdatedDate.split(' ')[0] + '</td>';
                    //tRow += '<td><input type="radio" style="cursor:pointer" name="promoradio" id="rdb' + List_PromoImageDetails[i].ImageName.replace(".jpg", "").replace(".bmp", "").replace(".png", "").replace(".jpeg", "").replace(".gif", "") + '" onclick="ChangePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName.replace(".jpg", "").replace(".jpeg", "").replace(".bmp", "").replace(".gif", "") + '\'); return false" ' + List_PromoImageDetails[i].ShowFlag.replace("True", "checked=checked").replace("False", "") + ' value="' + List_PromoImageDetails[i].ShowFlag + '"></td>';
                   
                    //tRow += '<td><a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-picture" title="View" aria-hidden="true" style="cursor:pointer" data-toggle="modal" data-target="#ImageViewModal" onmouseover="ViewPromoImage(\'' + List_PromoImageDetails[i].ImageName + '\',\'' + List_PromoImageDetails[i].Path + '\')"></span></a> 
                    //    | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-edit" title="Update" aria-hidden="true" style="cursor:pointer" onclick="UpdatePromoImages(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName + '\',\'' + List_PromoImageDetails[i].facility + '\',\'' + List_PromoImageDetails[i].service + '\',\'' + List_PromoImageDetails[i].start + '\',\'' + List_PromoImageDetails[i].details + '\',\'' + List_PromoImageDetails[i].Notes + '\',\'' + List_PromoImageDetails[i].Path + '\')"></span></a>
                    //    | <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeletePromoImage(\'' + List_PromoImageDetails[i].sid + '\',\'' + List_PromoImageDetails[i].ImageName + '\')"></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_PromoImageDetails tbody").html(tRow);
                //$("#tbl_PromoImageDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
        }
    });
}

function ChangePromoImage(sid, name) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to change your Promo Image to At The Top <br/> <span class=\"orange\">?</span></span></p>',
        function () {
            $.ajax({
                type: "POST",
                url: "../Handler/PromoImageHandler.asmx/InsertChangePromoImage",
                data: '{"sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        Success("Something went wrong!");
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Promo Image changed successfully.");
                        setTimeout(function () {
                            window.location.reload();
                            GetPromoImageDetail();
                        }, 2000)
                        //return false;
                    }
                    else {
                    }
                },
                error: function () {
                    Success("An error occured while updating Staff details");
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

function ischecked(flag) {
    if (flag == 'True') {
        return '';
    }
    else
        return 'none';
}

// View Image modal
function ViewPromoImage(imagename, Path) {
    //$("#imgPromo").attr("src", "../PromoImages/" + Path.replace("~", ".."));
    $.modal({
        content: '<div class="imgpop"><p>At The Top</p><img src="../PromoImages/' + Path.replace("~", "..") + '" id="imgPromo" alt=""/></div>',
        title: 'Image View',
        width: 400,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
//function ViewPromoImage(imagename, Path) {
//    $("#spnImageName").html(imagename.replace(".jpg", "").replace(".bmp", "").replace(".png", "").replace(".jpeg", "").replace(".gif", ""));
//    $("#imgPromo").attr("src", "../PromoImages/" + Path.replace("~", ".."));
//}
function DeletePromoImage(sid, name) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete your Promo Image <span class=\"orange\">' + name + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/PromoImageHandler.asmx/DeletePromoImage",
                type: "post",
                data: '{"sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Promo Image deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}
//function DeletePromoImage(sid, name) {
//    Ok("Are you sure you want to delete your Promo Image " + name + "?", "Delete", [sid])
//    //if (confirm("Are you sure you want to delete your Promo Image " + name + "?") == true) {

//    //}
//}
function Delete(sid) {
    $.ajax({
        type: "POST",
        url: "../Handler/PromoImageHandler.asmx/DeletePromoImage",
        data: '{"sid":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                Success("Something went wrong!");
                return false;
            }
            if (result.retCode == 1) {
                Success("Promo Image deleted successfully.");
                GetPromoImageDetail();
                return false;
            }
            else {
            }
        },
        error: function () {
            Success("An error occured while deleting Promo Image");
        }
    });
    Cancel()
}

// Slider Images // 
function UpdatePromoImages(sid, ImageName, facility, service, start, details, Notes, Path) {
    hiddensid = sid;
    
    //document.getElementById("img_prev").setAttribute("src", Path.replace("~", ".."))
    //document.getElementById("img_prev").setAttribute("Style", "width:300px;height:200px")
    
    Imageupdate();
    $("#txt_Name").val(ImageName);
    $("#txt_Facility").val(facility);
    $("#txt_service").val(service);
    $("#txt_Start").val(start);
    $("#facility").val(details);
    $("#txt_Note").val(Notes);
    $("#btn_AddPromo").val("Update")
    $("#btn_AddPromo").text("Update")
}
var sFileName, facility, service, start, details, Notes, VirtualPath
function UploadSliderImage() {
    var bValid = true;
    //hiddensid = 0;
    sFileName = $("#txt_Name").val();
    facility = $("#txt_Facility").val();
    service = $("#txt_service").val();
    start = $("#txt_Start").val();
    details = $("#txt_details").val();
    Notes = $("#txt_Note").val();
    isUpdate = false;
    if (sFileName == "") {
        bValid = false;
        $("#txt_Name").focus();
        Success("Please Insert Name");
    }
    $("img").each(function () {
        if ($(this).attr("src") == null || $(this).attr("src") == '') {
            //$(this).remove();
            Success("Please Select Image");
            bValid = false;
        }
        else {
            isUpdate = true;
        }
    });
    if (bValid == true) {
        var data;
        var fileUpload = $("#Upload").get(0);
        var files = fileUpload.files;
        data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        $.ajax({
            url: "../PromoImges.ashx?sid=" + hiddensid + "&sFileName=" + sFileName + "&facility=" + facility + "&service=" + service + "&start=" + start + "&details=" + details + "&Notes=" + Notes,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result == 1) {
                    //$("#reset").click();
                    //document.getElementById("img_prev").setAttribute("src", "")
                    Success("Promo Details Saved successfully.")
                    setTimeout(function () {
                        window.location.reload();
                        GetPromoImageDetail();
                    }, 1000)
                }
                else if (result == 0) {
                }
            },
            error: function (err) {
                //Success(err);
            }
        });
    }

}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $("#demo").css("display", "")
        //$("#demo").text(")
        reader.onload = function (e) {
            $('#img_prev')
              .attr('src', e.target.result)
              .width(400)
              .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
    else {
        $("#demo").css("display", "none")
    }
}
// End Slider Images // 