﻿

$(function () {
    var HotelCode = getParameterByName('sHotelID');
    var HotelName = getParameterByName('HName');
    GetRatelist(HotelCode, HotelName)
    GetButton(HotelCode, HotelName)
})

function GetButton(HotelCode, HotelName) {
    var html = '';
    html += '<a href="AddRate.aspx?sHotelID=' + HotelCode + '&HotelName=' + HotelName + '"" class="button anthracite-gradient"><i class="fa fa-plus"></i> &nbsp;Add New</a>                                                                                                  ';
    //html += '               <button type="button" class="button blue-gradient glossy" id="btn_Ratelist" onclick="">+Add New</button>  ';
    //html += '           </a>                                                                                                          ';
    //html += '           <br>                                                                                                          ';
    //html += '           <br>                                                                                                          ';
    $("#div_btn").append(html);
}

function GetRatelist(HotelCode, HotelName) {
    $("#tbl_Ratelist").dataTable().fnClearTable();
    $("#tbl_Ratelist").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();
    $.ajax({
        url: "handler/RoomHandler.asmx/GetRatelist",
        type: "post",
        data: '{"HotelCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrRateList = result.RateList
                for (var i = 0; i < arrRateList.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:3%" id="' + i + '" align="center">' + (i + 1) + '</td>'
                    //  html += '<td style="width:20%"  align="center">' + arrRateList[i].Supplier + ' </td>'
                    var Checkindt = GetDate(arrRateList[i].Checkin);
                    var Checkoutdt = GetDate(arrRateList[i].Checkout);
                    html += '<td style="width:13%" id="' + i + '" align="center">' + Checkindt + " to " + Checkoutdt + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].MealPlan + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].CurrencyCode + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center" title="\'' + arrRateList[i].ValidNationality + '\'">' + trancate_per(arrRateList[i].ValidNationality) + ' </td>'
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].MinStayRate + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].MaxStayRate + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].MinRoom + ' </td>';
                    html += '<td style="width:10%" id="' + i + '" align="center">' + arrRateList[i].MaxRoom + ' </td>';

                    //html += '<td style="width:7%"  align="center">' + arrRateList[i].RateType + ' </td>'
                    // html += '<td style="width:13%"  align="center">' + arrRateList[i].Checkout + ' </td>'

                    //if (arrRateList[i].DayWise == "No")
                    //{
                    //    html += '<td style="width:10%"  align="center">' + arrRateList[i].RR + ' </td>'
                    //}
                    //else
                    //{
                    //    html += '<td style="width:14%"  align="center">'
                    //    html += '<lable >DayWise Rate</lable>'
                    //    html += '    <span class="info-spot">'
                    //    html += '		<span class="icon-info-round"></span>'
                    //    html += '		<span class="info-bubble">'

                    //    if (arrRateList[i].MonRR == null)
                    //        html += '<small>Mon:</small> - <br/>'
                    //    else
                    //        html += '<small>Mon:</small> ' + arrRateList[i].MonRR + '<br/>'

                    //    if (arrRateList[i].TueRR == null)
                    //        html += '<small>Tue:</small> - <br/>'
                    //    else
                    //        html += '<small>Tue:</small> ' + arrRateList[i].TueRR + '<br/>'

                    //    if (arrRateList[i].WedRR == null)
                    //        html += '<small>Wed:</small> - <br/>'
                    //    else
                    //        html += '<small>Wed:</small> ' + arrRateList[i].WedRR + '<br/>'

                    //    if (arrRateList[i].ThuRR == null)
                    //        html += '<small>Thu:</small> - <br/>'
                    //    else
                    //        html += '<small>Thu:</small> ' + arrRateList[i].ThuRR + '<br/>'

                    //    if (arrRateList[i].FriRR == null)
                    //        html += '<small>Fri:</small> - <br/>'
                    //    else
                    //        html += '<small>Fri:</small> ' + arrRateList[i].FriRR + '<br/>'

                    //    if (arrRateList[i].SatRR == null)
                    //        html += '<small>Sat:</small> - <br/>'
                    //    else
                    //        html += '<small>Sat:</small> ' + arrRateList[i].SatRR + '<br/>'

                    //    if (arrRateList[i].SunRR == null)
                    //        html += '<small>Sun:</small> - <br/>'
                    //    else
                    //        html += '<small>Sun:</small> ' + arrRateList[i].SunRR + '<br/>'


                    //    html += '		</span>'
                    //    html += '	</span>'
                    //    html += '</td>'
                    //}

                    //html += '<td style="width:10%"  align="center"><a style="cursor:pointer" class="button" href="EditRate.aspx?sHotelID=' + HotelCode + '&HotelName=' + HotelName + '&RateID=' + arrRateList[i].HotelRateID + '&RateType=' + arrRateList[i].RateType + '"><span class="icon-pencil" title="Edit Rates"></span></a></td>';

                    // html += '<td style="width:10%" align="center"><a style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td></tr>'
                    // html += '<td style="width:10%" align="center"><a style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" titl></span></a></td></tr>'

                    $("#tbl_Ratelist").append(html);
                }

                $('[data-toggle="tooltip"]').tooltip()

                $("#tbl_Ratelist").dataTable({
                    bSort: false,
                    sPaginationType: 'full_numbers',
                    sSearch: false
                });
                $("#tbl_Ratelist").addClass("respTable");
                GenrateTable();
            }
        }
    })
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function trancate_per(title) {
    var length = 20;
    if (title.length > length) {
        title = title.substring(0, length) + '...';
    }
    return title;
}

function GenrateTable() {
    // Call template init (optional, but faster if called manually)
    var arrRate;
    var Id;
    $.template.init();
    $('#tbl_Ratelist').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        Id = this.id;
        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }
        // If there is already a special row
        if (row.length > 0) {
            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');
            // Remove row
            row.remove();
            return;
        }
        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');
            // Remove rows
            rows.remove();

        }
        // Style row
        if (Id != "") {
            tr.children().addClass('anthracite-gradient glossy');

            $('<tr class="row-drop"><td id="div_Rate' + Id + '" colspan="9"></td></tr>').insertAfter(tr);
            var arrRate = arrRateList[Id];

            var data = {
                arrRate: arrRate
            }

            $.ajax({
                url: "handler/RoomHandler.asmx/GetRateData",
                type: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        var RateList = result.RateData;
                        var html = '';
                        html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_RatesDetails' + Id + '"><th style="text-align:center">Room Type</th>';
                        html += '<th style="text-align:center" >Rate Type</th>';
                        html += '<th style="text-align:center" >Period</th>';
                        html += '<th style="text-align:center" >Room Rate</th>';
                        html += '<th style="text-align:center" >EB Rate</th>';
                        html += '<th style="text-align:center" >CWB Rate</th>';
                        html += '<th style="text-align:center" >CNB Rate</th>';
                        html += '<th style="text-align:center" >Offer</th>';
                        html += '<th style="text-align:center" >Cancellation Policy</th>';
                        html += '<th style="text-align:center" >Meal Rate</th>';
                        html += '<th style="text-align:center" >AddOns</th>';
                        html += '<th style="text-align:center" >Status</th>';
                        html += '<tbody>'
                        for (var i = 0; i < RateList.length; i++) {
                            html += '<tr><td align="center">' + RateList[i].RoomType + ' </td>'
                            html += '<td align="center">' + RateList[i].Type + ' </td>'
                            var from = GetDate(RateList[i].Checkin);
                            var To = GetDate(RateList[i].Checkout);
                            html += '<td align="center">' + from + ' to ' + To + ' </td>'
                            html += '<td align="center">'
                            if (RateList[i].DayWise == "No") {
                                html += RateList[i].RR
                            }
                            else {
                                html += '<lable >DayWise</lable>'
                                html += '    <span class="info-spot">'
                                html += '		<span class="icon-info-round"></span>'
                                html += '		<span class="info-bubble">'

                                if (RateList[i].MonRR == null)
                                    html += '<small>Mon:</small> - <br/>'
                                else
                                    html += '<small>Mon:</small> ' + RateList[i].MonRR + '<br/>'

                                if (RateList[i].TueRR == null)
                                    html += '<small>Tue:</small> - <br/>'
                                else
                                    html += '<small>Tue:</small> ' + RateList[i].TueRR + '<br/>'

                                if (RateList[i].WedRR == null)
                                    html += '<small>Wed:</small> - <br/>'
                                else
                                    html += '<small>Wed:</small> ' + RateList[i].WedRR + '<br/>'

                                if (RateList[i].ThuRR == null)
                                    html += '<small>Thu:</small> - <br/>'
                                else
                                    html += '<small>Thu:</small> ' + RateList[i].ThuRR + '<br/>'

                                if (RateList[i].FriRR == null)
                                    html += '<small>Fri:</small> - <br/>'
                                else
                                    html += '<small>Fri:</small> ' + RateList[i].FriRR + '<br/>'

                                if (RateList[i].SatRR == null)
                                    html += '<small>Sat:</small> - <br/>'
                                else
                                    html += '<small>Sat:</small> ' + RateList[i].SatRR + '<br/>'

                                if (RateList[i].SunRR == null)
                                    html += '<small>Sun:</small> - <br/>'
                                else
                                    html += '<small>Sun:</small> ' + RateList[i].SunRR + '<br/>'


                                html += '		</span>'
                                html += '	</span>'

                            }
                            html += '</td>'
                            if (RateList[i].EB != null)
                                html += '<td align="center">' + RateList[i].EB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].CWB != null)
                                html += '<td align="center">' + RateList[i].CWB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].CNB != null)
                                html += '<td align="center">' + RateList[i].CNB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].OfferId == "")
                                html += '<td align="center">-</td>'
                            else
                                html += '<td align="center" onclick="GetOffer(\'' + RateList[i].HotelRateID + '\')" id="lbl_Fillter' + Id + '" class="icon-lightning"></td>'

                            if (RateList[i].CancellationPolicyId == "1")
                                html += '<td align="center" style="color:red">Non Refundable</td>'
                            else
                                html += '<td align="center" onclick="GetCancellation(\'' + RateList[i].CancellationPolicyId + '\')" id="lbl_FillterCancle' + Id + '" class="icon-info-round"></td>'

                            html += '<td align="center" onclick="GetMeal(\'' + RateList[i].HotelRateID + '\')" class="icon-mixi"></td>'
                            html += '<td align="center" onclick="GetAddOn(\'' + RateList[i].HotelRateID + '\')" class="icon-cup"></td>'
                            if (RateList[i].Status == 'Active') {
                                html += '<td class="align-center"><input type="checkbox" id="chk' + RateList[i].HotelRateID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onclick="UpdateRateStatus(' + RateList[i].HotelRateID + ',\'Deactive\')"></td></tr>';
                            }
                            else {
                                html += '<td class="align-center"><input type="checkbox" id="chk' + RateList[i].HotelRateID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onclick="UpdateRateStatus(' + RateList[i].HotelRateID + ',\'Active\')"></td></tr>';
                            }

                        }
                        html += '</tbody>'
                    }
                    html += '</table>'
                    $('#div_Rate' + Id).append(html);
                    $('#lbl_Fillter' + Id).menuTooltip($('#filter').hide(), {
                        classes: ['with-small-padding', 'full-width']
                    });
                    $('#lbl_FillterCancle' + Id).menuTooltip($('#filter').hide(), {
                        classes: ['with-small-padding', 'full-width']
                    });
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                    //$("#tbl_RatesDetails"+ Id).dataTable({
                    //    bSort: false,
                    //    sPaginationType: 'full_numbers',
                    //    sSearch: false
                    //});
                    //$("#tbl_RatesDetails" + Id).addClass("respTable");

                }
            })
        }


    })
    //.on('sortStart', function () {
    //var rows = $(this).find('.row-drop');
    //if (rows.length > 0) {
    //    rows.prev().children().removeClass('anthracite-gradient glossy');
    //    rows.removeAll();
    //}
    //  });
}

function GetOffer(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Offer"></div>',

        title: 'Offers',
        width: 600,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetOffer",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrOfferList = result.ListOffer;
                var html = '';

                html += '<table class="table responsive-table responsive-table-on dataTable">';
                html += ' <th style="text-align:center">Offer Name</th>';
                html += '<th style="text-align:center">Offer Type</th>';
                html += '<th style="text-align:center">Season</th>';
                html += '<tbody>'
                for (var i = 0; i < arrOfferList.length; i++) {
                    html += '<tr><td align="center">' + arrOfferList[i].OfferName + ' </td>'
                    html += '<td align="center">' + arrOfferList[i].OfferType + ' </td>'
                    html += '<td align="center">' + arrOfferList[i].From + ' to ' + arrOfferList[i].To + ' </td></tr>'
                }
                html += '</tbody>'
                html += '</table>'
                $("#Div_Offer").append(html);
            }
            else {
                Success("Something went wrong");
            }


        },
        error: function () {
        }
    });
}

function GetCancellation(CancellationPolicyId) {
    var data = {
        CancellationPolicyId: CancellationPolicyId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Cancle"></div>',

        title: 'Cancellation',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetCancellation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrListCancle = result.ListCancle;
                var html = '';

                html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList"><th style="text-align:center">Cancelation Policy</th>';
                html += '<th style="text-align:center" >Refund Type</th>';
                html += '<th style="text-align:center" >Days Prior/Date</th>';
                html += '<th style="text-align:center" >Rate Type</th>';
                html += '<th style="text-align:center" >Rate</th>';
                html += '<th style="text-align:center" >Note</th>';
                html += '<tbody>'
                for (var i = 0; i < arrListCancle.length; i++) {
                    html += '<tr><td>' + arrListCancle[i].CancellationPolicy + '</td>';
                    html += '<td>' + arrListCancle[i].RefundType + '</td>';
                    if (arrListCancle[i].IsDaysPrior == "true") {
                        html += '<td>' + arrListCancle[i].DaysPrior + '</td>';
                    }
                    else {
                        html += '<td>' + arrListCancle[i].Date + '</td>';
                    }
                    html += '<td>' + arrListCancle[i].ChargeTypes + '</td>';
                    if (arrListCancle[i].ChargeTypes == "Percentile") {
                        html += '<td>' + arrListCancle[i].PerToCharge + '%</td>';
                    }
                    else if (arrListCancle[i].ChargeTypes == "Nights") {
                        html += '<td>' + arrListCancle[i].NightToCharge + ' Night</td>';
                    }
                    else {
                        html += '<td>' + arrListCancle[i].AmntToCharge + '</td>';
                    }
                    html += '<td>' + arrListCancle[i].Note + '</td>';
                    html += '</tr>';
                }
                html += '</tbody>'
                html += '</table>'
                $("#Div_Cancle").append(html);
            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function GetAddOn(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_AddOn"></div>',

        title: 'AddOns',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetAddOn",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrListAddOn = result.AddOnNew;
                var html = '';
                if (arrListAddOn.length != 0) {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    // html += '<th style="text-align:center" >Hi-Tea</th>';
                    html += '<tbody>'
                    for (var i = 0; i < arrListAddOn.length; i++) {
                        html += '<tr><td>' + arrListAddOn[i].Rate + '</td>';
                        html += '<td>' + arrListAddOn[i].CWB + '</td>';
                        html += '<td>' + arrListAddOn[i].CNB + '</td>';
                        html += '</tr>';
                    }
                    html += '</tbody>'
                    html += '</table>'
                    $("#Div_AddOn").append(html);
                }
                else {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    html += '<tr><td>-</td>';
                    html += '<td>-</td>';
                    html += '<td>-</td>';
                    html += '<td>-</td>';
                    html += '</tr>';

                    html += '</tbody>'
                    html += '</table>'
                    $("#Div_AddOn").append(html);
                }

            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function GetMeal(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Meal"></div>',

        title: 'Meal',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetMeal",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrAddOnMeal = result.AddOnMeal;
                var html = '';
                if (arrAddOnMeal.length != 0) {

                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    // html += '<th style="text-align:center" >Hi-Tea</th>';
                    html += '<tbody>'
                    for (var i = 0; i < arrAddOnMeal.length; i++) {
                        html += '<tr><td>' + arrAddOnMeal[i].MealType + '</td>';
                        html += ' <td>' + arrAddOnMeal[i].Rate + '</td>';
                        html += '<td>' + arrAddOnMeal[i].CWB + '</td>';
                        html += '<td>' + arrAddOnMeal[i].CNB + '</td>';
                        html += '</tr>';
                    }
                    html += '</tbody>'
                    html += '</table>'
                    $("#Div_Meal").append(html);
                }

                else {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    html += '<tr><td>-</td>';
                    html += ' <td>-</td>';
                    html += '<td>-</td>';
                    html += '<td>-</td>';
                    html += '</tr>';
                    html += '</tbody>'
                    html += '</table>'
                    $("#Div_Meal").append(html);
                }


            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function UpdateRateStatus(HotelRateId, status) {

    var data = {
        RateID: HotelRateId,
        status: status,
    }

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/UpdateRateStatus",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                Success("Status Updated Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                Success("Something went wrong");
            }


        },
        error: function () {
        }
    });
}

function GetDate(dt) {
    var dts = moment(dt, "DD.MM.YYYY").format("DD-MM-YY");
    return dts;
}