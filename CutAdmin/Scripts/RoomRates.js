﻿var arrFacilities = []; var arr;
var arrHotellist = new Array();
var arrFacilitiesList = new Array();
var pFacilities = "", selectedFacilities = [];
var arrRoomList = [];
var UserType;
$(document).ready(function () {
    GetCountry();
    getDates();

    // GetSuppliers();
    // GetCurrency();
});


function GetHotelList() {
    var City1 = $("#txt_Destination").val();
    var City = City1.split(',')[0];
    var Country = City1.split(',')[1].trim();
    var data = {
        Country: Country,
        City: City
    }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetMappedHotelstoUpdate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#listHotels").empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelList = result.HotelListbyCity;
                Div = '';
                for (var i = 0; i < arrHotelList.length; i++) {

                    Div += '<option data-id=' + arrHotelList[i].sid + '>' + arrHotelList[i].HotelName + '</option>'
                }
                $("#listHotels").append(Div);
            }
            else {
                AlertDanger(result.ex)
            }
        },

    });
}

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}

function getDates() {
    $("#txt_Checkin").datepicker($.extend({
        minDate: 0,
        onSelect: function () {
            var minDate = $(this).datepicker('getDate');
            minDate.setDate(minDate.getDate() + 1); //add One days
            $("#txt_Checkout").datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));

    $("#txt_Checkout").datepicker($.extend({
        onSelect: function () {
            var maxDate = $(this).datepicker('getDate');
            maxDate.setDate(maxDate.getDate());
            ChangeNights();
        }
    }, datepickersOpt));


}

function ChangeNights() {
    var Checkin = moment($("#txt_Checkin").val(), "DD-MM-YYYY");
    var Checkout = moment($("#txt_Checkout").val(), "DD-MM-YYYY");
    var CountDays = Checkout.diff(Checkin, 'days');
    $('#sel_Nights').val(CountDays);
    $("#drp_Nights .select span")[0].textContent = CountDays;
    // document.getElementById('sel_Nights').value = CountDays;
}

//function drpChangeNights() {
//    var Nights = $('#sel_Nights').val();
//    days = parseInt(Nights);
//    var Checkin = moment($("#txt_Checkin").val(), "DD-MM-YYYY");
//    var Checkout = moment($("#txt_Checkout").val(), "DD-MM-YYYY");
//    var CountDays = Checkout.diff(Checkin, 'days');
//    $('#sel_Nights').val(Nights);
//    $("#drp_Nights .select span")[0].textContent = Nights;
//    if (CountDays != Nights) {
//        $("#txt_Checkout").datepicker("setDate", Checkin.add(days, 'days'));

//    }

//}
// var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
function drpChangeNights() {
    days = $('#sel_Nights').val();
    days = parseInt(days);
    var firstDate = new Date($('#txt_Checkin').datepicker("getDate"));
    var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + days);
    $('#txt_Checkout').datepicker('setDate', secondDate);
    insertDepartureDate('', '', '');

}
function insertDepartureDate(value, date, inst) {
    var firstDate = new Date($('#txt_Checkin').datepicker("getDate"));
    var dateAdjust = $('#sel_Nights').val();
    var current_date = new Date();

    current_time = current_date.getTime();
    days = (firstDate.getTime() - current_time) / (1000 * 60 * 60 * 24);
    if (days < 0) {
        var add_day = 1;
    } else {
        var add_day = 2;
    }
    days = parseInt(days);

    $('#txt_Checkout').datepicker("option", "minDate", days + add_day);
    $('#txt_Checkout').datepicker("option", "maxDate", days + 365);
    dateAdjust = parseInt(dateAdjust);
    var secondDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + dateAdjust);
    $('#txt_Checkout').datepicker('setDate', secondDate);
}

var arrCountry = new Array();
function GetCountry() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    if (UserType == undefined || UserType != 'Agent')
                        ddlRequest += ' <option class="optCountry" value="AllCountry">For All Nationality</option>';
                       
                    for (i = 0; i < arrCountry.length; i++) {

                        if (arrCountry[i].Country != "AllCountry")
                            ddlRequest += '<option class="optCountry" value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    ddlRequest += ' <option value="Unselect" style="display:inline-block;" disabled>Unselect All</option>';
                    $("#sel_Nationality").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function ActivateHotel(HotelId, Status) {
    var hotel = HotelId;
    var s = status;
    var Data = { HotelId: HotelId, Status: Status };
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/HotelActivation",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var ActiveHotel = obj.ActiveHotel;
            if (obj.retCode == 1) {
                if (ActiveHotel == "True") {
                    Success("Hotel Activated Successfully");
                    location.reload();
                }
                else {
                    Success("Hotel Deactivated Successfully");
                    location.reload();
                }

                //GetAllSeasons();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


function DropRowwithId(drophotelId) {

    var trDropleft = "";
    var trDropmiddle = "";
    var trDropRight = "";
    var Arrunique = [];
    var CancelationPolicies = [];

    var RoomRates = $.grep(arrRoomRates, function (p) { return p.RoomId == drophotelId; })
               .map(function (p) { return p.PriceList; })[0];

    var RoomDetails = $.grep(arrRoomDetails, function (p) { return p.RoomTypeId == drophotelId; })
               .map(function (p) { return p; })[0];

    var Checkin = $("#txt_Checkin").val();
    var Checkout = $("#txt_Checkout").val();
    var OfferDetails = $.grep(arrSeasonOfferListt, function (p) { return p.ValidFrom <= Checkin })
               .map(function (p) { return p; })[0];

    // var OfferDetails = arrSeasonOfferListt;
    var unique = [];
    var CancelationDates = [];
    for (var i = 0; i < RoomRates.length; i++) {
        var found = false;

        var CancelationIds = RoomRates[i].CancelationPolicy.split(',');
        for (var cp = 0; cp < CancelationIds.length; cp++) {
            for (var j = 0; j < arrCancelations.length; j++) {
                if (CancelationIds[cp] == arrCancelations[j].CancelationID && CancelationIds[cp] != "") {
                    var checkin = RoomRates[i].CheckinDate;
                    var checout = RoomRates[i].CheckoutDate;
                    var ExtraBeds = RoomRates[i].EBRate;
                    var ChildWithBed = RoomRates[i].CWBRate;
                    var ChildWithoutBed = RoomRates[i].CNBRate;

                    CancelationDates.push(checkin + " To " + checout + "^" + arrCancelations[j].CancelationID + "^" + ExtraBeds + "^" + ChildWithBed + "^" + ChildWithoutBed);
                    unique.push(arrCancelations[j]);
                }
            }
        }

    }
    var Arrunique = unique.filter(onlyUnique);
    var ArrCdates = CancelationDates.filter(onlyUnique);

    //var AllDates = [];
    //for (var i = 0; i < ArrCdates.length; i++)
    //{
    //    var Cid = ArrCdates[i].split('^')[1];
    //    var found = false;
    //    for (var j = 0; j < Arrunique.length; j++) {
    //        if (Cid == Arrunique[j].CancelationID) {
    //            found = true;
    //            break;
    //        }                      
    //    }
    //    if (found == true) {
    //        AllDates.push({ Dates: ArrCdates[i].split('^')[0], CancelPolicies: Arrunique[j] });
    //    }
    //}

    var AllDates = [];
    for (var i = 0; i < Arrunique.length; i++) {
        var found = false;
        var Dates = "";
        var OtherRates = [];
        for (var j = 0; j < ArrCdates.length; j++) {
            var Cid = ArrCdates[j].split('^')[1];
            if (Cid == Arrunique[i].CancelationID) {
                found = true;
                Dates = ArrCdates[j].split('^')[0] + ",";
                var Extrabed = ArrCdates[j].split('^')[2];
                var ChildwithBed = ArrCdates[j].split('^')[3];
                var ChildwithoutBed = ArrCdates[j].split('^')[4];
                OtherRates.push({ Extrabed: Extrabed, ChildwithBed: ChildwithBed, ChildwithoutBed: ChildwithoutBed })
                //break;
            }
        }
        if (found == true) {
            Dates = Dates.replace(/^,|,$/g, '');
            AllDates.push({ Dates: Dates, CancelPolicies: Arrunique[i], OtherRates: OtherRates });
        }
    }

    var oldDate = "";
    //Left
    trDropleft += '<div style="color:white;position:center"><h6>Cancelation Policy</h6> ';
    for (var i = 0; i < AllDates.length; i++) {
        var CancelPolicies = AllDates[i].CancelPolicies;
        var Dates = AllDates[i].Dates;

        if (AllDates[i].Dates != oldDate) {
            oldDate = AllDates[i].Dates;
            trDropleft += '<strong>For Dates<strong>(' + AllDates[i].Dates + '):';
        }
        //Refund Type
        if (CancelPolicies == "") {
            trDropleft += '<ul class="bullet-list">';
            trDropleft += '<strong>Non Refundable<strong>';
            trDropleft += '</ul>';
        }

        trDropleft += '<ul class="bullet-list">';
        if (CancelPolicies.RefundType == "Non Refundable")
            trDropleft += '<li><small>The Amount for these Dates are Non Refundable</small></li>';
        else if (CancelPolicies.RefundType == "Refundable") {
            if (CancelPolicies.IsDaysPrior == "true") {
                if (CancelPolicies.ChargesType == "Amount") {
                    trDropleft += '<li><small>If Canceled before ' + CancelPolicies.DaysPrior + ' days of above Booking Dates. The Amount will be charge ' + CancelPolicies.AmountToCharge + ' $ </small></li>';
                }
                else if (CancelPolicies.ChargesType == "Percentile") {
                    trDropleft += '<li><small>If Canceled before ' + CancelPolicies.DaysPrior + ' days of above Booking Dates. The Amount will be charge ' + CancelPolicies.PercentageToCharge + '% </small></li>';
                }
                else if (CancelPolicies.ChargesType == "Nights") {
                    trDropleft += '<li><small>If Canceled before ' + CancelPolicies.DaysPrior + ' days of above Booking Dates. The Amount will be charged of ' + CancelPolicies.NightsToCharge + ' Nights</small></li>';
                }
            }
            else if (CancelPolicies.IsDaysPrior == "false") {
                if (CancelPolicies.ChargesType == "Amount") {
                    trDropleft += '<li><small>If Canceled after ' + CancelPolicies.Date + '. The Amount will be charge ' + CancelPolicies.AmountToCharge + ' $ </small></li>';
                }
                else if (CancelPolicies.ChargesType == "Percentile") {
                    trDropleft += '<li><small>If Canceled after ' + CancelPolicies.Date + '. The Amount will be charge ' + CancelPolicies.PercentageToCharge + '%  </small></li>';
                }
                else if (CancelPolicies.ChargesType == "Nights") {
                    trDropleft += '<li><small>If Canceled after ' + CancelPolicies.Date + '. The Amount will be charged of ' + CancelPolicies.NightsToCharge + ' Nights</small></li>';
                }
            }
        }
        else if (CancelPolicies.RefundType == "NoShow") {
            if (CancelPolicies.ChargesType == "Amount") {
                trDropleft += '<li><small>If Person Not Attend on Booking Dates. The Amount will be charge ' + CancelPolicies.AmountToCharge + ' $ </small></li>';
            }
            else if (CancelPolicies.ChargesType == "Percentile") {
                trDropleft += '<li><small>If Person Not Attend on Booking Dates. The Amount will be charge ' + CancelPolicies.PercentageToCharge + ' % </small></li>';
            }
            else if (CancelPolicies.ChargesType == "Nights") {
                trDropleft += '<li><small>If Person Not Attend on Booking Dates. The Amount will be charge ' + CancelPolicies.NightsToCharge + ' Nights </small></li>';
            }
        }

        trDropleft += '</ul>';
    }
    trDropleft += '</div><hr/>';
    // Details
    trDropRight += '<ul class="bullet-list">';
    var RoomAmenities = RoomDetails.RoomAmenitiesId;
    RoomAmenities = RoomAmenities.replace(/^,|,$/g, '');
    trDropRight += '<li>Room Amenities: <small>' + RoomAmenities + '</small></li>';
    trDropRight += '</ul><hr/>';
    var oldDate1 = "";
    for (var i = 0; i < AllDates.length; i++) {
        var OtherRates = AllDates[i].OtherRates[0];
        if (AllDates[i].Dates != oldDate1) {
            oldDate1 = AllDates[i].Dates;
            trDropleft += '<strong>For Dates<strong>(' + AllDates[i].Dates + ')';
            trDropleft += '<ul class="bullet-list">';
            trDropleft += '<li><small>Extra Bed Rate:' + OtherRates.Extrabed + '</small>&nbsp;&nbsp;&nbsp;<small>Child With Bed: ' + OtherRates.ChildwithBed + '</small>&nbsp;&nbsp;&nbsp;<small>Child Without Bed: ' + OtherRates.ChildwithoutBed + '</small></li>';
            trDropleft += '</ul>';
        }


    }
    //Middle


    //trDropmiddle += '<ul class="bullet-list">';
    //trDropmiddle += '<li>Offer: ' + OfferDetails.SeasonName + '</li>';
    //trDropmiddle += '</ul>';


    //Right
    trDropRight += '<ul class="bullet-list">';
    trDropRight += '<li>MaxOccupacy: ' + RoomDetails.RoomOccupancy + '</li>';
    trDropRight += '<li>Adults With Children Allowed: ' + RoomDetails.AdultsWithChildAllowed + '</li>';
    //trDropRight += '<li>Adults Without Children Allowed: ' + RoomDetails.AdultsWithoutChildAllowed + '</li>';
    trDropRight += '<li>No Of Children Witout Bed: ' + RoomDetails.NoOfChildWithoutBed + '</li>';
    trDropRight += '<li>Max Extrabed Allowed   : ' + RoomDetails.MaxExtrabedAllowed + '</li>';
    trDropRight += '<li>Bedding Type: ' + RoomDetails.BeddingType + '</li>';
    trDropRight += '<li>Room Size: ' + RoomDetails.RoomSize + '</li>';
    trDropRight += '<li>Smoking Allowed: ' + RoomDetails.SmokingAllowed + '</li>';
    trDropRight += '</ul>';

    $('#DropLeft').append(trDropleft);
    $('#DropMiddle').append(trDropmiddle);
    $('#DropRight').append(trDropRight);

}

//function GetSuppliers() {
//    var ddlRequest = '';
//    $("#sel_Supplier").empty();
//    if (arrSuppliers.length > 0) {
//        ddlRequest += '<option value="" selected="selected" disabled>Please Select</option>';
//        for (i = 0; i < arrSuppliers.length; i++) {
//            ddlRequest += '<option value="' + arrSuppliers[i] + '">' + arrSuppliers[i] + '</option>';
//            //ddlRequest += '<option value="' + arrSuppliers[i] + ></option>';
//        }
//        $("#sel_Supplier").append(ddlRequest);
//    }
//    else {
//        ddlRequest += '<option value="" disabled>Not Available</option>';
//    }
//}

function GetSuppliers() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "../GenralHandler.asmx/GetSuppliers",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSuppliersName = result.SupplierList;
                if (arrSuppliersName.length > 0) {
                    for (i = 0; i < arrSuppliersName.length; i++) {
                        ddlRequest += '<option value="' + arrSuppliersName[i].sid + '">' + arrSuppliersName[i].Supplier + '</option>';
                    }
                    $("#sel_Supplier").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}

function GetCurrency() {
    var ddlRequest = '';
    $("#sel_CurrencyCode").empty();
    if (arrCurrencyList.length > 0) {
        ddlRequest += '<option value="" selected="selected" disabled>Please Select</option>';
        for (i = 0; i < arrCurrencyList.length; i++) {
            ddlRequest += '<option value="' + arrCurrencyList[i].CurrencyCode + '">' + arrCurrencyList[i].CurrencyCode + '</option>';
        }
        $("#sel_CurrencyCode").append(ddlRequest);
    }
    else {
        ddlRequest += '<option value="" disabled>Not Available</option>';
    }
}

var arrSupplierRate = new Array();
function GetMealPlans() {
    var ddlRequest = '';
    $("#sel_MealPlan").empty();
    if (arrMealPlans.length > 0) {
        // ddlRequest += '<option value="" selected="selected" disabled>Please Select</option>';
        for (i = 0; i < arrMealPlans.length; i++) {
            ddlRequest += '<option value="' + arrMealPlans[i].MealPlanShort + 'selected="selected" disabled">' + arrMealPlans[i].MealPlanName + '</option>';
        }
        $("#sel_MealPlan").append(ddlRequest);
    }
    else {
        ddlRequest += '<option value=""  disabled>Not Available</option>';
    }
}

Array.prototype.unique = function () { return this.filter(function (value, index, self) { return self.indexOf(value) === index; }); }


var DatesHotel = [];
var RoomsHotel = [];
var arrRoomDetails = [];
var arrCancelations = [];
var arrSuppliers = [];
var arrCurrencyList = [];
var arrMealPlans = [];
var arrRoomTypes = [];
var arrRoomRates = [];
var nationalityCode = [];
var arrRateType = [];
var arrSeasonOfferListt = [];
var arrFilter = [];
var HotelCode = [];
var flags = [], Rooms = [], Datewise = [];
//var arrRateType = [{ id: 2, value: "Royal" }, { id: 3, value: "Super Delux" }, { id: 5, value: "Delux" }, { id: 6, value: "General" }]
//redirect to search page with data
var validNationality = new Array();
var nationality; var HotelName; var Destination;
var session = "";
var SearchValid = "";
function SearchRates() {
    debugger;
    //document.getElementById('tbl_HotelList').style.display = "none";

    var Message = "";
    
    //if ($("#radio_gen").prop("checked")) {
    //    SearchValid = "General";
    //}
    //else if ($("#radio_adv").prop("checked")) {
    //    SearchValid = "Advance";
    //}

    if ($("#SearchType1").is(":checked")) {
        SearchValid = "General";
    }
    else if ($("#SearchType2").prop("checked")) {
        SearchValid = "Advance";
    }
    else
        SearchValid = "Advance";

    Destination = $('#hdnDCode').val();
    var Checkin = $('#txt_Checkin').val();
    var Checkout = $('#txt_Checkout').val();
    //var Ratings = $('#Select_StarRating').val();
    HotelName = $('#txt_HotelName').val();
    HotelCode.push( $('#listHotels option').filter(function () { return this.value == HotelName; }).data('id'));
    //var Destination = $('#txt_Destination').val();
    nationalityCode = $('#sel_Nationality').val();
    validNationality = new Array();

    if (Destination == "") {
        Success("Enter the city name where you want to go!");
        return false;
    }
    if (HotelName == "") {
        Success("Enter the Hotel name!");
        return false;
    }
    if (Checkin == "") {
        Success('Please Enter Checkin Date');
        return false;
    }
    if (Checkout == "") {
        Success('Please Enter Checkout Date');
        return false;
    }

    if (nationalityCode == "" || nationalityCode == null) {
        Success("Please select Nationality");
        return false;
    }


    if (SearchValid != "Advance") {
        for (var i = 0; i < nationalityCode.length; i++) {
            if (nationalityCode[i] != "All" && nationalityCode[i] != "AllCountry") {
                var value = $.grep(arrCountry, function (p) { return p.Country == nationalityCode[i]; })
                                        .map(function (p) { return p; });
                validNationality.push(value[0])
            }
            else if (nationalityCode[i] == "AllCountry") {
                validNationality.push({ Country: "AllCountry", Countryname: "For All Nationality" })
            }


        }
    }
    else {
        nationalityCode = [];
        nationalityCode.push($('#sel_Nationality').val())
    }
    var nationalityName = $('#sel_Nationality option:selected').text();
    var DName = $('#txt_Destination').val();
    var Nights = $('#sel_Nights').val();
    var Adults = $('#Select_Adults1d').val();
    var Childs = $('#Select_Children1').val();
    var room = $('#Select_Rooms').val();

    var ddlRequest = '';

    var Supplier = "";
    if ($('#sel_Supplier').val() != null) {
        Supplier = $('#sel_Supplier').val();
    }
    var MealPlan = "";
    if ($('#sel_MealPlan').val() != null) {
        MealPlan = $('#sel_MealPlan').val();
    }
    var CurrencyCode = "";
    if ($('#sel_CurrencyCode').val() != null) {
        CurrencyCode = $('#sel_CurrencyCode').val();
    }


    if (HotelCode == null) {
        HotelCode = 0;
    }

    if (nationalityCode == null) {
        nationalityCode = "";
    }

    if (Nights == null) {
        Nights = 0;
    }
    if (Adults == null) {
        Adults = 0;
    }
    if (Childs == null) {
        Childs = 0;
    }

    else {
        var roomcount = parseInt(room);
        var occupancy = '';
        for (var i = 0; i < roomcount; i++) {
            if (i == 0) {
                occupancy = Room1();
            }
            else if (i == 1) {
                occupancy = occupancy + '$' + Room2();
            }
            else if (i == 2) {
                occupancy = occupancy + '$' + Room3();
            }
            else if (i == 3) {
                occupancy = occupancy + '$' + Room4();
            }
            else if (i == 4) {
                occupancy = occupancy + '$' + Room5();
            }
            else if (i == 5) {
                occupancy = occupancy + '$' + Room6();
            }
            else if (i == 6) {
                occupancy = occupancy + '$' + Room7();
            }
            else if (i == 7) {
                occupancy = occupancy + '$' + Room8();
            }
            else if (i == 8) {
                occupancy = occupancy + '$' + Room9();
            }
        }
        session = Destination + '_' + DName + '_' + Checkin + '_' + Checkout + '_' + room + '_' + occupancy + '_' + HotelCode + '_' + HotelName + '_' + nationalityCode + '_' + nationalityName;
        var trRequest = "";
        var data =
         {
             HotelCode: HotelCode,
             Destination: Destination,
             Checkin: Checkin,
             Checkout: Checkout,
             nationality: nationalityCode,
             Nights: Nights,
             Adults: Adults,
             Childs: Childs,
             Supplier: Supplier,
             MealPlan: MealPlan,
             CurrencyCode: CurrencyCode,
             AddSearchsession: session,
             SearchValid: SearchValid
         }
        $.ajax({
            type: "POST",
            url: "../handler/RoomHandler.asmx/GetRates",
            //data: '{"dFrom":"' + dFrom + '","dTo":"' + dTo + '"}',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {

                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    //document.getElementById('tbl_HotelList').style.display = "";
                    $('#divFilters').css("display", "");
                    trRequest = ''
                    arrRoomRates = result.ListRate;
                    arrRoomDetails = result.AllRooms;
                    arrSuppliers = result.SupplierList;
                    arrCurrencyList = result.CurrencyList;
                    arrMealPlans = result.MealPlanList;
                    arrDatesList = result.ListDates;
                    arrRateType = result.arrRateType;
                    var Usertype = result.Usertype;
                    arrFilter = result.Filter;
                    if (arrRoomRates.length > 0 && SearchValid != "Advance") {
                        
                        GetRates('RO', 0);

                        // $("#sel_ValidNationality option[value='All']").remove();

                        Natiion = [];

                        for (i = 0; i < arrRoomRates.length; i++) {
                            if (arrRoomRates[i].RoomOccupancy[0].Rooms.length != 0)
                            {
                                for (var j = 0; j < arrCountry.length; j++) {

                                    if (arrCountry[j].Country == arrRoomRates[i].Nationality) {
                                        Natiion.push(arrCountry[j]);
                                    }
                                }
                            }
                         
                            //if (arrRoomRates[i].Nationality == "AllCountry") {
                            //    Natiion.push({ Country: "AllCountry", Countryname: "For All Nationality" });
                            //}

                        }
                        $("#sel_ValidNationality").empty();
                        ddlRequest += '<option value="" selected="selected" disabled>Please Select</option>';
                        for (var i = 0; i < Natiion.length; i++) {

                            ddlRequest += '<option value="' + Natiion[i].Country + '">' + Natiion[i].Countryname + '</option>';
                        }
                        $("#sel_ValidNationality").append(ddlRequest);
                    }
                    else {
                        GenrateB2bRates();
                        GenerateFilter();
                    }



                }
                else if (result.retCode == -2) {
                    Message = result.ErrorMessage;
                    Success(Message);

                    return false;
                }
                else if (result.retCode == 0) {
                    $("#tbl_HotelList tbody").remove();
                    var trRequest = '<tbody>';
                    trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                    trRequest += '</tbody>';
                    $("#tbl_HotelList").append(trRequest);
                    AlertDanger(result.ex)
                }

            }
        });
    }

}
function ReturnUnique(arrRates) {
    var arrUnique = new Array();
    var uniqueRates = [];
    for (i = 0; i < arrRates.length; i++) {
        if (uniqueRates.indexOf(arrRates[i].Total) === -1 && arrRates[i].Total != 0 && arrRates[i].OfferRate == 0) {
            uniqueRates.push(arrRates[i].Total);
            arrUnique.push(arrRates[i])
        }
        else if (uniqueRates.indexOf(arrRates[i].OfferRate) === -1 && arrRates[i].OfferRate != 0) {
            uniqueRates.push(arrRates[i].OfferRate);
            arrUnique.push(arrRates[i])
        }
    }
    return arrUnique;
}

//function Room1() {
//    var adult = $('#Select_Adults1d').val();
//    var child = $('#Select_Children1d').val();
//    var age = 0;
//    if (parseInt(child) > 0) {
//        age = $('#Select_AgeChildFirst1d').val();
//        if (parseInt(child) == 2) {
//            age = age + '^' + $('#Select_AgeChildSecond1d').val();
//        }
//        else if (parseInt(child) == 3) {
//            age = age + '^' + $('#Select_AgeChildSecond1d').val() + '^' + $('#Select_AgeChildThird1d').val();
//        }
//        else if (parseInt(child) == 4) {
//            age = age + '^' + $('#Select_AgeChildSecond1d').val() + '^' + $('#Select_AgeChildThird1d').val() + '^' + $('#Select_AgeChildFourth1d').val();
//        }
//    }
//    return adult + '|' + child + '^' + age;
//}
function Room1() {
    
        var adult = $('#Select_Adults1d').val();
        var child = $('#Select_Children1').val();
        var age = 0;
        if (parseInt(child) > 0) {
            age = $('#Select_AgeChildFirst1d').val();
            if (parseInt(child) == 2) {
                age = age + '^' + $('#Select_AgeChildSecond1d').val();
            }
            else if (parseInt(child) == 3) {
                age = age + '^' + $('#Select_AgeChildSecond1d').val() + '^' + $('#Select_AgeChildThird1d').val();
            }
            else if (parseInt(child) == 4) {
                age = age + '^' + $('#Select_AgeChildSecond1d').val() + '^' + $('#Select_AgeChildThird1d').val() + '^' + $('#Select_AgeChildFourth1d').val();
            }
        }
    
    
    return adult + '|' + child + '^' + age;
}
function Room2() {
    var adult = $('#Select_Adults2d').val();
    var child = $('#Select_Children2d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst2d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond2d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond2d').val() + '^' + $('#Select_AgeChildThird2d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond2d').val() + '^' + $('#Select_AgeChildThird2d').val() + '^' + $('#Select_AgeChildFourth2d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room3() {
    var adult = $('#Select_Adults3d').val();
    var child = $('#Select_Children3d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst3d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond3d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond3d').val() + '^' + $('#Select_AgeChildThird3d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond3d').val() + '^' + $('#Select_AgeChildThird3d').val() + '^' + $('#Select_AgeChildFourth3d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room4() {
    var adult = $('#Select_Adults4d').val();
    var child = $('#Select_Children4d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst4d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond4d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond4d').val() + '^' + $('#Select_AgeChildThird4d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond4d').val() + '^' + $('#Select_AgeChildThird4d').val() + '^' + $('#Select_AgeChildFourth4d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
///////////////////////////////////////////////////////////////
function Room5() {
    var adult = $('#Select_Adults5d').val();
    var child = $('#Select_Children5').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst5d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond5d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond5d').val() + '^' + $('#Select_AgeChildThird5d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond5d').val() + '^' + $('#Select_AgeChildThird5d').val() + '^' + $('#Select_AgeChildFourth5d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room6() {
    var adult = $('#Select_Adults6d').val();
    var child = $('#Select_Children6d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst6d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond6d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond6d').val() + '^' + $('#Select_AgeChildThird6d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond6d').val() + '^' + $('#Select_AgeChildThird6d').val() + '^' + $('#Select_AgeChildFourth6d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room7() {
    var adult = $('#Select_Adults7d').val();
    var child = $('#Select_Children7d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst7d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond7d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond7d').val() + '^' + $('#Select_AgeChildThird7d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond7d').val() + '^' + $('#Select_AgeChildThird7d').val() + '^' + $('#Select_AgeChildFourth7d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room8() {
    var adult = $('#Select_Adults8d').val();
    var child = $('#Select_Children8d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst8d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond8d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond8d').val() + '^' + $('#Select_AgeChildThird8d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond8d').val() + '^' + $('#Select_AgeChildThird8d').val() + '^' + $('#Select_AgeChildFourth8d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}
function Room9() {
    var adult = $('#Select_Adults9d').val();
    var child = $('#Select_Children9d').val();
    var age = 0;
    if (parseInt(child) > 0) {
        age = $('#Select_AgeChildFirst9d').val();
        if (parseInt(child) == 2) {
            age = age + '^' + $('#Select_AgeChildSecond9d').val();
        }
        else if (parseInt(child) == 3) {
            age = age + '^' + $('#Select_AgeChildSecond9d').val() + '^' + $('#Select_AgeChildThird9d').val();
        }
        else if (parseInt(child) == 4) {
            age = age + '^' + $('#Select_AgeChildSecond9d').val() + '^' + $('#Select_AgeChildThird9d').val() + '^' + $('#Select_AgeChildFourth9d').val();
        }
    }
    return adult + '|' + child + '^' + age;
}



//function GetRates(Supplier) {
//    $("#tbl_HotelList").empty();
//    $("#ul_Supplier").empty();
//    $("#tab_Supplier").empty();
//    var ul = '';
//    var tab = '';
//    var table = '';
//    for (var i = 0; i < arrSuppliers.length; i++) {

//        if (i == 0)
//            ul += '<li class="active" id="li_Supplier' + i + '"><a href="#tab-' + i + '">' + arrSuppliers[i] + '</a></li>'
//        else
//            ul += '<li ><a href="#tab-' + i + '" id="li_Supplier' + i + '">' + arrSuppliers[i] + '</a></li>'                                                              //style="font-size: small";
//        if (i == 0)
//            tab += '<div id="tab-' + i + '" class="with-padding" > <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
//        else
//            tab += '<div id="tab-' + i + '" class="with-padding" style="display:none;"> <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
//        tab += '<thead>';
//        tab += '<tr>';
//        tab += '<th style="width:120px;height:30px;">Room Type</th>';
//        var arrRates = $.grep(arrRoomRates, function (p) { return p.Name == arrSuppliers[i]; })
//                      .map(function (p) { return p; });
//        for (var d = 0; d < arrDatesList.length; d++) {
//            tab += '<th style="width:90px;height:30px;">' + arrDatesList[d] + '</th>';
//        }
//        tab += '<tr>';
//        tab += '</thead>';
//        tab += '<tbody>'
//        tab += '<tr>';

//        tab += '</div>';
//        tab += '</tr>';
//        for (var r = 0; r < arrRoomDetails.length; r++) {
//            var Name = $.grep(arrRateType, function (p) { return p.RoomTypeID == arrRoomDetails[r].RoomTypeId; })
//                      .map(function (p) { return p.RoomType; });

//            for (var m = 0; m < Meals.length; m++) {
//                var sel = '<select id="sel_' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + m + '" class="Sel_Room"><option selected="selected" value="0" >0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>';
//                tab += '<tr>';

//                tab += '<td style="width:120px;background-color:white;height:32px;"><label id="lbl_' + arrSuppliers[i] + Name[0] + '_' + m + ' ">' + Name[0] + '(' + Meals[m] + ')' + '<label><br>' + '' + sel + '</td>';

//                for (var d = 0; d < arrDatesList.length; d++) {
//                    var OfferRates = [];
//                    var arrDateDetails = [];
//                    for (var rates = 0; rates < arrRates.length; rates++) {
//                        for (var rooms = 0; rooms < arrRates[rates].RoomOccupancy.length; rooms++) {
//                            var arrDates = $.grep(arrRates[rates].RoomOccupancy[rooms].Rooms, function (p) { return p.RoomTypeId == arrRoomDetails[r].RoomTypeId && p.RoomDescription == Meals[m]; })
//                                                .map(function (p) { return p.Dates; });
//                            if (arrDates.length != 0) {
//                                for (var dt = 0; dt < arrDates.length; dt++) {
//                                    for (var dates = 0; dates < arrDates[dt].length; dates++) {
//                                        if (arrDates[dt][dates].datetime == arrDatesList[d]) {
//                                            arrDateDetails.push(arrDates[dt][dates]);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    if (arrDateDetails.length != 0) {

//                        arrDateDetails = ReturnUnique(arrDateDetails);
//                        for (var k = 0; k < arrDateDetails.length; k++) {
//                            var Colour = "";
//                            if (arrDateDetails[k].NoOfCount == 0) {
//                                Colour = "#ff8e8e;";
//                            }
//                            else if (arrDateDetails[k].NoOfCount >= 1 && arrDateDetails[k].NoOfCount <= 100) {
//                                Colour = "#f9ca83;";
//                            }
//                            else
//                                Colour = "#fdfda0;";

//                            tab += '<td  style="background-color:' + Colour + 'font-weight:1000;font-size:15px">';

//                            if (arrDateDetails[k].NoOfInventory.length != 0) {
//                                var Inventory = arrDateDetails[k].NoOfInventory[0].InvNoOfRoom;
//                                var tSold = arrDateDetails[k].NoOfInventory[0].Sold;
//                                var Sold = Inventory - tSold;

//                                if (arrDateDetails[k].OfferRate != 0) {
//                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate" class="Rate"><input type="checkbox" class="Supplier' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + Meals[m] + '" name="ChkTotal' + i + '" id="totalchk' + i + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '">  ' + arrDateDetails[k].Total + '';
//                                    tab += '</label></td></tr></table>';
//                                }
//                                else {
//                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate"><input type="checkbox" class="Supplier' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + Meals[m] + '" name="ChkTotal' + i + '"id="totalchk' + i + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '">  ' + arrDateDetails[k].Total + '';
//                                    tab += '</label></td></tr></table>';
//                                }
//                            }
//                            else {

//                                if (arrDateDetails[k].OfferRate != 0) {
//                                    tab += '<table><tr><td style=><label id="" class="Rate"><input type="checkbox" class="Supplier' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + Meals[m] + '" name="ChkTotal' + i + '" id="totalchk' + i + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '"> <i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" /> ' + arrDateDetails[k].Total + '</label></td></tr></table>';
//                                    tab += '<table><tr><td style="color:red"><label><input type="checkbox" class="Supplier' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + Meals[m] + '" name="ChkOffer' + i + '" id="Offerchk' + i + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].OfferRate + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />  ' + arrDateDetails[k].OfferRate + '</label></td></tr></table>';
//                                }
//                                else {
//                                    tab += '<table><tr><td><input type="checkbox" class="Supplier' + i + '_' + arrRoomDetails[r].RoomTypeId + '_' + Meals[m] + '" name="ChkTotal' + i + '"id="totalchk' + i + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />' + arrDateDetails[k].Total + '</td></tr></table>';
//                                }

//                            }
//                            tab += '</td>';
//                        }
//                    }
//                    else {
//                        tab += '<td>-</td>';
//                    }
//                }
//                tab += '</tr>';
//            }
//        }
//        //tab += '<tr style="border:none;"><td colspan="7"><img src="loader.gif"  style="padding-left:20%;padding-right:20%" id="img_file_attach"  alt="" /></td></tr>';
//        tab += '</tbody>'
//        tab += '</table></div></div>'

//    }
//    $("#ul_Supplier").append(ul);
//    $("#tab_Supplier").append(tab);

//    // $("#" + Supplier + MealPlan).attr("checked", "checked");
//    $("#" + MealPlan + Supplier).prop("checked", true);
//    $("#li_Supplier" + Supplier).click();
//    //  GenrateDetails()
//}

function GetRates(MealPlan, Supplier) {
    $("#tbl_HotelList").empty();
    $("#ul_Supplier").empty();
    $("#tab_Supplier").empty();
    var ul = '';
    var tab = '';
    var table = '';
    var cssClass = "";
    for (var i = 0; i < arrSuppliers.length; i++) {

        if (i == 0)
            ul += '<li class="active" id="li_Supplier' + i + '"><a href="#tab-' + i + '">' + arrSuppliers[i] + '</a></li>'
        else
            ul += '<li ><a href="#tab-' + i + '" id="li_Supplier' + i + '">' + arrSuppliers[i] + '</a></li>'                                                              //style="font-size: small";
        if (i == 0)
            tab += '<div id="tab-' + i + '" class="with-padding" > <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
        else
            tab += '<div id="tab-' + i + '" class="with-padding" style="display:none;"> <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
        tab += '<thead>';
        tab += '<tr>';
        tab += '<th style="width:120px;height:30px;">Room Type</th>';
        var arrRates = $.grep(arrRoomRates, function (p) { return p.Name == arrSuppliers[i]; })
                      .map(function (p) { return p; });
        for (var d = 0; d < (arrDatesList.length - 1); d++) {
            tab += '<th style="width:90px;height:30px;">' + arrDatesList[d] + '</th>';
        }
        tab += '<tr>';
        tab += '</thead>';
        tab += '<tbody>'
        tab += '<tr>';

        tab += '</div>';
        tab += '</tr>';
        //   for (var r = 0; r < arrRoomRates.length; r++) {

        for (var rates = 0; rates < arrRates.length; rates++) {

            for (var rooms = 0; rooms < arrRates[rates].RoomOccupancy.length; rooms++) {
                
                for (var detail = 0; detail < arrRates[rates].RoomOccupancy[rooms].Rooms.length; detail++) {
                    var arrDateDetails = [];
                    var sel = '<select id="sel_' + i + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" class="Sel_Room"><option selected="selected" value="0" >0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>';
                    tab += '<tr>';

                    tab += '<td style="width:120px;background-color:white;height:32px;"><input type="checkbox" class="checkbox ChkRoom" name="ChkRoom' + i + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" id="chkRoom' + i + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" value="Supplier' + i + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '"><label id="lbl_' + arrSuppliers[i] + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + ' ">' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '(' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + ')' + '<label><br>' + '' + sel + '</td>';



                    for (var d = 0; d < arrDatesList.length; d++) {
                        var OfferRates = [];



                        if (arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates.length != 0) {
                            for (var dt = 0; dt < arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates.length; dt++) {

                                if (arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates[dt].datetime == arrDatesList[d]) {
                                    arrDateDetails.push(arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates[dt]);
                                }

                            }
                        }
                    }
                    if (arrDateDetails.length != 0) {


                        for (var k = 0; k < arrDateDetails.length; k++) {
                            var Colour = "";
                            if (arrDateDetails[k].NoOfCount == 0) {
                              //  Colour = "#ff8e8e;";
                                cssClass = "YellowTD";
                            }
                            else if (arrDateDetails[k].NoOfCount >= 1 && arrDateDetails[k].NoOfCount <= 100) {
                                //  Colour = "#f9ca83;";
                                cssClass = "OrangeTD";
                            }
                            else
                                //Colour = "#fdfda0;";
                                cssClass = "GreenTD";

                            tab += '<td class="' + cssClass + '" style="background-color:' + Colour + 'font-weight:1000;font-size:15px">';

                            if (arrDateDetails[k].NoOfInventory.length != 0) {
                                var Inventory = arrDateDetails[k].NoOfInventory[0].InvNoOfRoom;
                                var tSold = arrDateDetails[k].NoOfInventory[0].Sold;
                                var Sold = Inventory - tSold;

                                if (arrDateDetails[k].OfferRate != 0) {
                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate" class="Rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '">  ' + arrDateDetails[k].Total + '';
                                    tab += '</label></td></tr></table>';
                                }
                                else {
                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '">  ' + arrDateDetails[k].Total + '';
                                    tab += '</label></td></tr></table>';
                                }
                            }
                            else {

                                if (arrDateDetails[k].OfferRate != 0) {
                                    tab += '<table><tr><td style=><label id="" class="Rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="totalchk' + i + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + '"> <i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" /> ' + arrDateDetails[k].Total + '</label></td></tr></table>';
                                    tab += '<table><tr><td style="color:red"><label><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkOffer' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="Offerchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].OfferRate + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />  ' + arrDateDetails[k].OfferRate + '</label></td></tr></table>';
                                }
                                else {
                                    tab += '<table><tr><td><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />' + arrDateDetails[k].Total + '</td></tr></table>';
                                }

                            }
                            tab += '</td>';
                        }
                    }
                    else {
                        tab += '<td>-</td>';
                    }
                }

            }
            tab += '</tr>';
        }
        //  }
        //tab += '<tr style="border:none;"><td colspan="7"><img src="loader.gif"  style="padding-left:20%;padding-right:20%" id="img_file_attach"  alt="" /></td></tr>';
        tab += '</tbody>'
        tab += '</table></div></div>'

    }
    $("#ul_Supplier").append(ul);
    $("#tab_Supplier").append(tab);

    // $("#" + Supplier + MealPlan).attr("checked", "checked");
    $("#" + MealPlan + Supplier).prop("checked", true);
    $("#li_Supplier" + Supplier).click();
    //  GenrateDetails()

    $(".ChkRoom").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val();
        if (ndRoom == false)
            $("." + arrRate).addClass("checked");
        else
            $("." + arrRate).removeClass("checked");
        debugger
    });
}

var Meals = ["RO", "BB", "HB", "FB"]


function GetValidRates(MealPlan, Supplier, nationality) {
    $("#tbl_HotelList").empty();
    $("#ul_Supplier").empty();
    $("#tab_Supplier").empty();
    var ul = '';
    var tab = '';
    var table = '';
    var cssClass = "";
    for (var i = 0; i < arrSuppliers.length; i++) {

        if (i == 0)
            ul += '<li class="active" id="li_Supplier' + i + '"><a href="#tab-' + i + '">' + arrSuppliers[i] + '</a></li>'
        else
            ul += '<li ><a href="#tab-' + i + '" id="li_Supplier' + i + '">' + arrSuppliers[i] + '</a></li>'                                                              //style="font-size: small";
        if (i == 0)
            tab += '<div id="tab-' + i + '" class="with-padding" > <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
        else
            tab += '<div id="tab-' + i + '" class="with-padding" style="display:none;"> <div class="respTable" style="overflow:scroll"> <table class="table responsive-table HotelList" id="tbl_HotelList' + i + '" ><br/>'
        tab += '<thead>';
        tab += '<tr>';
        tab += '<th style="width:120px;height:30px;">Room Type</th>'; 
            arrRates = $.grep(arrRoomRates, function (p) { return p.Name == arrSuppliers[i] && p.Nationality == nationality; })
                    .map(function (p) { return p; })
        for (var d = 0; d < (arrDatesList.length - 1) ; d++) {
            tab += '<th style="width:90px;height:30px;">' + arrDatesList[d] + '</th>';
        }
        tab += '<tr>';
        tab += '</thead>';
        tab += '<tbody>'
        tab += '<tr>';

        tab += '</div>';
        tab += '</tr>';
        //   for (var r = 0; r < arrRoomRates.length; r++) {

        for (var rates = 0; rates < arrRates.length; rates++) {

            for (var rooms = 0; rooms < arrRates[rates].RoomOccupancy.length; rooms++) {

                for (var detail = 0; detail < arrRates[rates].RoomOccupancy[rooms].Rooms.length; detail++) {
                    var arrDateDetails = [];
                    var sel = '<select id="sel_' + i + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" class="Sel_Room"><option selected="selected" value="0" >0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>';
                    tab += '<tr>';

                    tab += '<td style="width:120px;background-color:white;height:32px;"><input type="checkbox" class="checkbox ChkRoom" name="ChkRoom' + i + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" id="chkRoom' + i + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '" value="Supplier' + i + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeId + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + '"><label id="lbl_' + arrSuppliers[i] + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '_' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + ' ">' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomTypeName + '(' + arrRates[rates].RoomOccupancy[rooms].Rooms[detail].RoomDescription + ')' + '<label><br>' + '' + sel + '</td>';




                    for (var d = 0; d < arrDatesList.length; d++) {
                        var OfferRates = [];



                        if (arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates.length != 0) {
                            for (var dt = 0; dt < arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates.length; dt++) {

                                if (arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates[dt].datetime == arrDatesList[d]) {
                                    arrDateDetails.push(arrRates[rates].RoomOccupancy[rooms].Rooms[detail].Dates[dt]);
                                }

                            }
                        }
                    }

                    if (arrDateDetails.length != 0) {


                        for (var k = 0; k < arrDateDetails.length; k++) {
                            var Colour = "";
                            if (arrDateDetails[k].NoOfCount == 0) {
                                // Colour = "#ff8e8e;";
                                cssClass = "YellowTD";

                            }
                            else if (arrDateDetails[k].NoOfCount >= 1 && arrDateDetails[k].NoOfCount <= 100) {
                                // Colour = "#f9ca83;";
                                cssClass = "OrangeTD";
                            }
                            else
                                // Colour = "#fdfda0;";
                                cssClass = "GreenTD";

                            tab += '<td class="' + cssClass + '" style="background-color:' + Colour + 'font-weight:1000;font-size:15px">';

                            if (arrDateDetails[k].NoOfInventory.length != 0) {
                                var Inventory = arrDateDetails[k].NoOfInventory[0].InvNoOfRoom;
                                var tSold = arrDateDetails[k].NoOfInventory[0].Sold;
                                var Sold = Inventory - tSold;

                                if (arrDateDetails[k].OfferRate != 0) {
                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate" class="Rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '">  ' + arrDateDetails[k].Total + '';
                                    tab += '</label></td></tr></table>';
                                }
                                else {
                                    tab += '<table><tr id="tr_rate" class="tr_invRate"><td id="td_rate" class="td_invRate"><label id="lbl_rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '">  ' + arrDateDetails[k].Total + '';
                                    tab += '</label></td></tr></table>';
                                }
                            }
                            else {

                                if (arrDateDetails[k].OfferRate != 0) {
                                    tab += '<table><tr><td style=><label id="" class="Rate"><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"> <i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" /> ' + arrDateDetails[k].Total + '</label></td></tr></table>';
                                    tab += '<table><tr><td style="color:red"><label><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkOffer' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" id="Offerchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].OfferRate + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />  ' + arrDateDetails[k].OfferRate + '</label></td></tr></table>';
                                }
                                else {
                                    tab += '<table><tr><td><input type="checkbox" class="checkbox Supplier' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '" name="ChkTotal' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"id="totalchk' + i + '_' + arrDateDetails[k].RoomTypeId + '_' + arrDateDetails[k].MealPlan + '"  value="' + arrDateDetails[k].datetime + "_" + arrDateDetails[k].Total + "_" + arrDateDetails[k].RateTypeId + "_" + arrDateDetails[k].Currency + "_" + arrDateDetails[k].RoomTypeId + "_" + arrDateDetails[k].MealPlan + '"><i class="' + GetCurrencyIcon(arrDateDetails[k].Currency) + '" />' + arrDateDetails[k].Total + '</td></tr></table>';
                                }

                            }
                            tab += '</td>';
                        }
                    }
                    else {
                        tab += '<td>-</td>';
                    }
                }

            }
            tab += '</tr>';
        }
        //  }
        //tab += '<tr style="border:none;"><td colspan="7"><img src="loader.gif"  style="padding-left:20%;padding-right:20%" id="img_file_attach"  alt="" /></td></tr>';
        tab += '</tbody>'
        tab += '</table></div></div>'

    }
    $("#ul_Supplier").append(ul);
    $("#tab_Supplier").append(tab);

    // $("#" + Supplier + MealPlan).attr("checked", "checked");
    $("#" + MealPlan + Supplier).prop("checked", true);
    $("#li_Supplier" + Supplier).click();
    // GenrateDetails()

        $(".ChkRoom").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val();
        if (ndRoom == false)
            $("." + arrRate).addClass("checked");
        else
            $("." + arrRate).removeClass("checked");
        debugger
    });
    
}

function SearchSelect(x) {

    if (x == "General") {
        $('.advance').css("display", "none");
        $('.test').css("display", "none");
        $("#sel_Nationality").attr("multiple", "multiple")
    }
    else if (x == "Advance") {
        $('.advance').css("display", "");
        $("#sel_Nationality").removeAttr("multiple")
    }
}

function CheckNationality(currentval) {
    if ($("#sel_Nationality option[value='All']").is(':selected')) {
        selectAll();
    }
    else if ($("#sel_Nationality option[value='Unselect']").is(':selected')) {
        UnselectAll();
    }

}

function selectAll() {
    $('#sel_Nationality option').prop('selected', true);
    $("#sel_Nationality option[value='All']").click();
}

function UnselectAll() {
    $('#sel_Nationality option').prop('selected', false);
    $("#sel_Nationality option[value='All']").click();
}

function CheckValidNationality(currentval) {
    if ($("#sel_ValidNationality option[value='All']").is(':selected')) {
        selectAllValid();
    }
    else if ($("#sel_ValidNationality option[value='Unselect']").is(':selected')) {
        UnselectAllValid();
    }

    GetValidRates('RO', 0, currentval);
}

function selectAllValid() {
    $('#sel_ValidNationality option').prop('selected', true);
    $("#sel_ValidNationality option[value='All']").click();
}

function UnselectAllValid() {
    $('#sel_ValidNationality option').prop('selected', false);
    $("#sel_ValidNationality option[value='All']").click();
}

