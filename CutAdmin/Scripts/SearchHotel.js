﻿

//redirect to search page with data
function Redirect() {
    debugger;
    //AddHotelSearchLog();
    var destination = $('#hdnDCode').val();
    var fdate = $('#datepicker_HotelCheckin').val();
    var tdate = $('#datepicker_HotelCheckout').val();
    var hotel = $('#hdnHCode').val();
    var rating = $('#Select_StarRating').val();
    var HName = $('#txt_HotelName').val();
    var DName = $('#txtCity').val();
    var room = $('#Select_Rooms').val();
    var nationalityCode = $('#Select_Nationality').val();

    if (hotel == null) {
        hotel = "";
    }
    if (HName == null) {
        HName = "";
    }
    if (rating == null) {
        rating = "";
    }
    //var nationalityCountry = $('#Select_Nationality').text();
    if (destination == "") {
        //alert("Enter the city name where you want to go!");
        $('#SpnMessege').text("Enter the city name where you want to go!");
        $("#ModelMessege").css("display", "")
        return false;
    }
    else if (nationalityCode == "-") {
        $('#SpnMessege').text("Please select nationality!");
        $("#ModelMessege").css("display", "")

        // alert("Please select nationality!");
        return false;
    }
    else {
        var roomcount = parseInt(room);
        var occupancy = '';
        for (var i = 0; i < roomcount; i++) {
            if (i == 0) {
                occupancy = Room1();
            }
            else if (i == 1) {
                occupancy = occupancy + '$' + Room2();
            }
            else if (i == 2) {
                occupancy = occupancy + '$' + Room3();
            }
            else if (i == 3) {
                occupancy = occupancy + '$' + Room4();
            }
            else if (i == 4) {
                occupancy = occupancy + '$' + Room5();
            }
            else if (i == 5) {
                occupancy = occupancy + '$' + Room6();
            }
            else if (i == 6) {
                occupancy = occupancy + '$' + Room7();
            }
            else if (i == 7) {
                occupancy = occupancy + '$' + Room8();
            }
            else if (i == 8) {
                occupancy = occupancy + '$' + Room9();
            }
        }
        var session = destination + '_' + DName + '_' + fdate + '_' + tdate + '_' + room + '_' + occupancy + '_' + hotel + '_' + HName + '_' + rating + '_' + nationalityCode;

        $.ajax({
            url: "VisaHandler.asmx/AddSearch",
            type: "post",
            data: '{"CityName":"' + DName + '","CheckIn":"' + $('#datepicker_HotelCheckin').val() + '","CheckOut":"' + $('#datepicker_HotelCheckout').val() + '","HotelCode":"' + $('#hdnHCode').val() + '","TotalNights":"' + occupancy + '","StarRating":"' + rating + '","Nationality":"' + nationalityCode + '","NumberOfRooms":"' + room + '","EntryDate":"' + fdate + '","AddSearchsession":"' + session + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            }
        })
        window.location.href = "waitforresponse.aspx?session=" + session;
    }
}