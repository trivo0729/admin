﻿$(function () {
    HotelCode = GetQueryStringParams('sHotelID');
    HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
    HotelAddr = GetQueryStringParams('HAddress').replace(/%20/g, ' ');
    SupplierId = GetQueryStringParams('SupplierId').replace(/%20/g, ' ');
    $("#lbl_Hotel").text(HotelName);
    $("#lbl_address").text(HotelAddr);
    GetRooms(HotelCode);


})

var ListNDates = [];
var arrDates = new Array();
var arrDatesList = new Array();
var arrDatesList2 = new Array();
var FullDate = new Array();
var DatList = "";
var RoomList = "";
var RoomTypeList = "";
var Month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
function GetRooms(HotelCode) {

    $.ajax({
        type: "POST",
        url: "../handler/RoomHandler.asmx/GetRooms",
        data: JSON.stringify({ sHotelId: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                RoomList = result.RoomList;
                RoomTypeList = result.RoomTypeList;

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}


function SearchInventory() {

    var Startdt = $('#datepicker_start').val();
    //var Startdt = new Date();
    var Enddt = $('#datepicker_end').val();


    var data =
         {
             HotelCode: HotelCode,
             Startdt: Startdt,
             Enddt: Enddt,
             SupplierId: SupplierId,
         }
    $.ajax({
        type: "POST",
        url: "InventoryHandler.asmx/SearchInventory",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {

            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                DatList = result.ListDatas;
                ListNDates = result.ListNDates;
                arrDates = new Array();
                arrDatesList = new Array();
                arrDatesList2 = new Array();
                FullDate = new Array();
                for (var m = 0; m < ListNDates.length; m++) {
                    for (var i = 0; i < ListNDates[m].List_Newdates.length; i++) {
                        arrDates.push(moment(ListNDates[m].List_Newdates[i]).format("L"));
                    }
                }
                for (var j = 0; j < arrDates.length; j++) {
                    var dateAr = arrDates[j].split('/');
                    //arrDatesList.push(dateAr[1] + '-' + dateAr[0].slice(-2) + '-' + dateAr[2]);
                    // FullDate.push(arrDates[j]);
                    FullDate.push(dateAr[1] + '-' + dateAr[0] + '-' + dateAr[2]);
                    arrDatesList.push(dateAr[1] + '-' + dateAr[0]);
                    arrDatesList2.push(dateAr[1] + '-' + dateAr[0]);
                }
                if ($("#Allocation_Id").is(":checked")) {
                    GetInventory("Allocation")
                }
                else if ($("#Allotmnet_Id").is(":checked")) {
                    GetInventory("Allotment")
                }
                else if ($("#Freesale_Id").is(":checked")) {
                    GetInventory("FreeSale")
                }
            }
        }
    });
}

function GetInventory(InventoryType) {
    var row = '';
    $("#div_tbl_InvList").empty();
    row += '<table class="table responsive-table colorTable responsive-table-on InvList" id="tbl_InvList" style="font-size: small;"  >';
    row += '<thead>';
    row += '<tr >';
    row += '<th>Room Type</th>';
    for (var d = 0; d < arrDatesList.length; d++) {
        var sDate = arrDatesList[d].split('-');
        var sMonth = Month[parseInt(sDate[1])-1 ]
        row += '<th><input type="checkbox" class="checkbox Date"   value="' + FullDate[d] + '"/>' + sDate[0] +'-'+  sMonth + '</th>';
    }
    row += '</tr>';
    row += '</thead>';
    row += '<tbody>'
    
    // row += '</div>';


    for (var i = 0; i < DatList.length; i++) {
        
        for (var j = 0; j < DatList[i].Listdata.length; j++) {
            if (DatList[i].Listdata[j].InventoryType == InventoryType) {
                row += '<tr class="RateDetails">';
                var Name = $.grep(RoomTypeList, function (p) { return p.RoomTypeID == DatList[i].Listdata[j].RoomType; })
                 .map(function (p) { return p.RoomType; })[0];
                row += '<td ><input type="checkbox" class="checkbox ChkRoom" name="ChkTotal' + i + '" id="chk' + i + '_' + Name + '(' + DatList[i].Listdata[j].RateType + ')' + '(' + DatList[i].Listdata[j].Sid + ') ' + '" value="' + Name + '(' + DatList[i].Listdata[j].RateType + ')' + '-' + DatList[i].Listdata[j].Sid + '-' +  DatList[i].Listdata[j].RoomType + DatList[i].Listdata[j].RateType+'" onclick="ChakedByRoom(\'' + DatList[i].Listdata[j].RoomType + DatList[i].Listdata[j].RateType + '\')"><label id="lbl_Room">' + Name + '(' + DatList[i].Listdata[j].RateType + ')' + '<label><br></td>';
                for (var d = 0; d < arrDatesList.length; d++) {

                    var dt1 = FullDate[d].split('-')[0];
                    var mn1 = FullDate[d].split('-')[1];
                    var yr1 = FullDate[d].split('-')[2];
                    var dt = arrDatesList2[d].split('-')[0];
                    var mn = arrDatesList2[d].split('-')[1];
                    var mnt = DatList[i].Listdata[j].Month;
                    row += DayInventory(dt, mn, mnt, DatList[i].Listdata[j], j, i, FullDate[d], DatList[i].Listdata[j].RoomType + DatList[i].Listdata[j].RateType)
                }
                row += '</tr>';
            }
        }
    }
    row += '</tr>';
    row += '</tbody>';
    row += '</table>';
    $("#div_tbl_InvList").append(row);
    $("#tbl_InvList").dataTable({
        bSort: false,
        sPaginationType: 'full_numbers',
        sSearch:false
    });
    $("#tbl_InvList_wrapper").addClass("respTable");
    $(".ChkRoom").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val().split('-')[2];
        if (ndRoom==false)
            $("." + arrRate).addClass("checked");
        else
            $("." + arrRate).removeClass("checked");
        debugger
    });
    $(".Date").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val();
        if (ndRoom == false)
            $("." + arrRate).addClass("checked");
        else
            $("." + arrRate).removeClass("checked");
        debugger
    });
}



function DayInventory(dt, mn, mnt, Listdata, j, i,Date,RoomType) {
    var dayRate = '';
    var row = "";
    switch (true) {
        case (dt == "01" && mn == mnt):
            dayRate = Listdata.Date_1;
            break;
        case (dt == "02" && mn == mnt):
            dayRate = Listdata.Date_2;
            break;
        case (dt == "03" && mn == mnt):
            dayRate = Listdata.Date_3;
            break;
        case (dt == "04" && mn == mnt):
            dayRate = Listdata.Date_4;
            break;
        case (dt == "05" && mn == mnt):
            dayRate = Listdata.Date_5;
            break;
        case (dt == "06" && mn == mnt):
            dayRate = Listdata.Date_6;
            break;
        case (dt == "07" && mn == mnt):
            dayRate = Listdata.Date_7;
            break;
        case (dt == "08" && mn == mnt):
            dayRate = Listdata.Date_8;
            break;
        case (dt == "09" && mn == mnt):
            dayRate = Listdata.Date_9;
            break;
        case (dt == "10" && mn == mnt):
            dayRate = Listdata.Date_10;
            break;
        case (dt == "11" && mn == mnt):
            dayRate = Listdata.Date_11;
            break;
        case (dt == "12" && mn == mnt):
            dayRate = Listdata.Date_12;
            break;
        case (dt == "13" && mn == mnt):
            dayRate = Listdata.Date_13;
            break;
        case (dt == "14" && mn == mnt):
            dayRate = Listdata.Date_14;
            break;
        case (dt == "15" && mn == mnt):
            dayRate = Listdata.Date_15;
            break;
        case (dt == "16" && mn == mnt):
            dayRate = Listdata.Date_16;
            break;
        case (dt == "17" && mn == mnt):
            dayRate = Listdata.Date_17;
            break;
        case (dt == "18" && mn == mnt):
            dayRate = Listdata.Date_18;
            break;
        case (dt == "19" && mn == mnt):
            dayRate = Listdata.Date_19;
            break;
        case (dt == "20" && mn == mnt):
            dayRate = Listdata.Date_20;
            break;
        case (dt == "21" && mn == mnt):
            dayRate = Listdata.Date_21;
            break;
        case (dt == "22" && mn == mnt):
            dayRate = Listdata.Date_22;
            break;
        case (dt == "23" && mn == mnt):
            dayRate = Listdata.Date_23;
            break;
        case (dt == "24" && mn == mnt):
            dayRate = Listdata.Date_24;
            break;
        case (dt == "25" && mn == mnt):
            dayRate = Listdata.Date_25;
            break;
        case (dt == "26" && mn == mnt):
            dayRate = Listdata.Date_26;
            break;
        case (dt == "27" && mn == mnt):
            dayRate = Listdata.Date_27;
            break;
        case (dt == "28" && mn == mnt):
            dayRate = Listdata.Date_28;
            break;
        case (dt == "29" && mn == mnt):
            dayRate = Listdata.Date_29;
            break;
        case (dt == "30" && mn == mnt):
            dayRate = Listdata.Date_30;
            break;
        case (dt == "31" && mn == mnt):
            dayRate = Listdata.Date_31;
            break;
    }
    if(dayRate != null)
    {
        dayRate = dayRate.split('_');
        if (dayRate[0] == "fs" && dayRate[1] != "")
            row += '<td style="width:120px;background-color:white;height:32px;"><input type="checkbox" class="checkbox ' + RoomType + ' ' + Date + ' Chkdate' + j + '" name="ChkTotal' + i + '" id="chk' + i + '_' + dt + '" value="' + Date + '"><label id="lbl_Room">' + dayRate[0] + '<label><br></td>';
        else
            row += '<td style="width:120px;background-color:white;height:32px;"><input type="checkbox" class="checkbox ' + RoomType + ' ' + Date + '  Chkdate' + j + '" name="ChkTotal' + i + '" id="chk' + i + '_' + dt + '" value="' + Date + '"><label id="lbl_Room">' + dayRate[0] + '<label><br></td>';
    }
    else
        row += '<td style="width:120px;background-color:white;height:32px;"><label id="lbl_Room">-<label><br></td>';
    return row;
}
function ChakedByRoom(RoomType) {
    $("." + RoomType).click();
}
function SelectByDate(Date) {
    $("." + Date).click();
}

function AllotmentInventory() {
    $("#tbl_InvList").dataTable().fnClearTable();
    $("#tbl_InvList").dataTable().fnDestroy();
    if ($("#Allocation_Id").is(":checked")) {
        GetInventory("Allocation")
    }
    else if ($("#Allotmnet_Id").is(":checked"))
    {
        GetInventory("Allotment")
    }
    else if ($("#Freesale_Id").is(":checked")) {
        GetInventory("FreeSale")
    }
}

var ListRoom = new Array();;
var ListRoomRange = [];
function StartSaleModal() {
    ListRoom = new Array();
    var Room = $('.RateDetails');
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0]
            var sID = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[1];
           
            for (var i = 1; i < ndListDate.length; i++) {
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked)
                {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length !=0)
                    ListRoom.push({ ID: sID, Name: sRoom, Date: arrDate });
                    arrDate = new Array();
                }
              
            }
            if(arrDate.length !=0)
                ListRoom.push({ ID: sID, Name: sRoom, Date: arrDate });
        }
    }

    if (ListRoom.length == 0) {
        Success("Please Select  Dates");
        return false;
    }
    $.modal.confirm(EditModal(), function () {
        UpdateInventory("Start Sale")

    }, function () {
        $('#modals').remove();
    });
}

var fs, ss;
function StopSaleModal() {

    ListRoom = new Array();
    var Room = $('.RateDetails');
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();;
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0]
            var sID = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[1];

            for (var i = 1; i < ndListDate.length; i++) {
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked) {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length != 0)
                        ListRoom.push({ ID: sID, Name: sRoom, Date: arrDate });
                    arrDate = new Array();
                }

            }
            if (arrDate.length != 0)
                ListRoom.push({ ID: sID, Name: sRoom, Date: arrDate });
        }
    }

    if (ListRoom.length == 0) {
        Success("Please Select  Dates");
        return false;
    }
    $.modal.confirm(EditModal(), function () {
        UpdateInventory("Stop Sale")

    }, function () {
        $('#modals').remove();
    });
}

var arrRate = new Array();
function EditModal() {
    arrRate = new Array();
    var tab = '';
    tab += 'Please Confirm  Selected Dates & Room<br><br>';
    for (var i = 0; i < ListRoom.length; i++) {
        {
            tab += '<span  style="font:bold">' + ListRoom[i].Name + '</span><br>';
            tab += '<span>' + ListRoom[i].Date[0] + '</span> to ';
            tab += '<span>' + ListRoom[i].Date[ListRoom[i].Date.length - 1] + '</span>';
            tab += '<br>'
        }

    }
    tab += '<br>'
    tab += '<br>'
    //$("#div_range").append(tab);
    return tab;
}


function UpdateInventory(value) {

    $.ajax({
        type: "POST",
        url: "Inventoryhandler.asmx/UpdateInventory",
        data: JSON.stringify({ ListRoomRate: ListRoom, status: value }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Inventory Updated Successfully");
                SearchInventory();
               
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })

}


