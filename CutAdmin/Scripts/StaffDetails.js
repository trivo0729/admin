﻿var HiddenId;
var arRoleList = new Array();
var arFormList = new Array();
var arFormListForRole = new Array();
var arrayToSubmit = new Array();

$(document).ready(function () {
    GetStaffDetails();
});


function GetStaffDetails() {
    debugger;
    $("#tbl_StaffDetails").dataTable().fnClearTable();
    $("#tbl_StaffDetails").dataTable().fnDestroy();
    //$("#tbl_StaffDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "handler/GenralHandler.asmx/GetStaffDetails",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_StaffDetails = result.Staff;
                var tRow = '';
                for (var i = 0; i < List_StaffDetails.length; i++) {
                    var Name;
                    var s = List_StaffDetails[i].ContactPerson.split('undefined');
                    if (s.length > 2)
                        Name = s[0] + " " + s[1]
                    else
                        Name = s
                    tRow += '<tr>';

                    List_StaffDetails[i].Validity = moment(List_StaffDetails[i].Validity).format("L LTS");

                    tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#StaffDetailModal" onclick="StaffDetailsModal(\'' + List_StaffDetails[i].AgencyName + '\',\'' + List_StaffDetails[i].Validity + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\',\'' + List_StaffDetails[i].Designation + '\',\'' + List_StaffDetails[i].Address + '\',\'' + List_StaffDetails[i].Description + '\',\'' + List_StaffDetails[i].Countryname + '\',\'' + List_StaffDetails[i].PinCode + '\',\'' + List_StaffDetails[i].phone + '\',\'' + List_StaffDetails[i].Mobile + '\',\'' + List_StaffDetails[i].Fax + '\',\'' + List_StaffDetails[i].email + '\',\'' + List_StaffDetails[i].Website + '\',\'' + List_StaffDetails[i].PANNo + '\',\'' + List_StaffDetails[i].Department + '\'); return false" title="Click to view Staff Details">' + Name + '  ' + List_StaffDetails[i].Last_Name + '</a></td>';
                    tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].password + '\'); return false" title="Click to edit Password">' + List_StaffDetails[i].uid + '</a></td>';
                    //tRow += '<td>' + List_StaffDetails[i].Designation + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Mobile + '</td>';
                    tRow += '<td>' + List_StaffDetails[i].StaffUniqueCode + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Description + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Countryname + '</td>';
                    var Title = "";
                    var flag = List_StaffDetails[i].LoginFlag;
                    if (flag == true) {
                        flag = "True";
                        Title = "Deactivate";
                    }
                    else {
                        flag = "False";
                        Title = "Activate";
                    }
                    flag = flag.replace("True", "fa fa-eye").replace("False", "fa fa-eye-slash")


                    //if (List_StaffDetails[i].LoginFlag == true)
                    //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">No</label></span></td>';
                    //else
                    //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">No</label></span></td>';


                    if (List_StaffDetails[i].LoginFlag == true) {
                        tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_StaffDetails[i].sid + '" name="medium-label-3" class="switch tiny" value="On"  checked  onchange="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\')"></td>';
                    }
                    else {
                        tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_StaffDetails[i].sid + '" name="medium-label-3" class="switch tiny" value="On"   onchange="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\')"></td>';
                    }


                    tRow += '<td class="align-center">'
                    tRow += '<span class="button-group">'
                    tRow += '			<a href="#" class="button" title="Staff Roles" onclick="GetFormList(\'' + List_StaffDetails[i].sid + '\')"><span class="icon-user"></span></a>'
                    tRow += '			<a href="AddStaff.aspx?sid=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sLastName=' + List_StaffDetails[i].Last_Name + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory + '&sDepartment=' + List_StaffDetails[i].Department + '" class="button" title="Edit"><span class="icon-pencil"></span></a>'
                    //tRow += '            <a href="#" class="button" title="' + Title + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"><span class="' + flag + '"></span></a>'
                    tRow += '			<a href="#" class="button" title="trash" onclick="DeleteStaffID(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\')"><span class="icon-trash"></span></a>'
                    tRow += '		</span>'
                    tRow += '</td>'
                    //tRow += '<td align="center"><a style="cursor:pointer" title="Edit" href="AddStaff.aspx?sid=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory + '&sDepartment=' + List_StaffDetails[i].Department + '"><span class="icon-pencil"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer" href="#"><span class="' + flag + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + flag + '" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp;<a> <span class="icon-user" title="Staff Roles" aria-hidden="true" style="cursor:pointer" onclick="GetFormList(\'' + List_StaffDetails[i].sid + '\')"></span></a> |  <a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaffID(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_StaffDetails tbody").html(tRow);
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
                $("#tbl_StaffDetails").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
                $("#tbl_StaffDetails").removeAttr("style");
                //$("#tbl_StaffDetails_length select").addClass("select anthracite-gradient glossy")
            }
        },
        error: function () {
        }
    });
}

function Activate(sid, flag, name, Last_Name) {
    var StaffStatus = "Activate";
    if (flag == "true") {
        StaffStatus = "Deactivate";
    }
    var status = flag.replace("True", "0").replace("False", "1");
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + StaffStatus + "<br/><span class=\"orange\"> " + name + '  ' + Last_Name + '</span>?</p>', function () {
        $.ajax({
            url: "HotelAdmin/Handler/GenralHandler.asmx/ActivateStaffLogin",
            type: "post",
            data: '{"sid":"' + sid + '","status":"' + status + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Staff status has been changed successfully!")
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)

                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }, function () {
        $('#modals').remove();
    });
}
function GetFormList(sid) {
    HiddenId = sid;
    $("#tblStaffForms").empty();
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/RoleManagementHandler.asmx/GetFormList?sid=" + sid,
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.Arr;
                if (arFormList.length > 0) {
                    var trForms = '<tbody>';
                    for (i = 0; i < arFormList.length; i = i + 4) {
                        if (i < arFormList.length) {
                            trForms += '<tr>';
                            trForms += '<td><input id="chk' + arFormList[i].sFormName + '" class="checkbox mid-margin-left chkFrm" type="checkbox" value="' + arFormList[i].nId + '"/> ' + arFormList[i].sDisplayName + '</td>';
                            if ((i + 1) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 1].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 1].nId + '"/> ' + arFormList[i + 1].sDisplayName + '</td>';
                            if ((i + 2) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 2].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 2].nId + '"/> ' + arFormList[i + 2].sDisplayName + '</td>';
                            if ((i + 3) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 3].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 3].nId + '"/> ' + arFormList[i + 3].sDisplayName + '</td>';
                            trForms += '</tr>';
                        }
                    }
                    trForms += '</tbody>';
                    //$("#tblStaffForms").append(trForms);
                    //$('input[type=checkbox]').attr("disabled", true);
                    $.modal({
                        content: '<table class="table table-striped table-bordered" id="tblStaffForms" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                     '</table><br>' +
                     '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',

                        title: 'Assign Roles',
                        width: 600,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                arFormListForRole = result.arrAuthForm;
                CheckRoles();

            }
        },
        error: function () {
            Success("An error occured while geting form list");
        }
    });
}
function CheckRoles() {
    debugger;
    if (arFormListForRole.length > 0) {
        for (j = 0; j < arFormList.length; j++) {
            for (k = 0; k < arFormListForRole.length; k++) {
                if (arFormList[j].sFormName == arFormListForRole[k].sFormName) {
                    $('#chk' + arFormList[j].sFormName).click();
                }
            }
        }
    }
}

function SubmitFormsForRole() {
    var j = 0;
    var arrFrm = $(".checked")
    for (var i = 0; i < arrFrm.length; i++) {
        arrayToSubmit[j] = arrFrm[i].childNodes[1].value;
        j++;
    }
    var arrayJson = JSON.stringify(arrayToSubmit);
    if (HiddenId != 'null') {
        $.ajax({
            type: "POST",
            url: "HotelAdmin/Handler/RoleManagementHandler.asmx/SetFormsForAdminStaff?sid=" + HiddenId,
            data: '{"StaffUid":"' + HiddenId + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
                }
                else if (result.retCode == 0) {
                    Success("Something went wrong!");
                }
            },
            error: function () {
                Success("Error occured while submitting checked forms.");
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}

function DeleteStaffID(uid, staffname, Last_Name) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + staffname + '  ' + Last_Name + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "HotelAdmin/Handler/GenralHandler.asmx/Delete",
                type: "post",
                data: '{"uid":"' + uid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Staff has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

function ExportStaffDetailsToExcel() {
    window.location.href = "HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=StaffDetails";
}


function PasswordModal(sid, uid, StaffName, password) {
    HiddenId = sid;
    GetPassword(sid, uid, StaffName, password);

};

function GetPassword(sid, uid, StaffName, password) {
    $("#txt_Password").val('');
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/GetPassword",
        data: '{"password":"' + password + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                pass = result.password;
                $.modal({
                    content: '<div class="modal-body">' +
            '<div class="scrollingDiv">' +
            '<div class="columns">' +
            '<div class="three-columns bold">User ID:</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="Agency-value" value="' + uid + '" class="input-unstyled full-width" type="text">' + '</div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Staff Name</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="' + StaffName + '" class="input-unstyled full-width" type="text"></div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Password</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + pass + '" class="input-unstyled full-width" type="text"></div></div> ' +
            '</div>' +
            '<div class="columns">' +
            '<div class="three-columns bold">&nbsp;</div>' +
            //'<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="openEmail()">Email</button>' +
            '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePassword()">Change&nbsp;Password</button></div>' +
            '</div></div></div>',

                    title: 'Edit Password',
                    width: 500,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
        error: function () {
            Success("An error occured while loading password.")
        }
    });
}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $('#lbl_ErrPassword').css("display", "none");
            $.modal.confirm('Are you sure you want to change password?', function () {
                $.ajax({
                    type: "POST",
                    url: "HotelAdmin/Handler/GenralHandler.asmx/StaffChangePassword",
                    data: '{"sid":"' + HiddenId + '","password":"' + $('#txt_Password').val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.Session != 1) {
                            Success("Session Expired");
                        }
                        if (result.retCode == 1) {
                            Success("Password changed successfully");
                            setTimeout(function () {
                                window.location.reload();
                            }, 500)
                        }
                        if (result.retCode == 0) {
                            Success("Something went wrong while processing your request! Please try again.");
                        }
                    },
                    error: function () {
                    }
                });
            }, function () {
                $('#modals').remove();
            });
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.Success('No Change found in Password!', {
            buttons: {
                'Cancel': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            }
        });
    }

}


function StaffDetailsModal(AgencyName, Validity, ContactPerson, LastName, sDesignation, Address, Description, Countryname, PinCode, phone, Mobile, Fax, email, Website, PANNo, Department) {
    $.modal({
        content: '<div class="respTable">' +
           '<table class="table responsive-table evenodd" id="sorting-advanced">' +
           '<tr><td class="even">Name:</td><td>' + ContactPerson + '  ' + LastName + '</td><td class="even">Email:</td><td>' + email + '</td></tr>' +
           '<tr><td class="even">Mobile:</td><td>' + Mobile + '</td><td class="even">Designation:</td><td>' + sDesignation + '</td></tr>' +
           '<tr><td class="even">Department:</td><td>' + Department + '</td><td class="even">Valid Upto:</td><td>' + Validity + '</td></tr></tr>' +
           '</tr>' +
           '</table>' +
           '</div>',
        title: 'Staff Details',
        width: 600,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
};
