﻿$(function () {
    GetPackages();

    $("#txt_ValidUpto").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
})
var HiddenId = 0;
function GetPackages() {
    $("#tbl_Package").dataTable().fnClearTable();
    $("#tbl_Package").dataTable().fnDestroy();
    //$("#tbl_Package tbody tr").remove()
    var Currency = $("#selCurrency").val();
    $.ajax({
        url: "../Handler/StaticPakageHandller.asmx/GetStaticPakages",
        type: "post",
        data: '{"Currency":"' + Currency + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arrPakages = result.tbl_Pakages;
                var tr = '';
                for (var i = 0; i < arrPakages.length; i++) {
                    var Currency = arrPakages[i].Currency;
                    if (Currency == "INR") {
                        Currency = "fa fa-inr";
                    }
                    else if (Currency == "USD") {
                        Currency = "fa fa-dollar";
                    }
                    else if (Currency == "AED") {
                        Currency = "Currency-AED";
                    }
                    tr += '<tr> '
                    tr += '<td>' + (i + 1) + '</td>                                                                                                                          '
                    tr += '<td>' + arrPakages[i].Package_Name + '</td>                                                                                                '
                    tr += '<td>' + arrPakages[i].Location + '</td>                                                                                           '
                    tr += '<td>' + arrPakages[i].Stay.replace('1', '1 Night').replace('2', '2 Night').replace('3', '3 Night') + '</td>                                                                                                                    '
                    tr += '<td><i class="' + Currency + '"> </i> ' + arrPakages[i].Pakages_Price + '</td>                                                                                                                    '
                    tr += '<td>                                                                                                                                '
                    tr += ' ' + arrPakages[i].StarRating.replace('1', '<img src="../img/1.png" style=" margin-left: 10%;" />').replace('2', '<img src="../img/2.png" style=" margin-left: 10%;"/>').replace('3', '<img src="../img/3.png" style=" margin-left: 10%;"/>').replace('4', '<img src="../img/4.png" style=" margin-left: 10%;"/>').replace('5', '<img src="../img/5.png" style=" margin-left: 10%;"/>') + '</td>'
                    if (arrPakages[i].ValidUpto == null) {
                        tr += '<td></td>'
                    }
                    else {
                        tr += '<td>' + arrPakages[i].ValidUpto + '</td>'
                    }
                    if (arrPakages[i].Status == "True") {
                        tr += '<td class="align-center"><input type="checkbox" id="chk_On' + arrPakages[i].sid + '" name="medium-label-3" class="switch tiny" value="On"  checked  onchange="Activate(\'' + arrPakages[i].sid + '\',\'' + arrPakages[i].Status + '\',\'' + arrPakages[i].Package_Name + '\')"></td>'
                    }
                    else {
                        tr += '<td class="align-center"><input type="checkbox" id="chk_On' + arrPakages[i].sid + '" name="medium-label-3" class="switch tiny" value="On"   onchange="Activate(\'' + arrPakages[i].sid + '\',\'' + arrPakages[i].Status + '\',\'' + arrPakages[i].Package_Name + '\')"></td>'
                    }
                    //tr += '<td class="align-center"><a class="button" onclick="UpdatePackage(\'' + arrPakages[i].sid + '\')"><span class="icon-pencil"></span></a></td>'
                    //tr += '<td class="align-center"><a class="button" onclick="UpdatePackage("' + arrPakages[i].sid + '","' + arrPakages[i].Package_Name + '","' + arrPakages[i].Location + '","' + arrPakages[i].Pakages_Price + '","' + arrPakages[i].Stay + '","' + arrPakages[i].StarRating + '","' + arrPakages[i].Currency + '","' + arrPakages[i].ValidUpto + '")"><span class="icon-pencil"></span></a></td>'
                    tr += '<td class="align-center"><a class="button" onclick="LoadPackage(\'' + arrPakages[i].sid + '\',\'' + arrPakages[i].Package_Name + '\',\'' + arrPakages[i].Location + '\',\'' + arrPakages[i].Pakages_Price + '\',\'' + arrPakages[i].Stay + '\',\'' + arrPakages[i].StarRating + '\',\'' + arrPakages[i].Currency + '\',\'' + arrPakages[i].ValidUpto + '\')"><span class="icon-pencil"></span></a></td>'
                    tr += '<td class="align-center"><span class="button-group children-tooltip">                                                               '
                    tr += '    <a href="#" class="button" title="trash" onclick="Delete(\'' + arrPakages[i].sid + '\')"><span class="icon-trash"></span></a></span></td>                '
                    tr += '</tr>                                                                                                                               '

                }
                $("#tbl_Package tbody").append(tr);
                //$("#tbl_Package tbody").html(tr);
                $("#tbl_Package").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_Package").removeAttribute("style")
            }
        }
    })
}

function Activate(sid, status, PackageName) {
    var PackageStatus = "Activate";
    if (status == "True") {
        PackageStatus = "Deactivate";
    }
    //var status = status.replace("True", "0").replace("False", "1");
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + PackageStatus + "<br/><span class=\"orange\"> " + PackageName + '</span>?</p>', function () {
        $.ajax({
            url: "../Handler/StaticPakageHandller.asmx/ActiveDeactive",
            type: "post",
            data: '{"sid":"' + sid + '","status":"' + status + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Package status has been changed successfully!")
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)

                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                    GetPackages();
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }, function () {
        $('#modals').remove();
    });
}

function Delete(sid) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + sid + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/StaticPakageHandller.asmx/Delete",
                type: "post",
                data: '{"sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Static Package has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

//********************Add Edit StaticPackage*******************************
function AddPackage() {
    var Packagename = $("#txt_Name").val();
    var Location = $("#txt_Location").val();
    var Price = $("#txt_Price").val();
    var Stay = $("#selStay").val();
    var Rating = $("#selStarRating").val();
    var Currency = $("#selPakageCurrency").val();
    var ValidUpto = $("#txt_ValidUpto").val();
    var bValid = true;
    if (Packagename == "") {
        Success("Please Insert Package Name")
        bValid = false;
        $("#txt_Name").focus()

    }
    else if ($("#txt_Location").val() == "") {
        Success("Please Insert Location")
        bValid = false;
        $("#txt_Location").focus();

    }
    else if ($("#txt_Price").val() == "") {
        Success("Please Insert Price")
        bValid = false;
        $("#txt_Price").focus();
    }
    else if ($("#txt_ValidUpto").val() == "") {
        Success("Please select valid upto")
        bValid = false;
        $("#txt_ValidUpto").focus();
    }
    if (bValid == true) {
        var DATA = {

            Packagename: Packagename,
            Location: Location,
            Price: Price,
            Stay: Stay,
            Rating: Rating,
            Currency: Currency,
            ValidUpto: ValidUpto
        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/StaticPakageHandller.asmx/AddPackages",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#PackageModal").modal("hide")
                    $("#btn_RegiterAgent").val("Add");
                    Success("Package Added Successfully");
                    window.location.reload();
                }

                Clear()
                GetPackages()
            }
        })
    }

}
function LoadPackage(sid, Name, Location, Price, Stay, Rating, Currency, ValidUpto) {


    $.modal({
        content: '<div class="modal-body" id="PackageModal2">' +
        '<div class="columns">' +
'<div class="six-columns twelve-columns-mobile"><label>Package Name:</label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Name2" placeholder="Package Name" value="' + Name + '" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'<div class="six-columns twelve-columns-mobile"><label>Location: </label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Location2" placeholder="Location" value="' + Location + '"  class="input-unstyled full-width"  type="text">' +
'</div></div></div>' +
'<div class="columns">' +
'<div  class="four-columns twelve-columns-mobile"><label>Currency: </label><div class="full-width button-height" id="DivselPakageCurrency">' +
'<select id="selPakageCurrency2" class="select currency">' +
'<option value="USD">USD</option><option value="INR">INR</option><option value="AED">AED</option> </select> ' +
'</div></div>' +
'<div class="four-columns twelve-columns-mobile"><label>Stay: </label><div class="full-width button-height" id="DivselStay">' +
'<select id="selStay2" class="select Stay">' +
'<option  value="1">1 Night</option><option value="2">2 Night</option><option value="3">3 Night</option>' +
'<option value="4">4 Night</option><option value="5">5 Night</option> </select> ' +
'</div></div>' +
'<div class="four-columns twelve-columns-mobile"><label>Star Rating: </label><div class="full-width button-height" id="DivselStarRating">' +
'<select id="selStarRating2" class="select Rating">' +
'<option  value="0">No Star</option><option value="1">1 Star</option><option value="2">2 Star</option>' +
'<option value="3">3 Star</option><option value="4">4 Star</option><option value="5">5 Star</option></select> ' +
'</div></div>' +
'</div><div class="columns">' +
'<div class="six-columns twelve-columns-mobile"><label>Price: </label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Price2" placeholder="Price" value="' + Price + '" class="input-unstyled full-width" type="text">' +
'</div></div>' +
'<div class="six-columns twelve-columns-mobile"><label>Valid Upto: </label> <div class="input full-width">' +
'<input name="datepicker" id="txt_ValidUpto2" placeholder="Valid Upto" value="' + ValidUpto + '" class="input-unstyled" placeholder="dd-mm-yyyy" type="text">' +
'</div></div></div>' +
'<p class="text-alignright"><input type="button" value="Update" onclick="UpdatePackage(\'' + sid + '\');" title="Submit Details" class="button anthracite-gradient"/></p>' +
'</div>',

        title: 'Add Package Details',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });

    AppendCurrency(Currency)
    AppendRating(Rating)
    AppendStay(Stay)
    //********** currency dropd load***************
    //var currDROPDOWN = '<option value="' + Currency + '" >' + Currency + '</option>'
    //$("#selPakageCurrency2").append(currDROPDOWN);
    //$("selPakageCurrency2").val(Currency)
    //$("#DivselPakageCurrency .select span")[0].textContent = Currency;
    //********** Stay dropd load***************
    //var stayDROPDOWN = '<option value="' + Stay + '" >' + Stay + '</option>'
    //$("#selStay2").append(stayDROPDOWN);
    //$("#DivselStay .select span")[0].textContent = Stay;


    //********** Star Rating dropd load***************
    //var starratingDROPDOWN = '<option value="' + Rating + '" >' + Rating + '</option>'
    //$("#selStarRating2").append(starratingDROPDOWN);
    //$("#DivselStarRating .select span")[0].textContent = Rating;

}




function AppendCurrency(Currency) {
    try {
        var checkclass = document.getElementsByClassName('check');
        $("#DivselPakageCurrency .select span")[0].textContent = Currency;
        for (var i = 0; i <= Currency.length; i++) {
            $('input[value="' + Currency + '"][class="currency"]').prop("selected", true);
            $("#selPakageCurrency2").val(Currency);
        }
    }
    catch (ex)
    { }
}

function AppendRating(Rating) {
    try {
        var checkclass = document.getElementsByClassName('check');
        $("#DivselStarRating .select span")[0].textContent = Rating + "Star";
        for (var i = 0; i <= Rating.length; i++) {
            $('input[value="' + Rating + '"][class="Rating"]').prop("selected", true);
            $("#selStarRating2").val(Rating);
        }
    }
    catch (ex)
    { }
}

function AppendStay(Stay) {
    try {
        var checkclass = document.getElementsByClassName('check');
        $("#DivselStay .select span")[0].textContent = Stay;
        for (var i = 0; i <= Stay.length; i++) {
            $('input[value="' + Stay + '"][class="Stay"]').prop("selected", true);
            $("#selStay2").val(Stay);
        }
    }
    catch (ex)
    { }
}

function UpdatePackage(sid) {

    var sid = sid;
    var Packagename = $("#txt_Name2").val();
    var Location = $("#txt_Location2").val();
    var Price = $("#txt_Price2").val();
    var Stay = $("#selStay2 option:selected").val();
    var Rating = $("#selStarRating2 option:selected").val();
    var Currency = $("#selPakageCurrency2 option:selected").val();
    var ValidUpto = $("#txt_ValidUpto2").val();
    var bValid = true;

    if (Packagename == "") {
        Success("Please Insert Package Name")
        bValid = false;
        $("#txt_Name2").focus()

    }
    else if ($("#txt_Location2").val() == "") {
        Success("Please Insert Location")
        bValid = false;
        $("#txt_Location2").focus();

    }
    else if ($("#txt_Price2").val() == "") {
        Success("Please Insert Price")
        bValid = false;
        $("#txt_Price2").focus();
    }
    else if ($("#txt_ValidUpto2").val() == "") {
        Success("Please select valid upto")
        bValid = false;
        $("#txt_ValidUpto2").focus();
    }
    if (bValid == true) {
        var DATA = {

            Packagename: Packagename,
            Location: Location,
            Price: Price,
            Stay: Stay,
            Rating: Rating,
            Currency: Currency,
            ValidUpto: ValidUpto,
            sid: sid
        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/StaticPakageHandller.asmx/UpdatePackages",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#PackageModal2").modal("hide")
                    Success("Package Updated Successfully");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }

                Clear()
                GetPackages()
            }
        })
    }

}
function Clear() {
    $("#txt_Name").val("");
    $("#txt_Location").val("");
    $("#txt_Price").val("");
    $("#selStay").val("0");
    $("#selStarRating").val("0");
    $("#txt_ValidUpto").val("");
    $("#selPakageCurrency").val('USD');
    HiddenId = 0;
}
