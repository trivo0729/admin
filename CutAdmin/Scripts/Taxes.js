﻿$(document).ready(function () {
    GetTaxces();
});
var sid = 0;
var arrTax = new Array();
function AddTax() {
    var Name = $("#txt_Name").val();
    if (Validate()) {
        $.modal.confirm("Are You Sure you want to Save " + Name + " ??", function () {
            var Desciption = $("#txt_Discription").val();
            var Value = $("#txt_Value").val();
            var Active = false;
            if (chk_Active.checked)
                Active = true
            var ParentTaxID = 0;
            var Data = {
                Name: Name,
                Desciption: Desciption,
                Value: Value,
                Active: Active,
                ParentTaxID: ParentTaxID,
                ID: sid
            }
            $.ajax({
                type: "POST",
                url: "../handler/TaxHandler.asmx/SaveTax",
                data: JSON.stringify({ objTax: Data }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        sid = 0;
                        Success("Tax Added Successfully");
                        $("#btn_reset").click()
                        GetTaxces();
                    }
                    else if (result.retCode == 2) {
                        Success(result.ErrorMsg);
                    }
                    else {
                        Success(result.ErrorMsg);
                        setTimeout(function () {
                            window.location.href = "default.aspx", 2000
                        }, 2000);
                    }
                }
            })
        },
        function () {
            $('#modals').remove();
        });
    }
}

function Validate() {
    var bValid = true;
    if ($("#txt_Name").val() == "") {
        $("#lbl_Name").text("*Please insert Tax Name");
        bValid = false;
    }
    else
        $("#lbl_Name").text("*");
    if ($("#txt_Value").val() == "") {
        $("#lbl_Value").text("*Please insert Tax Value");
        bValid = false;
    }
    else
        $("#lbl_Value").text("*");
    return bValid;
}

function GetTaxces() {
    $("#tbl_Tax").dataTable().fnClearTable();
    $("#tbl_Tax").dataTable().fnDestroy();
    $.ajax({
        url: "../handler/TaxHandler.asmx/LoadTax",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrTax = result.arrTax
                for (var i = 0; i < arrTax.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:5%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrTax[i].Name + ' </td>'
                    html += '<td style="width:30%"  align="center">' + arrTax[i].Desciption + ' </td>'
                    html += '<td style="width:5%"  align="center">' + arrTax[i].Value + ' </td>'


                    //html += '<td style="width:20%"  align="center"><span class="button-group icon-size2"><label for="chk_On' + i + '" class="button blue-active ' + arrTax[i].Active.toString().replace("true", "active") + '"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="ActiveTax(\'' + arrTax[i].ID + '\',\'' + 'true' + '\',\'' + arrTax[i].Name + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active ' + arrTax[i].Active.toString().replace("false", "active") + '"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off"  onclick="ActiveTax(\'' + arrTax[i].ID + '\',\'' + 'false' + '\',\'' + arrTax[i].Name + '\')">No</label></span></td>'
                    if (arrTax[i].Active) {
                        html += '<td class="align-center"><input type="checkbox" id="chk' + arrTax[i].ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" checked onclick="ActiveTax(' + arrTax[i].ID + ',\'False\',\'' + arrTax[i].Name + '\')"></td>';
                    }
                    else {
                        html += '<td class="align-center"><input type="checkbox" id="chk' + arrTax[i].ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" onclick="ActiveTax(' + arrTax[i].ID + ',\'True\',\'' + arrTax[i].Name + '\')"></td>';
                    }

                    //html += '<td>'
                    html += '<td style="width:20%" class="align-center">'
                    //html += '<span style="cursor:pointer" class="icon-pencil icon-size2" title="Edit" onclick="UpdateTax(\'' + arrTax[i].ID + '\',\'' + arrTax[i].Name + '\')"></span>'
                    html += '<a href="#" title="Edit" class="button" style="" onclick="UpdateTax(\'' + arrTax[i].ID + '\',\'' + arrTax[i].Name + '\')"><span class="icon-pencil"></span></a>'
                    //html += '| <span  style="cursor:pointer" onclick="Delete(\'' + arrTax[i].ID + '\',\'' + arrTax[i].Name + '\')" class="icon-trash icon-size2" title="Delete"></span>'
                    html += '<a style="display:none;" href="#" class="button" title="Delete" onclick="Delete(\'' + arrTax[i].ID + '\',\'' + arrTax[i].Name + '\')"><span class="icon-trash"></span></a>'
                    html += '</td>'

                    $("#tbl_Tax tbody").append(html);
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                }
                $('[data-toggle="tooltip"]').tooltip()
                $("#tbl_Tax").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                    "width": "104%"
                });
                $("#tbl_Tax").removeAttr("style");
            }
            else {
                Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}

function UpdateTax(ID, Name) {
    sid = ID;
    var TaxDetails = $.grep(arrTax, function (p) { return p.ID == ID })
                                            .map(function (p) { return p; })[0];
    $("#txt_Name").val(TaxDetails.Name)
    $("#txt_Discription").val(TaxDetails.Desciption)
    $("#txt_Value").val(TaxDetails.Value)
    if (TaxDetails.Active)
        $("#chk_Active").prop("checked", true)
    else
        $("#chk_Active").prop("checked", false)
}

function Delete(ID, Name) {
    $.modal.confirm("Are You Sure you want to Delete " + Name + " ??", function () {

        $.ajax({
            type: "POST",
            url: "../handler/TaxHandler.asmx/DeleteTax",
            data: JSON.stringify({ Taxid: ID }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Tax Deleted Successfully");
                    GetTaxces()
                }
                else if (result.retCode == 2) {
                    Success(result.ErrorMsg);
                }
            }
        })
    },
        function () {
            $('#modals').remove();
        })
}

function ActiveTax(ID, Status, Name) {
    $.modal.confirm("Are You Sure you want to  " + Status.replace("True", "Activate").replace("False", "de-activate") + " " + Name + "??", function () {

        var TaxDetails = $.grep(arrTax, function (p) { return p.ID == ID })
                                            .map(function (p) { return p; })[0];

        TaxDetails.Active = Status;
        $.ajax({
            type: "POST",
            url: "../handler/TaxHandler.asmx/SaveTax",
            data: JSON.stringify({ objTax: TaxDetails }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Tax " + Status.replace("True", "Activated").replace("False", "de-activated") + " Successfully");
                    GetTaxces();
                    $('#modals').remove();
                }
            }
        })
    },
        function () {
            $('#modals').remove();
        })
}