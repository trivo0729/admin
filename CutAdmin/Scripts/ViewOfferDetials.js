﻿$(document).ready(function () {
    if (location.href.indexOf('?') != -1) {
        Sid = GetQueryStringParams('sHotelID');
        HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
        GetAllSeasons()
        $('#htlname').val(HotelName);
    }
});
var Sid;
var HotelName;
function GetAllSeasons() {
     $("#tbl_HotelList").dataTable().fnClearTable();
    $("#tbl_HotelList").dataTable().fnDestroy();
    var Divs = '';
  
    var Data = { HotelId: Sid };
    $.ajax({
        type: "POST",
        url: "SearchUtilityHandler.asmx/GetAllSeasons",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var SeasonList = obj.SeasonList;
            $("#offerlistId").empty();
            if (obj.retCode == 1) {
               
                for (var i = 0; i < SeasonList.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td>' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td>' + SeasonList[i].RoomCategory + '</td>'
                    Divs += '<td>' + SeasonList[i].SeasonName + '</td>'
                    Divs += '<td>' + SeasonList[i].ValidFrom + '</td>'
                    Divs += '<td >' + SeasonList[i].ValidTo + '</td>'
                    if (SeasonList[i].MealPlan != null)
                        Divs += '<td>' + SeasonList[i].MealPlan + '</td>'
                    else
                        Divs += '<td> - </td>'
                    if (SeasonList[i].DaysPrior != "" && SeasonList[i].DaysPrior != null)
                        Divs += '<td>' + SeasonList[i].DaysPrior + ' Days</td>'
                    else if (SeasonList[i].BookBefore != "" && SeasonList[i].BookBefore != null)
                        Divs += '<td>' + SeasonList[i].BookBefore + '</td>'
                    else
                        Divs += '<td> - </td>'
                    if (SeasonList[i].OfferType != "" && SeasonList[i].OfferType != null) {
                        if (SeasonList[i].OfferType == "Discount") {
                            if (SeasonList[i].DiscountPer != "0" && SeasonList[i].DiscountPer != null)
                                Divs += '<td>Discount ' + SeasonList[i].DiscountPer + '%</td>'
                            else
                                Divs += '<td>Discount Amount ' + SeasonList[i].DiscountAmount + '</td>'
                        }
                        //'
                    }
                    else
                        Divs += '<td> - </td>'
                    Divs += '<td><i style="Cursor:Pointer" onclick="window.location.href=\'AddOfferRate.aspx?Id=' + SeasonList[i].Offer_Id + '&Sid=' + SeasonList[i].Sid +'&HName='+ HotelName+'\'" title="Edit Offer" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-pencil"></span></label></i></td>'
                    if (SeasonList[i].Active)
                        Divs += '<td><i style="Cursor:Pointer" onclick="ActivateOffer(\'' + SeasonList[i].Sid + '\',\'False\')"  aria-hidden="true" title="Deactivate Offer"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-tick"></span></label></i></td>'
                    else
                        Divs += '<td><i style="Cursor:Pointer" onclick="ActivateOffer(\'' + SeasonList[i].Sid + '\',\'True\')"  aria-hidden="true" title="Activate Offer"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-cross"></span></label></i></td>'
                    Divs += '</tr>'
                    if (i == 0) {
                        $("#HotelName").empty();
                        var Bind = SeasonList[i].HotelName + ", " + SeasonList[i].CityId + ", " + SeasonList[i].CountryId;
                        $("#HotelName").append(Bind);
                       
                    }
                }
            }
            else {
                Divs += '<tr>'
                Divs += '<td colspan="5" align="center">No Record found</td>'
                Divs += '</tr>'
            }
            $("#offerlistId").append(Divs);
            $("#tbl_HotelList").dataTable(
                   {
                       "bLength": false,
                        bSort: false, sPaginationType: 'full_numbers',
                       

                   });
        },
    });
}

function ActivateOffer(OfferId, Status) {
    var Data = { Sid: OfferId, Status: Status };
    $.ajax({
        type: "POST",
        url: "SearchUtilityHandler.asmx/ActivateOffer",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var SeasonList = obj.SeasonList;
            if (obj.retCode == 1) {
                if (Status == "True")
                    Success("Season Activated Successfully");
                else
                    Success("Season Deactivated Successfully");
                GetAllSeasons();
            }
            else {
                Success("Error while Season Activate");
            }
        },
    });
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}