﻿//$(document).ready(function () {
    //debugger;
    var ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('RefNo').replace(/\+/g, '%20');
    //var ref = ReservationID.split(/%27/);
    var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid').replace(/\+/g, '%20');
    //alert(ref[1], Uid)
    //alert("Print ME")
    InvoicePrint(ReservationID, Uid);
//});

function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function Print() {
    window.print();
}
var arrVisaDetails
var arrBookingTransactions;
var arrAgentDetails;
function InvoicePrint(ReservationID, Uid) {
    $.ajax({
        type: "POST",
        url: "../Handler/VisaDetails.asmx/PrintInvoice",
        data: '{"RefNo":"' + ReservationID + '","UID":"' + Uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.roleID == "Admin") {
                var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var Year = now.getFullYear()
                var today = (day) + "-" + (month) + "-" + (Year);
                $("#spnDate").text(today);
                arrVisaDetails = result.VisaDetails;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                $('#spnReference').text(arrVisaDetails[0].Vcode);
                for (var i = 0 ; i < arrAgentDetails.length; i++) {
                    $('#spnName').text(arrAgentDetails[0].AgencyName);
                    $('#spnAdd').text(arrAgentDetails[0].Address);
                    $('#spnCity').text(arrAgentDetails[0].Description);
                    $('#spnCountry').text(arrAgentDetails[0].Countryname);
                    $('#spnPhone').text(arrAgentDetails[0].phone);
                    $('#spnemail').text(arrAgentDetails[0].email);
                    $('#spnAgency').text(arrAgentDetails[0].Agentuniquecode)
                }
                for (var i = 0 ; i < arrVisaDetails.length; i++) {
                    var date = arrVisaDetails[0].AppliedDate
                    var DATE = date.split(/""/);
                    //$('#spnDate').text(DATE[0]);
                    $('#spnServiceNo').text(arrVisaDetails[0].Vcode);
                    //$('#spnDate').text(DATE[0]);
                    $('#spnHotelName').text(arrVisaDetails[0].IeService)
                    $('#spnVoucher').text(arrVisaDetails[0].Vcode);
                    $('#spnChkOut').text(arrVisaDetails[0].DepartingDate);
                    $('#spnChkIn').text(arrVisaDetails[0].ArrivalDate);
                    $('#PassengerName').text(arrVisaDetails[0].FirstName + " " + arrVisaDetails[0].MiddleName + " " + arrVisaDetails[0].LastName);
                    $('#Type').text(arrVisaDetails[0].IeService);
                    $('#spnStatus').text(arrVisaDetails[0].IeService);
                    $('#Ammount').text(Math.round(arrVisaDetails[0].VisaFee));
                    $('#Service').text(arrVisaDetails[0].PassportNo)
                    $('#spnRealAmt').text(Math.round(arrVisaDetails[0].TotalAmount));
                    var NetAmmount = Math.round(arrVisaDetails[0].TotalAmount * 100) / 100//returns 28.5
                    $('#Total').text(Math.round(NetAmmount));
                    $('#UrgenCharges').text(Math.round(arrVisaDetails[0].UrgentFee));
                    $('#OtherCharges').text(OtherFee);

                    $('#ServicesCharges').text(arrVisaDetails[0].ServiceTax);
                    var NewAmmt = Math.round(NetAmmount)
                    //alert(NewAmmt)
                    NumToWord(NewAmmt);
                }

                //}


            }
            if (result.retCode == 1 && result.roleID == "Agent") {
                var now = new Date();
                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);
                var Year = now.getFullYear()
                var today = (day) + "-" + (month) + "-" + (Year);
                $("#spnDate").text(today);
                arrVisaDetails = result.VisaDetails;
                arrBookingTransactions = result.BookingTransactions;
                arrAgentDetails = result.AgentDetails;
                for (var i = 0 ; i < arrAgentDetails.length; i++) {
                    $('#spnName').text(arrVisaDetails[0].FirstName + " " + arrVisaDetails[0].MiddleName + " " + arrVisaDetails[0].LastName);
                    $('#spnAdd').text(arrAgentDetails[0].Address);
                    $('#spnCity').text(arrAgentDetails[0].Description);
                    $('#spnCountry').text(arrAgentDetails[0].Countryname);
                    $('#spnPhone').text(arrAgentDetails[0].phone);
                    $('#spnemail').text(arrAgentDetails[0].email);
                    $('#spnAgentRef').text(arrAgentDetails[0].Agentuniquecode)
                }
                for (var i = 0 ; i < arrVisaDetails.length; i++) {
                    debugger;
                    var date = arrVisaDetails[0].AppliedDate
                    var DATE = date.split(/""/);
                    var TotalAmount = Math.round(result.Markup);
                    var VisaFee = Math.round(TotalAmount * 100) / 100//returns 28.5
                    var AgentMarkup = result.AgentMarkup;
                    //var OtherFee = parseFloat(parseFloat(TotalAmount) - parseFloat(AgentMarkup));
                    var OtherFee = parseFloat(0);
                    $('#spnServiceNo').text(arrVisaDetails[0].Vcode);
                    //$('#spnDate').text(DATE[0]);
                    $('#spnHotelName').text(arrVisaDetails[0].IeService)
                    $('#spnVoucher').text(arrVisaDetails[0].Vcode);
                    $('#spnChkOut').text(arrVisaDetails[0].DepartingDate);
                    $('#spnChkIn').text(arrVisaDetails[0].ArrivalDate);
                    $('#PassengerName').text(arrVisaDetails[0].FirstName + " " + arrVisaDetails[0].MiddleName + " " + arrVisaDetails[0].LastName);
                    $('#Type').text(arrVisaDetails[0].IeService);
                    $('#spnStatus').text(arrVisaDetails[0].IeService);
                    $('#Ammount').text(Math.round(arrVisaDetails[0].VisaFee));
                    $('#Service').text(arrVisaDetails[0].PassportNo)
                    $('#spnRealAmt').text(Math.round(VisaFee) + "/-");
                    var NetAmmount = Math.round(arrVisaDetails[0].TotalAmount * 100) / 100//returns 28.5
                    $('#Total').text(Math.round(NetAmmount));
                    $('#UrgenCharges').text(Math.round(arrVisaDetails[0].UrgentFee));
                    $('#OtherCharges').text(Math.round(OtherFee));

                    $('#ServicesCharges').text(Math.round(arrVisaDetails[0].ServiceTax));
                    NumToWord(Math.round(TotalAmount));
                }

                //}
            }

        },
        error: function () {
            $('#SpnMessege').text('error')
            $('#ModelMessege').modal('show')
            // alert('error')
        }
    });
}

var NUMBER2TEXT = {
    ones: ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'],
    tens: ['', '', 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'],
    sep: ['', ' thousand ', ' million ', ' billion ', ' trillion ', ' quadrillion ', ' quintillion ', ' sextillion ']
};



function onlyNumbers(evt) {
    var e = event || evt; // For trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function NumToWord(inputNumber) {
    var VisaFee = Math.round(inputNumber * 100) / 100//returns 28.5
    //alert(VisaFee)
    var str = new String(VisaFee)
    var splt = str.split("");
    var rev = splt.reverse();
    var once = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
    var twos = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
    var tens = ['', 'Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];

    numLength = rev.length;
    var word = new Array();
    var j = 0;

    for (i = 0; i < numLength; i++) {
        switch (i) {

            case 0:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = 'and ' + once[rev[i]];
                }
                word[j] = word[j];
                break;

            case 1:
                aboveTens();
                break;

            case 2:
                if (rev[i] == 0) {
                    word[j] = '';
                }
                else if ((rev[i - 1] == 0) || (rev[i - 2] == 0)) {
                    word[j] = once[rev[i]] + " Hundred ";
                }
                else {
                    word[j] = once[rev[i]] + " Hundred and";
                }
                break;

            case 3:
                if (rev[i] == 0 || rev[i + 1] == 1) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if ((rev[i + 1] != 0) || (rev[i] > 0)) {
                    word[j] = word[j] + " Thousand";
                }
                break;


            case 4:
                aboveTens();
                break;

            case 5:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if (rev[i + 1] !== '0' || rev[i] > '0') {
                    word[j] = word[j] + " Lakh";
                }

                break;

            case 6:
                aboveTens();
                break;

            case 7:
                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                    word[j] = '';
                }
                else {
                    word[j] = once[rev[i]];
                }
                if (rev[i + 1] !== '0' || rev[i] > '0') {
                    word[j] = word[j] + " Crore";
                }
                break;

            case 8:
                aboveTens();
                break;

                //            This is optional. 

                //            case 9:
                //                if ((rev[i] == 0) || (rev[i + 1] == 1)) {
                //                    word[j] = '';
                //                }
                //                else {
                //                    word[j] = once[rev[i]];
                //                }
                //                if (rev[i + 1] !== '0' || rev[i] > '0') {
                //                    word[j] = word[j] + " Arab";
                //                }
                //                break;

                //            case 10:
                //                aboveTens();
                //                break;

            default: break;
        }
        j++;
    }

    function aboveTens() {
        if (rev[i] == 0) { word[j] = ''; }
        else if (rev[i] == 1) { word[j] = twos[rev[i - 1]]; }
        else { word[j] = tens[rev[i]]; }
    }

    word.reverse();
    var finalOutput = '';
    for (i = 0; i < numLength; i++) {
        finalOutput = finalOutput + word[i];
    }
    document.getElementById('lblAmmountinwords').innerHTML = finalOutput;
}