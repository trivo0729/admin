﻿$(function () {
    GetExchangeMarkup();
    //GetCurrentDate();
})
var arrExchangeLog, arrExchangeMarkup, today1;
var MarkupSid = [];
function GetExchangeMarkup() {
    $("#tbl_log").dataTable().fnClearTable();
    $("#tbl_log").dataTable().fnDestroy();
    debugger;
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/GetExchangeLog",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrExchangeLog = result.sLogs;
                var option = '';
                if (arrExchangeLog.length > 0) {
                    $("#tbl_log tbody").remove();
                    $("#sel_Currency").empty()
                    var Html = '<tbody>';
                    for (i = 0; i < arrExchangeLog.length; i++) {
                        option += '<option value="' + arrExchangeLog[i].MarkupId + '">' + arrExchangeLog[i].Currency + '</option>'
                        Html += '<tr>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].Currency + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].ExchangeRate + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].MarkupRate + '</td>';
                        Html += '<td style="width: 10%;" class="align-center">' + arrExchangeLog[i].TotalExchange + '</td>';
                        //Html += '<td style="width:15%" class="align-center">' + arrExchangeLog[i].UpdateDate + '</td>';
                        Html += '<td style="width:10%" class="align-center">' + arrExchangeLog[i].UpdateBy + '</td>';
                        Html += '<td style="width:10%" class="align-center"><a style="cursor:pointer" class="button" title="Update Exchange" style="cursor:pointer" onclick="FCYEdit(\'' + arrExchangeLog[i].sid + '\',\'' + arrExchangeLog[i].MarkupId + '\',\'' + arrExchangeLog[i].Currency + '\',\'' + arrExchangeLog[i].ExchangeRate + '\',\'' + arrExchangeLog[i].MarkupAmt + '\',\'' + arrExchangeLog[i].MarkupPer + '\',\'' + arrExchangeLog[i].MarkupRate + '\',\'' + arrExchangeLog[i].TotalExchange + '\')"><span class="icon-pencil"></span></a></td>';
                        Html += '</tr>';
                    }
                    Html += '</tbody>';
                    $("#tbl_log").append(Html);
                    $("#sel_Currency").append(option);
                    $("#SpanUdtDate").append(arrExchangeLog[0].UpdateDate);
                    $("#tbl_log").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }
            }
        },
        error: function () {
        }
    });
}

function GetMarkup() {
    $("#FCYEditModal").modal("show")
}

var Currency, ExchangeRate, MarkupAmt, MarkupPer, LogSid;
//function Submit() {
//    Ok("Are You Sure you want to Update Exchange Markup ??", "Update", null)
//}

function Submit() {

    $.modal({
        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to Update</p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Update()">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function Update() {
    MarkupSid = []; Currency = []; ExchangeRate = []; MarkupAmt = []; MarkupPer = []; LogSid = [];
    LogSid.push($("#Hdn_FCY").val())
    MarkupSid.push($("#Hdn_Id").val())
    ExchangeRate.push($("#txt_FCY").val())
    Currency.push($("#sel_Currency option:selected").text())
    MarkupAmt.push($("#txt_SetMarkupAmt").val())
    MarkupPer.push($("#txt_SetMarkupPer").val())
    var data = {
        MarkupSid: MarkupSid,
        LogSid: LogSid,
        Currency: Currency,
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/UpdateExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Markup Updated")
                $("#demo").hide(1000);
                GetExchangeMarkup()
            }
            // Cancel()
        },
        error: function () {
        }
    });
}

function FCYEdit(SID, MarkupId, Curruncy, Rate, MarkupAmt, MarkupPer, MarkupRate, Total) {
    $("#Hdn_FCY").val(SID)
    $("#Hdn_Id").val(MarkupId)
    $("#sel_Currency").val(MarkupId)
    $("#div_Currency .select span")[0].textContent = Curruncy;
    $("#txt_FCY").val(Rate)
    $("#txt_SetMarkupAmt").val(MarkupAmt)
    $("#txt_SetMarkupPer").val(MarkupPer)
    $("#txt_MarkupAmt").val(MarkupRate)
    $("#txt_TotalFCY").val(Total)
    if (MarkupAmt == 0) {
        $("#td_Amt").css("display", "none")
        $("#td_Per").css("display", "")
    }
    else {
        $("#td_Amt").css("display", "")
        $("#td_Per").css("display", "none")
    }
    $("#demo").show(1000);
}
function FCYHide() {
    $("#demo").hide(1000);
}

function GetMarkupAmt(id) {
    ExchangeRate = []; MarkupAmt = []; MarkupPer = [];
    ExchangeRate.push($("#txt_FCY").val())
    if (id == "txt_SetMarkupAmt") {
        MarkupAmt.push($("#txt_SetMarkupAmt").val())
        MarkupPer.push(0)
        $("#txt_SetMarkupPer").val(0)
    }
    else {
        $("#txt_SetMarkupAmt").val(0)
        MarkupAmt.push(0)
        MarkupPer.push($("#txt_SetMarkupPer").val())
    }

    var data = {
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }

    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/GetExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#txt_TotalFCY").val(result.ExchangeRate[0])
                $("#txt_MarkupAmt").val(result.MarkupAmt[0])
            }

        },
        error: function () {
        }
    });
}

function GetCurrencyValue(id) {
    var OnlineRate = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
             .map(function (p) { return p.ExchangeRate; });
    var MarkupAmount = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
           .map(function (p) { return p.MarkupAmt; });
    var MarkupPercent = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.MarkupPer; });
    var sid = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.sid; });
    var Total = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
         .map(function (p) { return p.TotalExchange; });
    var Curruncy = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.Currency; });
    var MarkupAmt = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
          .map(function (p) { return p.MarkupAmt; });
    var MarkupPer = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
        .map(function (p) { return p.MarkupPer; });
    var MarkupRate = $.grep(arrExchangeLog, function (p) { return p.MarkupId == id; })
     .map(function (p) { return p.MarkupRate; });
    FCYEdit(sid, id, Curruncy, OnlineRate, MarkupAmt, MarkupPer, MarkupRate, Total)
}

function GetOnlineRate() {
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/GetOnlineRate",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Online Rates Updated")
                GetExchangeMarkup()
            }

        },
        error: function () {
        }
    });
}

function ShowSearchForm() {
    $("#searchResult").remove();
    $("#currId").val('');
    $("#updtBy").val('');
    var div = "";
    div += '<div id="searchResult" style="display:none" class="col-md-12">'
    div += '<table id="searchRslt" class="table table-bordered" style="margin-bottom:20px">'
    div += '<thead>'
    div += '<tr>'
    div += '<th align="center">Currency</th>'
    div += '<th align="center">Exchange Rate</th>'
    div += '<th align="center">Markup</th>'
    div += '<th align="center">Total</th>'
    div += '<th align="center">Update By</th>'
    div += '<th align="center">Update Time</th>'
    div += '</tr>'
    div += '</thead>'
    div += '<tbody>'
    div += '</tbody>'
    div += '</table>'
    div += '</div>'
    $("#rowId").append(div);
    $("#searchDiv").show(1000);
}

function HideSearchForm() {
    $("#searchDiv").hide(1000);
}

//function GetCurrentDate() {
//    var today = new Date();
//    var dd = today.getDate();
//    var mm = today.getMonth() + 1; //January is 0!

//    var yyyy = today.getFullYear();
//    if (dd < 10) {
//        dd = '0' + dd;
//    }
//    if (mm < 10) {
//        mm = '0' + mm;
//    }
//    today1 = dd + '/' + mm + '/' + yyyy;
//    document.getElementById("dateId").value = today1;
//}

function searchER() {
    var curr = $("#currId").val();
    var updatedBy = $("#updtBy").val();
    var updateDt = $("#dateId").val();
    var updateDate = updateDt.replace(/[/]/g, '-');

    if (curr == "") {
        Success("Please select 'currency'.");
        return false;
    }

    if (updatedBy == "") {
        Success("Please select 'Updated By'.");
        return false;
    }
    if (updateDt == "") {
        Success("Please enter date.");
        return false;
    }

    var data = {
        currency: curr,
        updatedBy: updatedBy,
        updateDate: updateDate
    }

    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/SearchExchangeUpdate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var searchResult = result.exchangeRate;

                if (searchResult.length == 0) {
                    return false;
                }

                var tr = "";
                for (var i = 0; i < searchResult.length; i++) {
                    tr += "<tr><td>" + searchResult[i].Currency + "</td>"
                    tr += "<td>" + searchResult[i].ExchangeRate + "</td>"
                    tr += "<td>" + searchResult[i].MarkupRate + "</td>"
                    tr += "<td>" + searchResult[i].TotalExchange + "</td>"
                    tr += "<td>" + searchResult[i].UpdateBy + "</td>"
                    tr += "<td>" + searchResult[i].UpdateDate.substr(11, 5) + "</td></tr>"
                }
                $("#searchRslt tbody").empty();
                $("#searchRslt tbody").append(tr);
            }
        },
        error: function () {
        }
    });

    $("#searchResult").show(500);
}

//Add exchange rate start here
function ShowAddDiv() {
        $.modal({
            content: '<div class="Exchange" id="demo1">' +
                        '<input type="hidden" id="Hdn_Id1" />' +
                        '<input type="hidden" id="Hdn_FCY1" />' +
                        '<br />' +
                     '<div class="columns">' +
                        '<div class="six-columns twelve-columns-mobile">' +
                        '<label>Currency:</label><div class="full-width button-height" id="div_Currency1">' +
                  '<select id="sel_Currency1" name="validation-select" class="select full-width">' +
                            '<option value="PKR">PKR</option>' +
                            '<option value="USD">USD</option>' +
                            '<option value="GBP">GBP</option>' +
                            '<option value="EUR">EUR</option>' +
                            '<option value="SAR">SAR</option>' +
                            '<option value="AED">AED</option>' +
                  '</select>' +
                            '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Currency1">' +
            '<b>* This field is required</b></label>' +
            '</div>' +
            '</div>' +
            '<div class="six-columns twelve-columns-mobile">' +
            '<label>Online Rate.:</label><div class="input full-width">' +
            '<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>' +
            '<input class="input-unstyled full-width" id="txt_FCY1" type="text" autocomplete="off">' +
            '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_FCY1">' +
            '<b>* This field is required</b></label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="columns">' +
            '<div class="three-columns twelve-columns-mobile">' +
            '<input type="radio" name="radio" id="radio_Amt1" value="" checked="checked" class=" mid-margin-left" onchange="AddMarkup1(this.id)">' +
            '<label for="radio_Amt" class="label">Markup Amount</label>' +
            '</div>' +
            '<div class="three-columns twelve-columns-mobile" style="width:24%; margin-left:1%">' +
            '<input type="radio" name="radio" id="radio_Per1" value="" class=" mid-margin-left" onchange="AddMarkup1(this.id)">' +
            '<label for="radio_Per" class="label">Markup in Percent</label>' +
            '</div>' +
            '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Application1">' +
            '<b>* This field is required</b></label>' +
            '<input type="hidden" id="txt_MarkupAmt1" value="" />' +
            '<div class="three-columns six-columns-mobile" id="div_MrkAmt1">' +
            '<label>Markup Amount:</label><div class="input full-width">' +
            '<input value="" class="input-unstyled full-width" type="text" id="txt_SetMarkupAmt1" onkeyup="GetMarkupAmt1(this.id)">' +
            '</div>' +
            '</div>' +
            '<div class="three-columns six-columns-mobile" style="display:none" id="div_MrkPer1">' +
            '<label>Markup Percent:</label><div class="input full-width">' +
            '<input value="" class="input-unstyled full-width" type="text" id="txt_SetMarkupPer1" onkeyup="GetMarkupAmt1(this.id)">' +
            '</div>' +
            '</div>' +
            '<div class="three-columns six-columns-mobile">' +
            '<label>Total Exchange:</label><div class="input full-width">' +
            '<input value="" class="input-unstyled full-width" readonly="readonly" type="text" id="txt_TotalFCY1">' +
            '</div>' +
            '</div>' +
            '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_SetMarkupPer1">' +
            '<b>* This field is required</b></label>' +
            '</div>' +
            '<p class="text-aligncenter">' +
            '<button type="submit1" class="button anthracite-gradient" onclick="AddExchangeRate()">Add</button>' +
            '</p>' +
            '<div class="clear"></div>' +
            '</div>',

                //    '<div class="columns">' +
                //    '     <div class="twelve-columns-mobile six-columns-tablet">' +
                //    '        <label>Groups  </label>' +
                //    '        <div class="full-width">' +
                //    '            <input name="prompt-value"  id="txt_GroupName"  class="input full-width"  type="text">' +
                //    '        </div></div>' +
                //    '</div>' +

                //    '<div class="standard-tabs margin-bottom" id="add-tabs">' +
                //   '<div class="respTable">' +
                //     '<table class="table responsive-table" id="sorting-advanced">                                                                                                   ' +
                //        ' <thead>                                                                                                                                                    ' +
                //            ' <tr>                                                                                                                                                   ' +
                //             '    <th scope="col">MarkUp Percentage </th>                                                                                                            ' +
                //                ' <th scope="col" class="align-center hide-on-mobile">MarkUp Amount</th>                                                                            ' +
                //                ' <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage </th>                                                           ' +
                //                ' <th scope="col" class="align-center">Commision Amount</th>                                                                                        ' +
                //            ' </tr>                                                                                                                                                  ' +
                //       '  </thead>                                                                                                                                                   ' +
                //        ' <tbody>                                                                                                                                                    ' +
                //        '     <tr>                                                                                                                                                   ' +
                //        '         <td>                                                                                                                                               ' +
                //        '             <div class="input full-width">                                                                                                                 ' +
                //        '                 <input name="prompt-value" id="txt_MarkPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>        ' +
                //        '         </td>                                                                                                                                              ' +
                //        '         <td>                                                                                                                                               ' +
                //        '             <div class="input full-width">                                                                                                                 ' +
                //        '                 <input name="prompt-value" id="txt_MarkUpAmountt" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>            ' +
                //        '         </td>                                                                                                                                              ' +
                //        '         <td>                                                                                                                                               ' +
                //        '             <div class="input full-width">                                                                                                                 ' +
                //        '                 <input name="prompt-value" id="txt_CommPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>     ' +
                //        '         </td>                                                                                                                                              ' +
                //        '         <td>                                                                                                                                               ' +
                //        '             <div class="input full-width">                                                                                                                 ' +
                //        '                 <input name="prompt-value" id="txt_CommAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>         ' +
                //        '         </td>                                                                                                                                              ' +
                //        '     </tr>                                                                                                                                                  ' +
                //        ' </tbody>                                                                                                                                                   ' +
                //        '</table>' +
                //       ' <div style="float:right;margin-top:6%">' +
                //        ' <button type="button" onclick="AddGroupDetails()" class="button anthracite-gradient UpdateMarkup">Add</button></div>' +
                //      '</div>' +
                //      '</div>'
                //,

            title: 'Add Exchange Rate',
            width: 700,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                //'Close': {
                //    classes: 'huge anthracite-gradient glossy full-width',
                //    click: function (win) { win.closeModal(); }
                //}
            },
            buttonsLowPadding: true
        });
}

function FCYHide1() {
    $("#demo1").hide(1000);
}

function AddExchangeRate() {
   
    var  MarkupSid, Currency, ExchangeRate , MarkupAmt , MarkupPer , LogSid ;
   
    MarkupSid = $("#Hdn_Id1").val();
    ExchangeRate = $("#txt_FCY1").val();
    Currency = $("#sel_Currency1 option:selected").text();
    MarkupAmt = $("#txt_SetMarkupAmt1").val();
    MarkupPer = $("#txt_SetMarkupPer1").val();
   
    var data = {
        MarkupSid: MarkupSid,
        Currency: Currency,
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }

    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/AddExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Markup added.")
                $("#demo").hide(1000);
                GetExchangeMarkup();
            }
            else if (result.retCode == 0) {
                alert("Something went wrong.");
            }
            // Cancel()
        },
        error: function () {
        }
    });
}

function AddMarkup1(id) {
    if (id != "radio_Amt1") {
        $("#div_MrkAmt1").css("display", "none")
        $("#div_MrkPer1").css("display", "")
    }
    else {
        $("#div_MrkPer1").css("display", "none")
        $("#div_MrkAmt1").css("display", "")
    }
}

function GetMarkupAmt1(id) {
    ExchangeRate = []; MarkupAmt = []; MarkupPer = [];
    ExchangeRate.push($("#txt_FCY1").val())
    if (id == "txt_SetMarkupAmt1") {
        MarkupAmt.push($("#txt_SetMarkupAmt1").val())
        MarkupPer.push(0)
        $("#txt_SetMarkupPer1").val(0)
    }
    else {
        $("#txt_SetMarkupAmt1").val(0)
        MarkupAmt.push(0)
        MarkupPer.push($("#txt_SetMarkupPer1").val())
    }

    var data = {
        ExchangeRate: ExchangeRate,
        MarkupAmt: MarkupAmt,
        MarkupPer: MarkupPer
    }

    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/ExchangeRateHandler.asmx/GetExchangeRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#txt_TotalFCY1").val(result.ExchangeRate[0])
                $("#txt_MarkupAmt1").val(result.MarkupAmt[0])
            }
        },
        error: function () {
        }
    });
}

function Submit1() {
    $.modal({
        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to Add exchange rate?</p>' +
'<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="AddExchangeRate()">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}