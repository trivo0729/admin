﻿/// <reference path="E:\Azhar\CutAdmin\CutAdmin\CutAdmin\Dashboard.aspx" />

function LoginLoad() {

    var UserName = localStorage.userName;
    var Password = localStorage.password;

    if (UserName != undefined || UserName != undefined) {
        document.getElementById("txt_UserId").value = localStorage.userName;
        document.getElementById("txt_UserPassword").value = localStorage.password;
    }
};

function LoginAdmin() {
    debugger;

    sUserName = document.getElementById("txt_UserId").value;
    sPassword = document.getElementById("txt_UserPassword").value;
    var bValid = true;

    if ($('#remember').is(':checked')) {
        // save username and password
        localStorage.userName = sUserName;
        localStorage.password = sPassword;
        localStorage.checkBoxValidation = $('#remember').val();
    } else {
        localStorage.userName = '';
        localStorage.password = '';
        localStorage.checkBoxValidation = '';
    }

    if (sUserName == "") {
        bValid = false;
        Success("User Name/email cannot be blank");
        document.getElementById('txt_UserId').focus();
        return false;
    }
    else if (!emailReg.test(sUserName)) {
        bValid = false;
        Success("The email address you have entered is invalid");
        document.getElementById('txt_UserId').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        Success("Password cannot be blank");
        document.getElementById('txt_UserPassword').focus();
        // $("#txt_UserPassword").focus();
        return false;
    }
    if (bValid == true) {
        //var Data = {
        //    sUserName: sUserName,
        //    sPassword: sPassword
        //}
        $.ajax({
            type: "POST",
            url: "DefaultHandler.asmx/UserLogin",
            data: '{"sUserName":"' + sUserName + '","sPassword":"' + sPassword + '"}',
            //data:JSON.stringify(Data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                    //window.location.href = "Dashboard.aspx";

                    return false;
                }
                else if (result.retCode == 1) {
                    if (result.roleID == "Admin")
                        window.location.href = "HotelAdmin/Dashboard.aspx";
                    else if (result.roleID == "Franchisee") {
                        window.location.href = "DashBoard.aspx";
                    }
                    else if (result.roleID == "Supplier") {
                        window.location.href = "DashBoard.aspx";
                    }
                    else if (result.roleID == "Agent") {

                        if (result.IsFirstLogIn == "True") {
                            window.location.href = "Agent/AgentDashboard.aspx";
                        }
                        else {

                            window.location.href = "Agent/AgentDashboard.aspx";
                        }
                    }

                    else if (result.roleID == "AdminStaff")
                        window.location.href = "HotelAdmin/Dashboard.aspx";
                    else if (result.roleID == "AgentStaff")
                        window.location.href = "Agent/SearchHotel.aspx";
                    else if (result.roleID == "FranchiseeStaff")
                        window.location.href = "Franchisee/DashBoard.aspx";
                    else if (result.roleID == "Inactive") {
                        Success('Your account is currently inactive, please wait for activation or contact administration.');
                    }
                    else if (result.roleID == "AddAgent") {
                        Success("Oops!! looks like you haven't loggin since long. Update your profile to Sign-In.")
                        setTimeout(function () {
                            window.location.href = "../Registration.aspx?uid=" + sUserName;
                        }, 2000);
               
                    }
                    return false;
                }
                else {
                    // Success('Username/Password did not match.');
                    Success("Invalid ID or Paasword");
                }
            },
            error: function () {
            }
        });


    }
}


function f3() {
    var Userid;
    var Pwd;
    var type;
    if (document.getElementById('uid3').value == "") {
        Success("User Name/email cannot be blank");
        document.getElementById('uid3').focus();
        return false;
    }
    else if (!emailReg.test(document.getElementById('uid3').value)) {
        Success("The email address you have entered is invalid");
        document.getElementById('uid3').focus();
        return false;
    }
    if (document.getElementById('pwd3').value == "") {
        Success("Password cannot be blank");
        document.getElementById('pwd3').focus();
        return false;
    }
    Userid = document.getElementById('uid3').value;
    Pwd = escape(document.getElementById('pwd3').value);
    type = "Transporter"
    window.location.href = "LoginValidate.aspx?Userid=" + Userid + "&Pwd=" + Pwd + "&type=" + type + "";
    //        window.location.href = "../CUT/LoginValidate.aspx?Userid=" + Userid + "&Pwd=" + Pwd + "&type=" + type + "";
}
//................Validation.......................//
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

