﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StaffDetails.aspx.cs" Inherits="CutAdmin.StaffDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/StaffDetails.js?v=1.23"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Staff Details</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="columns">
                <div class="nine-columns text-alignright">
                </div>
                <div class="two-columns text-alignright">
                    <a href="AddStaff.aspx" class="button anthracite-gradient" style="float:right"><span class="icon-user"></span>Add New</a>
                </div>
                <div class="one -columns">
                    <a onclick="ExportStaffDetailsToExcel()">
                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35">
                    </a>
                </div>
            </div>
            <div class="respTable">
                <table class="table responsive-table" id="tbl_StaffDetails">
                    <thead>
                        <tr>
                            <th scope="col">Staff Detail</th>
                            <th scope="col" class="align-center hide-on-mobile">Email | Password Manage </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Unique Code </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">IsActive</th>
                            <th scope="col" width="150" class="align-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>

</asp:Content>
