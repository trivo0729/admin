﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StaffProfile.aspx.cs" Inherits="CutAdmin.StaffProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/StaffProfile.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Update Profile</h1>
        </hgroup>
        <div class="with-padding">
            <hr>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>First Name<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input class="input-unstyled full-width" placeholder="First Name" type="text" id="txtFirstName">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Last Name<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Last Name" type="text" id="txtLastName">
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Designation<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Designation" type="text" id="txtDesignation">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Department<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Department" type="text" id="txtDepartment">
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Email<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" readonly="readonly" style="cursor:no-drop" placeholder="Email" type="text" id="txtEmail">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Mobile<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" placeholder="Mobile" type="text" id="txtMobileNumber">
                    </div>
                </div>
            </div>
            <p class="text-right">
                <input type="button" class="button anthracite-gradient" id="btnAddStaff" onclick="AddStaff()" value="Update" title="Submit Details">
                <a class="button anthracite-gradient" href="DashBoard.aspx">Back</a>
            </p>
        </div>
    </section>
</asp:Content>
