﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Suppliers.aspx.cs" Inherits="CutAdmin.Suppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ApiDetails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Suppliers</h1>
            <h2><a href="AddHotelSupplier.aspx" class="button compact anthracite-gradient"><span class="icon-user"></span>&nbsp; New Supplier</a></h2>
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_MasterSupplier">

                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Contact Person</th>
                            <th scope="col">Contact No</th>
                            <th scope="col">Hotel</th>
                            <th scope="col">Active</th>
                            <th scope="col" width="45">Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        <%--<tr>
                            <td>1</td>
                            <td>TBO</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>GTA</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Activity Supplier</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>From Hotel</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>D Travel</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>COZMO</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Adventures</td>
                            <td></td>
                            <td></td>
                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="align-center"><a href="AddHotelSupplier.html" class="button compact"><span class="icon-pencil"></span></a></td>
                        </tr>--%>

                    </tbody>

                </table>
            </div>
        </div>

    </section>
</asp:Content>
