﻿using System;
using System.Web;using CutAdmin.dbml;
using System.IO;
using System.Collections.Generic;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for UploadFacilityHandler
    /// </summary>
    public class UploadFacilityHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string fname = "";
            string Ext = "";
            string RandomNo = "";

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                RandomNo = context.Request.QueryString["RandomNo"];
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myPath = file.FileName;
                    string MyFileName = file.FileName;
                    Ext = Path.GetExtension(MyFileName);
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;

                        if (RandomNo == null)
                        {
                            RandomNo = context.Session["RandomNo"].ToString();
                            fname = context.Session["RandomNo"].ToString() + Ext;
                        }
                        else
                            fname = RandomNo + Ext;

                    }


                    fname = Path.Combine(context.Server.MapPath("uploads/FacilityIcon"), fname);
                    file.SaveAs(fname);
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write("File Uploaded Successfully!" + RandomNo + Ext);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}