﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewInvoice.aspx.cs" Inherits="CutAdmin.ViewInvoice" %>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>View Invoice</title>
    <style type="text/css">
        @page {
            size: A3 portrait;
            margin: 0.5cm;
        }

        @media print {
            .page {
                page-break-after: avoid;
            }
        }
    </style>
    <!-- Picker -->
    <%--<link href="css/css/font-awesome.min.css" rel="stylesheet" />--%>
    <script src="Scripts/Alert.js"></script>
     <script type="text/javascript" src="scripts/Invoice.js"></script>
     <%--<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>--%>
  
         
    
    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="css/2x.css?v=1">

    <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1">

    <!-- jQuery Form Validation -->
    <%--	<link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">--%>

    <!-- Additional styles -->
   
    <link rel="stylesheet" href="css/styles/modal.css?v=1">
   

    <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>
     <link href="css/custom.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
   
    <!-- iOS web-app metas -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- iPhone ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png" sizes="57x57">
    <!-- iPad ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad.png" sizes="72x72">
    <!-- iPhone (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-retina.png" sizes="114x114">
    <!-- iPad (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad-retina.png" sizes="144x144">

    <!-- iPhone SPLASHSCREEN (320x460) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="(device-width: 320px)">
    <!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone-retina.png" media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPhone 5 SPLASHSCREEN (640×1096) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone5.png" media="(device-height: 568px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="(device-width: 768px) and (orientation: portrait)">
    <!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="(device-width: 768px) and (orientation: landscape)">
    <!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait-retina.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)">
    <!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape-retina.png" media="(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)">
     
    <link rel="stylesheet" href="css/styles/table.css?v=1">
   
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/Invoice.js"></script>
    <script type="text/javascript">
        var ReservationID;
        var Status;
        $(document).ready(function () {
            Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
            if (Status == "Cancelled") {
                document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
            }
            ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
            var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid');

            //$("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));
            //InvoicePrint(ReservationID, Uid);
            document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationModel('" + ReservationID + "', '" + Status + "')");
            document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFInvoice('" + ReservationID + "', '" + Uid + "','" + Status + "')");
           
        });


        function ProceedToCancellation() {
            var Supplier = GetQueryStringParamsForAgentRegistrationUpdate('Supplier')
            debugger;
            if ($("#btnCancelBooking").val() == "Cancel Booking") {
                if ($("#hndIsCancelable").val() == "0") {

                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("Sorry! You cannot cancel this booking.")
                    $('#ModelMessege').modal('show')


                    // alert("Sorry! You cannot cancel this booking.")
                }
                else {
                    var data = {
                        ReservationID: $("#hndReservatonID").val(),
                        ReferenceCode: $("#hndReferenceCode").val(),
                        CancellationAmount: $("#hndCancellationAmount").val(),
                        BookingStatus: $("#hndStatus").val(),
                        Remark: $("#txtRemark").val(),
                        TotalFare: $("#hdn_TotalFare").val(),
                        ServiceCharge: $("#hdn_ServiceCharge").val(),
                        Total: $("#hdn_Total").val(),
                        Type: Supplier
                    }
                    $("#dlgLoader").css("display", "initial");
                    $.ajax({
                        type: "POST",
                        url: "../HotelHandler.asmx/HotelCancelBooking",
                        data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                alert("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
                                //document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
                                $("#AgencyBookingCancelModal").modal('hide');
                                setTimeout(function () { window.location.href = "ViewInvoice.aspx?datatable=ReservationId=" + ReservationID + "&Uid=" + Uid + "&Status=Cancelled&Supplier=" + Supplier; }, 2000);
                                //location.reload();
                                //$.ajax({
                                //    type: "POST",
                                //    url: "../EmailHandler.asmx/CancellationEmail",
                                //    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                                //    contentType: "application/json; charset=utf-8",
                                //    datatype: "json",
                                //    success: function (response) {
                                //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                //        if (result.Session == 0) {
                                //            alert("Some error occured, Please try again in some time.");

                                //        }
                                //        if (result.retCode == 1) {
                                //            //alert("Email has been sent successfully.");


                                //        }
                                //        else if (result.retCode == 0) {
                                //            alert("Sorry Please Try Later.");


                                //        }

                                //    },
                                //    error: function () {
                                //        alert('Something Went Wrong');
                                //    }

                                //});

                                ///// email ends here...


                            }
                            else if (result.retCode == 0) {
                                $('#AgencyBookingCancelModal').modal('hide')
                                $('#SpnMessege').text(result.Message);
                                $('#ModelMessege').modal('show')

                                //alert(result.Message);


                            }
                        },
                        error: function () {
                            $('#AgencyBookingCancelModal').modal('hide')
                            $('#SpnMessege').text('Something Went Wrong');
                            $('#ModelMessege').modal('show')
                            // alert("something went wrong");
                        },
                        complete: function () {
                            $("#dlgLoader").css("display", "none");
                        }
                    });
                }
            } else if ($("#btnCancelBooking").val() == "Confirm Booking") {
                debugger;
                if ($("#hndIsConfirmable").val() == "0") {
                    $('#AgencyBookingCancelModal').modal('hide')
                    $('#SpnMessege').text("Sorry! You cannot confirm this booking.")
                    $('#ModelMessege').modal('show')
                    // alert("Sorry! You cannot confirm this booking.")
                }
                else {
                    var data = {
                        ReservationID: $("#hndReservatonID").val()
                    }
                    $("#dlgLoader").css("display", "initial");
                    $.ajax({
                        type: "POST",
                        url: "../HotelHandler.asmx/ConfirmHoldBooking",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                $('#SpnMessege').text("Your booking has been confirmed.Confirmation email has been sent.!");
                                $('#ModelMessege').modal('show')
                                //  alert("Your booking has been confirmed.Confirmation email has been sent.!");
                                $("#AgencyBookingCancelModal").modal('hide');
                                location.reload();

                                //here mail goes for hold relaese
                                debugger;
                                $.ajax({
                                    type: "POST",
                                    url: "../EmailHandler.asmx/HoldReleaseEmail",
                                    data: '{"ReservationID":"' + ReserID + '","totalamount":"' + totalamount + '","checkin":"' + checkin + '","checkout":"' + checkout + '","bookingdate":"' + bookingdate + '","HotelName":"' + HotelName + '","PassengerName":"' + PassengerName + '","City":"' + City + '"}',
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (response) {
                                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                        if (result.Session == 0) {
                                            $('#SpnMessege').text("Some error occured, Please try again in some time.");
                                            $('#ModelMessege').modal('show')
                                            // alert("Some error occured, Please try again in some time.");

                                        }
                                        if (result.retCode == 1) {
                                            //alert("Email has been sent successfully.");


                                        }
                                        else if (result.retCode == 0) {
                                            $('#AgencyBookingCancelModal').modal('hide')
                                            $('#SpnMessege').text("Sorry Please Try Later.");
                                            $('#ModelMessege').modal('show')
                                            // alert("Sorry Please Try Later.");


                                        }

                                    },
                                    error: function () {
                                        $('#AgencyBookingCancelModal').modal('hide')
                                        $('#SpnMessege').text('Something Went Wrong');
                                        $('#ModelMessege').modal('show')
                                        //alert('Something Went Wrong');
                                    }

                                });
                                // Mail Ends Here....
                            }
                            else if (result.retCode == 0) {
                                $('#AgencyBookingCancelModal').modal('hide')
                                $('#SpnMessege').text(result.Message);
                                //   $('#ModelMessege').modal('show')
                                alert(result.Message);
                            }

                            else if (result.retCode == 2) {
                                //alert(result.Message);
                                $('#AgencyBookingCancelModal').modal('hide')
                                $("#dspAlertMessage").html("Your Balance is Insufficient to confirm this booking.");
                                $("#dspAlertMessage").css("display", "block");
                                //$("#AgencyBookingCancelModal").modal('hide');
                            }
                        },
                        error: function () {
                            $('#AgencyBookingCancelModal').modal('hide')
                            $('#SpnMessege').text("something went wrong during hold confirmation");
                            $('#ModelMessege').modal('show')
                            // alert("something went wrong during hold confirmation");
                        },
                        complete: function () {
                            $("#dlgLoader").css("display", "none");
                        }
                    });
                }
            }
        }

        function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }


    </script>
</head>

<body>

    <div style="margin-left: 35%;" id="BtnDiv">
        <input type="button" class="button anthracite-gradient" value="Email Invoice" style="cursor: pointer" title="Invoice" data-toggle="modal" data-target="#InvoiceModal" onclick="InvoiceMailModal()"/>
        <input type="button" class="button anthracite-gradient" value="Download" id="btn_VoucherPDF"/>
       <input type="button" class="button anthracite-gradient" value="Cancel Booking" id="btn_Cancel" />
    </div>
    <br />
    <div id="maincontainer" runat="server" class="print-portrait-a4" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray;">
        <%--        <div style="background-color: #F7B85B; height: 13px">
            <span>&nbsp;</span>
        </div>
        <div>
            <table style="height: 90px; width: 100%;">
                <tbody>
                    <tr>
                        <td style="width: 40%; padding-left: 15px">
                            <img src="../images/logo.png" alt="" style="width: 100%; height: auto;"></img>
                        </td>

                        <td style="width: 60%; padding-right: 25px; color: #57585A" align="right">
                            <span style="margin-right: 15px">
                                <br>
                                2nd Floor, Vishnu Complex, CA Road,<br>
                                Juni Mangalwari, Opp. Rahate Hospital,<br>
                                Nagpur - 440008, Maharashtra, INDIA<br>
                                <b>Tel:</b> +91-712-6660666
                                <br>
                                <b>Fax:</b>+91-712-2766520<br>
                                <b>Email:</b> info@clickurtrip.com<br>
                                accounts@clickurtrip.com<br>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #57585A"><span><b>Service Tax Reg No:</b> AADPZ22639MST001</span></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <table border="1" style="width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">
                <tr>
                    <td rowspan="3" style="width: 35%; color: #00CCFF; font-size: 30px; text-align: center; border: none; padding-right: 35px; padding-left: 10px">
                        <b>SALES INVOICE</b><br>
                        <span style="font-size: 30px"><b>Status : </b></span><span id="spnStatus" style="font-size: 30px; font-weight: 700">Booked</span>
                    </td>
                    <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 0px 10px; margin-bottom: 0px; color: #57585A">
                        <b>Invoice Date &nbsp;    : &nbsp;</b><span id="spnDate" style="font-weight: 500"> &nbsp; </span>
                    </td>
                    <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 10px 0px 3px 12px; margin-bottom: 0px; color: #57585A;">
                        <b>Invoice No  &nbsp;     : </b>&nbsp;<span id="spnInvoice" style="font-weight: 500"> </span>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 0px 10px; color: #57585A">
                        <b>Voucher No. &nbsp; :</b> <span id="spnVoucher" style="font-weight: 500">&nbsp;AGT-123456 </span>
                    </td>
                    <td style="border-top: none; border-bottom-width: 6px; border-bottom-color: gray; padding: 0px 0px 3px 10px; color: #57585A">
                        <b>Agent Code&nbsp; : &nbsp;</b><span id="spnAgentRef" style="font-weight: 500"> </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 25px; border: none"></td>
                </tr>
            </table>
        </div>

        <div>
            <table border="1" style="border-spacing: 0px; height: 150px; width: 100%; border-top: none; padding: 10px 0px 10px 0px; border-width: 0px 0px 3px 0px; border-bottom-color: #E6DCDC">
                <tr>
                    <td colspan="2" style="border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A"><b>Invoice To</b></td>
                    <td colspan="3" style="border: none; padding-bottom: 10px; padding-left: 10px; font-size: 20px; color: #57585A"><b>Service Details</b></td>
                </tr>
                <tr>
                    <td rowspan="6" style="width: 10%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; color: #57585A; font-weight: 900">
                        <span style="padding-left: 10px">Name</span><br>
                        <span style="padding-left: 10px">Address</span><br>
                        <span style="padding-left: 10px">City</span><br>
                        <span style="padding-left: 10px">Country</span><br>
                        <span style="padding-left: 10px">Phone</span><br>
                        <span style="padding-left: 10px">Email</span><br>
                    </td>
                    <td rowspan="6" style="width: 30%; border-width: 3px 0px 0px 0px; border-bottom-color: gray; padding-left: 10px; color: #57585A; font-weight: 500">:<span id="spnName" style="padding-left: 10px"></span><br>
                        :<span id="spnAdd" style="padding-left: 10px"></span><br>
                        :<span id="spnCity" style="padding-left: 10px"></span><br>
                        :<span id="spnCountry" style="padding-left: 10px"></span><br>
                        :<span id="spnPhone" style="padding-left: 10px"></span><br>
                        :<span id="spnemail" style="padding-left: 10px"></span><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border-width: 3px 0px 0px 0px; border-bottom-color: gray; border-spacing: 0px; background-color: #35C2F1; padding-left: 8px"><span style="color: white; font-size: 18px"><b>Hotel Name  :</b></span> <span id="spnHotelName" style="color: white; font-size: 18px"></span></td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none; background-color: #27B4E8; padding-left: 8px"><span style="color: white; font-size: 18px"><b>Destination :</b> <span id="spnHotelDestination" style="color: white; font-size: 18px">Mumbai</span></td>
                </tr>
                <tr style="color: white;">
                    <td style="width: 150px; border: none; background-color: #00AEEF; padding-bottom: 3px" align="center">
                        <span><b>Check In</b></span><br>
                        <span id="spnChkIn">DD-MMM-YYYY</span>
                    </td>
                    <td style="width: 150px; border: none; background-color: #00AEEF; padding-bottom: 3px" align="center">
                        <span><b>Check Out</b></span><br>
                        <span id="spnChkOut">DD-MMM-YYYY</span>
                    </td>
                    <td style="width: 150px; border: none; background-color: #A8A9AD; padding-bottom: 3px" align="center">
                        <span><b>Total Night(s)</b></span><br>
                        <span id="spnNights">02 Night(s)</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="border: none">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A"><b>Rate</b></span>
        </div>
        <div>
            <table id="tbl_Rate" border="1" style="margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none">
                <tr style="border: none;">
                    <td style="background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left; padding-left: 10px">
                        <span>No.</span>
                    </td>
                    <td style="width: 170px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">
                        <span>Room Type</span>
                    </td>
                    <td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center">
                        <span>Board</span>
                    </td>
                    <td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center">
                        <span>Rooms</span>
                    </td>
                    <td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">
                        <span>Nights</span>
                    </td>
                    <td style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">
                        <span>Rate</span>
                    </td>
                    <td style="width: 100px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: center;">
                        <span>Total</span>
                    </td>
                </tr>

            </table>
        </div>

        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A"><b>Cancellation Charge</b></span>
        </div>
        <div>
            <table id="tbl_Cancellation" border="1" style="margin-top: 3px; width: 100%; border-spacing: 0px; border-width: 3px 0px 3px 0px; border-top-color: gray; border-bottom-color: #E6DCDC">
                <tr style="border: none">
                    <td style="background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left; padding-left: 10px">
                        <span>No.</span>
                    </td>
                    <td style="width: 190px; height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left">
                        <span>Room Type</span>
                    </td>
                    <td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;">
                        <span>Rooms</span>
                    </td>
                    <td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;">
                        <span>Cancellation After</span>
                    </td>
                    <td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;">
                        <span>Charge/Unit</span>
                    </td>
                    <td align="center" style="height: 35px; background-color: #35C2F1; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;">
                        <span>Total Charge</span>
                    </td>
                </tr>

            </table>
        </div>

        <div style="font-size: 20px; padding-bottom: 10px; padding-top: 8px">
            <span style="padding-left: 10px; color: #57585A"><b>Bank Details</b></span>
        </div>
        <div>
            <table border="1" style="margin-top: 3px; height: 100px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-left: none; border-right: none">
                <tr style="border: none; background-color: #F7B85B; border-bottom-color: gray; color: #57585A;">
                    <td align="left" style="width: 15%; height: 35px; font-size: 0.875em; border: none; padding: 10px 0px 10px 10px; border-bottom: 3px; border-bottom-color: gray;">
                        <span>
                            <b>Bank Name </b>
                            <br>
                            <b>Account No </b>
                            <br>
                            <b>Branch   </b>
                            <br>
                            <b>Swift Code   </b>
                            <br>
                        </span>
                    </td>
                    <td align="left" style="width: 17%; height: 35px; font-size: 0.875em; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray; font-weight: 500">: &nbsp;&nbsp;<span>ICICI Bank</span><br>
                        : &nbsp;&nbsp;<span>023105500640</span><br>
                        : &nbsp;&nbsp;<span>Apmc Kalamna, Nagpur</span><br>
                        : &nbsp;&nbsp;<span>ICIC0000231</span><br>
                    </td>
                    <td align="left" style="width: 15%; height: 35px; font-size: 0.875em; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray;">
                        <span>
                            <b>Bank Name </b>
                            <br>
                            <b>Account No </b>
                            <br>
                            <b>Branch   </b>
                            <br>
                            <b>Swift Code </b>
                            <br>
                        </span>
                    </td>
                    <td align="left" style="width: 17%; height: 35px; font-size: 0.875em; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray; font-weight: 500">: &nbsp;&nbsp;<span>AXIS Bank</span><br>
                        : &nbsp;&nbsp;<span>914020021370944</span><br>
                        : &nbsp;&nbsp;<span>Lakadganj, Nagpur</span><br>
                        : &nbsp;&nbsp;<span>UTIB0000330</span><br>
                    </td>
                    <td align="left" style="width: 15%; height: 35px; font-size: 0.875em; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray;">
                        <span>
                            <b>Bank Name </b>
                            <br>
                            <b>Account No </b>
                            <br>
                            <b>Branch </b>
                            <br>
                            <b>Swift Code </b>
                            <br>
                        </span>
                    </td>
                    <td align="left" style="width: 17%; height: 35px; font-size: 0.875em; border: none; padding-left: 10px; border-bottom: 3px; border-bottom-color: gray; font-weight: 500">: &nbsp;&nbsp;<span>Bank Of India</span><br>
                        : &nbsp;&nbsp;<span>870120110000456</span><br>
                        : &nbsp;&nbsp;<span>Itwari, Nagpur</span><br>
                        : &nbsp;&nbsp;<span>BKID0008701</span><br>
                    </td>
                </tr>
            </table>
            <table border="1" style="height: 100px; width: 100%; border-spacing: 0px; border-bottom: none; border-left: none; border-right: none">
                <tr style="font-size: 20px; border-spacing: 0px">
                    <td colspan="5" height="20px" style="width: 70%; background-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A"><b>Terms & Conditions</b></td>
                    <td rowspan="2" style="border-bottom: none; border-left: none; text-align: center">
                        <img src="../images/signature.png" alt="" height="auto" width="auto"></img>
                    </td>
                </tr>
                <tr style="font-size: 15px">
                    <td colspan="5" style="background-color: #E6E7E9; border-top-width: 3px; border-top-color: #E6E7E9; padding: 10px 10px 10px 10px; color: #57585A;">
                        <ul style="list-style-type: disc">
                            <li>Kindly check all details carefully to avoid un-necessary complications</li>
                            <li>Cheque to be drawn in our company name on presentation of invoice</li>
                            <li>Subject to NAGPUR (INDIA) jurisdiction </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div style="background-color: #00AEEF; text-align: center; font-size: 21px; color: white">
            <span>Computer generated invoice do not require signature...

            </span>
        </div>--%>
    </div>

    <%--<div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>--%>

   <%-- <div class="modal fade bs-example-modal-lg" id="AgencyBookingCancelModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header" style="border-bottom: 1px solid #fff">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Booking Details</b></div>
                    <div class="frow2">
                        <table>
                            <tr>
                                <td><span class="dark bold">Hotel: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspHotelName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-In: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckin"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Check-Out: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspCheckout"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Passenger: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspPasssengerName"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Location: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspLocation"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Nights: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspNights"></span></td>
                            </tr>
                            <tr>
                                <td><span class="dark bold">Booking ID: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingID"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Booking Date: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingDate"></span></td>
                                <td style="padding-left: 20px;"><span class="dark bold">Amount: </span></td>
                                <td style="padding-left: 15px;"><span class="dark" id="dspBookingAmount"></span></td>
                            </tr>
                        </table>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Cancellation Policy</b></div>
                    <div class="frow2">
                        <div id="dspCancellationPolicy">
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    </div>

                    <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;" id="divRemark1"><b>Remark</b></div>
                    <div class="frow2" id="divRemark2">
                        <textarea rows="4" cols="140" id="txtRemark" class="form-control"></textarea>
                        <div class="clearfix"></div>
                        <br>
                    </div>
                    <div class="alert alert-warning" id="dspAlertMessage" style="display: none"></div>


                    <div class="frow2" style="text-align: center;">
                        <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br />
                        <input type="button" value="Cancel Booking" onclick="ProceedToCancellation(); return false" id="btnCancelBooking" class="btn-search4 margtop20" style="float: none;" />
                        <input type="button" value="Cancel Hold Booking" onclick="ProceedToCancelHoldBooking();" id="btnCancelHoldBooking" class="btn-search4 margtop20" style="float: none;" />
                        <input type="hidden" id="hndReferenceCode" />
                        <input type="hidden" id="hndCancellationAmount" />
                        <input type="hidden" id="hndReservatonID" />
                        <input type="hidden" id="hndIsCancelable" />
                        <input type="hidden" id="hndStatus" />
                        <input type="hidden" id="hndIsConfirmable" />
                        <input type="hidden" id="hdn_TotalFare" />
                        <input type="hidden" id="hdn_ServiceCharge" />
                        <input type="hidden" id="hdn_Total" />
                        <input type="hidden" id="hdn_Source" />
                        <input type="hidden" id="hdn_Supplier" />
                        <input type="hidden" id="hdn_AffiliateCode" />
                    </div>



                </div>

            </div>
        </div>
    </div>--%>

    <%--<div class="modal fade bs-example-modal-lg" id="InvoiceModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 60%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover table-responsive" id="tbl_SendInvoice" style="width: 100%">
                        <tr>
                            <td style="border: none;">
                                <span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;
                                       
                                <input type="text" id="txt_sendInvoice" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 70%;" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_sendInvoice"><b>* This field is required</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none;">
                                <input id="btn_sendInvoice" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailInvoice();" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>--%>

      <link href="css/jquery-ui.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/colors.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui.js"></script>
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>

    <!-- jQuery Form Validation -->
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <script src="js/developr.modal.js"></script>
  
</body>
</html>