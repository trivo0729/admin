﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using CutAdmin.dbml;
namespace CutAdmin
{
    public class VoucherManager
    {

        static helperDataContext db = new helperDataContext();
        public static string GenrateVoucher(string ReservationID, string Uid, string Status)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string Night = "";
                string Supplier = "";
                string Latitude = "";
                string Longitude = "";

                string HotelAddress = "";
                string HotelCity = "";
                string HotelCountry = "";
                string HotelPhone = "";
                string HotelPostal = "";
                string City = "";
                string AllPassengers = "";
                //  DataSet ds = new DataSet();
                helperDataContext DB = new helperDataContext();
                if (ReservationID != "")
                {
                    //  DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail, dtHotelAdd;
                    //SqlParameter[] SQLParams = new SqlParameter[2];
                    //SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
                    //SQLParams[1] = new SqlParameter("@sid", Uid);
                    //DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_BookingDetails", out ds, SQLParams);

                    //dtHotelReservation = ds.Tables[0];
                    //dtBookedPassenger = ds.Tables[1];
                    //dtBookedRoom = ds.Tables[2];
                    //dtBookingTransactions = ds.Tables[3];
                    //dtAgentDetail = ds.Tables[4];
                    //dtHotelAdd = ds.Tables[5];
                    Int64 ContactID;
                    Int64 UserID = 0;
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    UserID = AccountManager.GetSupplierByUser();
                    ContactID = DB.tbl_AdminLogins.Where(d => d.sid == UserID).FirstOrDefault().ContactID;

                    //if (objGlobalDefault.UserType != "Supplier")
                    //{
                    //    Uid = objGlobalDefault.ParentId.ToString();
                    //    ContactID = DB.tbl_AdminLogins.Where(d => d.sid == Convert.ToInt64(Uid)).FirstOrDefault().ContactID;
                    //}
                    //else
                    //{

                    //    ContactID = objGlobalDefault.ContactID;
                    //}
                    string Logo = HttpContext.Current.Session["logo"].ToString();
                    var dtHotelReservation = (from obj in db.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                    var dtBookedPassenger = (from obj in db.tbl_BookedPassengers where obj.ReservationID == ReservationID select obj).ToList();
                    var dtBookedRoom = (from obj in db.tbl_BookedRooms where obj.ReservationID == ReservationID select obj).ToList();
                    var CommonDB = new dbHotelhelperDataContext();
                    var dtHotelAdd = (from obj in CommonDB.tbl_CommonHotelMasters where obj.sid == Convert.ToInt64(dtHotelReservation.HotelCode) select obj).FirstOrDefault();

                    var UserDetail = (from obj in db.tbl_AdminLogins
                                      join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                      join objh in db.tbl_HCities on objc.Code equals objh.Code
                                      where obj.sid == Convert.ToInt64(Uid) 
                                      select new
                                      {
                                          obj.AgencyLogo,
                                          obj.AgencyName,
                                          obj.AgencyType,
                                          obj.Agentuniquecode,
                                          obj.ContactID,
                                          objc.Address,
                                          objc.email,
                                          objc.Mobile,
                                          objc.phone,
                                          objc.Fax,
                                          objc.PinCode,
                                          objc.sCountry,
                                          objc.Website,
                                          objh.Countryname,
                                          objh.Description
                                      }).FirstOrDefault();

                    Night = Convert.ToString(dtHotelReservation.NoOfDays);
                    Supplier = dtHotelReservation.Source;
                    if (dtHotelReservation.LatitudeMGH != "")
                        Latitude = dtHotelReservation.LatitudeMGH;
                    Longitude = dtHotelReservation.LongitudeMGH;

                    HotelAddress = dtHotelAdd.HotelAddress;
                    City = dtHotelAdd.CityId;
                    //string[] CitySplit = City.Split(',');
                    //HotelCity = CitySplit[0];
                    HotelCountry = dtHotelAdd.CountryId;
                    //    HotelPhone = dtHotelAdd.Rows[0]["Number_"].ToString();
                    //   HotelPostal = dtHotelAdd.Rows[0]["PostalCode"].ToString();
                    string Hoteldestination = dtHotelReservation.City;
                    string InvoiceID = dtHotelReservation.InvoiceID;
                    string VoucherID = dtHotelReservation.VoucherID;
                    string CheckIn = dtHotelReservation.CheckIn;


                    CheckIn = CheckIn.Replace("00:00", "");
                    if (CheckIn.Contains('/'))
                    {
                        string[] CheckInDate = CheckIn.Split('/');
                        CheckIn = CheckInDate[2] + '-' + CheckInDate[1] + '-' + CheckInDate[0];
                    }
                    string CheckOut = dtHotelReservation.CheckOut;
                    CheckOut = CheckOut.Replace("00:00", "");
                    if (CheckIn.Contains('/'))
                    {
                        string[] CheckOutDate = CheckOut.Split('/');
                        CheckOut = CheckOutDate[2] + '-' + CheckOutDate[1] + '-' + CheckOutDate[0];
                    }


                    string ReservationDate = dtHotelReservation.ReservationDate;
                    ReservationDate = ReservationDate.Replace("00:00", "");
                    if (CheckIn.Contains('/'))
                    {
                        string[] ReservationBookingDate = ReservationDate.Split('/');
                        ReservationDate = ReservationBookingDate[2] + '-' + ReservationBookingDate[1] + '-' + ReservationBookingDate[0];

                    }



                    string HotelName = (dtHotelReservation.HotelName);
                    string AgentRef = (dtHotelReservation.AgentRef);
                    string AgencyName = UserDetail.AgencyName;
                    string Address = UserDetail.Address;
                    string Description = UserDetail.Description;
                    string Countryname = UserDetail.Countryname;
                    string phone = UserDetail.phone;
                    string email = UserDetail.email;
                    string agentcode = UserDetail.Agentuniquecode;
                    string CanAmtWoutNight = "";
                    string CanAmtWithTax = "";

                    string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                    sb.Append("<div style=\"font-size: ; width: 100%;\" >");
                    sb.Append("<table style=\"height: 30px; width: 100%;\">");
                    sb.Append("<tbody>");
                    sb.Append("<tr style=\" color: #57585A;\">");
                    sb.Append("<td id=\"AgentLogo\" style=\"width: 33%;padding-left:15px\">");
                    if (DefaultManager.UrlExists(Url + "AgencyLogos/" + UserDetail.Agentuniquecode + ".png"))
                        sb.Append("<img  src=\"" + Url + "AgencyLogo/" + Logo + "\" height=\"auto\" width=\"auto\"></img>");
                    else
                        sb.Append("<h1 class=\"\" height=\"auto\" width=\"auto\">" + UserDetail.AgencyName + "</h1>"); ;
                    //sb.Append("</td>");
                    //sb.Append("<td style=\"width: 33%\"></td>");
                    sb.Append("<td style=\"width: 33%; padding-right:15px\" align=\"right\">");
                    sb.Append("<span style=\"margin-right: 15px\">");
                    sb.Append("<br>");
                    sb.Append(UserDetail.AgencyName);
                    sb.Append(" " + Address + ",<br>");
                    //sb.Append("Juni Mangalwari, Opp. Rahate Hospital,<br>");
                    sb.Append("" + Description + "-" + UserDetail.PinCode + "," + Countryname + "<br>");
                    sb.Append("Tel: " + UserDetail.Mobile + ", Fax:" + UserDetail.Fax + "<br>");
                    sb.Append(" <B>Email: " + UserDetail.email + "<br>");
                    sb.Append("</span>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody>");
                    sb.Append("</table>");
                    sb.Append("</div>");
                    sb.Append("<br>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"1\" style=\"width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left:none; border-right:none\">");
                    sb.Append("<tr>");
                    sb.Append("<td rowspan=\"3\" >");
                    sb.Append("<b style=\"width:35%; color: #3e3434; font-size: 30px; text-align: left; border: none; padding-right:35px; padding-left:10px\">HOTEL VOUCHER</b>");
                    if (Status == "Hold")
                        sb.Append("<b style=\"width:35%; color: red; font-size: 30px; text-align: left; border: none;\">: On Hold</b>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"border-top: none; border-bottom-width: 2px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom:0px;color: #57585A\">");
                    sb.Append("<b> Booking Date &nbsp;&nbsp; &nbsp; &nbsp;     :</b><span style=\"color: #757575\">" + ReservationDate + "</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"border-top: none; border-bottom-width: 2px; border-bottom-color: gray; padding: 10px 0px 0px 3px; margin-bottom: 0px;color: #57585A\">");
                    sb.Append("<b>Voucher No  &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     : </b><span style=\"color: #757575\">" + VoucherID + "</span>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
                    sb.Append("<b> Agent Ref No.&nbsp;&nbsp; &nbsp; &nbsp; :</b> <span style=\"color: #757575\">" + AgentRef + " </span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"border-top: none;  border-bottom-color: gray; padding: 0px 0px 0px 3px;color: #57585A\">");
                    sb.Append("<b>  Supplier Ref No &nbsp;&nbsp; &nbsp; &nbsp; :</b><span style=\"color:#757575\">");
                    //sb.Append(ReservationID);
                    sb.Append("</span>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td colspan=\"2\" style=\"height: 25px; border:none\"></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");

                    sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                    sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>BOOKING DETAILS</b></span>");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%;font-size: 16px; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                    sb.Append("<tr style=\"border: none\">");
                    sb.Append("<td style=\"border: none;color: #57585A\">");
                    sb.Append("<span style=\" margin-left: 15px\"><b>Hotel Name</b></span><br>");
                    sb.Append("<span style=\"margin-left: 15px\"><b>Check In</span><br>");
                    sb.Append("<span style=\"margin-left: 15px\"><b>Check Out</b></span><br>");
                    sb.Append("<span style=\" margin-left: 15px\"><b>Address</b></span><br>");
                    sb.Append("<span style=\" margin-left: 15px\"><b>City</b></span><br>");
                    sb.Append("<span style=\" margin-left: 15px\"><b>Country</b></span><br>");
                    sb.Append("<span style=\" margin-left: 15px\"><b>Phone</b></span><br>");
                    sb.Append("<span style=\" margin-left: 15px\"><b>Postal Code</b></span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"border: none;color: #57585A\">");


                    sb.Append(": <span style=\" margin-left: 15px\"><b>" + HotelName + "</b></span><br>");
                    sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckIn + "</b></span><br>");
                    sb.Append(": <span style=\"margin-left: 15px\"><b>" + CheckOut + "</b></span><br>");
                    sb.Append(": <span style=\" margin-left: 15px\">" + HotelAddress + "</span><br>");
                    sb.Append(": <span style=\" margin-left: 15px\">" + City + "</span><br>");
                    sb.Append(": <span style=\" margin-left: 15px\">" + HotelCountry + "</span><br>");
                    sb.Append(": <span style=\" margin-left: 15px\">" + HotelPhone + "</span><br>");
                    sb.Append(": <span style=\" margin-left: 15px\">" + HotelPostal + "</span>");
                    sb.Append("<span style=\"font-size:20px; font-weight:700;float:right\">" + Night + " Night</span><br>");
                    sb.Append("</td>");
                    sb.Append("<!--<td colspan=\"2\">Sum: $180</td>-->");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");


                    sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                    sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>GUEST & ROOM DETAILS</b></span>");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 3px; border-top-color: gray; border-bottom-width: 3px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                    for (var i = 0; i < dtBookedRoom.Count; i++)
                    {
                        AllPassengers = "";
                        for (int p = 0; p < dtBookedPassenger.Count; p++)
                        {
                            string brrn = (dtBookedRoom[i].RoomNumber);
                            string bprn = (dtBookedPassenger[p].RoomNumber);

                            if (dtBookedPassenger[p].RoomNumber == dtBookedRoom[i].RoomNumber)
                            {
                                AllPassengers += dtBookedPassenger[p].Name + " " + dtBookedPassenger[p].LastName + ", ";

                            }

                        }

                        AllPassengers = AllPassengers.TrimEnd(' ');
                        AllPassengers = AllPassengers.TrimEnd(',');
                        string Chid_Age = dtBookedRoom[i].ChildAge;
                        string chidage = "";
                        string[] splitChid_Age = Chid_Age.Split('|');
                        Int32 Length = splitChid_Age.Length;
                        for (int m = 0; m < splitChid_Age.Length - 1; m++)
                        {
                            if (m != Length - 2)
                            {
                                chidage += splitChid_Age[m] + '|';
                            }
                            else
                            {
                                chidage += splitChid_Age[m];
                            }
                        }

                        string Remark = dtBookedRoom[i].Remark;
                        if (Chid_Age == "")
                        {
                            Chid_Age = "-";
                        }
                        if (Remark == "")
                        {
                            Remark = "-";
                        }
                        sb.Append("<tr style=\"border-spacing: 0px;\">");
                        sb.Append("<td style=\"border-left: none; border-right: none;border-top-width: 3px; border-top-color: white; background-color: gray; color: white; font-size: 18px;  padding-left:10px; font-weight:700\">" + "Room " + dtBookedRoom[i].RoomNumber + "</td>");
                        //sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: gray; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                        sb.Append("<td colspan=\"7\" style=\"border-left: none; border-right: none; border-top-width: 3px; border-top-color: white; height: 35px; background-color: gray; color: white; font-size: 18px;  padding-left: 10px; font-weight: 700;\">");
                        //  sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + dtBookedRoom.Rows[i]["LeadingGuest"].ToString() + "</span></td>");
                        sb.Append("Guest Name :&nbsp;&nbsp;&nbsp; <span>" + AllPassengers + "</span></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                        sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Room Type</td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"> Board </td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\">Total Rooms</td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"> Adults </td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\">Child </td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\">Child Age</td>");
                        sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px\">Remark</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                        sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtBookedRoom[i].RoomType + "</td>");
                        if (dtBookedRoom[i].BoardText == "BB")
                        {
                            dtBookedRoom[i].BoardText = "Bed & Breakfast";
                        }
                        if (dtBookedRoom[i].BoardText == "RO")
                        {
                            dtBookedRoom[i].BoardText = "Room Only";
                        }
                        if (dtBookedRoom[i].BoardText == "HB")
                        {
                            dtBookedRoom[i].BoardText = "Half Board";
                        }
                        if (dtBookedRoom[i].BoardText == "FB")
                        {
                            dtBookedRoom[i].BoardText = "Full Board";
                        }
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].BoardText + "</td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].TotalRooms + "</td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].Adults + "</td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtBookedRoom[i].Child + "</td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + chidage + "</td>");
                        sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\">" + Remark + "</td>");
                        sb.Append("</tr>");
                    }
                    if (Supplier == "GRN")
                    {
                        sb.Append("<tr style=\"border: none; text-align: center; color: #57585A; font-weight:600\">");
                        sb.Append("<td align=\"left\" style=\"border: none; padding-top: 15px; padding: 15px 0px 0px 10px; \">Email Id</td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"> Nationality </td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\">Booking Status</td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                        sb.Append("<td style=\"border: none; padding-top: 15px\"></td>");
                        //sb.Append("</tr>");
                        sb.Append("</tr>");
                        sb.Append("<tr style=\"color: #B0B0B0; border: none; text-align: center; color: #57585A;\">");
                        //  sb.Append("<td align=\"left\" style=\"border: none; padding:12px 0px 15px 10px;\">" + dtHotelAdd.Rows[0]["ContactEmail"].ToString() + "</td>");
                        // sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtHotelAdd.Rows[0]["PaxNationality"].ToString() + "</td>");

                        if (dtHotelReservation.Status == "Vouchered")
                        {
                            sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">Confirmed</td>");
                        }
                        else
                        {
                            sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\">" + dtHotelReservation.Status + "</td>");
                        }



                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                        sb.Append("<td style=\"border: none; padding-top: 12px; padding-bottom: 15px;\"></td>");
                        sb.Append("<td align=\"left\" style=\"border: none; padding-left: 8px; padding-top: 12px; padding-bottom: 15px;\"></td>");
                        sb.Append("</tr>");
                    }

                    sb.Append("</table>");
                    sb.Append("</div>");


                    //Cancellation table goes here.............................................
                    sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px; color: #57585A\">");
                    sb.Append("<span style=\"padding-left:10px\"><b>Cancellation Charge</b></span>");
                    sb.Append("</div>");
                    sb.Append("<div>");
                    sb.Append("<table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-width:3px 0px 3px 0px; border-top-color: gray;  border-bottom-color: #E6DCDC\">");
                    sb.Append("<tr style=\"border: none\">");

                    sb.Append("<td align=\"center\" style=\"background-color: gray; color: white; font-size: 15px; border: none;  font-weight: 700;  padding-left:10px\">");
                    sb.Append("<span>No.</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"width:190px; height: 35px; background-color: gray; color: white; font-size: 15px; border: none; font-weight: 700; text-align: left\">");
                    sb.Append("<span>Room Type</span>");
                    sb.Append("</td>");
                    sb.Append("<td align=\"center\" style=\"height: 35px; background-color: gray; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                    sb.Append("<span>Rooms</span>");
                    sb.Append("</td>");
                    sb.Append("<td align=\"center\" style=\"height: 35px; background-color: gray; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                    sb.Append("<span>Cancellation Details</span>");
                    sb.Append("</td>");
                    //sb.Append("<td align=\"center\" style=\"height: 35px; background-color: gray; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                    //sb.Append("<span>Charge/Unit (<i class='fa fa-inr'></i>)</span>");
                    //sb.Append("</td>");
                    //sb.Append("<td align=\"center\" style=\"height: 35px; background-color: gray; color: white; font-size: 15px; border: none; padding-left: 10px; font-weight: 700;\">");
                    //sb.Append("<span>Total Charge (<i class='fa fa-inr'></i>)</span>");
                    //sb.Append("</td>");
                    sb.Append("</tr>");

                    CanAmtWithTax = "";
                    CanAmtWoutNight = "";

                    for (var i = 0; i < dtBookedRoom.Count; i++)
                    {
                        // string SupplierNoChargeDate = dtBookedRoom.Rows[i]["SupplierNoChargeDate"].ToString();
                        string SupplierNoChargeDate = dtBookedRoom[i].CutCancellationDate;
                        SupplierNoChargeDate = SupplierNoChargeDate.TrimEnd('|');

                        CanAmtWithTax = dtBookedRoom[i].CancellationAmount;
                        CanAmtWithTax = CanAmtWithTax.TrimEnd('|');
                        if (Supplier == "MGHs")
                        {
                            #region MGH

                            if (CanAmtWithTax.Contains('|'))
                            {
                                string[] AmtWithTax = CanAmtWithTax.Split('|');
                                CanAmtWoutNight = "";
                                CanAmtWithTax = "";
                                for (int c = 0; c < AmtWithTax.Length; c++)
                                {
                                    if (AmtWithTax[c] != "")
                                    {
                                        if (c != (AmtWithTax.Length - 1))
                                        {
                                            CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                            CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                        }
                                        else
                                        {
                                            CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]), 2, MidpointRounding.AwayFromZero)).ToString();

                                            CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) * (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //CanAmtWithTax = dtBookedRoom.Rows[i]["CancellationAmount"].ToString();
                                CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax), 2, MidpointRounding.AwayFromZero)).ToString();
                                CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero) * (Convert.ToDecimal(Night))).ToString();
                            }

                            #endregion MGH
                        }
                        else
                        {
                            #region Other Supplier

                            if (CanAmtWithTax.Contains('|'))
                            {
                                string[] AmtWithTax = CanAmtWithTax.Split('|');
                                CanAmtWoutNight = "";
                                CanAmtWithTax = "";
                                for (int c = 0; c < AmtWithTax.Length; c++)
                                {
                                    if (AmtWithTax[c] != "")
                                    {
                                        if (c != (AmtWithTax.Length - 1))
                                        {
                                            CanAmtWoutNight += (decimal.Round((Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night))), 2, MidpointRounding.AwayFromZero)).ToString() + "|";

                                            CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString() + "|";
                                        }
                                        else
                                        {
                                            CanAmtWoutNight += (decimal.Round(Convert.ToDecimal(AmtWithTax[c]) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();

                                            CanAmtWithTax += (decimal.Round((Convert.ToDecimal(AmtWithTax[c])), 2, MidpointRounding.AwayFromZero)).ToString();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                CanAmtWithTax = dtBookedRoom[i].CancellationAmount.TrimEnd('|');
                                if (CanAmtWithTax == "")
                                    CanAmtWithTax = "0";
                                CanAmtWoutNight = (decimal.Round(Convert.ToDecimal(CanAmtWithTax) / (Convert.ToDecimal(Night)), 2, MidpointRounding.AwayFromZero)).ToString();
                                CanAmtWithTax = (decimal.Round((Convert.ToDecimal(CanAmtWithTax)), 2, MidpointRounding.AwayFromZero)).ToString();
                            }

                            #endregion Other Supplier
                        }

                        sb.Append("<tr style=\"border: none; background-color: #E6E7E9; padding: 15px 0px 0px 10px; text-align: left; color: #57585A\">");
                        sb.Append("<td align=\"center\" style=\"width:30px; border: none; text-align: left; padding-left: 15px;\">" + (i + 1) + "</td>");
                        sb.Append("<td style=\"border: none\">" + dtBookedRoom[i].RoomType + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + "Room &nbsp" + dtBookedRoom[i].RoomNumber + "</td>");
                        sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + SupplierNoChargeDate + "</td>");
                        //sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\" >" + CanAmtWoutNight + "</td>");
                        //sb.Append("<td align=\"center\" style=\"border: none; padding-bottom: 8px; padding-top: 8px;\">" + CanAmtWithTax + "</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("<tr style=\"border: none; background-color: #E6E7E9; text-align: left;\">");
                    sb.Append("<td colspan=\"7\" style=\"border: none; width: 20px; padding: 0px 15px 15px 25px; color: #ECA236;\">");
                    sb.Append("*Dates & timing will calculated based on local timing </td>");
                    sb.Append("</tr>");

                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</div>");

                    sb.Append("<div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                    sb.Append("<span style=\"padding-left:10px;color: #57585A\"><b>Terms & Conditions</b></span>");
                    sb.Append("</div>");
                    sb.Append("<hr style=\"border-top-width: 3px\">");
                    sb.Append("<div style=\"height:auto; color: #57585A; font-size:small\">");
                    sb.Append("<ul style=\"list-style-type: disc\">");
                    sb.Append("<li>Please collect all extras directly from clients prior to departure</li>");
                    sb.Append("<li>");
                    sb.Append("It is mandatory to present valid Passport at the time of check-in for International");
                    sb.Append("hotel booking & any valid photo ID for domestic hotel booking");
                    sb.Append("</li>");
                    sb.Append("<li>");
                    sb.Append("Hotel holds right to reject check in or charge supplement due to wrongly selection");
                    sb.Append("of Nationality or Residency at the time of booking");
                    sb.Append("</li>");
                    sb.Append("<li>");
                    sb.Append("Early check-in & late check-out will be subject to availability and approval by");
                    sb.Append("respective hotel only");
                    sb.Append("</li>");
                    sb.Append("<li>");
                    sb.Append("Kindly inform hotel in advance for any late check-in, as hotel may automatically");
                    sb.Append("cancel the room in not check-in by 18:00Hrs local time");
                    sb.Append("</li>");
                    sb.Append("<li>");
                    sb.Append(" You may need to deposit security amount at some destination as per the local rule");
                    sb.Append("and regulations at the time of check-in");
                    sb.Append("</li>");
                    sb.Append("<li>");
                    sb.Append(" Tourism fees may apply at the time of check-in at some destinations like Dubai,Paris, etc...");

                    sb.Append("</li>");
                    sb.Append("<li>General check-in time is 14:00Hrs & check-out time is 12:00Hrs</li>");
                    sb.Append("</ul>");
                    sb.Append("</div>");
                    sb.Append("<br>");
                    sb.Append("<div style=\"background-color: gray; text-align: center; font-size: 20px; color: white; height: 23px\">");
                    sb.Append("<span>");
                    sb.Append("Check your booking details carefully and inform us immediately before it’s too late...");
                    sb.Append("</span>");
                    sb.Append("</div><br>");
                }
                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }

        private static bool UrlExists(string url)
        {
            try
            {
                new System.Net.WebClient().DownloadData(url);
                return true;
            }
            catch (System.Net.WebException e)
            {
                if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                else
                    throw;
            }
        }
    }
}