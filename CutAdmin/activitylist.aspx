﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="activitylist.aspx.cs" Inherits="CutAdmin.activitylist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <script src="Scripts/ActivityFiles.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
           // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
               // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });


        });


    </script>
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Additional styles -->
    
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin margin-left">
            <h3>Activities</h3>
            <hr />
        </hgroup>

        <div class="with-padding">

            <%-- <p class="wrapped left-icon">
                Activity ->  View Activity
            </p>--%>


            <div style="float: right">
                <a href="AddActivity.aspx">
                    <button type="button" class="button blue-gradient glossy" id="btn_AddActivity" onclick="">+ Add New</button>
                </a>

                <%-- &nbsp;&nbsp;&nbsp;  
                <a href="AddTourMode.aspx">          
                <button type="button" class="button blue-gradient glossy" id="btn_Type"  onclick="#">+Type</button>	
				</a>    

                &nbsp;&nbsp;&nbsp; 
                 <a href="AddPriorityType.aspx">              
				<button type="button" class="button blue-gradient glossy" id="btn_Priority"  onclick="#">+Priority</button>			
              </a>--%>
            </div>
            <br />
            <br />
            <br />

            <table class="table responsive-table" id="tbl_Actlist" width="100%">

                <thead>
                    <tr>

                        <th align="center" scope="col">S.N </th>
                        <th align="center" scope="col">Name </th>
                        <th align="center" scope="col">City,Country </th>
                        <th align="center" scope="col">Tour Type</th>
                        <th align="center" scope="col">Activity Type</th>
                        <th align="center" scope="col">Inventory </th>

                        <th align="center" scope="col">Edit | Delete</th>
                        <th align="center" scope="col">Rates</th>

                    </tr>
                </thead>

            </table>
            <%--<div  class="table-footer ">
                 <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
			<br /><br />
			</div>--%>
        </div>

    </section>
    <!-- End main content -->



    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>
    <script src="js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- glDatePicker -->
  <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>
    <script src="js/developr.modal.js"></script>


    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Table sort - DataTables
        var table = $('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
                { 'bSortable': false, 'aTargets': [0, 5] }
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnInitComplete': function (oSettings) {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        $('#sorting-example1').tablesorter({
            headers: {
                0: { sorter: false },
                5: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
                row = tr.next('.row-drop'),
                rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');

            // Add fake row
            $('<tr class="row-drop">' +
                '<td colspan="' + tr.children().length + '">' +
                    '<div class="float-right">' +
                        '<button type="submit" class="button glossy mid-margin-right">' +
                            '<span class="button-icon"><span class="icon-mail"></span></span>' +
                            'Send mail' +
                        '</button>' +
                        '<button type="submit" class="button glossy">' +
                            '<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
                            'Remove' +
                        '</button>' +
                    '</div>' +
                    '<strong>Name:</strong> John Doe<br>' +
                    '<strong>Account:</strong> admin<br>' +
                    '<strong>Last connect:</strong> 05-07-2011<br>' +
                    '<strong>Email:</strong> john@doe.com' +
                '</td>' +
            '</tr>').insertAfter(tr);

        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });

        // Table sort - simple
        $('#sorting-example2').tablesorter({
            headers: {
                5: { sorter: false }
            }
        });

    </script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>

</asp:Content>
