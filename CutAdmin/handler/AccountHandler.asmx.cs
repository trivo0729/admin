﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for AccountHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class AccountHandler : System.Web.Services.WebService
    {
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        string json = "";

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        [WebMethod(EnableSession = true)]
        public string GetCreditInformation()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Int32 Get = 10;
            //string sAdminCredit, sCreditLog, sBankDeposit;
            DataSet ds = new DataSet();
            DataTable dtAdminCredit, dtCreditLog, dtBankDeposit;
            DBHelper.DBReturnCode retCode =DataLayer.AccountManager.CreditInformation(out ds);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                dtAdminCredit = ds.Tables[0];
                dtCreditLog = ds.Tables[1];
                dtBankDeposit = ds.Tables[2];


                try
                {
                    
                    List<Dictionary<string, object>> sAdminCredit = new List<Dictionary<string, object>>();
                    sAdminCredit = JsonStringManager.ConvertDataTable(dtAdminCredit);

                   
                    List<Dictionary<string, object>> sCreditLog = new List<Dictionary<string, object>>();
                    sCreditLog = JsonStringManager.ConvertDataTable(dtCreditLog);

                    // sBankDeposit = "";
                    Session["PaggingSession"] = dtBankDeposit;
                    string Count = (dtBankDeposit.Rows.Count).ToString();
                    IEnumerable<DataRow> allButFirst = dtBankDeposit.AsEnumerable().Skip(0);
                    dtBankDeposit = allButFirst.CopyToDataTable();
                   
                    List<Dictionary<string, object>> sBankDeposit = new List<Dictionary<string, object>>();
                    sBankDeposit = JsonStringManager.ConvertDataTable(dtBankDeposit);
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, Count = Count, AdminCredit = sAdminCredit, CreditLog = sCreditLog, BankDeposit = sBankDeposit });

                    //return "{\"Session\":\"1\",\"retCode\":\"1\",\"Count\":\"" + Count + "\",\"AdminCredit\":[" + sAdminCredit.Trim(',') + "],\"CreditLog\":[" + sCreditLog.Trim(',') + "],\"BankDeposit\":[" + sBankDeposit.Trim(',') + "]}";
                }

                catch
                {
                    return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                }

            }

            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod]
        public string Deleterecord(Int64 Id)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_BankDeposit Delete = DB.tbl_BankDeposits.Single(x => x.sId == Id);
                    DB.tbl_BankDeposits.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                }
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


      
    }
}
