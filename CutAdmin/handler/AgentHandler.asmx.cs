﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for AgentHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AgentHandler : System.Web.Services.WebService
    {
        string json = "";
        //helperDataContext DB = new helperDataContext();

        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string LoadAllBanks()
        {
            DataTable dtResult = new DataTable();
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 pid = objGlobalDefault.ParentId;
                Int64 ContactID = objGlobalDefault.ContactID;

                try
                {
                    using (var DB = new helperDataContext())
                    {
                        var Country = (from obj in DB.tbl_Contacts
                                       join objc in DB.tbl_HCities on obj.Code equals objc.Code
                                       where obj.ContactID == ContactID
                                       select new
                                       {
                                           objc.Countryname
                                       }).FirstOrDefault();
                        var List = (from obj in DB.tbl_BankDetails
                                    where obj.ParentId == pid && obj.Country == Country.Countryname
                                    select new
                                    {
                                        obj.sid,
                                        obj.BankName,
                                        obj.AccountNo,
                                        obj.Branch,
                                        obj.SwiftCode,
                                        obj.ParentId

                                    }).ToList();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BankName = List });
                    }
                }
                catch
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;


            }
            else
                return "{\"Session\":\"0\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string InsertDeposit(string BankName, string AccountNumber, string AmountDeposit, string Remarks, string TypeOfDeposit, string TypeOfCash, string TypeOfCheque, string EmployeeName, string ReceiptNumber, string ChequeDDNumber, string ChequeDrawn, string DepositDate)
        {
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 uId = objGlobalDefaults.sid;
            //if (objGlobalDefaults.UserType == "SupplierStaff")
            //    uId = objGlobalDefaults.ParentId;
            Int64 uId = AccountManager.GetUserByLogin();
            Int64 Parentid = AccountManager.GetSupplierByUser();
            string Franchisee = objGlobalDefaults.Franchisee;
            string Agency = objGlobalDefaults.AgencyName;
            string DepositDat = DepositDate;
            decimal dlAmountDeposit;
            decimal.TryParse(AmountDeposit, out dlAmountDeposit);
            string sCreationdate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);

            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_BankDeposit Add = new tbl_BankDeposit();
                    Add.uId = uId;
                    Add.DepositAmount = dlAmountDeposit;
                    Add.BankName = BankName;
                    Add.AccountNumber = AccountNumber;
                    Add.ChequeDetail = "";
                    Add.CreationDate = sCreationdate;
                    Add.Remarks = Remarks;
                    Add.DepositUpdateFlag = false;
                    Add.CreditedAmount = 000;
                    Add.Updatedate = "";
                    Add.BankNarration = "";
                    Add.ExecutiveName = "";
                    Add.DepositDate = DepositDat;
                    Add.Typeofdeposit = TypeOfDeposit;
                    Add.Typeofcash = TypeOfCash;
                    Add.Typeofcheque = TypeOfCheque;
                    Add.EmployeeName = EmployeeName;
                    Add.ReceiptNumber = ReceiptNumber;
                    Add.TransactionId = ReceiptNumber;
                    Add.ChequeorDDNumber = ChequeDDNumber;
                    Add.ChequeDrawn = ChequeDrawn;
                    Add.ExecutiveRemarks = "";
                    Add.Mobile = "N/A";
                    Add.ApproveFlag = false;
                    Add.FranchiseeId = Franchisee;
                    //Add.ParentID = Parentid;
                    DB.tbl_BankDeposits.InsertOnSubmit(Add);
                    DB.SubmitChanges();
                }
                AccountManager.SupportMail("", objGlobalDefaults.Agentuniquecode, Agency, BankName, AccountNumber, AmountDeposit, Remarks, TypeOfDeposit, TypeOfCash, TypeOfCheque, EmployeeName, ReceiptNumber, ChequeDDNumber, ChequeDrawn, DepositDate);

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            //DBHelper.DBReturnCode retCode = AccountManager.AddDeposit(BankName, AccountNumber, AmountDeposit, Remarks, TypeOfDeposit, TypeOfCash, TypeOfCheque, EmployeeName, ReceiptNumber, ChequeDDNumber, ChequeDrawn, Date);
            //if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //    string Name = objGlobalDefaults.ContactPerson;
            //    string Email = objGlobalDefaults.uid;
            //    string Amount = AmountDeposit;
            //    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            //}
            //else
            //{
            //    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            //}
            return json;
        }

        public class AgentInfo
        {
            public float AgentCredit { get; set; }
            public string SupplierType { get; set; }
            public float AgentCreditLimit { get; set; }
            public float AgentVisaMarkup { get; set; }
            public DataTable dtVisaDetails { get; set; }
            //public string[] VisaPath { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetAgentInfo()
        {
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 uId = AccountManager.GetUserByLogin();
                //if (objGlobalDefaults.UserType == "SupplierStaff")
                //    uId = objGlobalDefaults.ParentId;
                string AgentName = objGlobalDefaults.ContactPerson;
                string AgentCode = objGlobalDefaults.Agentuniquecode;
                string AvailableCredit = "0";
                string CreditLimit = "0";
                string CreditLimit_Cmp = "0";
                string MaxCreditLimit = "0";
                string MaxCreditLimit_Cmp = "0";
                bool CreditFlag = false;
                bool OneTimeCreditFlag = false;
                string CurrencyClass = objGlobalDefaults.Currency;
                string CompanyNo = "";
                string CompanyMob = "";
                string ComapanyMail = "";
                Int64 ContactID = objGlobalDefaults.ContactID;
                // ... Switch on the string.
                switch (CurrencyClass)
                {
                    case "AED":
                        CurrencyClass = "Currency-AED";

                        break;
                    case "SAR":
                        CurrencyClass = "Currency-SAR";
                        break;
                    case "EUR":
                        CurrencyClass = "fa fa-eur";
                        break;
                    case "GBP":
                        CurrencyClass = "fa fa-gbp";
                        break;
                    case "USD":
                        CurrencyClass = "fa fa-dollar";
                        break;
                    case "INR":
                        CurrencyClass = "fa fa-inr";
                        break;
                }
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_Contacts
                                where obj.ContactID == ContactID

                                select new
                                {
                                    obj.Code,
                                    obj.StateID,
                                    obj.sCountry,
                                }).ToList();

                    string AgentCity = List[0].Code;
                    string AgentState = List[0].StateID;

                    Session["AgentCity"] = AgentCity;
                    Session["AgentState"] = AgentState;
                }
                DataTable dtResult;
                DBHelper.DBReturnCode retCode = AccountManager.GetAvailableCredit(out dtResult);
                AgentInfo objAgentInfo = new AgentInfo();
                if (DBHelper.DBReturnCode.SUCCESS == retCode)
                {
                    if (dtResult.Rows.Count > 0)
                    {
                        if (HttpContext.Current.Session["AgentInfo"] == null)
                        {
                            if (dtResult.Rows[0]["AvailableCredit"].ToString() != "")
                                AvailableCredit = dtResult.Rows[0]["AvailableCredit"].ToString();
                            AvailableCredit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(AvailableCredit));

                            if (dtResult.Rows[0]["CreditAmount"].ToString() != "")
                                CreditLimit = dtResult.Rows[0]["CreditAmount"].ToString();
                            CreditLimit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(CreditLimit));

                            if (dtResult.Rows[0]["CreditAmount_Cmp"].ToString() != "")
                                CreditLimit_Cmp = dtResult.Rows[0]["CreditAmount_Cmp"].ToString();
                            CreditLimit_Cmp = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(CreditLimit_Cmp));

                            if (dtResult.Rows[0]["MaxCreditLimit"].ToString() != "")
                                MaxCreditLimit = dtResult.Rows[0]["MaxCreditLimit"].ToString();
                            MaxCreditLimit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(MaxCreditLimit));

                            if (dtResult.Rows[0]["MaxCreditLimit_Cmp"].ToString() != "")
                                MaxCreditLimit_Cmp = dtResult.Rows[0]["MaxCreditLimit_Cmp"].ToString();
                            MaxCreditLimit_Cmp = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(MaxCreditLimit_Cmp));

                            if (dtResult.Rows[0]["Credit_Flag"].ToString() != "")
                                CreditFlag = Convert.ToBoolean(dtResult.Rows[0]["Credit_Flag"]);
                            if (dtResult.Rows[0]["OTC"].ToString() != "")
                                OneTimeCreditFlag = Convert.ToBoolean(dtResult.Rows[0]["OTC"]);
                            objAgentInfo.AgentCredit = Convert.ToSingle(dtResult.Rows[0]["AvailableCredit"]);
                            objAgentInfo.AgentCreditLimit = Convert.ToSingle(dtResult.Rows[0]["CreditAmount"]);
                            HttpContext.Current.Session["AgentInfo"] = objAgentInfo;
                        }
                        else
                        {
                            objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
                            if (dtResult.Rows[0]["AvailableCredit"].ToString() != "")
                                AvailableCredit = dtResult.Rows[0]["AvailableCredit"].ToString();
                            AvailableCredit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(AvailableCredit));

                            if (dtResult.Rows[0]["CreditAmount"].ToString() != "")
                                CreditLimit = dtResult.Rows[0]["CreditAmount"].ToString();
                            CreditLimit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(CreditLimit));

                            if (dtResult.Rows[0]["CreditAmount_Cmp"].ToString() != "")
                                CreditLimit_Cmp = dtResult.Rows[0]["CreditAmount_Cmp"].ToString();
                            CreditLimit_Cmp = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(CreditLimit_Cmp));

                            if (dtResult.Rows[0]["MaxCreditLimit"].ToString() != "")
                                MaxCreditLimit = dtResult.Rows[0]["MaxCreditLimit"].ToString();
                            MaxCreditLimit = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(MaxCreditLimit));

                            if (dtResult.Rows[0]["MaxCreditLimit_Cmp"].ToString() != "")
                                MaxCreditLimit_Cmp = dtResult.Rows[0]["MaxCreditLimit_Cmp"].ToString();
                            MaxCreditLimit_Cmp = String.Format(new CultureInfo("en-IN", false), "{0:n}", Convert.ToDouble(MaxCreditLimit_Cmp));

                            if (dtResult.Rows[0]["Credit_Flag"].ToString() != "")
                                CreditFlag = Convert.ToBoolean(dtResult.Rows[0]["Credit_Flag"]);
                            if (dtResult.Rows[0]["OTC"].ToString() != "")
                                OneTimeCreditFlag = Convert.ToBoolean(dtResult.Rows[0]["OTC"]);
                            objAgentInfo.AgentCredit = Convert.ToSingle(dtResult.Rows[0]["AvailableCredit"]);
                            objAgentInfo.AgentCreditLimit = Convert.ToSingle(dtResult.Rows[0]["CreditAmount"]);
                            HttpContext.Current.Session["AgentInfo"] = objAgentInfo;
                        }

                    }
                    retCode = AccountManager.GetFranchiseeOnAgent(out dtResult);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        CompanyNo = dtResult.Rows[0]["phone"].ToString();
                        CompanyMob = dtResult.Rows[0]["Mobile"].ToString();
                        ComapanyMail = dtResult.Rows[0]["email"].ToString();

                    }

                }

                return "{\"Session\":\"1\",\"retCode\":\"1\",\"AgentName\":\"" + AgentName + "\",\"AgentCode\":\"" + AgentCode + "\",\"AvailableCredit\":\"" + AvailableCredit + "\",\"CreditLimit\":\"" + CreditLimit + "\",\"MaxCreditLimit\":\"" + MaxCreditLimit + "\",\"CreditFlag\":\"" + CreditFlag + "\",\"OneTimeCreditFlag\":\"" + OneTimeCreditFlag + "\",\"CreditLimit_Cmp\":\"" + CreditLimit_Cmp + "\",\"MaxCreditLimit_Cmp\":\"" + MaxCreditLimit_Cmp + "\",\"CurrencyClass\":\"" + CurrencyClass + "\",\"Mobile\":\"" + CompanyMob + "\",\"Phone\":\"" + CompanyNo + "\",\"Email\":\"" + ComapanyMail + "\"}";
            }
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplierName()
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    var List = (from obj in DB.tbl_AdminLogins where obj.sid == objGlobalDefault.ParentId select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, SupplierName = List });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }
}
