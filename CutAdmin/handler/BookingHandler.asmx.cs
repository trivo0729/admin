﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();

        dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }

            public Int64 Id { get; set; }
            public Int64 AddOnsID { get; set; }
            public bool IsCumpulsary { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 RateID { get; set; }
            public string RateType { get; set; }
            public Decimal Rate { get; set; }
            public Int64 UserID { get; set; }


        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }

        public class RateGroup
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), Convert.ToInt64(arrHotelDetails.HotelId));
                return jsSerializer.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, errMsg = ex.Message });
            }
        }

      
        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingList"] = null;
                List<Reservations> BookingList = Reservations.BookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d=>d.Sid).ToList() });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GroupBookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingList"] = null;
                List<Reservations> BookingList = Reservations.GroupBookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList() });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            try
            {
                List<Reservations> BookingList = Reservations.BookingList();
                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingPending });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingConfirmed });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = BookingList.Where(d => d.Status == "Cancelled").ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingRejected });
                }
                else if (status == "OnRequest" && type == "Bookings")
                {
                    var OnHoldBookings = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookings });
                }
                else if (status == "OnRequest" && type == "Requested")
                {
                    var OnHoldRequested = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }
                else if (status == "OnRequest" && type == "Confirmed")
                {
                    var OnHoldConfirmed = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldConfirmed });
                }



                else if (status == "Vouchered" && type == "ReconfirmPending")
                {
                    var ReconfirmPending = BookingList.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmPending });
                }
                else if (status == "Vouchered" && type == "ReconfirmRequested")
                {
                    var ReconfirmRequested = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmRequested });
                }
                else if (status == "Vouchered" && type == "ReconfirmReject")
                {
                    var ReconfirmReject = BookingList.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmReject });
                }

                else if (status == "GroupRequest" && type == "GroupRequestPending")
                {
                    var GroupRequestPending = BookingList.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestPending });
                }
                else if (status == "GroupRequest" && type == "GroupRequestRequested")
                {
                    var GroupRequestRequested = BookingList.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestRequested });
                }
                else if (status == "GroupRequest" && type == "GroupRequestReject")
                {
                    var GroupRequestReject = BookingList.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestReject });
                }

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Search(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SupplierBookingList"];
            DataRow[] row = null;
            try
            {
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("Name like '%" + Passenger + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                Session["SearchSupplierBookingList"] = BookingList;
                List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                BookingList.Dispose();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }


        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var Detail = (from obj in DB.tbl_CommonHotelReservations
                                  join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                                  join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
                                  where objPass.IsLeading == true && obj.ReservationID == ReservationID
                                  select new
                                  {
                                      obj.Sid,
                                      obj.ReservationID,
                                      obj.ReservationDate,
                                      obj.Status,
                                      obj.TotalFare,
                                      obj.TotalRooms,
                                      obj.HotelName,
                                      obj.CheckIn,
                                      obj.CheckOut,
                                      obj.City,
                                      obj.Children,
                                      obj.BookingStatus,
                                      obj.NoOfAdults,
                                      obj.Source,
                                      obj.Uid,
                                      obj.NoOfDays,
                                      objPass.Name,
                                      objPass.LastName,
                                      objPass.RoomNumber,
                                      objroom.CancellationAmount,
                                      objroom.CutCancellationDate
                                  }).ToList().Distinct();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAmendmentDetail(string ReservationID)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var Detail = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    using (var db = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    tbl_CommonHotelReconfirmationDetail Confirm = new tbl_CommonHotelReconfirmationDetail();
                    Confirm.UserID = UserId;
                    Confirm.ReservationId = ReservationId;
                    Confirm.ReconfirmDate = ConfirmDate;
                    Confirm.Staff_Name = StaffName;
                    Confirm.ReconfirmThrough = ReconfirmThrough;
                    Confirm.HotelConfirmationNo = HotelConfirmationNo;
                    Confirm.Comment = Comment;
                    DB.tbl_CommonHotelReconfirmationDetails.InsertOnSubmit(Confirm);
                    DB.SubmitChanges();

                    DBHelper.DBReturnCode retcode = DBHelper.DBReturnCode.SUCCESS;// BookingManager.ConfirmHoldBooking(ReservationId);

                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
                        Bit.IsConfirm = true;
                        Bit.Status = "Vouchered";
                        Bit.HotelConfirmationNo = HotelConfirmationNo;
                        DB.SubmitChanges();
                        ReconfirmationMail(Convert.ToInt64(Bit.Uid), Confirm.sid, HotelName, ReservationId, StaffName, Comment);

                        //New Work///

                        Int64 Uid = 0; string sTo = "";
                        Uid = AccountManager.GetSupplierByUser();
                        sTo = objGlobalDefault.uid;
                        sTo += "," + (from obj in db.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                        //  sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                        MailManager.SendReconfrmInvoice(ReservationId, Uid, sTo);
                    }
                    }

                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string AcceptGroupReq(Int64 Id, string Status, string Remark)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
                    Accept.Status = "Vouchered";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string RejectGroupReq(Int64 Id, string Status , string Remark)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
                    Accept.Status = "Cancelled";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


        public static bool ReconfirmationMail(Int64 Uid,Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                using (var DB = new dbHotelhelperDataContext())
                {
                    using (var db = new helperDataContext())
                    {
                        var arrDetail = (from objRes in db.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                        string CompanyName = (from objRes in db.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                        var sReservation = (from obj in DB.tbl_CommonHotelReservations
                                            from objAgent in db.tbl_AdminLogins
                                            where obj.ReservationID == ReservationId
                                            select new
                                            {
                                                //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                                CompanyName = CompanyName,
                                                customer_Email = objAgent.uid,
                                                customer_name = objAgent.AgencyName,
                                                VoucherNo = obj.VoucherID,
                                                PareniId = objAgent.ParentID

                                            }).FirstOrDefault();

                        var sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                        string BCcTeamMails = sMail.BCcMail;
                        string CcTeamMail = sMail.CcMail;


                        StringBuilder sb = new StringBuilder();

                        sb.Append("<html>");
                        sb.Append("<head>");
                        sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                        sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                        sb.Append("<style>");
                        sb.Append("<!--");
                        sb.Append(" /* Font Definitions */");
                        sb.Append(" @font-face");
                        sb.Append("	{font-family:\"Cambria Math\";");
                        sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                        sb.Append("@font-face");
                        sb.Append("	{font-family:Calibri;");
                        sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                        sb.Append(" /* Style Definitions */");
                        sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                        sb.Append("	{margin-top:0cm;");
                        sb.Append("	margin-right:0cm;");
                        sb.Append("	margin-bottom:10.0pt;");
                        sb.Append("	margin-left:0cm;");
                        sb.Append("	line-height:115%;");
                        sb.Append("	font-size:11.0pt;");
                        sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                        sb.Append("a:link, span.MsoHyperlink");
                        sb.Append("	{color:blue;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append("a:visited, span.MsoHyperlinkFollowed");
                        sb.Append("	{color:purple;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append(".MsoPapDefault");
                        sb.Append("	{margin-bottom:10.0pt;");
                        sb.Append("	line-height:115%;}");
                        sb.Append("@page Section1");
                        sb.Append("	{size:595.3pt 841.9pt;");
                        sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                        sb.Append("div.Section1");
                        sb.Append("	{page:Section1;}");
                        sb.Append("-->");
                        sb.Append("</style>");
                        sb.Append("</head>");
                        sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                        sb.Append("<div class=Section1>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                        sb.Append("" + sReservation.customer_name + ",</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                        //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                        sb.Append("from&nbsp;<b>" + sReservation.CompanyName + "</b></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                        sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                        sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                        sb.Append(" style='border-collapse:collapse'>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                        sb.Append("  No.</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                        sb.Append("  Name</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                        sb.Append("  ID</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                        sb.Append("  by</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append(" <tr>");
                        sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                        sb.Append("  / Note</span></p>");
                        sb.Append("  </td>");
                        sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                        sb.Append("  </td>");
                        sb.Append(" </tr>");
                        sb.Append("</table>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                        sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                        sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                        sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                        sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                        sb.Append("<p class=MsoNormal>&nbsp;</p>");
                        sb.Append("</div>");
                        sb.Append("</body>");
                        sb.Append("</html>");

                        string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                        List<string> attachmentList = new List<string>();

                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Dictionary<string, string> Email2List = new Dictionary<string, string>();
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                        Email1List.Add(sReservation.customer_Email, "");
                        Email1List.Add(BCcTeamMails, "");
                        Email2List.Add(CcTeamMail, "");

                        MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveAmendemnt(string ReservationID, List<tbl_CommonBookedPassenger> ListPax)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                   
                    var oldDetails = (from obj in DB.tbl_CommonBookedPassengers
                                    where obj.ReservationID == ReservationID
                                    select obj).ToList();
                    if (oldDetails.Count != 0)
                    {
                        DB.tbl_CommonBookedPassengers.DeleteAllOnSubmit(oldDetails);
                        DB.SubmitChanges();
                    }
                    DB.tbl_CommonBookedPassengers.InsertAllOnSubmit(ListPax);
                    DB.SubmitChanges();

                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }



    }
}

