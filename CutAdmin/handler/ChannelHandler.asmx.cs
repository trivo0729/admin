﻿using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ChannelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class ChannelHandler : System.Web.Services.WebService
    {
         helperDataContext db= new helperDataContext();

        // Get Tax Details
        [WebMethod(EnableSession = true)]
        public string GetAuthority(Int64 Sid)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                var sForms = (from obj in db.tbl_AgentForms where obj.Type == "Form" select obj).Distinct().ToList();
                var sAPI = (from obj in db.tbl_AgentForms where obj.Type == "Hotel" || obj.Type == "Activity" select obj).Distinct().ToList();

                // API Grant All //
                //List<int> ListAgent = (from obj in db.tbl_AdminLogins where obj.UserType =="Agent" select obj.sid).ToList();

                //List<tblAgentRoleManager> List = new List<tblAgentRoleManager>();
                //foreach (var nUid in ListAgent)
                //{
                //    foreach (var nFormId in sAPI)
                //    {
                //        List.Add(new tblAgentRoleManager
                //        {
                //            nFormId = Convert.ToInt64(nFormId.nId),
                //            nUid = nUid,
                //        });
                //    }
                //}
                //db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                //db.SubmitChanges();

                var AssigndForms = (from objForm in db.tbl_AgentForms
                                    from objAgent in db.tblAgentRoleManagers
                                    where objForm.nId == objAgent.nFormId && objAgent.nUid == Sid && objForm.Type == "Form"
                                    select new
                                    {
                                        objForm.sFormName,
                                        objAgent.nId
                                    });

                var AssigndAPI = (from objForm in db.tbl_AgentForms
                                  from objAgent in db.tblAgentRoleManagers
                                  where objForm.nId == objAgent.nFormId && objAgent.nUid == Sid && objForm.Type == "Hotel"
                                  select new
                                  {
                                      objForm.sFormName,
                                      objAgent.nId
                                  });
                var sHold = (from obj in db.tbl_AdminLogins where obj.sid == Sid select obj.HoldGrant).FirstOrDefault();
                var SupplierVisible = (from obj in db.tbl_AdminLogins where obj.sid == Sid select obj.SupplierVisible).FirstOrDefault();
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                    sForms = sForms,
                    AssigndForms = AssigndForms,
                    sAPI = sAPI,
                    AssigndAPI = AssigndAPI,
                    sHold = sHold,
                    SupplierVisible = SupplierVisible,

                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }

        [WebMethod]
        public string SetFormsForAgentRole(Int64 nUid, bool SupplierVisible, bool OnHold, bool Cropping, string Type, params string[] arr)
        {
            try
            {
                List<tblAgentRoleManager> List = (from obj in db.tblAgentRoleManagers
                                                  join
                                                      objForm in db.tbl_AgentForms on obj.nFormId equals objForm.nId
                                                  where obj.nUid == nUid && objForm.Type == Type
                                                  select obj).ToList();
                db.tblAgentRoleManagers.DeleteAllOnSubmit(List);
                var AgentAuthority = (from obj in db.tbl_AdminLogins where obj.sid == nUid select obj).FirstOrDefault();
                AgentAuthority.SupplierVisible = SupplierVisible;
                AgentAuthority.HoldGrant = OnHold;
                db.SubmitChanges();
                List = new List<tblAgentRoleManager>();
                foreach (var nFormId in arr)
                {
                    List.Add(new tblAgentRoleManager
                    {
                        nFormId = Convert.ToInt64(nFormId),
                        nUid = nUid,
                    });
                }
                db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                db.SubmitChanges();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        public void DefaultAccess(Int64 UID)
        {
            List<tblAgentRoleManager> List = new List<tblAgentRoleManager>();
            List<tbl_AgentForm> sAPI = new List<tbl_AgentForm>();
            sAPI.Add(new tbl_AgentForm { nId = 6 });
            sAPI.Add(new tbl_AgentForm { nId = 9 });
            sAPI.Add(new tbl_AgentForm { nId = 11 });
            sAPI.Add(new tbl_AgentForm { nId = 12 });
            foreach (var nFormId in sAPI)
            {
                List.Add(new tblAgentRoleManager
                {
                    nFormId = Convert.ToInt64(nFormId.nId),
                    nUid = UID,
                });
            }
            db.tblAgentRoleManagers.InsertAllOnSubmit(List);
            db.SubmitChanges();
        }

        [WebMethod(EnableSession = true)]
        public string Get()
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            try
            {

                objSerlizer.MaxJsonLength = Int32.MaxValue;

                var sAPI = (from obj in db.tbl_AgentForms where obj.Type == "Hotel" select obj).Distinct().ToList();

                var AssigndAPI = (from objForm in db.tbl_AgentForms
                                  from objAgent in db.tblAgentRoleManagers
                                  where objForm.nId == objAgent.nFormId && objAgent.nUid == 232 && objForm.Type == "Hotel"
                                  select new
                                  {
                                      objForm.sFormName,
                                      objAgent.nId
                                  });


                return objSerlizer.Serialize(new { retCode = 1, Session = 1, sAPI = sAPI, AssigndAPI = AssigndAPI, });
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForRole(string Type, params string[] arr)
        {
            try
            {
                List<tblAgentRoleManager> List = (from obj in db.tblAgentRoleManagers
                                                  join
                                                      objForm in db.tbl_AgentForms on obj.nFormId equals objForm.nId
                                                  where obj.nUid == 232 && objForm.Type == Type
                                                  select obj).ToList();
                db.tblAgentRoleManagers.DeleteAllOnSubmit(List);
                List = new List<tblAgentRoleManager>();
                foreach (var nFormId in arr)
                {
                    List.Add(new tblAgentRoleManager
                    {
                        nFormId = Convert.ToInt64(nFormId),
                        nUid = 232,
                    });
                }
                db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                db.SubmitChanges();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

    }
}
