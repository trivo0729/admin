﻿using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for CityAddUpdateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CityAddUpdateHandler : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string GetCountryCity()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CountryList = (from obj in DB.tbl_HCities
                                       select new
                                       {
                                           obj.Countryname,
                                           obj.Country,
                                       }).Distinct().ToList();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, CountryList = CountryList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(true)]
        public string SearchCity(string SearchCity)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
         
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_HCities
                                where obj.Description.Contains(SearchCity)
                                select new
                                {
                                    obj.Country,
                                    obj.Countryname,
                                    obj.Description,
                                    obj.Code,

                                }).Distinct().ToList();


                    CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
                    DataTable dt = lsttodt.ToDataTable(List);
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });

                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddCity(string CountryCode, string CountryName, string CityName)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string jsonString = "";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;

            Random generator = new Random();
            String Password = generator.Next(0, 999999).ToString("D6");
            Password = Cryptography.EncryptText(Password);
            try
            {
                using (var DB = new helperDataContext())
                {
                    string UniqueCode = CountryCode + GenerateRandomString(4);

                    CutAdmin.dbml.tbl_HCity objCity = new CutAdmin.dbml.tbl_HCity();
                    objCity.Country = CountryCode;
                    objCity.Countryname = CountryName;
                    objCity.Description = CityName;
                    objCity.Code = UniqueCode;
                    DB.tbl_HCities.InsertOnSubmit(objCity);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }
        public static string GenerateRandomString(int length)
        {
            string rndstring = "";
            bool IsRndlength = false;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            if (IsRndlength)
                length = rnd.Next(4, length);
            for (int i = 0; i < length; i++)
            {
                int toss = rnd.Next(1, 10);
                if (toss > 5)
                    rndstring += (char)rnd.Next((int)'A', (int)'Z');
                else
                    rndstring += rnd.Next(0, 9).ToString();
            }
            return rndstring;
        }
    }
}
