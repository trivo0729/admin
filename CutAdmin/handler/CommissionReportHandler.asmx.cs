﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for CommissionReportHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CommissionReportHandler : System.Web.Services.WebService
    {

        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";
         static helperDataContext db = new helperDataContext();
         dbHotelhelperDataContext dbTax = new dbHotelhelperDataContext();
  
      
        //[WebMethod(EnableSession = true)]
        //public string GetCommissionInvoiceReport(string Month, string Year)
        //{
        //    try
        //    {
        //        Session["CommisionReportList"] = null;
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];


        //        var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.ParentID == objGlobalDefault.sid select obj).ToList().OrderBy(d => d.ID).ToList();

        //        var arrReser = (from obj in db.tbl_CommonHotelReservations where obj.ParentID == objGlobalDefault.sid select obj).ToList();

        //        List<CutAdmin.dbml.tbl_Invoice> ListDetail = new List<tbl_Invoice>();
        //        List<tbl_CommonHotelReservation> ListReserve = new List<tbl_CommonHotelReservation>();
        //        List<CommissionReport> ListReport = new List<CommissionReport>();

        //        foreach (var item in arrInvoice)
        //        {
        //            string Mnth = item.InvoiceDate.Split('-')[1];
        //            string Yer = item.InvoiceDate.Split('-')[2];
        //            if (Month == Mnth && Year == Yer)
        //            {
        //                if (arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault() != null)
        //                {
        //                    ListDetail.Add(item);
        //                    ListReserve.Add(new tbl_CommonHotelReservation
        //                    {
        //                        CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckIn,
        //                        CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckOut,
        //                        NoOfDays = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().NoOfDays,
        //                    });

        //                    ListReport.Add(new CommissionReport
        //                    {
        //                        InvoiceDate = item.InvoiceDate,
        //                        InvoiceNo = item.InvoiceNo,
        //                        CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckIn,
        //                        CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckOut,
        //                        RoomNight = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().NoOfDays.ToString(),
        //                        InvoicAmount = item.InvoiceAmount.ToString(),
        //                        CommissionAmount = item.Commission.ToString()
        //                    });
        //                }

        //            }
        //        }
        //        DataTable dtResult = new DataTable();
        //        CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
        //        dtResult = lsttodt.ToDataTable(ListReport);
        //        Session["CommisionReportList"] = dtResult;
        //        dtResult.Dispose();

        //        if (ListDetail.Any())
        //        {
        //            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListDetail, ArrReser = ListReserve });
        //        }
        //        else
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }

        //    }


        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

     
        //[WebMethod(EnableSession = true)]
        //public string GetCommissionInvoiceReportAll(string strYear)
        //{
        //    try
        //    {
        //        Session["CommisionReportList"] = null;
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];


        //        var arrInvoice = (from obj in dbTax.tbl_Invoices where obj.ParentID == objGlobalDefault.sid select obj).ToList().OrderBy(d => d.ID).ToList();

        //        var arrReser = (from obj in db.tbl_CommonHotelReservations where obj.ParentID == objGlobalDefault.sid select obj).ToList();

        //        List<tbl_Invoice> ListDetail = new List<tbl_Invoice>();
        //        List<tbl_CommonHotelReservation> ListReserve = new List<tbl_CommonHotelReservation>();
        //        List<CommissionReport> ListReport = new List<CommissionReport>();

        //        foreach (var item in arrInvoice)
        //        {
        //            string Yer = item.InvoiceDate.Split('-')[2];
        //            if (strYear == Yer)
        //            {

        //                if (arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault() != null)
        //                {
        //                    ListDetail.Add(item);
        //                    ListReserve.Add(new tbl_CommonHotelReservation
        //                    {
        //                        CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckIn,
        //                        CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckOut,
        //                        NoOfDays = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().NoOfDays,
        //                    });

        //                    ListReport.Add(new CommissionReport
        //                        {
        //                            InvoiceDate = item.InvoiceDate,
        //                            InvoiceNo = item.InvoiceNo,
        //                            CheckIn = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckIn,
        //                            CheckOut = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().CheckOut,
        //                            RoomNight = arrReser.Where(d => d.InvoiceID == item.InvoiceNo && d.ParentID == objGlobalDefault.sid).FirstOrDefault().NoOfDays.ToString(),
        //                            InvoicAmount = item.InvoiceAmount.ToString(),
        //                            CommissionAmount = item.Commission.ToString()
        //                        });

        //                }
        //            }

        //        }

        //        DataTable dtResult = new DataTable();
        //        CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
        //        dtResult = lsttodt.ToDataTable(ListReport);
        //        Session["CommisionReportList"] = dtResult;
        //        dtResult.Dispose();
               

        //        if (ListDetail.Any())
        //        {
        //            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = ListDetail, ArrReser = ListReserve });
        //        }
        //        else
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }

        //    }


        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        public class CommissionReport
        {
            public string InvoiceDate { get; set; }
            public string InvoiceNo { get; set; }
            public string CheckIn { get; set; }
            public string CheckOut { get; set; }
        
            public string RoomNight { get; set; }
            public string InvoicAmount { get; set; }
            public string CommissionAmount { get; set; }
        }
    }
}
