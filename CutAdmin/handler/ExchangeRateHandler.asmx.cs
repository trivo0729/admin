﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ExchangeRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ExchangeRateHandler : System.Web.Services.WebService
    {
        string json = "";
        CutAdmin.HotelAdmin.Handler.ExchangeRateHandler obj = new HotelAdmin.Handler.ExchangeRateHandler();


        [WebMethod(EnableSession = true)]
        public string GetExchangeLog()
        {
            return json = obj.GetExchangeLog();
        }

         #region Search Exchange Rate
        [WebMethod(EnableSession = true)]
        public string SearchExchangeUpdate(string currency, string updatedBy, string updateDate)
        {
            return json=obj.SearchExchangeUpdate( currency, updatedBy, updateDate);
        }
         #endregion

        #region Add Update
        [WebMethod(EnableSession = true)]
        public string GetExchangeRate(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            return json = obj.GetExchangeRate( ExchangeRate, MarkupAmt, MarkupPer);
        }

        [WebMethod(EnableSession = true)]
        public string UpdateExchangeRate(Int64[] MarkupSid, Int64[] LogSid, string[] Currency, decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            return json=obj.UpdateExchangeRate( MarkupSid, LogSid, Currency, ExchangeRate, MarkupAmt,  MarkupPer);
        }

        [WebMethod(EnableSession = true)]
        public string AddExchangeRate(string MarkupSid, string Currency, string ExchangeRate, string MarkupAmt, string MarkupPer)
        {
            return json = obj.AddExchangeRate( MarkupSid,  Currency,  ExchangeRate,  MarkupAmt,  MarkupPer);
        }

        #endregion

        #region Update Excnage from Online
        [WebMethod(EnableSession = true)]
        public string GetOnlineRate()
        {
            return json = obj.GetOnlineRate();
        }
        #endregion
    }
}
