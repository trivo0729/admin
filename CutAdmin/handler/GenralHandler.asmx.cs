﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CommonLib.Response;
using System.Reflection;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        dbHotelhelperDataContext dbTax = new dbHotelhelperDataContext();
        //dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region SAVE TAXES
        [WebMethod(EnableSession = true)]
        public string SaveAddOn(Comm_AddOn objAddOn)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    if (objAddOn.ID == 0)
                    {
                        if (HttpContext.Current.Session["LoginUser"] == null)
                            throw new Exception("Session Expired ,Please Login and try Again");
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        Int64 Uid = objGlobalDefault.sid;
                        if (objGlobalDefault.UserType != "Supplier")
                            Uid = objGlobalDefault.ParentId;
                        if (DB.Comm_AddOns.Where(d => d.AddOnName == objAddOn.AddOnName && d.UserId == Uid).ToList().Count != 0)
                            return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add this." });
                        objAddOn.UserId = Uid;
                        DB.Comm_AddOns.InsertOnSubmit(objAddOn);
                        DB.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                    else
                    {
                        var arrTax = DB.Comm_AddOns.Where(d => d.ID == objAddOn.ID).FirstOrDefault();
                        arrTax.AddOnName = objAddOn.AddOnName;
                        arrTax.Details = objAddOn.Details;
                        arrTax.Type = objAddOn.Type;
                        arrTax.IsMeal = objAddOn.IsMeal;
                        arrTax.Activate = objAddOn.Activate;
                        DB.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string DeleteAddOns(Int64 ID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var arrAddOns = DB.Comm_AddOns.Where(d => d.ID == ID).FirstOrDefault();
                    DB.Comm_AddOns.DeleteOnSubmit(arrAddOns);
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadAddOns()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                if (objGlobalDefault.UserType != "Supplier")
                    Uid = objGlobalDefault.ParentId;
                using (var DB = new dbHotelhelperDataContext())
                {
                    var arrAddOns = (from obj in DB.Comm_AddOns
                                     where obj.UserId == Uid
                                     select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrAddOns = arrAddOns });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion

        #region Supplier Staff

        [WebMethod(EnableSession = true)]
        public string GetStaffDetails()
        {
            try
            {
                Int64 Uid = AccountManager.GetSupplierByUser();
                using ( helperDataContext DB = new helperDataContext())
                {
                 var Stafflist = (from obj in DB.tbl_StaffLogins
                                 join objc in DB.tbl_Contacts on obj.ContactID equals objc.ContactID
                                 where obj.ParentId == Uid && obj.UserType == "AdminStaff"
                                 select new
                                 {
                                     obj.ContactPerson,
                                     obj.Last_Name,
                                     obj.Designation,
                                     obj.dtLastAccess,
                                     obj.Gender,
                                     obj.uid,
                                     obj.sid,
                                     obj.Department,
                                     obj.StaffUniqueCode,
                                     obj.password,
                                     obj.LoginFlag,
                                     obj.PANNo,
                                     obj.Updatedate,
                                     objc.Address,
                                     objc.email,
                                     objc.Mobile,
                                     objc.phone

                                 }).ToList();
                Session["StafflistSession"] = ConvertToDatatable(Stafflist);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, Staff = Stafflist });
                }
               
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        #endregion

        #region ConvertToDatatable
        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        #endregion


        #region Hotel Info
        [WebMethod(EnableSession = true)]
        public string GetHotelInfo(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new dbHotelhelperDataContext())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMasters
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelLatitude,
                                            obj.HotelLangitude,
                                            obj.HotelAddress,
                                            obj.HotelZipCode,
                                            obj.HotelImage,
                                            obj.HotelCategory,
                                            obj.HotelDescription
                                        }).FirstOrDefault();
                    return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

         [WebMethod(EnableSession = true)]
         public string GetHotelImages(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new dbHotelhelperDataContext())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMasters
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelImage,
                                            obj.SubImages

                                        }).FirstOrDefault();
                    string Image = arrHotelInfo.HotelImage;
                    List<Image> arrImage = new List<Image>();
                    if (Image != null)
                    {
                        List<string> Url = Image.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    foreach (var sImage in arrHotelInfo.SubImages.Split('^'))
                    {
                        arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + sImage, Count = arrHotelInfo.SubImages.Split('^').ToList().Count });
                    }
                    return jsSerializer.Serialize(new { retCode = 1, arrImage = arrImage });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

         [WebMethod(EnableSession = true)]
         public string GetHotelInfoBySearch(string Search)
         {
             jsSerializer = new JavaScriptSerializer();
             try
             {
                 var arrHotelInfo = GenralManager.GetHotelInfo(Search);
                 return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
             }
             catch
             {
                 return jsSerializer.Serialize(new { retCode = 0, });
             }
         }
        #endregion

         #region ListtoDataTable
         public class ListtoDataTable
         {
             public DataTable ToDataTable<T>(List<T> items)
             {
                 DataTable dataTable = new DataTable(typeof(T).Name);
                 //Get all the properties by using reflection   
                 PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                 foreach (PropertyInfo prop in Props)
                 {
                     //Setting column names as Property names  
                     dataTable.Columns.Add(prop.Name);
                 }
                 foreach (T item in items)
                 {
                     var values = new object[Props.Length];
                     for (int i = 0; i < Props.Length; i++)
                     {

                         values[i] = Props[i].GetValue(item, null);
                     }
                     dataTable.Rows.Add(values);
                 }

                 return dataTable;
             }
         }
         #endregion

         
    }
}
