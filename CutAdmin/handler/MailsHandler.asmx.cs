﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for MailsHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MailsHandler : System.Web.Services.WebService
    {
        
        JavaScriptSerializer objSerialize = new JavaScriptSerializer();
        helperDataContext db = new helperDataContext();
        string jsonString = "";

        #region VisaActivityMails

        [WebMethod(EnableSession = true)]
        public string GetVisaActivityMails(string Type)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;

            var VisaMailsList = (from obj in db.tbl_ActivityMails
                                  where obj.Type == Type && obj.ParentID == 232
                                  select new
                                  {
                                      obj.Activity
                                  }).ToList();

            var MailsList = (from obj in db.tbl_ActivityMails
                             where obj.Type == Type && obj.ParentID == ParentID
                             select new
                             {
                                 obj.sid,
                                 obj.Activity,
                                 obj.CcMail,
                                 obj.Email,
                                 obj.ErroMessage
                             }).ToList();

            if (VisaMailsList.Count > 0 && VisaMailsList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = VisaMailsList, MailsList = MailsList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetOtbActivityMails(string Type)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;

            var OtbMailsList = (from obj in db.tbl_ActivityMails
                                  where obj.Type == Type && obj.ParentID == 232
                                  select new
                                  {
                                      obj.Activity
                                  }).ToList();

            var MailsList = (from obj in db.tbl_ActivityMails
                             where obj.Type == Type && obj.ParentID == ParentID
                                select new
                                {
                                    obj.sid,
                                    obj.Activity,
                                    // obj.BCcMail,
                                    obj.CcMail,
                                    obj.Email,
                                    obj.ErroMessage
                                }).ToList();

            if (OtbMailsList.Count > 0 && OtbMailsList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = OtbMailsList, MailsList = MailsList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelActivityMails(string Type)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 ParentID = objGlobalDefault.sid;
            //DBHelper.DBReturnCode retcode = VisaManager.GetVisaMails(Activity,Type, out dtResult);


            var HotelMailsList = (from obj in db.tbl_ActivityMails
                                  where obj.Type == Type && obj.ParentID == 232
                                  select new
                                  {
                                      obj.Activity
                                  }).ToList();

            var MailsList = (from obj in db.tbl_ActivityMails
                             where obj.Type == Type && obj.ParentID == ParentID
                                  select new
                                  {
                                      obj.sid,
                                      obj.Activity,
                                      //  obj.BCcMail,
                                      obj.CcMail,
                                      obj.Email,
                                      obj.ErroMessage
                                  }).ToList();

            if (HotelMailsList.Count > 0 && HotelMailsList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = HotelMailsList, MailsList = MailsList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Mails Add Update
        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail, string Message)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefault.sid;
            if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            try
            {
                using (var db = new helperDataContext())
                {
                    var sActivity = (from obj in db.tbl_ActivityMails where obj.Activity == Activity && obj.Type == Type && obj.ParentID == Uid select obj).FirstOrDefault();

                    if (sActivity == null)
                    {
                        tbl_ActivityMail activity = new tbl_ActivityMail();
                        activity.Type = Type;
                        activity.Activity = Activity;
                        activity.ParentID = objGlobalDefault.sid;
                        activity.Email = MailsId;
                        activity.CcMail = CcMails;
                        activity.BCcMail = BCcMail;
                        activity.ErroMessage = Message;
                        db.tbl_ActivityMails.InsertOnSubmit(activity);
                        db.SubmitChanges();
                    }
                    else
                    {
                        sActivity.Email = MailsId;
                        sActivity.CcMail = CcMails;
                        sActivity.BCcMail = BCcMail;
                        sActivity.ErroMessage = Message;
                        db.SubmitChanges();
                    }
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            //try
            //{
            //    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //    Int64 ParentID = objGlobalDefault.sid;
            //    var sActivity = (from obj in db.tbl_ActivityMails where obj.Activity == Activity && obj.Type == Type select obj).FirstOrDefault();
            //    sActivity.Email = MailsId;
            //    sActivity.CcMail = CcMails;
            //    sActivity.BCcMail = BCcMail;
            //    sActivity.ErroMessage = Message;
            //    sActivity.ParentID = ParentID;
            //    db.SubmitChanges();
            //    return objSerialize.Serialize(new { retCode = 1 });
            //}
            //catch
            //{
            //    return objSerialize.Serialize(new { retCode = 0 });

            //}
        }
        #endregion

    }
}
