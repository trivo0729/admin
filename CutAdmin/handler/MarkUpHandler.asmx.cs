﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.dbml;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for MarkUpHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MarkUpHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
         static helperDataContext db = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";


        [WebMethod(EnableSession = true)]
        public string LoadMarkUp()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                using (var dbTax = new helperDataContext())
                {
                    if (objGlobalDefault.UserType != "Supplier")
                    {
                        Uid = objGlobalDefault.ParentId;
                        var List = (from obj in dbTax.tbl_AgentMarkups where obj.uid == Uid select obj).ToList();

                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AgentMarkUps = List });
                    }
                    else if (objGlobalDefault.UserType == "Supplier")
                    {
                        var List = (from obj in dbTax.tbl_GlobalMarkups  select obj).ToList();

                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, AgentMarkUps = List });
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        #region Group Markup
        [WebMethod(EnableSession = true)]
        public string GetGroup()
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                List<string> ListDetail = new List<string>();
                using (var db = new helperDataContext())
                {
                    if (objGlobalDefault.UserType == "Admin")
                    {
                        var List = (from obj in db.tbl_GroupMarkups select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                    if (objGlobalDefault.UserType == "AdminStaff")
                    {
                        var List = (from obj in db.tbl_GroupMarkups  select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetGroupMarkup(Int64 Id)
        {
            try
            {
                var arrMarkups = MarkupManger.GetService();
                List<Service> arrService = new List<Service>();
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrMarkups)
                    {
                        var arrSupplier = new List<Supplier>();
                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GroupMarkupDetails
                                             where obj.Type == Convert.ToInt64(arrMarkups.IndexOf(objService) + 1)
                                             && obj.GroupId == Id && obj.Supplier == objSupplier.Name
                                             select new Supplier
                                             {
                                                 Name = obj.Supplier,
                                                 MarkupAmt = Convert.ToDecimal(obj.MarkupAmmount),
                                                 MarkupPer = Convert.ToDecimal(obj.MarkupPercentage),
                                                 CommAmt = Convert.ToDecimal(obj.CommessionAmmount),
                                                 CommPer = Convert.ToDecimal(obj.CommessionPercentage),
                                                 MarkupID = obj.sid
                                             }).FirstOrDefault();
                            if (arrMarkup == null)
                            {
                                arrSupplier.Add(new Supplier
                                {
                                    Name = objSupplier.Name,
                                    MarkupAmt = 0,
                                    MarkupPer = 0,
                                    CommAmt = 0,
                                    CommPer = 0,
                                    MarkupID = 0
                                });
                            }
                            else
                            {
                                arrSupplier.Add(arrMarkup);
                            }

                        }
                        arrService.Add(new Service { Name = objService.Name, ListSupplier = arrSupplier });
                    }
                    json = jsSerializer.Serialize(new { retCode = 1, arrDetails = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveGroupMarkup(List<Service> arrService, Int64 GroupId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrService)
                    {

                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GroupMarkupDetails
                                             where obj.Type == Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                                 && obj.Supplier == objSupplier.Name && obj.GroupId == GroupId
                                             select obj).FirstOrDefault();
                            if (arrMarkup != null)
                            {
                                arrMarkup.MarkupAmmount = objSupplier.MarkupAmt;
                                arrMarkup.MarkupPercentage = objSupplier.MarkupPer;
                                arrMarkup.CommessionAmmount = objSupplier.CommAmt;
                                arrMarkup.CommessionPercentage = objSupplier.CommPer;
                                db.SubmitChanges();
                            }
                            else
                            {
                                arrMarkup = new dbml.tbl_GroupMarkupDetail
                                {
                                    GroupId = GroupId,
                                    MarkupAmmount = objSupplier.MarkupAmt,
                                    MarkupPercentage = objSupplier.MarkupPer,
                                    //ParentID = 0,
                                    Supplier = objSupplier.Name,
                                    TaxApplicable = false,
                                    CommessionPercentage = objSupplier.CommPer,
                                    CommessionAmmount = objSupplier.CommAmt,
                                    Type = Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                };
                                db.tbl_GroupMarkupDetails.InsertOnSubmit(arrMarkup);
                                db.SubmitChanges();

                            }

                        }

                    }
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }


        //****************************************** Popup Code ************************************************************

        [WebMethod(EnableSession = true)]
        public string GetGuropName(string GetgName)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                List<string> ListDetail = new List<string>();
                using (var db = new helperDataContext())
                {
                    if (objGlobalDefault.UserType == "Admin")
                    {
                        var List = (from obj in db.tbl_GroupMarkups where obj.GroupName == GetgName select obj).ToList();

                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, GetgnameArr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                    if (objGlobalDefault.UserType == "AdminStaff")
                    {
                        var List = (from obj in db.tbl_GroupMarkups where  obj.GroupName == GetgName select obj).ToList();
                        if (List.Any())
                        {
                            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, GetgnameArr = List });
                        }
                        else
                        {
                            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                        }
                    }
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string AddGroup(string GroupName)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                using (var db = new helperDataContext())
                {

                    CutAdmin.dbml.tbl_GroupMarkup obj = new CutAdmin.dbml.tbl_GroupMarkup();
                    obj.GroupName = GroupName;
                    obj.GroupId = "0";
                    obj.ParentID = AccountManager.GetSupplierByUser();
                    db.tbl_GroupMarkups.InsertOnSubmit(obj);
                    db.SubmitChanges();
                    return jsSerializer.Serialize(new { retCode = 1, ID = obj.sid });
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        #endregion


        #region Individual Markup
        [WebMethod(EnableSession = true)]
        public string GetIndividualMarkup(Int64 sid)
        {
            try
            {
                var arrMarkups = MarkupManger.GetService();
                List<Service> arrService = new List<Service>();
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrMarkups)
                    {
                        var arrSupplier = new List<Supplier>();
                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_IndividualMarkups
                                             where obj.Type == Convert.ToInt64(arrMarkups.IndexOf(objService) + 1)
                                              && obj.Supplier == objSupplier.Name && obj.AgentId == sid
                                             select new Supplier
                                             {
                                                 Name = obj.Supplier,
                                                 MarkupAmt = Convert.ToDecimal(obj.MarkupAmmount),
                                                 MarkupPer = Convert.ToDecimal(obj.MarkupPercentage),
                                                 CommAmt = Convert.ToDecimal(obj.CommessionAmmount),
                                                 CommPer = Convert.ToDecimal(obj.CommessionPercentage),
                                                 MarkupID = obj.sid
                                             }).FirstOrDefault();
                            if (arrMarkup == null)
                            {
                                arrSupplier.Add(new Supplier
                                {
                                    Name = objSupplier.Name,
                                    MarkupAmt = 0,
                                    MarkupPer = 0,
                                    CommAmt = 0,
                                    CommPer = 0,
                                    MarkupID = 0
                                });
                            }
                            else
                            {
                                arrSupplier.Add(arrMarkup);
                            }

                        }
                        arrService.Add(new Service { Name = objService.Name, ListSupplier = arrSupplier });
                    }
                    json = jsSerializer.Serialize(new { retCode = 1, arrDetails = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveIndividual(List<Service> arrService, Int64 AgentId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrService)
                    {

                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_IndividualMarkups
                                             where obj.Type == Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                                 && obj.Supplier == objSupplier.Name && obj.AgentId == AgentId
                                             select obj).FirstOrDefault();
                            if (arrMarkup != null)
                            {
                                arrMarkup.MarkupAmmount = objSupplier.MarkupAmt;
                                arrMarkup.MarkupPercentage = objSupplier.MarkupPer;
                                arrMarkup.CommessionAmmount = objSupplier.CommAmt;
                                arrMarkup.CommessionPercentage = objSupplier.CommPer;
                                db.SubmitChanges();
                            }
                            else
                            {
                                arrMarkup = new dbml.tbl_IndividualMarkup
                                {
                                    AgentId = AgentId,
                                    MarkupAmmount = objSupplier.MarkupAmt,
                                    MarkupPercentage = objSupplier.MarkupPer,
                                    Supplier = objSupplier.Name,
                                    CommessionPercentage = objSupplier.CommPer,
                                    CommessionAmmount = objSupplier.CommAmt,
                                    Type = Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                };
                                db.tbl_IndividualMarkups.InsertOnSubmit(arrMarkup);
                                db.SubmitChanges();

                            }

                        }

                    }
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        [WebMethod(EnableSession = true)]
        public string UpdateIndividualMarkupDetails(Int64 Sid, Int64 IndMarkPercentage, Int64 IndMarkUpAmount, Int64 IndCommPercentage, Int64 IndCommAmount)
        {
            try
            {
                CutAdmin.dbml.tbl_IndividualMarkup Det = DB.tbl_IndividualMarkups.Where(d => d.AgentId == Sid).FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = IndMarkPercentage;
                Det.MarkupAmmount = IndMarkUpAmount;
                Det.CommessionAmmount = IndCommAmount;
                Det.CommessionPercentage = IndCommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


        #region Global Markup
        [WebMethod(EnableSession = true)]
        public string GetGlobalMarkup()
        {
            try
            {
                List<Service> arrService = new List<Service>();
                var arrMarkups = MarkupManger.GetService();
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrMarkups)
                    {
                        var arrSupplier = (from obj in db.tbl_GlobalMarkups
                                           where obj.Type == Convert.ToInt64(arrMarkups.IndexOf(objService) + 1)
                                           select new Supplier
                                           {
                                               Name = obj.Supplier,
                                               MarkupAmt = Convert.ToDecimal(obj.MarkupAmmount),
                                               MarkupPer = Convert.ToDecimal(obj.MarkupPercentage),
                                               CommAmt = Convert.ToDecimal(obj.CommessionAmmount),
                                               CommPer = Convert.ToDecimal(obj.CommessionPercentage),
                                               MarkupID = obj.sid
                                           }).ToList();
                        arrService.Add(new Service { Name = objService.Name, ListSupplier = arrSupplier });
                    }
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveGlobalMarkup(List<Service> arrService)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrService)
                    {

                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrMarkup = (from obj in db.tbl_GlobalMarkups
                                             where obj.Type == Convert.ToInt64(arrService.IndexOf(objService) + 1)
                                                 && obj.Supplier == objSupplier.Name
                                             select obj).FirstOrDefault();
                            arrMarkup.MarkupAmmount = objSupplier.MarkupAmt;
                            arrMarkup.MarkupPercentage = objSupplier.MarkupPer;
                            arrMarkup.CommessionAmmount = objSupplier.CommAmt;
                            arrMarkup.CommessionPercentage = objSupplier.CommPer;
                            db.SubmitChanges();
                        }

                    }
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateGlobalMarkupDetails(Int64 MarkPercentage, Int64 MarkUpAmount, Int64 CommPercentage, Int64 CommAmount)
        {
            try
            {
                CutAdmin.dbml.tbl_GlobalMarkup Det = DB.tbl_GlobalMarkups.FirstOrDefault();

                Det.Type = 1;
                Det.MarkupPercentage = MarkPercentage;
                Det.MarkupAmmount = MarkUpAmount;
                Det.CommessionAmmount = CommAmount;
                Det.CommessionPercentage = CommPercentage;
                DB.SubmitChanges();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });

            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAgentGroup(Int64 AgentId)
        {
            try
            {
                var List = (from obj in DB.tbl_AgentGroupMarkupMappings where obj.AgentId == AgentId select obj).ToList();
                if (List.Any())
                {
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        #endregion


    }
}
