﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for OTBHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OTBHandler : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region OTB List
        [WebMethod(EnableSession = true)]
        public string GetAllOTBDetails()
        {
            string jsonString = "";
            List<string> NoPax = new List<string>();
            List<string> RefNo = new List<string>();
            DataSet dsResult = new DataSet();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOtbList(out dsResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                //for (int i = 0; i < dtResult.Rows.Count; i++)
                //{
                //    RefNo.Add(dtResult.Rows[i]["OTBNo"].ToString());
                //}
                //NoPax = Agent.DataLayer.OTBManager.GetOtbPaxNo(RefNo);
                //dtResult.Columns.Add("num_Pax", typeof(string));
                //string Converted = "";
                //for (int i = 0; i < NoPax.Count; i++)
                //{
                //    Converted = Convert.ToString(NoPax[i]);
                //    dtResult.Rows[i]["num_Pax"] = Converted;
                //}
                dtResult = OTBManager.GetOtbPaxNo(dsResult);
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_OTB\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Airports List
        [WebMethod(EnableSession = true)]
        public string GetAirports(string Airlines)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetAirports(Airlines, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Airports\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Get Update Mails
        [WebMethod(EnableSession = true)]
        public string GetOtbMails(string Airlines)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOtbMails(Airlines, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Airports\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string UpdateOTBMails(string Airlines, string Mails, string CCMails)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.UpdateOtbMails(Airlines, Mails, CCMails);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region OtbCharges
        [WebMethod(EnableSession = true)]
        public string GetOTBCharges(string Airlines, string From)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOtbCharges(Airlines, From, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_OTB\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Otb Search
        [WebMethod(EnableSession = true)]
        public string SearchOTB(string dteTo, string dteFrom, string dteTravel, string FirstName, string LastName, string VisaType, string AirLine, string PnrNo, string uid)
        {
            DataTable dtResult;
            string jsonString;
            DataRow[] rows;
            DataSet dsResult = new System.Data.DataSet();
            //DBHelper.DBReturnCode retCode = OTBAdminManager.SerchOTBAll(dteTo, dteFrom, dteTravel, FirstName, LastName, VisaType, AirLine, PnrNo, uid, out dtResult);
            DBHelper.DBReturnCode retCode = OTBAdminManager.GetOtbList(out dsResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                dtResult = dsResult.Tables[0];
                if (dteTo != "" && dteTo != "")
                {

                    DateTime to = ConvertDateTime(dteTo);
                    DateTime from = ConvertDateTime(dteFrom);
                    dtResult.Columns.Add("Date", typeof(DateTime));
                    string Chk_Dt = "";
                    DateTime Chk_Dt_Converted = DateTime.Now;
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        Chk_Dt = (dtResult.Rows[i]["IssueDate"]).ToString();
                        Chk_Dt_Converted = ConvertDateTime(Chk_Dt);
                        dtResult.Rows[i]["Date"] = Chk_Dt_Converted;
                    }
                    rows = dtResult.Select("(Date  >= '" + to + "' AND Date <= '" + from + "')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                else if (dteTravel != "")
                {
                    DateTime TravelDate = ConvertDateTime(dteTravel);
                    DateTime from = ConvertDateTime(dteFrom);
                    dtResult.Columns.Add("Date", typeof(DateTime));
                    string Chk_Dt = "";
                    DateTime Chk_Dt_Converted = DateTime.Now;
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        Chk_Dt = (dtResult.Rows[i]["IssueDate"]).ToString();
                        Chk_Dt_Converted = ConvertDateTime(Chk_Dt);
                        dtResult.Rows[i]["Date"] = Chk_Dt_Converted;
                    }
                    rows = dtResult.Select("(Date= '" + TravelDate + "')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (FirstName != "")
                {
                    rows = dtResult.Select("(Name like '%" + FirstName + "%')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (LastName != "")
                {
                    rows = dtResult.Select("(LastName like '%" + LastName + "%')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (AirLine != "-Select AirLine-")
                {
                    rows = dtResult.Select("(In_AirLine like '%" + AirLine + "%')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (VisaType != "--Salect Visa Type--")
                {
                    rows = dtResult.Select("(VisaType like '%" + VisaType + "%')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (PnrNo != "")
                {
                    rows = dtResult.Select("(In_PNR like '%" + PnrNo + "%')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (uid != "")
                {
                    rows = dtResult.Select("(uid = '" + Convert.ToInt64(uid) + "')");
                    //rows = dtResult.Select("( Date>=" + From + " and Date<=" + To + " and AgencyName=" + AgentId + ")");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    dtResult = rows.CopyToDataTable();
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }

                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_OTB\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }
        //#region Search Otb
        [WebMethod(EnableSession = true)]
        public string GetOTBByRefrence(string RefrenceNo)
        {
            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOTBbyCode(RefrenceNo, Uid, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("OTBNo"),
                    value = data.Field<String>("OTBNo")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetOTBByVisaNo(string VisaNo)
        {
            string jsonString = "";
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefaults.sid;
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOTBbyVisaNo(VisaNo, Uid, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("VisaNo"),
                    value = data.Field<String>("VisaNo")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetOTBByNameOnAdmin(string FirstName)
        {
            string jsonString = "";
            DataTable dtResult;
            //GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOTBbyName(FirstName, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Name"),
                    value = data.Field<String>("Name")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetOTBByLastName(string LastName)
        {
            string jsonString = "";
            DataTable dtResult;
            //GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOTBbyLastName(LastName, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("LastName"),
                    value = data.Field<String>("LastName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetOTBByPnrNo(string PnrNo)
        {
            string jsonString = "";
            DataTable dtResult;
            //GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //Int64 Uid = objGlobalDefaults.sid;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetOTBbyPnrNo(PnrNo, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("In_PNR"),
                    value = data.Field<String>("In_PNR")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetOTBByAgencyName(string AgencyName)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetAgencyName(AgencyName, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("sid"),
                    value = data.Field<String>("AgencyName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        #endregion

        #region Update Otb Charges
        [WebMethod(EnableSession = true)]
        public string UpdateOTBCharges(string Airlines, string From, string Currency, decimal OtbFee, decimal UrgentFee)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.UpdateOtbCharges(Airlines, From, Currency, OtbFee, UrgentFee);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Otb Call Center
        #region Get Visa By OTB
        [WebMethod(EnableSession = true)]
        public string GetVisaByOTB(Int64 AgentId)
        {
            string jsonString = "";
            string OtbRefNo = "";
            if (Session["OtbRefNo"] == null)
            {
                OtbRefNo = "OTB-" + GenerateRandomNumber();
                Session["OtbRefNo"] = OtbRefNo;
            }
            else
            {
                OtbRefNo = Session["OtbRefNo"].ToString();
            }

            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBAdminManager.GetVisaByOTB(AgentId, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Visa\":[" + jsonString.Trim(',') + "],\"OtbRefNo\":\"" + OtbRefNo + "\"}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\",\"OtbRefNo\":\"" + OtbRefNo + "\"}";
            }
            return jsonString;
        }
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
        #endregion

        #region Otb Charges
        [WebMethod(EnableSession = true)]
        public string GetAirlinesOtbCharges(Int64 AgentId, string Airline, string Airports, string[] RefrenceNo, string OtbType, string Supplier)
        {
            string jsonString = "";
            DataSet dsResult;
            Int64 UserId = AgentId;
            string AgentCurrency = "";
            if (Airline != "EK" || Airline != "9w")
            {
                DataSet dtResult;
                DataTable dtCurrency = new DataTable(), dtAgentCurrency = new DataTable(), dtOtb = new DataTable();
                DBHelper.DBReturnCode retcode = OTBAdminManager.GetOtbAgentCharges(AgentId, Airline, Airports, out dtResult);
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    jsonString = "";
                    dtOtb = dtResult.Tables[0];
                    AgentCurrency = dtResult.Tables[1].Rows[0]["CurrencyCode"].ToString();
                    string Currency = dtOtb.Rows[0]["Currency"].ToString();
                    float Charges = Convert.ToSingle(dtOtb.Rows[0]["Charge"]);
                    float Urgent = Convert.ToSingle(dtOtb.Rows[0]["UrgentChg"]);
                    string Required = dtOtb.Rows[0]["Required"].ToString();
                    string EmailFormat = dtOtb.Rows[0]["EmailFormat"].ToString();
                    string Email1 = dtOtb.Rows[0]["Email1"].ToString();
                    string CCEmailId = dtOtb.Rows[0]["CCEmailId"].ToString();
                    float OtherCarges = Convert.ToSingle(Charges) * Convert.ToSingle(14.5) / 100;
                    string Email2 = dtOtb.Rows[0]["Email2"].ToString();
                    float InrCurrency = 0, Ammount = 0;
                    float TotalAmt = 0;
                    if (Currency != AgentCurrency)
                    {

                        if (Currency != "INR")
                        {
                            CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(Currency, out dtCurrency);
                            InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                            Ammount = Charges * InrCurrency;
                            if (AgentCurrency != "INR")
                            {
                                CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                TotalAmt = Ammount / InrCurrency;
                            }
                            else
                            {
                                TotalAmt = Ammount;
                            }

                        }
                        else
                        {
                            if (AgentCurrency != "INR")
                            {
                                 CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                TotalAmt = Charges / InrCurrency;
                            }
                            else
                            {
                                TotalAmt = Charges;
                            }

                        }
                        float UrgentCharges = 0;
                        float VisaCharges = TotalAmt;

                        if (OtbType == "Normal")
                        {
                            UrgentCharges = 0;
                        }
                        else
                        {
                            if (Currency != "INR")
                            {
                                CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(Currency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                Ammount = 500 * InrCurrency;
                                if (AgentCurrency != "INR")
                                {
                                    CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                    InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                    TotalAmt = Ammount / InrCurrency;
                                }
                                else
                                {
                                    TotalAmt = 500;
                                }

                            }
                            else
                            {
                                if (AgentCurrency != "INR")
                                {
                                    CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                    InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                    TotalAmt = 500 / InrCurrency;
                                }
                                else
                                {
                                    UrgentCharges = 500;
                                }

                            }

                        }
                        DBHelper.DBReturnCode retcode1 = OTBAdminManager.GetAgentOtbMarkups(UserId, Supplier, out dsResult);
                        if (retcode1 == DBHelper.DBReturnCode.SUCCESS)
                        {

                            DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentMarkup;
                            dtIndividualMarkup = dsResult.Tables[2];
                            dtGroupMarkup = dsResult.Tables[1];
                            dtGlobalMarkup = dsResult.Tables[0];
                            dtAgentMarkup = dsResult.Tables[3];
                            decimal IndMarkuper, IndMarkAmt, IndCommper, ActualMarkupper, ActualCommission, IndCommAmt, IndMarkTotal, TotalMarkup;
                            decimal GroupMarkuper, GroupMarkAmt, GroupCommper, GroupCommAmt;

                            if (dtIndividualMarkup.Rows.Count != 0)
                            {

                                IndMarkuper = 0;
                                IndMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                IndCommper = 0;
                                IndCommAmt = 0;
                            }
                            else
                            {
                                IndMarkuper = 0;
                                IndMarkAmt = 0;
                                IndCommper = 0;
                                IndCommAmt = 0;
                            }
                            ActualMarkupper = (IndMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            ActualCommission = (IndCommper / 100) * Convert.ToDecimal(TotalAmt);
                            //IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt) - Getgreatervalue(ActualCommission, IndCommAmt);
                            IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt);
                            if (dtGroupMarkup.Rows.Count != 0)
                            {
                                if (Convert.ToString(dtGroupMarkup.Rows[0]["MarkupPercentage"]) != "")
                                {
                                    GroupMarkuper = Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupPercentage"]); ;
                                }
                                else
                                {
                                    GroupMarkuper = 0;
                                }

                                GroupMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                GroupCommper = 0;
                                GroupCommAmt = 0;

                            }
                            else
                            {
                                GroupMarkuper = 0;
                                GroupMarkAmt = 0;
                                GroupCommper = 0;
                                GroupCommAmt = 0;
                            }

                            decimal ActualGroupMarkupper = (GroupMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGroupCommission = (GroupCommper / 100) * Convert.ToDecimal(TotalAmt);
                            //decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt) - Getgreatervalue(ActualGroupCommission, GroupCommAmt);
                            decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt);
                            decimal GlobalMarkuper, GlobalMarkAmt, GlobalCommper, GlobalCommAmt;

                            if (dtGlobalMarkup.Rows.Count != 0)
                            {
                                GlobalMarkuper = 0;
                                GlobalMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                GlobalCommper = 0;
                                GlobalCommAmt = 0;

                            }
                            else
                            {
                                GlobalMarkuper = 0;
                                GlobalMarkAmt = 0;
                                GlobalCommper = 0;
                                GlobalCommAmt = 0;
                            }
                            decimal ActualGlobalMarkupper = (GlobalMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGlobalCommission = (GlobalCommper / 100) * Convert.ToDecimal(TotalAmt);
                            //decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt) - Getgreatervalue(ActualGlobalCommission, GlobalMarkAmt);
                            decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt);
                            decimal AgentMarkupper, AgentMarkupAmt, AgentperAmt, ActualAgentMarkup;
                            if (dtAgentMarkup.Rows.Count != 0)
                            {
                                AgentMarkupper = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Percentage"]);
                                AgentMarkupAmt = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Amount"]);
                            }
                            else
                            {
                                AgentMarkupper = 0;
                                AgentMarkupAmt = 0;

                            }
                            AgentperAmt = 0;
                            ActualAgentMarkup = 0;
                            TotalMarkup = IndMarkTotal + ActualGroupMarkTotal + ActualGlobalMarkTotal + 0;
                            decimal OtbCharges = Convert.ToDecimal(VisaCharges);
                            OtherCarges = Convert.ToSingle(TotalMarkup);
                            decimal OtherFee = Convert.ToDecimal(OtherCarges);
                            decimal UrgentFee = Convert.ToDecimal(UrgentCharges);
                            decimal ServiceTax = 0;
                            decimal ActualAmmount = OtbCharges + OtherFee + UrgentFee + ServiceTax;
                            decimal Chargesper = Convert.ToDecimal(VisaCharges) + UrgentFee + Convert.ToDecimal(OtherFee);
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaCharges\":\"" + String.Format("{0:F4}", OtbCharges) + "\",\"UrgentCharges\":\"" + String.Format("{0:F4}", UrgentFee) + "\",\"OtherCarges\":\"" + String.Format("{0:F4}", OtherFee) + "\",\"Chargesper\":\"" + String.Format("{0:F4}", Chargesper) + "\",\"EmailFormat\":\"" + EmailFormat + "\",\"Email1\":\"" + Email1 + "\",\"CCEmailId\":\"" + CCEmailId + "\",\"Email2\":\"" + Email2 + "\",\"Emirates\":\"0\",\"AgentCurrency\":\"" + AgentCurrency + "\"}";
                            dtResult.Dispose();
                            return jsonString;

                        }
                        else
                        {
                            float Chargesper = VisaCharges + UrgentCharges + OtherCarges;
                            OtherCarges = 0;
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaCharges\":\"" + String.Format("{0:F4}", VisaCharges) + "\",\"UrgentCharges\":\"" + String.Format("{0:F4}", UrgentCharges) + "\",\"OtherCarges\":\"" + String.Format("{0:F4}", OtherCarges) + "\",\"Chargesper\":\"" + String.Format("{0:F4}", Chargesper) + "\",\"EmailFormat\":\"" + EmailFormat + "\",\"Email1\":\"" + Email1 + "\",\"CCEmailId\":\"" + CCEmailId + "\",\"Email2\":\"" + Email2 + "\",\"Emirates\":\"0\",\"AgentCurrency\":\"" + AgentCurrency + "\"}";
                            dtResult.Dispose();
                        }

                    }
                    else if (Currency == AgentCurrency)
                    {
                        if (Currency != "INR")
                        {
                            CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(Currency, out dtCurrency);
                            InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                            Ammount = Charges * InrCurrency;
                            if (AgentCurrency != "INR")
                            {
                                CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                TotalAmt = Ammount / InrCurrency;
                            }
                            else
                            {
                                TotalAmt = Ammount;
                            }

                        }
                        else
                        {
                            if (AgentCurrency != "INR")
                            {
                                CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                TotalAmt = Ammount / InrCurrency;
                            }
                            else
                            {
                                TotalAmt = Convert.ToSingle(Charges);
                            }

                        }
                        if (OtbType == "Normal")
                        {
                            Urgent = 0;
                        }
                        else
                        {
                            if (Currency != "INR")
                            {
                               CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(Currency, out dtCurrency);
                                InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                Ammount = 500 * InrCurrency;
                                if (AgentCurrency != "INR")
                                {
                                    CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                    InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                    TotalAmt = Ammount / InrCurrency;
                                }
                                else
                                {
                                    TotalAmt = 500;
                                }

                            }
                            else
                            {
                                if (AgentCurrency != "INR")
                                {
                                    CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                                    InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
                                    TotalAmt = 500 / InrCurrency;
                                }
                                else
                                {
                                    Urgent = Convert.ToSingle(500);
                                }

                            }
                        }
                        DBHelper.DBReturnCode retcode1 = OTBAdminManager.GetAgentOtbMarkups(UserId, Supplier, out dsResult);
                        if (retcode1 == DBHelper.DBReturnCode.SUCCESS)
                        {

                            DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentMarkup;
                            dtIndividualMarkup = dsResult.Tables[2];
                            dtGroupMarkup = dsResult.Tables[1];
                            dtGlobalMarkup = dsResult.Tables[0];
                            dtAgentMarkup = dsResult.Tables[3];
                            decimal IndMarkuper, IndMarkAmt, IndCommper, ActualMarkupper, ActualCommission, IndCommAmt, IndMarkTotal, TotalMarkup;
                            decimal GroupMarkuper, GroupMarkAmt, GroupCommper, GroupCommAmt;
                            if (dtIndividualMarkup.Rows.Count != 0)
                            {
                                IndMarkuper = 0;
                                IndMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                IndCommper = 0;
                                IndCommAmt = 0;
                            }
                            else
                            {
                                IndMarkuper = 0;
                                IndMarkAmt = 0;
                                IndCommper = 0;
                                IndCommAmt = 0;
                            }
                            ActualMarkupper = (IndMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            ActualCommission = (IndCommper / 100) * Convert.ToDecimal(TotalAmt);
                            IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt) - Getgreatervalue(ActualCommission, IndCommAmt);
                            if (dtGroupMarkup.Rows.Count != 0)
                            {
                                GroupMarkuper = 0;
                                GroupMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                GroupCommper = 0;
                                GroupCommAmt = 0;

                            }
                            else
                            {
                                GroupMarkuper = 0;
                                GroupMarkAmt = 0;
                                GroupCommper = 0;
                                GroupCommAmt = 0;
                            }

                            decimal ActualGroupMarkupper = (GroupMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGroupCommission = (GroupCommper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt) - Getgreatervalue(ActualGroupCommission, GroupCommAmt);
                            decimal GlobalMarkuper, GlobalMarkAmt, GlobalCommper, GlobalCommAmt;

                            if (dtGlobalMarkup.Rows.Count != 0)
                            {
                                GlobalMarkuper = 0;
                                GlobalMarkAmt = GetMarkpAmmount(Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupAmmount"]), Currency, AgentCurrency);
                                GlobalCommper = 0;
                                GlobalCommAmt = 0;

                            }
                            else
                            {
                                GlobalMarkuper = 0;
                                GlobalMarkAmt = 0;
                                GlobalCommper = 0;
                                GlobalCommAmt = 0;
                            }
                            decimal ActualGlobalMarkupper = (GlobalMarkuper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGlobalCommission = (GlobalCommper / 100) * Convert.ToDecimal(TotalAmt);
                            decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt) - Getgreatervalue(ActualGlobalCommission, GlobalCommAmt);
                            decimal AgentMarkupper, AgentMarkupAmt, AgentperAmt, ActualAgentMarkup;
                            if (dtAgentMarkup.Rows.Count != 0)
                            {
                                AgentMarkupper = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Percentage"]);
                                AgentMarkupAmt = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Amount"]);
                            }
                            else
                            {
                                AgentMarkupper = 0;
                                AgentMarkupAmt = 0;

                            }
                            AgentperAmt = 0;
                            ActualAgentMarkup = 0;
                            TotalMarkup = IndMarkTotal + ActualGroupMarkTotal + ActualGlobalMarkTotal + 0;
                            decimal OtbCharges = Convert.ToDecimal(TotalMarkup);
                            OtherCarges = Convert.ToSingle(TotalMarkup);
                            decimal OtherFee = Convert.ToDecimal(OtherCarges);
                            decimal UrgentFee = Convert.ToDecimal(Urgent);
                            decimal ServiceTax = 0;
                            decimal ActualAmmount = OtbCharges + OtherFee + UrgentFee + ServiceTax;
                            decimal Chargesper = Convert.ToDecimal(Charges) + UrgentFee + Convert.ToDecimal(OtherFee);
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaCharges\":\"" + String.Format("{0:F4}", Charges) + "\",\"UrgentCharges\":\"" + String.Format("{0:F4}", UrgentFee) + "\",\"OtherCarges\":\"" + String.Format("{0:F4}", OtherFee) + "\",\"Chargesper\":\"" + String.Format("{0:F4}", Chargesper) + "\",\"EmailFormat\":\"" + EmailFormat + "\",\"Email1\":\"" + Email1 + "\",\"CCEmailId\":\"" + CCEmailId + "\",\"Email2\":\"" + Email2 + "\",\"Emirates\":\"0\",\"AgentCurrency\":\"" + AgentCurrency + "\"}";
                            dtResult.Dispose();
                            return jsonString;

                        }
                        else
                        {
                            float Chargesper = Charges + Urgent + OtherCarges;
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaCharges\":\"" + String.Format("{0:F4}", Charges) + "\",\"UrgentCharges\":\"" + String.Format("{0:F4}", Urgent) + "\",\"OtherCarges\":\"" + String.Format("{0:F4}", OtherCarges) + "\",\"Chargesper\":\"" + String.Format("{0:F4}", Chargesper) + "\",\"EmailFormat\":\"" + EmailFormat + "\",\"Email1\":\"" + Email1 + "\",\"CCEmailId\":\"" + CCEmailId + "\",\"Email2\":\"" + Email2 + "\",\"Emirates\":\"0\",\"AgentCurrency\":\"" + AgentCurrency + "\"}";
                            dtResult.Dispose();
                        }

                    }



                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\",\"Emirates\":\"0\"}";
                }

            }
            else
            {
                if (RefrenceNo != null)
                {
                    DBHelper.DBReturnCode retcode = OTBAdminManager.VisaForEmirates(RefrenceNo, Airline);
                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Emirates\":\"1\"}";
                    }
                    else
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Emirates\":\"1\"}";
                    }

                }

            }

            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public static Decimal GetMarkpAmmount(decimal Amount, string Currency, string AgentCurrency)
        {
            decimal MarkupAmount = 0, InrCurrency = 0;
            DataTable dtCurrency;
            if (AgentCurrency != "INR")
            {
               CutAdmin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(AgentCurrency, out dtCurrency);
                InrCurrency = Convert.ToDecimal(dtCurrency.Rows[0]["INR"]);
                MarkupAmount = Amount / InrCurrency;
            }
            else
            {
                MarkupAmount = Amount;
            }

            return MarkupAmount;
        }
        [WebMethod(EnableSession = true)]
        public static Decimal Getgreatervalue(decimal Percentage, decimal Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }
        #endregion

        #region Apply For Otb

        [WebMethod(EnableSession = true)]
        public string ApplyOtbCut(Int64 Uid, string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] RefNo, string sRdbType, string EmailFormat, string Email1, string Email2, string CCEmailId, string OtbRefNo)
        {
            float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
            float AgentOtbMarkup = 0;
            float OtbCharges = TotalPerPass - AgentOtbMarkup;
            float otbTotalAmmount = OtbCharges * PaxNo;
            float ServiceCharges = OtbCharges - VisaFee;
            DBHelper.DBReturnCode retCode = OTBAdminManager.ApllyOtb(Uid, ArrivalAirLine, DepartingAirline, Arrivalfrom, DepartingFrom, DepartingDate, ArrivalDate, ArrivingFlightNo, DerartingFlightNo, ArrivingPnr, DerartingPnr, DepartingTicketCopy, VisaFee, UrgentCharges, ServiceCharges, OtbCharges, PaxNo, otbTotalAmmount, RefNo, sRdbType, OtbRefNo, Email1, Email2, CCEmailId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS || retCode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
            {
                Session["OtbRefNo"] = null;
                InvoiceMailManager.Status = "Requested";
                InvoiceMailManager.Service = "OTB";
                InvoiceMailManager.Invoice = OtbRefNo;
                var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                bool MailSent = InvoiceMailManager.SendInvoiceMail();
                Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                retCode = OTBEmailManager.AirlinesOtbMail(OtbRefNo, Uid);
                if (retCode == BL.DBHelper.DBReturnCode.EXCEPTION)
                {
                    retCode = OTBManager.IsMailNotSend(OtbRefNo);
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"1\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"0\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }



        }
        [WebMethod(EnableSession = true)]
        public string ApplyOtbOther(Int64 Uid, string ArrivalAirLine, string DepartingAirline, string Arrivalfrom, string DepartingFrom, string DepartingDate, string ArrivalDate, string ArrivingFlightNo, string DerartingFlightNo, string ArrivingPnr, string DerartingPnr, string DepartingTicketCopy, float VisaFee, float UrgentCharges, float OtherCharges, float TotalPerPass, Int64 PaxNo, float TotalAmmount, string[] FirstName, string[] LastName, string[] VisaType, string[] VisaNo, string[] Issue, string[] PassportNo, string[] VisaCopy, string sRdbType, string otbType, string EmailFormat, string Email1, string Email2, string CCEmailId, string[] Sponsor, string[] SponsorNo, string OtbRefNo, string[] DOB, string[] OtherPassportNo, string[] PlaceIssue, string[] IssueCountry, string[] ContactNo)
        {
            float BookingAmtWithTax = Convert.ToSingle(TotalAmmount);
            float OtbCharges = TotalPerPass;
            float otbTotalAmmount = OtbCharges * PaxNo;
            float ServiceCharges = OtbCharges - VisaFee; bool Mail = false;
            DBHelper.DBReturnCode retCode = OTBAdminManager.OtherCompanyOtb(Uid, ArrivalAirLine, DepartingAirline, Arrivalfrom, DepartingFrom, DepartingDate, ArrivalDate, ArrivingFlightNo, DerartingFlightNo, ArrivingPnr, DerartingPnr, DepartingTicketCopy, VisaFee, UrgentCharges, ServiceCharges, OtbCharges, PaxNo, otbTotalAmmount, FirstName, LastName, VisaType, VisaNo, Issue, PassportNo, VisaCopy, sRdbType, otbType, Sponsor, SponsorNo, EmailFormat, Email1, Email2, CCEmailId, OtbRefNo, DOB, OtherPassportNo, PlaceIssue, IssueCountry, ContactNo);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                Session["OtbRefNo"] = null;
                InvoiceMailManager.Status = "Requested";
                InvoiceMailManager.Service = "OTB";
                InvoiceMailManager.Invoice = OtbRefNo;
                var parentThreadCulture = CultureInfo.GetCultureInfo("en-IN");
                bool MailSent = InvoiceMailManager.SendInvoiceMail();
                Thread.CurrentThread.CurrentCulture = parentThreadCulture;
                retCode = OTBEmailManager.AirlinesOtbMail(OtbRefNo, Uid);
                if (retCode == BL.DBHelper.DBReturnCode.EXCEPTION)
                {
                    retCode = OTBManager.IsMailNotSend(OtbRefNo);
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"1\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"0\"}";

            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        public string MailSubject(string Airlines, Int64 PaxNo, string PassengerName)
        {
            string Subject = "";
            if (Airlines == "Indigo Airlines ") { Subject = "6E OTB – AGENT CODE: 00348 - " + PaxNo.ToString() + " PAX"; }
            else if (Airlines == "Air India") { Subject = "Fwd: AI - OK TO BOARD REQUEST - ( " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Air India Express") { Subject = "Fwd: IX- OK TO BOARD REQUEST- " + PaxNo.ToString() + " - PAX )"; }
            else if (Airlines == "Spicejet Airlines") { Subject = "APPLY OTB " + PassengerName + "+" + PaxNo.ToString() + " PAX )"; }
            else if (Airlines == "Air Arabia") { Subject = "APPLY OTB " + PassengerName + "(" + PaxNo.ToString() + "PAX )"; }
            else { Subject = "OK TO BOARD REQUEST- " + PassengerName + " (" + PaxNo.ToString() + "- PAX )"; }
            return Subject;
        }

        #endregion

        #endregion

        #region Visa Load By Id
        [WebMethod(EnableSession = true)]
        public string GetVisaByCode(string VisaCode)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = OTBManager.GetVisaByCode(out dtResult, VisaCode);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Visa\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion
    }
}
