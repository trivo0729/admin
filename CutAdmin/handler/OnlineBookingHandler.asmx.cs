﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for OnlineBookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OnlineBookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        dbHotelhelperDataContext dbTax = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }

            public Int64 Id { get; set; }
            public Int64 AddOnsID { get; set; }
            public bool IsCumpulsary { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 RateID { get; set; }
            public string RateType { get; set; }
            public Decimal Rate { get; set; }
            public Int64 UserID { get; set; }


        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }

        public class RateGroup
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), Convert.ToInt64(arrHotelDetails.HotelId));
                return jsSerializer.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, errMsg = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GenrateBookingDetails(List<RateGroup> ListRates, string Serach)
        {
            try
            {
                Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                if (arrHotelDetails == null)
                    throw new Exception("Please Search again ,Rates are Changged.");
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.RateList;
                arrHotelDetails.RateList[0].Charge = new ServiceCharge();
                float RoomTotal = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate[0].RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        arrRate.OtherRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100); }); //  Take Other Rate
                        Other += arrRate.OtherRates.Select(d => d.TotalRate).ToList().Sum();
                        foreach (var objOther in arrRate.OtherRates)
                        {
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                                    other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                            }
                        }

                        RoomTotal += arrRate.Total;
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        #endregion

                        #region Check Inventory
                        foreach (var objDate in arrRate.Dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, Convert.ToDecimal(objDate.RateTypeId), objDate.RoomTypeId.ToString(), objDate.datetime, AccountManager.GetSupplierByUser());
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }



                if (noInventory.Any(d => d == 0) && bList.Any(d => d == true))
                    throw new Exception("Inventory is not available ,Please Contact Administrator or Change checking Date.");
                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                Session["RateList" + Serach] = Rooms;
                arrHotelDetails.RateList[0].Charge = new ServiceCharge { RoomRate = RoomTotal, OtherRates = other, TotalPrice = RoomTotal + other.Select(d => d.TotalRate).ToList().Sum() };
                Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }



        [WebMethod(EnableSession = true)]
        public string GetBookingDetails(string Serach)
        {
            try
            {
                List<RoomType> Rooms = (List<RoomType>)Session["RateList" + Serach];
                List<RateGroup> ListRates = (List<RateGroup>)Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, arrHotelDetails.RateList[0].Charge });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingList"] = null;
                List<OnlineReservations> BookingList = OnlineReservations.OnlineBookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList() });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GroupBookingList()
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingList"] = null;
                List<OnlineReservations> BookingList = OnlineReservations.OnlineGroupBookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList() });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            try
            {
                List<OnlineReservations> BookingList = OnlineReservations.OnlineBookingList();
                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingPending });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingConfirmed });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = BookingList.Where(d => d.Status == "Cancelled").ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingRejected });
                }
                else if (status == "OnRequest" && type == "Bookings")
                {
                    var OnHoldBookings = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldBookings });
                }
                else if (status == "OnRequest" && type == "Requested")
                {
                    var OnHoldRequested = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldRequested });
                }
                else if (status == "OnRequest" && type == "Confirmed")
                {
                    var OnHoldConfirmed = BookingList.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = OnHoldConfirmed });
                }



                else if (status == "Vouchered" && type == "ReconfirmPending")
                {
                    var ReconfirmPending = BookingList.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmPending });
                }
                else if (status == "Vouchered" && type == "ReconfirmRequested")
                {
                    var ReconfirmRequested = BookingList.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmRequested });
                }
                else if (status == "Vouchered" && type == "ReconfirmReject")
                {
                    var ReconfirmReject = BookingList.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = ReconfirmReject });
                }

                else if (status == "GroupRequest" && type == "GroupRequestPending")
                {
                    var GroupRequestPending = BookingList.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestPending });
                }
                else if (status == "GroupRequest" && type == "GroupRequestRequested")
                {
                    var GroupRequestRequested = BookingList.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestRequested });
                }
                else if (status == "GroupRequest" && type == "GroupRequestReject")
                {
                    var GroupRequestReject = BookingList.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = GroupRequestReject });
                }

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Search(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SupplierBookingList"];
            DataRow[] row = null;
            try
            {
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("Name like '%" + Passenger + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                        BookingList = row.CopyToDataTable();
                }
                Session["SearchSupplierBookingList"] = BookingList;
                List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                BookingList.Dispose();
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        //[WebMethod(EnableSession = true)]
        //public string BookingCancle(string ReservationID, string Status)
        //{
        //    try
        //    {
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Int64 Uid = AccountManager.GetUserByLogin();
        //        //if (objGlobalDefault.UserType != "Supplier")
        //        //    Uid = objGlobalDefault.ParentId;
        //        bool response = InventoryManager.UpdateInvOnCancellation(ReservationID);
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            if (response)
        //            {
        //                List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
        //                List<InventoryManager.RecordInv> Record = InventoryManager.Record;
        //                for (int i = 0; i < Record.Count; i++)
        //                {
        //                    tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
        //                    Recordnew.BookingId = Record[i].BookingId;
        //                    Recordnew.InvSid = Record[i].InvSid;
        //                    Recordnew.SupplierId = Record[i].SupplierId;
        //                    Recordnew.HotelCode = Record[i].HotelCode;
        //                    Recordnew.RoomType = Record[i].RoomType;
        //                    Recordnew.RateType = Record[i].RateType;
        //                    Recordnew.InvType = Record[i].InvType;
        //                    Recordnew.Date = Record[i].Date;
        //                    Recordnew.Month = Record[i].Month;
        //                    Recordnew.Year = Record[i].Year;
        //                    Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
        //                    Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
        //                    Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
        //                    Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
        //                    Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
        //                    Recordnew.UpdateOn = Record[i].UpdateOn;
        //                    AddRecord.Add(Recordnew);
        //                }
        //                DB.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
        //                DB.SubmitChanges();
        //            }

        //            tbl_CommonHotelReservation Update = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID && x.Uid == Uid);
        //            Update.Status = "Cancelled";

        //            DB.SubmitChanges();

        //            var CancleDetail = (from obj in DB.tbl_CommonBookedRooms where obj.ReservationID == ReservationID select obj).ToList();
        //            var BookingDetail = (from obj in DB.tbl_CommonHotelReservations where obj.ReservationID == ReservationID select obj).ToList();
        //            var Cancle = CancleDetail[0].CutCancellationDate.Split('|');
        //            var Cancleamnt = CancleDetail[0].CancellationAmount.Split('|');
        //            List<DateTime> CDate = new List<DateTime>();
        //            DateTime CancleDate = DateTime.Now;

        //            Int64 Parent = AccountManager.GetSupplierByUser();

        //            string Res = "HQ-" + ReservationID;
        //            var InvoiceDetails = (from obj in db.tbl_Invoices where obj.InvoiceNo == BookingDetail[0].InvoiceID && obj.AgentID == Uid && obj.ParentID == Parent select obj).ToList();
        //            // var Commission = InvoiceDetails[0].Commission;
        //            var Commission = "0";

        //            decimal RefundAmnt = 0;
        //            decimal CanclAmnt = 0;
        //            decimal Total = 0;
        //            //for (int i = 0; i < CancleDetail.Count; i++)
        //            //{
        //            //    CDate.Add(DateTime.ParseExact(CancleDetail[i].CutCancellationDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
        //            //}

        //            for (int i = 0; i < Cancle.Length - 1; i++)
        //            {

        //                if (CancleDate >= DateTime.ParseExact(Cancle[i], "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture))
        //                {
        //                    CanclAmnt = Convert.ToDecimal(Cancleamnt[i]);
        //                    Total = Convert.ToDecimal(CancleDetail[0].RoomAmount);
        //                    RefundAmnt = Total - CanclAmnt;
        //                    break;
        //                }
        //            }

        //            //Email Sending


        //            Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
        //            // GlobalDefault objGlobalDefault = new GlobalDefault();
        //            if (HttpContext.Current.Session["LoginUser"] != null)
        //            {
        //                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //                // ParentID = objGlobalDefault.sid;
        //            }

        //            string BookingStatus = "Vouchered";
        //            DBHelper.DBReturnCode retCode = HotelManager.CancelBooking(ReservationID, CanclAmnt.ToString(), BookingStatus, "", BookingDetail[0].TotalFare.ToString(), Commission.ToString(), "");
        //            if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //            {
        //                var sReservation = (from obj in DB.tbl_HotelReservations
        //                                    join objAgent in DB.tbl_AdminLogins on obj.Uid equals objAgent.sid
        //                                    where obj.ReservationID == ReservationID
        //                                    select new
        //                                    {
        //                                        customer_Email = objAgent.uid,
        //                                        customer_name = obj.AgencyName,
        //                                        VoucherNo = obj.VoucherID,
        //                                        Uid = obj.Uid,
        //                                        Email = objAgent.uid,
        //                                        ContactPerson = objAgent.ContactPerson,
        //                                        CurrencyCode = objAgent.CurrencyCode,
        //                                        HotelName = obj.HotelName,
        //                                        City = obj.City,
        //                                        checkin = obj.CheckIn,
        //                                        checkout = obj.CheckOut,
        //                                        Ammount = obj.TotalFare,
        //                                        Nights = obj.NoOfDays,
        //                                        Passenger = obj.bookingname,
        //                                        InvoiceID = obj.InvoiceID,
        //                                        VoucherID = obj.VoucherID,
        //                                        bookingdate = obj.ReservationDate,
        //                                        Status = obj.Status,
        //                                    }).FirstOrDefault();

        //                //  var sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();



        //                //var sMail = new tbl_ActivityMail();
        //                ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
        //                var sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == ParentID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();
        //                string BCcTeamMails = "";
        //                string CcTeamMail = "";

        //                if (sMail != null)
        //                {
        //                    BCcTeamMails = sMail.Email;
        //                    CcTeamMail = sMail.CcMail;
        //                }
        //                Dictionary<string, string> Email1List = new Dictionary<string, string>();
        //                Email1List.Add(BCcTeamMails, BCcTeamMails);
        //                Email1List.Add(sReservation.customer_Email, sReservation.customer_Email);



        //                string Logo = HttpContext.Current.Session["logo"].ToString();
        //                if (objGlobalDefault.UserType == "SupplierStaff" || objGlobalDefault.UserType == "Agent")
        //                {
        //                    Logo = (from obj in DB.tbl_AdminLogins where obj.sid == objGlobalDefault.ParentId select obj.Agentuniquecode).FirstOrDefault();
        //                }
        //                string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
        //                StringBuilder sb = new StringBuilder();

        //                sb.Append("<!DOCTYPE html>");
        //                sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
        //                sb.Append("<head>    ");
        //                sb.Append("<meta charset=\"utf-8\" />");
        //                sb.Append("</head>");
        //                sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">    ");
        //                sb.Append("<div style=\"height:50px;width:auto\">    ");
        //                sb.Append("<br />");
        //                sb.Append("<img src=" + Url + "AgencyLogo/" + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />    ");
        //                sb.Append("</div>");
        //                sb.Append("<br>");
        //                sb.Append("<div style=\"margin-left:10%\">");
        //                sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">Dear " + sReservation.ContactPerson + ",</span><br /></p>");
        //                sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">We have received your request, your Booking Details.</span><br /></p>       ");
        //                sb.Append("<style type=\"text/css\">");
        //                sb.Append(".tg  {border-collapse:collapse;border-spacing:0;}");
        //                sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
        //                sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
        //                sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
        //                sb.Append(".tg .tg-yw4l{vertical-align:top}");
        //                sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>    ");
        //                sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
        //                sb.Append("<tr>");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + ReservationID + "</td>  ");
        //                sb.Append("</tr>");
        //                sb.Append("<tr>");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Reservation Status</b></td>  ");
        //                sb.Append("<td class=\"tg-yw4l\">:" + sReservation.Status + "</td>  ");
        //                sb.Append("</tr>  ");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Reservation Date</b></td>  ");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.bookingdate + "</td>");
        //                sb.Append("</tr>  ");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>  ");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.HotelName + "</td>");
        //                sb.Append("</tr>  ");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Location</b></td>  ");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.City + "</td>");
        //                sb.Append("</tr>");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Check-In</b></td>  ");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkin + "</td>");
        //                sb.Append("</tr>");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Check-Out </b></td>");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkout + "</td>");
        //                sb.Append("</tr>  ");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Rate</b></td>");
        //                sb.Append("<td class=\"tg-yw4l\">:" + sReservation.CurrencyCode + " " + Convert.ToInt32(sReservation.Ammount) / Convert.ToInt32(sReservation.Nights) + "</td>  ");
        //                sb.Append("</tr>  ");
        //                sb.Append("<tr>  ");
        //                sb.Append("<td class=\"tg-9hbo\"><b>Total</b></td>");
        //                sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.CurrencyCode + "" + sReservation.Ammount + "</td>  ");
        //                sb.Append("</tr>");
        //                sb.Append("</table></div>                            ");
        //                sb.Append("<p><span style=\\\"margin-left:2%;font-weight:400\\\">For any further clarification & query kindly feel free to contact us</span><br /></p>    ");
        //                sb.Append("<span style=\\\"margin-left:2%\\\">    ");
        //                sb.Append("<b>Thank You,</b><br />    ");
        //                sb.Append("</span>    ");
        //                sb.Append("<span style=\\\"margin-left:2%\\\">    ");
        //                sb.Append("Administrator    ");
        //                sb.Append("</span>                 ");
        //                sb.Append("        </div>       ");
        //                sb.Append("     <div>    ");
        //                sb.Append("        <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">    ");
        //                sb.Append("            <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">    ");
        //                sb.Append("                <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  Clickurhotel.com</div></td>    ");
        //                sb.Append("            </tr>    ");
        //                sb.Append("        </table>    ");
        //                sb.Append("     </div>    ");
        //                sb.Append("     </body>    ");
        //                sb.Append("     </html>");

        //                string Title = "Cancel Confirmation";
        //                MailManager.SendMail(sReservation.customer_Email, Title, sb.ToString(), "", CcTeamMail);


        //            }


        //            string Message = EmailManager.GetErrorMessage("Booking Cancelled");
        //            return jsSerializer.Serialize(new { retCode = 1, Session = 1, Message = Message });
        //        }
        //    }
        //    catch
        //    {
        //        string Message = EmailManager.GetErrorMessage("Booking Cancelled Error");
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetDetail(string ReservationID)
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            var Detail = (from obj in DB.tbl_CommonHotelReservations
        //                          join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
        //                          join objroom in DB.tbl_CommonBookedRooms on obj.ReservationID equals objroom.ReservationID
        //                          where objPass.IsLeading == true && obj.ReservationID == ReservationID
        //                          select new
        //                          {
        //                              obj.Sid,
        //                              obj.ReservationID,
        //                              obj.ReservationDate,
        //                              obj.Status,
        //                              obj.TotalFare,
        //                              obj.TotalRooms,
        //                              obj.HotelName,
        //                              obj.CheckIn,
        //                              obj.CheckOut,
        //                              obj.City,
        //                              obj.Children,
        //                              obj.BookingStatus,
        //                              obj.NoOfAdults,
        //                              obj.Source,
        //                              obj.Uid,
        //                              obj.NoOfDays,
        //                              objPass.Name,
        //                              objPass.LastName,
        //                              objPass.RoomNumber,
        //                              objroom.CancellationAmount,
        //                              objroom.CutCancellationDate
        //                          }).ToList().Distinct();

        //            return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
        //        }
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetAmendmentDetail(string ReservationID)
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            var Detail = (from obj in DB.tbl_CommonBookedPassengers where obj.ReservationID == ReservationID select obj).ToList();

        //            return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
        //        }
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }
        //}


        //[WebMethod(EnableSession = true)]
        //public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            Int64 UserId = objGlobalDefault.sid;
        //            tbl_CommonHotelReconfirmationDetail Confirm = new tbl_CommonHotelReconfirmationDetail();
        //            Confirm.UserID = UserId;
        //            Confirm.ReservationId = ReservationId;
        //            Confirm.ReconfirmDate = ConfirmDate;
        //            Confirm.Staff_Name = StaffName;
        //            Confirm.ReconfirmThrough = ReconfirmThrough;
        //            Confirm.HotelConfirmationNo = HotelConfirmationNo;
        //            Confirm.Comment = Comment;
        //            DB.tbl_CommonHotelReconfirmationDetails.InsertOnSubmit(Confirm);
        //            DB.SubmitChanges();

        //            DBHelper.DBReturnCode retcode = BookingManager.ConfirmHoldBooking(ReservationId);

        //            if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //            {
        //                tbl_CommonHotelReservation Bit = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationId);
        //                Bit.IsConfirm = true;
        //                Bit.Status = "Vouchered";
        //                Bit.HotelConfirmationNo = HotelConfirmationNo;
        //                DB.SubmitChanges();
        //                ReconfirmationMail(Convert.ToInt64(Bit.Uid), Confirm.sid, HotelName, ReservationId, StaffName, Comment);

        //                //New Work///

        //                Int64 Uid = 0; string sTo = "";
        //                Uid = AccountManager.GetSupplierByUser();
        //                sTo = objGlobalDefault.uid;
        //                sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
        //                //  sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
        //                MailManager.SendReconfrmInvoice(ReservationId, Uid, sTo);
        //            }

        //        }
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public string AcceptGroupReq(Int64 Id, string Status, string Remark)
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            string AgencyName = objGlobalDefault.AgencyName;
        //            tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
        //            Accept.Status = "Vouchered";
        //            Accept.UpdatedBy = AgencyName;
        //            Accept.Remark = Remark;
        //            DB.SubmitChanges();
        //        }
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public string RejectGroupReq(Int64 Id, string Status, string Remark)
        //{
        //    try
        //    {
        //        using (var DB = new dbHotelhelperDataContext())
        //        {
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            string AgencyName = objGlobalDefault.AgencyName;
        //            tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservations.Single(x => x.Sid == Id);
        //            Accept.Status = "Cancelled";
        //            Accept.UpdatedBy = AgencyName;
        //            Accept.Remark = Remark;
        //            DB.SubmitChanges();
        //        }
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        //    }
        //}


        public static bool ReconfirmationMail(Int64 Uid, Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
                using (var DB = new helperDataContext())
                {
                    var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                    string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in DB.tbl_HotelReservations
                                        from objAgent in DB.tbl_AdminLogins
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = sMail.BCcMail;
                    string CcTeamMail = sMail.CcMail;


                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + sReservation.customer_name + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + sReservation.CompanyName + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                    sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                    sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    sb.Append("  No.</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    sb.Append("  Name</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    sb.Append("  ID</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                    sb.Append("  by</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                    sb.Append("  / Note</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append("</table>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";

                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sReservation.customer_Email, "");
                    Email1List.Add(BCcTeamMails, "");
                    Email2List.Add(CcTeamMail, "");

                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveAmendemnt(string ReservationID, List<tbl_CommonBookedPassenger> ListPax)
        {
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {

                    var oldDetails = (from obj in DB.tbl_CommonBookedPassengers
                                      where obj.ReservationID == ReservationID
                                      select obj).ToList();
                    if (oldDetails.Count != 0)
                    {
                        DB.tbl_CommonBookedPassengers.DeleteAllOnSubmit(oldDetails);
                        DB.SubmitChanges();
                    }
                    DB.tbl_CommonBookedPassengers.InsertAllOnSubmit(ListPax);
                    DB.SubmitChanges();

                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }



    }
}

