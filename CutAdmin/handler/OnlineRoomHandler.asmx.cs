﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.dbml;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for OnlineRoomHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OnlineRoomHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
        dbHotelhelperDataContext db = new dbHotelhelperDataContext();
        DBHelper.DBReturnCode retCode;

        [WebMethod(EnableSession = true)]
        public string GetRooms(Int64 sHotelId)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();

            if (sHotelId > 0)
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                    //objHotelList = JsonStringManager.ConvertDataTable(dtResult);          
                    var RoomList = (from obj in DB.tbl_commonRoomDetails
                                    join meta in DB.tbl_commonRoomTypes on obj.RoomTypeId equals meta.RoomTypeID
                                    where obj.HotelId == sHotelId && obj.Approved != false

                                    select new
                                    {
                                        meta.RoomType,
                                        meta.RoomTypeID,
                                        obj.RoomSize,
                                        obj.SmokingAllowed,
                                        obj.BeddingType,
                                        obj.MaxExtrabedAllowed,
                                        obj.RoomOccupancy,
                                        obj.RoomId,
                                        obj.RoomAmenitiesId,
                                        obj.RoomDescription,

                                    }

                                    ).ToList();

                    var RoomTypeList = (from obj in DB.tbl_commonRoomTypes select obj).ToList();
                    var RoomAmenityList = (from obj in DB.tbl_commonAmunities select obj).ToList();

                    return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomAmenityList = RoomAmenityList, RoomTypeList = RoomTypeList });
                }
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string getRoomwithId(Int64 RoomId, Int64 HotelCode)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();

            if (RoomId > 0)
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    var RoomList = (from Room in DB.tbl_commonRoomDetails where Room.RoomId == RoomId && Room.HotelId == HotelCode select Room).FirstOrDefault();
                    var RoomType = (from RType in DB.tbl_commonRoomTypes where RType.RoomTypeID == RoomList.RoomTypeId select RType).FirstOrDefault();
                    List<string> AmenityList = new List<string>();
                    foreach (string AmenityId in RoomList.RoomAmenitiesId.Split(','))
                    {
                        var Amenity = (from obj in DB.tbl_commonAmunities where obj.RoomAmunityID == Convert.ToInt64(AmenityId) select obj.RoomAmunityName).FirstOrDefault();
                        AmenityList.Add(Amenity);
                    }


                    return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomType = RoomType, AmenityList = AmenityList });
                }
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        Int64 sid = 0;
        Int64 SidNew = 0;
        [WebMethod(EnableSession = true)]
        //public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<string> InventoryType, List<string> NoOfInventoryRoom)
        public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> RateType, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<List<Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight)
        {
            try
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetSupplierByUser();

                //AddInventory(HotelCode,Supplier,MealPlan,CheckinR,CheckoutR,RoomTypeID,RateTypeCode)
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetSupplierByUser();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                //RoomTypeID, CheckinR/CheckoutR,      
                int[] arr = new int[5];
                string[] status = new string[2];
                status[0] = "Active";
                status[1] = "Deactive";
                int countRateId = 0,  CountRoomTypes = 0;
                //string[]
                int k = 0;
                // int m = m + CheckinR.Count; ;
                for (int i = 0; i < RoomTypeID.Count(); i++)
                {

                    if (CancelPolicy[i] == "")
                    {
                        CancelPolicy[i] = "1";
                    }
                    List<tbl_commonRoomRate> ListRoomID = new List<tbl_commonRoomRate>();
               using (var DB = new dbHotelhelperDataContext())
                    {
                        ListRoomID = (from obj in DB.tbl_commonRoomRates where (obj.RoomId == Convert.ToInt64(RoomTypeID[i]) && obj.HotelID == HotelCode) select obj).ToList();
                    }
                    if (ListRoomID.Count == 0)
                    {
                        #region If No Entries present
                        for (int j = 0; j < CheckinR.Count; j++)
                        {
                            AddPrices(RoomTypeID[i], HotelCode, sUserID, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                RateInclude, RateExclude, MinStay, MaxStay, TaxIncluded, TaxType, TaxOn, TaxValue,
                                TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k]);
                            k++;
                        }

                        #endregion
                    }
                    else
                    {
                        bool AvlGenData, AvlNationality, AvlCancelation = false, AvlOffer = false, AvlDates = false;
                        //var dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && d.MinStay == Convert.ToInt64(MinStay) && d.MaxStay == Convert.ToInt64(MaxStay)).ToList();
                        var dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && (d.RateType == RateTypeCode[i])).ToList();

                        var dNationality = ListRoomID.Where(d => (d.ValidNationality == Country));

                        if (dGeneralData.Count() > 0) { AvlGenData = true; }
                        else { AvlGenData = false; }
                        if (dNationality.Count() > 0) { AvlNationality = true; }
                        else { AvlNationality = false; }

                        var RateTypes = dGeneralData.Select(d => d.RateType).ToList();
                        var RateID = dGeneralData.Select(d => d.HotelRateID).ToList();




                        if (AvlGenData == false)
                        {
                            #region if Supplier, MealPlan, Currency Not  Exists
                            for (int j = 0; j < CheckinR.Count; j++)
                            {
                                AddPrices(RoomTypeID[i], HotelCode, Uid, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                   RateInclude, RateExclude, MinStay, MaxStay, TaxIncluded, TaxType, TaxOn, TaxValue,
                                   TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                   Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                   RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k]);
                                k++;
                            }
                            #endregion
                        }
                        else if (AvlGenData == true)
                        {

                            for (int j = 0; j < CheckinR.Count; j++)
                            {
                                var ListWithinDates = dGeneralData.Where(d => DateTime.Parse(d.Checkin) <= DateTime.Parse(CheckinR[j]) && DateTime.Parse(d.Checkout) >= DateTime.Parse(CheckoutR[j])).ToList();
                                if (AvlNationality == false && ListWithinDates.Count > 0)
                                {
                                    #region If Nationality is different Add only Nationality to Existing Rates

                                    var Nationalities = dGeneralData.Select(d => d.ValidNationality).ToList();
                                    string nations = "";
                                    int count = 0;
                                    foreach (string nation in Nationalities)
                                    {
                                        nations += nation + Country;
                                    }
                                    List<string> uniques = nations.Split('^').Distinct().ToList();
                                    string newNationality = string.Join("^", uniques);
                                    foreach (var data in dGeneralData)
                                    {
                                        if (data.RoomId == Convert.ToInt64(RoomTypeID[i]) && data.RateType == RateTypeCode[i])
                                        {
                                            foreach (Int64 RTID in RateID)
                                            {
                                           using (var DB = new dbHotelhelperDataContext())
                                                {
                                                    tbl_commonRoomRate Rate = DB.tbl_commonRoomRates.Single(x => x.HotelRateID == RTID);
                                                    Rate.ValidNationality = newNationality;
                                                    DB.SubmitChanges();
                                                    CountRoomTypes += 1;
                                                }
                                            }
                                        }
                                    }
                                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                                    return json;
                                    #endregion

                                }
                                else
                                {
                                    AddPrices(RoomTypeID[i], HotelCode, Uid, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                       RateInclude, RateExclude, MinStay, MaxStay, TaxIncluded, TaxType, TaxOn, TaxValue,
                                       TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                       Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                       RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k]);
                                    k++;
                                }

                            }


                        }

                    }



                }
                if (ListAddOnsRates != null)
                {
               using (var DB = new dbHotelhelperDataContext())
                    {
                        List<Int64> RateIds = DB.tbl_commonRoomRates.OrderByDescending(obj => obj.HotelRateID).ToList().Take(ListAddOnsRates.Count).Select(r => Convert.ToInt64(r.HotelRateID)).Reverse().ToList();

                        if (RateIds.Count != null)
                        {
                            for (int i = 0; i < ListAddOnsRates.Count; i++)
                            {
                                List<Comm_AddOnsRate> ListAddOns = ListAddOnsRates[i];
                                if (ListAddOns.Count != 0)
                                {
                                    ListAddOns.ForEach(d => d.RateID = Convert.ToInt64(RateIds[i]));
                                    TaxManager.SaveTaxDetails(ListAddOns);
                                }
                            }
                        }
                    }
                    //List<Comm_TaxMapping> ListAddOns = ListAddOnsRates[i].Take(m).ToList();
                    //if (ListAddOns.Count != 0)
                    //{
                    //    ListAddOns.ForEach(d => d.ServiceID = DB.tbl_commonRoomRates.OrderByDescending(obj => obj.HotelRateID).FirstOrDefault().HotelRateID);
                    //    TaxManager.SaveTaxDetails(ListAddOns);

                    //}


                }
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                return json;

            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<string> TypeRate, List<List<Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight)
        {
            try
            {
                //AddInventory(HotelCode,Supplier,MealPlan,CheckinR,CheckoutR,RoomTypeID,RateTypeCode)
                DataLayer.GlobalDefault objUser = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetSupplierByUser();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
           using (var DB = new dbHotelhelperDataContext())
                {
                    var listRate = (from obj in DB.tbl_commonRoomRates where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.Checkin == CheckinR && obj.Checkout == CheckoutR && obj.ValidNationality.Contains(Country) select obj).ToList();
                    //var listRate = (from obj in DB.tbl_commonRoomRates where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.ValidNationality.Contains(Country) select obj).ToList();

                    for (int i = 0; i < RoomTypeID.Count(); i++)
                    {

                        if (CancelPolicy[i] == "")
                        {
                            CancelPolicy[i] = "1";
                        }
                        string Id = "";
                        try
                        {
                            Id = listRate[i].RoomId.ToString();
                        }
                        catch
                        {
                            Id = "";
                        }

                        if (RoomTypeID.Contains(Id))
                        {
                            for (int j = 0; j < listRate.Count; j++)
                            {

                                if (RoomTypeID[i] == listRate[j].RoomId.ToString())
                                {
                                    tbl_commonRoomRate update = DB.tbl_commonRoomRates.Single(x => x.HotelRateID == listRate[j].HotelRateID && x.RoomId == listRate[j].RoomId);
                                    update.RoomId = Convert.ToInt64(RoomTypeID[j]);
                                    update.HotelID = HotelCode;
                                    update.SupplierId = sUserID;
                                    update.ValidNationality = Country;
                                    update.CurrencyCode = Currency;
                                    update.MealPlan = MealPlan;
                                    update.HotelContractId = 0;
                                    update.RateType = RateTypeCode[j];
                                    update.Type = TypeRate[j];
                                    update.Checkin = CheckinR;
                                    update.Checkout = CheckoutR;
                                    update.BookingCode = BookingCode[j];
                                    update.CancellationPolicyId = CancelPolicy[j];
                                    update.OfferId = Offers[j];
                                    update.TariffNote = RateNote[j];
                                    update.DayWise = Daywise[j];
                                    if (Daywise[j] == "Yes")
                                    {
                                        if (Sunday[j] != "0")
                                        {
                                            update.SunRR = Convert.ToDecimal(Sunday[j].Split(',')[0]);
                                            // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                                        }
                                        if (Monday[j] != "0")
                                        {
                                            update.MonRR = Convert.ToDecimal(Monday[j].Split(',')[0]);
                                            // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                                        }
                                        if (Tuesday[j] != "0")
                                        {
                                            update.TueRR = Convert.ToDecimal(Tuesday[j].Split(',')[0]);
                                            // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                                        }
                                        if (Wednesday[j] != "0")
                                        {
                                            update.WedRR = Convert.ToDecimal(Wednesday[j].Split(',')[0]);
                                            //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                                        }
                                        if (Thursday[j] != "0")
                                        {
                                            update.ThuRR = Convert.ToDecimal(Thursday[j].Split(',')[0]);
                                            //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                                        }
                                        if (Friday[j] != "0")
                                        {
                                            update.FriRR = Convert.ToDecimal(Friday[j].Split(',')[0]);
                                            // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                                        }
                                        if (Saturday[j] != "0")
                                        {
                                            update.SatRR = Convert.ToDecimal(Saturday[j].Split(',')[0]);
                                            // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                                        }
                                        update.RR = null;
                                        //update.EB = null;
                                        //update.CWB = null;
                                        //update.CNB = null;
                                    }
                                    else
                                    {
                                        update.RR = Convert.ToDecimal(RoomRates[j]);
                                        //update.EB = Convert.ToDecimal(ExtraBeds[j]);
                                        //update.CWB = Convert.ToDecimal(CWB[j]);
                                        //update.CNB = Convert.ToDecimal(CWN[j]);
                                        update.SunRR = null;
                                        update.MonRR = null;
                                        update.TueRR = null;
                                        update.WedRR = null;
                                        update.TueRR = null;
                                        update.FriRR = null;
                                        update.SatRR = null;
                                    }
                                    update.EB = Convert.ToDecimal(ExtraBeds[j]);
                                    update.CWB = Convert.ToDecimal(CWB[j]);
                                    update.CNB = Convert.ToDecimal(CWN[j]);
                                    update.Status = "Active";
                                    update.OfferNight = Convert.ToInt64(OfferNight[i]);
                                    //DB.tbl_commonRoomRates.InsertOnSubmit(update);
                                    DB.SubmitChanges();
                                }

                            }

                        }
                        //else if (RateID!=null)
                        //{
                        //    var listRateDate = (from obj in DB.tbl_commonRoomRates where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.ValidNationality.Contains(Country) select obj).ToList();
                        //    for (int j = 0; j < listRateDate.Count; j++)
                        //    {

                        //        if (RoomTypeID[i] == listRateDate[j].RoomId.ToString())
                        //        {
                        //            tbl_commonRoomRate update = DB.tbl_commonRoomRates.Single(x => x.HotelRateID == listRateDate[j].HotelRateID && x.RoomId == listRateDate[j].RoomId);
                        //            update.RoomId = Convert.ToInt64(RoomTypeID[i]);
                        //            update.HotelID = HotelCode;
                        //            update.SupplierId = sUserID;
                        //            update.ValidNationality = Country;
                        //            update.CurrencyCode = Currency;
                        //            update.MealPlan = MealPlan;
                        //            update.HotelContractId = 0;
                        //            update.RateType = RateTypeCode[j];
                        //            update.Type = TypeRate[j];
                        //            update.Checkin = CheckinR;
                        //            update.Checkout = CheckoutR;
                        //            update.BookingCode = BookingCode[j];
                        //            update.CancellationPolicyId = CancelPolicy[j];
                        //            update.OfferId = Offers[j];
                        //            update.TariffNote = RateNote[j];
                        //            update.DayWise = Daywise[j];
                        //            if (Daywise[j] == "Yes")
                        //            {
                        //                if (Sunday[j] != "0")
                        //                {
                        //                    update.SunRR = Convert.ToDecimal(Sunday[j].Split(',')[0]);
                        //                    // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                        //                }
                        //                if (Monday[j] != "0")
                        //                {
                        //                    update.MonRR = Convert.ToDecimal(Monday[j].Split(',')[0]);
                        //                    // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                        //                }
                        //                if (Tuesday[j] != "0")
                        //                {
                        //                    update.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
                        //                    // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                        //                }
                        //                if (Wednesday[j] != "0")
                        //                {
                        //                    update.WedRR = Convert.ToDecimal(Wednesday[j].Split(',')[0]);
                        //                    //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                        //                }
                        //                if (Thursday[j] != "0")
                        //                {
                        //                    update.ThuRR = Convert.ToDecimal(Thursday[j].Split(',')[0]);
                        //                    //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                        //                }
                        //                if (Friday[j] != "0")
                        //                {
                        //                    update.FriRR = Convert.ToDecimal(Friday[j].Split(',')[0]);
                        //                    // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                        //                }
                        //                if (Saturday[j] != "0")
                        //                {
                        //                    update.SatRR = Convert.ToDecimal(Saturday[j].Split(',')[0]);
                        //                    // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                        //                }
                        //                update.RR = null;
                        //                //update.EB = null;
                        //                //update.CWB = null;
                        //                //update.CNB = null;
                        //            }
                        //            else
                        //            {
                        //                update.RR = Convert.ToDecimal(RoomRates[j]);
                        //                //update.EB = Convert.ToDecimal(ExtraBeds[j]);
                        //                //update.CWB = Convert.ToDecimal(CWB[j]);
                        //                //update.CNB = Convert.ToDecimal(CWN[j]);
                        //                update.SunRR = null;
                        //                update.MonRR = null;
                        //                update.TueRR = null;
                        //                update.WedRR = null;
                        //                update.TueRR = null;
                        //                update.FriRR = null;
                        //                update.SatRR = null;
                        //            }
                        //            update.EB = Convert.ToDecimal(ExtraBeds[j]);
                        //            update.CWB = Convert.ToDecimal(CWB[j]);
                        //            update.CNB = Convert.ToDecimal(CWN[j]);
                        //            update.Status = "Active";
                        //            update.OfferNight = Convert.ToInt64(OfferNight[i]);
                        //            //DB.tbl_commonRoomRates.InsertOnSubmit(update);
                        //            DB.SubmitChanges();
                        //        }
                        //    }
                            
                        //}

                        else
                        {
                            tbl_commonRoomRate Rate = new tbl_commonRoomRate();
                            Rate.RoomId = Convert.ToInt64(RoomTypeID[i]);
                            Rate.HotelID = HotelCode;
                            Rate.SupplierId = sUserID;
                            Rate.ValidNationality = Country;
                            Rate.CurrencyCode = Currency;
                            Rate.MealPlan = MealPlan;
                            Rate.HotelContractId = 0;
                            Rate.Checkin = CheckinR;
                            Rate.Checkout = CheckoutR;
                            Rate.Type = TypeRate[i];
                            Rate.RateType = RateTypeCode[i];
                            Rate.BookingCode = BookingCode[i];
                            Rate.CancellationPolicyId = CancelPolicy[i];
                            Rate.OfferId = Offers[i];
                            Rate.TariffNote = RateNote[i];
                            Rate.DayWise = Daywise[i];
                            if (Daywise[i] == "Yes")
                            {
                                if (Sunday[i] != "0")
                                {
                                    Rate.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
                                    // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                                }
                                if (Monday[i] != "0")
                                {
                                    Rate.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
                                    // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                                }
                                if (Tuesday[i] != "0")
                                {
                                    Rate.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
                                    // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                                }
                                if (Wednesday[i] != "0")
                                {
                                    Rate.WedRR = Convert.ToDecimal(Wednesday[i].Split(',')[0]);
                                    //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                                }
                                if (Thursday[i] != "0")
                                {
                                    Rate.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
                                    //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                                }
                                if (Friday[i] != "0")
                                {
                                    Rate.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
                                    // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                                }
                                if (Saturday[i] != "0")
                                {
                                    Rate.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
                                    // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                                }
                                Rate.RR = null;
                                Rate.EB = null;
                                Rate.CWB = null;
                                Rate.CNB = null;
                            }
                            else
                            {
                                Rate.RR = Convert.ToDecimal(RoomRates[i]);
                                Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
                                Rate.CWB = Convert.ToDecimal(CWB[i]);
                                Rate.CNB = Convert.ToDecimal(CWN[i]);
                                Rate.SunRR = null;
                                Rate.MonRR = null;
                                Rate.TueRR = null;
                                Rate.WedRR = null;
                                Rate.TueRR = null;
                                Rate.FriRR = null;
                                Rate.SatRR = null;
                            }
                            Rate.Status = "Active";
                            Rate.OfferNight = Convert.ToInt64(OfferNight[i]);
                            DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
                            DB.SubmitChanges();

                        }
                    }

                    if (ListAddOnsRates != null)
                    {

                        List<Int64> RateIds = DB.tbl_commonRoomRates.OrderByDescending(obj => obj.HotelRateID).ToList().Take(ListAddOnsRates.Count).Select(r => Convert.ToInt64(r.HotelRateID)).Reverse().ToList();
                        if (RateIds.Count != null)
                        {
                            for (int i = 0; i < ListAddOnsRates.Count; i++)
                            {
                                List<Comm_AddOnsRate> ListAddOns = ListAddOnsRates[i];
                                if (ListAddOns.Count != 0)
                                {
                                    ListAddOns.ForEach(d => d.RateID = Convert.ToInt64(RateIds[i]));
                                    TaxManager.SaveTaxDetails(ListAddOns);
                                }
                            }
                        }


                    }
                }

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                return json;

            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        // public void AddPrices(string RoomTypeID, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string RateTypeCode, string CheckinR, string CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, string BookingCode, string CancelPolicy, string Offers, string RateNote, string Daywise, string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday, string RoomRates, string ExtraBeds, string CWB, string CWN, string InventoryType, string NoOfInventoryRoom)
        public void AddPrices(string RoomTypeID, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string RateTypeCode, string CheckinR, string CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, string BookingCode, string CancelPolicy, string Offers, string RateNote, string RateType, string Daywise, string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday, string RoomRates, string ExtraBeds, string CWB, string CWN, string OfferNight)
        {
            try
            {
           using (var DB = new dbHotelhelperDataContext())
                {
                    tbl_commonRoomRate Rate = new tbl_commonRoomRate();
                    Rate.RoomId = Convert.ToInt64(RoomTypeID);
                    Rate.HotelID = HotelCode;
                    Rate.SupplierId = Supplier;
                    Rate.ValidNationality = Country;
                    Rate.CurrencyCode = Currency;
                    Rate.MealPlan = MealPlan;
                    Rate.HotelContractId = 0;
                    Rate.RateType = RateTypeCode;
                    Rate.Checkin = CheckinR;
                    Rate.Checkout = CheckoutR;
                    Rate.RateInclude = RateInclude;
                    Rate.RateExclude = RateExclude;
                    Rate.MinStay = Convert.ToInt16(MinStay);
                    Rate.MaxStay = 0;
                    Rate.TaxIncluded = TaxIncluded;
                    Rate.TaxType = TaxType;
                    Rate.TaxOn = TaxOn;
                    Rate.TaxValue = Convert.ToInt16(TaxValue);
                    Rate.TaxDetails = TaxDetails;
                    Rate.BookingCode = BookingCode;
                    Rate.CancellationPolicyId = CancelPolicy;
                    Rate.OfferId = Offers;
                    Rate.TariffNote = RateNote;
                    Rate.Type = RateType;
                    Rate.DayWise = Daywise;
                    if (Daywise == "Yes")
                    {
                        if (Sunday != "0")
                        {
                            Rate.SunRR = Convert.ToDecimal(Sunday.Split(',')[0]);
                            // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                        }
                        if (Monday != "0")
                        {
                            Rate.MonRR = Convert.ToDecimal(Monday.Split(',')[0]);
                            // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                        }
                        if (Tuesday != "0")
                        {
                            Rate.TueRR = Convert.ToDecimal(Tuesday.Split(',')[0]);
                            // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                        }
                        if (Wednesday != "0")
                        {
                            Rate.WedRR = Convert.ToDecimal(Wednesday.Split(',')[0]);
                            //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                        }
                        if (Thursday != "0")
                        {
                            Rate.ThuRR = Convert.ToDecimal(Thursday.Split(',')[0]);
                            //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                        }
                        if (Friday != "0")
                        {
                            Rate.FriRR = Convert.ToDecimal(Friday.Split(',')[0]);
                            // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                        }
                        if (Saturday != "0")
                        {
                            Rate.SatRR = Convert.ToDecimal(Saturday.Split(',')[0]);
                            // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                        }

                    }
                    else
                    {
                        Rate.RR = Convert.ToDecimal(RoomRates);
                        //Rate.EB = Convert.ToDecimal(ExtraBeds);
                        //Rate.CWB = Convert.ToDecimal(CWB);
                        //Rate.CNB = Convert.ToDecimal(CWN);
                    }
                    Rate.EB = Convert.ToDecimal(ExtraBeds);
                    Rate.CWB = Convert.ToDecimal(CWB);
                    Rate.CNB = Convert.ToDecimal(CWN);
                    Rate.Status = "Active";
                    Rate.OfferNight = Convert.ToInt64(OfferNight);
                    DB.tbl_commonRoomRates.InsertOnSubmit(Rate);
                    DB.SubmitChanges();
                    sid = Rate.HotelRateID;
                }
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        public class RatePrice
        {
            public string Date { get; set; }
            public string Daywise { get; set; }
            public string RRRate { get; set; }
            public string EBRate { get; set; }
            public string CWBRate { get; set; }
            public string CNBRate { get; set; }
            public string MonRate { get; set; }
            public string TueRate { get; set; }
            public string WedRate { get; set; }
            public string ThuRate { get; set; }
            public string FriRate { get; set; }
            public string SatRate { get; set; }
            public string SunRate { get; set; }
            public string CancelationPolicy { get; set; }
            public string CheckinDate { get; set; }
            public string CheckoutDate { get; set; }
        }
        public class Rates
        {
            public string RateType { get; set; }
            public string RoomId { get; set; }
            public string RoomName { get; set; }
            public List<RatePrice> PriceList { get; set; }
        }
        public class RateList
        {
            public string RoomId { get; set; }
            public string HotelRateID { get; set; }
            public string ValidNationality { get; set; }
            public string Checkin { get; set; }
            public string Checkout { get; set; }
            public string CurrencyCode { get; set; }
            public string RR { get; set; }
            public string RateType { get; set; }
            public string MealPlan { get; set; }
            public string Supplier { get; set; }

            public string DayWise { get; set; }
            public string MonRR { get; set; }
            public string TueRR { get; set; }
            public string WedRR { get; set; }
            public string ThuRR { get; set; }
            public string FriRR { get; set; }
            public string SatRR { get; set; }
            public string SunRR { get; set; }
        }
        [WebMethod(EnableSession = true)]
        public string GetRates(Int64[] HotelCode, string Destination, string Checkin, string Checkout, string[] nationality, int Nights, int Adults, int Childs, string Supplier, string MealPlan, string CurrencyCode, string AddSearchsession, string SearchValid)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            int adult = 0;
            int child = 0;
            try
            {
                if (Session["session" + AddSearchsession] == null)
                {
                    Session["session"] = AddSearchsession;
                    int Age = 0;
                    if (HotelCode.Length != 0)
                    {
                        string ErrorMessage;
                        List<Dictionary<string, object>> RatesList = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> HotelRatesList1 = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> HotelRatesList = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> SeasonOfferList = new List<Dictionary<string, object>>();
                   using (var DB = new dbHotelhelperDataContext())
                        {
                            var HotelDetails = (from obj in DB.tbl_CommonHotelMasters where obj.sid == HotelCode[0] select obj).FirstOrDefault();
                            var AllRooms = (from Room in DB.tbl_commonRoomDetails where Room.HotelId == HotelCode[0] select Room).ToList();
                            List<RateGroup> RateList = RatesManager.GetRateList(HotelCode[0], Checkin, Checkout, nationality, AllRooms, SearchValid);
                            if (RateList.Count == 0)
                            {
                                return objserialize.Serialize(new { Session = 1, retCode = -2, ErrorMessage = "Hotel is not available" });
                            }
                            double noDays = (DateTime.ParseExact(RatesManager.CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) - DateTime.ParseExact(RatesManager.CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)).TotalDays;
                            List<string> ListDates = new List<string>();
                            for (int i = 0; i <= noDays; i++)
                            {
                                ListDates.Add(DateTime.ParseExact(RatesManager.CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).ToString("dd-MM-yyyy"));
                            }
                            var GlobalDefault = (GlobalDefault)Session["LoginUser"];
                            var CurrencyList = (from obj in DB.tbl_CommonCurrencies select obj).ToList();
                            var MealPlanList = (from obj in DB.tbl_CommonMealPlans select obj).ToList();
                            RateList = RateList.Distinct().ToList();
                            var SupplierList = RateList.Select(d => d.Name).Distinct().ToList();
                            // Store avail rate into session
                            #region AvailRates
                            var arrHotel = new CommonLib.Response.CommonHotelDetails();
                            string[] arrSearchAttr = AddSearchsession.Split('_');
                            var arrHotelInfo = (from obj in DB.tbl_CommonHotelMasters
                                                where obj.sid == HotelCode[0]
                                                select new
                                                {
                                                    obj.sid,
                                                    obj.HotelName,
                                                    obj.HotelLatitude,
                                                    obj.HotelLangitude,
                                                    obj.HotelAddress,
                                                    obj.HotelZipCode,
                                                    obj.HotelImage,
                                                    obj.SubImages,
                                                    obj.HotelCategory,
                                                    obj.HotelDescription,
                                                    obj.HotelFacilities
                                                }).FirstOrDefault();
                            string Image = arrHotelInfo.HotelImage;
                            string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                            List<Image> arrImage = new List<Image>();
                            if (Image != null)
                            {
                                List<string> Url = Image.Split('^').ToList();

                                foreach (string Link in Url)
                                {
                                    if (Link != "")
                                        arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                                }
                            }
                            if (arrHotelInfo.SubImages != null)
                            {
                                List<string> Url = arrHotelInfo.SubImages.Split('^').ToList();

                                foreach (string Link in Url)
                                {
                                    if (Link != "")
                                        arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                                }
                            }
                            List<string> sFacilities = new List<string>();
                            foreach (var objID in arrHotelInfo.HotelFacilities.Split(','))
                            {
                                if (objID != "")
                                    sFacilities.Add((from obj in DB.tbl_commonFacilities where obj.HotelFacilityID == Convert.ToInt64(objID) select obj).First().HotelFacilityName);
                            }


                            arrHotel = new CommonHotelDetails
                            {
                                DateFrom = arrSearchAttr[2],
                                DateTo = arrSearchAttr[3],
                                HotelId = arrHotelInfo.sid.ToString(),
                                Description = arrHotelInfo.HotelDescription,
                                HotelName = arrHotelInfo.HotelName,
                                Category = arrHotelInfo.HotelCategory,
                                Address = arrHotelInfo.HotelAddress,
                                Image = arrImage,
                                RateList = RateList,
                                Langitude = arrHotelInfo.HotelLangitude,
                                Latitude = arrHotelInfo.HotelLatitude,
                                Facility = sFacilities.Distinct().ToList()
                            };
                            Session["AvailDetails" + AddSearchsession] = arrHotel;
                            #endregion
                            string json = objserialize.Serialize(new { Session = 1, retCode = 1, ListRate = RateList, Usertype = GlobalDefault.UserType, AllRooms = AllRooms, SupplierList = SupplierList, CurrencyList = CurrencyList, MealPlanList = MealPlanList, ListDates = ListDates, Filter = RoomFilter.GenrateFilter(AddSearchsession) });
                            Session["session" + AddSearchsession] = json;
                            return json;

                        }
                    }

                    return objserialize.Serialize(new { Session = 1, retCode = -2, ErrorMessage = "Hotel is not available for these Dates" });
                }
                else
                {
                    json = Session["session" + AddSearchsession].ToString();
                    return json;
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = ex.Message });
            }



        }
        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        {
            DateTime chunkEnd;
            while ((chunkEnd = start.AddDays(dayChunkSize)) < end)
            {
                yield return Tuple.Create(start, chunkEnd);
                start = chunkEnd;
            }
            yield return Tuple.Create(start, end);
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingRates()
        {

            List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];

            if (Supplier.Count != 0)
            {

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, Rates = Supplier });
            }
            else
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelAddress(Int64 HotelCode)
        {
            try
            {
           using (var DB = new dbHotelhelperDataContext())
                {
                    var HotelAddres = (from obj in DB.tbl_CommonHotelMasters where obj.sid == HotelCode select obj).FirstOrDefault();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, HotelAddress = HotelAddres });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }


        }

        [WebMethod(EnableSession = true)]
        public string GetRoomOccupancy(Int64 AdultCount, Int64 ChildCount, int RateTypeID, int MealID, Int64 RateID)
        {
            try
            {
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                RatesManager.ListRateTypes = RatesManager.GetRates(Convert.ToInt64(Supplier[0].HotelCode));
           using (var DB = new dbHotelhelperDataContext())
                {
                    foreach (var objDetails in Supplier[0].Details)
                    {
                        for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                        {
                            RatesManager.Rate objRate = Supplier[0].Details.Where(
                                                           d => d.RateTypeID == RateTypeID &&
                                                           d.MealID == MealID && d.Rate.RateID == RateID
                                                           ).FirstOrDefault().Rate;
                            var sRates = (from obj in DB.tbl_commonRoomRates where obj.HotelRateID == Convert.ToInt64(objDetails.Rate.ListDates[i].RateID) select obj).FirstOrDefault();
                            // RatesManager.Rate objRate = objDetails.Rate.ListDates[i].Where(d => d.RateID == Convert.ToInt64(RateID));

                            if (objRate.MaxOccupancy >= AdultCount + ChildCount)
                            {
                                if (objRate.RoomOccupancy + objRate.MaxEB < AdultCount)
                                    throw new Exception("Maximum adults exceed");

                                Int64 RoomOccupancy = objRate.RoomOccupancy;
                                Int64 noAdultEB = 0;
                                float EBRate = 0;
                                float CWBRate = 0;
                                float CNBRate = 0;
                                RoomOccupancy = RoomOccupancy - AdultCount;
                                if (RoomOccupancy < 0)
                                {
                                    if (RoomOccupancy + objRate.MaxEB < 0)
                                        throw new Exception("Room Ocuupancy No Valid");
                                    noAdultEB = (-RoomOccupancy);
                                    RoomOccupancy = RoomOccupancy + objRate.MaxEB;
                                    //if (RoomOccupancy != 0)
                                    //    throw new Exception("No Extrabeds exceed");
                                    //EBRate = noAdultEB * objRate.EBRate;
                                    if (RoomOccupancy != 0)
                                        throw new Exception("No Extrabeds exceed");
                                    EBRate = noAdultEB * Convert.ToInt32(sRates.EB);



                                }
                                if (ChildCount != 0)
                                {
                                    Int64 CWB = 0, CNB = 0;
                                    if (noAdultEB == 0)
                                    {
                                        if (objRate.MaxEB == 1)
                                        {
                                            CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            // CWB =  (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        else if (objRate.MaxEB > 1 && ChildCount == 1)
                                        {
                                            //CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            CWB = (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        else
                                        {
                                            CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            // CWB = (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        if ((objRate.MaxEB - ChildCount) < 0)
                                        {
                                            //RoomOccupancy = ChildCount + objRate.MaxEB;
                                            CNB = (-(objRate.MaxEB - ChildCount));
                                            CNBRate = CNB * Convert.ToInt32(sRates.CNB);
                                        }
                                        //else
                                        //CWBRate = ChildCount * objRate.CWBRate;

                                    }
                                    else
                                    {
                                        RoomOccupancy = RoomOccupancy - ChildCount;
                                        if (RoomOccupancy < 0)
                                        {
                                            CNB = -(RoomOccupancy);
                                            CNBRate = CNB * Convert.ToInt32(sRates.CNB);
                                            CWBRate = 0 * Convert.ToInt32(sRates.CWB);

                                        }

                                    }


                                }
                                objDetails.Rate.ListDates[i].CNBRate = RatesManager.GetRate(Convert.ToSingle(CNBRate), "ChildNoBeds"); ;
                                objDetails.Rate.ListDates[i].CWBRate = RatesManager.GetRate(Convert.ToSingle(CWBRate), "ChildWithBeds"); ;
                                objDetails.Rate.ListDates[i].EBRate = RatesManager.GetRate(Convert.ToSingle(EBRate), "ExtraBeds"); ;
                                objDetails.Rate.ListDates[i].TotalPrice = (Convert.ToInt32(objDetails.Rate.ListDates[i].Rate.BaseRate) + EBRate + CNBRate + CWBRate);
                                // RatesManager.GetTotalChargeTax(objDetails.Rate.ListDates[i]);

                            }
                            else
                            {
                                throw new Exception("Room Ocuupancy No Valid");
                            }
                            objDetails.Rate.TotalCharge = RatesManager.GetRate(Convert.ToSingle(objDetails.Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum()), "ExtraBeds");
                        }
                        Session["RatesDetails"] = Supplier;
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1, rate = objDetails.Rate });


                    }
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, message = ex.Message });

            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateInventory(List<RatesManager.Supplier> Supplier)
        {
            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        tbl_CommonInventory Update = new tbl_CommonInventory();
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        try
                        {
                       using (var DB = new dbHotelhelperDataContext())
                            {
                                Update = DB.tbl_CommonInventories.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                if (Update != null)
                                    Update.Sold = (Convert.ToDecimal(Update.Sold) + Convert.ToDecimal(objDetails.noRooms)).ToString();
                                DB.SubmitChanges();
                            }
                        }

                        catch { }

                        Inventory = jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                    }

                }
            }
            else
            {
                Inventory = jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return Inventory;
        }

        [WebMethod(EnableSession = true)]
        public string StartSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    // var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) select obj).ToList();
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                   using (var DB = new dbHotelhelperDataContext())
                        {
                            var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) && obj.RateID == Convert.ToString(objDetails.Rate.RateID) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {
                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == false)
                                {
                                    tbl_CommonInventory set = DB.tbl_CommonInventories.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                    set.IsStop = true;
                                    DB.SubmitChanges();
                                }
                                else
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Alresdy Have Start Sale" });
                                }
                            }
                        }
                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Start Sale Done" });
                }

            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string StopSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    // var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) select obj).ToList();
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                   using (var DB = new dbHotelhelperDataContext())
                        {
                            var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) && obj.RateID == Convert.ToString(objDetails.Rate.RateID) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {

                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == true)
                                {
                                    tbl_CommonInventory set = DB.tbl_CommonInventories.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                    set.IsStop = false;
                                    DB.SubmitChanges();
                                }
                                else
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Alresdy Have Stop Sale" });
                                }
                            }
                        }

                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Stop Sale Done" });
                }

            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string CheckFreeSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
           using (var DB = new dbHotelhelperDataContext())
                {
                    foreach (var objDetails in Supplier[0].Details)
                    {

                        for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                        {
                            DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {
                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == false)
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "These Dates are Stop Sale" });
                                }
                            }
                        }
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "These Dates are Stop Sale Continue to Booking" });
                    }
                }
            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        //[WebMethod(EnableSession = true)]
        //public string GetRatelist(string HotelCode)
        //{
        //    try
        //    {
        //        var Supplier = AccountManager.GetSupplierByUser();
        //   using (var DB = new dbHotelhelperDataContext())
        //        {
        //            var RateList1 = (from obj in DB.tbl_commonRoomRates
        //                             join objc in DB.tbl_AdminLogins
        //                              on obj.SupplierId equals objc.sid
        //                             where obj.HotelID == Convert.ToInt64(HotelCode) && obj.Status == "Active" && obj.SupplierId == Supplier
        //                             select new
        //                             {
        //                                 obj.RoomId,
        //                                 obj.HotelRateID,
        //                                 obj.ValidNationality,
        //                                 obj.Checkin,
        //                                 Date = DateTime.ParseExact(obj.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture),
        //                                 obj.Checkout,
        //                                 obj.CurrencyCode,
        //                                 obj.RR,
        //                                 obj.RateType,
        //                                 obj.MealPlan,
        //                                 obj.DayWise,
        //                                 obj.MonRR,
        //                                 obj.TueRR,
        //                                 obj.WedRR,
        //                                 obj.ThuRR,
        //                                 obj.FriRR,
        //                                 obj.SatRR,
        //                                 obj.SunRR,
        //                                 objc.sid,
        //                             }).Distinct().ToList();
        //            var RateList = RateList1.OrderByDescending(s => s.Date).Distinct().ToList();

        //            List<RateList> ListNew = new List<RateList>();
        //            foreach (var item in RateList)
        //            {

        //                if (!ListNew.Exists(d => d.Checkin == item.Checkin && d.Checkout == item.Checkout && d.MealPlan == item.MealPlan))
        //                {

        //                    ListNew.Add(new RateList
        //                    {
        //                        RoomId = item.RoomId.ToString(),
        //                        HotelRateID = item.HotelRateID.ToString(),
        //                        ValidNationality = item.ValidNationality,
        //                        Checkin = item.Checkin,
        //                        Checkout = item.Checkout,
        //                        CurrencyCode = item.CurrencyCode,
        //                        RR = item.RR.ToString(),
        //                        RateType = item.RateType,
        //                        MealPlan = item.MealPlan,
        //                        Supplier = item.sid.ToString(),
        //                        DayWise = item.DayWise,
        //                        MonRR = item.MonRR.ToString(),
        //                        TueRR = item.TueRR.ToString(),
        //                        WedRR = item.WedRR.ToString(),
        //                        ThuRR = item.ThuRR.ToString(),
        //                        FriRR = item.FriRR.ToString(),
        //                        SatRR = item.SatRR.ToString(),
        //                        SunRR = item.SunRR.ToString(),

        //                    });


        //                }

        //            }

        //            return jsSerializer.Serialize(new { retCode = 1, Session = 1, RateList = ListNew });
        //        }
        //    }
        //    catch
        //    {
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

        //    }

        //}


        [WebMethod(EnableSession = true)]
        public string ChekAvailRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR)
        {
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                DateTime sCheckIn, sCheckOut;
                List<DateTime> ListDates = new List<DateTime>();
                //List<DateTime> ListOldDates = new List<DateTime>();
                for (int i = 0; i < CheckinR.Count; i++)
                {
                    sCheckIn = DateTime.ParseExact(CheckinR[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sCheckOut = DateTime.ParseExact(CheckoutR[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    double noDays = (sCheckOut - sCheckIn).TotalDays;

                    for (int j = 0; j <= noDays; j++)
                    {
                        ListDates.Add(sCheckIn.AddDays(j));

                    }
                }

                List<tbl_commonRoomRate> ListRoomRates = new List<tbl_commonRoomRate>();
                List<tbl_commonRoomRate> dGeneralData = new List<tbl_commonRoomRate>();
                List<tbl_commonRoomRate> dNationality = new List<tbl_commonRoomRate>();

           using (var DB = new dbHotelhelperDataContext())
                {
                    ListRoomRates = (from obj in DB.tbl_commonRoomRates where obj.HotelID == HotelCode && obj.SupplierId == objGlobalDefault.sid select obj).ToList();
                }
                //for (int i = 0; i < ListRoomID.Count; i++)
                //{
                //    dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && (DateTime.Parse(d.Checkin) <= DateTime.Parse(CheckinR[0]))).ToList();
                //    dNationality = ListRoomID.Where(d => (d.ValidNationality == Country)).ToList();
                //}


                bool AvlGenData, AvlNationality = false;
                var sRate = new List<tbl_commonRoomRate>();
                foreach (var Rate in ListRoomRates)
                {

                    List<DateTime> ListOldDates = new List<DateTime>();
                    if (Rate.MealPlan == MealPlan && Rate.CurrencyCode == Currency && Rate.SupplierId == objGlobalDefault.sid && Rate.Status == "Active" && Rate.RateType == "GN")
                    {

                        sCheckIn = DateTime.ParseExact(Rate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sCheckOut = DateTime.ParseExact(Rate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        double noDays = (sCheckOut - sCheckIn).TotalDays;

                        for (int j = 0; j <= noDays; j++)
                        {
                            ListOldDates.Add(sCheckIn.AddDays(j));

                        }
                        if (Rate.ValidNationality == Country)
                            AvlNationality = true;

                        bool result = ListOldDates.Any(x => ListDates.Contains(x));
                        if (result == true)
                            sRate.Add(Rate);


                    }
                }

                if (sRate.Count != 0)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, sRate = sRate });
                else
                    return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetURate(Int64 RateID)
        {
            try
            {
           using (var DB = new dbHotelhelperDataContext())
                {
                    var Rate = (from obj in DB.tbl_commonRoomRates where obj.HotelRateID == RateID select obj).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Rate = Rate });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        public class ListOfTaxes
        {

            public Int64 ID { get; set; }
            public Int64 TaxID { get; set; }
            public Int64 TaxOnID { get; set; }
            public Int64 IsAddOn { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 ServiceID { get; set; }
            public string ServiceName { get; set; }
            public Int64 ParentID { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetOtherRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR)
        {
            var dbTax = new dbHotelhelperDataContext();
            try
            {
                List<ListOfTaxes> ListTax = new List<ListOfTaxes>();
           using (var DB = new dbHotelhelperDataContext())
                {
                    var listRate = (from obj in DB.tbl_commonRoomRates where obj.HotelID == HotelCode && obj.SupplierId == Supplier && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.Checkin == CheckinR && obj.Checkout == CheckoutR && obj.ValidNationality.Contains(Country) select obj).ToList();

                    for (int i = 0; i < listRate.Count; i++)
                    {
                        var Tax = (from obj in dbTax.Comm_TaxMappings where obj.HotelID == HotelCode && obj.ServiceID == listRate[i].HotelRateID select obj).ToList();
                        for (int j = 0; j < Tax.Count; j++)
                        {
                            ListTax.Add(new ListOfTaxes
                            {
                                ID = Tax[j].ID,
                                TaxID = Tax[j].TaxID,
                                TaxOnID = Convert.ToInt64(Tax[j].TaxOnID),
                                IsAddOn = Convert.ToInt64(Tax[j].IsAddOns),
                                HotelID = Convert.ToInt64(Tax[j].HotelID),
                                ServiceID = Convert.ToInt64(Tax[j].ServiceID),
                                ServiceName = Tax[j].ServiceName

                            });
                        }

                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, listRate = listRate, ListTax = ListTax });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string FilterRate(List<string> RoomType, List<string> MealType, float MinPrice, float MaxPrice)
        {
            try
            {
                string AddSearchsession = HttpContext.Current.Session["session"].ToString();
                List<CommonLib.Response.RateGroup> objRateGroup = new List<RateGroup>();
                objRateGroup.Add(new RateGroup
                {
                    RoomOccupancy = RoomFilter.FilterRate(AddSearchsession, RoomType, MealType, MinPrice, MaxPrice),
                });

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, objRateGroup = objRateGroup });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }
    }
}
