﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for PackageDetailHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PackageDetailHandler : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string UpdatePackageBasicDetails(Int64 nID, string sPackageName, string sCity, string sCategory, Int64 nDuration, string dvalidFrom, string dvalidupto, string sThemes, string sDesctiption, Decimal dTax, Int64 nCancelDays, Decimal dCancelCharge, string sTermsCondition, string sInventory)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = PackageManager.UpdatePackageBasicDetails(nID, sPackageName, sCity, sCategory, nDuration, dvalidFrom, dvalidupto, sThemes, sDesctiption, dTax, nCancelDays, dCancelCharge, sTermsCondition, sInventory);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1, nID = nID });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPackageDetail(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //if (!Utils.ValidateSession())
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.SinglePackageDetails(nID, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_PackageDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         sPackageName = data.Field<string>("sPackageName"),
                         sPackageDestination = data.Field<string>("sPackageDestination"),
                         sPackageCategory = data.Field<string>("sPackageCategory"),
                         sPackageThemes = data.Field<string>("sPackageThemes"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sPackageDescription = data.Field<string>("sPackageDescription"),
                         dValidityFrom = data.Field<string>("dValidityFrom"),
                         dValidityTo = data.Field<string>("dValidityTo"),
                         dTax = data.Field<Decimal>("dTax"),
                         nCancelDays = data.Field<Int64>("nCancelDays"),
                         dCancelCharge = data.Field<Decimal>("dCancelCharge"),
                         sTermsCondition = data.Field<String>("sTermsCondition"),
                         sInventory = data.Field<String>("sInventory")
                     }).ToList();
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_PackageDetail = List_PackageDetail });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveItinerary(Int64 nID, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            PackageManager.DBReturnCode retCode = PackageManager.SaveItinerary(nID, listItinerary, nCategoryID, sCategoryName);
            if (retCode == PackageManager.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        //[WebMethod(EnableSession = true)]
        //public string SaveItinerary(Int64 nID, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        //{
        //    JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //    DBHelper.DBReturnCode retCode = PackageManager.SaveItinerary(nID, listItinerary, nCategoryID, sCategoryName);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        return objSerializer.Serialize(new { retCode = 1 });
        //    }
        //    else
        //    {
        //        return objSerializer.Serialize(new { retCode = 0 });
        //    }
        //}


        [WebMethod(EnableSession = true)]
        public string UpdatePricingDetails(Int64 nPackageID, Int64 nCategoryID, String nCategoryName, Decimal sSingleAdult, Decimal sCouple, Decimal sExtraAdult, Decimal sInfantKid, Decimal sKidWBed, Decimal sKidWOBed, String sStaticInclusion, String sDynamicInclusion, String sStaticExclusion, String sDynamicExclusion)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = PackageManager.UpdatePricingDetails(nPackageID, nCategoryID, nCategoryName, sSingleAdult, sCouple, sExtraAdult, sInfantKid, sKidWBed, sKidWOBed, sStaticInclusion, sDynamicInclusion, sStaticExclusion, sDynamicExclusion);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }



        [WebMethod(EnableSession = true)]
        public string GetPricingDetail(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //if (!Utils.ValidateSession())
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetPricingDetail(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_pricingDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         sCategoryName = data.Field<string>("sCategoryName"),
                         dSingleAdult = data.Field<Decimal>("dSingleAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dSingleAdult").ToString(),
                         dCouple = data.Field<Decimal>("dCouple").ToString() == "0.00" ? "" : data.Field<Decimal>("dCouple").ToString(),
                         dExtraAdult = data.Field<Decimal>("dExtraAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dExtraAdult").ToString(),
                         dInfantKid = data.Field<Decimal>("dInfantKid").ToString() == "0.00" ? "" : data.Field<Decimal>("dInfantKid").ToString(),
                         dKidWBed = data.Field<Decimal>("dKidWBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWBed").ToString(),
                         dKidWOBed = data.Field<Decimal>("dKidWOBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWOBed").ToString(),
                         sStaticInclusions = data.Field<string>("sStaticInclusions").Trim(','),
                         sDynamicInclusion = data.Field<string>("sDynamicInclusion").Trim(','),
                         sStaticExclusion = data.Field<string>("sStaticExclusion").Trim(','),
                         sDynamicExclusion = data.Field<string>("sDynamicExclusion").Trim(',')
                     }).ToList();
                Int64 nCount = Convert.ToInt64(dtResult.Rows.Count);
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_pricingDetail = List_pricingDetail, nCount = nCount });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetItineraryDetail(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //if (!Utils.ValidateSession())
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetItineraryDetail(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_ItineraryDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("nCategoryName"),
                         sItinerary_1 = data.Field<string>("sItinerary_1"),
                         sItinerary_2 = data.Field<string>("sItinerary_2"),
                         sItinerary_3 = data.Field<string>("sItinerary_3"),
                         sItinerary_4 = data.Field<string>("sItinerary_4"),
                         sItinerary_5 = data.Field<string>("sItinerary_5"),
                         sItinerary_6 = data.Field<string>("sItinerary_6"),
                         sItinerary_7 = data.Field<string>("sItinerary_7"),
                         sItinerary_8 = data.Field<string>("sItinerary_8"),
                         sItinerary_9 = data.Field<string>("sItinerary_9"),
                         sItinerary_10 = data.Field<string>("sItinerary_10"),
                         sItinerary_11 = data.Field<string>("sItinerary_11"),
                         sItinerary_12 = data.Field<string>("sItinerary_12"),
                         sItinerary_13 = data.Field<string>("sItinerary_13"),
                         sItinerary_14 = data.Field<string>("sItinerary_14"),
                         sItinerary_15 = data.Field<string>("sItinerary_15")
                     }).ToList();
                Int64 nCount = dtResult.Rows.Count;
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_ItineraryDetail = List_ItineraryDetail, nCount = nCount });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetItineraryHotelDetail(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //if (!Utils.ValidateSession())
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetItineraryHotelDetail(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_ItineraryHotelDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("sCategoryName"),
                         sHotelName_1 = data.Field<string>("sHotelName_1"),
                         sHotelName_2 = data.Field<string>("sHotelName_2"),
                         sHotelName_3 = data.Field<string>("sHotelName_3"),
                         sHotelName_4 = data.Field<string>("sHotelName_4"),
                         sHotelName_5 = data.Field<string>("sHotelName_5"),
                         sHotelName_6 = data.Field<string>("sHotelName_6"),
                         sHotelName_7 = data.Field<string>("sHotelName_7"),
                         sHotelName_8 = data.Field<string>("sHotelName_8"),
                         sHotelName_9 = data.Field<string>("sHotelName_9"),
                         sHotelName_10 = data.Field<string>("sHotelName_10"),
                         sHotelName_11 = data.Field<string>("sHotelName_11"),
                         sHotelName_12 = data.Field<string>("sHotelName_12"),
                         sHotelName_13 = data.Field<string>("sHotelName_13"),
                         sHotelName_14 = data.Field<string>("sHotelName_14"),
                         sHotel_Code_1 = data.Field<string>("sHotel_Code_1"),
                         sHotel_Code_2 = data.Field<string>("sHotel_Code_2"),
                         sHotel_Code_3 = data.Field<string>("sHotel_Code_3"),
                         sHotel_Code_4 = data.Field<string>("sHotel_Code_4"),
                         sHotel_Code_5 = data.Field<string>("sHotel_Code_5"),
                         sHotel_Code_6 = data.Field<string>("sHotel_Code_6"),
                         sHotel_Code_7 = data.Field<string>("sHotel_Code_7"),
                         sHotel_Code_8 = data.Field<string>("sHotel_Code_8"),
                         sHotel_Code_9 = data.Field<string>("sHotel_Code_9"),
                         sHotel_Code_10 = data.Field<string>("sHotel_Code_10"),
                         sHotel_Code_11 = data.Field<string>("sHotel_Code_11"),
                         sHotel_Code_12 = data.Field<string>("sHotel_Code_12"),
                         sHotel_Code_13 = data.Field<string>("sHotel_Code_13"),
                         sHotel_Code_14 = data.Field<string>("sHotel_Code_14"),
                         sHotelDescrption_1 = data.Field<string>("sHotelDescrption_1"),
                         sHotelDescrption_2 = data.Field<string>("sHotelDescrption_2"),
                         sHotelDescrption_3 = data.Field<string>("sHotelDescrption_3"),
                         sHotelDescrption_4 = data.Field<string>("sHotelDescrption_4"),
                         sHotelDescrption_5 = data.Field<string>("sHotelDescrption_5"),
                         sHotelDescrption_6 = data.Field<string>("sHotelDescrption_6"),
                         sHotelDescrption_7 = data.Field<string>("sHotelDescrption_7"),
                         sHotelDescrption_8 = data.Field<string>("sHotelDescrption_8"),
                         sHotelDescrption_9 = data.Field<string>("sHotelDescrption_9"),
                         sHotelDescrption_10 = data.Field<string>("sHotelDescrption_10"),
                         sHotelDescrption_11 = data.Field<string>("sHotelDescrption_11"),
                         sHotelDescrption_12 = data.Field<string>("sHotelDescrption_12"),
                         sHotelDescrption_13 = data.Field<string>("sHotelDescrption_13"),
                         sHotelDescrption_14 = data.Field<string>("sHotelDescrption_14"),
                         sHotelImage1 = data.Field<string>("sHotelImage1"),
                         sHotelImage2 = data.Field<string>("sHotelImage2"),
                         sHotelImage3 = data.Field<string>("sHotelImage3"),
                         sHotelImage4 = data.Field<string>("sHotelImage4"),
                         sHotelImage5 = data.Field<string>("sHotelImage5"),
                         sHotelImage6 = data.Field<string>("sHotelImage6"),
                         sHotelImage7 = data.Field<string>("sHotelImage7"),
                         sHotelImage8 = data.Field<string>("sHotelImage8"),
                         sHotelImage9 = data.Field<string>("sHotelImage9"),
                         sHotelImage10 = data.Field<string>("sHotelImage10"),
                         sHotelImage11 = data.Field<string>("sHotelImage11"),
                         sHotelImage12 = data.Field<string>("sHotelImage12"),
                         sHotelImage13 = data.Field<string>("sHotelImage13"),
                         sHotelImage14 = data.Field<string>("sHotelImage14")
                     }).ToList();
                Int64 nCount = dtResult.Rows.Count;
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_ItineraryHotelDetail = List_ItineraryHotelDetail, nCount = nCount });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetProductImages(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetProductImages(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                var List_ImageDetail = dtResult.AsEnumerable()
                    .Select(data => new
                    {
                        nID = data.Field<Int64>("nID"),
                        nPackageID = data.Field<Int64>("nPackageID"),
                        ImageArray = data.Field<string>("ImageArray"),
                    }).ToList();
                return objSerializer.Serialize(new { Session = 1, retCode = 1, List_ImageDetail = List_ImageDetail });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string add_ProductImagesData(Int64 nID, string arr_Image)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = PackageManager.add_ProductImagesData(nID, arr_Image);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string DeletePackage(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = PackageManager.DeletePackage(nID);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }



    }
}
