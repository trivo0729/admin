﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for PromoImageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PromoImageHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        string json = "";
        int i;
        [WebMethod(EnableSession = true)]
        public string GetPromoImageDetail()
        {


            try
            {
                using (var db = new CutAdmin.dbml.helperDataContext())
                {
                    var List = (from obj in DB.tbl_PromoImages select obj).ToList();
                    if (List.Count != 0)
                    {
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_PromoImage = List });
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }


            }

            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }
        [WebMethod(EnableSession = true)]
        public string InsertChangePromoImage(Int64 sid)
        {
            try
            {
                int rowsAffected = 0;
                bool ShowFlag = false;
                if (sid != 0)
                {
                    ShowFlag = true;

                    var data = (from obj in DB.tbl_PromoImages where obj.sid == sid select obj).First();

                    data.ShowFlag = ShowFlag;
                    DB.SubmitChanges();

                    var data2 = (from obj in DB.tbl_PromoImages where obj.sid != sid select obj).ToList();
                    for (i = 0; i < data2.Count; i++)
                    {
                        data2[i].ShowFlag = false;
                        DB.SubmitChanges();
                    }

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        [WebMethod(true)]
        public string DeletePromoImage(Int64 sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_PromoImage Delete = DB.tbl_PromoImages.Single(x => x.sid == sid);
                    DB.tbl_PromoImages.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    //tbl_Contact Condlt = DB.tbl_Contacts.Single(y => y.ContactID == contactId);
                    //DB.tbl_Contacts.DeleteOnSubmit(Condlt);
                    //DB.SubmitChanges();
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
    }
}
