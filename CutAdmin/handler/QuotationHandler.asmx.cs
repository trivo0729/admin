﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for QuotationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class QuotationHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        string json = "";
        [WebMethod(EnableSession = true)]
        public string GetAllQuotationDetail()
        {
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_QuotationDetails select obj).ToList();
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string SearchQuotationDetail(string CompanyName)
        {
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_QuotationDetails where obj.Companyname.Contains(CompanyName) select obj).ToList();
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAgentDetail()
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            objSerializer.MaxJsonLength = int.MaxValue;
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_AdminLogins
                            select new
                            {
                                obj.sid,
                                obj.AgencyName,
                            }).ToList().Distinct();
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, List_Agent = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }
        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CountryList = (from obj in DB.tbl_HCities
                                       select new
                                       {
                                           obj.Countryname,
                                           obj.Country,
                                       }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, Country = CountryList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var CityList = (from obj in DB.tbl_HCities
                                    where obj.Countryname == country
                                    select new
                                    {
                                        obj.Description,
                                        obj.Code,
                                    }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, City = CityList });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }
        [WebMethod(EnableSession = true)]
        public string GetState()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                    var List = (from obj in DB.tbl_GstStates
                                select new
                                {
                                    obj.SateName,
                                    obj.StateID,
                                }).ToList().Distinct();

                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, List = List });
                }
            }

            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetStaffDetails()
        {
            string json = "";
            try
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 ParentId = objGlobalDefault.sid;
                var List = (from obj in DB.tbl_StaffLogins
                            join objcont in DB.tbl_Contacts on obj.ContactID equals objcont.ContactID
                            //where obj.ParentId == ParentId
                            select obj
                            ).ToList().Distinct();

                json = objSerializer.Serialize(new { Session = 1, retCode = 1, Staff = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string GetAgencyDetails(Int64 AgentID)
        {
            helperDataContext DB = new helperDataContext();
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                var arrAgency = (from obj in DB.tbl_AdminLogins
                                 from objContact in DB.tbl_Contacts
                               
                                 where obj.ContactID == objContact.ContactID &&
                                 obj.sid == AgentID
                                 select new
                                 {
                                     obj.sid,
                                     obj.AgencyName,
                                     obj.Agentuniquecode,
                                     obj.ContactPerson,
                                     obj.Designation,
                                     objContact.Address,
                                     Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                     Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                     objContact.PinCode,
                                     obj.uid,
                                     obj.password,
                                    
                                     objContact.phone,
                                     objContact.email,
                                     objContact.Mobile,
                                     objContact.Fax,
                                     objContact.Website,
                                     obj.IATANumber,
                                     obj.UserType,
                                     obj.dtLastAccess,
                                     obj.EnableCheckAccount,
                                     obj.Updatedate,
                                     obj.LoginFlag,
                                     obj.CurrencyCode,
                                     obj.ParentID,
                                     obj.agentCategory,
                                     obj.PANNo,
                                     obj.GSTNumber,

                                 }).FirstOrDefault();



                return objSerializer.Serialize(new { retCode = 1, arrAgency = arrAgency });
            }
            catch
            {
                return objSerializer.Serialize(new { });
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateQuotation(Int64 ID, string Date, string Quoteto, string Companyname, string Email, string Phone, string Mobile, string Contactperson, string Address, string City, string State, string Country, string Leadguestname, string Adults, string Child, string Infant, string TourStart, string TourEnd, string QuotationDetails, string Quotedby, string Currentstatus, string NoteOrRemark)
        {
            string json = "";
            try
            {
                tbl_QuotationDetail Update = DB.tbl_QuotationDetails.Single(x => x.Sid == ID);
                Update.Date = Date;
                Update.QuotedTo = Quoteto;
                Update.Companyname = Companyname;
                Update.Email = Email;
                Update.Phone = Phone;
                Update.Mobile = Mobile;
                Update.Contactperson = Contactperson;
                Update.Leadguestname = Leadguestname;
                Update.Address = Address;
                Update.City = City;
                Update.State = State;
                Update.Country = Country;
                Update.Adults = Adults;
                Update.Child = Child;
                Update.Infant = Infant;
                Update.TourStart = TourStart;
                Update.TourEnd = TourEnd;
                Update.QuotationDetails = QuotationDetails;
                Update.Quotedby = Quotedby;
                Update.Currentstatus = Currentstatus;
                Update.Remark = NoteOrRemark;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string AddQuotation(string Date, string Quoteto, string Companyname, string Email, string Phone, string Mobile, string Contactperson, string Address, string City, string State, string Country, string Leadguestname, string Adults, string Child, string Infant, string TourStart, string TourEnd, string QuotationDetails, string Quotedby, string Currentstatus, string NoteOrRemark)
        {
            string json = "";
            try
            {

                tbl_QuotationDetail Add = new tbl_QuotationDetail();
                Add.Date = Date;
                Add.QuotedTo = Quoteto;
                Add.Companyname = Companyname;
                Add.Email = Email;
                Add.Phone = Phone;
                Add.Mobile = Mobile;
                Add.Contactperson = Contactperson;
                Add.Leadguestname = Leadguestname;
                Add.Address = Address;
                Add.City = City;
                Add.State = State;
                Add.Country = Country;
                Add.Adults = Adults;
                Add.Child = Child;
                Add.Infant = Infant;
                Add.TourStart = TourStart;
                Add.TourEnd = TourEnd;
                Add.QuotationDetails = QuotationDetails;
                Add.Quotedby = Quotedby;
                Add.Currentstatus = Currentstatus;
                Add.Remark = NoteOrRemark;
                DB.tbl_QuotationDetails.InsertOnSubmit(Add);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetQuotationByID(Int64 Id)
        {
            string json = "";
            try
            {
                string QuotedTo; 
                var List = (from obj in DB.tbl_QuotationDetails
                            join meta in DB.tbl_AdminLogins on Convert.ToInt64(obj.QuotedTo) equals meta.sid
                            join objcont in DB.tbl_StaffLogins on Convert.ToInt64(obj.Quotedby) equals objcont.sid
                            where obj.Sid == Id
                            select new
                            {
                                obj.Sid,
                                obj.Date,
                                obj.QuotedTo,
                                obj.Quotedby,
                                //AgencyName = (from objagancyname in DB.tbl_AdminLogins where objagancyname.sid = Convert.ToInt64(obj.QuotedTo) select objagancyname.AgencyName).FirstOrDefault().AgencyName,
                                //ContactPerson = (from objcontcper in DB.tbl_StaffLogins where objcontcper.sid = obj.Quotedby select objcontcper.ContactPerson).FirstOrDefault().ContactPerson,
                                obj.QuoteNo,
                                obj.Companyname,
                                obj.Email,
                                obj.Phone,
                                obj.Mobile,
                                obj.Contactperson,
                                obj.Address,
                                obj.City,
                                obj.State,
                                obj.Country,
                                obj.Leadguestname,
                                obj.Adults,
                                obj.Child,
                                obj.Infant,
                                obj.TourStart,
                                obj.TourEnd,
                                obj.QuotationDetails,
                                obj.Currentstatus,
                                obj.Remark,
                                obj.Importantnote,
                                meta.AgencyName,
                                objcont.ContactPerson

                                

                            }).ToList();

                //var List = (from obj in DB.tbl_QuotationDetails where obj.Sid == Id select obj).ToList();
                //var List = (from obj in DB.tbl_QuotationDetails
                //            where obj.Sid == Id select obj
                //            select new
                //                 {
                //                 obj.sid;
                //                 }).ToList();

                json = objSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod]
        public string DeleteQuotationByID(Int64 Id)
        {
            string json = "";
            try
            {

                tbl_QuotationDetail Delete = DB.tbl_QuotationDetails.Single(x => x.Sid == Id);
                DB.tbl_QuotationDetails.DeleteOnSubmit(Delete);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }
}
