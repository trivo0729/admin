﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.DataLayer;
using CommonLib.Response;
namespace CutAdmin.handler
{
    /// <summary>
    /// Search Rates  handler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
   
    [System.Web.Script.Services.ScriptService]
    public class SearchHandler : System.Web.Services.WebService
    {
        #region SearchHotel
        [WebMethod(EnableSession = true)]
        public string SearchHotel(Int64[] HotelCode, string Destination, string Checkin, string Checkout, string[] nationality, int Nights, int Adults, int Childs, string Supplier, string MealPlan, string CurrencyCode, string AddSearchsession, string SearchValid)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            List<CommonHotelDetails> ListHotels = new List<CommonHotelDetails>();
            List<string> arrSupplier = new List<string>();
            try
            {
                if (Session["CommonList" + AddSearchsession] == null)
                {
                    Session["CommonList"] = AddSearchsession;
                    ListHotels = SearchManager.GetHotelDetails(HotelCode, Checkin, Checkout, nationality, SearchValid, AddSearchsession);
                    if (ListHotels.Count != 0)
                    {
                        arrSupplier = ListHotels.Select(d => d.Supplier).Distinct().ToList();
                    }
                    string json = objserialize.Serialize(new { Session = 1, retCode = 1, ListHotels = ListHotels, SupplierList = arrSupplier, Filter = ""});
                    return objserialize.Serialize(new { Session = 1, retCode = -2, ErrorMessage = "Hotel is not available for these Dates" });
                }
                else
                {
                    jsonString = Session["CommonList" + AddSearchsession].ToString();
                    return jsonString;
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = ex.Message });
            }
        }

        #endregion

        #region Get Search
        
        #endregion

    }
}
