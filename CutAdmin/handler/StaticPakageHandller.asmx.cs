﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for StaticPakageHandller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class StaticPakageHandller : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        string json = "";

        [WebMethod(true)]
        public string  GetStaticPakages(string Currency)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_StaticPackages where obj.Currency == Currency orderby obj.sid descending select obj).Distinct().ToList();



                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_Pakages = List });


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(true)]
        public string ActiveDeactive(Int64 sid, string status)
        {

            try
            {
                tbl_StaticPackage Data = DB.tbl_StaticPackages.Single(x => x.sid == sid);

                if (status == "True")
                {
                    Data.Status = "False";
                }
                else
                {
                    Data.Status = "True";
                }

                DB.SubmitChanges();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string AddPackages(string Packagename, string Location, string Price, string Stay, string Rating, string Currency, string ValidUpto)
        {

            try
            {
                 using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_StaticPackage obj = new tbl_StaticPackage();
                    obj.Package_Name = Packagename;
                    obj.Pakages_Price = Price;
                    obj.Stay = Stay;
                    obj.Location = Location;
                    obj.Currency = Currency;
                    obj.StarRating = Rating;
                    obj.MarchandId = Franchisee;
                    obj.ValidUpto = ValidUpto;
                    obj.Status = "True";
                    DB.tbl_StaticPackages.InsertOnSubmit(obj);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string UpdatePackages(Int64 sid, string Packagename, string Location, string Price, string Stay, string Rating, string Currency, string ValidUpto)
        {

            try
            {
                using (var DB = new helperDataContext())
                {
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_StaticPackage updPackage = DB.tbl_StaticPackages.Single(y => y.sid == sid);
                    updPackage.Package_Name = Packagename;
                    updPackage.Pakages_Price = Price;
                    updPackage.Stay = Stay;
                    updPackage.Location = Location;
                    updPackage.Currency = Currency;
                    updPackage.StarRating = Rating;
                    updPackage.MarchandId = Franchisee;
                    updPackage.ValidUpto = ValidUpto;
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
           [WebMethod(true)]
         public string Delete(Int64 sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelhelperDataContext DB = new dbHotelhelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new helperDataContext())
                {
                    tbl_StaticPackage Delete = DB.tbl_StaticPackages.Single(x => x.sid == sid);
                    DB.tbl_StaticPackages.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    //tbl_Contact Condlt = DB.tbl_Contacts.Single(y => y.ContactID == contactId);
                    //DB.tbl_Contacts.DeleteOnSubmit(Condlt);
                    //DB.SubmitChanges();
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
     
    }
}
