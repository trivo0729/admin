﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;
using CutAdmin.BL;
using System.Web.Script.Serialization;
using CutAdmin.DataLayer;
using CommonLib.Response;
using CutAdmin.dbml;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for TaxHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TaxHandler : System.Web.Services.WebService
    {
        //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region SAVE TAXES
        [WebMethod(EnableSession = true)]
        public string SaveTax(Comm_Tax objTax)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
               using (var DB = new dbHotelhelperDataContext())
                {
                    if (objTax.ID == 0)
                    {
                        if (HttpContext.Current.Session["LoginUser"] == null)
                            throw new Exception("Session Expired ,Please Login and try Again");
                        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];

                        string UserType = objGlobalDefault.UserType;
                        Int64 Uid = 0;
                        CutAdmin.GenralHandler objgnrl = new CutAdmin.GenralHandler();
                        Uid = objgnrl.GetUserID(UserType);


                        //Int64 Uid = objGlobalDefault.sid;
                        //if (objGlobalDefault.UserType != "Supplier")
                        //    Uid = objGlobalDefault.ParentId;
                        if (DB.Comm_Taxes.Where(d => d.Name == objTax.Name && d.UserID == Uid).ToList().Count != 0)
                            return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add" });
                        objTax.UserID = Uid;
                        DB.Comm_Taxes.InsertOnSubmit(objTax);
                        DB.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                    else
                    {
                        var arrTax = DB.Comm_Taxes.Where(d => d.ID == objTax.ID).FirstOrDefault();
                        arrTax.Name = objTax.Name;
                        arrTax.ParentTaxID = objTax.ParentTaxID;
                        arrTax.Value = objTax.Value;
                        arrTax.Desciption = objTax.Desciption;
                        arrTax.Active = objTax.Active;
                        DB.SubmitChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                }
            }
            catch(Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0 ,ErrorMsg=ex.Message});
            }

        }
        [WebMethod(EnableSession = true)]
        public string ActiveTax(Comm_Tax objTax)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
               using (var DB = new dbHotelhelperDataContext())
                {
                    var arrTax = DB.Comm_Taxes.Where(d => d.ID == objTax.ID).FirstOrDefault();
                    arrTax.Name = objTax.Name;
                    arrTax.ParentTaxID = objTax.ParentTaxID;
                    arrTax.Value = objTax.Value;
                    arrTax.Desciption = objTax.Desciption;
                    arrTax.Active = objTax.Active;
                    DB.Comm_Taxes.InsertOnSubmit(objTax);
                    DB.SubmitChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string DeleteTax(Int64 Taxid)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
               using (var DB = new dbHotelhelperDataContext())
                {
                    var arrTax = DB.Comm_Taxes.Where(d => d.ID == Taxid).FirstOrDefault();
                    DB.Comm_Taxes.DeleteOnSubmit(arrTax);
                    DB.SubmitChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadTax()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                var UserType = objGlobalDefault.UserType;
                Int64 Uid = 0;
                CutAdmin.GenralHandler objgnrl = new CutAdmin.GenralHandler();
                Uid = objgnrl.GetUserID(UserType);

                //Int64 Uid = objGlobalDefault.sid;
                //if (objGlobalDefault.UserType != "Supplier")
                //    Uid = objGlobalDefault.ParentId;
               using (var DB = new dbHotelhelperDataContext())
                {
                    var arrTax = (from obj in DB.Comm_Taxes
                                  where obj.UserID == Uid
                                  select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrTax = arrTax });
                }
            }
            catch(Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion

        #region Tax Mapping
        [WebMethod(EnableSession = true)]
        public string SaveTaxMapping(List<Comm_TaxMapping> objTaxReturn)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
               using (var DB = new dbHotelhelperDataContext())
                {
                    DB.Comm_TaxMappings.InsertAllOnSubmit(objTaxReturn);
                    DB.SubmitChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }
        #endregion


        #region Get TaxMapping & Taxces
        [WebMethod(EnableSession = true)]
        public string GetTaxMapping(Int64 HotelID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new dbHotelhelperDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UID = objGlobalDefault.sid;
                    if (objGlobalDefault.UserType == "SupplierStaff")
                        UID = objGlobalDefault.ParentId;
                    var arrTax = (from obj in dbTax.Comm_Taxes
                                  where obj.UserID == UID && obj.Active == true
                                  select obj).ToList();
                    var arrChargeRate = (from obj in dbTax.Comm_TaxMappings
                                         where obj.HotelID == HotelID && obj.IsAddOns == false
                                         select obj).ToList();

                    //var arrMeals = (from obj in db.Comm_AddOns where obj.IsMeal == true && obj.UserId == UID select obj).ToList().OrderBy(d => d.IsMeal).ToList();
                    var arrAddOns = (from obj in dbTax.Comm_AddOns where obj.UserId == UID select obj).ToList().OrderBy(d => d.IsMeal).ToList();
                    if (arrAddOns.Count == 0 || arrAddOns.All(D => D.IsMeal == false))
                    {
                        arrAddOns = new List<Comm_AddOn>();
                        arrAddOns.Add(new Comm_AddOn
                        {
                            AddOnName = "Breakfast",
                            Details = "Breakfast",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "BB,HB,FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOn
                        {
                            AddOnName = "Lunch",
                            Details = "Lunch",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "HB,FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOn
                        {
                            AddOnName = "Dinner",
                            Details = "Dinner",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOn
                        {
                            AddOnName = "Hi-Tea",
                            Details = "Hi-Tea",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "FB",
                            UserId = UID
                        });
                        dbTax.Comm_AddOns.InsertAllOnSubmit(arrAddOns);
                        dbTax.SubmitChanges();
                    }
                    var arrAddOnsTax = (from obj in dbTax.Comm_TaxMappings
                                        where obj.HotelID == HotelID && obj.IsAddOns == true
                                        select obj).ToList();
                    return jsSerializer.Serialize(new
                    {
                        retCode = 1,
                        arrTax = arrTax,
                        arrChargeRate = arrChargeRate,
                        arrAddOns = arrAddOns,
                        arrAddOnsTax = arrAddOnsTax,
                    });
                }
            }
            catch(Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion


        #region Get AddOnsMapping & Taxces
        [WebMethod(EnableSession = true)]
        public string GetAddOnsMapping(Int64 HotelID,Int64 RoomId)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 UID = objGlobalDefault.sid;
                    if (objGlobalDefault.UserType == "SupplierStaff")
                        UID = objGlobalDefault.ParentId;
                    var arrTax = (from obj in DB.Comm_Taxes
                                  where obj.UserID == UID && obj.Active == true
                                  select obj).ToList();
                    var arrAddOns = (from obj in DB.Comm_AddOns
                                     where obj.UserId == UID && obj.IsMeal == false
                                     select obj).ToList();

                    var arrMeals = (from obj in DB.Comm_AddOns
                                    where obj.UserId == UID && obj.IsMeal == true
                                    select obj).ToList();

                    var arrAddOnsRate = (from obj in DB.Comm_TaxMappings
                                         where obj.HotelID == HotelID && obj.IsAddOns == true
                                         select obj).ToList();

                    var arrAddOnsPrice = (from obj in DB.Comm_AddOnsRates
                                          where obj.HotelID == HotelID && obj.RateID == RoomId
                                          select obj).ToList();

                    List<object> arrMealRate = new List<object>();
                    foreach (var objAddOns in arrAddOnsPrice)
                    {
                        var sMeals = (from obj in DB.Comm_AddOns where obj.ID == objAddOns.AddOnsID select obj).FirstOrDefault();
                        arrMealRate.Add(new
                        {
                            sMeals.AddOnName,
                            sMeals.IsMeal,
                            sMeals.MealType,
                            Rate = objAddOns.Rate,
                            objAddOns.IsCumpulsury,
                            objAddOns.RateType,

                        });
                    }
                    return jsSerializer.Serialize(new
                    {
                        retCode = 1,
                        arrTax = arrTax,
                        arrAddOnsRate = arrAddOnsRate,
                        arrAddOns = arrAddOns,
                        arrMeals = arrMeals,
                        arrAddOnsPrice = arrMealRate
                    });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion


        #region GetAddOnsByRoom
        [WebMethod(EnableSession = true)]
        public string GetAddOnsByRoom(Int64 HotelID, Int64 RateTypeID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new dbHotelhelperDataContext())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    var arrTax = (from obj in DB.Comm_Taxes
                                  where obj.UserID == objGlobalDefault.sid
                                  select obj).ToList();
                    var arrAddOns = (from obj in DB.Comm_AddOns
                                     where obj.UserId == objGlobalDefault.sid
                                     select obj).ToList();
                    var arrAddOnsRate = (from obj in DB.Comm_TaxMappings
                                         where obj.HotelID == HotelID && obj.IsAddOns == true && obj.ServiceID == RateTypeID
                                         select new
                                         {
                                             ID = obj.TaxID
                                         }).Distinct().ToList();
                    List<TaxRate> ListAddOnsRate = new List<TaxRate>();
                    foreach (var objAddOns in arrAddOnsRate)
                    {
                        var ListTaxes = DB.Comm_TaxMappings.Where(r => r.TaxID == objAddOns.ID && r.ServiceID == RateTypeID && r.IsAddOns == true).ToList();
                        TaxRate objRate = new TaxRate();
                        objRate.RateName = arrAddOns.Where(D => D.ID == objAddOns.ID).FirstOrDefault().AddOnName;
                        objRate.Type = arrAddOns.Where(D => D.ID == objAddOns.ID).FirstOrDefault().Type;
                        objRate.BaseRate = Convert.ToSingle(ListTaxes.FirstOrDefault().ServiceName);
                        objRate.TaxOn = new List<Tax>();
                        foreach (var objTax in ListTaxes)
                        {
                            if (DB.Comm_Taxes.Where(d => d.ID == objTax.TaxOnID).ToList().Count != 0)
                            {
                                objRate.TaxOn.Add(new Tax
                                {
                                    TaxName = DB.Comm_Taxes.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Name,
                                    TaxPer = Convert.ToSingle(DB.Comm_Taxes.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Value),
                                    TaxRate = Convert.ToSingle(DB.Comm_Taxes.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Value) * objRate.BaseRate / 100,
                                });
                            }
                            else if (objTax.TaxOnID == 0)
                            {
                                objRate.TaxOn.Add(new Tax
                                {
                                    TaxName = "Base Rate",
                                    TaxPer = 0,
                                    TaxRate = 0,
                                });
                            }

                        }
                        objRate.TotalRate = objRate.BaseRate + objRate.TaxOn.Select(d => d.TaxRate).ToList().Sum();
                        ListAddOnsRate.Add(objRate);
                    }


                    return jsSerializer.Serialize(new { retCode = 1, arrAddOnsRate = ListAddOnsRate });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        public  List<TaxRate> GetRates(Int64 HotelCode)
        {
            List<TaxRate> arrRates = new List<TaxRate>();
            try
            {
               using (var DB = new dbHotelhelperDataContext())
                {
                    var TaxRates = DB.Comm_TaxMappings.Where(d => d.HotelID == HotelCode && d.IsAddOns == false).ToList();
                    foreach (var objRate in TaxRates.Select(d => d.TaxID).Distinct().ToList())
                    {
                        var ListTaxes = TaxRates.Where(r => r.TaxID == objRate).ToList().Select(d => d.TaxOnID).Distinct().ToList();
                        List<Tax> arrTax = new List<Tax>();
                        foreach (var objTax in ListTaxes)
                        {
                            if (DB.Comm_Taxes.Where(d => d.ID == objTax).ToList().Count != 0)
                            {
                                arrTax.Add(new Tax
                                {
                                    TaxName = DB.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Name,
                                    TaxPer = Convert.ToSingle(DB.Comm_Taxes.Where(d => d.ID == objTax).FirstOrDefault().Value),
                                    TaxRate = 0,
                                });
                            }
                            else if (objTax == 0)
                            {
                                arrTax.Add(new Tax
                                {
                                    TaxName = "Base Rate",
                                    TaxPer = Convert.ToSingle(DB.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Value),
                                    TaxRate = 0,
                                });
                            }

                        }
                        if (DB.Comm_Taxes.Where(d => d.ID == objRate).ToList().Count != 0)
                        {
                            arrRates.Add(new TaxRate
                            {

                                RateName = DB.Comm_Taxes.Where(d => d.ID == objRate).FirstOrDefault().Name,
                                TaxOn = arrTax
                            });
                        }
                    }
                }
            }
            catch
            {

            }
            return arrRates;
        }
        #endregion


        #region Tax Configration
        //[WebMethod(EnableSession = true)]
        //public string Configration(bool bOwn, bool bHotel, bool bNetShow)
        //{
        //    jsSerializer = new JavaScriptSerializer();
        //    try
        //    {
        //        Int64 Uid = AccountManager.GetSupplierByUser();
        //        using (var db = new helperDataContext())
        //        {
        //            var arrLogin = (from obj in db.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault();
        //            arrLogin.HotelTax = bHotel;
        //            arrLogin.ShowNet = bNetShow;
        //            arrLogin.OwnTax = bOwn;
        //            db.SubmitChanges();
        //        }
        //        return jsSerializer.Serialize(new { retCode = 1 });
        //    }
        //    catch (Exception ex)
        //    {
        //        return jsSerializer.Serialize(new { retCode = 1, ex = ex.Message });
        //    }
        //}
        #endregion

//******************************************* Code for AddTaxces.aspx *********************************************************

       

        // Get Tax Details
        [WebMethod(EnableSession = true)]
        public string GetTaxDetails()
        
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                helperDataContext db = new helperDataContext();
                var Sevices = (from obj in db.tbl_TAXes select obj.Service).Distinct().ToList(); //where obj.ParentID == objGlobalDefault.sid
                var data = TaxManager.GetTaxByService(Sevices[0]);
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                    TaxDetails = data,
                    Sevices = Sevices
                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }

        // Get Tax By Service
        [WebMethod(EnableSession = true)]
        public string GetTaxService(string Service)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                var data = TaxManager.GetTaxByService(Service);
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                    TaxDetails = data,
                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }


        // Save Service 
        [WebMethod(EnableSession = true)]
        public string AddServiceDetails(string Service, decimal Gst, decimal OnMarkupGST, bool Active, Int64 tid, decimal Vat, decimal TDS)
        {
            helperDataContext db = new helperDataContext();
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            try
            {
                if (tid != 0)
                {
                    var data = (from obj in db.tbl_TAXes where obj.tID == tid select obj).First();
                    data.GstTax = Gst;
                    data.MarkupGstTax = OnMarkupGST;
                    data.OnMarkup = Active;
                    data.VatTax = Vat;
                    data.TDS = TDS;
                    db.SubmitChanges();

                }
                else
                {
                    tbl_TAX data = new tbl_TAX
                    {
                        Service = Service,
                        GstTax = Gst,
                        OnMarkup = Active,
                        MarkupGstTax = OnMarkupGST,
                        //ParentID = objGlobalDefault.sid,
                        UpdateDate = DateTime.Now.ToShortDateString(),
                        VAT = 0,
                        VatTax = Vat,
                        TDS = TDS
                    };
                    db.tbl_TAXes.InsertOnSubmit(data);
                    db.SubmitChanges();
                }
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }

    }
}
