﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for TransactionHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class TransactionHandler : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        [WebMethod(EnableSession = true)]
        public string GetTransactions()
        {
            Int32 Get = 10;
            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode = TransactionManager.GetTransactions(out dtResult);

            DataView myDataView = dtResult.DefaultView;
            myDataView.Sort = "Sid DESC";
            //myDataView.RowFilter = "Particulars NOT LIKE '%One%' OR Particulars NOT LIKE '%Fix%'";
            //myDataView.RowFilter = "Particulars NOT LIKE '%Fix%'";
            DataTable SorteddtResult = myDataView.ToTable();
            Session["PaggingSession"] = SorteddtResult;

            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                    IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0);
                    SorteddtResult = allButFirst.CopyToDataTable();
                    string Count = (dtResult.Rows.Count).ToString();
                    List<Dictionary<string, object>> sAgentStatment = new List<Dictionary<string, object>>();
                    sAgentStatment = JsonStringManager.ConvertDataTable(SorteddtResult); dtResult.Dispose();
                    SorteddtResult.Dispose();
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, AgencyName = AgencyName, Count = Count, AgentStatement = sAgentStatment });

                }
                catch
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    SorteddtResult = dtResult.Clone();
                }
            }


            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"AgentStatement\":\"0\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetTransactionByDate(string dFrom, string dTo, string TransType, string RefNo)
        {
            Int32 Get = 10;
            string jsonString = "";
            DataTable dtResult = null;
            DataRow[] rows = null;
            DateTime From = DateTime.Now;
            DateTime To = DateTime.Now;
            DataTable SorteddtResult = null;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;

            if (objGlobalDefaults != null)
            {
                DataTable dtSession = (DataTable)HttpContext.Current.Session["PaggingSession"];
                dtResult = dtSession.Copy();
                if (dtResult != null)
                {
                    if (TransType == "Credited")
                    {
                        dtResult.Columns.Add("Credit", typeof(string));
                        dtResult.Columns.Add("Debit", typeof(string));

                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            dtResult.Rows[i]["Credit"] = (dtResult.Rows[i]["CreditedAmount"]).ToString();

                            dtResult.Rows[i]["Debit"] = (dtResult.Rows[i]["DebitedAmount"]).ToString();
                        }

                        IEnumerable<DataRow> allButFirst = dtResult.AsEnumerable().Where(data => data.Field<string>("Debit") == "");

                        try
                        {
                            dtResult = allButFirst.CopyToDataTable();
                        }
                        catch
                        {
                            return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        }



                    }

                    if (TransType == "Debited")
                    {
                        dtResult.Columns.Add("Credit", typeof(string));
                        dtResult.Columns.Add("Debit", typeof(string));

                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            dtResult.Rows[i]["Credit"] = (dtResult.Rows[i]["CreditedAmount"]).ToString();

                            dtResult.Rows[i]["Debit"] = (dtResult.Rows[i]["DebitedAmount"]).ToString();
                        }
                        IEnumerable<DataRow> allButFirst = dtResult.AsEnumerable().Where(data => data.Field<string>("Credit") == "");
                        try
                        {
                            dtResult = allButFirst.CopyToDataTable();
                        }
                        catch
                        {
                            return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        }

                    }


                    if (dFrom != "" || dTo != "")
                    {
                        //From = ConvertDateTime(dFrom);
                        From = DateTime.ParseExact(dFrom, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        To = DateTime.ParseExact(dTo, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        //To = ConvertDateTime(dTo);
                        dtResult.Columns.Add("Date", typeof(DateTime));
                        string Chk_Dt = "";
                        DateTime Chk_Dt_Converted = DateTime.Now;
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            Chk_Dt = (dtResult.Rows[i]["TransactionDate"]).ToString();
                            //Chk_Dt_Converted = ConvertDateTime(Chk_Dt);
                            Chk_Dt_Converted = DateTime.ParseExact(Chk_Dt.Split(' ')[0], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            dtResult.Rows[i]["Date"] = Chk_Dt_Converted;
                        }
                        rows = dtResult.Select("(Date  >= '#" + From + "#' AND Date <= '#" + To + "#')");
                        if (rows.Length == 0)
                        {
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                            return jsonString;
                        }
                        dtResult = rows.CopyToDataTable();

                        try
                        {
                            dtResult = rows.CopyToDataTable();
                        }
                        catch
                        {
                            return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        }
                    }


                    if (RefNo != "")
                    {
                        rows = dtResult.Select("(ReferenceNo like '%" + RefNo + "%')");
                        if (rows.Length == 0)
                        {
                            jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                            return jsonString;
                        }
                        // dtResult = rows.CopyToDataTable();

                        try
                        {
                            dtResult = rows.CopyToDataTable();
                        }
                        catch
                        {
                            return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        }
                    }

                    try
                    {
                        string Count = (dtResult.Rows.Count).ToString();
                        Session["PaggingSearch"] = dtResult;
                        IEnumerable<DataRow> allButFirst = dtResult.AsEnumerable().Skip(0).Take(Get);
                        SorteddtResult = allButFirst.CopyToDataTable();
                        //  SorteddtResult = dtResult.Copy();
                        DataView myDataView = SorteddtResult.DefaultView;
                        myDataView.Sort = "Sid DESC";
                        SorteddtResult = myDataView.ToTable();
                        jsonString = "";
                        foreach (DataRow dr in SorteddtResult.Rows)
                        {
                            jsonString += "{";
                            foreach (DataColumn dc in SorteddtResult.Columns)
                            {
                                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                            }
                            jsonString = jsonString.Trim(',') + "},";
                        }
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"AgencyName\":\"" + AgencyName + "\",\"Count\":\"" + Count + "\",\"AgentStatement\":[" + jsonString.Trim(',') + "]}";
                        SorteddtResult.Dispose();

                    }
                    catch
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                }
            }
            else
            {
                jsonString = "{\"Session\":\"0\",\"retCode\":\"0\"}";
            }





            return jsonString;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }

    }
}
