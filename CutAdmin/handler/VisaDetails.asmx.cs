﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for VisaDetails
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VisaDetails : System.Web.Services.WebService
    {

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        string json = "";
        int i;
        
        #region Visa Add Update

        [WebMethod(true)]
        public string AddVisa(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.AddVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string Update(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, Int64 AgentId)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaRefNo()
        {
            string VisaCode = "CUT-" + GenerateRandomNumber();
            string logoFileName = VisaCode;
            return logoFileName;

        }
        [WebMethod(EnableSession = true)]
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
        [WebMethod(true)]
        public string AddVisaIncomplete(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.AddIncompleteVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string AddOfflineVisa(Int64 sid, string FirstName, string MiddleName, string LastName, string Gender, string VisaType, string Nationality, string ApplicationNo, string Sponsor, string VisaCode)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            if (sid == 0)
                VisaCode = "Offline-" + GenerateRandomNumber();
            DBHelper.DBReturnCode retcode = VisaDetailsManager.AOfflineVisa(sid, FirstName, MiddleName, LastName, Gender, VisaType, Nationality, ApplicationNo, Sponsor, VisaCode);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {

                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }
        #endregion

        #region Visa
        [WebMethod(EnableSession = true)]
        public string Visa()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            int sEcho = ToInt(HttpContext.Current.Request.Params["sEcho"]);
            Int64 iDisplayLength = ToInt(HttpContext.Current.Request.Params["iDisplayLength"]);
            Int64 iDisplayStart = ToInt(HttpContext.Current.Request.Params["iDisplayStart"]);
            string Search = HttpContext.Current.Request.Params["search[value]"];

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.Visa(iDisplayLength, iDisplayStart, Search, 1, "ASC", out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                //Visa = Visa.GetRange( Convert.ToInt16(iDisplayStart), Math.Min(Convert.ToInt16(iDisplayLength), Convert.ToInt16(Visa.Count - iDisplayStart)));
                jsSerializer.MaxJsonLength = Int32.MaxValue;
                // bool Invoice = InvoiceMailManager.SendInvoiceMail("OTB-1611547125");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string VisaByPosting()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            int sEcho = ToInt(HttpContext.Current.Request.Params["sEcho"]);
            Int64 iDisplayLength = ToInt(HttpContext.Current.Request.Params["iDisplayLength"]);
            Int64 iDisplayStart = ToInt(HttpContext.Current.Request.Params["iDisplayStart"]);
            string Search = HttpContext.Current.Request.Params["search[value]"];

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetPostingVisa(iDisplayLength, iDisplayStart, Search, 1, "ASC", out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                //Visa = Visa.GetRange( Convert.ToInt16(iDisplayStart), Math.Min(Convert.ToInt16(iDisplayLength), Convert.ToInt16(Visa.Count - iDisplayStart)));
                jsSerializer.MaxJsonLength = Int32.MaxValue;
                // bool Invoice = InvoiceMailManager.SendInvoiceMail("OTB-1611547125");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPostingVisa()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            int sEcho = ToInt(HttpContext.Current.Request.Params["sEcho"]);
            Int64 iDisplayLength = ToInt(HttpContext.Current.Request.Params["iDisplayLength"]);
            Int64 iDisplayStart = ToInt(HttpContext.Current.Request.Params["iDisplayStart"]);
            string Search = HttpContext.Current.Request.Params["search[value]"];

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetPostingVisa(iDisplayLength, iDisplayStart, Search, 1, "ASC", out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                //Visa = Visa.GetRange( Convert.ToInt16(iDisplayStart), Math.Min(Convert.ToInt16(iDisplayLength), Convert.ToInt16(Visa.Count - iDisplayStart)));
                jsSerializer.MaxJsonLength = Int32.MaxValue;
                // bool Invoice = InvoiceMailManager.SendInvoiceMail("OTB-1611547125");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        public static int ToInt(string toParse)
        {
            int result;
            if (int.TryParse(toParse, out result)) return result;

            return result;
        }

        [WebMethod(EnableSession = true)]
        public string OfflineVisa()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.OfflineVisa(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string VisaCode = "CUTOffline-" + GenerateRandomNumber();
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAdminVisaByService(Int64 Type, string Process)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaByService(out dtResult, Type, Process);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string VisaLoadByUserId(Int64 UserId)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            if (UserId == 0)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                UserId = objGlobalDefault.sid;
            }
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.VisaLoadByUserId(out dtResult, UserId);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataTable dtTop10 = dtResult.Rows.Cast<DataRow>().OrderBy(x => x["sid"]).Take(5).CopyToDataTable();
                DataRow[] rows = dtResult.Select("sid<5");
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtTop10);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetAgentByUid(Int64 sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetAgentByUid(out dtResult, sid);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> AgentDetails = new List<Dictionary<string, object>>();
                AgentDetails = JsonStringManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_AgentDetails = AgentDetails });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetVisaByCode(string VisaCode)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaByCode(out dtResult, VisaCode);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                Session["AgentId"] = dtResult.Rows[0]["UserId"].ToString();
                Session["AgentCurrency"] = dtResult.Rows[0]["CurrencyCode"].ToString();
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_Visa = Visa, AgencyName = dtResult.Rows[0]["AgencyName"].ToString(), AgentId = Session["AgentId"].ToString() });
            }
            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(true)]
        public string DocumentStatus(string VCode)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.DocumentStatus(VCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetVisaByService(Int64 Type, string Process)
        {
            string jsonString = "";
            AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            DataTable dtResult;
            DataSet dsResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaByService(out dtResult, Type, Process);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault != null)
                {
                    Int64 UserId = objGlobalDefault.sid;
                    DBHelper.DBReturnCode recode1 = VisaDetailsManager.GetAgentMarkups(UserId, Process, out dsResult);
                    CutAdmin.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
                    if (recode1 == DBHelper.DBReturnCode.SUCCESS)
                    {

                        DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentMarkup;
                        dtIndividualMarkup = dsResult.Tables[2];
                        dtGroupMarkup = dsResult.Tables[1];
                        dtGlobalMarkup = dsResult.Tables[0];
                        dtAgentMarkup = dsResult.Tables[3];
                        decimal IndMarkuper, IndMarkAmt, IndCommper, TotalAmt, ActualMarkupper, ActualCommission, IndCommAmt, IndMarkTotal, TotalMarkup;
                        decimal GroupMarkuper, GroupMarkAmt, GroupCommper, GroupCommAmt;
                        TotalAmt = Convert.ToDecimal(dtResult.Rows[0]["OtherFee"]);
                        if (dtIndividualMarkup.Rows.Count != 0)
                        {
                            IndMarkuper = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupPercentage"]);
                            IndMarkAmt = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupAmmount"]);
                            IndCommper = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["CommessionPercentage"]);
                            IndCommAmt = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["CommessionAmmount"]);
                        }
                        else
                        {
                            IndMarkuper = 0;
                            IndMarkAmt = 0;
                            IndCommper = 0;
                            IndCommAmt = 0;
                        }
                        ActualMarkupper = (IndMarkuper / 100) * TotalAmt;
                        ActualCommission = (IndCommper / 100) * TotalAmt;
                        IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt) - Getgreatervalue(ActualCommission, IndCommAmt);
                        if (dtGroupMarkup.Rows.Count != 0)
                        {
                            GroupMarkuper = Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupPercentage"]);
                            GroupMarkAmt = Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupAmmount"]);
                            GroupCommper = Convert.ToDecimal(dtGroupMarkup.Rows[0]["CommessionPercentage"]);
                            GroupCommAmt = Convert.ToDecimal(dtGroupMarkup.Rows[0]["CommessionAmmount"]);

                        }
                        else
                        {
                            GroupMarkuper = 0;
                            GroupMarkAmt = 0;
                            GroupCommper = 0;
                            GroupCommAmt = 0;
                        }

                        decimal ActualGroupMarkupper = (GroupMarkuper / 100) * TotalAmt;
                        decimal ActualGroupCommission = (GroupCommper / 100) * TotalAmt;
                        decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt) - Getgreatervalue(ActualGroupCommission, GroupCommAmt);
                        decimal GlobalMarkuper, GlobalMarkAmt, GlobalCommper, GlobalCommAmt;

                        if (dtGlobalMarkup.Rows.Count != 0)
                        {
                            GlobalMarkuper = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupPercentage"]);
                            GlobalMarkAmt = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupAmmount"]);
                            GlobalCommper = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["CommessionPercentage"]);
                            GlobalCommAmt = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["CommessionAmmount"]);

                        }
                        else
                        {
                            GlobalMarkuper = 0;
                            GlobalMarkAmt = 0;
                            GlobalCommper = 0;
                            GlobalCommAmt = 0;
                        }
                        decimal ActualGlobalMarkupper = (GlobalMarkuper / 100) * TotalAmt;
                        decimal ActualGlobalCommission = (GlobalCommper / 100) * TotalAmt;
                        decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt) - Getgreatervalue(ActualGlobalCommission, GlobalMarkAmt);
                        decimal AgentMarkupper, AgentMarkupAmt, AgentperAmt, ActualAgentMarkup;
                        if (dtAgentMarkup.Rows.Count != 0)
                        {
                            AgentMarkupper = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Percentage"]);
                            AgentMarkupAmt = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Amount"]);
                        }
                        else
                        {
                            AgentMarkupper = 0;
                            AgentMarkupAmt = 0;

                        }
                        AgentperAmt = (AgentMarkupper / 100) * TotalAmt;
                        ActualAgentMarkup = Getgreatervalue(AgentperAmt, AgentMarkupAmt);
                        objAgentInfo.AgentVisaMarkup = Convert.ToSingle(ActualAgentMarkup);
                        HttpContext.Current.Session["AgentInfo"] = objAgentInfo;
                        TotalMarkup = IndMarkTotal + ActualGroupMarkTotal + ActualGlobalMarkTotal + ActualAgentMarkup;
                        decimal VisaFee = Convert.ToDecimal(dtResult.Rows[0]["VisaFee"]);
                        decimal OtherFee = Convert.ToDecimal(dtResult.Rows[0]["OtherFee"]) + TotalMarkup;
                        decimal UrgentFee = Convert.ToDecimal(dtResult.Rows[0]["UrgentFee"]);
                        decimal ServiceTaxper = Convert.ToDecimal(dtResult.Rows[0]["ServiceTax"]);
                        decimal ServiceTax = OtherFee * ServiceTaxper / 100;
                        decimal ActualAmmount = VisaFee + OtherFee + UrgentFee + ServiceTax;
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaFee\":\"" + VisaFee + "\",\"OtherFee\":\"" + OtherFee + "\",\"UrgentFee\":\"" + UrgentFee + "\",\"ServiceTax\":\"" + ServiceTax + "\",\"ActualAmmount\":\"" + ActualAmmount + "\"}";
                        dtResult.Dispose();
                        return jsonString;

                    }

                }
                else
                {
                    jsonString = "{\"Session\":\"0\",\"retCode\":\"0\"}";
                }

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public static Decimal Getgreatervalue(decimal Percentage, decimal Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

        [WebMethod(true)]
        public string UpdateVisa(string Type, string Nationality, string Currency, decimal Fee, decimal Urgent)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateChargesByNationality(Type, Nationality, Currency, Fee, Urgent);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string AddVisaCharge(string Type, string Nationality, string Currency, decimal Fee, decimal Urgent)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaChargesByCurrency(out dtResult, Type, Nationality, Currency);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                try
                {
                    tbl_VisaChargesDetail Vissa = DB.tbl_VisaChargesDetails.Single(d => d.VisaType == Type && d.Nationality == Nationality && d.Currency == Currency);
                    Vissa.Fees = Fee;
                    Vissa.UrgentChg = Urgent;
                    DB.SubmitChanges();

                    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch
                {
                    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            else
            {
                try
                {
                    tbl_VisaChargesDetail Visa = new tbl_VisaChargesDetail();
                    Visa.VisaType = Type;
                    Visa.Nationality = Nationality;
                    Visa.Required = "Y";
                    Visa.Currency = Currency;
                    Visa.Fees = Fee;
                    Visa.UrgentChg = Urgent;
                    Visa.UpdateBy = UserId;

                    DB.tbl_VisaChargesDetails.InsertOnSubmit(Visa);
                    DB.SubmitChanges();

                    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch
                {
                    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }


        }


        [WebMethod(EnableSession = true)]
        public string GetCountry(string Code)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetCountry(out dtResult, Code);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_VisaCountry\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetVisaDetailsAgentByDate(Int64 Uid, string dFrom, string dTo)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaDetailsAgentByDate(Uid, dFrom, dTo, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataView myDataView = dtResult.DefaultView;
                myDataView.Sort = "Sid DESC";
                DataTable SorteddtResult = myDataView.ToTable();
                jsonString = "";
                foreach (DataRow dr in SorteddtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in SorteddtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaDetailsAgentByDate\":[" + jsonString.Trim(',') + "]}";
                SorteddtResult.Dispose();

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion

        #region VisaTransaction
        [WebMethod(EnableSession = true)]
        public string ConfirmVisa(string Vcode)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            AgentInfo objAgentInfo = (AgentInfo)Session["AgentInfo"];
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            DBHelper.DBReturnCode retcode;
            DataTable dtResult; string Message = "";
            CutAdmin.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            float AvailableCrdit = 0;
            GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if (objAgentInfo != null)
            {
                if (objAgentInfo.AgentCredit >= 0)
                {
                    AvailableCrdit = objAgentInfo.AgentCredit + objAgentInfo.AgentCreditLimit + objGlobalDefault.OTC;
                }
                else
                {
                    AvailableCrdit = objAgentInfo.AgentCreditLimit + objGlobalDefault.OTC;
                }

            }

            //using (TransactionScope objTransactionScope = new TransactionScope())
            //{
            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaLoadByCode(Vcode, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                float BookingAmtWithTax = Convert.ToSingle(dtResult.Rows[0]["TotalAmount"]) + 0;
                if (AvailableCrdit < BookingAmtWithTax - objAgentInfo.AgentVisaMarkup)
                {
                    return js.Serialize(new { session = 1, retCode = 2, Message = "Insufficient Balance to complete this Application." });
                }
                else
                {
                    retcode = VisaDetailsManager.ConformBooking(Vcode, out Message);
                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        InvoiceMailManager.Status = "Requested";
                        InvoiceMailManager.Service = "Visa";
                        InvoiceMailManager.Invoice = Vcode;
                        bool MailSent = InvoiceMailManager.SendInvoiceMail();
                        Message = CutAdmin.DataLayer.EmailManager.GetErrorMessage("Visa Apply");
                        return js.Serialize(new { session = 0, retCode = 1, Message = Message, mail = MailSent });
                    }
                    else
                    {
                        Message = CutAdmin.DataLayer.EmailManager.GetErrorMessage("Incomplete application");
                        return js.Serialize(new { session = 0, retCode = 1, Message = Message });
                    }
                }

            }
            else
            {
                //objTransactionScope.Dispose();
                Message = CutAdmin.DataLayer.EmailManager.GetErrorMessage("Incomplete application");
                return js.Serialize(new { session = 0, retCode = 0, Message = Message });
            }
            // }
        }
        [WebMethod(EnableSession = true)]
        public string EmailSend(string RefNo)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgentUniqueCode = objGlobalDefault.Agentuniquecode;
            string sAgencyName = objGlobalDefault.AgencyName;
            Int64 sAgentId = objGlobalDefault.sid;
            string nMobile = "";
            string sEmail = objGlobalDefault.uid;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.SendEmailAdmin(sAgencyName, sAgentId, AgentUniqueCode, nMobile, sEmail, RefNo);
            if (retcode == DBHelper.DBReturnCode.SUCCESSNOAFFECT)
            {
                sJsonString = "{\"retCode\":\"1\",}";
            }
            else
            {
                sJsonString = "{\"retCode\":\"0\",}";
            }
            return sJsonString;
        }
        #endregion

        #region Get Languages
        [WebMethod(EnableSession = true)]

        public string GetLanguages(string Code)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetLanguages(out dtResult, Code);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Languaes\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Invoice
        [WebMethod(EnableSession = true)]
        public string PrintInvoice(string RefNo, Int64 UID)
        {
            string jsonString = "";
            string sVisaReservation, sBookingTransactions, sAgentDetails;
            DataSet ds;
            DataTable dtVisaReservation, dtBookingTransactions, dtAgentDetail;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string UserType = objGlobalDefault.UserType;
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.VisaInvoiceDetails(RefNo, UID, out ds);
            if (retcode == DBHelper.DBReturnCode.SUCCESS && UserType == "Admin")
            {
                jsonString = "";
                dtVisaReservation = ds.Tables[0];
                dtBookingTransactions = ds.Tables[1];
                dtAgentDetail = ds.Tables[2];
                dtVisaReservation = ds.Tables[0];
                dtBookingTransactions = ds.Tables[1];
                dtAgentDetail = ds.Tables[2];
                sVisaReservation = "";
                foreach (DataRow dr in dtVisaReservation.Rows)
                {
                    sVisaReservation += "{";
                    foreach (DataColumn dc in dtVisaReservation.Columns)
                    {
                        sVisaReservation += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sVisaReservation = sVisaReservation.Trim(',') + "},";
                }
                sBookingTransactions = "";
                foreach (DataRow dr in dtBookingTransactions.Rows)
                {
                    sBookingTransactions += "{";
                    foreach (DataColumn dc in dtBookingTransactions.Columns)
                    {
                        sBookingTransactions += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sBookingTransactions = sBookingTransactions.Trim(',') + "},";
                }

                sAgentDetails = "";
                foreach (DataRow dr in dtAgentDetail.Rows)
                {
                    sAgentDetails += "{";
                    foreach (DataColumn dc in dtAgentDetail.Columns)
                    {
                        sAgentDetails += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sAgentDetails = sAgentDetails.Trim(',') + "},";
                }
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaDetails\":[" + sVisaReservation.Trim(',') + "],\"BookingTransactions\":[" + sBookingTransactions.Trim(',') + "],\"AgentDetails\":[" + sAgentDetails.Trim(',') + "],\"roleID\":\"Admin\"}";

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESS && UserType == "Agent")
            {
                //MarkupsAndTaxes objMarkup = (MarkupsAndTaxes)HttpContext.Current.Session["markups"];
                //Decimal Markup = Convert.ToDecimal(objMarkup.PerAgentMarkup);
                float Markuper = 0;
                float MarkupAmt = 0;
                float ActualMarkps = 0;
                DataTable dtMarkup;
                retcode = VisaDetailsManager.GetAgentMarkp(UserId, out dtMarkup);
                Markuper = Convert.ToSingle(dtMarkup.Rows[0]["Percentage"]);
                MarkupAmt = Convert.ToSingle(dtMarkup.Rows[0]["Amount"]);
                retcode = VisaDetailsManager.VisaInvoiceDetails(RefNo, UID, out ds);
                jsonString = "";
                dtVisaReservation = ds.Tables[0];
                float TotalAmt = Convert.ToSingle(dtVisaReservation.Rows[0]["TotalAmount"]);
                float ActualAmt = TotalAmt * (Markuper / 100);
                if (ActualAmt > MarkupAmt)
                {
                    //ActualMarkps =TotalAmt+ ActualAmt;
                    ActualMarkps = TotalAmt;
                }
                else
                {
                    ActualMarkps = TotalAmt;

                }
                dtBookingTransactions = ds.Tables[1];
                dtAgentDetail = ds.Tables[2];
                sVisaReservation = "";
                foreach (DataRow dr in dtVisaReservation.Rows)
                {
                    sVisaReservation += "{";
                    foreach (DataColumn dc in dtVisaReservation.Columns)
                    {
                        sVisaReservation += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sVisaReservation = sVisaReservation.Trim(',') + "},";
                }
                sBookingTransactions = "";
                foreach (DataRow dr in dtBookingTransactions.Rows)
                {
                    sBookingTransactions += "{";
                    foreach (DataColumn dc in dtBookingTransactions.Columns)
                    {
                        sBookingTransactions += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sBookingTransactions = sBookingTransactions.Trim(',') + "},";
                }

                sAgentDetails = "";
                foreach (DataRow dr in dtAgentDetail.Rows)
                {
                    sAgentDetails += "{";
                    foreach (DataColumn dc in dtAgentDetail.Columns)
                    {
                        sAgentDetails += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    sAgentDetails = sAgentDetails.Trim(',') + "},";
                }
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaDetails\":[" + sVisaReservation.Trim(',') + "],\"BookingTransactions\":[" + sBookingTransactions.Trim(',') + "],\"AgentDetails\":[" + sAgentDetails.Trim(',') + "],\"roleID\":\"Agent\",\"Markup\":\"" + ActualMarkps + "\"}";

                //foreach (DataRow dr in dtResult.Rows)
                //{
                //    jsonString += "{";
                //    foreach (DataColumn dc in dtResult.Columns)
                //    {
                //        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                //    }
                //    jsonString = jsonString.Trim(',') + "},";
                //}
                //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_AgentVisa\":[" + jsonString.Trim(',') + "],\"Markup\":\"" + Markup + "\",\"roleID\":\"Agent\"}";
                //dtResult.Dispose();

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region VisaApplicationDetails
        [WebMethod(EnableSession = true)]
        public string GetVisaApplication(string VisaCode)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaApplication(out dtResult, VisaCode);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_VisaApplication\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Visa Search
        [WebMethod(EnableSession = true)]
        public string GetVisaCode(string Vcode)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaCode(Vcode, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Vcode"),
                    value = data.Field<String>("Vcode")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaRefByUserId(string Vcode)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaCodeUser(Vcode, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Vcode"),
                    value = data.Field<String>("Vcode")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaApplicationName(string Name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaApplication(Name, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("FirstName"),
                    value = data.Field<String>("FirstName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaApplicationByUser(string Name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaApplicationByUser(Name, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("FirstName"),
                    value = data.Field<String>("FirstName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaDocumentNumber(string Number)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaDcumentNo(Number, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("PassportNo"),
                    value = data.Field<String>("PassportNo")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaDocumentNumberUser(string Number)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaDcumentNoByUser(Number, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("PassportNo"),
                    value = data.Field<String>("PassportNo")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetAgencyName(string AgencyName)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetAgencyName(AgencyName, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Sid"),
                    value = data.Field<String>("AgencyName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string SearchVisa(string RefNo, string ApplicationName, string PassportNo, string Status, string fromDate, string ToDate, string VisaType)
        {
            DataTable dtResult;
            string jsonString;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.SerchVisaAll(RefNo, ApplicationName, PassportNo, Status, fromDate, ToDate, VisaType, out dtResult);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_VisaDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string AdminVisaSearch(string RefNo, string ApplicationName, string PassportNo, string Status, string fromDate, string ToDate, string VisaType, Int64 Agency)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            string jsonString;
            int sEcho = ToInt(Context.Request.Params["sEcho"]);
            Int64 iDisplayLength = ToInt(Context.Request.Params["iDisplayLength"]);
            Int64 iDisplayStart = ToInt(Context.Request.Params["iDisplayStart"]);
            string Search = Context.Request.Params["sSearch"];

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            DBHelper.DBReturnCode retCode = VisaDetailsManager.Visa(iDisplayLength, iDisplayStart, Search, 1, "ASC", out dtResult);
            DataRow[] rows;
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                if (RefNo != "")
                {
                    rows = dtResult.Select("(Vcode = '" + RefNo + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    // dtResult = rows.CopyToDataTable();

                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (ApplicationName != "")
                {
                    rows = dtResult.Select("(FirstName = '" + ApplicationName + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (PassportNo != "")
                {
                    rows = dtResult.Select("(PassportNo = '" + PassportNo + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";

                    }
                }
                if (Status != "")
                {
                    rows = dtResult.Select("(Status = '" + Status + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                //if (fromDate != "" && ToDate !="")
                //{
                //    dtResult.Columns.Add("Date", typeof(DateTime));
                //    DateTime Chk_Dt_Converted = DateTime.Now;
                //    string Chk_Dt = "";
                //    //for (int i = 0; i < dtResult.Rows.Count; i++)
                //    //{
                //    //    Chk_Dt = (dtResult.Rows[i]["AppliedDate"]).ToString();
                //    //    Chk_Dt_Converted =Convert.ToDateTime(Chk_Dt);
                //    //    dtResult.Rows[i]["Date"] = Chk_Dt_Converted;
                //    //}
                //    //DateTime From = Convert.ToDateTime(fromDate);
                //    //DateTime To = Convert.ToDateTime(ToDate);
                //    rows = dtResult.Select("(AppliedDate  like'%" + fromDate + "% AND Date <= #" + To + "#)");
                //    if (rows.Length == 0)
                //    {
                //        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                //        return jsonString;
                //    }
                //    try
                //    {
                //        dtResult = rows.CopyToDataTable();
                //    }
                //    catch
                //    {
                //        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                //    }
                //}
                if (VisaType != "-")
                {
                    rows = dtResult.Select("(IeService = '" + VisaType + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                if (Agency != 0)
                {
                    rows = dtResult.Select("(UserId = '" + Agency + "')");
                    if (rows.Length == 0)
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                        return jsonString;
                    }
                    try
                    {
                        dtResult = rows.CopyToDataTable();
                    }
                    catch
                    {
                        return "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                jsonString = "";
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_VisaDetails = Visa });
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion

        #region Visa Status
        [WebMethod(EnableSession = true)]
        public string UpdateVisaStatus(Int64 sid, string status, Int64 UserId, string RefrenceNo)
        {
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateVisaStatus(sid, status);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //if (status == "Posted")
                //{
                bool Mail = VisaDetailsManager.VisaStatusMail(RefrenceNo, UserId, status);
                if (Mail == true)
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"1\"}";
                }
                else
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"0\"}";
                }
                //}

                //return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"0\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        #region Visanew Charges
        [WebMethod(EnableSession = true)]
        public string GetVisaChargesByNationality(string Type, string Nationality)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaByNationality(out dtResult, Type, Nationality);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Visa\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetVisaChargesByCurrency(string Type, string Nationality, string Currency)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaDetailsManager.GetVisaChargesByCurrency(out dtResult, Type, Nationality, Currency);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Visa\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion

        #region Automation
        #region PostVisa
        [WebMethod(EnableSession = true)]
        public string PostVisa(Int64 sid, string status, string RefrenceNo, Int64 UserId, string Username, string Password, string AppNo)
        {
            //DBHelper.DBReturnCode retCode = Automation.PostVisa(RefrenceNo, Username, Password);
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateStatusbyApplication(RefrenceNo, Username, Password, AppNo);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        //[WebMethod(EnableSession = true)]
        //public string PostOnEdnrd(string[] RefrenceNo, Int64 UserId, string Username, string Password, string Supplier, string[] PaxType)
        //{
        //    JavaScriptSerializer objSerialize = new JavaScriptSerializer();
        //    string[] AppNo = new string[RefrenceNo.Length], Status = new string[RefrenceNo.Length];
        //    bool retCode = false;

        //    if (Supplier != "COZMO")
        //    {
        //        retCode = VisaPostingManager.VisaPosting(RefrenceNo, Username, Password, out AppNo, out Status, Supplier);
        //    }
        //    else if (Supplier == "COZMO")
        //    {
        //        retCode = VisaPostingManager.CozmoPosting(RefrenceNo, PaxType, Username, Password, out AppNo, out Status, Supplier);
        //    }

        //    if (retCode == true)
        //    {
        //        return objSerialize.Serialize(new { Session = 1, retCode = 1, AppNo = AppNo, Status = Status });
        //    }
        //    else
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //}


        //[WebMethod(EnableSession = true)]
        //public string DownloadOnEdnrd(string[] RefrenceNo, string[] Appl_No, Int64 UserId, string Username, string Password, string Supplier, string[] PaxType)
        //{
        //    JavaScriptSerializer objSerialize = new JavaScriptSerializer();
        //    string[] AppNo = new string[RefrenceNo.Length], Status = new string[RefrenceNo.Length];
        //    bool retCode = false;

        //    if (Supplier != "COZMO")
        //    {
        //        retCode = UploadVisaManager.VisaDownload(RefrenceNo, Appl_No, Username, Password, out AppNo, out Status, Supplier);
        //    }
        //    else if (Supplier == "COZMO")
        //    {
        //        retCode = UploadVisaManager.CozmoDownload(RefrenceNo, PaxType, Username, Password, out AppNo, out Status, Supplier);
        //    }

        //    if (retCode == true)
        //    {
        //        return objSerialize.Serialize(new { Session = 1, retCode = 1, AppNo = AppNo, Status = Status });
        //    }
        //    else
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //}


        [WebMethod(EnableSession = true)]
        public string RefreshStatus()
        {
            VisaStatusManager obj = new VisaStatusManager();
            try
            {
                obj.RefreshStatus();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #endregion

        #region GetVisaStatus
        [WebMethod(EnableSession = true)]
        public string GetIEVisaStatus()
        {
            DataSet dsResult;

            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisalistByAppList(out dsResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                VisaPostingManager objAutomation = new VisaPostingManager();
                string Path = HttpContext.Current.Server.MapPath("~/lib/").ToString();
                string Snap = HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap").ToString();
                string[] ListStatus;
                Parallel.For(0, 2, i =>
                {

                    if (i == 0)
                        objAutomation.StatusByVisa(dsResult.Tables[1], Path, Snap);
                    else
                        objAutomation.StatusByApp_No(dsResult.Tables[0], Path, Snap, out ListStatus);
                });
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPostingSnap(string RefrenceNo)
        {
            List<string> SnapPath = new List<string>();
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            try
            {
                string VisaPath = System.Web.HttpContext.Current.Server.MapPath("~/VisaImages/VisaSnap/");
                DirectoryInfo dir = new DirectoryInfo(VisaPath);
                FileInfo[] files = null;
                files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains(RefrenceNo))
                    {
                        SnapPath.Add(file.Name);
                    }
                }
                if (SnapPath.Count != 0)
                    return objSerialize.Serialize(new { Session = 1, retCode = 1, SnapPath = SnapPath });
                else
                    return objSerialize.Serialize(new { Session = 1, retCode = 0, SnapPath = SnapPath });
            }
            catch
            {
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string GetVisaDoc(string RefrenceNo)
        {
            string Path = "";
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            try
            {
                string VisaPath = System.Web.HttpContext.Current.Server.MapPath("~/VisaImages/");
                string ext = "";
                DirectoryInfo dir = new DirectoryInfo(VisaPath);
                FileInfo[] files = null;
                files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains(RefrenceNo + "_Visa"))
                    {
                        Path = file.Name;
                        ext = file.Extension;
                        break;
                    }
                }
                if (Path.Length != 0)
                {
                    return objSerialize.Serialize(new { Session = 1, retCode = 1, Path = Path, ext = ext });
                }

                else
                    return objSerialize.Serialize(new { Session = 1, retCode = 0 });
            }
            catch
            {
                return objSerialize.Serialize(new { Session = 1, retCode = 0 });
            }

        }
        #endregion

        #region Multipule Status Upload
        //[WebMethod(EnableSession = true)]
        //public string UpdateMultipuleStatus(string[] VCode, string[] ApplicationNo, string Username, string Password)
        //{
        //    DBHelper.DBReturnCode retCode = Automation.GetMultipuleStatus(VCode, ApplicationNo, Username, Password);
        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    else
        //    {
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //}

        #endregion
        #endregion

        #region Delete Attachments
        [WebMethod(EnableSession = true)]
        public string DeleteAttachment(string hidenId, string FileNo)
        {
            DBHelper.DBReturnCode retCode = VisaDetailsManager.DeleteAttachments(hidenId, FileNo);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        #region Visa Reject By Status
        [WebMethod(EnableSession = true)]
        public string RejectByAdmin(string RefNo, Int64 Uid, decimal Amount, bool IsRefund)
        {
            DBHelper.DBReturnCode retCode = VisaManager.RejectByStatus(RefNo, Uid, Amount, IsRefund);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        //#region Attach Visa
        //[WebMethod(EnableSession = true)]
        //public string Upload(string DropDownList1, string DropDownList2, string TextBox1, string HiddenField1, string FileUpload1)
        //{
        //    if (TextBox1.Length == 8)
        //    {
        //        string VisaNo = DropDownList1 + "/" + DropDownList2 + "/" + TextBox1;
        //        string ImageCode = HiddenField1;
        //        string Serverpath = Server.MapPath("~/VisaImages/");
        //        string message = "Please upload files with less than 32KB";
        //        string message1 = "Visa Copy Uploded";
        //        string Url = "VisaDetails.aspx?Image=" + ImageCode;
        //        string script = "window.onload = function(){ alert('";
        //        script += message;
        //        script += "');";
        //        script += "window.location = '";
        //        script += Url;
        //        script += "'; }";
        //        string script1 = "window.onload = function(){ alert('";
        //        script1 += message1;
        //        script1 += "');";
        //        script1 += "window.location = '";
        //        script1 += "'; }";
        //        string[] sFileExtension = FileUpload1.Split('.');
        //        string sFileName = ImageCode + "_Visa" + "." + sFileExtension[1];
        //        string Filename = FileUpload1;
        //        string[] ext = Filename.Split('.');
        //        //string type = ext[1];
        //        string filePath = FileUpload1.PostedFile.FileName;
        //        string type = Path.GetExtension(filePath);
        //        if (type == ".pdf" || type == ".jpg" || type == ".png")
        //        {
        //            if (IsPostBack && FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength < 3200000)
        //            {

        //                string sFolderName = Serverpath;
        //                string PhysicalPath = sFolderName + sFileName;
        //                if (!Directory.Exists(sFolderName))
        //                {
        //                    Directory.CreateDirectory(sFolderName);
        //                }
        //                else
        //                {
        //                    string[] files = Directory.GetFiles(sFolderName);
        //                    foreach (string fileName in files)
        //                    {
        //                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
        //                        {
        //                            File.Delete(fileName);
        //                        }
        //                    }
        //                }
        //                FileUpload1.SaveAs(PhysicalPath);
        //                lblMessage.Visible = true;
        //                DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaStatus(ImageCode, VisaNo);
        //                if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //                {
        //                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Visa Uploded Sucessfully.')", true);
        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Something went wrong while processing your request! Please try again.')", true);
        //                }

        //            }
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please upload files with less than 32KB')", true);
        //            }
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Upload .pdf File Only!!')", true);
        //        }

        //    }
        //    else if (TextBox1 == "")
        //    {
        //        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Insert Visa No!!')", true);
        //        return "{\"Session\":\"1\",\"retCode\":\"0\"}";

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Insert Valid Visa No!!')", true);
        //    }

            
                
        //    //DBHelper.DBReturnCode retCode = VisaManager.RejectByStatus(RefNo, Uid, Amount, IsRefund);
        //    //if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    //{
        //    //    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    //}
        //    //else
        //    //{
        //    //    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    //}
        //}
        //#endregion

    }
}
