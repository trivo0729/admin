﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for VisaHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VisaHandler : System.Web.Services.WebService
    {
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Visa Call Center

        #region Save Visa Against Agent
        #region Visa Add Update

        [WebMethod(true)]
        public string AddVisa(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, string AppDate, Int64 AgentId, string Supplier, string User, string Password, bool offlineStatus, string AppNo, string VisaNo)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaManager.AddVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, AgentId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry, AppDate, UserId, Supplier, User, Password, offlineStatus, AppNo, VisaNo);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string Update(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, Int64 AgentId)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = AgentId;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string UpdateOffline(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry, Int64 AgentId, string Supplier, string AppNo, string VisaNo)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = AgentId;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateVisaoffline(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry, Supplier, AppNo, VisaNo);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetVisaRefNo()
        {
            string VisaCode = "CUT-" + GenerateRandomNumber();
            string logoFileName = VisaCode;
            return logoFileName;

        }
        [WebMethod(EnableSession = true)]
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
        [WebMethod(true)]
        public string AddVisaIncomplete(string sProcessing, string dteArrival, string dteDeparting, string sType, string sFirst, string sMiddle, string sLast, string sFather, string sMother, string sHusband, string sLanguage, string sGender, string sMarital, string sNationality, string sBirth, string sBirthPlace, string sBirthCountry, string sReligion, string sProfession, string sPassport, string sIssuing, string sIssueDate, string sExpirationDate, string sAddress1, string sAddress2, string sCity, string sCountry, string sTelephone, string sVisa, string sArriAirLine, string sArriFlight, string sArrivalFrom, string sDeptAirLine, string sDeptFlight, string sDeptFrom, string VisaFee, string OtherFee, string UrgentFee, string ServiceTax, string TotalAmount, string sVisaCountry)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            DBHelper.DBReturnCode retCode = VisaDetailsManager.AddIncompleteVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        #endregion
        #endregion

        #region Get AgenList
        #region Agent Auto Complete
        [WebMethod(EnableSession = true)]
        public string GetAgentList(string name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaManager.GetAgentRoleList(name, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Sid"),
                    value = data.Field<String>("AgencyName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        #endregion

        #region Auto Complete
        [WebMethod(EnableSession = true)]
        public string GetPassportNo(string PassportNo)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaManager.GetPassportNo(PassportNo, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<Int64>("sid").ToString(),
                    value = data.Field<String>("PassportNo")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        #endregion
        [WebMethod(true)]
        public string GetPassportDetails(Int64 Sid)
        {
            JavaScriptSerializer jsSerialize = new JavaScriptSerializer();
            string json = "";
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retcode = VisaManager.GetPassportDetails(Sid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string Status = dtResult.Rows[0]["Status"].ToString();
                bool Valid = false;
                List<Dictionary<string, object>> Visa = new List<Dictionary<string, object>>();
                Visa = JsonStringManager.ConvertDataTable(dtResult);

                if (Status == "Approved")
                {
                    string Date = ApproveDate(dtResult.Rows[0]["History"].ToString());
                    DateTime dtFromDate = DateTime.Now;
                    DateTime dtToDate = DateTime.ParseExact(Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    TimeSpan difference = dtFromDate - dtToDate;
                    double days = difference.TotalDays;
                    if (dtResult.Rows[0]["IeService"].ToString() == "14 days Service VISA" && days > 14)
                    {
                        Valid = true;
                    }
                    else if (days > 60)
                    {
                        Valid = true;
                    }
                }
                else if (Status == "Approve")
                {
                    Valid = true;
                }

                return jsSerialize.Serialize(new { retcode = 1, Session = 1, Visa = Visa, Valid = Valid });
            }
            else
            {
                return jsSerialize.Serialize(new { Session = 1, retCode = 0 });
            }


        }
        public string ApproveDate(string History)
        {
            string Date = "";
            int i = 0;
            string[] SpliteDate = History.Split('^');
            string[] Status;
            for (i = 0; i < SpliteDate.Length; i++)
            {
                Status = SpliteDate[i].Split(' ');
                if (Status[0] == "Entered")
                {
                    Date = Status[Status.Length - 1];
                    string[] DateFormate = Date.Split('-');
                    Date = DateFormate[1] + "-" + DateFormate[0] + "-" + DateFormate[2];
                    break;
                }
                else if (Status[0] == "Approved")
                {
                    Date = Status[Status.Length - 1];
                    string[] DateFormate = Date.Split('-');
                    Date = DateFormate[1] + "-" + DateFormate[0] + "-" + DateFormate[2];
                    break;
                }
            }
            return Date;
        }

        #region AgenDetails


        [WebMethod(true)]
        public string GetAgenDetails(Int64 uid)
        {
            string Vcode = this.Context.Request.QueryString["Vcode"];
            string json = "";
            Session["AgentCurrency"] = null;
            Session["AgentId"] = null;
            JavaScriptSerializer objSreialize = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            string Agentuniquecode = "";
            string AvailableCredit = "";
            string ContactPerson = "";
            string OTC = "";
            string CreditGiven = "";
            float Balance = 0;
            DBHelper.DBReturnCode retcode = VisaManager.GetAgentDetails(uid, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string CurrencyCode = dtResult.Rows[0]["CurrencyCode"].ToString();
                Session["AgentCurrency"] = CurrencyCode;
                Session["AgentId"] = uid;

                string CurrencyImage = "";
                switch (CurrencyCode)
                {
                    case "AED":
                        CurrencyImage = "Currency-AED";

                        break;
                    case "SAR":
                        CurrencyImage = "Currency-SAR";
                        break;
                    case "EUR":
                        CurrencyImage = "fa fa-eur";
                        break;
                    case "GBP":
                        CurrencyImage = "fa fa-gbp";
                        break;
                    case "USD":
                        CurrencyImage = "fa fa-dollar";
                        break;
                    case "INR":
                        CurrencyImage = "fa fa-inr";
                        break;
                }
                Session["CurrencyClass"] = CurrencyImage;
                Agentuniquecode = dtResult.Rows[0]["Agentuniquecode"].ToString();
                AvailableCredit = "<i class='" + CurrencyImage + "'> </i> " + dtResult.Rows[0]["AvailableCredit"].ToString();
                if (Convert.ToSingle(dtResult.Rows[0]["AvailableCredit"]) <= 0)
                {
                    if (Convert.ToSingle(dtResult.Rows[0]["CreditAmount_Cmp"]) > 0)
                    {
                        Balance = Convert.ToSingle(dtResult.Rows[0]["CreditAmount"]);
                    }
                    else if (Convert.ToSingle(dtResult.Rows[0]["MaxCreditLimit_Cmp"]) > 0)
                    {
                        Balance = Convert.ToSingle(dtResult.Rows[0]["MaxCreditLimit"]);
                    }


                }
                else
                    Balance += Convert.ToSingle(dtResult.Rows[0]["AvailableCredit"]);


                ContactPerson = dtResult.Rows[0]["ContactPerson"].ToString();
                OTC = Convert.ToString("<i class='" + CurrencyImage + "'> </i> " + dtResult.Rows[0]["MaxCreditLimit"] + " /" + " <i class='" + CurrencyImage + "'> </i> " + dtResult.Rows[0]["MaxCreditLimit_Cmp"]);
                CreditGiven = Convert.ToString("<i class='" + CurrencyImage + "'> </i> " + dtResult.Rows[0]["CreditAmount"] + " /" + " <i class='" + CurrencyImage + "'> </i> " + dtResult.Rows[0]["CreditAmount_Cmp"]);
                //return "{\"Session\":\"1\",\"retCode\":\"1\",\"Agentuniquecode\":\"" + Agentuniquecode + "\",\"AvailableCredit\":\"" + AvailableCredit + "\",\"OTC\":\"" + OTC + "\",\"ContactPerson\":\"" + ContactPerson + "\",\"CreditGiven\":\"" + CreditGiven + "\"}";
                return objSreialize.Serialize(new { Session = 1, retCode = 1, Agentuniquecode = Agentuniquecode, AvailableCredit = AvailableCredit, OTC = OTC, ContactPerson = ContactPerson, CreditGiven = CreditGiven, Balance = Balance, CurrencyImage = CurrencyImage });
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


        }
        #endregion
        #endregion

        #region Visa Caharges
        //[WebMethod(EnableSession = true)]
        //public string GetVisaCharges(string Type, string Nationality, string Process, Int64 UserId)
        //{
        //    string jsonString = "";
        //    string ServiceType;
        //    if (Process == "Normal")
        //    {
        //        ServiceType = "1";
        //    }
        //    else
        //    {
        //        ServiceType = "2";
        //    }
        //    string Supplier = Nationality + "-" + Type + "-" + ServiceType;
        //    AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
        //    DataTable dtResult;
        //    DataSet dsResult;
        //    DBHelper.DBReturnCode retcode = VisaManager.GetVisaCharges(Type, Nationality, out dtResult);

        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        string Currency = dtResult.Rows[0]["Currency"].ToString();
        //        float Charges = Convert.ToSingle(dtResult.Rows[0]["Fees"]);
        //        float UrgentCharges = Convert.ToSingle(dtResult.Rows[0]["UrgentChg"]);
        //        float Urgent;
        //        string Required = dtResult.Rows[0]["Required"].ToString();

        //        if (Currency == "AED" && Required == "Y")
        //        {
        //            DataTable dtCurrency;
        //            //CUT.Admin.DataLayer.ExchangeRateManager.GetForeignExchangeRate(Currency, out dtCurrency);
        //            //float InrCurrency = Convert.ToSingle(dtCurrency.Rows[0]["INR"]);
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            if (objGlobalDefault != null)
        //            {

        //                //Int64 UserId = objGlobalDefault.sid;
        //                DBHelper.DBReturnCode recode1 = VisaManager.GetAgentMarkups(UserId, Supplier, out dsResult);
        //                //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
        //                if (recode1 == DBHelper.DBReturnCode.SUCCESS)
        //                {
        //                    if (Process == "Normal")
        //                    {
        //                        Urgent = 0;
        //                    }
        //                    else
        //                    {
        //                        Urgent = UrgentCharges * InrCurrency;
        //                    }

        //                    DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentMarkup;
        //                    dtIndividualMarkup = dsResult.Tables[2];
        //                    dtGroupMarkup = dsResult.Tables[1];
        //                    dtGlobalMarkup = dsResult.Tables[0];
        //                    dtAgentMarkup = dsResult.Tables[3];
        //                    decimal IndMarkuper, IndMarkAmt, IndCommper, TotalAmt, ActualMarkupper, ActualCommission, IndCommAmt, IndMarkTotal, TotalMarkup;
        //                    decimal GroupMarkuper, GroupMarkAmt, GroupCommper, GroupCommAmt;
        //                    TotalAmt = Convert.ToDecimal(Charges * InrCurrency);
        //                    if (dtIndividualMarkup.Rows.Count != 0)
        //                    {
        //                        IndMarkuper = 0;
        //                        IndMarkAmt = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupAmmount"]);
        //                        IndCommper = 0;
        //                        IndCommAmt = 0;
        //                    }
        //                    else
        //                    {
        //                        IndMarkuper = 0;
        //                        IndMarkAmt = 0;
        //                        IndCommper = 0;
        //                        IndCommAmt = 0;
        //                    }
        //                    ActualMarkupper = (IndMarkuper / 100) * TotalAmt;
        //                    ActualCommission = (IndCommper / 100) * TotalAmt;
        //                    //IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt) - Getgreatervalue(ActualCommission, IndCommAmt);
        //                    IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt);
        //                    if (dtGroupMarkup.Rows.Count != 0)
        //                    {
        //                        GroupMarkuper = 0;
        //                        GroupMarkAmt = Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupAmmount"]);
        //                        GroupCommper = 0;
        //                        GroupCommAmt = 0;

        //                    }
        //                    else
        //                    {
        //                        GroupMarkuper = 0;
        //                        GroupMarkAmt = 0;
        //                        GroupCommper = 0;
        //                        GroupCommAmt = 0;
        //                    }

        //                    decimal ActualGroupMarkupper = (GroupMarkuper / 100) * TotalAmt;
        //                    decimal ActualGroupCommission = (GroupCommper / 100) * TotalAmt;
        //                    //decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt) - Getgreatervalue(ActualGroupCommission, GroupCommAmt);
        //                    decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt);
        //                    decimal GlobalMarkuper, GlobalMarkAmt, GlobalCommper, GlobalCommAmt;

        //                    if (dtGlobalMarkup.Rows.Count != 0)
        //                    {
        //                        GlobalMarkuper = 0;
        //                        GlobalMarkAmt = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupAmmount"]);
        //                        GlobalCommper = 0;
        //                        GlobalCommAmt = 0;

        //                    }
        //                    else
        //                    {
        //                        GlobalMarkuper = 0;
        //                        GlobalMarkAmt = 0;
        //                        GlobalCommper = 0;
        //                        GlobalCommAmt = 0;
        //                    }
        //                    decimal ActualGlobalMarkupper = (GlobalMarkuper / 100) * TotalAmt;
        //                    decimal ActualGlobalCommission = (GlobalCommper / 100) * TotalAmt;
        //                    //decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt) - Getgreatervalue(ActualGlobalCommission, GlobalMarkAmt);
        //                    decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt);
        //                    decimal AgentMarkupper, AgentMarkupAmt, AgentperAmt, ActualAgentMarkup;
        //                    if (dtAgentMarkup.Rows.Count != 0)
        //                    {
        //                        AgentMarkupper = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Percentage"]);
        //                        AgentMarkupAmt = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Amount"]);
        //                    }
        //                    else
        //                    {
        //                        AgentMarkupper = 0;
        //                        AgentMarkupAmt = 0;

        //                    }
        //                    TotalMarkup = IndMarkTotal + ActualGroupMarkTotal + ActualGlobalMarkTotal;
        //                    decimal VisaFee = Convert.ToDecimal(TotalAmt);
        //                    decimal OtherFee = Convert.ToDecimal(TotalMarkup);
        //                    decimal UrgentFee = Convert.ToDecimal(Urgent);
        //                    //OtherFee = TotalMarkup + (TotalMarkup * Convert.ToDecimal(14.5 / 100));
        //                    OtherFee = TotalMarkup;
        //                    decimal ServiceTax = 0;
        //                    decimal ActualAmmount = VisaFee + OtherFee + UrgentFee + ServiceTax;
        //                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaFee\":\"" + VisaFee + "\",\"OtherFee\":\"" + OtherFee + "\",\"UrgentFee\":\"" + UrgentFee + "\",\"ServiceTax\":\"" + ServiceTax + "\",\"ActualAmmount\":\"" + ActualAmmount + "\"}";
        //                    dtResult.Dispose();
        //                    return jsonString;

        //                }

        //            }

        //        }
        //        else if (Currency == "INR" && Required == "Y")
        //        {
        //            if (Process == "Normal")
        //            {
        //                Urgent = 0;
        //            }
        //            else
        //            {
        //                Urgent = UrgentCharges;
        //            }
        //            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            if (objGlobalDefault != null)
        //            {
        //                //Int64 UserId = objGlobalDefault.sid;
        //                DBHelper.DBReturnCode recode1 = VisaManager.GetAgentMarkups(UserId, Supplier, out dsResult);
        //                //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
        //                if (recode1 == DBHelper.DBReturnCode.SUCCESS)
        //                {

        //                    DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentMarkup;
        //                    dtIndividualMarkup = dsResult.Tables[2];
        //                    dtGroupMarkup = dsResult.Tables[1];
        //                    dtGlobalMarkup = dsResult.Tables[0];
        //                    dtAgentMarkup = dsResult.Tables[3];
        //                    decimal IndMarkuper, IndMarkAmt, IndCommper, TotalAmt, ActualMarkupper, ActualCommission, IndCommAmt, IndMarkTotal, TotalMarkup;
        //                    decimal GroupMarkuper, GroupMarkAmt, GroupCommper, GroupCommAmt;
        //                    TotalAmt = Convert.ToDecimal(Charges);
        //                    if (dtIndividualMarkup.Rows.Count != 0)
        //                    {
        //                        IndMarkuper = 0;
        //                        IndMarkAmt = Convert.ToDecimal(dtIndividualMarkup.Rows[0]["MarkupAmmount"]);
        //                        IndCommper = 0;
        //                        IndCommAmt = 0;
        //                    }
        //                    else
        //                    {
        //                        IndMarkuper = 0;
        //                        IndMarkAmt = 0;
        //                        IndCommper = 0;
        //                        IndCommAmt = 0;
        //                    }
        //                    ActualMarkupper = (IndMarkuper / 100) * TotalAmt;
        //                    ActualCommission = (IndCommper / 100) * TotalAmt;
        //                    //IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt) - Getgreatervalue(ActualCommission, IndCommAmt);
        //                    IndMarkTotal = Getgreatervalue(ActualMarkupper, IndMarkAmt);
        //                    if (dtGroupMarkup.Rows.Count != 0)
        //                    {
        //                        GroupMarkuper = 0;
        //                        GroupMarkAmt = Convert.ToDecimal(dtGroupMarkup.Rows[0]["MarkupAmmount"]);
        //                        GroupCommper = 0;
        //                        GroupCommAmt = 0;

        //                    }
        //                    else
        //                    {
        //                        GroupMarkuper = 0;
        //                        GroupMarkAmt = 0;
        //                        GroupCommper = 0;
        //                        GroupCommAmt = 0;
        //                    }

        //                    decimal ActualGroupMarkupper = (GroupMarkuper / 100) * TotalAmt;
        //                    decimal ActualGroupCommission = (GroupCommper / 100) * TotalAmt;
        //                    //decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt) - Getgreatervalue(ActualGroupCommission, GroupCommAmt);
        //                    decimal ActualGroupMarkTotal = Getgreatervalue(ActualGroupMarkupper, GroupMarkAmt);
        //                    decimal GlobalMarkuper, GlobalMarkAmt, GlobalCommper, GlobalCommAmt;

        //                    if (dtGlobalMarkup.Rows.Count != 0)
        //                    {
        //                        GlobalMarkuper = 0;
        //                        GlobalMarkAmt = Convert.ToDecimal(dtGlobalMarkup.Rows[0]["MarkupAmmount"]);
        //                        GlobalCommper = 0;
        //                        GlobalCommAmt = 0;

        //                    }
        //                    else
        //                    {
        //                        GlobalMarkuper = 0;
        //                        GlobalMarkAmt = 0;
        //                        GlobalCommper = 0;
        //                        GlobalCommAmt = 0;
        //                    }
        //                    decimal ActualGlobalMarkupper = (GlobalMarkuper / 100) * TotalAmt;
        //                    decimal ActualGlobalCommission = (GlobalCommper / 100) * TotalAmt;
        //                    //decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt) - Getgreatervalue(ActualGlobalCommission, GlobalMarkAmt);
        //                    decimal ActualGlobalMarkTotal = Getgreatervalue(ActualGlobalMarkupper, GlobalMarkAmt);
        //                    decimal AgentMarkupper, AgentMarkupAmt, AgentperAmt, ActualAgentMarkup;
        //                    if (dtAgentMarkup.Rows.Count != 0)
        //                    {
        //                        AgentMarkupper = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Percentage"]);
        //                        AgentMarkupAmt = Convert.ToDecimal(dtAgentMarkup.Rows[0]["Amount"]);
        //                    }
        //                    else
        //                    {
        //                        AgentMarkupper = 0;
        //                        AgentMarkupAmt = 0;

        //                    }
        //                    //AgentperAmt = (AgentMarkupper / 100) * TotalAmt;
        //                    //ActualAgentMarkup = Getgreatervalue(AgentperAmt, AgentMarkupAmt);
        //                    //objAgentInfo.AgentVisaMarkup = Convert.ToSingle(ActualAgentMarkup);
        //                    //HttpContext.Current.Session["AgentInfo"] = objAgentInfo;
        //                    TotalMarkup = IndMarkTotal + ActualGroupMarkTotal + ActualGlobalMarkTotal;
        //                    decimal VisaFee = Convert.ToDecimal(Charges);
        //                    decimal OtherFee = TotalMarkup;
        //                    decimal UrgentFee = Convert.ToDecimal(Urgent);
        //                    //OtherFee = TotalMarkup + (TotalMarkup * Convert.ToDecimal(14.5 / 100));
        //                    OtherFee = TotalMarkup;
        //                    decimal ServiceTax = 0;
        //                    decimal ActualAmmount = VisaFee + OtherFee + UrgentFee + ServiceTax;
        //                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"VisaFee\":\"" + VisaFee + "\",\"OtherFee\":\"" + OtherFee + "\",\"UrgentFee\":\"" + UrgentFee + "\",\"ServiceTax\":\"" + ServiceTax + "\",\"ActualAmmount\":\"" + ActualAmmount + "\"}";
        //                    dtResult.Dispose();
        //                    return jsonString;

        //                }

        //            }
        //        }

        //        else
        //        {
        //            jsonString = "{\"Session\":\"0\",\"retCode\":\"0\"}";
        //        }

        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}
        [WebMethod(EnableSession = true)]
        public static Decimal Getgreatervalue(decimal Percentage, decimal Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

        [WebMethod(true)]
        public String GetCharges(decimal VisaFee, decimal otherFee, decimal UrgenFee, decimal SeviceTax)
        {
            decimal TotalAmmount = VisaFee + otherFee + UrgenFee + (otherFee * SeviceTax / 100);
            return String.Format("{0:0.00}", TotalAmmount); ;
        }
        #endregion

        #region VisaTransaction
        public static DBHelper.DBReturnCode GetAvailBalance(Int64 uId, out DataTable dt)
        {
            SqlParameter[] SQLParams = new SqlParameter[1];
            SQLParams[0] = new SqlParameter("@uid", uId);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_GetAvailableCredit", out dt, SQLParams);
            return retCode;
        }
        [WebMethod(EnableSession = true)]
        public string ConfirmVisa(string Vcode)
        {
            //GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //AgentInfo objAgentInfo = (AgentInfo)HttpContext.Current.Session["AgentInfo"];
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //js.MaxJsonLength = Int32.MaxValue;
            //DBHelper.DBReturnCode retcode;
            //DataTable dtResult;
            //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();

            //using (TransactionScope objTransactionScope = new TransactionScope())
            //{
            //    DBHelper.DBReturnCode retCode = VisaManager.GetVisaByCode(out dtResult, Vcode);
            //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
            //    {

            //        float BookingAmtWithTax = Convert.ToSingle(dtResult.Rows[0]["TotalAmount"]) - objAgentInfo.AgentVisaMarkup;
            //       float AvailableCrdit = 0;
            //       float BookingAmtWithoutTax = Convert.ToSingle(dtResult.Rows[0]["TotalAmount"]) - objAgentInfo.AgentVisaMarkup;
            //            string PassengerName = Convert.ToString(dtResult.Rows[0]["FirstName"]) + " " + Convert.ToString(dtResult.Rows[0]["MiddleName"]) + " " + Convert.ToString(dtResult.Rows[0]["LastName"]);
            //           //float BookingServiceTax = Convert.ToSingle(dtResult.Rows[0]["ServiceTax"]);
            //            float BookingServiceTax = 0;
            //            string BookingDate = dtResult.Rows[0]["IssuingDate"].ToString();
            //            string BookedBy = dtResult.Rows[0]["UserId"].ToString();
            //            string BookingRemark = "";
            //            float SupplierBasicAmt = BookingAmtWithoutTax;
            //            float CutComm = 0;
            //            float AgentComm = 0;
            //           // retcode = VisaManager.VisaBookingTxnAdd(Vcode, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "Visa", SupplierBasicAmt, AgentComm, CutComm, BookedBy, PassengerName);
            //            retcode = VisaManager.VisaBookingTxnAdd(Vcode, DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture), BookingAmtWithoutTax, BookingServiceTax, BookingAmtWithTax, 0, 0, 0, 0, 0, 0, 0, BookingDate, BookingRemark, 0, "Visa", SupplierBasicAmt, AgentComm, CutComm, BookedBy, PassengerName);
            //        if (retcode != DBHelper.DBReturnCode.SUCCESS)
            //            {
            //                objTransactionScope.Dispose();
            //                return js.Serialize(new { session = 1, retCode = 0, Message = "Error while adding Visa Application" });
            //            }
            //            else
            //            {
            //                retCode = VisaDetailsManager.DocumentStatus(Vcode);
            //                objTransactionScope.Complete();
            //                return js.Serialize(new { session = 1, retCode = 1 });
            //            }
            //        //}
            //    }
            //    else
            //    {
            //        objTransactionScope.Dispose();
            //        return js.Serialize(new { session = 0, retCode = 0, Message = "Error occured during Visa Application Conformation!" });
            //    }
            //}
            DataTable dtAgentDetails = new DataTable();

            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            AgentInfo objAgentInfo = (AgentInfo)Session["AgentInfo"];
            //objAgentInfo.AgentCredit = Convert.ToSingle(dtAgentDetails.Rows[0]["AvailableCredit"]);
            //objAgentInfo.AgentCreditLimit = Convert.ToSingle(dtAgentDetails.Rows[0]["CreditAmount"]);
            HttpContext.Current.Session["AgentInfo"] = objAgentInfo;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            DBHelper.DBReturnCode retcode;
            DataTable dtResult; string Message = "";
            //CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new MarkupsAndTaxes();
            float AvailableCrdit = 0;
            GlobalDefault objGolobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            DBHelper.DBReturnCode retCode = VisaDetailsManager.VisaLoadByCode(Vcode, out dtResult);
            if (objAgentInfo != null)
            {
                GetAvailBalance(Convert.ToInt64(dtResult.Rows[0]["UserId"]), out dtAgentDetails);
                if (Convert.ToSingle(dtAgentDetails.Rows[0]["AvailableCredit"]) >= 0)
                {
                    AvailableCrdit = Convert.ToSingle(dtAgentDetails.Rows[0]["AvailableCredit"]) + Convert.ToSingle(dtAgentDetails.Rows[0]["CreditAmount"]) + objGlobalDefault.OTC;
                }
                else
                {
                    AvailableCrdit = Convert.ToSingle(dtAgentDetails.Rows[0]["CreditAmount"]);
                }

            }

            //using (TransactionScope objTransactionScope = new TransactionScope())
            //{

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                float BookingAmtWithTax = Convert.ToSingle(dtResult.Rows[0]["TotalAmount"]) - objAgentInfo.AgentVisaMarkup;
                if (AvailableCrdit < BookingAmtWithTax)
                {
                    return js.Serialize(new { session = 1, retCode = 2, Message = "Insufficient Balance to complete this Application." });
                }
                else
                {
                    retcode = VisaDetailsManager.ConformBooking(Vcode, out Message);
                    if (retcode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        return js.Serialize(new { session = 0, retCode = 1, Message = Message });
                    }
                    else
                    {
                        return js.Serialize(new { session = 0, retCode = 0, Message = Message });
                    }
                }

            }
            else
            {
                //objTransactionScope.Dispose();
                return js.Serialize(new { session = 0, retCode = 0, Message = "Error occured during Visa Application Conformation!" });
            }
            // }
        }
        [WebMethod(EnableSession = true)]
        public string EmailSend(Int64 UserId, string RefNo)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgentUniqueCode = objGlobalDefault.Agentuniquecode;
            string sAgencyName = objGlobalDefault.AgencyName;
            string nMobile = "";
            string sEmail = objGlobalDefault.uid;
            //DBHelper.DBReturnCode retcode = VisaDetailsManager.SendEmailAdmin(sAgencyName, AgentUniqueCode, nMobile, sEmail, RefNo);
            DBHelper.DBReturnCode retcode = VisaManager.SendEmailAdmin(UserId, RefNo);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                sJsonString = "{\"retCode\":\"1\",}";
            }
            else
            {
                sJsonString = "{\"retCode\":\"0\",}";
            }
            return sJsonString;
        }
        #endregion

        #endregion

        #region Ednrd Username
        #region UserList
        [WebMethod(EnableSession = true)]
        public string GetUserList()
        {

            string jsonString = "";
            DataTable dtResult;
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            DBHelper.DBReturnCode retcode = VisaManager.GetUsersList(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_EdnrdUser\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Username Add&Update
        [WebMethod(EnableSession = true)]
        public string AddUser(string SupplierName, string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice)
        {
            string jsonString = "";
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            DBHelper.DBReturnCode retcode = VisaManager.AddUser(SupplierName, Username, Password, Hours96, Sevice14Day, Single30Days, Multipule30Days, Single90Days, Multipule90Days, Covertable90Days, Active, Hurs96Price, Service14DaysPrice, Single30DaysPrice, Multipule30DaysPrice, Single90DaysPrice, Multipule90DaysPrice, Covertable90DaysPrice);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string AddUserLogins(Int64 Sid, string SupplierName, Int64 SupplierId, string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice, bool Qouta, Int64 Qouta_Hurs96, Int64 Qouta_14Days, Int64 Qouta_Single30Days, Int64 Qouta_Multipule30Days, Int64 Qouta_Single90Days, Int64 Qouta_Multipule90Days, Int64 Qouta_Covertable90Days)
        {
            string jsonString = "";
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            DBHelper.DBReturnCode retcode = VisaManager.AddPostingLogins(Sid, SupplierName, SupplierId, Username, Password, Hours96, Sevice14Day, Single30Days, Multipule30Days, Single90Days, Multipule90Days, Covertable90Days, Active, Hurs96Price, Service14DaysPrice, Single30DaysPrice, Multipule30DaysPrice, Single90DaysPrice, Multipule90DaysPrice, Covertable90DaysPrice, Qouta, Qouta_Hurs96, Qouta_14Days, Qouta_Single30Days, Qouta_Multipule30Days, Qouta_Single90Days, Qouta_Multipule90Days, Qouta_Covertable90Days);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string UpdateUsername(string SupplierName, string Username, string Password, bool Hours96, bool Sevice14Day, bool Single30Days, bool Multipule30Days, bool Single90Days, bool Multipule90Days, bool Covertable90Days, bool Active, string Hurs96Price, string Service14DaysPrice, string Single30DaysPrice, string Multipule30DaysPrice, string Single90DaysPrice, string Multipule90DaysPrice, string Covertable90DaysPrice)
        {
            string jsonString = "";
            GlobalDefault objGlobalDefaults = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string AgencyName = objGlobalDefaults.AgencyName;
            DBHelper.DBReturnCode retcode = VisaManager.UpdateUser(Username, Password, Hours96, Sevice14Day, Single30Days, Multipule30Days, Single90Days, Multipule90Days, Covertable90Days, Active, Hurs96Price, Service14DaysPrice, Single30DaysPrice, Multipule30DaysPrice, Single90DaysPrice, Multipule90DaysPrice, Covertable90DaysPrice);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #endregion

        #region View Application
        [WebMethod(true)]
        public string ViewApplication(string RefNo)
        {
            Session["RefNo"] = RefNo;
            if (RefNo != "")
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #region Visa Status
        [WebMethod(EnableSession = true)]
        public string RejectApplication(string Vcode, Int64 AgentId, decimal DepositAmmount)
        {
            DBHelper.DBReturnCode retCode = VisaManager.ApplicationRejected(Vcode, AgentId, DepositAmmount);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\",\"Mail\":\"0\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion
        #endregion

        #region Visa Activity Mails
        [WebMethod(EnableSession = true)]
        public string GetVisaMails(string Activity, string Type)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaManager.GetVisaMails(Activity, Type, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Mails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = VisaManager.UpdateVisaMails(Activity, Type, MailsId, CcMails, BCcMail);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion
    }
}
