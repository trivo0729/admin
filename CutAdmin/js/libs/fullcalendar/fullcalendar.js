$(document).ready(function () {
    GetHolidays();
    LoadAllCount();

});
function GetHolidays() {
    $.ajax({
        type: "POST",
        url: "../Admin/handler/HolidayHandler.asmx/Get_events",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            $('#fullCalendar').fullCalendar('destroy')
            var arrEvents = new Array();
            for (var i = 0; i < result.sData.length; i++) {
                if (result.sData[i].Type == "Holiday")
                    Color = "#FF0000";
                else
                    Color = "#3584ff";
                var Event = {
                    id: result.sData[i].id,
                    title: result.sData[i].title,
                    start: result.sData[i].start,
                    url: result.sData[i].url,
                    end: result.sData[i].ends,
                    Type: result.sData[i].Type,
                    color: Color
                }
                arrEvents.push(Event)
            }
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#fullCalendar').fullCalendar({
                editable: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: arrEvents,
                // Convert the allDay from string to boolean
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                    event.editable = false;
                },
                selectable: true,
                selectHelper: true,
                /*select: function (start, end, allDay) {
                    var title = prompt('Event Title:');
                    // var url = prompt('Type Event url, if exits:');
                    if (title) {
                        $.ajax({
                            type: "POST",
                            url: '../Admin/handler/HolidayHandler.asmx/add_events',
                            data: '{"title":"' + title + '","start":"' + start.format() + '","end":"' + end.format() + '","url":""}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (json) {
                                alert('Added Successfully');
                                GetHolidays();
                            }
                        });
                        calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },*/
                editable: true,
                eventAfterAllRender: function (view) {
                    //$(".fc-event-container").append("<span class='closon'>X</span>");
                },
                eventDrop: function (event, delta) {
                    //$.ajax({
                    //    type: "POST",
                    //    url: '../Admin/handler/HolidayHandler.asmx/update_events',
                    //    data: '{"title":"' + event.title + '","start":"' + event.start.format().replace("T00:00:00", "") + '","end":"' + event.end.format().replace("T00:00:00", "") + '","id":"' + event.id + '"}',
                    //    contentType: "application/json; charset=utf-8",
                    //    datatype: "json",
                    //    success: function (json) {
                    //        alert("Updated Successfully");
                    //        GetHolidays();
                    //    }
                    //});
                },
                eventResize: function (event) {
                    //var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
                    //var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
                    //$.ajax({
                    //    url: '../Admin/handler/HolidayHandler.asmx/update_events',
                    //    data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                    //    type: "POST",
                    //    success: function (json) {
                    //        alert("Updated Successfully");
                    //        GetHolidays();
                    //    }
                    //});

                },
                eventClick: function (event) {
                    //$(".closon").click(function () {
                    //    $('#fullCalendar').fullCalendar('removeEvents', event._id);
                    //});
                },
            });
        }
    });
}