﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ActivityMails.aspx.cs" Inherits="CutAdmin.ActivityMails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ActivityMails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Activity Mails</h1>

        </hgroup>
        <div class="with-padding">
            <div class="standard-tabs margin-bottom" id="add-tabs">
                <ul class="tabs">
                    <li class="active" id="lst_Visa" onclick="MailsFor('Visa')"><a href="#Visa">Visa Mails</a></li>
                    <li id="lst_Otb" onclick="MailsFor('Otb')"><a href="#OTB">OTB Mails</a></li>
                    <li id="lst_Hotel" onclick="MailsFor('Hotel')"><a href="#Hotel">Hotel Mails</a></li>
                </ul>
                <div class="tabs-content">
                    <%-- <span id="commanDataTableHeading">Visa Mails</span>--%>
                    <div id="Visa">
                        <div class="with-padding">

                           <%-- <div class="table-header button-height anthracite-gradient">
                                <div class="float-right">
                                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                                </div>

                                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                                    <option value="1" selected="selected">10</option>
                                    <option value="2">20</option>
                                    <option value="3">40</option>
                                    <option value="4">100</option>
                                </select>
                                entries
                            </div>--%>
                            <div class="respTable">
                                <table class="table responsive-table" id="tbl_VisaDetails">

                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Update</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div id="OTB">
                        <div class="with-padding">
                           <%-- <div class="table-header button-height anthracite-gradient">
                                <div class="float-right">
                                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                                </div>

                                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                                    <option value="1" selected="selected">10</option>
                                    <option value="2">20</option>
                                    <option value="3">40</option>
                                    <option value="4">100</option>
                                </select>
                                entries
                            </div>--%>
                            <div class="respTable">
                                <table class="table responsive-table" id="tbl_OTBDetails">

                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Add | Edit </th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <%--<tr>
                                            <td>Visa Issued by us</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>If OTB not required</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Applied</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Approved</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Rejected</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>OTB Pending</td>
                                            <td>otb@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com;liyaquat@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>--%>


                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                    <div id="Hotel">
                        <div class="with-padding">
                            <%--<div class="table-header button-height anthracite-gradient">
                                <div class="float-right">
                                    Search&nbsp;<input type="text" name="table_search" id="table_search" value="" class="input mid-margin-left">
                                </div>

                                Show&nbsp;<select name="range" class="select anthracite-gradient glossy">
                                    <option value="1" selected="selected">10</option>
                                    <option value="2">20</option>
                                    <option value="3">40</option>
                                    <option value="4">100</option>
                                </select>
                                entries
                            </div>--%>
                            <div class="respTable">
                                <table class="table responsive-table" id="tbl_HotelDetails">

                                    <thead>
                                        <tr>
                                            <th scope="col">Activity</th>
                                            <th scope="col" class="align-center">Mail Id  </th>
                                            <th scope="col" class="align-center">CC Mail Id </th>
                                            <th scope="col" class="align-center">Add | Edit </th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <%--<tr>
                                            <td>Booking Confirm </td>
                                            <td>hotels@clickurtrip.com</td>
                                            <td>acc.online@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Booking Error</td>
                                            <td>online@clickurtrip.com</td>
                                            <td>niyaz@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Booking On Hold</td>
                                            <td>hotels@clickurtrip.com</td>
                                            <td>acc.online@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Booking Cancelled</td>
                                            <td>hotels@clickurtrip.com</td>
                                            <td>-</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Booking Reconfirm</td>
                                            <td>hotels@clickurtrip.com</td>
                                            <td>acc.online@clickurtrip.com</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>
                                        <tr>
                                            <td>Cancelletion Dead Line</td>
                                            <td>online@clickurtrip.com</td>
                                            <td>-</td>
                                            <td class="align-center"><a href="#" data-toggle="modal" data-target="#updatevisa" onclick="updatevisa()" class="button" title="Update"><span class="icon-publish "></span></a></td>
                                        </tr>--%>


                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Favicon count
        Tinycon.setBubble(2);

        // If the browser support the Notification API, ask user for permission (with a little delay)
        if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet()) {
            setTimeout(function () {
                notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function () {
                    // Confirmation message
                    if (notify.hasNotificationPermission()) {
                        notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
                            icon: 'img/demo/icon.png',
                            system: true
                        });
                    }
                    else {
                        notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
                            icon: 'img/demo/icon.png'
                        });
                    }
                });

            }, 2000);
        }

        /*
		 * Handling of 'other actions' menu
		 */

        var otherActions = $('#otherActions'),
			current = false;

        // Other actions
        $('.list .button-group a:nth-child(2)').menuTooltip('Loading...', {

            classes: ['with-mid-padding'],
            ajax: 'ajax-demo/tooltip-content.html',

            onShow: function (target) {
                // Remove auto-hide class
                target.parent().removeClass('show-on-parent-hover');
            },

            onRemove: function (target) {
                // Restore auto-hide class
                target.parent().addClass('show-on-parent-hover');
            }
        });

        // Delete button
        $('.list .button-group a:last-child').data('confirm-options', {

            onShow: function () {
                // Remove auto-hide class
                $(this).parent().removeClass('show-on-parent-hover');
            },

            onConfirm: function () {
                // Remove element
                $(this).closest('li').fadeAndRemove();

                // Prevent default link behavior
                return false;
            },

            onRemove: function () {
                // Restore auto-hide class
                $(this).parent().addClass('show-on-parent-hover');
            }

        });

        // updatevisa modal
        //function VisaModalFunc() {
         
        //};





    </script>
</asp:Content>
