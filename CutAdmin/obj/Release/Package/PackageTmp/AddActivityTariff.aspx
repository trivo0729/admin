﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddActivityTariff.aspx.cs" Inherits="CutAdmin.AddActivityTariff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/AddActivityTariff.js?V=1.2"></script>

    <script type="text/javascript" src="Scripts/ActMaster.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });


        });


    </script>

    <style type="text/css">
        div.Inclusion {
            height: 150px;
            overflow-y: auto;
            margin-top: 10px;
            background-color: #e6e6e6;
        }

        label.lblAtraction {
            cursor: pointer;
        }

        ::-webkit-scrollbar {
            width: 2px; /* for vertical scrollbars */
            height: 8px; /* for horizontal scrollbars */
        }

        ::-webkit-scrollbar-track {
            background: rgb(255, 255, 255);
        }

        ::-webkit-scrollbar-thumb {
            /*background: #ff9900;*/
            position: relative;
            top: 17px;
            float: right;
            width: 5px;
            height: 32px;
            background-color: rgb(204, 204, 204);
            border: 0px solid rgb(255, 255, 255);
            background-clip: padding-box;
            border-radius: 0px;
        }

        @media only screen and (max-width: 767px) {

            span {
                font-size: .8em;
            }
        }
    </style>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <%-- <hgroup id="main-title" class="thin">
            <h1>View Activity</h1>
        </hgroup>--%>

        <div class="with-padding">
            <div class="large-box-shadow white-gradient with-border">

                <div class="blue-gradient panel-control" style="height: 80px">
                    <h3 style="color: white" id="ActivityName"></h3>

                    <h3 style="margin-top: -5px">
                        <small class="white" style="float: left; vertical-align: middle"><b id="LocationName" style="font-size: 13px"></b></small>
                    </h3>

                </div>

 
                <div class="with-padding">

                    <div class="standard-tabs margin-bottom" id="addd-tabs">

                        <ul class="tabs">
                            <li id="11" style="display: none"><a href="#tab-11" style="cursor: pointer" onclick="">Ticket Only</a></li>
                            <li id="22" style="display: none"><a href="#tab-22" style="cursor: pointer" onclick="">Sharing</a></li>
                            <li id="33" style="display: none"><a href="#tab-33" style="cursor: pointer" onclick="">Private</a></li>
                        </ul>

                        <div class="tabs-content">

                            <%-- Start  Tab 1--%>
                            <div id="tab-11" class="with-padding">
                                <div class="columns">

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Supplier & Validity</h6>
                                    </div>

                                    <div class="new-row three-columns">
                                        <%--<h6>Supplier:</h6>--%>
                                        <label class="input-info">Supplier</label><span class="red">*</span>
                                        <select id="ddlSupplier" name="ddlSupplier" class="full-width select validate[required]">
                                            <option value="" selected="selected" disabled>Please select</option>
                                            <%--  <option value="clickurtrip.com">clickurtrip.com</option>--%>
                                        </select>
                                    </div>
                                    <div class="three-columns" id="TimeSlot" style="">

                                        <label class="input-info">Select Time Slots</label><span class="red">*</span>
                                        <select id="ddlTimeSlots" class="full-width glossy select multiple-as-single easy-multiple-selection PSLot check-list" multiple onchange="fnSlot()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>
                                    <div id="DatesUI" class="six-columns">
      
                                    </div>

                                    <div class="four-columns" id="tktSlot" style="display: none">
                                        <h6>Activity Time Slots </h6>
                                        <select id="ddlSlot" name="ddlSlot" class="full-width select validate[required]" onchange="SlotChange()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>


                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Rates</h6>
                                    </div>

                                    <div class="new-row twelve-columns" id="UIDates" style="margin-bottom:5px">

                                        <%--<div class="" id="UIDates">--%>
                                        <label style="font-size: 13px; font: bolder; color: orange">Dates</label>
                                        <%--</div>--%>
                                        <%--<label style="font-size:15px" id="lbl_Date"></label>--%>
                                    </div>


                                    <%--<div class="new-row twelve-columns" id="SlotTiming">
                                            <label style="font-size: 18px; font: bold;color:orange">Slot Timing:</label>
                                        </div>--%>

                                    <%--<div id="TimingSlot">
                                        </div>--%>

                                    <div id="RatesDetails" style="width: 98%">

                                        <%-- <div class="new-row three-columns">
                                            <h6>Currency :</h6>
                                            <select id="ddlCurrency" name="ddlCurrency" class="full-width select validate[required]">
                                                <option selected="" value="">Select</option>
                                                 <option value="AED">AED</option>
                                               
                                            </select>
                                        </div>
                                        <div class="three-columns">
                                            <h6>Adult :</h6>
                                            <input type="text" name="Text[]" id="txt_Adult" class="input full-width" value="">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Child 11 – 5 :</h6>
                                            <input type="text" name="Text[]" id="txt_Child_11-5" class="input full-width" value="">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Child 4 – 2 :</h6>
                                            <input type="text" name="Text[]" id="txt_Child_4-2" class="input full-width" value="">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Infant :</h6>
                                            <input type="text" name="Text[]" id="txt_Infant" class="input full-width" value="">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Cancellation Policy :</h6>
                                            <select id="ddlCancellation" name="ddlCancellation" class="full-width select validate[required]" onchange="SpecialDate()">
                                                <option selected="" value="">Select</option>
                                               
                                            </select>
                                        </div>
                                        <div class="three-columns">
                                            <h6>Offer :</h6>
                                           <select id="ddlOffer" name="ddlOffer" class="full-width select validate[required]" onchange="SpecialDate()">
                                                <option selected="" value="">Select</option>
                                              
                                            </select>
                                        </div>
                                     


                                        <div class="new-row three-columns">
                                            <h6>Inclusions :</h6>
                                            <input type="text" name="Text[]" id="txt_InclusionsTkt" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="InclusionsAdd('Tkt')" id="btnSerchName" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>

                                        <div class="three-columns">
                                            <h6>Exclusions  :</h6>
                                            <input type="text" name="Text[]" id="txt_ExclusionsTkt" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="ExclusionsAdd('Tkt')" id="btnSerchNamee" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>




                                        <div class="new-row four-columns">
                                            <div id="idinclusionTkt" class="Inclusion"></div>
                                        </div>

                                        <div class="four-columns">
                                            <div id="idexclusionTkt" class="Inclusion"></div>
                                        </div>
                                          <div class="four-columns">
                                            <h6>Tariff Note :</h6>
                                            <textarea id="txt_Tariffnote" class="input full-width autoexpanding" rows="8"></textarea>
                                        </div>--%>
                                    </div>

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Special Dates</h6>
                                    </div>

                                    <div class="new-row three-columns">

                                        <span class="button-group">
                                            <label for="chksplDateYes" class="button blue-active">
                                                <input type="radio" name="button-radio chksplDate" id="chksplDateYes" value="Yes" onclick="SpecialDate(this.value);">
                                                YES
                                            </label>
                                            <label for="chksplDateNo" class="button blue-active">
                                                <input type="radio" checked name="button-radio chksplDate" id="chksplDateNo" value="No" onclick="SpecialDate(this.value);">
                                                NO
                                            </label>

                                        </span>
                                    </div>

                                    <%--                                    <div class="new-row four-columns">
                                        <h6>Special Dates :</h6>
                                        <select id="ddlSpecialDates" name="ddlSpecialDates" class="full-width select validate[required]" onchange="SpecialDate()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>--%>

                                    <div class="three-columns" id="SpecialTimeSlot" style="display: none">
                                        <label class="input-info">Select Time Slots</label><span class="red">*</span>
                                        <select id="ddlSpecialTimeSlots" class="full-width glossy select multiple-as-single easy-multiple-selection SplPSLot check-list" multiple onchange="fnSpecialSlot()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>

                                    <div id="SpecialDates" class="six-columns">
                                    </div>



                                    <div id="SdateSlot" class="four-columns" style="display: none">

                                        <h6>Activity By Slots </h6>
                                        <select id="ddlDateSlot" name="ddlDateSlot" class="full-width select validate[required]" onchange="SDateSlotChange()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>


                                    <div class="new-row twelve-columns" id="SpecialDateTKTDiv" style="margin-bottom: -2px; display: none">
                                        <h6 class="underline margin-bottom">Rates For Special Dates</h6>
                                    </div>
                                    <div id="UISplDatesTKT" class="new-row twelve-columns" style="margin-bottom: 7px; display: none">

                                        <%--<div class="" id="UISplDatesTKT" style="display: none">--%>
                                        <label style="font-size: 13px; font: bolder; color: orange">Special Dates</label>
                                        <%--</div>--%>
                                        <%--<label style="font-size:15px" id="lbl_Date"></label>--%>
                                    </div>

                                    <div id="SpecialRatesDetails" style="width: 98%">
                                    </div>

                                    <%-- <div class="new-row four-columns">
                                            <h6>Adult Height :</h6>
                                            <input type="text" name="Text[]" onkeypress="return isDecimalNumber(event)" id="txt_AdultAge" class="input full-width" />
                                        </div>

                                        <div class="four-columns">
                                            <h6>Adult Price :</h6>
                                            <input type="text" name="Text[]" onkeypress="return isNumber(event)" id="txt_Adultprice" class="input full-width" />
                                        </div>

                                        <div class="four-columns">
                                            <h6>Infant Price :</h6>
                                            <input type="text" name="Text[]" onkeypress="return isNumber(event)" id="txt_infantprice" class="input full-width" />
                                        </div>

                                        <div class="new-row two-columns">
                                            <h6>Kid 1 <small>(2 to 7 yr)</small> Price :</h6>
                                            <input type="text" name="Text[]" id="txt_childprice" class="input full-width" value="">
                                        </div>

                                        <div class="two-columns">
                                            <h6>Kid 2 <small>(7 to 11 yr)</small> Price :</h6>
                                            <input type="text" name="Text[]" id="txt_childprice2" class="input full-width" value="">
                                        </div>

                                        <div class="four-columns">
                                            <h6>Kid Age :</h6>
                                            <input type="text" name="Text[]" onkeypress="return isDecimalNumber(event)" id="txt_child_Age" class="input full-width" />

                                            <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Acttype" value="TKT" class="form-control" style="display: none" />
                                        </div>

                                        <div class="four-columns">
                                            <h6>Adult Age Start :</h6>
                                            <input type="number" name="Text[]" onkeypress="return isNumber(event)" id="txt_adultagestart" class="input full-width" value="">
                                        </div>

                                        <div class="new-row four-columns">
                                            <h6>Duration :</h6>
                                            <input type="time" name="Text[]" id="txt_Duration" class="input full-width" value="">
                                        </div>

                                        <div class="four-columns">
                                            <h6>Start Time :</h6>
                                            <input type="time" name="Text[]" id="txt_Starttime" class="input full-width" value="">
                                        </div>

                                        <div class="four-columns">
                                            <h6>End Time :</h6>
                                            <input type="time" name="Text[]" id="txt_Endtime" class="input full-width" value="">
                                        </div>--%>

                                    <div class="new-row twelve-columns" id="ChildPolicy" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Child Policy</h6>
                                    </div>

                                    <div class="columns" id="ChildPolicyAllow">





                                        <div class="new-row three-columns">
                                            <label class="input-info">Child Age From</label>

                                            <input type="text" name="Text[]" id="txt_ChildAgeFrom" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Child Age Upto</label>
                                            <input type="text" name="Text[]" id="txt_ChildAgeUpto" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Small Child Age Upto</label>
                                            <input type="text" name="Text[]" id="txt_SmallChildAgeupto" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Child Min Hight</label>
                                            <input type="text" name="Text[]" id="txt_ChildMinHeight" class="input full-width" value="" readonly="readonly">
                                        </div>
                                    </div>




                                    <div class="new-row twelve-columns">
                                        <h6></h6>
                                    </div>


                                    <div class="new-row ten-columns">
                                    </div>
                                    <div class="two-columns">
                                        <button type="button" class="button glossy mid-margin-right" onclick="Save2();" title="Submit Details">
                                            <span class="button-icon"><span class="icon-tick"></span></span>
                                            Add
                                        </button>
                                        <%--<button type="button" class="button glossy" onclick="window.location.href='AdminDashBoard.aspx'">
                                            <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
                                            Cancel
                                        </button>
                                        <button type="button" class="button glossy" onclick="DeleteActivityTariff('Tkt');">
                                            <span class="button-icon red-gradient"><span class="icon-minus-round"></span></span>
                                            Delete
                                        </button>--%>
                                    </div>

                                </div>
                                <input type="hidden" id="hdn_Tkt" value="0" />
                            </div>
                            <%-- End  Tab 1--%>




                            <%-- Start  Tab 2--%>
                            <div id="tab-22" class="with-padding">
                                <div class="columns">

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Supplier & Validity</h6>
                                    </div>

                                    <div class="new-row three-columns">
                                        <label class="input-info">Supplier</label><span class="red">*</span>
                                        <select id="ddlSupplierSIC" name="ddlSupplier" class="full-width select validate[required]">
                                            <option value="" selected="selected" disabled>Please select</option>

                                        </select>
                                    </div>
                                    <div class="three-columns" id="TimeSlotSIC" style="">
                                        <label class="input-info">Select Time Slots </label>
                                        <select id="ddlTimeSlotsSIC" class="full-width glossy select multiple-as-single easy-multiple-selection Tslots  check-list" multiple onchange="fnSlotSIC()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>
                                    <div id="DatesUISIC" class="six-columns">
                                        <%--<div class="four-columns">
                                                <h6>Pick up from :</h6>
                                                <input type="text" name="Text[]" id="txt_pickupfrom" class="input full-width" value="">
                                            </div>

                                            <div class="four-columns">
                                                <h6>Drop time :</h6>
                                                <input type="time" id="txt_DropTime" class="input full-width" value="">
                                            </div>--%>
                                    </div>


                                    <div class="four-columns" id="sicslot" style="display: none">
                                        <h6>Activity Time Slots </h6>
                                        <select id="ddlSlotSIC" name="ddlSlotSIC" class="full-width select validate[required]" onchange="SlotChangeSIC()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Rates</h6>
                                    </div>

                                    <%--<div class="new-row twelve-columns">
                                           
                                          
                                        </div>--%>


                                    <div class="new twelve-columns" id="UIDatesSIC" style="margin-bottom:5px">
                                        <label style="font-size: 13px; color: orange">Dates</label>
                                    </div>

                                    <%--<div class="new-row twelve-columns" id="SlotTimingSIC">
                                            <label style="font-size: 18px; font: bold;color:orange">Slot Timing:</label>
                                        </div>--%>


                                    <div id="RatesDetailsSIC" style="width: 98%">
                                    </div>

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Special Dates</h6>
                                    </div>
                                    <div class="new-row three-columns">
                                        <%--<h6>Special Dates :</h6>
                                        <select id="ddlSpecialDatesSIC" name="ddlSpecialDatesSIC" class="full-width select validate[required]" onchange="SpecialDateSIC()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>--%>
                                        <span class="button-group">
                                            <label for="sicchksplDateYes" class="button blue-active">
                                                <input type="radio" name="button-radio sicchksplDate" id="sicchksplDateYes" value="Yes" onclick="SpecialDateSIC(this.value);">
                                                YES
                                            </label>
                                            <label for="sicchksplDateNo" class="button blue-active">
                                                <input type="radio" checked name="button-radio sicchksplDate" id="sicchksplDateNo" value="No" onclick="SpecialDateSIC(this.value);">
                                                NO
                                            </label>

                                        </span>

                                    </div>

                                    <div class="three-columns" id="SpecialTimeSlotSIC" style="display: none">
                                        <label class="input-info">Select Time Slots</label><span class="red">*</span>
                                        <select id="ddlSpecialTimeSlotsSIC" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSpecialSlotSIC()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>

                                    <div id="SpecialDatesSIC" class="six-columns">
                                    </div>

                                    <div id="SdateSlotSIC" class="four-columns" style="display: none">

                                        <h6>Activity By Slots </h6>
                                        <select id="ddlDateSlotSIC" name="ddlDateSlotSIC" class="full-width select validate[required]" onchange="SDateSlotChangeSIC()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>
                                    <div class="new-row twelve-columns" id="SpecialDateSICDiv" style="margin-bottom: -2px; display: none">
                                        <h6 class="underline margin-bottom">Rates For Special Dates</h6>
                                    </div>
                                    <div class="new-row twelve-columns">

                                        <div class="" id="UISplDatesSIC" style="display: none; margin-bottom: -15px">
                                            <label style="font-size: 13px; font: bolder; color: orange">Special Dates</label>
                                        </div>
                                        <%--<label style="font-size:15px" id="lbl_Date"></label>--%>
                                    </div>
                                    <div id="SpecialRatesDetailsSIC" style="width: 98%">
                                    </div>


                                    <%--                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Pick Up & Drop Timing</h6>
                                    </div>
                                    <div class="new-row three-columns">
                                        <small class="input-info">Pick-Up From:</small>

                                        <input type="text" name="Text[]" id="txt_Pickupfrom" class="input full-width" value="" readonly="readonly">
                                    </div>
                                    <div class="three-columns">
                                        <small class="input-info">Pick-Up Time:</small>
                                        <input type="text" name="Text[]" id="txt_Pickupto" class="input full-width" value="" readonly="readonly">
                                    </div>
                                    <div class="three-columns">
                                        <small class="input-info">Drop-Off At :</small>
                                        <input type="text" name="Text[]" id="txt_Dropofat" class="input full-width" value="" readonly="readonly">
                                    </div>
                                    <div class="three-columns">
                                        <small class="input-info">Drop-Off Time:</small>
                                        <input type="text" name="Text[]" id="txt_Dropoftime" class="input full-width" value="" readonly="readonly">
                                    </div>
                                    <div class="new-row four-columns">
                                        <a onclick="Editable()" style="cursor: pointer;">Want To Edit Pickup & Drop Off Timing?</a>
                                    </div>--%>


                                    <div id="sicchildpolicyy" class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h6 class="underline margin-bottom">Child Policy</h6>
                                    </div>
                                    <div class="columns" id="sicchildpolicy">

                                        <div class="new-row three-columns">
                                            <label class="input-info">Child Age From</label>

                                            <input type="text" name="Text[]" id="txt_ChildAgeFromSIC" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Child Age Upto 	 </label>
                                            <input type="text" name="Text[]" id="txt_ChildAgeUptoSIC" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Small Child Age Upto </label>
                                            <input type="text" name="Text[]" id="txt_SmallChildAgeuptoSIC" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <label class="input-info">Child Min Hight</label>
                                            <input type="text" name="Text[]" id="txt_ChildMinHeightSIC" class="input full-width" value="" readonly="readonly">
                                        </div>
                                    </div>






                                    <%--                                        <div class="new-row five-columns">
                                            <h6>Inclusions :</h6>
                                            <input type="text" name="Text[]" id="txt_InclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="InclusionsAdd('Sic')" id="btnSerchName2" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>

                                        <div class="five-columns">
                                            <h6>Exclusions  :</h6>
                                            <input type="text" name="Text[]" id="txt_ExclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="ExclusionsAdd('Sic')" id="btnSerchName3" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>


                                        <div class="six-columns">
                                            <div id="idinclusionSic" class="Inclusion"></div>
                                        </div>

                                        <div class="six-columns">
                                            <div id="idexclusionSic" class="Inclusion"></div>
                                        </div>--%>



                                    <div class="new-row twelve-columns">
                                        <h6></h6>
                                    </div>


                                    <div class="new-row ten-columns">
                                    </div>
                                    <div class="two-columns">
                                        <button type="button" id="btn_RegiterAgent" class="button glossy mid-margin-right" onclick="Save();" title="Submit Details">
                                            <span class="button-icon"><span class="icon-tick"></span></span>
                                            Add
                                        </button>
                                        <%--<button type="button" class="button glossy" onclick="window.location.href='AdminDashBoard.aspx'">
                                            <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
                                            Cancel
                                        </button>
                                        <button type="button" id="btn_RegiterAgent1" class="button glossy" onclick="DeleteActivityTariff('Sic');">
                                            <span class="button-icon red-gradient"><span class="icon-minus-round"></span></span>
                                            Delete
                                        </button>--%>
                                    </div>


                                </div>
                                <input type="hidden" id="hdn_Sic" value="0" />
                            </div>

                            <%-- End  Tab 2--%>

                            <%-- Start  Tab 3--%>
                            <div id="tab-33" class="with-padding">
                                <div class="columns">

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h3 class="thin underline green">Supplier & Validity</h3>
                                    </div>

                                    <div class="new-row four-columns">
                                        <h6>Supplier<span class="red">*</span></h6>
                                        <select id="ddlSupplierPVT" name="ddlSupplier" class="full-width select validate[required]">
                                            <option value="" selected="selected" disabled>Please select</option>

                                        </select>
                                    </div>

                                    <div id="DatesUIPVT" class="eight-columns">
                                        <%--<div class="four-columns">
                                                <h6>Pick up from :</h6>
                                                <input type="text" name="Text[]" id="txt_pickupfrom" class="input full-width" value="">
                                            </div>

                                            <div class="four-columns">
                                                <h6>Drop time :</h6>
                                                <input type="time" id="txt_DropTime" class="input full-width" value="">
                                            </div>--%>
                                    </div>

                                    <div class="new-row four-columns" id="pvtslot">
                                        <h6>Activity Time Slots </h6>
                                        <select id="ddlSlotPVT" name="ddlSlotPVT" class="full-width select validate[required]" onchange="SlotChangePVT()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                    <div class="four-columns" id="TimeSlotPVT" style="display: none">
                                        <h6>Select Time Slots </h6>
                                        <select id="ddlTimeSlotsPVT" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSlotPVT()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>
                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h3 class="thin underline green">Special Dates</h3>
                                    </div>
                                    <div class="new-row four-columns">
                                        <h6>Special Dates </h6>
                                        <select id="ddlSpecialDatesPVT" name="ddlSpecialDatesPVT" class="full-width select validate[required]" onchange="SpecialDatePVT()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>

                                    <div id="SpecialDatesPVT" class="eight-columns">
                                    </div>

                                    <div id="SdateSlotPVT" class="new-row four-columns" style="display: none">

                                        <h6>Activity By Slots </h6>
                                        <select id="ddlDateSlotPVT" name="ddlDateSlotPVT" class="full-width select validate[required]" onchange="SDateSlotChangePVT()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>

                                    <div class="four-columns" id="SpecialTimeSlotPVT" style="display: none">
                                        <h6>Select Time Slots </h6>
                                        <select id="ddlSpecialTimeSlotsPVT" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSpecialSlotPVT()">
                                            <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                        </select>
                                    </div>
                                    <div id="SpecialRatesDetailsPVT" style="width: 100%">
                                    </div>


                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h3 class="thin underline green">Pick Up & Drop Timing</h3>
                                    </div>
                                    <div class="new-row three-columns">
                                        <h6>Pick-Up From</h6>

                                        <input type="text" name="Text[]" id="txt_PickupfromPVT" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns">
                                        <h6>Pick-Up Time</h6>
                                        <input type="text" name="Text[]" id="txt_PickuptoPVT" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns">
                                        <h6>Drop-Off At </h6>
                                        <input type="text" name="Text[]" id="txt_DropofatPVT" class="input full-width" value="">
                                    </div>
                                    <div class="three-columns">
                                        <h6>Drop-Off Time</h6>
                                        <input type="text" name="Text[]" id="txt_DropoftimePVT" class="input full-width" value="">
                                    </div>



                                    <div class="columns" id="pvtchildpolicy">
                                        <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                            <h3 class="thin underline green">Child Policy</h3>
                                        </div>
                                        <div class="new-row three-columns">
                                            <h6>Child Age From 	 </h6>

                                            <input type="text" name="Text[]" id="txt_ChildAgeFromPVT" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Child Age Upto 	 </h6>
                                            <input type="text" name="Text[]" id="txt_ChildAgeUptoPVT" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Small Child Age Upto </h6>
                                            <input type="text" name="Text[]" id="txt_SmallChildAgeuptoPVT" class="input full-width" value="" readonly="readonly">
                                        </div>
                                        <div class="three-columns">
                                            <h6>Child Min Hight</h6>
                                            <input type="text" name="Text[]" id="txt_ChildMinHeightPVT" class="input full-width" value="" readonly="readonly">
                                        </div>
                                    </div>






                                    <%--                                        <div class="new-row five-columns">
                                            <h6>Inclusions :</h6>
                                            <input type="text" name="Text[]" id="txt_InclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="InclusionsAdd('Sic')" id="btnSerchName2" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>

                                        <div class="five-columns">
                                            <h6>Exclusions  :</h6>
                                            <input type="text" name="Text[]" id="txt_ExclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="ExclusionsAdd('Sic')" id="btnSerchName3" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>


                                        <div class="six-columns">
                                            <div id="idinclusionSic" class="Inclusion"></div>
                                        </div>

                                        <div class="six-columns">
                                            <div id="idexclusionSic" class="Inclusion"></div>
                                        </div>--%>

                                    <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                        <h3 class="thin underline green">Rates</h3>
                                    </div>

                                    <%--<div class="new-row twelve-columns">
                                            
                                         
                                        </div>--%>


                                    <div class="new twelve-columns" id="UIDatesPVT">
                                        <label style="font-size: 18px; font: bold; color: orange">Dates</label>
                                    </div>

                                    <%--<div class="new-row twelve-columns" id="SlotTimingPVT">
                                            <label style="font-size: 18px; font: bold;color:orange">Slot Timing:</label>
                                        </div>--%>


                                    <div id="RatesDetailsPVT" style="width: 100%">
                                    </div>

                                    <div class="new-row twelve-columns">
                                        <h6></h6>
                                    </div>


                                    <div class="new-row ten-columns">
                                    </div>
                                    <div class="two-columns">
                                        <button type="button" <%--id="btn_RegiterAgent"--%> class="button glossy mid-margin-right" onclick="Save3();" title="Submit Details">
                                            <span class="button-icon"><span class="icon-tick"></span></span>
                                            Add
                                        </button>
                                        <%-- <button type="button" class="button glossy" onclick="window.location.href='AdminDashBoard.aspx'">
                                            <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
                                            Cancel
                                        </button>
                                        <button type="button"  class="button glossy" onclick="DeleteActivityTariff('Pvt');">
                                            <span class="button-icon red-gradient"><span class="icon-minus-round"></span></span>
                                            Delete
                                        </button>--%>
                                    </div>


                                </div>
                                <input type="hidden" id="hdn_Pvt" value="0" />
                            </div>
                            <%-- End  Tab 3--%>
                        </div>

                    </div>
                </div>
            </div>
        </div>



    </section>
    <!-- End main content -->

    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

    <!-- jQuery Form Validation -->
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>

</asp:Content>


