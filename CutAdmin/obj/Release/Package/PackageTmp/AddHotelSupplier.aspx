﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddHotelSupplier.aspx.cs" Inherits="CutAdmin.AddHotelSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ApiDetails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Hotel Supplier</h1>
        </hgroup>
        <div class="with-padding">
            <form action="#" class="frmSupplier">
                <h3>Company Information</h3>
                <hr />
                <div class="columns">
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Supplier<span class="red">*</span>:</label><div class="input full-width">
                            <input id="txt_Supplier" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Name of the Company" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Name of the Company:</label><div class="input full-width">
                            <input id="txt_Sup_Company" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Name of the Company" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>City:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_City" name="prompt-value" value="" class="input-unstyled full-width" placeholder="City" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Country:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Country" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Country" type="text">
                        </div>
                    </div>
                     <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>State:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_State" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Country" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Pin / Zip Code </label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Zip" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Pin / Zip Code " type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Email:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Email" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Telephone No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Phone" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Telephone No" type="text">
                        </div>
                    </div>

                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Fax No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Fax" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Fax No." type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Company Mobile No:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Mobile" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Company Mobile No" type="text">
                        </div>
                    </div>
                    <div class="six-columns six-columns-tablet twelve-columns-mobile">
                        <label>Address:</label><br />
                        <div class="input full-width">
                            <input id="txt_Address" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Address " type="text">
                        </div>
                    </div>
                </div>
                <h3>Owner / Director / Partner Details</h3>
                <hr />

                <div class="columns">
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Full Name </label><div class="input full-width">
                            <input id="txt_Sup_OwnerName" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Full Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Email :</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_OwnerEmail" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Mobile No. :</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_OwnerMobile" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                </div>
                <h3>Reservation Department Details</h3>
                <hr />
                <div class="columns">
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Reservation Manager </label><div class="input full-width">
                            <input id="txt_Sup_ResendName"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Full Name" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Email <span class="red">*</span>:</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_ResendEmail"  name="prompt-value" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Phone No. :</label><br />
                        <div class="input full-width">
                            <input  id="txt_Resend_Phone" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Mobile No. :</label><br />
                        <div class="input full-width">
                            <input id="txt_Resend_Mobile" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>

                </div>

                <h3>Accounts Department Details</h3>
                <hr />
                <div class="columns">
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Accounts Manager </label><div class="input full-width">
                            <input id="txt_Sup_Acc_Name" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Full Name" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Email :</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Acc_Email" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Phone No. :</label><br />
                        <div class="input full-width">
                            <input id="txt_Sup_Acc_Phone" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Mobile No. :</label><br />
                        <div class="input full-width">
                            <input  id="txt_Sup_Acc_Mobile" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>

                </div>
           <%--   Extra Details--%>
              <%--  <h3>Other Information</h3>
                <hr />

                <div class="columns">
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Taxation Details<span class="red"></span></label><div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="Taxation Details" type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>GST / VAT Registration No.:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="GST / VAT Registration No." type="text">
                        </div>
                    </div>
                    <div class="four-columns six-columns-tablet twelve-columns-mobile">
                        <label>Pan Card (active only if Indian) :</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="Pan Card (active only if Indian)" type="text">
                        </div>
                    </div>
                </div>

                <h3>Bank Details</h3>
                <hr />
                <div class="columns">
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Name of the Bank</label><div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="Name of the Bank" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>A/c No.</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="A/c No." type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>Branch:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="Branch" type="text">
                        </div>
                    </div>
                    <div class="three-columns six-columns-tablet twelve-columns-mobile">
                        <label>SWIFT:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" value="" class="input-unstyled full-width" placeholder="SWIFT" type="text">
                        </div>
                    </div>

                </div>
                <div class="columns">
                    <div class="twelve-columns six-columns-tablet twelve-columns-mobile textaraBank">
                        <label>Other Details </label>
                        <div class="input full-width">
                            <textarea name="" cols="" rows="" class="input-unstyled full-width"></textarea>
                        </div>
                    </div>
                </div>--%>
                 <%--  End of Extra Details--%>

                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <input type="checkbox"  id="chkActive" value="1" class="checkbox mid-margin-left"><label>Active:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <label>	Commission</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <label>TDS</label>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                         <input type="checkbox"  id="chkHotel" value="Hotel" class="checkbox mid-margin-left">
                        <label>Hotel:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <div class="input full-width">
                            <input id="txt_HotelComm1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                       <div class="input full-width">
                            <input id="txt_HotelTDS1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                         <input id="chkVisa1" type="checkbox" value="Visa"  class="checkbox mid-margin-left">
                        <label>Visa:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <div class="input full-width">
                            <input id="txt_VisaComm1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                       <div class="input full-width">
                            <input id="txt_VisaTDS1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                         <input type="checkbox"  id="chkOtb1" value="Otb" class="checkbox mid-margin-left">
                        <label>Otb:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <div class="input full-width">
                            <input id="txt_OtbComm1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                       <div class="input full-width">
                            <input id="txt_OtbTDS1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                         <input type="checkbox"  id="chkPackages" value="Packages" class="checkbox mid-margin-left">
                        <label>Packages:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <div class="input full-width">
                            <input id="txt_PackageComm1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                       <div class="input full-width">
                            <input id="txt_PackageTDS1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                         <input type="checkbox"  id="chkFlight" value="Flight" class="checkbox mid-margin-left">
                        <label>Flight:</label>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                        <div class="input full-width">
                            <input id="txt_FlightComm1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                    <div class="four-columns two-columns-tablet four-columns-mobile">
                       <div class="input full-width">
                            <input id="txt_FlightTDS1" name="prompt-value" value="" class="input-unstyled full-width" placeholder="Mobile No" type="text">
                        </div>
                    </div>
                   
                </div>
                 <%--<div class="four-columns three-columns-tablet five-columns-mobile">
                        <input type="checkbox"  id="checkbox-2" value="1" class="checkbox mid-margin-left">
                        <label>Packages:</label>
                    </div>--%>
                <p class="text-right" style="text-align:right;">
                    <button type="button" class="button anthracite-gradient UpdateMarkup" id="btn_Supplier" onclick="AddUpdate()">ADD</button>
                    <button type="button" class="button anthracite-gradient" onclick="window.location.href = 'Suppliers.aspx'">Back</button>
                </p>
            </form>



        </div>
    </section>
    <!-- End main content -->
</asp:Content>
