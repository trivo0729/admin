﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddLocation.aspx.cs" Inherits="CutAdmin.AddLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/AddLocation.js?v=1.32"></script>
    <%--<script src="Scripts/CountryCityCode.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            GetCountry();
        });
    </script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Location Details</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <%--<div class="columns">
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Country</label>
                            <div class="full-width button-height" onchange="GetCity()">
                                <span class="select replacement select-styled-list tracked" tabindex="0"><select id="selCountry" class="withClearFunctions" tabindex="-1"><option selected="selected" value="-">Select Any Country</option><option value="AF">AFGHANISTAN</option><option value="AL">ALBANIA</option><option value="DZ">ALGERIA</option><option value="AD">ANDORRA</option><option value="AI">ANGUILLA</option><option value="AR">ARGENTINA</option><option value="AU">AUSTRALIA</option><option value="AT">AUSTRIA</option><option value="BS">BAHAMAS</option><option value="BH">BAHRAIN</option><option value="BD">BANGLADESH</option><option value="BB">BARBADOS</option><option value="BY">BELARUS</option><option value="BE">BELGIUM</option><option value="BZ">BELIZE</option><option value="BJ">BENIN</option><option value="BT">BHUTAN</option><option value="BO">BOLIVIA</option><option value="BA">BOSNIA AND HERZEGOVINA</option><option value="BR">BRAZIL</option><option value="BG">BULGARIA</option><option value="KH">CAMBODIA</option><option value="CA">CANADA</option><option value="CL">CHILE</option><option value="CN">CHINA</option><option value="CO">COLOMBIA</option><option value="CR">COSTA RICA</option><option value="HR">CROATIA</option><option value="CY">CYPRUS</option><option value="CZ">CZECH REPUBLIC</option><option value="DK">DENMARK</option><option value="EC">ECUADOR</option><option value="EG">EGYPT</option><option value="EE">ESTONIA</option><option value="FJ">FIJI</option><option value="FI">FINLAND</option><option value="FR">FRANCE</option><option value="GE">GEORGIA</option><option value="DE">GERMANY</option><option value="GR">GREECE</option><option value="GL">GREENLAND</option><option value="HK">HONG KONG</option><option value="IN">INDIA</option><option value="ID">INDONESIA</option><option value="IE">IRELAND</option><option value="IL">ISRAEL</option><option value="IT">ITALY</option><option value="JM">JAMAICA</option><option value="JP">JAPAN</option><option value="JO">JORDAN</option><option value="KZ">KAZAKHSTAN</option><option value="KE">KENYA</option><option value="KW">KUWAIT</option><option value="KG">KYRGYZSTAN</option><option value="LA">LAOS</option><option value="LV">LATVIA</option><option value="LB">LEBANON</option><option value="LR">LIBERIA</option><option value="LY">LIBYA</option><option value="LI">LIECHTENSTEIN</option><option value="LT">LITHUANIA</option><option value="LU">LUXEMBOURG</option><option value="MO">MACAU</option><option value="MK">MACEDONIA</option><option value="MY">MALAYSIA</option><option value="MV">MALDIVES</option><option value="MT">MALTA</option><option value="MQ">MARTINIQUE</option><option value="MR">MAURITANIA</option><option value="MU">MAURITIUS</option><option value="MX">MEXICO</option><option value="FM">MICRONESIA</option><option value="MD">MOLDOVA</option><option value="MC">MONACO</option><option value="MN">MONGOLIA</option><option value="ME">MONTENEGRO</option><option value="MA">MOROCCO</option><option value="MZ">MOZAMBIQUE</option><option value="MM">MYANMAR</option><option value="NA">NAMIBIA</option><option value="NP">NEPAL</option><option value="NL">NETHERLANDS</option><option value="NZ">NEW ZEALAND</option><option value="NI">NICARAGUA</option><option value="NE">NIGER</option><option value="NG">NIGERIA</option><option value="NO">NORWAY</option><option value="OM">OMAN</option><option value="PK">PAKISTAN</option><option value="PW">PALAU</option><option value="PA">PANAMA</option><option value="PY">PARAGUAY</option><option value="PE">PERU</option><option value="PH">PHILIPPINES</option><option value="PL">POLAND</option><option value="PO">PORTUGAL</option><option value="PR">PUERTO RICO</option><option value="QA">QATAR</option><option value="RO">ROMANIA</option><option value="RU">RUSSIA</option><option value="RW">RWANDA</option><option value="BL">SAINT BARTHÉLEMY</option><option value="KN">SAINT KITTS AND NEVIS</option><option value="WS">SAMOA</option><option value="SM">SAN MARINO</option><option value="SA">SAUDI ARABIA</option><option value="SN">SENEGAL</option><option value="RS">SERBIA</option><option value="SC">SEYCHELLES</option><option value="SG">SINGAPORE</option><option value="SK">SLOVAKIA</option><option value="SI">SLOVENIA</option><option value="ZA">SOUTH AFRICA</option><option value="KR">SOUTH KOREA</option><option value="ES">SPAIN</option><option value="LK">SRI LANKA</option><option value="LC">ST LUCIA</option><option value="SD">SUDAN</option><option value="SE">SWEDEN</option><option value="CH">SWITZERLAND</option><option value="SY">SYRIA</option><option value="TW">TAIWAN</option><option value="TJ">TAJIKISTAN</option><option value="TZ">TANZANIA</option><option value="TH">THAILAND</option><option value="TT">TRINIDAD AND TOBAGO</option><option value="TN">TUNISIA</option><option value="TR">TURKEY</option><option value="UG">UGANDA</option><option value="UA">UKRAINE</option><option value="AE">UNITED ARAB EMIRATES</option><option value="GB">UNITED KINGDOM</option><option value="US">UNITED STATES</option><option value="UY">URUGUAY</option><option value="UZ">UZBEKISTAN</option><option value="VU">VANUATU</option><option value="VE">VENEZUELA</option><option value="VN">VIETNAM</option><option value="VI">VIRGIN ISLANDS (USA)</option><option value="YE">YEMEN</option><option value="ZM">ZAMBIA</option><option value="ZW">ZIMBABWE</option></select><span class="select-value">&nbsp;</span><span class="select-arrow"></span><span class="drop-down"></span></span>
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>City</label>
                            <div class="full-width button-height">
                                <span class="select replacement select-styled-list tracked" tabindex="0"><select id="selCity" class="withClearFunctions" tabindex="-1">
                                    <option selected="selected" value="-">Select Any City</option>
                                </select><span class="select-value">Select Any City</span><span class="select-arrow"></span><span class="drop-down"></span></span>
                            </div>
                        </div>
                        <div class="two-columns twelve-columns-mobile formBTn">
                            <br>
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="reset()">Reset</button>

                        </div>
                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <br>
                            <span class="icon-pdf right" onclick="ExportAgentDetailsToExcel('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportAgentDetailsToExcel('excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>--%>
            <%--<div class="columns">
                <div class="four-columns twelve-columns-mobile">
                    <label>Location Name:</label>

                    <input type="text" id="txt_Location" class="input full-width[required]" value="">
                </div>
                <div class="three-columns twelve-columns-mobile SelBox">
                    <label>Country:</label>

                    <div class="full-width button-height" id="DivCountry">
                        <select name="select90" id="selCountry" class="select country">
                            <option selected="selected" value="-">Select Any Country</option>
                        </select>
                    </div>
                </div>
                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                    <label>City:</label>

                    <div class="full-width button-height" id="City">
                        <select name="select90" id="selCity" class="select City">
                            <option selected="selected" value="-">Select Any City</option>
                        </select>
                    </div>

                </div>
                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                    <br />
                    <button type="button" id="btn_Save" style="margin-left: 10px" class="button glossy anthracite-gradient" onclick="SaveLocation()">Save</button>
                    <button type="button" id="btn_Update" style="margin-left: 10px; display: none" class="button glossy anthracite-gradient" onclick="UpdateLocation()">Update</button>
                </div>

            </div>--%>
            <div class="columns">
                <div class="four-columns twelve-columns-mobile">
                    <label>Name<span class="red">*</span></label>
                    <input type="text" id="txt_Location" class="input full-width" value="">
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <label>Country</label>
                    <div class="full-width button-height" id="DivCountry">
                        <select name="select90" id="selCountry" class="select country full-width">
                            <option selected="selected" value="-">Select Any Country</option>
                        </select>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <label>City</label>
                    <div class="full-width button-height" id="City">
                        <select name="select90" id="selCity" class="select City full-width">
                            <option selected="selected" value="-">Select Any City</option>
                        </select>
                    </div>
                </div>
                <div class="two-columns  twelve-columns-mobile Savebtn">
                    <br />
                    <button type="button" id="btn_Save" class="button anthracite-gradient buttonmrg full-width" onclick="SaveLocation()">Save</button>
                    <button type="button" id="btn_Update" style="display: none" class="button anthracite-gradient buttonmrg full-width" onclick="UpdateLocation()">Update</button>
                </div>
            </div>

            <div class="respTable">
                <table class="table responsive-table" id="tbl_GetLocation">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center"><span>Sr.No</span></th>
                            <th scope="col" class="align-center"><span>Location</span></th>
                            <th scope="col" class="align-center"><span>City</span></th>
                            <th scope="col" class="align-center"><span>Country</span></th>
                            <th scope="col" class="align-center"><span>Actions</span></th>
                            <%--<th scope="col"><span class="align-center">Update</span></th>
                            <th scope="col"><span class="align-center">Delete</span></th>--%>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <%--<div class="table-footer ">
                <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
                <br />
                <br />
            </div>--%>
        </div>

    </section>




    <!-- End main content -->

</asp:Content>
