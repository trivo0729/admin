﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddQuotation.aspx.cs" Inherits="CutAdmin.AddQuotation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddQuotation.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Quotation</h1>
        </hgroup>
        <div class="with-padding">
            <form action="#" class="addQuotation">
                <%--<h4>Company Information: </h4>--%>
                <hr>
                <div class="columns">
                    <div class="two-columns twelve-columns-mobile six-columns-tablet">
                        <label>Date:</label><br />
                        <div class="input full-width">
                            <input name="datepicker" id="txt_QuotationDate" placeholder="Date" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                    <div class="five-columns  six-columns-mobile six-columns-tablet">
                        <label>Quote to: </label>
                        <br />
                        <div class="full-width button-height" id="DivAgency">
                            <select id="selAgency" name="validation-select" class="select">
                               <%-- <option value="-">-Select Any City-</option>
                                <option value="IN001">AGARTALA</option>
                                <option value="IN003">AHMEDABAD</option>
                                <option value="IN002">AGRA</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="five-columns twelve-columns-mobile six-columns-tablet">
                        <label>Company name<span class="red">*</span>:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Companyname" placeholder="Company name" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Email<span class="red">*</span>:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Email" placeholder="Email" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet mobilephone">
                        <label>Phone.:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Phone" placeholder="Phone" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet mobilephone">
                        <label>Mobile.<span class="red">*</span>:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Mobile" placeholder="Mobile" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Country: </label>
                        <br />
                        <div class="full-width button-height" id="DivCountry">
                            <select id="selCountry"  name="validation-select" class="select OfferType">
                               <%-- <option selected="selected" value="-">Select Any Country</option>
                                <option value="AD">INDIA</option>
                                <option value="AE">UNITED ARAB EMIRATES</option>
                                <option value="AF">AFGHANISTAN</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>State: </label>
                        <br />
                        <div class="full-width button-height" id="DivState">
                            <select id="selState" name="validation-select" class="select OfferType">
                                <%--<option selected="selected" value="-">Select Any Country</option>
                                <option value="AD">INDIA</option>
                                <option value="AE">UNITED ARAB EMIRATES</option>
                                <option value="AF">AFGHANISTAN</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>City: </label>
                        <br />
                        <div class="full-width button-height" id="DivCity">
                            <select id="selCity" name="validation-select" class="select OfferType">
                                <%--<option value="-">-Select Any City-</option>
                                <option value="IN001">AGARTALA</option>
                                <option value="IN003">AHMEDABAD</option>
                                <option value="IN002">AGRA</option>--%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="six-columns twelve-columns-mobile six-columns-tablet bold">
                        <label>Contact person<span class="red">*</span>:</label><div class="input full-width">
                            <input name="prompt-value" id="txt_Contactperson" placeholder="Contact person name" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>

                    <div class="six-columns twelve-columns-mobile six-columns-tablet">
                        <label>Address:</label><div class="input full-width">
                            <input name="prompt-value" id="txt_Address" placeholder="Enter Address" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>

                </div>
                
                <div class="columns">
                    <div class="six-columns twelve-columns-mobile six-columns-tablet">
                        <label>Lead guest name:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Leadguestname" value="" class="input-unstyled full-width" placeholder="Full Name " type="text">
                        </div>
                    </div>
                   <div class="two-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Adults<span class="red">*</span>: </label>
                        <br />
                        <div class="full-width button-height" id="DivAdults">
                            <select id="selAdults" name="validation-select" class="select">
                                <option value="-">-Select no of Adults-</option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Child<span class="red">*</span>: </label>
                        <br />
                        <div class="full-width button-height" id="DivChild">
                            <select id="selChild" name="validation-select" class="select">
                                <option value="-">-Select no of Child-</option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                    </div>
                    <div class="two-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Infant<span class="red">*</span>: </label>
                        <br />
                        <div class="full-width button-height" id="DivInfant">
                            <select id="selInfant" name="validation-select" class="select">
                                <option value="-">-Select no of Infant-</option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>First Name:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="Agency-value" value="" class="input-unstyled full-width" placeholder="First Name" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Last Name:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="Agency-value" value="" class="input-unstyled full-width" placeholder="Last Name" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Designation:</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="Agency-value" value="" class="input-unstyled full-width" placeholder="Designation" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet bold">
                        <label>Company Name:</label><div class="input full-width">
                            <input name="prompt-value" id="Agency-value" value="" class="input-unstyled full-width" placeholder="Agency Name" type="text">
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="six-columns twelve-columns-mobile six-columns-tablet bold">
                        <label>Tour Start<span class="red">*</span>:</label><div class="input full-width">
                            <input name="promptdatepicker" id="txt_TourStart" placeholder="Tour Start" value="" class="input-unstyled full-width" type="text">
                        </div>
                    </div>

                    <div class="six-columns twelve-columns-mobile six-columns-tablet">
                        <label>Tour End<span class="red">*</span>:</label><div class="input full-width">
                            <input name="datepicker" id="txt_TourEnd" placeholder="Tour End" value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                   
                </div>
                <div class="columns">
                    <div class="twelve-columns twelve-columns-tablet">
				        <h3 class="thin">Quotation Details :</h3>
					   <%-- <textarea id="noise" name="noise">This is a rich-text editor: select <b>some text</b> and click buttons!</textarea>--%>
                        <textarea name="autoexpanding" id="noise" class="input full-width autoexpanding"></textarea>
				    </div>
                </div>
                 <div class="columns">
                     <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Quoted by<span class="red">*</span>: </label>
                        <br />
                        <div class="full-width button-height" id="DivQuotedby">
                            <select id="selQuotedby" name="validation-select" class="select">
                               <%-- <option value="-">-Select Any City-</option>
                                <option value="IN001">AGARTALA</option>
                                <option value="IN003">AHMEDABAD</option>
                                <option value="IN002">AGRA</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                        <label>Current status<span class="red">*</span>: </label>
                        <br />
                        <div class="full-width button-height" id="DivCurrentstatus">
                            <select id="selCurrentstatus" name="validation-select" class="select">
                               <option selected="selected" value="-">Select Status</option>
                                <option value="Posted">Post</option>
                                <option value="NotPosted">Not Post</option>
                                <option value="Approved">Approved</option>
                                <option value="Rejected">Rejected</option>
                            </select>
                        </div>
                    </div>
                    <div class="four-columns  twelve-columns-mobile six-columns-tablet">
                       <label>Note / Remark:</label><div class="input full-width">
                            <input name="prompt-value" id="txt_NoteorRemark" placeholder="Note / Remark " value="" class="input-unstyled full-width"  type="text">
                        </div>
                    </div>
                </div>
                <div class="columns">
                </div>
                <p class="text-right" style="text-align:right;">
                    <button type="button" id="btn_Quotation" class="button anthracite-gradient UpdateMarkup" onclick="AddQuotation();" value="Add Quotation">Add Quotation</button>
                    <button type="button" onclick="window.location.href = 'QuotationDetails.aspx'" class="button anthracite-gradient">Back</button>
                </p>
            </form>



        </div>
    </section>
    <!-- End main content -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>


    <script src="js/libs/ckeditor/ckeditor.js"></script>
    <script>

		// Call template init (optional, but faster if called manually)
		$.template.init();

		// CKEditor
		CKEDITOR.replace('noise', {
			height: 300
		});

	</script>
</asp:Content>
