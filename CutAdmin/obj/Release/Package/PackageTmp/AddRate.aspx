﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddRate.aspx.cs" Inherits="CutAdmin.AddSeasonRate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <script src="Scripts/AddRate.js?v=1.6"></script>--%>
    <script src="Scripts/AddRate2.js?v=2.5"></script>
    <%--<script src="Scripts/EditRate.js?v=1.0"></script>--%>
    <script src="Scripts/MastelHotelOffer.js?v=1.0"></script>
    <script src="Scripts/ChargesMapping.js"></script>
    <script src="Scripts/AddOnsCharges.js?v=2.3"></script>
    <script>
        $(function () {
            if (location.href.indexOf('?') != -1) {
                sHotelID = GetQueryStringParams("HotelCode");
            }
            GetTaxDetails();
        })
    </script>
      <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <script src="Scripts/moments.js"></script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
        /*ul li {
            list-style: none;
            cursor:pointer;
            padding-top:10px
        }*/
        .tab-active {
           /*height: auto;*/
        }

        .deleteMe {
            float: right;
            background: #bb3131;
            color: white;
        }
    </style>
     

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        
        <hgroup id="main-title" class="thin">
			<h1>Add Rate Details</h1>
            <h2><b class="grey"><strong>HOTEL</strong>: <strong><label id="lblHotel"></label></strong></b></h2>
            <hr/>			
		</hgroup>


                <div class="with-padding">
                    <div class="columns"> 
                    <div class="three-columns twelve-columns-mobile">                        
                            <label class="input-info">Rooms</label>
                            <select id="sel_Supplier" name="validation-select" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width" multiple="multiple">
                                <option value="All" selected="selected">All Rooms</option>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile" id="drp_Nationality">
                            <label >Nationality<span class="red">*</span></label>
                                <select id="sel_Nationality" onchange="CheckNationality(this.value);" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width" multiple>
                                    <option value="All">All&nbsp;/&nbsp;Unselect</option>     
                                </select>                               
                        </div>

                        <div class="three-columns twelve-columns-mobile SelBox" id="Currency">
                            <label >Currency<span class="red">*</span></label>
                            <select id="sel_CurrencyCode" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile SelBox">
                            <label >Meal Plan<span class="red">*</span></label>
                            <select id="sel_MealPlan" name="validation-select" class="select full-width">
                                <option value="-" selected="selected" disabled>Please select</option>
                            </select>
                        </div>
                    </div>
                    
                   <h4>Rate Validity</h4>
                     <hr/>

                    

                    <label id="clickRV" style="display:none" none">1</label>
                    <div id="idRateValidity" class="cRateValidity">
                        <div class="columns columnsRV" id="divRV0">
                            <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                <label class="input-info">Valid From<span class="red">*</span></label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtFromRV0" class="input-unstyled cRateValidFrom" style="cursor: pointer" value="">
                                </span>

                            </div>
                            <div class="three-columns ten-columns-mobile four-columns-tablet bold">
                                <label class="input-info">Valid To<span class="red">*</span></label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input class="input-unstyled cRateValidTo" type="text" id="txtToRV0" style="cursor: pointer" value="" />
                                    <%--<input type="text" id="txtToRV0" class="input-unstyled cRateValidTo" value="">--%>
                                </span>
                            </div>
                          
                            <div class="three-column plusminus">
                                <a id="lnkAddRV0" onclick="AddRateValidity(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>
                            </div>
                        </div>
                         <br />
                    </div>

                    <div class="columns" style="display:none"none;">
                          <div class="twelve-columns cRVDays" id="iRVDays0" >
                                <small class="input-info">Days:</small>
                                <label for="chkAllDays0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkAllDays0" value="All" onchange="CheckRVAllDays(0);">All</label>
                                <label for="chkSun0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSun0" value="Sun" onchange="CheckRVSingle(0);">Sun</label>
                                <label for="chkMon0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkMon0" value="Mon" onchange="CheckRVSingle(0);">Mon</label>
                                <label for="chkTue0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkTue0" value="Tue" onchange="CheckRVSingle(0);">Tue</label>
                                <label for="chkWed0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkWed0" value="Wed" onchange="CheckRVSingle(0);">Wed</label>
                                <label for="chkThu0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkThu0" value="Thu" onchange="CheckRVSingle(0);">Thu</label>
                                <label for="chkFri0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkFri0" value="Fri" onchange="CheckRVSingle(0);">Fri</label>
                                <label for="chkSat0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSat0" value="Sat" onchange="CheckRVSingle(0);">Sat</label>

                            </div>
                        <br />
                    </div>
               

                    <h4><input type="checkbox" id="chkSpecialDate" onchange="CheckSpecialDate()" /><label for="chkSpecialDate" class="label">SPECIAL DATES</label></h4>
                         <hr/>
                    <%-- special Dates--%>

                    <label id="clickSD" style="display:none" none">1</label>
                    <div id="idSpecialDate" class="cSpecialDate" style="display:none" none">

                        <div class="columns" id="divSD0">

                            <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                <label>Valid From</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtFromSD0" class="input-unstyled ctxtfromSD" placeholder="Select Date">
                                </span>
                            </div>
                            <div class="three-columns ten-columns-mobile four-columns-tablet bold">
                                <label >Valid To</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtToSD0" class="input-unstyled ctxttoSD" placeholder="Select Date" >
                                </span>
                            </div>
                          
                            <div class="three-column plusminus">
                                
                                <a id="lnkAddSD0" onclick="AddSpecialDates(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="columns" id="dispSD" style="display:none">
                          <div class="twelve-columns cSDDays" id="iSDDays0">
                                <small class="input-info">Days:</small>
                                <label for="chkAllSD0" class="label">
                                    <input type="checkbox" id="chkAllSD0" value="All" class="cDaySD" onclick="CheckSDAllDays(0);">All</label>
                                <label for="chkSunSD0" class="label">
                                    <input type="checkbox" id="chkSunSD0" value="Sun" class="cDaySD" onclick="CheckSDSingle(0);">Sun</label>
                                <label for="chkMonSD0" class="label">
                                    <input type="checkbox" id="chkMonSD0" value="Mon" class="cDaySD" onclick="CheckSDSingle(0);">Mon</label>
                                <label for="chkTueSD0" class="label">
                                    <input type="checkbox" id="chkTueSD0" value="Tue" class="cDaySD" onclick="CheckSDSingle(0);">Tue</label>
                                <label for="chkWedSD0" class="label">
                                    <input type="checkbox" id="chkWedSD0" value="Wed" class="cDaySD" onclick="CheckSDSingle(0);">Wed</label>
                                <label for="chkThuSD0" class="label">
                                    <input type="checkbox" id="chkThuSD0" value="Thu" class="cDaySD" onclick="CheckSDSingle(0);">Thu</label>
                                <label for="chkFriSD0" class="label">
                                    <input type="checkbox" id="chkFriSD0" value="Fri" class="cDaySD" onclick="CheckSDSingle(0);">Fri</label>
                                <label for="chkSatSD0" class="label">
                                    <input type="checkbox" id="chkSatSD0" value="Sat" class="cDaySD" onclick="CheckSDSingle(0);">Sat</label>

                            </div>
                    </div>
                    <%--End special Dates--%>

                    <hr/>

                          <p><a href="#" class="button anthracite-gradient" style="cursor:pointer" onClick="AddDates();">Add Dates</a>

                     <%--<div class="columns">
                          <div class="twelve-columns">
                               <a onclick="AddDates();" style="cursor:pointer" class="button float-left" >Add Dates</a>
                          </div>                         
                     </div>--%>


                    <div style="display:none" id="DivDates">
                        <h4 >Rooms</h4>
                    <br />
                    <div class="side-tabs same-height">
						<ul  class="tabs" id="TabRooms" >
						</ul>
						<div class="tabs-content NestesDiv" id="TabContent">
						</div>
					</div>

                    <div class="standard-tabs margin-bottom" id="hdndiv1" style="display:none"></div>
                     <br />
                     <br />
                     <br />

                    <h6 class="underline margin-top" style="display:none" id="spldtheading">For Special Dates</h6>
<br />
                    <div class="side-tabs same-height">
						<ul class="tabs" id="TabRoomsSpl">
						</ul>
						<div class="tabs-content NestesDivSD" id="TabContentSpl">
						</div>
					</div>

                    <div class="standard-tabs margin-bottom" id="" style="display:none"></div>
                     <br />
                     <br />
                     <br />
                    </div>
                    

                
                <br />
                  
                       <h4 class="txt-detail">Tax Details</h4>
                     <hr/> 
                     <span class="labeltxt">(If Tax Already Included in above Rates)</span>
                      <label>Tax Included:</label>
                              <span class="button-group">
                                  <label for="chkTaxIncluded1" class="button grey-active">
                                      <input type="radio" checked name="button-radio chkTaxIncluded" id="chkTaxIncluded1" value="YES"  onclick="checkTaxApllied(this.value);" >
                                      YES
                                  </label>
                                  <label for="chkTaxIncluded2" class="button grey-active">
                                      <input type="radio"  name="button-radio chkTaxIncluded" id="chkTaxIncluded2" value="NO" onclick="checkTaxApllied(this.value);">
                                      NO
                                  </label>
                                 
                              </span>
                              
                               <input id="txtIncluded" style="display:none"none;"/>
                         
                      <div class="" id="div_TaxDetails">
                          
                     
                          
                       </div>
                      <div class="" id="div_UpdateTaxDetails" style="display:none">
                          
                     
                          
                       </div>
                      <br />
                       <br />
                     <h4 class="txt-detail">Tariff Policy</h4>
 <hr/>
                       <div class="columns">
                        
                          <div class="three-columns twelve-columns-mobile addinput ClassDynamic">
                              <label>Inclusion:</label>
                               <ul id="myUL" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV" class="header">
                                  <input type="text" id="myInput" placeholder="Title...">
                                  <span onclick="newElement()" class="addBtn">Add</span>
                              </div>

                             
                          </div>
                          <div class="three-columns twelve-columns-mobile ClassDynamic">
                              <label>Exclusion:</label>
                               <ul id="myUL1" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV1" class="header">
                                  <input type="text" id="myInput1" placeholder="Title...">
                                  <span onclick="newElement1()" class="addBtn">Add</span>
                              </div>
                          </div>
                          </div>
                      <div class="columns">
                           <div class="two-columns twelve-columns-mobile">
                              <label>Min Stay:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtminStay" class="input full-width">
                          </div>
                           <div class="two-columns twelve-columns-mobile">
                              <label>Max Stay:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtmaxStay" class="input full-width">
                          </div>
                          <div class="two-columns twelve-columns-mobile">
                              <label>Min Room:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtminRoom" class="input full-width">
                          </div>
                            <div class="two-columns twelve-columns-mobile SelBox">
                              <label>Upload Contract:</label>
                              <input type="file" id="uploadContract" value="" class="file">
                          </div>
                      </div>

         <p class="text-textright"><button type="button" onclick="SaveRates()" class="button anthracite-gradient ">Submit</button></p>   
                 
                <%--<div class="button-height with-mid-padding silver-gradient no-margin-top">
                    <a  class="button blue-gradient float-right" onclick="SaveRates()" style="cursor:pointer">Submit</a>
                    <br />
                </div>--%>
 </div> 
           

    </section>
</asp:Content>
