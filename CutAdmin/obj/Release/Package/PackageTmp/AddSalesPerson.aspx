﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddSalesPerson.aspx.cs" Inherits="CutAdmin.AddSalesPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/AddSalesPerson.js?v=1.8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Sales Staff Registration Form</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <form action="#" class="addagent">
                <h4>Personal details</h4>
                <hr>
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>First Name<span style="color: red;">*</span></label>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_FirstName" value="" class="input-unstyled full-width" placeholder="First Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Last Name<span style="color: red;">*</span> </label>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_LastName" value="" class="input-unstyled full-width" placeholder="Last Name" type="text">
                        </div>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Gender </label>
                        <div class="full-width button-height" id="Gender">
                            <select id="selGender" class="select UsrTyp">
                                <option selected="selected" value="0">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Mobile<span style="color: red;">*</span>  </label>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Mobile" value="" class="input-unstyled full-width txtmob" placeholder="Mobile" type="text">
                        </div>
                    </div>
                    <div class="two-columns twelve-columns-mobile six-columns-tablet">
                        <label style="display: none;">Mobile  </label>
                        <br />
                        <i onclick="AddNumber()" aria-hidden="true">
                            <label for="pseudo-input-2" class="button anthracite-gradient"><span class="icon-plus"></span></label>
                        </i>
                        <%--<i class="fa fa-plus"><input class="button anthracite-gradient UpdateMarkup" type="button" title="Add More No" onclick="AddNumber()"></i>--%>
                        <%--  <i class="fa fa-plus"><input type="button" class="button anthracite-gradient UpdateMarkup" title="Add More No" onclick="AddNumber()" /></i>--%>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Email<span style="color: red;">*</span> </label>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Email" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Date Of Birth<span style="color: red;">*</span> </label>
                        <div class="input full-width">
                            <input name="datepicker" id="txt_DOB" value="" class="input-unstyled full-width" placeholder="Date Of Birth" type="text">
                        </div>
                    </div>
                </div>
                <div id="moreNo" style="display: none">
                </div>
                <div class="columns">
                    <div class="twelve-columns twelve-columns-mobile six-columns-tablet">
                        <label>Residential Address<span style="color: red;">*</span></label>

                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Address" value="" class="input-unstyled full-width" placeholder="Address" type="text">
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet" id="DivCountry">
                        <label>Country<span style="color: red;">*</span> </label>

                        <br>
                        <div class="full-width button-height">
                            <select id="selCountry" class="select OfferType" onchange="GetStateDiv()">
                                <option selected="selected" value="-">Select Any Country</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet" id="DivState" style="display: none">
                        <label>State<span style="color: red;">*</span> </label>

                        <br>
                        <div class="full-width button-height" id="State">
                            <select id="selState" class="select OfferType">
                                <%--<option selected="selected" value="-">Select Any Country</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns  twelve-columns-mobile six-columns-tablet" id="DivCity">
                        <label>City<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height">
                            <select id="selCity" class="select OfferType">
                                <option value="-">-Select Any City-</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Pin Code  </label>
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_PinCode" value="" class="input-unstyled full-width" placeholder="Pin Code" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Sales Territory<span style="color: red;">*</span> </label>
                        <div class="full-width button-height" id="Territory">
                            <select id="selTerritory" onchange="GetTerritory()" class="select">
                                <option selected="selected" value="">Select Territory</option>
                                <option value="Country">Country</option>
                                <option value="State">State</option>
                                <option value="City">City</option>
                                <option value="Agency">Agency</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="columns">

                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="TeriDiv" style="display: none">
                        <label>Country<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height" id="TerCountry">
                            <select id="select_TerCountry" multiple="multiple" class=" full-width Country">
                                <%-- <option value="-">-Select Any City-</option>--%>
                            </select>
                        </div>
                    </div>

                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="TeriDivState" style="display: none">
                        <label>State<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height" id="TerState">
                            <select id="select_TerState" multiple="multiple" class="full-width state">
                                <%--<option value="-">-Select Any City-</option>--%>
                            </select>
                        </div>
                    </div>

                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="TeriDivCityCountry" style="display: none">
                        <label>Country<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height" id="TerCityCountry">
                            <select id="selTerCityCountry" onchange="GetCityCountry()" class="full-width Country">
                                <%--<option value="-">-Select Any City-</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="TeriDivCity" style="display: none">
                        <label>City<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height" id="TerCity">
                            <select id="select_TerCity" multiple="multiple" class="full-width Country">
                                <%--<option value="-">-Select Any City-</option>--%>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="TeriDivAgency" style="display: none">
                        <label>Agency<span style="color: red;">*</span> </label>
                        <br>
                        <div class="full-width button-height" id="TerAgency">
                            <select id="select_TerAgency" multiple="multiple" class="full-width Country">
                                <%--<option value="-">-Select Any City-</option>--%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div style="float: right" class="two-columns  twelve-columns-mobile four-columns-tablet">
                        <%--<button type="button" class="button anthracite-gradient UpdateMarkup">Register</button>--%>
                        <input id="btn_RegiterSales" type="button" value="Register" class="button anthracite-gradient UpdateMarkup" onclick="ValidateLogin();" title="Submit Details" />
                        <a class="button" href="SalesPersonDetails.aspx">Back</a>
                    </div>
                </div>

            </form>
        </div>
    </section>
</asp:Content>
