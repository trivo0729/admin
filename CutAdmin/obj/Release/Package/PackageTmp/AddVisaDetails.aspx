﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddVisaDetails.aspx.cs" Inherits="CutAdmin.AddVisaDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="scripts/AddVisaDetails.js"></script>
    <script type="text/javascript" src="scripts/EdnrdLoginDetails.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();
            //GetGroups();
            $('[data-toggle="popover"]').popover();

        });
        $(function () {
            var oldValue;
            var unsaved = false;
            $(":input").change(function () {
                unsaved = true;
            });
            $('input[type="text"]').keyup(function () {
                if (this.id != "txt_Username")
                {
                    // = this.value()
                    if (/^[A-Z0 -9]+$/i.test(this.value)) {

                    }
                    else {
                        this.value = '';
                    }
                }
                
            });

        });
        function AddVisaNo(id) {
            if (id == "VisaNo_Cut")
            {
                $("#txt_Application").css("display", "none")
                $("#tbl_VisaNo").css("display", "")
            }
            else

            {
                $("#txt_Application").css("display", "")
                $("#tbl_VisaNo").css("display", "none")
            }
            
        }
        function InsertVisaNo() {
            $("#txt_Application").val($("#visa_no1").val() + $("#visa_no2").val() + $("#visa_no3").val());
            $("#VisaNo_Model").modal("hide")
        }
    </script>
    <style type="text/css">
        div.scrollingDiv {
            width: auto;
            height: 600PX;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            debugger;
            $("#txt_Agent").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "VisaHandler.asmx/GetAgentList",
                        data: "{'name':'" + document.getElementById('txt_Agent').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    GetAgentDetails(ui.item.id)
                    $('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txtPassport").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "VisaHandler.asmx/GetPassportNo",
                        data: "{'PassportNo':'" + document.getElementById('txtPassport').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    debugger;
                    GetPassportDetails(ui.item.id)
                    //$('#hdnDCode').val(ui.item.id);
                }
            }); 
        });
        function SelectType(Id) {
            if (Id == "rdb_Cut")
            {
                $("#Application").css('display', 'none')
                $("#lblNopix").css('display', 'none')
                $('#div_Supplier').css('display', 'none')
            }
            else
            {
                $("#Application").css('display', '')
                $("#lblNopix").css('display', '')
                $('#div_Supplier').css('display', '')
            }

        }
        function GetPassportDetails(sid) {
            $.ajax({
                type: "POST",
                url: "VisaHandler.asmx/GetPassportDetails",
                data: '{"Sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retcode == 1) {
                        var arrDetails = result.Visa;
                        if (result.Valid == false)
                        {
                            // Passport Details //
                            $("#txtIssuing").val(arrDetails[0].IssuingGovernment);
                            $("#txtDoi").val(arrDetails[0].IssuingDate);
                            $("#txtED").val(arrDetails[0].ExpDate);
                            $("#txtAddress1").val(arrDetails[0].AddressLine1);
                            $("#txtAddress2").val(arrDetails[0].AddressLine2);
                            $("#txtCity").val(arrDetails[0].City);
                            $("#selCountry").val(arrDetails[0].Country);
                            $("#txtTelephone").val(arrDetails[0].Telephone);

                            // Passenger Details //
                            $('#txtFirst').val(arrDetails[0].FirstName);
                            $('#txtMiddle').val(arrDetails[0].MiddleName);
                            $('#txtLast').val(arrDetails[0].LastName);
                            $('#txtFather').val(arrDetails[0].FatherName);
                            $('#txtMother').val(arrDetails[0].MotherName);
                            $('#txtHusband').val(arrDetails[0].HusbandMame)
                            $('#selLanguage').val(arrDetails[0].Language);
                            $('#selGender').val(arrDetails[0].Gender);
                            $('#selMarital').val(arrDetails[0].MaritalStatus);
                            $('#selNationality').val(arrDetails[0].PresentNationality);
                            $('#txtDob').val(arrDetails[0].Birth);
                            $('#selCountry1').val(arrDetails[0].BirthCountry);
                            $('#selDepartment').val();
                            $('#txt_BirthPlace').val(arrDetails[0].BirthPlace);
                            $('#selReligion').val(arrDetails[0].Religion);
                            $("#selProfession option:selected").text(arrDetails[0].Profession)
                        }
                        else
                        {
                            Ok("Visa for Passport Id " + $("#txtPassport").val() + " is already Approved,Please click Ok to See visa Copy..", "VisaCopy", [arrDetails[0].Vcode])
                        }
                        


                        
                    }
                },
                error: function () {
                }
            });
        }
        function VisaCopy(Code) {
            $('#Ticket').empty();
            var tRow = '';
            tRow = '<iframe src="../VisaImages/' + Code + '_6.pdf" width="80%" height="500px"  />';
            $('#Ticket').append(tRow)
            $("#Modal96Hours").modal("show")
            Cancel()
        }
        var Currency;
        function GetCurrency(CurrencyImage) {
            Currency = CurrencyImage
            $(".Currency").addClass(CurrencyImage)
                
        }
         </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="container mt25 offset-0">

<div class="fblueline">
                    
                    <a style="color: white" href="#" >Call Center</a>
    <span class="farrow"></span>
                    <a style="color: white" data-toggle="tooltip" data-placement="bottom"  title="Add Visa"><b> Visa Application</b></a><br />
                </div>
            <!-- CONTENT -->
            <div class="col-md-12 pagecontainer2 offset-0">
                <div class="col-md-12 offset-0">
                    <!-- Tab panes from left menu -->
                    <div class="tab-content5">

                        <!-- TAB 1 -->
                        <div class="tab-pane padding40 active margleft15" id="profile">

                            <!-- Admin top -->

                            <div class="col-md-4">
                            </div>

                            <!-- End of Admin top -->

                            <div class="clearfix"></div>

                               <p class="lato size30 slim">
                      Visa Application Form 
                    </p>
                            <div class="line2"></div>

                            <div class="col-md-12 offset-0">
                                <div class="row">
                            <div class="col-md-2" style="padding-top: 20px">
                                <input type="radio" id="rdb_Cut" value="Normal" checked="checked" name="type" onclick="SelectType(this.id)">
                                Online Visa
                               
                            </div>

                            <div class="col-md-2" style="padding-top: 20px">
                                <input type="radio" id="rdb_Other" value="Urgent" name="type" onclick="SelectType(this.id)">
                                Apply Offline
                               
                            </div>
                            
                            <div class="col-md-3" style="padding-top: 5px; display: none" id="lblNopix">Supplier*:
                                <select id="selSupplier" class="form-control" onchange="Notification()">
                                    </select>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_selSupplier">
                                            <b>* This field is required</b></label>
                            </div>
                                    <div class="col-md-1" style="padding-top: 30px;display: none" id="div_Supplier">
                                    <span class="orange glyphicon glyphicon-edit size20" data-toggle="tooltip" title="New Supplier" style="cursor:pointer"  onclick="OpenDilogBox()" ></span>
                                    </div>
                            <div class="col-md-4" style="padding-top: 5px; display: none" id="Application">
                                 <input type="radio" id="AppNo_Cut" value="" checked="checked" name="AppNo" onclick="AddVisaNo(this.id)">Application No
                                <input type="radio" id="VisaNo_Cut" value=""  name="AppNo" onclick="AddVisaNo(this.id)"> Visa No
                                <input type="text" class=" form-control" id="txt_Application"/>
                                <table id="tbl_VisaNo" style="display:none">
                                    <tr>
                                        <td style="width:25%"><select name="visa_no1" id="visa_no1" class="form-control">
<option value="212">212</option>
<option value="216">216</option>
<option value="201">201</option>
<option value="204">204</option>
<option value="206">206</option>
<option value="208">208</option>
<option value="210">210</option>
<option value="214">214</option>
</select></td>
                                         <td style="width:25%"><select name="visa_no2" id="visa_no2" class="form-control">
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>

        </select></td>
                                         <td><input name="visa_no3" id="visa_no3" type="text" size="24" maxlength="8" class="form-control"></td>
                                    </tr>
                                </table>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Application">
                                            <b>* This field is required</b></label>

                            </div>
</div>
                                
                                <%--<div class="row">
                        <div class="col-md-2" style="margin-top: 1%">
                            <span class="text-left">Search For :</span>
                        </div>
                        <div class="col-md-3">
                            <div class="radio">
                                <input id="rdbAgent" type="radio" name="searchfor" value="Agent" style="margin-top: 1.5%"><span class="text-left">Agent</span>
                            </div>
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input id="txtAgent" type="text" class="form-control ui-autocomplete-input" placeholder="Agent Name" >
                            <input type="hidden" id="hdnAgCode">
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_AgentName">
                                <b>* This field is required</b></label>
                        </div>
                        <div class="col-md-3">
                            <div class="radio">
                                <input id="rdbCorporate" type="radio" name="searchfor" value="Corporate" style="margin-top: 1.5%"><span class="text-left">Corporate</span>
                            </div>
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input id="txtCorporate" type="text" class="form-control ui-autocomplete-input" placeholder="Corporate Name" disabled="disabled" autocomplete="off">
                            <input type="hidden" id="hdnCorpCode">
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_CorporateName">
                                <b>* This field is required</b></label>
                        </div>
                        <div class="col-md-3">
                            <div class="radio">
                                <input id="rdbFranchisee" type="radio" name="searchfor" value="Franchisee" style="margin-top: 1.5%"><span class="text-left">Franchisee</span>
                            </div>
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input id="txtFranchisee" type="text" class="form-control ui-autocomplete-input" placeholder="Franchisee Name" disabled="disabled" autocomplete="off">
                            <input type="hidden" id="hdnFrchCode">
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_FranchiseeName">
                                <b>* This field is required</b></label>
                        </div>
                    </div>--%>

                                <div class="row">
                                    <div class="col-md-4">
                                        <br/>
                                        Agency <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                    <input type="text" id="txt_Agent" class="form-control" title="Select Agent"/>
                                    <input type="hidden" id="hdnDCode" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Agent">
                                            <b>* This field is required</b></label>
                                    </div>
                                   <%-- <div class="col-md-4">
                                        <br/>
                                        Available Credit <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                    <input type="text" id="txt_Credit" class="form-control" readonly="readonly"  placeholder=" Available Credit" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_Available">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br/>
                                        Unique Code <label style="color: red; margin-top: 3px;"  >
                                            <b>*</b></label>
                                        :
                                    <input type="text" id="txt_Unique" class="form-control"  readonly="readonly"  placeholder="Unique Code" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Unique">
                                            <b>* This field is required</b></label>
                                    </div>--%>
                                    <div class="col-md-4">
                                        <br/><br/>
                                        <span class="lato size14 grey" style="float: left;    margin-top: 2%; padding-bottom: 10px;">
                                            Agent Name:<span class="orange" id="spAgent"> - </span>,  <br />
                                             Agent Code:<span class="orange" id="spUniqueCode"> - </span>,
                                           
                                            Available limit:<span class="orange" id="spAvailableLimit">00.00</span>
                                            <input type="hidden" id="txt_Avail" value="0" />
          
            
            
        </span>
                                    </div>
                                    <div class="col-md-4">
                                        <br/><br/>
                                        <span class="lato size14 grey" style="float: left;    margin-top: 2%; padding-bottom: 10px;">
                                         
            
                                             Credit Limit:[<span class="orange" id="spCreditLimit">0.00/0.00</span>]
                                             <br />
                                        One Time Limit:[<span class="orange" id="spOneTimeCreditLimit">00.00</span>]
                                             </span>
                                    </div>
                                </div>

                                <div class="line2"></div>

                                 <%--<span class="size16 bold">Passport Details</span>--%>
                                              <p class="lato size30 slim">
                      Passport Details
                    </p>
                                <div class="line2"></div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <br />
                                        Passport No
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                       <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtPassport" placeholder="Passport Number" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassport">
                                            <b>* This field is required</b></label>

                                    </div>
                                    <div class="col-md-3">
                                        <br>
                                        Place of Issue (As per Passport)
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtIssuing" placeholder="Issuing Government" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtIssuing">
                                            <b>* This field is required</b></label>
                                    </div>

                                    <div class="col-md-3">
                                        <br>
                                        Date Of Issue
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                       <input type="text" id="txtDoi" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" title="Select Date" style="cursor: pointer" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDoi">
                                            <b>* This field is required</b></label>
                                    </div>

                                    <div class="col-md-3">
                                        <br />
                                        Expiry Date
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                         <input type="text" id="txtED" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" style="cursor: pointer" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtED">
                                            <b>* This field is required</b></label>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                <br>
                                <br>
                               <%-- <span class="size16 bold">Address Details (as Per Passport)</span>--%>
                               
                                <div class="line1"></div>

                                

                                <div class="row">
                                    
                                    <div class="col-md-4">
                                        <br/>
                                        Processing<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selProcessing" class="form-control" onchange="ServiceType()">
                                            <option value="-">-Select-</option>
                                            <option value="1" selected="selected">Normal</option>
                                            <option value="2">Urgent</option>
                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selProcessing">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br/>
                                        Visa Type<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                    <select id="selService" class="form-control" onchange="Notification()">
                                                <option value="-" selected="selected">--Salect Visa Type--</option>
                                                <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                                <option value="1">14 days Service VISA</option>
                                                <option value="2">30 days Tourist Single Entry</option>
                                                <option value="3">30 days Tourist Multiple Entry</option>
                                                <option value="4">90 days Tourist Single Entry</option>
                                                <option value="5">90 days Tourist Multiple Entry</option>
                                                <option value="7">90 Days Tourist Single Entry Convertible</option>
                                                
                                    </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selService">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br/>
                                       Nationality<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                    <select id="selVisaCountry" class="form-control" onchange="Notification()">
                                                <option value="-" selected="selected">--Salect Visa Country--</option>
                                                <option value="Indian">Indian</option>
                                                <option value="Pakistani">Pakistani</option>
                                                <option value="Sri Lankan">Sri Lankan</option>
                                                <option value="South African">South African</option>
                                                <option value="Mozambican">Mozambican</option>
                                                <option value="Malaysian">Malaysian</option>
                                                <option value="Indonesian">Indonesian</option>
                                                <option value="Philippines">Philippines</option>
                                      
                                                
                                    </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selVisaCountry">
                                            <b>* This field is required</b></label>
                                    </div>

                                </div>
                                <br />
                                <br />
                                <div class="row" style="display: none" id="Important">

                                    <div class="alert alert-warning">
                                        Important Note:
                                        <label id="Note">(96hrs) It is compulsory to attach valid confirm ticket showing you passenger exit within 96hrs..</label>
                                    </div>
                                </div>

                                <span class="size16 bold">Visa Charges</span>
                                 <span class="glyphicon glyphicon-edit" aria-hidden="true" id="spn_EnterManual" style="float:right;cursor:pointer"   data-toggle="tooltip" data-placement="left"  title="Enter Manually" onclick="EnterMaually()"></span>
                                <div class="line2"></div>

                                <div class="row">
                                    <div class="alert alert-info">
                                        <label id="lbl_Manual" style="display:none">false</label>
                                        <div align="center">
                                            <div class="col-md-1"><br /></div>
                                            <div class="col-md-2">
                                                <label>Visa Fee</label><br />
                                                <i id="Charges" class="Currency"></i>  <label style="color: black" class="lblManual" id="VisaFee">0.00</label>
                                                <input type="text" id="txt_VisaFee" class="form-control Manual" placeholder="0.00" style="display:none"  onkeyup="GetCharges()"/><br />

                                            </div>
                                            <div class="col-md-2">
                                                <label>Other Fee</label><br />
                                               <i class="Currency"></i>  <label style="color: black" class="lblManual" id="OtherFee">0.00</label>
                                                 <input type="text" id="txt_OtherFee" class="form-control Manual" placeholder="0.00" style="display:none" onkeyup="GetCharges()"/><br />

                                            </div>
                                            <div class="col-md-2">
                                                <label>Urgent Fee</label><br />
                                                 <i class="Currency"></i>  <label style="color: black" class="lblManual" id="UrgentFee">0.00</label>
                                                 <input type="text" id="txt_UrgentFee" class="form-control Manual" placeholder="0.00" style="display:none"  onkeyup="GetCharges()"/><br />

                                            </div>
                                            <div class="col-md-2">
                                                <label>Service Tax [%]</label><br />
                                                <i class="Currency"></i>  <label style="color: black" class="lblManual" id="ServiceTax">0.00</label>
                                                  <input type="text" id="txt_ServiceTax" class="form-control Manual" placeholder="0.00" style="display:none" onkeyup="GetCharges()"/><br />
                                            </div>
                                            <div class="col-md-2">
                                                <label>Total Amount</label> <br />
                                                 <i class="Currency"></i>  <label style="color: black" class="lblManual" id="TotalAmount">0.00</label>
                                                <input readonly="readonly" type="text" id="txt_TotalAmount" style="display:none" class="form-control Manual" placeholder="0.00" title="Visa Fee"/><br />

                                            </div>
                                             <div class="col-md-1"><br /></div>
                                            <br />
                                            <br /> <br />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <%--<span class="size16 bold">Passenger Details</span>--%>
                                        <p class="lato size30 slim">
                      Passenger Details
                    </p>
                                <div class="line2"></div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <br>
                                        First Name<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtFirst" placeholder="First Name" class="form-control" onchange="CheckValidation(this.id)" >
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtFirst">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Middle Name                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtMiddle" placeholder=" Middle Name" class="form-control">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtMiddle">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Last Name<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtLast" placeholder=" Last Name" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtLast">
                                            <b>* This field is required</b></label>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <br>
                                        Father's Name<label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtFather" placeholder="Father Name" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtFather">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Mother's Name
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtMother" placeholder=" Mother Name" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtMother">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Language Spoken
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selLanguage" class="form-control" onchange="CheckValidation(this.id)">

                                            <option value="-" selected="">--Select Language--</option>
                                            <option value="1">ARABIC</option>
                                            <option value="2">ENGLISH</option>
                                            <option value="3">FRENCH</option>
                                            <option value="4">HINDI</option>
                                            <option value="5">URDU</option>
                                            <option value="6">GERMAN</option>
                                            <option value="7">ITALIAN</option>
                                            <option value="8">SWAHILI</option>
                                            <option value="9">SPANISH</option>
                                            <option value="10">RUSSIAN</option>
                                            <option value="11">INDONESIAN</option>
                                            <option value="12">MALAYSIAN</option>
                                            <option value="13">MALAYAN</option>
                                            <option value="14">BALOCHI</option>
                                            <option value="15">TURKISH</option>
                                            <option value="16">JAPANESE</option>
                                            <option value="17">CHINESE</option>
                                            <option value="18">THAI</option>
                                            <option value="19">TAGALOG</option>
                                            <option value="20">FARSI</option>
                                            <option value="21">SINHALESE</option>
                                            <option value="22">BANGLA</option>
                                            <option value="23">TELUGU</option>
                                            <option value="98">Unknown</option>
                                            <option value="99">Others</option>

                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selLanguage">
                                            <b>* This field is required</b></label>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <br>
                                        Gender
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                       <select id="selGender" class="form-control" onchange="CheckGender();CheckValidation(this.id)">
                                           <option value="-" selected="selected">-Select Gender-</option>
                                           <option value="1">Male</option>
                                           <option value="2">Female</option>
                                       </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selGender">
                                            <b>* This field is required</b></label>

                                    </div>



                                    <div class="col-md-4">
                                        <br>
                                        Marital Status
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selMarital" class="form-control" onchange="CheckGender();CheckValidation(this.id)" >
                                            <option value="-" selected="selected">--Select Status--</option>
                                            <option value="1">Single</option>
                                            <option value="2">Married</option>
                                            <option value="3">Divorced</option>
                                            <option value="4">Window</option>
                                            <option value="5">Deceased</option>
                                            <option value="6">Unspecific</option>
                                            <option value="7">Child</option>
                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selMarital">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4" id="husband">
                                        <br>
                                        Spouse Name
                                        <label style="color: red; margin-top: 3px;" id="Spouse">
                                            <b>*</b></label>
                                        :
                                        <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtHusband" placeholder=" Spouse Name" class="form-control">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtHusband">
                                            <b>* This field is required</b></label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-4">
                                        <br />
                                        Present  Nationality
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selNationality" class="form-control" onchange="CheckValidation(this.id)">
                                            <option value="-">-Select Nationality-</option>
                                            <option value="209">AFGHANISTAN</option>
                                            <option value="473">ALBANIA</option>
                                            <option value="131">ALGERIA</option>
                                            <option value="445">ANDORRA</option>
                                            <option value="305">ANGOLA</option>
                                            <option value="663">ANTIGUA &amp; BARBUDA</option>
                                            <option value="605">ARGENTINA</option>
                                            <option value="263">ARMENIA</option>
                                            <option value="701">AUSTRALIA</option>
                                            <option value="417">AUSTRIA</option>
                                            <option value="265">AZERBAIJAN</option>
                                            <option value="619">BAHAMAS</option>
                                            <option value="107">BAHRAIN</option>
                                            <option value="207">BANGLADESH</option>
                                            <option value="657">BARBADOS</option>
                                            <option value="411">BELGIUM</option>
                                            <option value="651">BELIZE</option>
                                            <option value="369">BENIN</option>
                                            <option value="634">BERMUDA</option>
                                            <option value="211">BHUTAN</option>
                                            <option value="609">BOLIVIA</option>
                                            <option value="463">BOSNIA AND HERZEG</option>
                                            <option value="375">BOTSWANA</option>
                                            <option value="603">BRAZIL</option>
                                            <option value="401">BRITAIN</option>
                                            <option value="253">BRUNEI</option>
                                            <option value="465">BULGARIA</option>
                                            <option value="351">BURKINA FASO</option>
                                            <option value="307">BURUNDI</option>
                                            <option value="393">CABO VERDE</option>
                                            <option value="215">CAMBODIA</option>
                                            <option value="309">CAMEROON</option>
                                            <option value="501">CANADA</option>
                                            <option value="363">CENTRAL AFRICA REP</option>
                                            <option value="311">CHAD</option>
                                            <option value="607">CHILE</option>
                                            <option value="219">CHINA</option>
                                            <option value="611">COLOMBIA</option>
                                            <option value="642">COMONWEALTH DOMINICA</option>
                                            <option value="301">COMOROS</option>
                                            <option value="313">CONGO Republic</option>
                                            <option value="623">COSTARICA</option>
                                            <option value="453">CROATIA</option>
                                            <option value="621">CUBA</option>
                                            <option value="431">CYPRUS</option>
                                            <option value="452">CZECH</option>
                                            <option value="274">DAGHYSTAN</option>
                                            <option value="315">DAHOOMI</option>
                                            <option value="361">DEM REP OF CONGO</option>
                                            <option value="423">DENMARK</option>
                                            <option value="141">DJIBOUTI</option>
                                            <option value="625">DOMINICAN</option>
                                            <option value="613">ECUADOR</option>
                                            <option value="125">EGYPT</option>
                                            <option value="635">EL SALVADOR</option>
                                            <option value="251">ENTIAGO</option>
                                            <option value="303">ERITREN</option>
                                            <option value="459">ESTONIA</option>
                                            <option value="317">ETHIOPIA</option>
                                            <option value="705">FIJI</option>
                                            <option value="433">FINLAND</option>
                                            <option value="403">FRANCE</option>
                                            <option value="659">FRENCH GUIANA</option>
                                            <option value="371">GABON</option>
                                            <option value="387">GAMBIA</option>
                                            <option value="273">GEORGIA</option>
                                            <option value="407">GERMANY</option>
                                            <option value="381">GHAMBIA</option>
                                            <option value="319">GHANA</option>
                                            <option value="385">GHINIA BISSAU</option>
                                            <option value="429">GREECE</option>
                                            <option value="365">GREENLAND</option>
                                            <option value="649">GRENADA</option>
                                            <option value="627">GUATAMALA</option>
                                            <option value="653">GUYANA</option>
                                            <option value="639">HAITI</option>
                                            <option value="409">HOLLAND</option>
                                            <option value="647">HONDURAS</option>
                                            <option value="223">HONG KONG</option>
                                            <option value="467">HUNGARY</option>
                                            <option value="443">ICELAND</option>
                                            <option value="205" selected="selected">INDIA</option>
                                            <option value="243">INDONESIA</option>
                                            <option value="201">IRAN</option>
                                            <option value="113">IRAQ</option>
                                            <option value="427">IRELAND</option>
                                            <option value="405">ITALY</option>
                                            <option value="323">IVORY COAST</option>
                                            <option value="629">JAMAICA</option>
                                            <option value="231">JAPAN</option>
                                            <option value="121">JORDAN</option>
                                            <option value="503">KAIMAN ISLAN</option>
                                            <option value="715">KALDUNIA NEW</option>
                                            <option value="261">KAZAKHESTAN</option>
                                            <option value="325">KENYA</option>
                                            <option value="667">KINGSTONE</option>
                                            <option value="391">KIRIBATI</option>
                                            <option value="476">KOSOVA</option>
                                            <option value="105">KUWAIT</option>
                                            <option value="489">KYRGYZ REPUBLIC</option>
                                            <option value="117">LABANON</option>
                                            <option value="245">LAOS</option>
                                            <option value="461">LATVIA</option>
                                            <option value="377">LESOTHO</option>
                                            <option value="327">LIBERIA</option>
                                            <option value="127">LIBYA</option>
                                            <option value="449">LIECHTENSTEIN</option>
                                            <option value="457">LITHUANIA</option>
                                            <option value="413">LUXEMBOURG</option>
                                            <option value="259">MACAU</option>
                                            <option value="329">MADAGASCAR</option>
                                            <option value="333">MALAWI</option>
                                            <option value="241">MALAYSIA</option>
                                            <option value="257">MALDIVES</option>
                                            <option value="335">MALI</option>
                                            <option value="435">MALTA</option>
                                            <option value="727">MARSHALL ISLAND</option>
                                            <option value="661">MARTINIQUE</option>
                                            <option value="721">MARYANA ISLAND</option>
                                            <option value="135">MAURITANIA</option>
                                            <option value="367">MAURITIUS</option>
                                            <option value="601">MEXICO</option>
                                            <option value="732">MICRONESIA</option>
                                            <option value="481">MOLDOVA</option>
                                            <option value="439">MONACO</option>
                                            <option value="249">MONGOLIA</option>
                                            <option value="488">MONTENEGRO</option>
                                            <option value="133">MOROCCO</option>
                                            <option value="337">MOZAMBIQUE</option>
                                            <option value="373">NAMEBIA</option>
                                            <option value="737">NAURU</option>
                                            <option value="235">NEPAL</option>
                                            <option value="707">NEW GHINIA</option>
                                            <option value="703">NEW ZEALAND</option>
                                            <option value="631">NICARAGUA</option>
                                            <option value="339">NIGER</option>
                                            <option value="341">NIGERIA</option>
                                            <option value="229">NORTH KOREA</option>
                                            <option value="421">NORWAY</option>
                                            <option value="723">OKINAWA</option>
                                            <option value="203">PAKISTAN</option>
                                            <option value="669">PALAU</option>
                                            <option value="123">PALESTINE</option>
                                            <option value="633">PANAMA</option>
                                            <option value="731">PAPUA NEW GUINE</option>
                                            <option value="645">PARAGUAY</option>
                                            <option value="615">PERU</option>
                                            <option value="237">PHILIPPINES</option>
                                            <option value="471">POLAND</option>
                                            <option value="425">PORTUGAL</option>
                                            <option value="641">PUERTO RICO</option>
                                            <option value="109">QATAR</option>
                                            <option value="485">REPUBL. OF MACEDONIA</option>
                                            <option value="490">REPUBLIC OF BELARUS</option>
                                            <option value="213">REPUBLIC OF MYANMAR</option>
                                            <option value="321">REPUPLIC OF GUINEA</option>
                                            <option value="469">ROMANIA</option>
                                            <option value="343">ROWANDA</option>
                                            <option value="477">RUSSIA</option>
                                            <option value="491">SAINT LUCIA</option>
                                            <option value="665">SAINT VINSENT</option>
                                            <option value="447">SAN MARINO</option>
                                            <option value="395">SAO TOME</option>
                                            <option value="103">SAUDI ARABIA</option>
                                            <option value="345">SENEGAL</option>
                                            <option value="383">SICHEL</option>
                                            <option value="347">SIERRA LEONE</option>
                                            <option value="225">SINGAPORE</option>
                                            <option value="454">SLOVAKIA</option>
                                            <option value="455">SLOVENIA</option>
                                            <option value="725">SOLOMON ISLAND</option>
                                            <option value="139">SOMALIA</option>
                                            <option value="349">SOUTH AFRICA</option>
                                            <option value="227">SOUTH KOREA</option>
                                            <option value="138">SOUTH SUDAN</option>
                                            <option value="437">SPAIN</option>
                                            <option value="217">SRI LANKA</option>
                                            <option value="606">ST HELENA</option>
                                            <option value="487">ST KITTS-NAVIS</option>
                                            <option value="137">SUDAN</option>
                                            <option value="111">SULTANATE OF OMAN</option>
                                            <option value="462">SURBIA</option>
                                            <option value="655">SURINAME</option>
                                            <option value="379">SWAZILAND</option>
                                            <option value="419">SWEDEN</option>
                                            <option value="415">SWIZERLAND</option>
                                            <option value="119">SYRIA</option>
                                            <option value="713">TAHITI</option>
                                            <option value="221">TAIWAN</option>
                                            <option value="267">TAJIKSTAN</option>
                                            <option value="353">TANZANIA</option>
                                            <option value="711">TASMANIA</option>
                                            <option value="239">THAILAND</option>
                                            <option value="483">THE HELLENIC REPBL</option>
                                            <option value="709">TIMOR LESTE</option>
                                            <option value="255">TONGA</option>
                                            <option value="637">TRINIDAD</option>
                                            <option value="129">TUNISIA</option>
                                            <option value="475">TURKEY</option>
                                            <option value="269">TURKMENISTAN</option>
                                            <option value="735">TUVALU</option>
                                            <option value="502">U S A</option>
                                            <option value="357">UGANDA</option>
                                            <option value="479">UKRAINE</option>
                                            <option value="643">URGWAY</option>
                                            <option value="271">UZBAKISTAN</option>
                                            <option value="505">United Nations</option>
                                            <option value="733">VANVATU</option>
                                            <option value="441">VATICAN</option>
                                            <option value="617">VENEZUELA</option>
                                            <option value="233">VIETNAM</option>
                                            <option value="729">W SAMOA</option>
                                            <option value="115">YEMEN</option>
                                            <option value="464">YUGOSLAVIA</option>
                                            <option value="359">ZAMBIA</option>
                                            <option value="331">ZIMBABWE</option>

                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selNationality">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br />
                                        Birth Date :
                                      <input type="text" id="txtDob" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" title="Select Date" style="cursor: pointer" onchange="CheckValidation(this.id)"/>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDob">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br />
                                        Birth Country
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selCountry1" class="form-control" onchange="CheckValidation(this.id)">
                                            <option value="-">-Select Nationality-</option>
                                            <option value="209">AFGHANISTAN</option>
                                            <option value="473">ALBANIA</option>
                                            <option value="131">ALGERIA</option>
                                            <option value="445">ANDORRA</option>
                                            <option value="305">ANGOLA</option>
                                            <option value="663">ANTIGUA &amp; BARBUDA</option>
                                            <option value="605">ARGENTINA</option>
                                            <option value="263">ARMENIA</option>
                                            <option value="701">AUSTRALIA</option>
                                            <option value="417">AUSTRIA</option>
                                            <option value="265">AZERBAIJAN</option>
                                            <option value="619">BAHAMAS</option>
                                            <option value="107">BAHRAIN</option>
                                            <option value="207">BANGLADESH</option>
                                            <option value="657">BARBADOS</option>
                                            <option value="411">BELGIUM</option>
                                            <option value="651">BELIZE</option>
                                            <option value="369">BENIN</option>
                                            <option value="634">BERMUDA</option>
                                            <option value="211">BHUTAN</option>
                                            <option value="609">BOLIVIA</option>
                                            <option value="463">BOSNIA AND HERZEG</option>
                                            <option value="375">BOTSWANA</option>
                                            <option value="603">BRAZIL</option>
                                            <option value="401">BRITAIN</option>
                                            <option value="253">BRUNEI</option>
                                            <option value="465">BULGARIA</option>
                                            <option value="351">BURKINA FASO</option>
                                            <option value="307">BURUNDI</option>
                                            <option value="393">CABO VERDE</option>
                                            <option value="215">CAMBODIA</option>
                                            <option value="309">CAMEROON</option>
                                            <option value="501">CANADA</option>
                                            <option value="363">CENTRAL AFRICA REP</option>
                                            <option value="311">CHAD</option>
                                            <option value="607">CHILE</option>
                                            <option value="219">CHINA</option>
                                            <option value="611">COLOMBIA</option>
                                            <option value="642">COMONWEALTH DOMINICA</option>
                                            <option value="301">COMOROS</option>
                                            <option value="313">CONGO Republic</option>
                                            <option value="623">COSTARICA</option>
                                            <option value="453">CROATIA</option>
                                            <option value="621">CUBA</option>
                                            <option value="431">CYPRUS</option>
                                            <option value="452">CZECH</option>
                                            <option value="274">DAGHYSTAN</option>
                                            <option value="315">DAHOOMI</option>
                                            <option value="361">DEM REP OF CONGO</option>
                                            <option value="423">DENMARK</option>
                                            <option value="141">DJIBOUTI</option>
                                            <option value="625">DOMINICAN</option>
                                            <option value="613">ECUADOR</option>
                                            <option value="125">EGYPT</option>
                                            <option value="635">EL SALVADOR</option>
                                            <option value="251">ENTIAGO</option>
                                            <option value="303">ERITREN</option>
                                            <option value="459">ESTONIA</option>
                                            <option value="317">ETHIOPIA</option>
                                            <option value="705">FIJI</option>
                                            <option value="433">FINLAND</option>
                                            <option value="403">FRANCE</option>
                                            <option value="659">FRENCH GUIANA</option>
                                            <option value="371">GABON</option>
                                            <option value="387">GAMBIA</option>
                                            <option value="273">GEORGIA</option>
                                            <option value="407">GERMANY</option>
                                            <option value="381">GHAMBIA</option>
                                            <option value="319">GHANA</option>
                                            <option value="385">GHINIA BISSAU</option>
                                            <option value="429">GREECE</option>
                                            <option value="365">GREENLAND</option>
                                            <option value="649">GRENADA</option>
                                            <option value="627">GUATAMALA</option>
                                            <option value="653">GUYANA</option>
                                            <option value="639">HAITI</option>
                                            <option value="409">HOLLAND</option>
                                            <option value="647">HONDURAS</option>
                                            <option value="223">HONG KONG</option>
                                            <option value="467">HUNGARY</option>
                                            <option value="443">ICELAND</option>
                                            <option value="205" selected="selected">INDIA</option>
                                            <option value="243">INDONESIA</option>
                                            <option value="201">IRAN</option>
                                            <option value="113">IRAQ</option>
                                            <option value="427">IRELAND</option>
                                            <option value="405">ITALY</option>
                                            <option value="323">IVORY COAST</option>
                                            <option value="629">JAMAICA</option>
                                            <option value="231">JAPAN</option>
                                            <option value="121">JORDAN</option>
                                            <option value="503">KAIMAN ISLAN</option>
                                            <option value="715">KALDUNIA NEW</option>
                                            <option value="261">KAZAKHESTAN</option>
                                            <option value="325">KENYA</option>
                                            <option value="667">KINGSTONE</option>
                                            <option value="391">KIRIBATI</option>
                                            <option value="476">KOSOVA</option>
                                            <option value="105">KUWAIT</option>
                                            <option value="489">KYRGYZ REPUBLIC</option>
                                            <option value="117">LABANON</option>
                                            <option value="245">LAOS</option>
                                            <option value="461">LATVIA</option>
                                            <option value="377">LESOTHO</option>
                                            <option value="327">LIBERIA</option>
                                            <option value="127">LIBYA</option>
                                            <option value="449">LIECHTENSTEIN</option>
                                            <option value="457">LITHUANIA</option>
                                            <option value="413">LUXEMBOURG</option>
                                            <option value="259">MACAU</option>
                                            <option value="329">MADAGASCAR</option>
                                            <option value="333">MALAWI</option>
                                            <option value="241">MALAYSIA</option>
                                            <option value="257">MALDIVES</option>
                                            <option value="335">MALI</option>
                                            <option value="435">MALTA</option>
                                            <option value="727">MARSHALL ISLAND</option>
                                            <option value="661">MARTINIQUE</option>
                                            <option value="721">MARYANA ISLAND</option>
                                            <option value="135">MAURITANIA</option>
                                            <option value="367">MAURITIUS</option>
                                            <option value="601">MEXICO</option>
                                            <option value="732">MICRONESIA</option>
                                            <option value="481">MOLDOVA</option>
                                            <option value="439">MONACO</option>
                                            <option value="249">MONGOLIA</option>
                                            <option value="488">MONTENEGRO</option>
                                            <option value="133">MOROCCO</option>
                                            <option value="337">MOZAMBIQUE</option>
                                            <option value="373">NAMEBIA</option>
                                            <option value="737">NAURU</option>
                                            <option value="235">NEPAL</option>
                                            <option value="707">NEW GHINIA</option>
                                            <option value="703">NEW ZEALAND</option>
                                            <option value="631">NICARAGUA</option>
                                            <option value="339">NIGER</option>
                                            <option value="341">NIGERIA</option>
                                            <option value="229">NORTH KOREA</option>
                                            <option value="421">NORWAY</option>
                                            <option value="723">OKINAWA</option>
                                            <option value="203">PAKISTAN</option>
                                            <option value="669">PALAU</option>
                                            <option value="123">PALESTINE</option>
                                            <option value="633">PANAMA</option>
                                            <option value="731">PAPUA NEW GUINE</option>
                                            <option value="645">PARAGUAY</option>
                                            <option value="615">PERU</option>
                                            <option value="237">PHILIPPINES</option>
                                            <option value="471">POLAND</option>
                                            <option value="425">PORTUGAL</option>
                                            <option value="641">PUERTO RICO</option>
                                            <option value="109">QATAR</option>
                                            <option value="485">REPUBL. OF MACEDONIA</option>
                                            <option value="490">REPUBLIC OF BELARUS</option>
                                            <option value="213">REPUBLIC OF MYANMAR</option>
                                            <option value="321">REPUPLIC OF GUINEA</option>
                                            <option value="469">ROMANIA</option>
                                            <option value="343">ROWANDA</option>
                                            <option value="477">RUSSIA</option>
                                            <option value="491">SAINT LUCIA</option>
                                            <option value="665">SAINT VINSENT</option>
                                            <option value="447">SAN MARINO</option>
                                            <option value="395">SAO TOME</option>
                                            <option value="103">SAUDI ARABIA</option>
                                            <option value="345">SENEGAL</option>
                                            <option value="383">SICHEL</option>
                                            <option value="347">SIERRA LEONE</option>
                                            <option value="225">SINGAPORE</option>
                                            <option value="454">SLOVAKIA</option>
                                            <option value="455">SLOVENIA</option>
                                            <option value="725">SOLOMON ISLAND</option>
                                            <option value="139">SOMALIA</option>
                                            <option value="349">SOUTH AFRICA</option>
                                            <option value="227">SOUTH KOREA</option>
                                            <option value="138">SOUTH SUDAN</option>
                                            <option value="437">SPAIN</option>
                                            <option value="217">SRI LANKA</option>
                                            <option value="606">ST HELENA</option>
                                            <option value="487">ST KITTS-NAVIS</option>
                                            <option value="137">SUDAN</option>
                                            <option value="111">SULTANATE OF OMAN</option>
                                            <option value="462">SURBIA</option>
                                            <option value="655">SURINAME</option>
                                            <option value="379">SWAZILAND</option>
                                            <option value="419">SWEDEN</option>
                                            <option value="415">SWIZERLAND</option>
                                            <option value="119">SYRIA</option>
                                            <option value="713">TAHITI</option>
                                            <option value="221">TAIWAN</option>
                                            <option value="267">TAJIKSTAN</option>
                                            <option value="353">TANZANIA</option>
                                            <option value="711">TASMANIA</option>
                                            <option value="239">THAILAND</option>
                                            <option value="483">THE HELLENIC REPBL</option>
                                            <option value="709">TIMOR LESTE</option>
                                            <option value="255">TONGA</option>
                                            <option value="637">TRINIDAD</option>
                                            <option value="129">TUNISIA</option>
                                            <option value="475">TURKEY</option>
                                            <option value="269">TURKMENISTAN</option>
                                            <option value="735">TUVALU</option>
                                            <option value="502">U S A</option>
                                            <option value="357">UGANDA</option>
                                            <option value="479">UKRAINE</option>
                                            <option value="643">URGWAY</option>
                                            <option value="271">UZBAKISTAN</option>
                                            <option value="505">United Nations</option>
                                            <option value="733">VANVATU</option>
                                            <option value="441">VATICAN</option>
                                            <option value="617">VENEZUELA</option>
                                            <option value="233">VIETNAM</option>
                                            <option value="729">W SAMOA</option>
                                            <option value="115">YEMEN</option>
                                            <option value="464">YUGOSLAVIA</option>
                                            <option value="359">ZAMBIA</option>
                                            <option value="331">ZIMBABWE</option>

                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                            <b>* This field is required</b></label>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <br />
                                        Birth Place
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                              <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_BirthPlace" placeholder="Birth Place" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_BirthPlace">
                                            <b>* This field is required</b></label>
                                    </div>

                                    <div class="col-md-4">
                                        <br>
                                        Religion
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selReligion" class="form-control" onchange="CheckValidation(this.id)">
                                            <option value="-" selected="selected">--Select Religion--</option>
                                            <option value="0">Unknown</option>
                                            <option value="1">MUSLIM</option>
                                            <option value="2">CHRISTIAN</option>
                                            <option value="3">HINDHU</option>
                                            <option value="4">BUDIST</option>
                                            <option value="5">SIKH</option>
                                            <option value="6">KADIANI</option>
                                            <option value="7">BAHAEI</option>
                                            <option value="8">JEWISH</option>
                                            <option value="9">Zorostrian</option>
                                        </select>


                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selReligion">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Profession
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>
                                        :
                                        <select id="selProfession" class="form-control" onchange="CheckValidation(this.id)">
                                            <option value="-" selected="selected">--Select Profession--</option>
                                            <option value="NONE">NONE</option>
                                            <option value="STUDENT">STUDENT</option>
                                            <option value="SALES REPRESENTATIVE">SALES REPRESENTATIVE</option>
                                            <option value="MARKETING ASSISTANT">MARKETING ASSISTANT</option>
                                            <option value="MARKETING EXECUTIVE">MARKETING EXECUTIVE</option>
                                            <option value="BUSINESSMAN">BUSINESSMAN</option>
                                            <option value="BUSINESSWOMAN">BUSINESSWOMAN</option>
                                            <option value="BUSINESS PERSON">BUSINESS PERSON</option>
                                            <option value="BUSINESS">BUSINESS</option>
                                             <option value="HOUSE WIFE">HOUSE WIFE</option>
                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selProfession">
                                            <b>* This field is required</b></label>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <p class="lato size30 slim">
                     Address Details (as Per Passport)
                    </p>
                                <div class="line2"></div>
                                Address1
                                <label style="color: red; margin-top: 3px;">
                                    <b>*</b></label>:
                                      <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtAddress1" placeholder="Address1" class="form-control" onchange="CheckValidation(this.id)">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtAddress1">
                                    <b>* This field is required</b></label>
                                <br>
                                Address2
                                <label style="color: red; margin-top: 3px;">
                                    <b>*</b></label>:
                                      <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtAddress2" placeholder="Address2" class="form-control" onchange="CheckValidation(this.id)">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtAddress2">
                                    <b>* This field is required</b></label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <br>
                                        City
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                         <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtCity" placeholder="City" class="form-control" onchange="CheckValidation(this.id)">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtCity">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-4">
                                        <br>
                                        Country
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                        <select id="selCountry" class="form-control">
                                            <option value="-">-Select Nationality-</option>
                                            <option value="209">AFGHANISTAN</option>
                                            <option value="473">ALBANIA</option>
                                            <option value="131">ALGERIA</option>
                                            <option value="445">ANDORRA</option>
                                            <option value="305">ANGOLA</option>
                                            <option value="663">ANTIGUA &amp; BARBUDA</option>
                                            <option value="605">ARGENTINA</option>
                                            <option value="263">ARMENIA</option>
                                            <option value="701">AUSTRALIA</option>
                                            <option value="417">AUSTRIA</option>
                                            <option value="265">AZERBAIJAN</option>
                                            <option value="619">BAHAMAS</option>
                                            <option value="107">BAHRAIN</option>
                                            <option value="207">BANGLADESH</option>
                                            <option value="657">BARBADOS</option>
                                            <option value="411">BELGIUM</option>
                                            <option value="651">BELIZE</option>
                                            <option value="369">BENIN</option>
                                            <option value="634">BERMUDA</option>
                                            <option value="211">BHUTAN</option>
                                            <option value="609">BOLIVIA</option>
                                            <option value="463">BOSNIA AND HERZEG</option>
                                            <option value="375">BOTSWANA</option>
                                            <option value="603">BRAZIL</option>
                                            <option value="401">BRITAIN</option>
                                            <option value="253">BRUNEI</option>
                                            <option value="465">BULGARIA</option>
                                            <option value="351">BURKINA FASO</option>
                                            <option value="307">BURUNDI</option>
                                            <option value="393">CABO VERDE</option>
                                            <option value="215">CAMBODIA</option>
                                            <option value="309">CAMEROON</option>
                                            <option value="501">CANADA</option>
                                            <option value="363">CENTRAL AFRICA REP</option>
                                            <option value="311">CHAD</option>
                                            <option value="607">CHILE</option>
                                            <option value="219">CHINA</option>
                                            <option value="611">COLOMBIA</option>
                                            <option value="642">COMONWEALTH DOMINICA</option>
                                            <option value="301">COMOROS</option>
                                            <option value="313">CONGO Republic</option>
                                            <option value="623">COSTARICA</option>
                                            <option value="453">CROATIA</option>
                                            <option value="621">CUBA</option>
                                            <option value="431">CYPRUS</option>
                                            <option value="452">CZECH</option>
                                            <option value="274">DAGHYSTAN</option>
                                            <option value="315">DAHOOMI</option>
                                            <option value="361">DEM REP OF CONGO</option>
                                            <option value="423">DENMARK</option>
                                            <option value="141">DJIBOUTI</option>
                                            <option value="625">DOMINICAN</option>
                                            <option value="613">ECUADOR</option>
                                            <option value="125">EGYPT</option>
                                            <option value="635">EL SALVADOR</option>
                                            <option value="251">ENTIAGO</option>
                                            <option value="303">ERITREN</option>
                                            <option value="459">ESTONIA</option>
                                            <option value="317">ETHIOPIA</option>
                                            <option value="705">FIJI</option>
                                            <option value="433">FINLAND</option>
                                            <option value="403">FRANCE</option>
                                            <option value="659">FRENCH GUIANA</option>
                                            <option value="371">GABON</option>
                                            <option value="387">GAMBIA</option>
                                            <option value="273">GEORGIA</option>
                                            <option value="407">GERMANY</option>
                                            <option value="381">GHAMBIA</option>
                                            <option value="319">GHANA</option>
                                            <option value="385">GHINIA BISSAU</option>
                                            <option value="429">GREECE</option>
                                            <option value="365">GREENLAND</option>
                                            <option value="649">GRENADA</option>
                                            <option value="627">GUATAMALA</option>
                                            <option value="653">GUYANA</option>
                                            <option value="639">HAITI</option>
                                            <option value="409">HOLLAND</option>
                                            <option value="647">HONDURAS</option>
                                            <option value="223">HONG KONG</option>
                                            <option value="467">HUNGARY</option>
                                            <option value="443">ICELAND</option>
                                            <option value="205" selected="selected">INDIA</option>
                                            <option value="243">INDONESIA</option>
                                            <option value="201">IRAN</option>
                                            <option value="113">IRAQ</option>
                                            <option value="427">IRELAND</option>
                                            <option value="405">ITALY</option>
                                            <option value="323">IVORY COAST</option>
                                            <option value="629">JAMAICA</option>
                                            <option value="231">JAPAN</option>
                                            <option value="121">JORDAN</option>
                                            <option value="503">KAIMAN ISLAN</option>
                                            <option value="715">KALDUNIA NEW</option>
                                            <option value="261">KAZAKHESTAN</option>
                                            <option value="325">KENYA</option>
                                            <option value="667">KINGSTONE</option>
                                            <option value="391">KIRIBATI</option>
                                            <option value="476">KOSOVA</option>
                                            <option value="105">KUWAIT</option>
                                            <option value="489">KYRGYZ REPUBLIC</option>
                                            <option value="117">LABANON</option>
                                            <option value="245">LAOS</option>
                                            <option value="461">LATVIA</option>
                                            <option value="377">LESOTHO</option>
                                            <option value="327">LIBERIA</option>
                                            <option value="127">LIBYA</option>
                                            <option value="449">LIECHTENSTEIN</option>
                                            <option value="457">LITHUANIA</option>
                                            <option value="413">LUXEMBOURG</option>
                                            <option value="259">MACAU</option>
                                            <option value="329">MADAGASCAR</option>
                                            <option value="333">MALAWI</option>
                                            <option value="241">MALAYSIA</option>
                                            <option value="257">MALDIVES</option>
                                            <option value="335">MALI</option>
                                            <option value="435">MALTA</option>
                                            <option value="727">MARSHALL ISLAND</option>
                                            <option value="661">MARTINIQUE</option>
                                            <option value="721">MARYANA ISLAND</option>
                                            <option value="135">MAURITANIA</option>
                                            <option value="367">MAURITIUS</option>
                                            <option value="601">MEXICO</option>
                                            <option value="732">MICRONESIA</option>
                                            <option value="481">MOLDOVA</option>
                                            <option value="439">MONACO</option>
                                            <option value="249">MONGOLIA</option>
                                            <option value="488">MONTENEGRO</option>
                                            <option value="133">MOROCCO</option>
                                            <option value="337">MOZAMBIQUE</option>
                                            <option value="373">NAMEBIA</option>
                                            <option value="737">NAURU</option>
                                            <option value="235">NEPAL</option>
                                            <option value="707">NEW GHINIA</option>
                                            <option value="703">NEW ZEALAND</option>
                                            <option value="631">NICARAGUA</option>
                                            <option value="339">NIGER</option>
                                            <option value="341">NIGERIA</option>
                                            <option value="229">NORTH KOREA</option>
                                            <option value="421">NORWAY</option>
                                            <option value="723">OKINAWA</option>
                                            <option value="203">PAKISTAN</option>
                                            <option value="669">PALAU</option>
                                            <option value="123">PALESTINE</option>
                                            <option value="633">PANAMA</option>
                                            <option value="731">PAPUA NEW GUINE</option>
                                            <option value="645">PARAGUAY</option>
                                            <option value="615">PERU</option>
                                            <option value="237">PHILIPPINES</option>
                                            <option value="471">POLAND</option>
                                            <option value="425">PORTUGAL</option>
                                            <option value="641">PUERTO RICO</option>
                                            <option value="109">QATAR</option>
                                            <option value="485">REPUBL. OF MACEDONIA</option>
                                            <option value="490">REPUBLIC OF BELARUS</option>
                                            <option value="213">REPUBLIC OF MYANMAR</option>
                                            <option value="321">REPUPLIC OF GUINEA</option>
                                            <option value="469">ROMANIA</option>
                                            <option value="343">ROWANDA</option>
                                            <option value="477">RUSSIA</option>
                                            <option value="491">SAINT LUCIA</option>
                                            <option value="665">SAINT VINSENT</option>
                                            <option value="447">SAN MARINO</option>
                                            <option value="395">SAO TOME</option>
                                            <option value="103">SAUDI ARABIA</option>
                                            <option value="345">SENEGAL</option>
                                            <option value="383">SICHEL</option>
                                            <option value="347">SIERRA LEONE</option>
                                            <option value="225">SINGAPORE</option>
                                            <option value="454">SLOVAKIA</option>
                                            <option value="455">SLOVENIA</option>
                                            <option value="725">SOLOMON ISLAND</option>
                                            <option value="139">SOMALIA</option>
                                            <option value="349">SOUTH AFRICA</option>
                                            <option value="227">SOUTH KOREA</option>
                                            <option value="138">SOUTH SUDAN</option>
                                            <option value="437">SPAIN</option>
                                            <option value="217">SRI LANKA</option>
                                            <option value="606">ST HELENA</option>
                                            <option value="487">ST KITTS-NAVIS</option>
                                            <option value="137">SUDAN</option>
                                            <option value="111">SULTANATE OF OMAN</option>
                                            <option value="462">SURBIA</option>
                                            <option value="655">SURINAME</option>
                                            <option value="379">SWAZILAND</option>
                                            <option value="419">SWEDEN</option>
                                            <option value="415">SWIZERLAND</option>
                                            <option value="119">SYRIA</option>
                                            <option value="713">TAHITI</option>
                                            <option value="221">TAIWAN</option>
                                            <option value="267">TAJIKSTAN</option>
                                            <option value="353">TANZANIA</option>
                                            <option value="711">TASMANIA</option>
                                            <option value="239">THAILAND</option>
                                            <option value="483">THE HELLENIC REPBL</option>
                                            <option value="709">TIMOR LESTE</option>
                                            <option value="255">TONGA</option>
                                            <option value="637">TRINIDAD</option>
                                            <option value="129">TUNISIA</option>
                                            <option value="475">TURKEY</option>
                                            <option value="269">TURKMENISTAN</option>
                                            <option value="735">TUVALU</option>
                                            <option value="502">U S A</option>
                                            <option value="357">UGANDA</option>
                                            <option value="479">UKRAINE</option>
                                            <option value="643">URGWAY</option>
                                            <option value="271">UZBAKISTAN</option>
                                            <option value="505">United Nations</option>
                                            <option value="733">VANVATU</option>
                                            <option value="441">VATICAN</option>
                                            <option value="617">VENEZUELA</option>
                                            <option value="233">VIETNAM</option>
                                            <option value="729">W SAMOA</option>
                                            <option value="115">YEMEN</option>
                                            <option value="464">YUGOSLAVIA</option>
                                            <option value="359">ZAMBIA</option>
                                            <option value="331">ZIMBABWE</option>

                                        </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selCountries">
                                            <b>* This field is required</b></label>
                                    </div>
                                    

                                    <div class="col-md-4">
                                        <br />
                                        Contact No:
                                        <input type="text" id="txtTelephone" placeholder="0" class="form-control" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtTelephone">
                                            <b>* This field is required</b></label>
                                    </div>
                                   <%-- <div class="col-md-3">
                                        <br />
                                        ZIP Code
                                        <label style="color: red; margin-top: 3px;">
                                            <b>*</b></label>:
                                        <input type="text" id="txtZIP" placeholder="ZIP No" class="form-control" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtZIP">
                                            <b>* This field is required</b></label>
                                    </div>--%>
                                </div>
                                <br />
                                <br />
                                <%--<span class="size16 bold">Flight Details</span>--%>
                                <p class="lato size30 slim">
                     Flight Details 
                    </p>
                                <div class="line2"></div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <br />
                                        Arrival AirLine:
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqAirLine">
                                            <b>*</b></label>
                                        :
                                      <select id="selAirLine" class="form-control">
                                          <option value="-" selected="selected">-Select AirLine-</option>
                                          <option value="1">ABU DHABI</option>
                                          <option value="2">AEROFLOT </option>
                                          <option value="3">AEROSVIT</option>
                                          <option value="4">AIR ARABIA</option>
                                          <option value="5">AIR ASTANA</option>
                                          <option value="6">AIR BERLIN</option>
                                          <option value="7">AIR INIDA</option>
                                          <option value="8">AIR INDIA EXPERSS</option>
                                          <option value="9">AIR MAURITIUS</option>
                                          <option value="10">AIR SEYCHELLES</option>
                                          <option value="11">ARABIAN AIRLINES</option>
                                          <option value="12">AUSTRIAN AIRLINES</option>
                                          <option value="13">AZERBAI JAN AIR</option>
                                          <option value="14">BAHRAIN AIR</option>
                                          <option value="15">BIMAN BANGLADESH</option>
                                          <option value="16">JAP BRITISH AIRWAYSANESE</option>
                                          <option value="17">CATHAY PACIFIC</option>
                                          <option value="18">CHINA SOUTHERN</option>
                                          <option value="19">CATHY PACIFIC</option>
                                          <option value="20">CHINA SOUTHERN</option>
                                          <option value="21">CRIMEA AIR</option>
                                          <option value="22">DELTA AIR</option>
                                          <option value="23">CUTCH AIRLINES</option>
                                          <option value="98">EGYPT AIRWAYS</option>
                                          <option value="99">EMIRATES</option>
                                          <option value="10">ETIHAD</option>
                                          <option value="11">FINNAIR</option>
                                          <option value="12">FLY DUBAI</option>
                                          <option value="13">GULF AIR</option>
                                          <option value="14">HAHN AIR</option>
                                          <option value="15">INDIGO</option>
                                          <option value="16">IRAN AIR</option>
                                          <option value="17">IRAQI AIRWAYS</option>
                                          <option value="18">JET AIR</option>
                                          <option value="19">KENYA AIR</option>
                                          <option value="20">KING FISHER</option>
                                          <option value="21">KISH AIR</option>
                                          <option value="22">KLM ROYAL DUTCH</option>
                                          <option value="23">KOREAN AIR</option>
                                          <option value="98">KUWAIT AIR</option>
                                          <option value="99">LUFTHANSA</option>
                                          <option value="10">MAHAN AIR</option>
                                          <option value="11">MIHIN LANKA</option>
                                          <option value="12">NORDSTAR</option>
                                          <option value="13">OMAN AIR ORENBURG AIRLINES</option>
                                          <option value="14">PEGASUS AIRLINES</option>
                                          <option value="15">QATAR AIRWAYS</option>
                                          <option value="16">ROSSIYA</option>
                                          <option value="17">ROYAL BRUNEI</option>
                                          <option value="18">ROYAL JORDANIAN</option>
                                          <option value="19">RWANDAIR EXPRESS</option>
                                          <option value="20">S7 (SIBERIA) AIRLINES</option>
                                          <option value="21">SAUDI AIRLAINES</option>
                                          <option value="22">SINGAPORE  AIRLINES</option>
                                          <option value="23">SOMON AIR</option>
                                          <option value="98">SOUTH AFRICAN AIRWAY</option>
                                          <option value="99">SPICE JET</option>
                                          <option value="20">SRILANKAN AIRLINES</option>
                                          <option value="21">SWISS AIR</option>
                                          <option value="22">THAI AIR</option>
                                          <option value="23">TRANSAERO</option>
                                          <option value="98">TURKISH AIRLINES</option>
                                          <option value="99">UKRAINE AIRLINES</option>
                                          <option value="20">UNITED AIRLINES</option>
                                          <option value="21">URAL AIRLINES (U6)</option>
                                          <option value="22">UZBEKISTAN AIRWAYS</option>
                                          <option value="23">VIRGIN ATLANTIC</option>
                                          <option value="98">YEMEN AIRWAYS</option>
                                          <option value="99">JETLITE</option>
                                          <option value="22">ATLANTA</option>
                                          <option value="23">CRUISE</option>
                                          <option value="98">RUSLINE</option>
                                      </select>



                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selAirLine">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Arrival flight No
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqflightNo">
                                            <b>*</b></label>
                                        :
                                       <input type="text" id="txtflightNo" placeholder="Arrival flight No" class="form-control" style="cursor: pointer" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtflightNo">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Arrival from
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqArrivalNo">
                                            <b>*</b></label>
                                        :
                                       <input type="text" id="txtArrivalfrom" placeholder="Arrival from" class="form-control" style="cursor: pointer" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtArrivalfrom">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Arrival Date
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqArrivalDate">
                                            <b>*</b></label>
                                        :
                                       <input type="text" id="dteArrival" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" style="cursor: pointer">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_dteArrival">
                                            <b>* This field is required</b></label>
                                    </div>



                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <br />
                                        Deprture  AirLine:
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqDeprtureAirLine">
                                            <b>*</b></label>
                                        :
                                      <select id="selDeprtureAirLine" class="form-control">
                                          <option value="-" selected="selected">-Select AirLine-</option>
                                          <option value="1">ABU DHABI</option>
                                          <option value="2">AEROFLOT </option>
                                          <option value="3">AEROSVIT</option>
                                          <option value="4">AIR ARABIA</option>
                                          <option value="5">AIR ASTANA</option>
                                          <option value="6">AIR BERLIN</option>
                                          <option value="7">AIR INIDA</option>
                                          <option value="8">AIR INDIA EXPERSS</option>
                                          <option value="9">AIR MAURITIUS</option>
                                          <option value="10">AIR SEYCHELLES</option>
                                          <option value="11">ARABIAN AIRLINES</option>
                                          <option value="12">AUSTRIAN AIRLINES</option>
                                          <option value="13">AZERBAI JAN AIR</option>
                                          <option value="14">BAHRAIN AIR</option>
                                          <option value="15">BIMAN BANGLADESH</option>
                                          <option value="16">JAP BRITISH AIRWAYSANESE</option>
                                          <option value="17">CATHAY PACIFIC</option>
                                          <option value="18">CHINA SOUTHERN</option>
                                          <option value="19">CATHY PACIFIC</option>
                                          <option value="20">CHINA SOUTHERN</option>
                                          <option value="21">CRIMEA AIR</option>
                                          <option value="22">DELTA AIR</option>
                                          <option value="23">CUTCH AIRLINES</option>
                                          <option value="98">EGYPT AIRWAYS</option>
                                          <option value="99">EMIRATES</option>
                                          <option value="10">ETIHAD</option>
                                          <option value="11">FINNAIR</option>
                                          <option value="12">FLY DUBAI</option>
                                          <option value="13">GULF AIR</option>
                                          <option value="14">HAHN AIR</option>
                                          <option value="15">INDIGO</option>
                                          <option value="16">IRAN AIR</option>
                                          <option value="17">IRAQI AIRWAYS</option>
                                          <option value="18">JET AIR</option>
                                          <option value="19">KENYA AIR</option>
                                          <option value="20">KING FISHER</option>
                                          <option value="21">KISH AIR</option>
                                          <option value="22">KLM ROYAL DUTCH</option>
                                          <option value="23">KOREAN AIR</option>
                                          <option value="98">KUWAIT AIR</option>
                                          <option value="99">LUFTHANSA</option>
                                          <option value="10">MAHAN AIR</option>
                                          <option value="11">MIHIN LANKA</option>
                                          <option value="12">NORDSTAR</option>
                                          <option value="13">OMAN AIR ORENBURG AIRLINES</option>
                                          <option value="14">PEGASUS AIRLINES</option>
                                          <option value="15">QATAR AIRWAYS</option>
                                          <option value="16">ROSSIYA</option>
                                          <option value="17">ROYAL BRUNEI</option>
                                          <option value="18">ROYAL JORDANIAN</option>
                                          <option value="19">RWANDAIR EXPRESS</option>
                                          <option value="20">S7 (SIBERIA) AIRLINES</option>
                                          <option value="21">SAUDI AIRLAINES</option>
                                          <option value="22">SINGAPORE  AIRLINES</option>
                                          <option value="23">SOMON AIR</option>
                                          <option value="98">SOUTH AFRICAN AIRWAY</option>
                                          <option value="99">SPICE JET</option>
                                          <option value="20">SRILANKAN AIRLINES</option>
                                          <option value="21">SWISS AIR</option>
                                          <option value="22">THAI AIR</option>
                                          <option value="23">TRANSAERO</option>
                                          <option value="98">TURKISH AIRLINES</option>
                                          <option value="99">UKRAINE AIRLINES</option>
                                          <option value="20">UNITED AIRLINES</option>
                                          <option value="21">URAL AIRLINES (U6)</option>
                                          <option value="22">UZBEKISTAN AIRWAYS</option>
                                          <option value="23">VIRGIN ATLANTIC</option>
                                          <option value="98">YEMEN AIRWAYS</option>
                                          <option value="99">JETLITE</option>
                                          <option value="22">ATLANTA</option>
                                          <option value="23">CRUISE</option>
                                          <option value="98">RUSLINE</option>
                                      </select>
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_selDeprtureAirLine">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Deprture  flight No
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqDeprtureflight">
                                            <b>*</b></label>
                                        :
                                       <input type="text" id="txtDeprtureflightNo" placeholder="Deprture  flight No" class="form-control" style="cursor: pointer" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDeprtureflightNo">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Deprture  from
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqDeprturefrom">
                                            <b>*</b></label>
                                        :
                                       <input type="text" id="txtDeprturefrom" placeholder="Arrival from" class="form-control" style="cursor: pointer" />
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDeprturefrom">
                                            <b>* This field is required</b></label>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        Departing Date
                                        <label style="color: red; margin-top: 3px;" id="lbl_ReqDepartingDate">
                                            <b>*</b></label>
                                        :
                                         <input type="text" id="dteDeparting" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" style="cursor: pointer">
                                        <label style="color: red; margin-top: 3px; display: none" id="lbl_dteDeparting">
                                            <b>* This field is required</b></label>
                                    </div>
                                </div>

                                <br />
                                <br />
                                <table align="right">
                                    <tbody>
                                        <tr>
                                            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);" id="Update">
                                                <input id="btn_RegiterAgent" type="button" value="Save & Continue" class="btn-search margtop-2" style="width: 100%;" onclick="GetVisaCode()" title="Submit Details">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <br>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- End of Tab panes from left menu -->

                </div>
                <!-- END OF RIGHT CPNTENT -->

                <div class="clearfix"></div>
            </div>
            <!-- END CONTENT -->
        </div>
                <%--</div>--%>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="VisaDetailModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="height: 100%">
        <div class="modal-dialog modal-lg" style="width: 85%;">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left">Visa Details</h4>
                </div>

                <div class="modal-body">
                    <div class="scrollingDiv">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Visa details</b><label id="No"></label></div>

                        <div class="frow2">
                            <table style="width: 80%" align="center">

                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 40%;"><span class="dark bold">Visa Type: </span><span class="dark" id="VisaType1"></span></td>
                                    <td style="width: 20%;"><span class="dark bold">Ref.No.:</span> <span class="dark" id="Ref"></span></td>
                                </tr>
                            </table>

                        </div>
                        <div class="frow2">
                            <table style="width: 80%;" align="center">
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Processing Type: </span><span class="dark" id="Processing1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Visa Fee:</span> <span class="dark" id="VisaFee1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Other Fee </span><span class="dark" id="OtherFee1"></span></td>

                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td><span class="dark bold">Urgent Fee: </span><span class="dark" id="UrgentFee1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td><span class="dark bold">Service Tax :</span> <span class="dark" id="ServiceTax1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td><span class="dark bold">Total Amount: </span><span class="dark" id="TotalAmount1"></span></td>

                                </tr>
                            </table>
                        </div>


                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Passenger Details</b></div>

                        <div class="frow2">
                            <table style="width: 80%;" align="center">
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">First Name:</span> <span class="dark" id="first1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Middle Name: </span><span class="dark" id="Middle1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Last Name: </span><span class="dark" id="Last1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Father's Name:</span> <span class="dark" id="Father1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Mother's Name: </span><span class="dark" id="Mother1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Language: </span><span class="dark" id="Language1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Gender:</span> <span class="dark" id="Gender1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Marital Status: </span><span class="dark" id="Marital1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Spouse Name : </span><span class="dark" id="Husband1"></span></td>
                                    <td style="padding-left: 15px;"></td>
                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Nationality:</span> <span class="dark" id="Nationality1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">D. O.B : </span><span class="dark" id="dteBD1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                    <td style="width: 33%;"><span class="dark bold">Birth Country: </span><span class="dark" id="BirthCountry1"></span></td>
                                    <%--<td style="padding-left: 15px;"></td>--%>
                                </tr>
                            </table>
                        </div>


                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Passport Details</b></div>

                        <div class="frow2">
                            <table style="width: 80%;" align="center">
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Passport No:</span> <span class="dark" id="PassportNo1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Please of Issue: </span><span class="dark" id="PleaseIssue1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold"></span><span class="dark"></span></td>

                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Date Of Issue</span> <span class="dark" id="dteIssue1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Expiry Date: </span><span class="dark" id="dteExpiry1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold"></span><span class="dark"></span></td>
                                </tr>

                            </table>

                        </div>

                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Address Details</b></div>

                        <div class="frow2">
                            <table style="width: 80%;" align="center">
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Address1 :</span> <span class="dark" id="Address1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Contact No: </span><span class="dark" id="Contact1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Country: </span><span class="dark" id="Country1"></span></td>
                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Address2: </span><span class="dark" id="Address2"></td>
                                    <td style="width: 33%;"><span class="dark bold">ZIP Code :</span> <span class="dark" id="ZIP1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">City </span><span class="dark" id="City1"></span></td>
                                </tr>
                            </table>
                        </div>

                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Traivailing Details </b></div>
                        <div class="frow2">
                            <table style="width: 80%;" align="center">
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Arrival AirLine :</span> <span class="dark" id="ArrAir"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Arrival flight No: </span><span class="dark" id="ArrFlightVo"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Arrival from: </span><span class="dark" id="ArrFrom"></span></td>

                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Deprture AirLine :</span> <span class="dark" id="DptAir"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Deprture  flight No: </span><span class="dark" id="DptFormNo"></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Deprture  from: </span><span class="dark" id="DptFrom"></span></td>

                                </tr>
                                <tr style="width: 100%; height: 100%">
                                    <td style="width: 33%;"><span class="dark bold">Arrival Date: </span><span class="dark" id="Arrival1"></span></td>
                                    <td style="width: 33%;"><span class="dark bold"></span><span class="dark" id=""></span></td>
                                    <td style="width: 33%;"><span class="dark bold">Departing  Date: </span><span class="dark" id="Departing1"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="frow2">
                            <div class="alert alert-warning">
                                Important information about your booking:<br>
                                <p class="size12">
                                    <input type="checkbox" data-bind="checked: accept" id="Condition">
                                    Charges Will Be Apply <a style="cursor: pointer" target="_blank">Terms and Conditions</a>
                                </p>
                            </div>
                        </div>
                        <div class="frow2" style="text-align: center;">
                            <img style="-webkit-user-select: none; margin-top: 10px; display: none;" id="dlgLoader" src="../images/loader.gif"><br>
                            <input type="button" value="Confirm Visa" onclick="SaveUpload()" id="btnCancelBooking" class="btn-search4 margtop20" style="float: none;">
                            <input type="button" value="Cancel" onclick="dispose()" id="Cancel" class="btn-search4 margtop20" style="float: none;">
                        </div>
                        <br />
                        <br />
                        <br />
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="ErrorDocumentModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
    
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Incomplete Document</b><label id="No"></label></div>
                        <div class="alert">
                          <p class="size17">
                              It looks like you have missed some information and your form is incomplete <br /><br />

                            Go back by clicking Back button and fill all the mandatary fields to<br />
                            complete the form or click on Save to complete the application later.

                            </p>
                          <table align="right" style="margin-left: -50%;">
                                    <tbody><tr>
                                         <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Back" onclick="dispose()" class="btn-search4 margtop20" style="float: none;">
                                        </td>
                                        <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                            <input type="button" value="Save" onclick="SaveContinue()" id="btnBooking" class="btn-search4 margtop20" style="float: none;">
                            
                                        </td>
                                       
                                    </tr>
                                </tbody></table>  
                            <br />

                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="UpdateUserModal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body" style="overflow: scroll;height: 450px;">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Update Login Details</b></div>
                        <div class="frow2">
                            <table id="tblForms" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td colspan="1" >
                                            <label >
                                            Active :</label>
                                          <input id="chkActive" type="checkbox" value="Active"/>
                                            
                                            </td>
                                        <td align="center">
                                            <input id="txt_Username" placeholder="User name" type="text"  class=" form-control" />
                                            </td>
                                        <td align="center">
                                            <input id="txt_Password"  type="text"  class=" form-control" placeholder="Password" />
                                            </td>
                                         
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <input id="txt_Supplier" type="text"  class=" form-control" placeholder="Supplier Name"/>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chk96hr" type="checkbox" value="96hr"/>
                                            96 Hour</td>
                                         <td>
                                            <input id="96hrPrice"  class=" form-control" type="text" placeholder="96 Hour Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="96hrCr" name="Cr">
                                                 <option value="INR" selected="selected">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    <tr> 
                                        <td><input id="chk14Days" type="checkbox" value="14Days"/>14 Days</td>
                                         <td>
                                            <input id="14DaysPrice"  class=" form-control" type="text" placeholder="14 Days Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="14DaysCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    <tr>
                                          <td>
                                            <input id="chk30Single" type="checkbox" value="30Single"/>
                                            30 Days Single</td>
                                        <td>
                                            <input id="30SinglePrice"  class=" form-control" type="text" placeholder="30 Single Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="30SingleCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    
                                        <%--<td>
                                            <input id="chk30Multipule" type="checkbox" value="30Multipule"/>
                                            30 Days Multipule</td>--%>
                                    </tr>
                                    <tr>
                                          <td>
                                            <input id="chk30Multipule" type="checkbox" value="30Multipule"/>
                                            30 Days Multiple</td>
                                        <td>
                                            <input id="30MultipulePrice"  class=" form-control" type="text" placeholder="30 Days Multiple Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="30MultipuleCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    <tr>
                                         <td>
                                            <input id="chk90Single" type="checkbox" value="90Single"/>
                                            90 Days Single</td>
                                        <td>
                                            <input id="90SinglePrice"  class=" form-control" type="text" placeholder="90 Days Single Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="90SingleCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    <tr>
                                         <td>
                                            <input id="chk90Multipule" type="checkbox" value="90Multipule"/>
                                            90 Days Multiple</td>
                                        <td>
                                            <input id="90MultipulePrice"  class=" form-control" type="text" placeholder="90 Days Multiple Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="90MultipuleCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    <tr>
                                         <td>
                                            <input id="chk90Covertable" type="checkbox" value="90Covertable"/>
                                            90 Days Convetable  <input type="button" onclick="CheckBtn()" class="btn-search" value="Update" title="Update Suppliers" id="btn_User" style="padding-bottom:-10px"/></td></td>
                                        <td>
                                            <input id="90ConvetablePrice"  class=" form-control" type="text" placeholder="90 Convetable Price"/>
                                            </td>
                                      
                                         <td>
                                             <select  class=" form-control" id="90ConvetableCr" name="Cr">
                                                 <option value="INR">INR</option>
                                                 <option value="AED">AED</option>
                                                 <option value="USD">USD</option>
                                             </select>
                                         </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            <table  class="table table">
                                <tr>
                                    <td>
                                      
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

      <div class="modal fade" id="Modal96Hours" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <%--<img src="../images/dash/logo.png" alt="ClickUrTrip.com" />--%>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close" onclick="Redirect()"><span aria-hidden="true">&times;</span></button>
                    <label style=" display:none" id="lblpreview">Show</label>
                  <%--  <h4 class="modal-title" id="gridSystemModalLabel4" style="text-align: left">Supporting Document</h4>--%>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b>Visa Copy</b><label id="No"></label></div>
                         <table style="width: 100%" align="center">
                                    <tbody>
                                        <tr style="width: 100%">
                                            <td align="center" style="width: 50%" id="Ticket" >
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
