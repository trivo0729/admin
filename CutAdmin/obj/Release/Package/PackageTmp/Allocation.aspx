﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StartSale.aspx.cs" Inherits="CutAdmin.StartSale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <script src="Scripts/HotelInventory.js?v=1.0"></script>


    <%--<script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });
            $("#datepicker_Till").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });

        });


    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <div class="with-padding">
            <div class="row block margin-bottom">
                <span class="block-title anthracite-gradient glossy">
                    <h3>
                        <u>
                            <label id="lbl_Hotel" style="float: left"></label>
                        </u>
                    </h3>
                    <br />
                    <label id="lbl_roomtype" style="float: right"></label>
                    <br />
                    <label id="lbl_address"></label>
                </span>
                <div class="with-padding " id="div_Rooms">
                    <div class="columns">
                        <div class="new-row two-columns" id="" style="">

                             <small class="input-info">Select Room*:</small><br />
                            <select id="ddlRoom" class="select multiple-as-single easy-multiple-selection allow-empty check-list full-widtht" onchange="" multiple>
                            </select>
                        </div>
                        <div class="two-columns" id="" style="">

                            <small class="input-info">Select Rate Type*:</small>
                            <select id="ddlRatetype" class="full-width glossy select multiple-as-single easy-multiple-selection check-list Rate" multiple onchange="fnRatType()">
                                <%--<option class="" value="" selected="selected">Rate Type</option>--%>
                                <option class="" value="Contracted">Contracted</option>
                                <option class="" value="Promo">Promo</option>
                                <option class="" value="Tactical">Tactical</option>
                            </select>
                        </div>

                        <div class="two-columns" id="">
                            <small class="input-info">No Of Room* :</small>
                            <input type="text" name="" id="txt_NoOfRoom" class="input full-width  " value="">
                        </div>
                        <div class="six-columns" id="DatesUI" style="">
                        </div>
                        <%--<div class="three-columns">
                            <small class="input-info">Free Sale Till*:</small>
                            <input class="input ui-autocomplete-input full-width" type="text" id="datepicker_Till" name="" style="cursor: pointer; width: 90%" value="" />

                        </div>--%>
                    </div>
                    <details class="details margin-bottom" onclose>
                        <summary>Optional</summary>
                        <div class="columns" style="min-height: 130px; margin-top: 20px; margin-left: 14px">
                            <div class="eight-columns">
                                <ul>
                                    <li>
                                        <div>
                                            <span>Days Prior.</span>
                                            <span>
                                                <input type="text" name="" id="txt_DaysPrior" class="input" value="" style="width: 5%"></span>
                                        </div>
                                    </li>
                                    <br />

                                    <br />
                                    <div>
                                        <li>Inventory sold live or on request.
                                                <span class="button-group" style="margin-left: 5%">
                                                    <label for="chk_Live" class="button blue-active">
                                                        <input type="radio" name="button-radio" id="chk_Live" value="Live" onclick="Redirectbtn(this.value)" checked>
                                                        Live
                                                    </label>
                                                    <label for="chk_Request" class="button red-active">
                                                        <input type="radio" name="button-radio" id="chk_Request" value="Request" onclick="Redirectbtn(this.value)">
                                                        Request
                                                    </label>

                                                </span>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </details>
                    <div class="columns">
                        <div class="new-row twelve-columns" id="">
                            <button type="button" style="float: right" value="Allocation" class="button glossy mid-margin-right" onclick="SaveAllocation(this.value);" title="Submit Details">
                                <span class="button-icon"><span class="icon-tick"></span></span>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content -->
</asp:Content>

