﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CancelationPolicies.aspx.cs" Inherits="CutAdmin.CancelationPolicies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/CancelationPolicies.js?v=1.3"></script>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">
        <div class="with-padding">

            <hgroup id="main-title" class="thin">
                <div class="Columns">
                    <h1>Cancelation Policies</h1>
                    <label style="font-size: 18px; margin-left: 658px;">Cancellation Days</label>
                    <input type="text" class="input full-width" id="CancellationDays" onchange="SaveCancellationDays()" style="float: right; width: 20%;" />
                </div>
                <br />
                <br />
                <hr />
            </hgroup>
            <div class="with-padding">
                <div class="columns">
                    <div class="new-row three-columns">
                        <span class="button-group" id="divRefund">
                            <label for="rd_Refundable" class="button grey-active">
                                <input type="radio" name="refund" id="rd_Refundable" value="Refundable" onclick="checkNoShow();" checked>
                                Refundable
                            </label>
                            <label for="rd_NoShow" class="button grey-active">
                                <input type="radio" name="refund" id="rd_NoShow" value="NoShow" onclick="checkNoShow();">
                                No Show
                            </label>
                            <%-- <label for="rd_NoShow" class="button blue-active">
									<input type="radio" name="refund" id="rd_NonRef" value="NoShow">
									No Show
								</label>--%>								
                        </span>
                    </div>
                    <div class="four-columns" id="divMainDateDays">
                        <span class="in-group" id="divDaysPrior">
                            <label for="rd_DaysPrior" class="button grey-active">
                                <input type="radio" name="DaysPrior" id="rd_DaysPrior" value="DaysPrior" onclick="checkDaysPrerior();" checked>
                                Days Prior
                            </label>

                            <label for="rd_Date" class="button grey-active">
                                <input type="radio" name="DaysPrior" id="rd_Date" value="Date" onclick="checkDaysPrerior();">
                                Date
                            </label>

                            <span id="divchkDaysPrior">
                                <input type="text" onkeyup="checkRefund();" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txt_PriorDays" class="input" value="" style="width: 80px;">
                            </span>
                            <span class="input" id="divchkDate" style="display: none;">
                                <span class="icon-calendar"></span>
                                <input type="text" onkeyup="checkRefund();" id="txt_Date" class="input-unstyled cRateValidFrom" value="" style="width: 80px;">
                            </span>
                        </span>

                    </div>
                    <div class="two-columns">
                        <select id="sel_RateType" class="select full-width" onchange="fSelectRateType();">
                            <option value="Amount">Fixed Amount</option>
                            <option value="Percentile">Percentile</option>
                            <option value="Nights">Nights</option>
                        </select>
                    </div>
                    <div id="divAmount" class="two-columns">

                        <input type="text" placeholder="Amount" onkeyup="checkRefund();" onkeypress='return event.charCode >= 31 && event.charCode <= 57' id="txt_Amount" class="input full-width" value="">
                    </div>
                    <div id="divPercent" class="two-columns" style="display: none">
                        <input type="text" placeholder="Percent" onkeyup="checkRefund();" onkeypress='return event.charCode >= 31 && event.charCode <= 57' id="txt_Percent" class="input full-width" value="">
                    </div>
                    <div id="divNights" class="two-columns" style="display: none">
                        <input type="text" placeholder="Nights" onkeyup="checkRefund();" onkeypress='return event.charCode >= 31 && event.charCode <= 57' id="txt_Nights" class="input full-width" value="">
                    </div>
                    <div class="new-row three-columns">
                        <input type="text" id="txt_temp" class="input" value="" style="display: none">
                          <label> Policy Name</label>
                       
                           <input type="text" id="txt_CancelationPolicy" class="input" value="" disabled>
                    </div>
                    <div class="five-columns">
                        <label>Note</label>
                        <textarea name="autoexpanding" id="txtCancelNote" class="input full-width"></textarea>
                    </div>
                    <div class="two-columns">
                        <br />
                        <button type="button" style="margin-left: 10px" class="button anthracite-gradient" onclick="SaveCancelation()">Save</button>
                    </div>
                </div>
                <div class="columns">
                    <div class="new-row twelve-columns">
                        <%--<h4>Cancelation Policies</h4>
                        <hr />--%>
                        <table class="table responsive-table responsive-table-on" id="tbl_CancelationList" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Cancelation Policy</th>
                                    <th scope="col">Refund Type</th>
                                    <th scope="col">Days Prior/Date</th>
                                    <th scope="col">Rate Type</th>
                                    <th scope="col">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


</asp:Content>
