﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="CutAdmin.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="HotelAdmin/Scripts/ChangePassword.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Change Password</h1>
        </hgroup>
        <div class="with-padding">
            <form action="#">
                <hr>
                <br />
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Current Password</label><br>
                        <div class="input full-width">
                            <input id="txtOldPassword" type="password" class="input-unstyled full-width">
                        </div>
                        <label style="color: red; margin-top: 3px; display: none;" id="lbl_OldPassword">
                                    <b>* This field is required</b>
                        </label>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>New Password</label><br>
                        <div class="input full-width">
                            <input id="txtNewPassword" type="password" class="input-unstyled full-width">
                        </div>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_NewPassword">
                                    <b>* This field is required</b>
                        </label>
                    </div>
                    <div class="four-columns twelve-columns-mobile six-columns-tablet">
                        <label>Confirm New Password</label><br>
                        <div class="input full-width">
                            <input id="txtConfirmNewPassword" type="Password" class="input-unstyled full-width">
                        </div>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmNewPassword">
                                    <b>* This field is required</b>
                        </label>
                    </div>
                    <div style="float:right;margin-top:2%">
                        <input id="btnSubmit" type="button" value="Save Changes" class="button anthracite-gradient UpdateMarkup" style="width: 78%" onclick="ChangePassword()">
                    </div>
                </div>
                <input type="hidden" id="hdnpassword" />
            </form>
        </div>
    </section>
</asp:Content>
