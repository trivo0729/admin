﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CityMapping.aspx.cs" Inherits="CutAdmin.GetCountryCityMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="Scripts/CountryCityCode.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetCountry();
        });
    </script>
    <script src="Scripts/GetCountryCityMapping.js"></script>

	<!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" class="clearfix with-menu with-shortcuts">

<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3> Cities Mapping</h3><hr />
		</hgroup>
        
		<div class="with-padding">

		
			<div class="button-height">
				Country:
				<select name="select90"  id="selCountry" class="select blue-gradient glossy mid-margin-left">
                     <option selected="selected" value="-">Select Any Country</option>
				</select>
                &nbsp;&nbsp;&nbsp;
                City:
				<select name="select90"  id="selCity" class="select blue-gradient glossy mid-margin-left">
                    <option selected="selected" value="-">Select Any City</option>
				</select>
                &nbsp;&nbsp;&nbsp;
				<button type="button" class="button blue-gradient glossy" id="btn_GetCityCode"  onclick="GetMappingDetails();">Go</button>
			
               
            </div>
		
             <table class="table responsive-table" id="tbl_GetCityCodeMapping">
                        <tbody>
                            <tr style="background-color:#494747;color:white">
                                
                                <td>
                                    <span class="text-left">Supplier</span>
                                </td>
                                <td>
                                    <span class="text-left">Country Name</span>
                                </td>
                                <td>
                                    <span class="text-left">Code</span>
                                </td>
                                <td>
                                    <span class="text-left">City Name</span>
                                </td>
                                <td>
                                    <span class="text-left">City Code</span>
                                </td>
                                <td>
                                    <span class="text-left">Area Name</span>
                                </td>
                                <td>
                                    <span class="text-left">Area Code</span>
                                </td>
                                <td>
                                    <span class="text-left">Select</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
			<div  class="table-footer ">
                 <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
			<br /><br />
			</div>

	
		</div>

	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

	<script>

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>
</asp:Content>
