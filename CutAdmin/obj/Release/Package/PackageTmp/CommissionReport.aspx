﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CommissionReport.aspx.cs" Inherits="CutAdmin.CommissionReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="../js/libs/jquery-1.10.2.min.js"></script>

    <script src="Scripts/CommissionInvoiceDetailReport.js?v=1.15"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Commision Report</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <div class="columns">
              <%--  <div class="four-columns ">
                    <label><b>Supplier </b></label>
                    <select id="selSupplier"  class="select Supplier">
                        <option value="-">-Select Any Supplier-</option>
                       
                    </select>
                </div>--%>
                <div class="two-columns ">
                    <label><b>Month </b></label>
                    <select id="selMonth"  class="select Supplier">
                        <option value="-">-Select Any Month-</option>
                       <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>

                  <div class="two-columns ">
                    <label><b>Year </b></label>
                    <select id="selYear"  class="select">
                        <option value="-">-Select Any Year-</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                    </select>
                </div>
                <div class="one-columns ">
                    <br />
                   <button type="button" class="button anthracite-gradient" onclick="Search()" id="btn_Search">Search</button>
                </div>
                <div class="three-columns">
                     <br />
                    <label ><b>Total Commission : </b></label>
                    <label  id="UnPaidAmt"></label>
                </div>

                 <div class="two-columns twelve-columns-mobile bold text-alignright">
                     <br />
                                <span class="icon-pdf right" onclick="ExportCommReportToExcel('PDF')" style="float:left">
                                    <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                </span>
                                <span class="icon-excel right" onclick="ExportCommReportToExcel('excel')" style="float:left">
                                    <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                            </div>
            </div>
            <div class="respTable">
                <table class="table responsive-table" id="tbl_CommInvoiceDetails">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center hide-on-mobile">Sr No.</th>
                             <th scope="col" class="align-center hide-on-mobile">Invoice Date</th>
                            <th scope="col" class="align-center hide-on-mobile">Invoice No </th>
                             <th scope="col" class="align-center hide-on-mobile">Check-In </th>
                             <th scope="col" class="align-center hide-on-mobile">Check-Out </th>
                            <th scope="col" class="align-center hide-on-mobile">Room Night </th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Invoice Amount</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Commision Amount</th>
                            <%--<th scope="col" width="150" class="align-center">Action</th>--%>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
