﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="CutAdmin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <!-- Additional styles -->

    <script src="Scripts/Dashboard.js?v=1.26"></script>
    <script src="Scripts/BookingList.js?v=1.0"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
            <h1>Dashboard</h1>
        </hgroup>
        <!--Black Strip -->
        <div class="dashboard">
            <div class="columns">
                <div class="three-columns twelve-columns-mobile">
                    <div class="twelve-columns">
                        <div class="silver">Total Sales</div>
                        <div class="large-text-shadow align-central">
                            <h3 class="mid-margin-bottom">AED 100,000,000</h3>
                        </div>

                        <div class="ten-columns" style="border-top: 1.2px solid #cccccc; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);"></div>
                        <div class="columns mid-margin-top">
                            <div class="six-columns">
                                <h4 class="align-left">12,345</h4>
                                <span class="align-left silver">Rooms</span>
                            </div>
                            <div class="six-columns">
                                <h4 class="align-left">12,345</h4>
                                <span class="align-left silver">Nights</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <div class="twelve-columns">
                        <div class="silver">Commission Earned</div>
                        <div class="large-text-shadow align-central">
                            <h3 class="mid-margin-bottom">AED 100,000,000</h3>
                        </div>
                        <div class="ten-columns" style="border-top: 1.2px solid #cccccc; box-shadow: inset 0 1px 0 rgba(255, 255, 254, 0.67);"></div>

                        <div class="columns mid-margin-top">
                            <div class="six-columns">
                                <h4 class="align-left">12,345</h4>
                                <span class="align-left silver">Settled</span>
                            </div>
                            <div class="six-columns">
                                <h4 class="align-left">12,345</h4>
                                <span class="align-left silver">Pending</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile"></div>
                <div class="three-columns twelve-columns-mobile new-row-mobile">
                    <ul class="stats split-on-mobile">
                        <li>
                            <a href="AgentDetails.aspx">
                                <strong>
                                    <lable id="hotel_Count"></lable>
                                </strong>
                                <br>
                                Hotels 
                            </a>
                        </li>
                        <li>
                            <a href="AgentDetails.aspx">
                                <strong>
                                    <lable id="Agents"></lable>
                                </strong>
                                <br>
                                Agents
                            </a>
                        </li>

                    </ul>
                </div>

            </div>

        </div>
        <!--First Row -->
        <div class="with-padding">
           <!--Booking Reconfirmations -->
            <div class="columns">
                <div class="three-columns large-box-shadow orange-bg glossy " id="BookingsReconfirm">
                  
                </div>
                <!--On Request Bookings-->
                <div class="three-columns large-box-shadow blue-bg glossy " id="BookingsRequests">
                  
                </div>
                 <!--Bookings On Hold-->
                <div class="three-columns large-box-shadow green-bg glossy" id="BookingOnHold">
                 
                </div>
                 <!--Group Request-->
                <div class="three-columns large-box-shadow linen" id="GroupRequest">
                  
                </div>
            </div>

            <!--Second Row -->
             <div class="columns">
                <div class="three-columns large-box-shadow orange-bg glossy ">
                    <div class="with-mid-padding"style="background-color:darkgoldenrod; border-bottom:2px solid navajowhite">
                        <h4 class="icon-line-graph icon-size2">Top Buyer</h4>

                    </div>
                   
                    <div class="with-mid-padding" id="div_Buyer">
                       
                    </div>
                </div>
                <div class="three-columns large-box-shadow blue-bg">
                    <div class="with-mid-padding"style="background-color:steelblue; border-bottom:2px solid lightblue">
                        <h4 class="icon-link icon-size2">Top Supplier</h4>
                    </div>                    
                    <div class="with-mid-padding" id="div_Supplier">
                       
                    </div>
                </div>

                <div class="three-columns large-box-shadow green-bg">
                    <div class="with-mid-padding"style="background-color:darkolivegreen; border-bottom:2px solid greenyellow">
                        <h4 class="icon-globe icon-size2">Top Destinations</h4>

                    </div>                   
                    <div class="with-mid-padding" id="div_Destination">
                       
                    </div>
                </div>
                <div class="three-columns large-box-shadow linen">
                    <div class="with-mid-padding"style="background-color:grey; border-bottom:2px solid darkgrey">
                        <h4 class="icon-home icon-size2">Top Hotels</h4>
                    </div>                    
                    <div class="with-mid-padding" id="div_Hotels">
                      
                    </div>
                </div>

            </div>
            <div class="columns datepicker" id="">
            </div>
        </div>
    </section>
</asp:Content>
