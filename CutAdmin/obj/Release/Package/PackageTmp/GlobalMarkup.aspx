﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="GlobalMarkup.aspx.cs" Inherits="CutAdmin.GlobalMarkup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/GlobalMarkup.js?v=1.5"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Global Markup</h1>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Markup">
            
        </div>
    </section>
</asp:Content>
