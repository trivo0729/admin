﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="GroupMarkup.aspx.cs" Inherits="CutAdmin.GroupMarkup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="Scripts/GroupMarkup.js?v=1.3"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>GroupMarkup  
                 <div class="Groupmark" id="DivGroupmark" style="float: right">
                     <select id="SelGroup" onchange="GetGroupMarkup()" name="validation-select" class="select Groupmark">
                     </select>
                     <button type="button" id="btn_RegiterAgent" class="button anthracite-gradient Updategroup  " onclick="AddGroupModalFunc();" title="Add Group">Add</button>

                 </div>
            </h1>

            <hr />
        </hgroup>

        <%--<div class="with-padding">

            <div class="columns">

                <div class="four-columns eight-columns-mobile">
                </div>
                <div class="two-columns six-columns-mobile bold text-right">
                </div>
            </div>


            <div class="standard-tabs margin-bottom tabs-active" id="add-tabs" style="height: 30px;">
            </div>
        </div>--%>
        <div class="with-padding" id="div_Markup">
            
        </div>
    </section>
</asp:Content>
