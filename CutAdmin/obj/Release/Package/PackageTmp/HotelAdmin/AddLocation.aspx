﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="AddLocation.aspx.cs" Inherits="CutAdmin.HotelAdmin.AddLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>

    <script src="Scripts/AddLocation.js?v=1.4"></script>

    <%--<script type="text/javascript">
        $(document).ready(function () {
            GetCountry();
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
            <h1>Location Details</h1>
            <hr />
            <%--   <h2 class="updateDate"><span class="lato size15 grey">Update Online Rate</span></h2>--%>
        </hgroup>

        <div class="with-padding">

            <div class="columns">
                <div class="three-columns three-columns-mobile">
                    <label>Location Name</label><span class="red" id="lbl_Name">*</span>
                    <div class="input full-width">
                        <input value="" class="input-unstyled full-width" type="text" id="txt_Location">
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <label>Country</label><span class="red" id="lbl_Country">*</span>
                    <div class="full-width button-height" id="DivCountry">
                        <select id="selCountry" name="select90" class="select country full-width">
                            <option selected="selected" value="-">Select Any Country</option>
                        </select>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <label>Select Any City</label><span class="red" id="lbl_City">*</span>
                    <div class="full-width button-height" id="City">
                        <select id="selCity" name="select90" class="select City full-width">
                            <option selected="selected" value="-">Select Any City</option>
                        </select>
                    </div>
                </div>
                <div class="three-columns twelve-columns-mobile">
                    <br />
                    <button type="button" id="btn_Save" class="button anthracite-gradient" onclick="SaveLocation()">Save</button>
                    <button type="button" id="btn_Update" style="display: none" class="button anthracite-gradient" onclick="UpdateLocation()">Update</button>
                </div>
            </div>



            <div class="respTable">
                <table class="table responsive-table" id="tbl_GetLocation">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Sr.No</th>
                            <th scope="col" class="align-center">Location</th>
                            <th scope="col" class="align-center">City</th>
                            <th scope="col" class="align-center">Country</th>
                            <th scope="col" class="align-center">Actions</th>
                            <%--<th scope="col" class="align-center">Update</th>
                            <th scope="col" class="align-center">Delete</th>--%>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>

    </section>
</asp:Content>
