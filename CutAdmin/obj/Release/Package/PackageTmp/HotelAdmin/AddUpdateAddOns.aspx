﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="AddUpdateAddOns.aspx.cs" Inherits="CutAdmin.HotelAdmin.AddUpdateAddOns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <link href="css/dynamicDiv.css" rel="stylesheet" />
    <script src="../Scripts/AddOns.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Addons Master </h1>
        </hgroup>

        <div class="with-padding">




            <div class="columns">
                <div class="new-row-mobile four-columns five-columns-tablet twelve-columns-mobile">
                    <form>
                        <p class="block-label button-height">
                            <label for="block-label-1" class="label">Name <span class="required red" id="lbl_Name">*</span></label>
                            <input type="text" name="block-label-1" id="txt_Name" class="input full-width" value="">
                        </p>
                        <p class="block-label button-height">
                            <label for="block-label-1" class="label">Discription</label>
                            <input type="text" name="block-label-1" id="txt_Discription" class="input full-width" value="">
                        </p>
                        <p class="block-label button-height"  id="DivGroupmark">
                            <label for="block-label-1" class="label">Type <span class="required red" id="lbl_Type">*</span></label>
                           
                                <select  class="select full-width" id="Sel_Type">
                                <option value="Select Type" selected="selected">Select Type</option>
                                <option value="false" >Genral</option>
                                <option value="true">Meal </option>

                            </select>
                           
                        </p>
                        <p class="button-height">
                            <button type="button" class="button big" onclick="SaveAddOns()" id="btn_Add">Save</button>
                            <input type="reset" class="button big" id="btn_reset" value="Reset" />
                        </p>
                    </form>

                </div>
                <div class="new-row-tablet new-row-mobile eight-columns twelve-columns-tablet">


                    <table class="table responsive-table" id="tbl_AddOns">

                        <thead>
                            <tr>
                                <th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Type</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>

    </section>
</asp:Content>
