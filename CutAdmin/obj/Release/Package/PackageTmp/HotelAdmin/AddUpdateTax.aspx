﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="AddUpdateTax.aspx.cs" Inherits="CutAdmin.HotelAdmin.AddUpdateTax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <link href="css/dynamicDiv.css" rel="stylesheet" />
    <script src="../Scripts/Taxes.js?v=1.1" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Tax Master </h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <form>
                <div class="columns">
                   
                    <%--<div class="new-row-mobile four-columns five-columns-tablet twelve-columns-mobile">
                    <form>
                        <p class="block-label button-height">
                            <label for="block-label-1" class="label">Name <span class="required red" id="lbl_Name">*</span></label>
                            <input type="text" name="block-label-1" id="txt_Name" class="input full-width" value="">
                        </p>
                        <p class="block-label button-height">
                            <label for="block-label-1" class="label">Discription</label>
                            <input type="text" name="block-label-1" id="txt_Discription" class="input full-width" value="">
                        </p>
                        <p class="block-label button-height">
                            <label for="block-label-1" class="label">Value(%) <span class="required red" id="lbl_Value">*</span></label>
                            <input type="text" name="block-label-1" id="txt_Value" class="input full-width" value="">
                        </p>
                        
                        <p class="block-label button-height">
                            <label for="chk_Active" id="lbl_Check" class="label">
                                Active
                                <input type="checkbox" id="chk_Active" class="replacement " /></label>
                        </p>

                        <p class="button-height">
                            <button type="button" class="button big" onclick="AddTax()" id="btn_Add">Save</button>
                            <input type="reset" class="button big" id="btn_reset" value="Reset" />
                        </p>
                    </form>
                </div>--%>


                    <div class="three-columns   twelve-columns-mobile">
                        <label for="block-label-1" class="label">Name</label><span class="red" id="lbl_Name">*</span>
                        <input type="text" name="block-label-1" id="txt_Name" placeholder="Name" class="input full-width" value="">
                    </div>
                    <div class="three-columns two-columns-tablet twelve-columns-mobile">
                        <label for="block-label-1" class="label">Discription</label>
                        <input type="text" name="block-label-1" id="txt_Discription" placeholder="Discription" class="input full-width" value="">
                    </div>

                    <div class="three-columns two-columns-tablet twelve-columns-mobile">
                        <label for="block-label-1" class="label">Value(%)</label><span class="red" id="lbl_Value">*</span>
                        <input type="text" name="block-label-1" id="txt_Value" placeholder="%" class="input full-width" value="">
                    </div>

                    <div class="one-column two-columns-tablet six-columns-mobile" style="padding-top: 1.1%">
                        <br />
                        <label for="chk_Active" id="lbl_Check" class="label">
                            Active
                        <input type="checkbox" id="chk_Active" class="replacement " />
                        </label>
                    </div>

                    <div class="two-columns three-columns-tablet six-columns-mobile SaveReset">
                        <br />
                        <%--<input type="button" class="button anthracite-gradient buttonmrg" id="btn_reset" value="Reset" onclick="Reset()">--%>
                        <input type="reset" class="button anthracite-gradient buttonmrg" id="btn_reset" value="Reset" />

                        <input type="button" class="button anthracite-gradient buttonmrg" value="Save" onclick="AddTax()" id="btn_Add">
                    </div>

                </div>
            </form>
                <%--<div class="new-row-tablet new-row-mobile eight-columns twelve-columns-tablet">--%>
                    <table class="table responsive-table" id="tbl_Tax">
                        <thead>
                            <tr>
                                <th scope="col" class="align-center hide-on-mobile">Sr No</th>
                                <th scope="col" class="align-center">Tax Name</th>
                                <th scope="col" class="align-center">Tax Description</th>
                                <th scope="col" class="align-center">Value(%)</th>
                                <th scope="col" class="align-center">Active</th>
                                <th scope="col" class="align-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                <%--</div>--%>
            <%--</div>--%>
        </div>
    </section>
</asp:Content>
