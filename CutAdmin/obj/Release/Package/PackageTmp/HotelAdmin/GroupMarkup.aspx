﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="GroupMarkup.aspx.cs" Inherits="CutAdmin.HotelAdmin.GroupMarkup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    
    <script src="Scripts/GroupMarkup.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Group Markup</h1>
            <hr />
        </hgroup>

        <div class="with-padding">

            <div class="columns">
                <div class="one-column four-columns-mobile bold">
                    <label>Groups</label></div>
                <div class="four-columns eight-columns-mobile">
                    <div class="full-width Groupmark" id="DivGroupmark">
                        <select  id="SelGroup" onchange="GetGroupMarkup()" name="validation-select" class="select Groupmark">
                        </select>
                    </div>
                </div>
                <div class="two-columns six-columns-mobile bold text-right">
                    <button type="button" onclick="AddGroup()" class="button anthracite-gradient Updategroup">Add Group</button>
                </div>
            </div>


            <div class="standard-tabs margin-bottom" id="add-tabs">

                <div class="tabs-content">
                    <div id="Hotel" class="with-padding">
                        <div class="respTable">
                            <table class="table responsive-table" id="sorting-advanced">

                                <thead>
                                    <tr>
                                        <th scope="col">MarkUp Percentage </th>
                                        <th scope="col" class="align-center hide-on-mobile">MarkUp Amount</th>
                                        <th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage</th>
                                        <th scope="col" class="align-center">Commision Amount</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txt_MarkUpPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txt_MarkUpAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txt_CommisionPercentage" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>
                                        </td>
                                        <td>
                                            <div class="input full-width">
                                                <input name="prompt-value" id="txt_CommisionAmount" value="" class="input-unstyled full-width" placeholder="0%" type="text"></div>
                                        </td>
                                    </tr>
                                   
                                </tbody>

                            </table>
                            <p class="text-right">
                                <button type="button" onclick="Update()" class="button anthracite-gradient UpdateMarkup">Update</button></p>
                        </div>
                    </div>
                   

                </div>

            </div>


        </div>

    </section>

</asp:Content>
