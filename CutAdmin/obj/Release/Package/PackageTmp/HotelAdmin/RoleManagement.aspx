﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="RoleManagement.aspx.cs" Inherits="CutAdmin.HotelAdmin.RoleManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    
    <script src="Scripts/RoleManagement.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Role Management</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="columns">
                <div class="one-column two-columns-mobile">
                    <label>Roles</label>
                </div>
                <div class="ten-columns">
                    <div class="full-width button-height">
                        <select id="selRoles" class="select" onchange="AppendFormTable(this.value)">
                        </select>
                    </div>
                </div>
            </div>
            <div class="respTable">
                <table class="table table-striped table-bordered" id="tblForms" cellspacing="0" cellpadding="0" border="0">
                </table>
            </div>
            <p class="text-alignright">
                <button type="button" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Submit</button>
            </p>
        </div>
    </section>
</asp:Content>
