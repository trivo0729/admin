﻿$(document).ready(function () {
    HotelActivityMails();
})

function HotelActivityMails() {
    $("#tbl_HotelDetails").dataTable().fnClearTable();
    $("#tbl_HotelDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../HotelAdmin/Handler/MailsHandler.asmx/GetHotelActivityMails",
        data: '{ "Type": "Hotel"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var HotelMailsList = result.Arr;
                var tRow = '';

                var Email = '';
                var CcMail = '';
                var ErrorMessage = "";
                var Type = 'Hotel';
                for (var i = 0; i < HotelMailsList.length; i++) {
                    var Name;

                    if (HotelMailsList[i].Email == null || HotelMailsList[i].Email == "") {

                        Email = '-';

                    }
                    else {
                        Email = HotelMailsList[i].Email;

                    }

                    if (HotelMailsList[i].CcMail == null || HotelMailsList[i].CcMail == "") {
                        CcMail = '-';
                    }
                    else {

                        CcMail = HotelMailsList[i].CcMail;
                    }

                    if (HotelMailsList[i].ErroMessage == null) {


                        ErrorMessage = '';
                    }
                    else {
                        ErrorMessage = HotelMailsList[i].ErroMessage;
                    }
                    tRow += '<tr>';
                    //tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#StaffDetailModal" onclick="StaffDetailsModal(\'' + List_BankDetails[i].BankName + '\',\'' + List_BankDetails[i].AccountNo + '\',\'' + List_BankDetails[i].Branch + '\',\'' + List_BankDetails[i].SwiftCode + '\',\'); return false" title="Click to view Bank Details">' + List_BankDetails[i].BankName + '</a></td>';
                    //tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_BankDetails[i].sid + '\',\'' + List_BankDetails[i].uid + '\',\'' + List_BankDetails[i].ContactPerson + '\',\'' + List_BankDetails[i].password + '\'); return false" title="Click to edit Password">' + List_BankDetails[i].AccountNo + '</a></td>';
                    //tRow += '<td>' + VisaMailsList[i].BankName + '</td>';
                    tRow += '<td style="width:20%">' + HotelMailsList[i].Activity + '</td>';

                    tRow += '<td style="width:25%">' + Email + '</td>';
                    //tRow += '<td>' + VisaMailsList[i].Email + '</td>';
                    tRow += '<td style="width:25%">' + CcMail + '</td>';
                    tRow += '<td style="width:20%">' + ErrorMessage + '</td>';

                    tRow += '<td style="width:10%" class="align-center"><a style="cursor:pointer" data-toggle="modal" data-target="#VisaModal"  onclick="VisaModalFunc(\'' + HotelMailsList[i].Activity + '\',\'' + Email + '\',\'' + CcMail + '\',\'' + Type + '\',\'' + ErrorMessage + '\'); return false" title="Click to view hotel details"><span class="icon-publish" title="Update" aria-hidden="true" ></span></a></td>';


                    //tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#HotelModal"><span class="glyphicon glyphicon-plus" title="Add" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true" style="cursor:pointer" onclick="VisaModal(\'' + HotelMailsList[i].sid + '\',\'' + HotelMailsList[i].Activity + '\',\'' + HotelMailsList[i].Email + '\',\'' + HotelMailsList[i].CcMail + '\'); return false" title="Click to add activity"></span></a></td>';

                    //tRow += '<td align="center"><a style="cursor:pointer"  data-toggle="modal" data-target="#VisaModal"  href="AddBankDetails.aspx?sid=' + VisaMailsList[i].sid + '"><span class="glyphicon glyphicon-plus" title="Add" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true" style="cursor:pointer" onclick="DeleteBankID(\'' + VisaMailsList[i].sid + '\',\'' + VisaMailsList[i].Activity + '\')"></span></a></td>';

                    //tRow += '<td align="center"><a style="cursor:pointer" href="AddBankDetails.aspx?sid=' + List_BankDetails[i].sid + '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer" href="#"><span class="' + List_StaffDetails[i].LoginFlag.replace("True", "glyphicon glyphicon-eye-open").replace("False", "glyphicon glyphicon-eye-close") + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + List_StaffDetails[i].LoginFlag.replace("True", "Click To Deactivate").replace("False", "Click To Activate") + '" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp;<a> <span class="glyphicon glyphicon-user" title="Staff Roles" aria-hidden="true" style="cursor:pointer" onclick="GetFormList(\'' + List_StaffDetails[i].sid + '\')"></span></a> |  <a style="cursor:pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaffID(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_HotelDetails tbody").html(tRow);
                $("#tbl_HotelDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_HotelDetails").removeAttribute("style")
            }
        },
        error: function () {
        }
    });
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function AddMails(Act) {
    debugger
    var bValid = true;
    //var Activity = "Booking Error";
    var Activity = Act;
    var Meassage = $('#txt_Error').val();
    //var MailId = "online@clickurtrip.com";
    var MailId = $("#txt_Email").val();
    var Type = "Hotel";
    //if ($("#lst_Visa").hasClass("active") == true) {
    //    Meassage = $("#VisaMessage").val()
    //    Activity = $("#Sel_Activity").val();
    //    MailId = $("#txt_MailId").val();
    //    Type = "Visa"
    //    if (!emailReg.test(MailId)) {
    //        bValid = false;
    //        $("#txt_MailId").focus();
    //        Success("Please Insert Valid Mail Id")
    //    }
    //}
    //else if ($("#lst_Hotel").hasClass("active") == true) {
    //    Meassage = $("#HotelMessage").val()
    //    Activity = $("#Sel_HotelActivity").val();
    //    MailId = $("#txt_HotelMailId").val();
    //    Type = "Hotel"
    //    if (!emailReg.test(MailId)) {
    //        bValid = false;
    //        $("#txt_HotelMailId").focus();
    //        Success("Please Insert Valid Mail Id")
    //    }
    //}
    //else if ($("#lst_Otb").hasClass("active") == true) {
    //    Meassage = $("#OTBMessage").val()
    //    Activity = $("#Sel_AirLineInMail").val();
    //    MailId = $("#txt_AirlinesMailId").val();
    //    Type = "OTB"
    //    if (!emailReg.test(MailId)) {
    //        bValid = false;
    //        $("#txt_AirlinesMailId").focus();
    //        Success("Please Insert Valid Mail Id")
    //    }
    //}
    var CcMails = "";
    var Mails = document.getElementsByClassName('email_name'), i;
    for (i = 0; i < Mails.length; i += 1) {
        if (i != (Mails.length - 1)) {

            if (!emailReg.test(Mails[i].textContent)) {
                bValid = false;
                $(".multiple_emails-input text-left").focus();
                Success("Please Insert Valid Mail Id")
            }
            else {
                if (!emailReg.test(Mails[i].textContent)) {
                    bValid = false;
                    $(".multiple_emails-input text-left").focus();
                    Success("Please Insert Valid Mail Id")
                }
                else {
                    CcMails += Mails[i].textContent + ";";
                }

            }
        }
        else {
            CcMails += Mails[i].textContent;
        }

        var Message = $("#Message")

    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../HotelAdmin/Handler/MailsHandler.asmx/UpdateVisaMails",
            data: '{ "Activity": "' + Activity + '","Type": "' + Type + '","MailsId": "' + MailId + '","CcMails": "' + CcMails + '","BCcMail": "","Message": "' + Meassage + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Mail Updated Successfully..")

                    $(".multiple_emails-ul").empty();
                    // $('#VisaModal').modal('hide');

                    //$("#VisaModal .close").click()
                    if (Type == "Visa") {
                        $("#Sel_Activity").val("-");
                        $("#txt_MailId").val("");
                        VisaActivityMails();
                    }
                    else if (Type == "OTB") {

                        $("#Sel_AirLineInMail").val("-");
                        $("#txt_AirlinesMailId").val("");
                        OTBActivityMails();
                    }
                    else if (Type == "Hotel") {
                        $("#Sel_HotelActivity").val("-");
                        $("#txt_HotelMailId").val("");
                        //HotelActivityMails();
                        window.location.reload();
                    }
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
            }
        });
    }

}
function RemoveLi(link) {
    link.remove();
}
var Act = "";
function VisaModalFunc(Activity, Email, CcMail, type, Msg) {
    debugger;
    $(".multiple_emails-ul").empty();
    Act = Activity;
    var cont = '';
    cont += '<div class="modal-body">'
    cont += '<div class="scrollingDiv">'
    cont += '<div class="columns">'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Activity</label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Activity" value="' + Activity + '" class="input-unstyled full-width"  placeholder="Activity Name" type="text">'
    cont += '</div></div>'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Mail Id</label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Email" value="' + Email + '" class="input-unstyled full-width"  placeholder="Mail Id" type="text">'
    cont += '</div></div>'
    cont += '<div class="twelve-columns twelve-columns-mobile"><label>Error Message</label><br/> <div class="input full-width">'
    cont += '<input name="prompt-value" id="txt_Error" value="' + Msg + '" class="input-unstyled full-width"  placeholder="Error Message" type="text">'
    cont += '</div></div>'
    cont += '<div class="updatemail"><ul  id="CcMailList">'
    if (CcMail != "" && CcMail != "-") {
        var CcArr = CcMail.split(";");
        for (var i = 0; i < (CcArr.length) ; i++) {
            cont += '<li class="multiple_emails-email" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="fa fa-times"></i></a><span class="email_name">' + CcArr[i] + '</span></li>'
        }
        $(".multiple_emails-ul").append(cont)
    }
    cont += '</ul><input type="text" id="inp_CcAddMail" style="border:none; margin-left:10px; margin-bottom:2px" onblur="AddCcMail()" placeholder="Enter Cc Mail here"></div></div><p class="text-right"><button type="button" class="button anthracite-gradient" onclick="AddMails(\'' + Act + '\')">Add</button></p></div></div>'

    $.modal({
        content: cont,
        title: 'Update Mails',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

//function VisaModalFunc(Activity, Email, CcMail, type, Msg) {
//    debugger;


//    $(".multiple_emails-ul").empty();

//    var CCmails = CcMail.split(';')
//    var li = ''
//    if (CCmails != "" && CCmails != "-") {
//        for (var i = 0; i < (CCmails.length) ; i++) {
//            li += '<li class="multiple_emails-email" onclick="RemoveLi(this)"><a style="cursor:pointer"   class="multiple_emails-close" title="Remove"><i class="glyphicon glyphicon-remove"></i></a><span class="email_name" data-email="' + CCmails[i] + '">' + CCmails[i] + '</span></li>'
//        }

//        $(".multiple_emails-ul").append(li)
//        //$("#txt_CCMailId").append(li)
//    }

//    if (type == "Visa") {
//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();
//        $("#header").text("Visa Mails");
//        //var VisaHtml = 'Select Activity :<select id="Sel_Activity" class="form-control" onchange="ActivityMails()"><option selected="selected" value="-">-Select Activity-</option><option>Visa Apply</option><option>Incomplete application</option> <option>Delete Incomplete application</option><option>Documents Required</option><option>Application Approved</option><option>Application Rejected</option><option>Application Cancelled</option><option>Entered Country</option><option>Exit Country</option><option>Stay Limit Expiry</option><option>Visa Expire</option></select><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_MailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">;'

//        var VisaHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_Activity" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_MailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';

//        $("#divForVisa").append(VisaHtml);

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="VisaMessage" class="form-control col-md-10" style="height:60px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#VisaMessage").text(Msg);

//        $("#Sel_Activity").val(Activity);
//        if (Email == "-") {
//            $("#txt_MailId").val("");
//        } else {
//            $("#txt_MailId").val(Email);
//        }
//    }
//    else if (type == "Otb") {

//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();

//        var OTBHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_AirLineInMail" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Name"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_AirlinesMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';
//        $("#header").text("OTB Mails");
//        $("#divForVisa").append(OTBHtml);

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="OTBMessage" class="form-control" style="height:80px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#OTBMessage").text(Msg);

//        $("#Sel_AirLineInMail").val(Activity);
//        if (Email == "-") {
//            $("#txt_AirlinesMailId").val("");
//        } else {
//            $("#txt_AirlinesMailId").val(Email);
//        }
//    }
//    else if (type == "Hotel") {

//        $("#divForVisa").empty();
//        $("#ErrorDiv").empty();
//        $("#header").text("Hotel Mails");
//        //var HotelHtml = 'Select Activity :<select id="Sel_HotelActivity" class="form-control" onchange="ActivityMails()"><option selected="selected" value="-">-Select Activity-</option><option>Booking Confirm</option><option>Booking Error</option><option>Booking On Hold </option><option>Booking Cancelled</option><option>Booking Reconfirm</option><option>Cancelletion Dead Line</option><option>On Hold booking dead line</option><option>List of check in</option><option>Offer Booking Mail</option></select><label style="color: red; margin-top: 3px; display: none" id="lbl_Sel_HotelActivity"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_HotelMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">;'

//        var HotelHtml = 'Activity :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="Sel_HotelActivity" placeholder="Activity" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off" readonly><label style="color: red; margin-top: 3px; display: none" id="lbl_Sel_HotelActivity"><b>* This field is required</b></label><br> Mail Id :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_HotelMailId" placeholder="Mail Id" class="form-control ui-autocomplete-input" data-original-title=" " title=" " autocomplete="off">';
//        $("#divForVisa").append(HotelHtml);


//        $("#Sel_HotelActivity").val(Activity);
//        if (Email == "-") {
//            $("#txt_HotelMailId").val("");
//        }
//        else {
//            $("#txt_HotelMailId").val(Email);
//        }

//        var Error = 'Error Message :<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> <textarea id="HotelMessage" class="form-control" style="height:80px;width:540px"></textarea><label style="color: red; margin-top: 3px; display: none" id="lbl_ErrorMessage"><b>* This field is required</b></label>';
//        $("#ErrorDiv").append(Error);
//        $("#HotelMessage").text(Msg);

//    }


//    //$("#txt_CCMailId").val(CCmails);


//}

function AddCcMail() {
    var NewCcMail = $('#inp_CcAddMail').val();
    if (!NewCcMail) {
        return false;
    }
    if (!emailReg.test(NewCcMail)) {
        bValid = false;
        $("inp_CcAddMail").focus();
        Success("Please Insert Valid Mail Id");
        return false;
    }
    var ccli = '<li class="multiple_emails" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="fa fa-times"></i></a><span class="email_name">' + NewCcMail + '</span></li>'
    $('#CcMailList').append(ccli);
    $('#inp_CcAddMail').val('');
}