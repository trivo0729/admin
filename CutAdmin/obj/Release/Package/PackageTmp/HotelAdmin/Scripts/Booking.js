﻿

$(document).ready(function () {
    //   Validate();
    //BookingList()
});
var arrSupplierBooking = new Array()
var arrBooking = new Array();
function Validate() {
    arrBooking = new Array()
    arrSupplierBooking = new Array();
    var Checkin = $('#txt_Checkin').val();
    var Checkout = $('#txt_Checkout').val();
   // var arrDates = new Array();
    var nationality = $('#sel_ValidNationality option:selected').val();
    var NationalityName = $('#sel_ValidNationality option:selected').text();
    if (nationality == "" || nationality == null) {
        Success("please select valid nationality")
        return false;
    }
    //var room = $('.Sel_Room option:selected').val();
    //if (room == "" || room == null || room == "0") {
    //    alert("please select Room")
    //    return false;
    //}

    var Supplier = $("#tabs .active").text()
    for (var i = 0; i < arrSuppliers.length; i++) {
        if (Supplier == arrSuppliers[i]) {
            for (var r = 0; r < arrRateType.length; r++) {
                var noRooms = 0;
                for (var m = 0; m < Meals.length; m++) {
                    noRooms = $("#sel_" + i + "_" + arrRateType[r].RoomTypeID + "_" + m).val();
                    //  var arrDates = new Array();
                    var arrRate = new Array();
                    var arrDates = new Array();
                    var ListRateID = [];
                    if (noRooms != 0) {
                        var arrRates = $(".Supplier" + i + '_' + arrRateType[r].RoomTypeID + '_' + Meals[m]);
                        if (arrRates.length != 0) {
                            for (var c = 0; c < arrRates.length; c++) {
                                if (arrRates[c].checked) {
                                    if (ListRateID.indexOf(arrRates[c].value.split('_')[2]) === -1) {
                                        ListRateID.push(arrRates[c].value.split('_')[2]);
                                    }
                                    arrDates.push({
                                        Date: arrRates[c].value.split('_')[0],
                                        Rate: { BaseRate: arrRates[c].value.split('_')[1] },
                                        RateID: arrRates[c].value.split('_')[2]
                                    })
                                }
                            }
                        }
                    }
                    if (noRooms != 0 && arrDates.length != 0) {

                      //  for (var n = 0; n < noRooms; n++) {
                        for (var n = 0; n < ListRateID.length; n++)
                       {
                            var arrRateDates = $.grep(arrDates, function (p) { return p.RateID == ListRateID[n]; })
                                        .map(function (p) { return p; })
                            arrRate = { RateID: ListRateID[n], ListDates: arrDates };                                                    
                            if (n == 0)
                            {
                                arrBooking.push({
                                    RoomIndex : n+1,
                                    RateTypeID: arrRateType[r].RoomTypeID,
                                    RateType: arrRateType[r].RoomType,
                                    MealID: m,
                                    MealName: Meals[m],
                                    noRooms: noRooms,
                                    Rate: arrRate,

                                });
                            }
                            else if(ListRateID[n] != arrRate.RateID)
                            {
                                arrBooking.push({
                                    RoomIndex: n+1,
                                    RateTypeID: arrRateType[r].RoomTypeID,
                                    RateType: arrRateType[r].RoomType,
                                    MealID: m,
                                    MealName: Meals[m],
                                    noRooms: noRooms,
                                    Rate: arrRate,

                                });
                            }
                                               
                                                     
                        }                                              
                    }              
                }
            //arrBooking = arrBooking.filter(function (item) {
            //            return item !== arrRateType[r].RoomTypeID
                //})
                //var Item = arrRateType[r].RoomTypeID;
                //remove(arrBooking, Item);

                

            }
            arrSupplierBooking.push({ HotelCode: HotelCode, HotelName: HotelName, Nationality: nationality, NationalityName: NationalityName, Name: Supplier, Details: arrBooking })
           // CheckFreeSale(arrSupplierBooking);
            if (arrDatesList.length == arrDates.length) {
                //  UpdateMappedHotel()


                if (arrBooking.length != 0) {
                    $.ajax({
                        type: "POST",
                        url: "RoomHandler.asmx/BookingRates",
                        data: JSON.stringify({ Supplier: arrSupplierBooking }),
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                window.location.href = "BookingRates.aspx"
                            }
                            else {
                                Success("Something Went Wrong")
                                return false;
                            }
                        }
                    })
                }
            }

            else {
                UpdateMappedHotel()
                //   alert("plz select Dates")
                return false;
            }

        }
    }
}

function remove(arrBooking, Item)
{
    for (var i = arrBooking.length; i--;)
    {

        if (arrBooking[i].RateTypeID === Item && i == 1)
        {
            arrBooking.splice(i, 1);
        }
    }
}

function GetBookingRates() {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/GetBookingRates",
        //data: JSON.stringify({ Supplier: arrSupplierBooking }),
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSupplierBooking = result.Rates;
                $("#lbl_Hotel").text(arrSupplierBooking[0].HotelName)
                $("#lbl_nattionalty").text("Nationality: " + arrSupplierBooking[0].NationalityName)
                GetHotelAddress(arrSupplierBooking[0].HotelCode)
               // GenrateRooms();

            }
            else {
                Success(result.message)
                return false;
            }
        }
    })
}

function GenrateRooms() {
    var html = '';
    var noRooms = 1;
    var no = "";
    for (var i = 0; i < arrSupplierBooking[0].Details.length; i++) {
        for (var r = 0; r < arrSupplierBooking[0].Details[i].noRooms; r++) {
            var RateTypeID = arrSupplierBooking[0].Details[i].RateTypeID;
            var MealID = arrSupplierBooking[0].Details[i].MealID;
          
            html += '<div class="columns">'
          

            html += '<div class="five-columns">'
            html += '<div class="new-row columns">'
            html += '<label class="orange" id="lbl_Room">Room ' + noRooms + ' : ' + arrSupplierBooking[0].Details[i].RateType + '(' + arrSupplierBooking[0].Details[i].MealName + ')' + '</label>'
          //  html += '<input type="button" class="button blue-gradient" style="margin-left:5%" onclick="OpenAddOnsPopUp(\'' + RateTypeID + '\',\'' + arrSupplierBooking[0].HotelCode + '\')" value="AddOns" />'
            html += '</div>'
            html += '</div>'

            html += '<div class="three-columns">'
            html += '<div class="new-row columns">'
            html += 'Adults:<input type="number" class="input full-width" min="1" value="1" onchange="GenratePax(this.id,\'' + 'Adults' + '\',\'' + RateTypeID + '\',\'' + MealID + '\',\'' + noRooms + '\',\'' + arrSupplierBooking[0].Details[i].Rate.RateID + '\')" id="txtAdults_' + RateTypeID + '_' + MealID + '_' + noRooms + '" >'
            html += '</div>'
            html += '</div>'

            html += '<div class="three-columns">'
            html += '<div class="new-row columns">'
            html += 'Childs:<input type="number" class="input full-width" min="0" value="0" onchange="GenratePax(this.id,\'' + 'Childs' + '\',\'' + RateTypeID + '\',\'' + MealID + '\',\'' + noRooms + '\',\'' + arrSupplierBooking[0].Details[i].Rate.RateID + '\')" id="txtChilds_' + RateTypeID + '_' + MealID + '_' + noRooms + '">'
            html += '</div>'
            html += '</div>'
          
            html += '</div>'

            html += '<div class="columns">'
          
            html += '<div class="five-columns" style="margin-top: 15px;">'
            html += '<div class="new-row columns">'

            html += '<div class="three-columns">'            
            html += '<select id="sel_Gender0_' + RateTypeID + '_' + MealID + '_' + noRooms + '" class="select  full-width" >'
            html += '<option value="Mr">Mr.</option>'
            html += '<option value="Mrs">Mrs.</option>'
            html += '</select>'
            html += '</div>'

            html += '<div class="five-columns">'
            html += '<input type="text" class="input full-width FirstN" placeholder="First Name" value="" id="txtFirstName0_' + RateTypeID + '_' + MealID + '_' + noRooms + '">'
            html += '</div>'

            html += '<div class="four-columns">'
            html += '<input type="text" class="input full-width LastN" placeholder="Last Name" value="" id="txtLastName0_' + RateTypeID + '_' + MealID + '_' + noRooms + '">'
            html += '</div>'
            //html += ''
            //html += ''
            //html += ''
            //html += '</div>'

            html += '<div  id="MorePax' + RateTypeID + '_' + MealID + '_' + noRooms + '">'
            html += '</div>'

            html += '<div  id="MorePaxChild' + RateTypeID + '_' + MealID + '_' + noRooms + '">'
            html += '</div>'

         
            html += '<div  class="twelve-columns"  id="AddonsList' + '_' + noRooms + '">'

            html += '</div>'
            html += '<div  class="twelve-columns"  >'
            html += '<fieldset class="fieldset">'
            html += '<legend class="legend">Cancellation Policy</legend>'
            //html += '<label id="lbl_Cancellation' + '_' + RateTypeID + '_' + MealID + '_' + arrSupplierBooking[0].Details[i].Rate.RateID + '_' + noRooms + '">&nbsp;&nbsp;Cancellation:</label>'
            for (var j = 0; j < arrSupplierBooking[0].Details[i].Rate.ListCancel.length; j++) {
                // html += '<tr>';
                html += '<tr  style="background-color:white;"><li class="orange">' + arrSupplierBooking[0].Details[i].Rate.ListCancel[j] + '</li></tr>';
                // html += '</tr>';
            }
            html += '</fieldset>'
            html += '</div>'
            html += '</div>'
            html += '</div>'

            html += '<br/><div class="seven-columns">'
            html += '<details class="details" >'
            html += '<summary>Date Breckups</summary>'
            html += ' <div> '
            html += '<table class="table HotelList" id="tbl_RateList' + '_' + RateTypeID + '_' + MealID + '_' + arrSupplierBooking[0].Details[i].Rate.RateID + '_' + noRooms + '" style="font-size: small;">'
            html += '<thead>';
            html += '<tr>';
            html += '<th style="height:30px;">Dates</th>';
            html += '<th style="height:30px;">Room Occ</th>';
            html += '<th style="height:30px;">Ex Bed</th>';
            html += '<th style="height:30px;">Child</th>';
            html += '<th style="height:30px;">RRate</th>';
            html += '<th style="height:30px;">EBRate</th>';
            html += '<th style="height:30px;">CWB</th>';
            html += '<th style="height:30px;">CNB</th>';
            html += '<th style="height:30px;"></th>';
            html += '<tr>';
            html += '</thead>';
            html += '<tbody>'
            for (var C = 0; C < arrSupplierBooking[0].Details[i].Rate.ListDates.length; C++) {
                html += '<tr>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].Date + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.RoomOccupancy + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.MaxEB + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.MaxCWB + '</td>';
               // html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].TotalPrice + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].Rate.TotalRate + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].EBRate.TotalRate + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].CWBRate.TotalRate + '</td>';
                html += '<td style="background-color:white;height:32px;">' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].CNBRate.TotalRate + '</td>';
                html += '<td style="background-color:white;height:32px;"><span class="orange" style="cursor:pointer" onclick="OpenAddOnsPopUp(\'' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].RateID + '\',\'' + arrSupplierBooking[0].HotelCode + '\',\'' + arrSupplierBooking[0].Details[i].Rate.ListDates[C].Date + '\',\'' + noRooms + '\',\'' + MealID + '\',\'' + RateTypeID + '\')">AddOns</span></td>';
                html += '</tr>';
            }

            html += '</tbody>'
            html += '</table></div>	</details>';
            html += '<br>'
            html += '<br>'
            html += '<div class="twelve-columns">'
            html += '<fieldset class="fieldset">'
            html += '<legend class="legend">Charges & Tax</legend>'
            html += '<p class="button-height inline-label"><label for="validation-required" readonly class="label">Room Total:</label>'
            html += '<input type="text" name="validation-required" style="width: 100px;  text-align: center;" readonly id="txt_RoomTotal' + RateTypeID + '_' + MealID + '_' + noRooms + '" class="input validate[required]" value="' +  numberWithCommas(arrSupplierBooking[0].Details[i].Rate.TotalCharge.BaseRate) + '">'
            /*Tax Breckups*/
            var arrCharges = arrSupplierBooking[0].Details[i].Rate.TotalCharge.Charge;
            for (var c = 0; c < arrCharges.length; c++) {
                html += '<br/><label for="validation-required" readonly class="label">' + arrCharges[c].RateName + ':</label>';
                html += '<input type="text"  readonly  class="input validate[required]" style="width: 100px;  text-align: center;"  id="txt_texRate' + RateTypeID + '_' + MealID + '_' + noRooms + '_'+c+'" value="' + numberWithCommas(arrCharges[c].TotalRate) + '">'
                html += ' <span class="info-spot">'
                html += '<span class="icon-info-round"></span>'
                html += '<span class="info-bubble">'
                for (var t = 0; t < arrCharges[c].TaxOn.length; t++) {
                    html += '<small>' + arrCharges[c].TaxOn[t].TaxName + '(' + arrCharges[c].TaxOn[t].TaxPer + '%):</small> ' + numberWithCommas(arrCharges[c].TaxOn[t].TaxRate) + '<br/>'
                }

                html += '</span>'
                html += '</span>'
            }
            html += '<br/><label for="validation-required" readonly class="label">AddOns Total:</label>'
            html += '<input type="text" name="validation-required" style="width: 100px;  text-align: center;" readonly id="txt_AddOns' + RateTypeID + '_' + MealID + '_' + noRooms + '" class="input validate[required]" value="0.00">'

            html += '<br/><label for="validation-required" readonly class="label">Total:</label><input type="hidden" id="txtHiddenTotal' + RateTypeID + '_' + MealID + '_' + noRooms + '" value="' + arrSupplierBooking[0].Details[i].Rate.TotalCharge.TotalRate + '" />'
            html += '<input type="text" name="validation-required" style="width: 100px;  text-align: center;" readonly id="txt_Total' + RateTypeID + '_' + MealID + '_' + noRooms + '" class="input validate[required]" value="' + numberWithCommas(arrSupplierBooking[0].Details[i].Rate.TotalCharge.TotalRate) + '">'



            html += '</p></fieldset>'
            html += '</div>'
            html += '</div>'
          
            html += '<div class="five-columns">'
            html += '</div>'

            html += '<div class="seven-columns">'
          
            html += '</div>'

           

            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<br/><br/>'
             noRooms++;
        }
        
    }
   
    html += '<div class="one-columns " >'
    html += '<input type="button" class="button blue-gradient right" onclick="CheckAvailCredit()" value="Book" />'
    html += ' </div> '
    $("#div_Rooms").append(html);
    

}
function numberWithCommas(x) {
    x = x.toString();
    // get stuff before the dot
    var d = x.indexOf('.');
    var s2 = d === -1 ? x : x.slice(0, d);

    // insert commas every 3 digits from the right
    for (var i = s2.length - 3; i > 0; i -= 3)
        s2 = s2.slice(0, i) + ',' + s2.slice(i);

    // append fractional part
    if (d !== -1)
        s2 += x.slice(d);
    return s2;

}

function GatDates(arrDates, RateTypeID, MealID, noRoom) {
    var Dates = "";
    //if (arrDates.length != 0)
    //{
    //    Dates = arrDates[0].Date + " / " + arrDates[arrDates.length - 1].Date;
    //}
    Dates += '<select id="sel_Date_' + RateTypeID + '_' + MealID + '_' + noRoom + '" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width" multiple>'
    for (var i = 0; i < arrDates.length; i++) {

        Dates += '<option value="' + arrDates[i].Rate + '" selected="selected">' + arrDates[i].Date + ' (' + arrDates[i].Rate + ' )</option>'
    }
    Dates += '</select>'

    return Dates;
}

var Totalfare = 0;
var Amount = new Array();
var newTotal = 0;
function GenratePax(ID, PaxType, RateTypeID, MealID, noRoom, RateID) {

    var AdultCount = $("#txtAdults_" + RateTypeID + '_' + MealID + '_' + noRoom + "").val();
    var ChildCount = $("#txtChilds_" + RateTypeID + '_' + MealID + '_' + noRoom + "").val();
    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/GetRoomOccupancy",
        data: JSON.stringify({ AdultCount: AdultCount, ChildCount: ChildCount, RateTypeID: RateTypeID, MealID: MealID, RateID: RateID }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Rate = result.rate;
                var html = '';
                var Total = 0;
                Amount = 0;
                $("#tbl_RateList" + '_' + RateTypeID + '_' + MealID + '_' + RateID + '_' + noRoom + " tbody tr").remove();
                var tr = '';
                for (var k = 0; k < Rate.ListDates.length; k++) {
                    tr += '<tr>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.ListDates[k].Date + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.RoomOccupancy + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.MaxEB + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.MaxCWB + '</td>';
                   // tr += '<td style="background-color:white;height:32px;">' + Rate.TotalPrice + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.ListDates[k].Rate.TotalRate + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.ListDates[k].EBRate.TotalRate + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.ListDates[k].CWBRate.TotalRate + '</td>';
                    tr += '<td style="background-color:white;height:32px;">' + Rate.ListDates[k].CNBRate.TotalRate + '</td>';
                    tr += '<td style="background-color:white;height:32px;"><span class="orange" style="cursor:pointer" onclick="OpenAddOnsPopUp(\'' + RateTypeID + '\',\'' + arrSupplierBooking[0].HotelCode + '\',\'' + Rate.ListDates[k].Date + '\',\'' + noRoom + '\',\'' + MealID + '\',\'' + RateTypeID + '\')">AddOns</span></td>';

                    //tr += '<td style="background-color:white;height:32px;">' + Rate.EBRate + '</td>';
                    //tr += '<td style="background-color:white;height:32px;">' + Rate.CWBRate + '</td>';
                    //tr += '<td style="background-color:white;height:32px;">' + Rate.CNBRate + '</td>';
                    tr += '</tr>';

                   // Total = Total + Rate.ListDates[k].TotalPrice;
                    //Rate.ListDates[k].TotalPrice = Total;
                    //Rate.TotalPrice = Total;
                }
                for (var j = 0; j < Rate.ListDates.length; j++) {
                    Total = Total + Rate.ListDates[j].TotalPrice;
                }
                
               // Rate.TotalPrice = Total;
                //Amount.push(Total);
                //Totalfare = Totalfare + Total;
                $("#tbl_RateList" + '_' + RateTypeID + '_' + MealID + '_' + RateID + '_' + noRoom + " tbody").append(tr);
                $("#txt_RoomTotal" + RateTypeID + '_' + MealID + '_' + noRoom).val(numberWithCommas(Rate.TotalCharge.BaseRate));
                $("#txt_Total" + RateTypeID + '_' + MealID + '_' + noRoom).val(numberWithCommas(Rate.TotalCharge.TotalRate + parseFloat($("#txt_AddOns" + RateTypeID + '_' + MealID + '_' + noRoom).val())));
                for (var t = 0; t < Rate.TotalCharge.Charge.length; t++) {
                    $("#txt_texRate" + RateTypeID + '_' + MealID + '_' + noRoom+ '_' +t ).val(numberWithCommas( Rate.TotalCharge.Charge[t].TotalRate))
                }
                if (PaxType == "Adults") {
                    $("#MorePax" + RateTypeID + "_" + MealID + "_" + noRoom).empty();
                    for (var i = 1; i < $("#" + ID).val() ; i++) {
                        html += '<div class="new-row columns">'
                        html += ''
                        html += '<div class="three-columns">'
                        html += ''
                        html += '<select id="sel_Gender' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '" class="select full-width">'
                        html += '<option value="Mr">Mr</option>'
                        html += '<option value="Mrs">Mrs</option>'
                        html += '</select>'
                        html += '</div>'
                        html += '<div class="four-columns">'
                        html += ''
                        html += '<input type="text" class="input full-width FirstN" placeholder="First Name" value="" id="txtFirstName' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '">'
                        html += '</div>'
                        html += '<div class="four-columns">'
                        html += ''
                        html += '<input type="text" class="input full-width LastN" placeholder="Last Name" value="" id="txtLastName' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '">'
                        html += '</div>'
                        html += '</div>'
                    }
                    $("#MorePax" + RateTypeID + "_" + MealID + "_" + noRoom).append(html);
                }
                else if (PaxType == "Childs") {
                    $("#MorePaxChild" + RateTypeID + "_" + MealID + "_" + noRoom).empty();
                    for (var i = 0; i < $("#" + ID).val() ; i++) {
                        html += '<div class="new-row columns">'
                        html += ''
                        html += '<div class="three-columns">'
                        html += ''
                        html += '<select id="sel_childGender' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '" class="select  full-width">'
                        html += '<option value="Master">Master</option>'
                        html += '<option value="Miss">Miss</option>'
                        html += '</select>'
                        html += '</div>'
                        html += '<div class="four-columns">'
                        html += ''
                        html += '<input type="text" class="input full-width FirstNC" placeholder="First Name" value="" id="txtCHFirstName' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '">'
                        html += '</div>'
                        html += '<div class="four-columns">'
                        html += ''
                        html += '<input type="number" class="input full-width Age" placeholder="Child Age" min="1"  max="9" id="txtAge' + i + '_' + RateTypeID + '_' + MealID + '_' + noRoom + '">'
                        html += '</div>'
                        html += ''
                        html += ''
                        html += ''
                        html += '</div>'
                    }
                    $("#MorePaxChild" + RateTypeID + "_" + MealID + "_" + noRoom).append(html);
                }
            }
        
            else {
                $("#" + ID).val($("#" + ID).val() - 1)
                Success(result.message)
                return false;
            }
            //Amount.push(Total);
            //Totalfare = Totalfare + Total;
        }
    })
}

var arrHotelAddress;
function GetHotelAddress(HotelCode) {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/GetHotelAddress",
        data: JSON.stringify({ HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelAddress = result.HotelAddress;
                $("#lbl_address").text("Address: " + arrHotelAddress.HotelAddress)
                GenrateRooms();
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

function UpdateMappedHotel() {
    if (arrBooking.length == 0) {
        Success("please select Rooms and Dates")
        return false;
    }
    $.modal({
        content:
				    ' <div class="columns" style="padding-left: 10px">' +
                    'your search of checkIn,checkout and what you have selected' + ' <br>' + ' not same do you want to continue? ' +
                      '  </div>' +
                      '<div class="columns">' +
				'<div class="two-columns">' +

                '    <button type="button" style="margin-left: 10px;margin-top: 15px" class="button glossy blue-gradient" onclick="Book()">Yes</button>' +
                '  </div>' +
                 '<div class="two-columns">',

        title: 'Booking',
        width: 400,
        height: 100,
        scrolling: true,
        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        // buttonsLowPadding: true
    });

}
function CloseModel() {
    $.modal({

        actions: {
            'No': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },


    });
}


function CheckAvailCredit() {
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/ValidateTransaction",
        data: JSON.stringify({ arrAdons: arrAddOns }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Booking()
            }
            else {
                Success(result.ErrorMsg);
                return false;
            }
        }
    })
}

function Book() {
    if (arrBooking.length != 0) {
        $.ajax({
            type: "POST",
            url: "RoomHandler.asmx/BookingRates",
            data: JSON.stringify({ Supplier: arrSupplierBooking }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    window.location.href = "BookingRates.aspx"
                    
                }
                else {
                    Success("Something Went Wrong")
                    return false;
                }
            }
        })
    }
}

function Booking() {
    var AdultCount;
    var ChildCount
    for (var i = 0; i < arrSupplierBooking[0].Details.length; i++)
    {
        var Custumer = new Array();
        var SelTitle = new Array();
        for (var r = 0; r < arrSupplierBooking[0].Details[i].noRooms; r++) {
            var RateTypeID = arrSupplierBooking[0].Details[i].RateTypeID;
            var MealID = arrSupplierBooking[0].Details[i].MealID;
            AdultCount = $("#txtAdults_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val();
            ChildCount = $("#txtChilds_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val();
         
             for (var ad = 0; ad < AdultCount; ad++)
             {
                var arrCustumer = {
                    Age:30,
                    type :"AD",
                    LastName: $("#txtLastName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val(),
                    Name: $("#txtFirstName" + ad + "_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val(),
                    Title: $("#sel_Gender" + ad + "_" + RateTypeID + "_" + MealID + "_" + (i + 1) + " option:selected").val(),
                    RoomNo: r + 1,
                }
                Custumer.push(arrCustumer);
            }
            for (var cd = 0; cd < ChildCount; cd++) {
                var arrCustumer = {
                    Age: $("#txtAge" + cd + "_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val(),
                    type: "CH",
                    Name: $("#txtCHFirstName" + cd + "_" + RateTypeID + "_" + MealID + "_" + (i + 1)).val(),
                    Title: $("#sel_childGender" + cd + "_" + RateTypeID + "_" + MealID + "_" + (i + 1) + " option:selected").val(),
                    RoomNo: r + 1,
                }
                Custumer.push(arrCustumer);
            }
            arrSupplierBooking[0].Details[i].Rate.LisCustumer = Custumer;

        }
        // arrSupplierBooking[0].Details[i].Rate.LisCustumer = Custumer;
        //if (Custumer[i].Name == "" || Custumer[i].Name == null)
        //{
        //    alert("Please Enter Name");
        //    return false;
        //}
        //if (Custumer[i].LastName == "" || Custumer[i].LastName == null)
        //{
        //    alert("Please Enter Last Name");
        //    return false;
        //}
    }
    var arrtTotal = [];
    for (var i = 0; i < $(".Totall").length; i++) {
        if ($('.Totall')[i].outerText != "")
            arrtTotal.push($('.Totall')[i].outerText);
    }
    
    if (arrSupplierBooking.length != 0) {
       
        $.ajax({
            type: "POST",
            url: "handler/BookingHandler.asmx/Booking",
            data: JSON.stringify({ Supplier: arrSupplierBooking, Total: Totalfare, Amount: arrtTotal, arrAddOns: arrAddOns }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                //alert("Booking Success");
               // window.location.href = "RoomRates.aspx"
                if (result.retCode == 1) {
                    Success("Booking Success");
                    setTimeout(function () {
                        window.location.href = "BookingList.aspx";
                    }, 2000)
           
                    //UpdateInventory();
                }
                else {
                //    alert("Something Went Wrong")
                    return false;
                }
            }
        })
    }
}



function GetPrintInvoice(ReservationID, Uid, Status, Type) {
    //if (Status == 'Vouchered' || Status == 'Cancelled') {
    var win = window.open('ViewInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '&Supplier=' + Type, '_blank');
}

function GetPrintVoucher(ReservationID, Uid, Latitude, Longitude, Status, Type) {
    debugger;
    if (Status == 'Vouchered') {

        var win = window.open('ViewVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
        //window.open('../Agent/Voucher.html?ReservationId=' + ReservationID + '&Status=' + Status + '&Uid=' + Uid + '&Latitude=' + Latitude + '&Longitutude=' + Longitude + '&Type=' + Type, 'tester', 'left=50000,top=50000,width=800,height=600');
    }
    else if (Status == 'Booking') {
        Success('Please Confirm Your Booking!')
    }
    else
        Success('You cannot get voucher for cancelled booking!')
}

function UpdateInventory() {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/UpdateInventory",
        data: JSON.stringify({ Supplier: arrSupplierBooking }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrSupplierBooking = result.Rates;
            }
            else {
                Success(result.message)
                return false;
            }
        }
    })
}

function ValidSale(cvalue) {
    arrBooking = new Array();
    arrFreeSale = new Array();
    arrSupplierBooking = new Array();
    var Supplier = $("#tabs .active").text()
    for (var i = 0; i < arrSuppliers.length; i++) {
        if (Supplier == arrSuppliers[i]) {
            for (var r = 0; r < arrRateType.length; r++) {
                var noRooms = 1;
                for (var m = 0; m < Meals.length; m++) {
                  //  noRooms = $("#sel_" + i + "_" + arrRateType[r].RoomTypeID + "_" + m).val();
                    //  var arrDates = new Array();
                    var arrRate = new Array();
                    var arrDates = new Array();
                    var ListRateID = [];
                    if (noRooms == 1) {
                        var arrRates = $(".Supplier" + i + '_' + arrRateType[r].RoomTypeID + '_' + Meals[m]);
                        if (arrRates.length != 0) {
                            for (var c = 0; c < arrRates.length; c++) {
                                if (arrRates[c].checked) {
                                    if (ListRateID.indexOf(arrRates[c].value.split('_')[2]) === -1) {
                                        ListRateID.push(arrRates[c].value.split('_')[2]);
                                    }
                                    arrDates.push({ Date: arrRates[c].value.split('_')[0], Rate: arrRates[c].value.split('_')[1], RateID: arrRates[c].value.split('_')[2] })
                                }
                            }
                        }
                    }
                    //else {
                    //    alert("please select Room!!");
                    //}
                    if (noRooms == 1 && arrDates.length != 0) {

                        for (var n = 0; n < ListRateID.length; n++) {
                            var arrRateDates = $.grep(arrDates, function (p) { return p.RateID == ListRateID[n]; })
                                        .map(function (p) { return p; })
                            arrRate = { RateID: ListRateID[n], ListDates: arrDates };

                            arrBooking.push({
                                RateTypeID: arrRateType[r].RoomTypeID,
                                RateType: arrRateType[r].RoomType,
                                MealID: m,
                                MealName: Meals[m],
                                //noRooms: noRooms,
                                Rate: arrRate,

                            });
                        }
                        
                    }
                }
            }
        }
    }
    arrSupplierBooking.push({ HotelCode: HotelCode, HotelName: HotelName, Name: Supplier, Details: arrBooking })


    //var RoomType = "";
    //for (var i = 0; i < arrBooking.length; i++)
    //{
    //    RoomType = arrBooking[i].RateType;
    //}


    if (cvalue == "Start Sale") {
        $.modal({
            content:
                        ' <div class="columns" style="padding-left: 10px">' +
                        'Do You Want to Start Sale' + ' <br>' +
                          '  </div>' +
                          '<div class="columns">' +
                    '<div class="two-columns">' +

                    '    <button type="button" style="margin-left: 10px;margin-top: 15px" class="button glossy blue-gradient" onclick="StartSale()">Yes</button>' +
                    '  </div>' +
                     '<div class="two-columns">',

            title: 'Start Sale',
            width: 400,
            height: 100,
            scrolling: true,
            actions: {
                'No': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },

            // buttonsLowPadding: true
        });
    }
    else {
        $.modal({
            content:
                        ' <div class="columns" style="padding-left: 10px">' +
                       'Do You Want to Stop Sale' + ' <br>' +
                          '  </div>' +
                          '<div class="columns">' +
                    '<div class="two-columns">' +

                    '    <button type="button" style="margin-left: 10px;margin-top: 15px" class="button glossy blue-gradient" onclick="StopSale()">Yes</button>' +
                    '  </div>' +
                     '<div class="two-columns">',

            title: 'Stop Sale',
            width: 400,
            height: 100,
            scrolling: true,
            actions: {
                'No': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },

            // buttonsLowPadding: true
        });

    }
}

function StartSale() {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/StartSale",
        data: JSON.stringify({ Supplier: arrSupplierBooking }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrSupplierBooking = result.Inventory;
                Success(result.message);
                SearchRates();
               // window.location.href = "RoomRates.aspx";
            }
            else {
                Success(result.message)
                return false;
            }
        }
    })
}

function StopSale() {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/StopSale",
        data: JSON.stringify({ Supplier: arrSupplierBooking }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrSupplierBooking = result.Inventory;
                Success(result.message);
                SearchRates();
            }
            else {
                Success(result.message)
                return false;
            }
        }
    })
}


function CheckFreeSale() {

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/CheckFreeSale",
        data: JSON.stringify({ Supplier: arrSupplierBooking }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrSupplierBooking = result.Inventory;
                Success(result.message)
            }
            else {
                Success(result.message)
                return false;
            }
        }
    })
}