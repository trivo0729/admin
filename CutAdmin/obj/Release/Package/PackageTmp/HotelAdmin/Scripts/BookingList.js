﻿$(document).ready(function () {
    var Status = getParameterByName('Status');
    var type = getParameterByName('Type');
    BookingListAll();
    if (type != "") {
        BookingListFilter(Status, type);
    }

    $("#Check-In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Check-Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var BookingList;
var SupplierList;
function BookingListAll() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    // $("#tbl_BookingList tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/BookingList",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                SupplierList = result.SupplierList;
                htmlGenrator();
                GetBookingListSupplier(SupplierList);
            }
            else {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function BookingListFilter(status, type) {

    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var data = {
        status: status,
        type: type
    }
    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/BookingListFilter",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                // BookingList = new Array();
                BookingList = result.BookingList;
                htmlGenrator();
            }
            else {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        }
    })
}

function Search() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();

    var Agency = $("#txt_Agency").val();
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var SupplierRef = $("#txt_Supplier").val();
    var Supplier = $("#selSupplier option:selected").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();

    var data = {
        Agency: Agency,
        CheckIn: CheckIn,
        CheckOut: CheckOut,
        Passenger: Passenger,
        BookingDate: BookingDate,
        Reference: Reference,
        SupplierRef: SupplierRef,
        Supplier: Supplier,
        HotelName: HotelName,
        Location: Location,
        ReservationStatus: ReservationStatus
    }

    $.ajax({
        type: "POST",
        url: "handler/BookingHandler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                BookingList = result.BookingList;
                htmlGenrator();

            }
            else if (result.retCode == 0) {
                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.")
        }
    });
}

function Reset() {
    $("#txt_Agency").val('');
    $("#Check-In").val('');
    $("#Check-Out").val('');
    $("#txt_Passenger").val('');
    $("#Bookingdate").val('');
    $("#txt_Reference").val('');
    $("#txt_Supplier").val('');
    $("#selSupplier").val('All');
    $("#txt_Hotel").val('');
    $("#txt_Location").val('');
    $("#selReservation").val('All');
}

function htmlGenrator() {
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++) {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;

        trow += '<tr>';
        trow += '<td class="align-center" style="width:3%">' + (i + 1) + '</td>';
        trow += '<td class="align-center" style="width:10%">' + BookingList[i].ReservationDate + ' </td>';

        if (BookingList[i].Status == 'Cancelled') {
            trow += '<td style="width:7%"><a style="cursor:pointer" title="Confirm" onclick="Success(\'This booking is cancelled.\');">' + BookingList[i].ReservationID + '</a></td>';
        } else {
            trow += '<td class="align-center" style="width:10%"><a style="cursor:pointer" title="Confirm" onclick="ConfirmHoldBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a></td>';
            //trow += '<td>' + BookingList[i].ReservationID + ' </td>';
        }

        trow += '<td class="align-center" style="width:10%">' + BookingList[i].AgencyName + '</td>';
        trow += '<td class="align-center" style="width:10%">' + BookingList[i].Name + '</td>';
        trow += '<td class="align-center" style="width:12%">' + BookingList[i].HotelName + ', ' + BookingList[i].City + ' </td>';
        trow += '<td class="align-center" style="width:11%">' + BookingList[i].CheckIn + ' </td>';
        trow += '<td class="align-center" style="width:11%">' + BookingList[i].CheckOut + ' </td>';
        trow += '<td class="align-center" style="width:4%">' + BookingList[i].TotalRooms + ' </td>';
        trow += '<td class="align-center" style="width:10%">' + BookingList[i].Status + ' </td>';
        trow += '<td class="align-center" style="width:7%"><span><i class="fa fa-inr"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        trow += '<td class="align-center" style="width:1%"><a style="cursor:pointer" title="Invoice"  onclick="GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">View</a> </td>';
        trow += '<td class="align-center" style="width:1%"><a style="cursor:pointer" title="Voucher"  onclick="GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">View</a>  </td>';
        trow += '</tr>';
    }
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()

    $("#tbl_BookingList").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $('#tbl_BookingList').removeAttr("style");
}

function GetSupplier() {
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetSupplier",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Supplier = obj.Arr;
            var ul = '';
            $("#CustomerDetails").empty();
            if (obj.retCode == 1) {
                if (Supplier.length > 0) {
                    $("#selSupplier").empty();
                    var ddlRequest = '<option selected="selected">All</option>';
                    for (i = 0; i < Supplier.length; i++) {
                        ddlRequest += '<option value="' + Supplier[i].AgencyName + '">' + Supplier[i].AgencyName + '</option>';
                    }
                    $("#selSupplier").append(ddlRequest);
                }
            }
            else {

            }
        }
    });
}


function GetBookingListSupplier(SupplierList) {

    var data = {
        SupplierList: SupplierList
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetBookingListSupplier",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            debugger;
            Supplier = obj.Arr;
            var ul = '';
            $("#CustomerDetails").empty();
            if (obj.retCode == 1) {
                if (Supplier.length > 0) {
                    $("#selSupplier").empty();
                    var ddlRequest = '<option selected="selected">All</option>';
                    for (i = 0; i < Supplier.length; i++) {
                        ddlRequest += '<option value="' + Supplier[i].AgencyName + '">' + Supplier[i].AgencyName + '</option>';
                    }
                    $("#selSupplier").append(ddlRequest);
                }
            }
            else {

            }
        }
    });
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {
    //OpenCancellationPopup(ReservationID, Status);
    $("#hdn_Supplier").val(Source);
    //$("#hdn_AffiliateCode").val(AffilateCode);
    OpenHoldPopup(ReservationID, Status);

    //$("#hdn_HoldDate").val(HoldDate);
    //$("#hdn_DeadLineDate").val(DeadLineDate);
    //$('#ConfirmAlertForOnHoldModel').modal('show');
}

function ConfirmHoldBooking(ReservationID, Uid, Status, Source) {
    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "Handler/BookingHandler.asmx/GetDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;

                $.modal({
                    content:

                  '<div class="modal-body">' +
                  '' +
                  '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                  '<tr>' +
                  '<h4>Booking Detail</h4>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].Name + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +


                  '<table class="table table-hover table-responsive" style="width: 100%">' +
                   '<tr>' +
                  '<h4>Re-Confirmation Detail</h4>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +

                  '<tr>' +
                  '<td>' +
                  '<span class="text-left">Staff Name :&nbsp;&nbsp; <input type="text" id="StaffName" class="input" > </span> ' +
                  '' +
                  '</td>' +
                   '<td>' +
                  '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>&nbsp;&nbsp;' +
                  '' +
                  '</td>' +
                  '</tr>' +
                  '</table>' +

                  '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                  '<tr>' +
                    '<td style="border-bottom: none;" >' +
                  '<span class="text-left">Comment :  <input type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                   '' +
                  '</td>' +
                  '</tr>' +
                   '</table>' +

                   '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button red-gradient" style="width: 20%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                  '</div>',
                    title: 'Re-confirm Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'No': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }

                    },

                    // buttonsLowPadding: true

                });

                $("#ConfirmDate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    //onSelect: insertDepartureDate,
                    //minDate: "dateToday",
                    //maxDate: "+3M +10D"
                });
            }
            else if (result.retCode == 0) {
                $('#SpnMessege').text('Something Went Wrong');
                $('#ModelMessege').modal('show')
                // alert("error occured while getting cancellation details")
            }
        },
        error: function (xhr, status, error) {
            $('#SpnMessege').text("Getting Error");
            $('#ModelMessege').modal('show')
            // alert("Error on cancellation popup:" + " " + xhr.readyState + " " + xhr.status);
        }
    });
}

function SaveConfirmDetail(ReservationID, status, HotelName) {
    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").val();
    if (ReconfirmThrough == "-") {
        Success('Please Select Reconfirm Through.');
        return false;
    }

    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,
    }

    $.ajax({
        type: "POST",
        url: "BookingHandler.asmx/SaveConfirmDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Confirm Detail Save.");
                $("#ConfirmDate").val('');
                $("#HotelConfirmationNo").val('');
                $("#StaffName").val('');
                $("#ReconfirmThrough option:selected").val('-');
                $("#Comment").val('');
                window.location.href = "BookingList.aspx";
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });


}

function ExportBookingDetailsToExcel(Document) {
    debugger;

    var Agency = $("#txt_Agency").val();
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var SupplierRef = $("#txt_Supplier").val();
    var Supplier = $("#selSupplier option:selected").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();
    var Type = "All";
    if (Agency == "" && CheckIn == "" && CheckOut == "" && Passenger == "" && BookingDate == "" && Reference == "" && SupplierRef == "" && Supplier == "All" && HotelName == "" && Location == "" && ReservationStatus =="All") {
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AllBookingDetails&Type=" + Type + "&Document=" + Document;
    }
    else {
        Type = "Search";
        window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AllBookingDetails&Type=" + Type + "&Document=" + Document;
    }
}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}