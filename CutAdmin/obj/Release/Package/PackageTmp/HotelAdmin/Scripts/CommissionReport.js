﻿
$(document).ready(function () {
    GetAgentDetail();

});

function GetAgentDetail() {
    $("#tbl_SupplierDetails").dataTable().fnClearTable();
    $("#tbl_SupplierDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/GetAgentDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                var div = "";
                for (var i = 0; i < List_AgentDetails.length; i++)
                {
                    div += '<option value="' + List_AgentDetails[i].sid + '">' + List_AgentDetails[i].AgencyName + '</option>'
                }

                $("#selSupplier").append(div);
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

function GetCommissionReport() {
    var Id = $("#selSupplier option:selected").val();
    if (Id == "" || Id == "-") {
        Success("Please Select Supplier.");
        return false;
    }

    $("#tbl_SupplierDetails").dataTable().fnClearTable();
    $("#tbl_SupplierDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/CommissionReportHandler.asmx/GetCommissionReport",
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            $("#UnPaidAmt").empty();
            if (result.retCode == 1) {
                var List_Details = result.Arr;
                var div = "";
                var Amount = 0;
                var Currency = "";
                for (var i = 0; i < List_Details.length; i++)
                {
                    Amount += List_Details[i].UnPaidAmunt;
                    Currency = List_Details[i].CurrencyCode;
                    div += '<tr>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].AgencyName + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].InsertDate + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].Commission + '</td>';
                    div += '<td class="align-center hide-on-mobile">' + List_Details[i].UnPaidAmunt + '</td>';
                    div += '<td class="align-center hide-on-mobile"><a href="#" class="button" title="Edit" onclick="UpdateModal(\'' + List_Details[i].ID + '\',\'' + List_Details[i].SupplierID + '\',\'' + List_Details[i].Commission + '\',\'' + List_Details[i].UnPaidAmunt + '\')"><span class="icon-pencil"></span></a> </td>';
                }
                var AmountCurrency = Currency + " " + Amount;
                $("#UnPaidAmt").text(AmountCurrency);
                $("#tbl_SupplierDetails").append(div);
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_SupplierDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                $('#tbl_SupplierDetails').removeAttr("style");
            }
        },
        error: function () {
        }

    });
}

var UnPaidAmount = 0;

function UpdateModal(ID, SupplierID, Commission, UnPaidAmunt) {
    UnPaidAmount = UnPaidAmunt;
    $.modal({
        content: '<div class="modal-body">' +
          '<div class="scrollingDiv">' +
          '<div class="columns">' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Commission Amount</label>' +
          '<div class="input full-width">' +
          '<input class="input-unstyled full-width" value="' + Commission + '" readonly="readonly" type="text" id="txt_CommissionAmount">' +
          '</div>' +
          '</div>' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Un-Paid Amount</label>' +
          '<div class="input full-width">' +
          '<input  class="input-unstyled full-width" value="' + UnPaidAmunt + '" readonly="readonly"  type="text" id="txt_Un-PaidAmount">' +
          '</div>' +
          '</div>' +
          '<div class="twelve-columns three-columns-mobile">' +
          '<label>Paid Amount</label><span class="red" id="lbl_PaidAmount">*</span>' +
          '<div class="input full-width">' +
          '<input value="" class="input-unstyled full-width" type="text" id="txt_PaidAmount" onkeyup="Calculate()">' +
          '</div>' +
          '</div>' +
          '</div></div>' +
          '<p class="text-alignright"><a style="cursor:pointer" href="#" class="button compact anthracite-gradient editpro" onclick="UpdateCommission(\'' + ID + '\',\'' + SupplierID + '\',\'' + Commission + '\',\'' + UnPaidAmount + '\')">Save</a></p>' +
          '</div>',
        //onclick="UpdateCommission(\'' + SupplierID + '\',\'' + Commission + '\',\'' + UnPaidAmunt + '\')"
        title: 'Update Commission',
        width: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function UpdateCommission(ID, SupplierID, Commission) {
    var PaidAmunt = $("#txt_PaidAmount").val();
    var UnPaidAmunt = $("#txt_Un-PaidAmount").val();
    var data = {
        ID: ID,
        SupplierID: SupplierID,
        Commission: Commission,
        UnPaidAmunt: UnPaidAmunt,
        PaidAmunt: PaidAmunt
    }
    $.ajax({
        type: "POST",
        url: "handler/CommissionReportHandler.asmx/UpdateCommission",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                Success("Updated Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                Success("Something Went Wrong");
                window.location.reload();
            }
        }
    });

}


function Calculate() {
    var reg = new RegExp('[0-9]$');
    if ($('#txt_PaidAmount').val() != "") {
        if (!(reg.test($('#txt_PaidAmount').val()))) {
            Success("Only numeric values is allowed");
            return false;
        }
        var PaidAmount = parseInt($('#txt_PaidAmount').val(), 10);
    }
    if (PaidAmount != undefined && PaidAmount <= UnPaidAmount) {
        var Total = PaidAmount - UnPaidAmount;
        $("#txt_Un-PaidAmount").val(Math.abs(Total));
    }
    else if (PaidAmount != undefined && PaidAmount >= UnPaidAmount) {
        Success("Paid Amount Should Not be greater than UnPaid Amount");
        $('#txt_PaidAmount').val("");
        $("#txt_Un-PaidAmount").val(UnPaidAmount);
    }
    else {
        $("#txt_Un-PaidAmount").val(UnPaidAmount);
    }

}