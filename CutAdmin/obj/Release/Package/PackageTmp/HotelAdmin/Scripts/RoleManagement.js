﻿var arRoleList = new Array();
var arFormList = new Array();
var arFormListForRole = new Array();
var arrayToSubmit = new Array();
var sSelectedRoleValue
$(document).ready(function () {
    //Get Roles
    $.ajax({
        type: "POST",
        url: "Handler/RoleManagementHandler.asmx/GetRoleList",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arRoleList = result.Arr;
                if (arRoleList.length > 0) {
                    var opRoles = '<option value="null">-- Select Role --</option>';
                    for (i = 0; i < arRoleList.length; i++) {
                        opRoles += '<option  value="' + arRoleList[i].sRoleName + '">' + arRoleList[i].sRoleName + '</option>';
                    }
                    $("#selRoles").append(opRoles);
                }
            }
        },
        error: function () {
            Success("An error occured while geting role list");
        }
    });

    //Get Forms
    $.ajax({
        type: "POST",
        url: "Handler/RoleManagementHandler.asmx/GetFormList",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.Arr;
                if (arFormList.length > 0) {
                    var trForms = '<tbody>';
                    for (i = 0; i < arFormList.length; i = i + 4) {
                        if (i < arFormList.length) {
                            trForms += '<tr>';
                            trForms += '<td><input id="chk' + arFormList[i].sFormName + '" class="checkbox mid-margin-left chkFrm" type="checkbox" value="' + arFormList[i].sFormName + '"/> ' + arFormList[i].sDisplayName + '</td>';
                            if ((i + 1) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 1].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 1].sFormName + '"/> ' + arFormList[i + 1].sDisplayName + '</td>';
                            else
                                trForms += '<td></td>';
                            if ((i + 2) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 2].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 2].sFormName + '"/> ' + arFormList[i + 2].sDisplayName + '</td>';
                            else
                                trForms += '<td></td>';
                            if ((i + 3) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 3].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 3].sFormName + '"/> ' + arFormList[i + 3].sDisplayName + '</td>';
                            else
                                trForms += '<td></td>';
                            trForms += '</tr>';
                        }
                    }
                    trForms += '</tbody>';
                    $("#tblForms").append(trForms);
                    $('input[type=checkbox]').attr("disabled", true);
                }
            }
        },
        error: function () {
            Success("An error occured while geting form list");
        }
    });
});

function validateRole(sRoleName) {
    if (sRoleName == "null") {
        Success("You haven't select any role! Please do select the role first.");
        $('input[type=checkbox]').attr("disabled", true);
        return false;
    }
    else
        return true;
}


function AppendFormTable(sRoleName) {
    debugger;
    var i, j, k;
    for (i = 0; i < arFormList.length; i++) {
        $('#chk' + arFormList[i].sFormName).click();
    }
    if (validateRole(sRoleName) == true) {
        sSelectedRoleValue = sRoleName;
        //Get Form list based on role
        $.ajax({
            type: "POST",
            url: "Handler/RoleManagementHandler.asmx/GetFormsForRole",
            data: '{"sRoleName":"' + sRoleName + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    $('input[type=checkbox]').removeAttr('disabled');
                    arFormListForRole = result.Arr;
                    if (arFormListForRole.length > 0) {
                        for (j = 0; j < arFormList.length; j++) {
                            for (k = 0; k < arFormListForRole.length; k++) {
                                if (arFormList[j].sFormName == arFormListForRole[k].sFormName) {
                                    $('#chk' + arFormList[j].sFormName).click();
                                }
                            }
                        }
                    }
                }
            },
            error: function () {
                Success("An error occured while geting checked form list");
            }
        });
    }
}

function SubmitFormsForRole() {
    var j = 0;
    //for (var i = 0; i < arFormList.length; i++) {
    var arrFrm = $(".checked")
    //if ($('#chk' + arFormList[i].sFormName).prop('checked')) {
    for (var i = 0; i < arrFrm.length; i++) {
        arrayToSubmit[j] = arrFrm[i].childNodes[1].value;
        j++;
    }

    // }
    // }
    var arrayJson = JSON.stringify(arrayToSubmit);
    if ($("#selRoles").val() != 'null') {
        $.ajax({
            type: "POST",
            url: "Handler/RoleManagementHandler.asmx/SetFormsForRole",
            data: '{"sSelectedRole":"' + sSelectedRoleValue + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    window.location.reload();
                }
                else if (result.retCode == 0) {
                    Success("Something goes wrong!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
            
                }
            },
            error: function () {
                Success("error occured while submitting checked forms");
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
             
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}