﻿var HotelCode = 0; var HotelName; var RoomId = 0; var AmenityList = []; var code1;var ParentId;
var ID = "";

$(function () {
    debugger
    var sPageURL = window.location.href;
    code1 = sPageURL.split('?');
    if (getParameterByName('RoomId') ==  "") {
        var code = code1[1].split('=')[1];
        HotelCode = code.split('&')[0];
        var Name = code1[1].split('&')[1];
        HotelName = Name.split('=')[1];
        document.getElementById('DivAddRooms').style.display = '';
        GetRoomType();
        GetRoomAmenities();
        // $("#tab_Amenities .wizard-next").click(function () {
        $('.wizard #tab_Amenities').on('wizardleave', function () {
            var fields = $("input[name='chkAmenities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Amenity!');
                //$(".wizard-prev ").click();
                $(".completed .active").click();
                return false;
            }

        })
        $("#tab_Policies").click(function () {
            debugger;
            var fields = $("input[name='chkAmenities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Amenity!');
                $(".wizard-prev ").click();
                return false;
            }

        })
    }
    else {
        HotelCode = getParameterByName('sHotelID');
        HotelName = getParameterByName('HotelName');
        RoomId = getParameterByName('RoomId');
        ParentId = getParameterByName('ParentId');
        ID = RoomId;
        GetRoomType();
        getRoomwithId();
        document.getElementById('DivUpdateRooms').style.display = '';


        $("#tab_Amenities1 .wizard-next").click(function () {
            var fields = $("input[name='chkAmenities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Amenity!');
                $(".wizard-prev ").click();
                return false;
            }

        })
        $("#tab_Policies1").click(function () {
            debugger;
            var fields = $("input[name='chkAmenities']").serializeArray();
            if (fields.length === 0) {
                Success('Please Select Atleast One Amenity!');
                $(".wizard-prev ").click();
                return false;
            }

        })

    }



})

function getRoomwithId() {
    var data =
  {
      RoomId: RoomId,
      HotelCode: HotelCode
  }
    $.ajax({
        type: "POST",
        url: "../handler/RoomHandler.asmx/getRoomwithId",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            RoomList = result.RoomList;
            RoomType = result.RoomType;
            AmenityList = result.AmenityList;
            if (result.retCode == 1) {
                $('#txtRoomType1').val(RoomType.RoomType);
                $('#QtyOccupacy1').val(RoomList.RoomOccupancy);
                $('#RoomSize1').val(RoomList.RoomSize);
                //$('#MaxChildsAllowed1').val(RoomList.MaxChildAllowed);
                // $('#AdultsWithChilds1').val(RoomList.AdultsWithChildAllowed);
                $('#AdultsWithoutChilds1').val(RoomList.NoOfChildWithoutBed);
                $('#RoomDescription1').val(RoomList.RoomDescription);
                $('#MaxExtrabedAllowed1').val(RoomList.MaxExtrabedAllowed);
               
                //$("#BeddingType1").val(RoomList.BeddingType)
                $("#div_bed .select span")[0].textContent = RoomList.BeddingType;
                $('input[value="' + RoomList.BeddingType + '"][class="OfferType"]').prop("selected", true);
                $("#BeddingType1").val(RoomList.BeddingType)
                //$("#SmokingAllowed1").val(RoomList.SmokingAllowed)
                // $("#drp_Smooking1 .select span")[0].textContent = RoomList.SmokingAllowed;
                //$("#SmokingAllowed1 .select span")[0].textContent = RoomList.SmokingAllowed;
                $("#div_smoke .select span")[0].textContent = RoomList.SmokingAllowed;
                $('input[value="' + RoomList.SmokingAllowed + '"][class="OfferType"]').prop("selected", true);
                $("#SmokingAllowed1").val(RoomList.SmokingAllowed)
                GetRoomAmenities();
                $('#RoomNotes1').val(RoomList.RoomType);
                debugger
                if (RoomList.SubImages != null) {
                    var SubImg = [];
                    SubImg = RoomList.SubImages.split('^');
                    if (SubImg.length != 0) {
                        var tdRequest = "";
                        var ImgRequest = '';
                        for (var j = 0; j < SubImg.length; j++) {
                            if (SubImg[j] != 'undefined' && SubImg[j] != '') {
                                ImgRequest += '<div class="three-columns divImg" ><img style="width:100%"  onclick="divSelect(this, \'' + SubImg[j] + '\');"  src="../RoomImages/' + SubImg[j] + '"><a onclick="deletePreview(this, \'' + j + '\',\'' + SubImg[j] + '\')"  class="button"><span class="icon-trash"></span></a></div>';
                                RoomImages[j] = SubImg[j];

                                $("#hdnImgupdate").val()
                            }

                        }

                        $('#image_preview1').append(ImgRequest);
                    }

                }
            }
            if (result.retCode == 0) {
                Success('Something went wrong')
            }
        },

    });
}

function GetRoomType() {
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetRoomType",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRoomType = result.RoomType;
                Div = '';
                for (var i = 0; i < arrRoomType.length; i++) {

                    Div += '<option data-id=' + arrRoomType[i].RoomTypeID + '>' + arrRoomType[i].RoomType + '</option>'
                }
                $("#RoomType").append(Div);
                $("#RoomType1").append(Div);
            }
        },

    });
}

function GetRoomAmenities() {
    $("#divAddAmenities").empty();
    $("#divAddAmenities1").empty();
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetRoomAmenities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRoomAmenities = result.arrRoomAmenities;
                var chkRequest = '<div class="columns">';
                for (i = 0; i < arrRoomAmenities.length; i++) {
                    chkRequest += '<div class="four-columns">'
                    chkRequest += '<input type="checkbox" name="chkAmenities" onchange="AddAmenities(id)" id="HtlAmenities' + i + '" class="HotelAmenities" value="' + arrRoomAmenities[i].RoomAmunityID + '" title="' + arrRoomAmenities[i].RoomAmunityName + '"><label for="HtlAmenities' + i + '">' + arrRoomAmenities[i].RoomAmunityName + '</label></span>';
                    chkRequest += '</div>'

                }
                if (getParameterByName('RoomId') == "") {
                    $("#divAddAmenities").append(chkRequest);
                }
                else {
                    $("#divAddAmenities1").append(chkRequest);
                }


                if (AmenityList.length > 0) {
                    for (i = 0; i < arrRoomAmenities.length; i++) {
                        for (var j = 0; j < AmenityList.length; j++) {
                            if (arrRoomAmenities[i].RoomAmunityName == AmenityList[j]) {
                                $(':checkbox[value="' + arrRoomAmenities[i].RoomAmunityID + '"]').prop('checked', true)
                            }
                        }
                    }
                }


            }
        },

    });
}

$('#wizAmenities .wizard-next').filter(function () {
    return $(this).removeClass('.button').length == 1;
})

var AmenitiesCode;
function AddAmenities() {

    var addedAmenitiesValues = "";
    var rAmenities = document.getElementsByClassName('HotelAmenities')
    for (var i = 0; i < rAmenities.length; i++) {
        if (rAmenities[i].checked) {
            addedAmenitiesValues = addedAmenitiesValues + ',' + rAmenities[i].value;
        }

    }
    addedAmenitiesValues = addedAmenitiesValues.replace(/^,|,$/g, '');
    AmenitiesCode = addedAmenitiesValues;
}

function SaveAmenities() {
    var roomAmenityID = '';
    var RoomAmenityText = $('#txtAmenities').val();   
    var RoomAmenityText1 = $('#txtAmenities1').val();

    if (RoomAmenityText == "" && RoomAmenityText1 == "") {
        Success("Please enter amenities name.");
        return false;
    }
    //else if (true) {

    //}

    //if (RoomAmenityText1 == "") {
    //    Success("Please enter amenities name.");
    //    return false;
    //}
    if (RoomAmenityText1 != "" && RoomAmenityText == "")
    {
        RoomAmenityText = RoomAmenityText1;
    }
    var HotelId = HotelCode;
    var data =
  {
      RoomAmenityText: RoomAmenityText,
      HotelId: HotelId
  }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/AddRoomAmenity",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            roomAmenityID = result.RoomAmenityID;
            if (result.retCode == 1) {
                Success("Room Amenity Saved")
                GetRoomAmenities();
            }
            if (result.retCode == 0) {
                Success('Something went wrong')
            }
        },

    });
}


function getHotelDetails(HotelCode) {
    var data =
  {
      HotelCode: HotelCode
  }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/GetHotelByHotelId",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelDetail = result.HotelListbyId[0];
                $('#HotelNameTitle').val(arrHotelDetail.HotelName);
                $('#HtlName1').val(arrHotelDetail.HotelName);
                $('#HtlRatings1').val(arrHotelDetail.HotelCategory);
                $('#HtlDescription1').val(arrHotelDetail.HotelDescription);
                $('#HtlAddress1').val(arrHotelDetail.HotelAddress);
                $('#htlCity1').val(arrHotelDetail.CityId);
                $('#htlCountry1').val(arrHotelDetail.CountryId);
                $('#htlZipcode1').val(arrHotelDetail.HotelZipCode);
                $('#htlLangitude1').val(arrHotelDetail.HotelLangitude);
                $('#htlLatitude1').val(arrHotelDetail.HotelLatitude);
                $('#txtChildAgeFrom1').val(arrHotelDetail.ChildAgeFrom);
                $('#txtChildAgeTo1').val(arrHotelDetail.ChildAgeTo);
                $('#txtTripAdviserLink1').val(arrHotelDetail.TripAdviserLink);
                $('#txtHotelGroup1').val(arrHotelDetail.HotelGroup);
                $('#txtCheckinTime1').val(arrHotelDetail.CheckinTime);
                $('#txtCheckoutTime1').val(arrHotelDetail.CheckoutTime);
                $('#selPatesAllowed1').val(arrHotelDetail.PatesAllowed);
                $('#selLiquorPolicy1').val(arrHotelDetail.LiquorPolicy);
                $('#TxtContactPerson1').val(arrHotelDetail.ContactPerson);
                $('#txtMobileNo1').val(arrHotelDetail.MobileNo);
                $('#txtEmail1').val(arrHotelDetail.EmailAddress);

                nFacilities = arrHotelDetail.HotelFacilities.split(',');
                if (arrHotelDetail.HotelImage != "") {
                    $('.selMainImage').attr('src', arrHotelDetail.HotelImage);
                    MainImage = arrHotelDetail.HotelImage;
                }
                if (arrHotelDetail.SubImages != "") {
                    var SubImg = [];
                    SubImg = arrHotelDetail.SubImages.split(',');
                    var tdRequest = "";
                    for (var i = 0; i < SubImg.length; i++) {
                        tdRequest += '<img src="' + SubImg[i] + '"  class="SubImage" width="30%" style="padding:1px;">';
                        SubImages += SubImg[i] + ',';
                    }
                    $('#image-holder2').append(tdRequest);

                }
                sid = arrHotelDetail.sid;
                HotelBedsCode = arrHotelDetail.HotelBedsCode;
                DotwCode = arrHotelDetail.DotwCode;
                MGHCode = arrHotelDetail.MGHCode;
                ExpediaCode = arrHotelDetail.ExpediaCode;
                GRNCode = arrHotelDetail.GRNCode;

            }
        },

    });
}

var HRoomTypeID;
function SaveRoomType() {
    var roomtypeID = '';
    var RoomTypeText = $('#txtRoomType').val();
    var RoomTypeText1 = $('#txtRoomType1').val();
    if (RoomTypeText1 != "") {
        RoomTypeText = RoomTypeText1
    }
    var HotelId = HotelCode;
    var data =
  {
      RoomTypeText: RoomTypeText,
      HotelId: HotelId
  }
    $.ajax({
        type: "POST",
        url: "../HotelHandler.asmx/AddRoomType",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            roomtypeID = result.RoomTypeId;
            if (result.retCode == 1) {
                HRoomTypeID = roomtypeID;
            }
            if (result.retCode == 0) {
                HRoomTypeID = "";
            }
        },

    });
}

function AddRoomDetails() {
    debugger
    AddAmenities();
    //var fileUpload = $("#RoomImages").get(0);
    //var fileUpload1 = $("#RoomImages1").get(0);

    //var files = fileUpload.files;
    //var files1 = fileUpload1.files;
    //if (files.length > 0)
    //{
    //    SaveRoomImages();
    //}
    //SaveRoomImages();
    var val = $('#txtRoomType').val();
    var RoomTypeID = $('#RoomType option').filter(function () { return this.value == val; }).data('id');
    if (RoomTypeID == "" || RoomTypeID == undefined) {
        SaveRoomType();
        setTimeout(function () {
            RoomTypeID = HRoomTypeID;
            var RoomID = RoomId;
            var HotelID = HotelCode;
            //var NoOfRooms = $('#QtyRooms').val();
            var MaxOccupacy = $('#QtyOccupacy').val();
            // var MaxChilds = $('#MaxChildsAllowed').val();
            //  var AdultsWithChilds = $('#AdultsWithChilds').val();
            var AdultsWithoutChilds = $('#AdultsWithoutChilds').val();
            var RoomDescription = $('#RoomDescription').val();
            var RoomNotes = $('#RoomNotes').val();
            var RoomAmenitiesID = AmenitiesCode;
            var MaxExtrabedAllowed = $('#MaxExtrabedAllowed').val();
            var RoomSize = $('#RoomSize').val();
            var BeddingType = $('#BeddingType').val();
            var SmokingAllowed = $('#SmokingAllowed').val();
            //var Interconnection = $('#Interconnection').val();
            //var MainImage = $('#ImgUrl-Add').val();
            //var SubImages = arrSubImages;

            var dataToPass = {
                RoomID: RoomID,
                HotelID: HotelID,
                RoomTypeID: RoomTypeID,
                RoomAmenitiesID: RoomAmenitiesID,
                //NoOfRooms: NoOfRooms,
                MaxOccupacy: MaxOccupacy,
                // MaxChilds: MaxChilds,
                // AdultsWithChilds: AdultsWithChilds,
                AdultsWithoutChilds: AdultsWithoutChilds,
                RoomDescription: RoomDescription,
                RoomNotes: RoomNotes,
                MaxExtrabedAllowed: MaxExtrabedAllowed,
                RoomSize: RoomSize,
                BeddingType: BeddingType,
                SmokingAllowed: SmokingAllowed,
                //Interconnection: Interconnection,
                //MainImage: MainImage,
                //SubImages: SubImages
            }


            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/AddHotelRoom",
                data: JSON.stringify(dataToPass),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        ID = result.id;
                        Success("Room Added Successfully")
                        setTimeout(function () {
                            window.location.href = "RoomList.aspx?sHotelID=" + HotelID + "&HName=" + HotelName;
                        }, 2000);
                       
                        var fileUpload = $("#RoomImages").get(0);
                        var fileUpload1 = $("#RoomImages1").get(0);

                        var files = fileUpload.files;
                        var files1 = fileUpload1.files;
                        if (files.length > 0) {
                            SaveRoomImages();
                        }
                    }
                    if (result.retCode == 0) {
                        Success("Something went wrong");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                     
                    }
                },
                error: function () {
                    Success("An error occured while Adding details");

                }
            });
        }, 3000);
    }
    else {
        var RoomID = RoomId;
        var HotelID = HotelCode;
        //var NoOfRooms = $('#QtyRooms').val();
        var MaxOccupacy = $('#QtyOccupacy').val();
        // var MaxChilds = $('#MaxChildsAllowed').val();
        // var AdultsWithChilds = $('#AdultsWithChilds').val();
        var AdultsWithoutChilds = $('#AdultsWithoutChilds').val();
        var RoomDescription = $('#RoomDescription').val();
        var RoomNotes = $('#RoomNotes').val();
        var RoomAmenitiesID = AmenitiesCode;
        var MaxExtrabedAllowed = $('#MaxExtrabedAllowed').val();
        var RoomSize = $('#RoomSize').val();
        var BeddingType = $('#BeddingType').val();
        var SmokingAllowed = $('#SmokingAllowed').val();
        //var Interconnection = $('#Interconnection').val();
        //var MainImage = $('#ImgUrl-Add').val();
        //var SubImages = arrSubImages;

        var dataToPass = {
            RoomID: RoomID,
            HotelID: HotelID,
            RoomTypeID: RoomTypeID,
            RoomAmenitiesID: RoomAmenitiesID,
            //NoOfRooms: NoOfRooms,
            MaxOccupacy: MaxOccupacy,
            // MaxChilds: MaxChilds,
            // AdultsWithChilds: AdultsWithChilds,
            AdultsWithoutChilds: AdultsWithoutChilds,
            RoomDescription: RoomDescription,
            RoomNotes: RoomNotes,
            MaxExtrabedAllowed: MaxExtrabedAllowed,
            RoomSize: RoomSize,
            BeddingType: BeddingType,
            SmokingAllowed: SmokingAllowed,
            //Interconnection: Interconnection,
            //MainImage: MainImage,
            //SubImages: SubImages
        }


        $.ajax({
            type: "POST",
            url: "../HotelHandler.asmx/AddHotelRoom",
            data: JSON.stringify(dataToPass),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    ID = result.id;
                    Success("Room Added Successfully")
                    setTimeout(function () {
                        window.location.href = "RoomList.aspx?sHotelID=" + HotelID + "&HName=" + HotelName;
                    }, 2000);
                  
                    var fileUpload = $("#RoomImages").get(0);
                    var fileUpload1 = $("#RoomImages1").get(0);

                    var files = fileUpload.files;
                    var files1 = fileUpload1.files;
                    if (files.length > 0) {
                        SaveRoomImages();
                    }
                }
                if (result.retCode == 0) {
                    Success("Something went wrong");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                  
                }
            },
            error: function () {
                Success("An error occured while Adding details");

            }
        });
    }

}



function onSubmit() {
    var fields = $("input[name='AmenityName']").serializeArray();
    if (fields.length == 0) {
        Success('Please Select Atleast One Amenity');
        return false;
    }
    else {
        Success(fields.length + " items selected");
    }
}


function UpdateRoomDetails() {
    debugger
    AddAmenities();
    SaveRoomImages();
    var val = $('#txtRoomType1').val();
    var RoomTypeID = $('#RoomType1 option').filter(function () { return this.value == val; }).data('id');
    if (RoomTypeID == "" || RoomTypeID == undefined) {
        SaveRoomType();
        setTimeout(function () {
            RoomTypeID = HRoomTypeID;
            var RoomID = RoomId;
            var HotelID = HotelCode;
            //var NoOfRooms = $('#QtyRooms').val();
            var MaxOccupacy = $('#QtyOccupacy1').val();
            //var MaxChilds = $('#MaxChildsAllowed1').val();
            // var AdultsWithChilds = $('#AdultsWithChilds1').val();
            var AdultsWithoutChilds = $('#AdultsWithoutChilds1').val();
            var RoomDescription = $('#RoomDescription1').val();
            var RoomNotes = $('#RoomNotes1').val();
            var RoomAmenitiesID = AmenitiesCode;
            var MaxExtrabedAllowed = $('#MaxExtrabedAllowed1').val();
            var RoomSize = $('#RoomSize1').val();
            var BeddingType = $('#BeddingType1').val();
            var SmokingAllowed = $('#SmokingAllowed1').val();
            //var Interconnection = $('#Interconnection').val();
            //var MainImage = $('#ImgUrl-Add').val();
            //var SubImages = arrSubImages;

            var dataToPass = {
                RoomID: RoomID,
                HotelID: HotelID,
                RoomTypeID: RoomTypeID,
                RoomAmenitiesID: RoomAmenitiesID,
                //NoOfRooms: NoOfRooms,
                MaxOccupacy: MaxOccupacy,
                //  MaxChilds: MaxChilds,
                //  AdultsWithChilds: AdultsWithChilds,
                AdultsWithoutChilds: AdultsWithoutChilds,
                RoomDescription: RoomDescription,
                RoomNotes: RoomNotes,
                MaxExtrabedAllowed: MaxExtrabedAllowed,
                RoomSize: RoomSize,
                BeddingType: BeddingType,
                SmokingAllowed: SmokingAllowed,
                //Interconnection: Interconnection,
                //MainImage: MainImage,
                //SubImages: SubImages
            }


            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/AddHotelRoom",
                data: JSON.stringify(dataToPass),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        Success("Room Update Successfully")
                        setTimeout(function () {
                            window.location.href = "RoomList.aspx?sHotelID=" + HotelID + "&HName=" + HotelName;
                        }, 2000);
                      
                    }
                    if (result.retCode == 0) {
                        Success("Something went wrong");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    
                    }
                },
                error: function () {
                    Success("An error occured while Adding details");

                }
            });
        }, 3000);
    }
    else {
        var RoomID = RoomId;
        var HotelID = HotelCode;
        //var NoOfRooms = $('#QtyRooms').val();
        var MaxOccupacy = $('#QtyOccupacy1').val();
        //  var MaxChilds = $('#MaxChildsAllowed1').val();
        //  var AdultsWithChilds = $('#AdultsWithChilds1').val();
        var AdultsWithoutChilds = $('#AdultsWithoutChilds1').val();
        var RoomDescription = $('#RoomDescription1').val();
        var RoomNotes = $('#RoomNotes1').val();
        var RoomAmenitiesID = AmenitiesCode;
        var MaxExtrabedAllowed = $('#MaxExtrabedAllowed1').val();
        var RoomSize = $('#RoomSize1').val();
        var BeddingType = $('#BeddingType1').val();
        var SmokingAllowed = $('#SmokingAllowed1').val();
        //var Interconnection = $('#Interconnection').val();
        //var MainImage = $('#ImgUrl-Add').val();
        //var SubImages = arrSubImages;

        var dataToPass = {
            RoomID: RoomID,
            HotelID: HotelID,
            RoomTypeID: RoomTypeID,
            RoomAmenitiesID: RoomAmenitiesID,
            //NoOfRooms: NoOfRooms,
            MaxOccupacy: MaxOccupacy,
            // MaxChilds: MaxChilds,
            // AdultsWithChilds: AdultsWithChilds,
            AdultsWithoutChilds: AdultsWithoutChilds,
            RoomDescription: RoomDescription,
            RoomNotes: RoomNotes,
            MaxExtrabedAllowed: MaxExtrabedAllowed,
            RoomSize: RoomSize,
            BeddingType: BeddingType,
            SmokingAllowed: SmokingAllowed,
            //Interconnection: Interconnection,
            //MainImage: MainImage,
            //SubImages: SubImages
        }


        $.ajax({
            type: "POST",
            url: "../HotelHandler.asmx/AddHotelRoom",
            data: JSON.stringify(dataToPass),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Room Update Successfully")
                    setTimeout(function () {
                        window.location.href = "HotelList.aspx";
                    }, 2000);
                 
                }
                if (result.retCode == 0) {
                    Success("Something went wrong");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                 
                }
            },
            error: function () {
                Success("An error occured while Adding details");

            }
        });
    }
}


function UpdateRoomDetailsApproved() {
    debugger
    AddAmenities();
    SaveRoomImages();
    var val = $('#txtRoomType1').val();
    var RoomTypeID = $('#RoomType1 option').filter(function () { return this.value == val; }).data('id');
    if (RoomTypeID == "" || RoomTypeID == undefined) {
        SaveRoomType();
        setTimeout(function () {
            RoomTypeID = HRoomTypeID;
            var RoomID = RoomId;
            var HotelID = HotelCode;
            //var NoOfRooms = $('#QtyRooms').val();
            var MaxOccupacy = $('#QtyOccupacy1').val();
            //var MaxChilds = $('#MaxChildsAllowed1').val();
            // var AdultsWithChilds = $('#AdultsWithChilds1').val();
            var AdultsWithoutChilds = $('#AdultsWithoutChilds1').val();
            var RoomDescription = $('#RoomDescription1').val();
            var RoomNotes = $('#RoomNotes1').val();
            var RoomAmenitiesID = AmenitiesCode;
            var MaxExtrabedAllowed = $('#MaxExtrabedAllowed1').val();
            var RoomSize = $('#RoomSize1').val();
            var BeddingType = $('#BeddingType1').val();
            var SmokingAllowed = $('#SmokingAllowed1').val();
            //var Interconnection = $('#Interconnection').val();
            //var MainImage = $('#ImgUrl-Add').val();
            //var SubImages = arrSubImages;

            var dataToPass = {
                RoomID: RoomID,
                ParentId: ParentId,
                HotelID: HotelID,
                RoomTypeID: RoomTypeID,
                RoomAmenitiesID: RoomAmenitiesID,
                //NoOfRooms: NoOfRooms,
                MaxOccupacy: MaxOccupacy,
                //  MaxChilds: MaxChilds,
                //  AdultsWithChilds: AdultsWithChilds,
                AdultsWithoutChilds: AdultsWithoutChilds,
                RoomDescription: RoomDescription,
                RoomNotes: RoomNotes,
                MaxExtrabedAllowed: MaxExtrabedAllowed,
                RoomSize: RoomSize,
                BeddingType: BeddingType,
                SmokingAllowed: SmokingAllowed,
                //Interconnection: Interconnection,
                //MainImage: MainImage,
                //SubImages: SubImages
            }


            $.ajax({
                type: "POST",
                url: "../HotelHandler.asmx/AddHotelAppprovedRoom",
                data: JSON.stringify(dataToPass),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        Success("Room Approved Successfully")
                        setTimeout(function () {
                            window.location.href = "RoomList.aspx?sHotelID=" + HotelID + "&HName=" + HotelName;
                        }, 2000);

                    }
                    if (result.retCode == 0) {
                        Success("Something went wrong");
                        setTimeout(function () {
                            window.location.reload();
                        }, 4000);

                    }
                },
                error: function () {
                    Success("An error occured while Adding details");

                }
            });
        }, 3000);
    }
    else {
        var RoomID = RoomId;
        var HotelID = HotelCode;
        //var NoOfRooms = $('#QtyRooms').val();
        var MaxOccupacy = $('#QtyOccupacy1').val();
        //  var MaxChilds = $('#MaxChildsAllowed1').val();
        //  var AdultsWithChilds = $('#AdultsWithChilds1').val();
        var AdultsWithoutChilds = $('#AdultsWithoutChilds1').val();
        var RoomDescription = $('#RoomDescription1').val();
        var RoomNotes = $('#RoomNotes1').val();
        var RoomAmenitiesID = AmenitiesCode;
        var MaxExtrabedAllowed = $('#MaxExtrabedAllowed1').val();
        var RoomSize = $('#RoomSize1').val();
        var BeddingType = $('#BeddingType1').val();
        var SmokingAllowed = $('#SmokingAllowed1').val();
        //var Interconnection = $('#Interconnection').val();
        //var MainImage = $('#ImgUrl-Add').val();
        //var SubImages = arrSubImages;

        var dataToPass = {
            RoomID: RoomID,
            ParentId: ParentId,
            HotelID: HotelID,
            RoomTypeID: RoomTypeID,
            RoomAmenitiesID: RoomAmenitiesID,
            //NoOfRooms: NoOfRooms,
            MaxOccupacy: MaxOccupacy,
            // MaxChilds: MaxChilds,
            // AdultsWithChilds: AdultsWithChilds,
            AdultsWithoutChilds: AdultsWithoutChilds,
            RoomDescription: RoomDescription,
            RoomNotes: RoomNotes,
            MaxExtrabedAllowed: MaxExtrabedAllowed,
            RoomSize: RoomSize,
            BeddingType: BeddingType,
            SmokingAllowed: SmokingAllowed,
            //Interconnection: Interconnection,
            //MainImage: MainImage,
            //SubImages: SubImages
        }


        $.ajax({
            type: "POST",
            url: "../HotelHandler.asmx/AddHotelAppprovedRoom",
            data: JSON.stringify(dataToPass),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Room Approved Successfully")
                    setTimeout(function () {
                        window.location.href = "HotelList.aspx";
                    }, 4000);

                }
                if (result.retCode == 0) {
                    Success("Something went wrong");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);

                }
            },
            error: function () {
                Success("An error occured while Adding details");

            }
        });
    }
}

// Room Images
var RoomImages = []
function preview_images() {
    var fileUpload = $("#RoomImages").get(0);
    var files = fileUpload.files;
    var total_file = document.getElementById("RoomImages").files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append('<div class="three-columns divImg" ><img style="width:100%"  onclick="divSelect(this, \'' + files[i].name + '\');"  src="' + URL.createObjectURL(event.target.files[i]) + '"><a onclick="deletePreview(this, \'' + i + '\',\'' + files[i].name + '\')"  class="button"><span class="icon-trash"></span></a></div>');
        RoomImages[i] = files[i].name;
    }

}
deletePreview = function (ele, i, filename) {
    "use strict";
    var arrImg = [];
    try {
        $(ele).parent().remove();
        //window.filesToUpload.splice(i, 1);
        for (var i = 0; i < RoomImages.length; i++) {
            if (RoomImages[i] != filename && RoomImages[i] != undefined) {
                arrImg[i] = RoomImages[i];
            }
        }
        RoomImages = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}

divSelect = function (ele, filename) {
    "use strict";
    $('.divImg').removeClass('selectedImg');
    var arrImg = [];
    try {
        $(ele).parent().addClass('selectedImg')

        arrImg[0] = filename;
        for (var i = 1; i < RoomImages.length; i++) {
            if (RoomImages[i] != filename) {
                arrImg[i] = RoomImages[i];
            }
            else {
                arrImg[i] = RoomImages[0];
            }
        }
        RoomImages = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}


function SaveRoomImages() {
    var SubImages = '';
    var fileUpload = $("#RoomImages").get(0);
    var fileUpload1 = $("#RoomImages1").get(0);

    var files = fileUpload.files;
    var files1 = fileUpload1.files;
    //if (files1.length > 0 && files.length == 0) {
    //    files = fileUpload1.files;
    //}
    if (files.length != 0) {
        for (var i = 0; i < RoomImages.length; i++) {
            if (RoomImages[i] != undefined && RoomImages[i] != "") {
                SubImages += RoomImages[i] + "^";
            }
        }
    }
    else if (files1.length > 0) {
        for (var i = 0; i < files1.length; i++) {
            if (files1[i].name != undefined && files1[i].name != "") {
                SubImages += files1[i].name + "^";
            }
        }
    }
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    for (var i = 0; i < files1.length; i++) {
        data.append(files1[i].name, files1[i]);
    }
    $.ajax({
        url: "../RoomImageHandler.ashx?HotelId=" + HotelCode + "&RoomId=" + ID + "&ImagePath=" + SubImages,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            // Success("Images Uploaded. Please wait..")
        },
    });
}


function preview_images1() {
    debugger
    var fileUpload = $("#RoomImages1").get(0);
    var files = fileUpload.files;
    var total_file = fileUpload.files.length;
    var OldLength = RoomImages.length;
    for (var i = 0; i < total_file; i++) {
        if (files[i].name != "undefined" || files[i].name != "") {
            $('#image_preview1').append('<div class="three-columns divImg" ><img style="width:100%"  onclick="divSelect(this, \'' + files[i].name + '\');"  src="' + URL.createObjectURL(event.target.files[i]) + '"><a onclick="deletePreview(this, \'' + i + '\',\'' + files[i].name + '\')"  class="button"><span class="icon-trash"></span></a></div>');
            for (var j = OldLength + 1; j == OldLength + total_file; j++) {
                RoomImages[j] = files[i].name;
            }
        }
    }

}
// End Room Images

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

