﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelInventory.aspx.cs" Inherits="CutAdmin.HotelInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <script>
        $(function () {
            HotelCode = GetQueryStringParams('sHotelID').replace(/%20/g, ' ');
            HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');


        })
    </script>
    <script>
        function Redirect(value) {
            if (value == "Start") {
                window.location.href = "StartSale.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
            }
            else if (value == "Stop") {
                window.location.href = "StopSale.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
            }
        }
        function ShowSwitch() {
            if ($("#chk_freesale").is(':checked')) {
                $("#Switch").show();
            }
            else {
                $("#Switch").hide();
            }
        }
        function GetQueryStringParams(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('%');
            //var sURLVariables = sVariables.split('%20');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    </script>

    <script>
        function Allocation() {
            window.location.href = "Allocation.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
        }
    </script>
    <script>
        function Allotment() {
            window.location.href = "Allotment.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
        }
    </script>

    <script src="Scripts/HotelInventory.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotel Inventory</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <span class="">
                <h3 id="lbl_Hotel" style="margin-bottom:5px"></h3>
                
                <label id="lbl_address"></label>
            </span>
            <br />
            <br />
            
            <div class="row block margin-bottom">

                <div class="with-padding " id="div_Rooms">
                    <div class="columns">
                        <div class="two-columns">
                            <input type="checkbox" name="chk_freesale" id="chk_freesale" value="1" class="" onchange="ShowSwitch()">
                            <label for="chk_freesale" class="label">Free Sale</label>
                        </div>
                        <div class="two-columns">
                            <input type="checkbox" name="chk_allocation" id="chk_allocation" value="1" class="" onchange="ShowAllocation()">
                            <label for="chk_allocation" class="label">Allocation</label>
                        </div>
                        <div class="two-columns">
                            <input type="checkbox" name="chk_allotment" id="chk_allotment" value="1" class="" onchange="ShowAllotment()">
                            <label for="chk_allotment" class="label">Allotment</label>
                        </div>
                        <div class="six-columns" id="Switch" style="display: none">
                            <span class="button-group">
                                <label for="chk_Start" class="button grey-active">
                                    <input type="radio" name="button-radio" id="chk_Start" value="Start" onclick="Redirect(this.value)">
                                    Start Sale
                                </label>
                                <label for="chk_Stop" class="button red-active">
                                    <input type="radio" name="button-radio" id="chk_Stop" value="Stop" onclick="Redirect(this.value)">
                                    Stop Sale
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content -->




    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>
    <script src="js/developr.table.js"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>


</asp:Content>
