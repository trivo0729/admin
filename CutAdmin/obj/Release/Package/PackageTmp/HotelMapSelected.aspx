﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelMapSelected.aspx.cs" Inherits="CutAdmin.HotelMapSelected" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/HotelMapSelected.js?v=1.3"></script>



    <!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
   

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3> Hotel Mapping</h3>
		</hgroup>
        <div class="with-padding">
            <div class="columns">
                <h3 class="wrapped margin-left white" style="background-color: #006699">Selected Hotel Details</h3>

                <div class="twelve-columns twelve-columns-tablet twelve-columns-mobile">


                    <div class="side-tabs same-height margin-bottom tabs-active tabs-animated tab-opened" style="min-height: 200px;">

                        <ul class="tabs">
                            <li class="active"><a href="#divHotelname">Hotel Name</a></li>
                            <li class=""><a href="#divHotelAddress">Address</a></li>
                            <li class=""><a href="#divHotelDescription">Description</a></li>
                            <li class=""><a href="#divHotelRatings">Ratings</a></li>
                            <li class=""><a href="#divFacilities">Facilities</a></li>

                        </ul>
                        <div class="tabs-content" style="width: 68%; float: left;">

                            <div id="divHotelname" class="with-padding tab-active" style="display: block; min-height: 300px;">
                            </div>

                            <div id="divHotelAddress" class="with-padding" style="display: none; min-height: 300px;">
                            </div>


                            <div id="divHotelDescription" class="with-padding" style="display: none; min-height: 300px;">
                            </div>

                            <div id="divHotelRatings" class="with-padding" style="display: none; min-height: 300px;">
                            </div>



                            <div id="divFacilities" class="with-padding" style="display: none; min-height: 300px;">
                                <div id="divFacilities1"></div>
                                <div id="divFacilities2" class="boxed" style="border-style: groove;background-color:transparent; border-width: 1px; margin-top: 10%;">
                                    Select Suppliers to decide your Facilities
                                </div>
                            </div>







                        </div>

                        <div style="float: right; color: white; background-color: #006699; border: none; width: 30%;">
                            <dl class="accordion same-height">

                                <dt>Hotel Name:</dt>
                                <dd>
                                    <div class="">
                                        <textarea id="lblHotelname" style="color: white; background: none; border: none; resize: none"></textarea>
                                        <%-- <input id="lblHotelname" style="width: 100%; border: none;height:100%" class="label" readonly />--%>
                                    </div>
                                </dd>

                                <dt>Hotel Address:</dt>
                                <dd>
                                    <div class="">
                                        <textarea id="lblHotelAddress" style="color: white; background: none; border: none; padding: 0; font-size: x-small; min-height: 50px; width: 100%; resize: none" readonly></textarea>
                                    </div>
                                </dd>

                                <dt>Hotel Description:</dt>
                                <dd>
                                    <div style="min-height: 200px;">
                                        <textarea id="lblHotelDescription" style="color: white; background: none; border: none; padding: 0; font-size: x-small; min-height: 200px; width: 100%; resize: none" readonly></textarea>
                                    </div>
                                </dd>
                                <dt>Ratings:</dt>
                                <dd>
                                    <div class="">
                                        <textarea id="lblHotelRatings" style="color: white; background: none; border: none; padding: 0; font-size: x-small; min-height: 50px; width: 100%; resize: none" readonly></textarea>
                                    </div>
                                </dd>
                                <dt>Hotel Facilities:</dt>
                                <dd>
                                    <div style="min-height: 200px;">
                                        <textarea id="lblHotelFacilities" style="color: white; background: none; border: none; padding: 0; font-size: x-small; min-height: 200px; width: 100%; resize: none" readonly></textarea>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <input id="SelectedFacilities" hidden="hidden" />
            <hr />
            <p class="button-height inline-label">
                <button type="button" class="button blue-gradient mid-margin-right float-right" onclick="GetSelectedData();">NEXT</button>
            </p>
            <hr />
           <%-- <div class="columns">
                <h3 class="wrapped margin-left white" style="background-color: #006699">Add Hotel Mapping</h3>
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline green">Hotel Details</h3>
                </div>

                <div class="new-row four-columns">
                    <h6>Hotel Name:</h6>
                    <input type="text" id="HtlName" size="9" class="input full-width" value="">
                </div>

                <div class="four-columns">
                    <h6>Ratings:</h6>
                    <input type="text" id="HtlRatings" class="input full-width" value="">
                </div>



                <div class="new-row twelve-columns">

                    <h6>Description</h6>
                    <textarea name="autoexpanding" id="HtlDescription" class="input full-width autoexpanding" style="min-height: 100px;"></textarea>

                </div>

                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline green">Location Details</h3>
                </div>

                <div class="new-row four-columns">

                    <h6>Address</h6>
                    <textarea name="autoexpanding" id="HtlAddress" class="input full-width" style="overflow: hidden; resize: none; height: auto; min-height: 100px"></textarea>

                </div>
                <div class="four-columns">
                    <h6>City:</h6>
                    <input type="text" id="htlCity" class="input full-width" value="" readonly>
                </div>
                <div class="four-columns">
                    <h6>Country:</h6>
                    <input type="text" id="htlCountry" class="input full-width" value="" readonly>
                </div>
                <div class="two-columns">
                    <h6>Zip Code:</h6>
                    <input type="text" id="htlZipcode" class="input full-width" value="">
                </div>
                <div class="three-columns">
                    <h6>Langitude:</h6>
                    <input type="text" id="htlLangitude" class="input full-width" value="">
                </div>
                <div class="three-columns">
                    <h6>Latitude:</h6>
                    <input type="text" id="htlLatitude" class="input full-width" value="">
                </div>


                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline green">Properties Details</h3>
                </div>

                <div class="new-row six-columns">
                    <h6>Facilities:</h6>
                    <textarea name="autoexpanding" id="HtlFacilities" class="input full-width autoexpanding" style="min-height: 50px;"></textarea>
                </div>
                <div class="three-columns">
                    <h6>Pates Allowed:</h6>
                    <select id="selPatesAllowed" class="full-width glossy blue-gradient select  expandable-list ">
                        <option selected="selected" value="-" disabled>Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        <option value="Not Specified">Not Specified</option>
                    </select>
                </div>
                <div class="three-columns">
                    <h6>Liquor Policy:</h6>
                    <select id="selLiquorPolicy" class="full-width glossy blue-gradient select  expandable-list ">
                        <option selected="selected" value="-" disabled>Select</option>
                        <option value="Allowed in room">Allowed in room</option>
                        <option value="Allowed in lobby">Allowed in lobby</option>
                        <option value="Allowed in restaurant">Allowed in restaurant</option>
                        <option value="Allowed in dedicated area">Allowed in dedicated area</option>
                        <option value="Not allowed">Not allowed</option>
                        <option value="Not Specified">Not Specified</option>
                    </select>
                </div>

                <div class="new-row three-columns">
                    <h6>Trip Adviser Link:</h6>
                    <input type="text" class="input full-width " value="" id="txtTripAdviserLink">
                </div>

                <div class="three-columns">
                    <h6>Hotel Group:</h6>
                    <input type="text" class="input full-width" value="" id="txtHotelGroup">
                </div>

                <div class="three-columns">
                    <h6>Checkin Time:</h6>
                    <input type="time" class="input full-width" value="" id="txtCheckinTime">
                </div>
                <div class="three-columns">
                    <h6>Checkout Time:</h6>
                    <input type="time" class="input full-width" value="" id="txtCheckoutTime">
                </div>

                <div class="new-row four-columns">
                    <h6>Adult Min. Age:</h6>
                    <input type="text" class="input full-width " value="" id="txtAdultMinAge">
                </div>

                <div class="four-columns">
                    <h6>Child Age From:</h6>
                    <input type="number" class="input full-width" value="" id="txtChildAgeFrom">
                </div>

                <div class="four-columns">
                    <h6>Child Age To:</h6>
                    <input type="number" class="input full-width" value="" id="txtChildAgeTo">
                </div>

                <div class="new-row twelve-columns">
                    <button type="button" class="button glossy blue-gradient float-right" id="btn_Map" onclick="SaveHotelMapping();">Save Mapping</button>
                </div>






            </div>
            <br />
            <br />
            <br />--%>
        </div>
<%--                <div class="six-columns six-columns-tablet twelve-columns-mobile">


                    <h3 class="thin underline">Selected Details of Hotel</h3>

                    <div class="margin-bottom ">
                        <div class="panel-content linen">
                            <div class="panel-load-target scrollable with-padding custom-scroll" style="position: relative;">

                                <p class="button-height inline-label">
                                    <label for="input-3" class="label"><b>Adult Min. Age</b></label>
                                    <input type="number" name="input-3" width="50%" class="input " value="0" id="txtAdultMinAge" placeholder="Adult Min. Age"><br />
                                </p>
                                <p class="button-height inline-label">
                                    <label for="input-3" class="label"><b>Child Age From:</b></label>
                                    <input type="number" name="input-3" width="50%" class="input " value="0" id="txtChildAgeFrom" placeholder="Child Age From">
                                </p>
                                <p class="button-height inline-label">
                                    <label for="input-3" class="label"><b>Child Age To</b></label>
                                    <input type="number" name="input-3" width="50%" class="input " value="0" id="txtChildAgeTo" placeholder="Child Age To">
                                </p>





                                <p class="button-height inline-label">
                                    <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="SaveHotelMapping();">Save Mapping</button>
                                </p>
                            </div>

                        </div>

                    </div>

                </div>--%>


	</section>
	<!-- End main content -->



	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="js/libs/jquery-1.10.2.min.js"></script>
	<script src="js/setup.js"></script>

	<!-- Template functions -->
	<script src="js/developr.input.js"></script>
	<script src="js/developr.navigable.js"></script>
	<script src="js/developr.notify.js"></script>
	<script src="js/developr.scroll.js"></script>
	<script src="js/developr.tooltip.js"></script>
	<script src="js/developr.table.js"></script>
    <script src="js/developr.accordions.js"></script>

	<!-- Plugins -->
	<script src="js/libs/jquery.tablesorter.min.js"></script>
	<script src="js/libs/DataTables/jquery.dataTables.min.js"></script>

	<script>

	    // Call template init (optional, but faster if called manually)
	    $.template.init();

	    // Table sort - DataTables
	    var table = $('#sorting-advanced');
	    table.dataTable({
	        'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
	        ],
	        'sPaginationType': 'full_numbers',
	        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
	        'fnInitComplete': function (oSettings) {
	            // Style length select
	            table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
	            tableStyled = true;
	        }
	    });

	    // Table sort - styled
	    $('#sorting-example1').tablesorter({
	        headers: {
	            0: { sorter: false },
	            5: { sorter: false }
	        }
	    }).on('click', 'tbody td', function (event) {
	        // Do not process if something else has been clicked
	        if (event.target !== this) {
	            return;
	        }

	        var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

	        // If click on a special row
	        if (tr.hasClass('row-drop')) {
	            return;
	        }

	        // If there is already a special row
	        if (row.length > 0) {
	            // Un-style row
	            tr.children().removeClass('anthracite-gradient glossy');

	            // Remove row
	            row.remove();

	            return;
	        }

	        // Remove existing special rows
	        rows = tr.siblings('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }

	        // Style row
	        tr.children().addClass('anthracite-gradient glossy');

	        // Add fake row
	        $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

	    }).on('sortStart', function () {
	        var rows = $(this).find('.row-drop');
	        if (rows.length > 0) {
	            // Un-style previous rows
	            rows.prev().children().removeClass('anthracite-gradient glossy');

	            // Remove rows
	            rows.remove();
	        }
	    });

	    // Table sort - simple
	    $('#sorting-example2').tablesorter({
	        headers: {
	            5: { sorter: false }
	        }
	    });

	</script>
</asp:Content>
