﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="InventoryDetails.aspx.cs" Inherits="CutAdmin.InventoryDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="Scripts/InventoryDetails.js?v=1.37"></script>
    <style >
        label span{
            margin: 0px;
            float:left
        }
    </style>
    <script type="text/javascript">
        var date = new Date();
        var time = new Date(date.getTime());
        time.setMonth(date.getMonth() + 1);
        time.setDate(0);
        // var days = time.getDate() > date.getDate() ? time.getDate() - date.getDate() : 0;
        var days = 10;
        $(document).ready(function () {
            $("#datepicker_start").datepicker({
                autoclose: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                onSelect: insertDepartureDate,
                minDate: "dateToday",
            });
            $("#datepicker_end").datepicker({
                dateFormat: "dd-mm-yy",
                autoclose: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
            });
            $("#datepicker_start").datepicker("setDate", new Date());
            var dateObject = $("#datepicker_start").datepicker('getDate', days);
            dateObject.setDate(dateObject.getDate() + days);
            $("#datepicker_end").datepicker("setDate", dateObject);
            var date = $("#datepicker_end").datepicker("setDate", dateObject);
            setTimeout(function () {
                GetInventory();
            }, 1000);
        });


    </script>
    <script>
        var splitMonth = "";
        var lastDay = "";
        function insertDepartureDate() {
            var StartSelDate = $("#datepicker_start").val();
            var splitDate = StartSelDate.split('-');
            var splitMonth = splitDate[1];
            var date = new Date(), y = date.getFullYear(), m = new Date().getMonth()
            var lastDay = new Date(y, splitMonth, 0);
            lastDay = moment(lastDay).format("DD-MM-YYYY")
            $("#datepicker_end").val(lastDay);
            GetInventory();
        }
        var endDate = new Date();
        var endDates = new Date();
        function DateDisable(chk) {
            var dateObject = $("#datepicker_end").datepicker('getDate', '+1d');

            if (chk == 1) {
                $("#datepicker_end").datepicker("destroy");
                $('#datepicker_end').datepicker({
                    dateFormat: 'DD-MM-YYYY',
                    minDate: endDate,
                    onSelect: function (date) {
                        DateDisable(2);
                    }
                });

            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotels Inventory</h1>
            <hr />
        </hgroup>

        <div class="with-padding ">
            <div class="columns">
                <div class="" style="margin-top: 20px">
                     <button class="button  anthracite-gradient" onclick="Previous();" title="Previous"><i class="icon-chevron-left icon-size2"></i></button>
                </div>
                <div class="two-columns">
                    <label class="input-info">Start Date</label><span class="red">*</span>
                    <input class="input ui-autocomplete-input disabled full-width dt1" type="text" id="datepicker_start" name="datepicker_From" style="cursor: pointer" value="" />
                </div>
                <div class="two-columns">
                    <label class="input-info">End Date</label><span class="red">*</span>
                    <input class="input ui-autocomplete-input full-width disabled dt2" type="text" id="datepicker_end" onchange="GetInventory()" style="cursor: pointer" value="" />
                </div>
                <div class="" id="" style="margin-top: 20px">
                     <button   class="button anthracite-gradient" onclick="Next();" title="Next" ><i class="icon-chevron-right icon-size2"></i></button>
                </div>
                <div class="one-columns" id="" style="margin-top: 15px">
                    <label for="Freesale_Id"> <input type="radio" class="radio" name="radio" id="Freesale_Id" onchange="GetInventory()" checked>Freesale</label>
                </div>
                <div class="one-columns" id="" style="margin-top: 15px">
                    <label for="Allocation_Id"> <input type="radio" class="radio" name="radio" id="Allocation_Id" onchange="GetInventory()">Allocation</label>
                </div>
                <div class="one-columns" id="" style="margin-top: 15px">
                    <label for="Allotmnet_Id">  <input type="radio" class="radio" name="radio" id="Allotmnet_Id" onchange="GetInventory()">Allotment</label>
                </div>
                <div class="one-columns" id="" style="margin-top: 15px">
                    <input type="button" class="button anthracite-gradient" id="btn_St" onclick="StartSaleModal();" value="Start Sale" />
                    <input type="button" class="button anthracite-gradient" id="btn_US" style="display:none" onclick="UpdateSaleModal();" value="Update" />
                </div>
                <div class="one-columns" id="" style="margin-top: 15px">
                    <input type="button" class="button anthracite-gradient" id="btn_SS" onclick="StopSaleModal();" value="Stop Sale" />
               
                </div>
                <div class="one-columns" id="AmM" style="margin-top: 15px; display: none">
                    <input type="button" class="button anthracite-gradient" value="Amendment" />
                </div>
            </div>

            <div id="table-scroll" class="table-scroll">
                <div class="respTable" id="div_tbl_InvList">
                </div>
            </div>
        </div>
    </section>

</asp:Content>
