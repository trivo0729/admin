﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MasterOffer.aspx.cs" Inherits="CutAdmin.MasterHotelOffer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%--<script>
         $(function () {
             $("#datepicker").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "dd-mm-yy"
             });
         })
    </script>--%>

    <script src="Scripts/HotelOffer.js?v=2.3"></script>
    <script src="Scripts/MastelHotelOffer.js?v=1.93"></script>
     <!-- Additional styles -->

   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Offer Details</h1>
            <hr>
        </hgroup>
        <div class="with-padding">

            <%--<div class="columns" id="DivAddOfferRate">--%>
                
                       <%--<h3 class="wrapped margin-left white" style="background-color: #006699;width:90%">Add Offer Rate</h3>--%>
                
                <%-- <div class="new-row twelve-columns">
                    <input id="htlnameRate" style="background:none;border:none;color:#006699;font-size:medium;font-weight:bold;width:100%" readonly/>
                </div>--%>
            <div class="columns">
                <%--<div class="new-row one-columns"></div>--%>
                <%--<div class="three-columns">
                        <span class="text-center">Season</span>
                        <select class="full-width  select  expandable-list" name="selectSeason" id="ddlSeason" onchange="ChangeSeason()" style="margin-bottom: 10px;"></select>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_ddlSeason">
                            <b>* This field is required</b></label>
                    </div>--%>
                <%--<div class="three-columns">
                        <span class="text-left">Meal Plan:</span>
                        <br />
                        <select id="SelectMealPlan" class="full-width  select  expandable-list">
                            <option value="-">Select Meal Plan</option>
                            <option value="Room Only">Room Only</option>
                            <option value="Bed & Breakfast">Bed & Breakfast</option>
                            <option value="Half Board">Half Board</option>
                            <option value="Full Board">Full Board</option>
                        </select>
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_SelectMealPlan">
                            <b>* This field is required</b></label>
                    </div>--%>

                <div class="four-columns twelve-columns-mobile" id="NationalityDiv">
                    <%--<h6>Offer Nationality:</h6>--%>
                    <label for="select_OfferNationality" class="label">Offer Nationality <span class="red">*</span></label>
                    <select id="select_OfferNationality" class="full-width select multiple-as-single easy-multiple-selection allow-empty check-list Onataionality" multiple>
                        <%--<option value="-">Select Country</option> <option value="AFGHANISTAN">AFGHANISTAN</option><option value="ALBANIA">ALBANIA</option><option value="ALGERIA">ALGERIA</option><option value="ANDORRA">ANDORRA</option><option value="ANGUILLA">ANGUILLA</option><option value="ARGENTINA">ARGENTINA</option><option value="AUSTRALIA">AUSTRALIA</option><option value="AUSTRIA">AUSTRIA</option><option value="BAHAMAS">BAHAMAS</option><option value="BAHRAIN">BAHRAIN</option><option value="BANGLADESH">BANGLADESH</option><option value="BARBADOS">BARBADOS</option><option value="BELARUS">BELARUS</option><option value="BELGIUM">BELGIUM</option><option value="BELIZE">BELIZE</option><option value="BENIN">BENIN</option><option value="BHUTAN">BHUTAN</option><option value="BOLIVIA">BOLIVIA</option><option value="BOSNIA" and="" herzegovina="">BOSNIA AND HERZEGOVINA</option><option value="BRAZIL">BRAZIL</option><option value="BULGARIA">BULGARIA</option><option value="CAMBODIA">CAMBODIA</option><option value="CANADA">CANADA</option><option value="CHILE">CHILE</option><option value="CHINA">CHINA</option><option value="COLOMBIA">COLOMBIA</option><option value="COSTA" rica="">COSTA RICA</option><option value="CROATIA">CROATIA</option><option value="CYPRUS">CYPRUS</option><option value="CZECH" republic="">CZECH REPUBLIC</option><option value="DENMARK">DENMARK</option><option value="ECUADOR">ECUADOR</option><option value="EGYPT">EGYPT</option><option value="ESTONIA">ESTONIA</option><option value="FIJI">FIJI</option><option value="FINLAND">FINLAND</option><option value="FRANCE">FRANCE</option><option value="GEORGIA">GEORGIA</option><option value="GERMANY">GERMANY</option><option value="GREECE">GREECE</option><option value="GREENLAND">GREENLAND</option><option value="HONG" kong="">HONG KONG</option><option value="INDIA">INDIA</option><option value="INDONESIA">INDONESIA</option><option value="IRELAND">IRELAND</option><option value="ISRAEL">ISRAEL</option><option value="ITALY">ITALY</option><option value="JAMAICA">JAMAICA</option><option value="JAPAN">JAPAN</option><option value="JORDAN">JORDAN</option><option value="KAZAKHSTAN">KAZAKHSTAN</option><option value="KENYA">KENYA</option><option value="KUWAIT">KUWAIT</option><option value="KYRGYZSTAN">KYRGYZSTAN</option><option value="LAOS">LAOS</option><option value="LATVIA">LATVIA</option><option value="LEBANON">LEBANON</option><option value="LIBERIA">LIBERIA</option><option value="LIBYA">LIBYA</option><option value="LIECHTENSTEIN">LIECHTENSTEIN</option><option value="LITHUANIA">LITHUANIA</option><option value="LUXEMBOURG">LUXEMBOURG</option><option value="MACAU">MACAU</option><option value="MACEDONIA">MACEDONIA</option><option value="MALAYSIA">MALAYSIA</option><option value="MALDIVES">MALDIVES</option><option value="MALTA">MALTA</option><option value="MARTINIQUE">MARTINIQUE</option><option value="MAURITANIA">MAURITANIA</option><option value="MAURITIUS">MAURITIUS</option><option value="MEXICO">MEXICO</option><option value="MICRONESIA">MICRONESIA</option><option value="MOLDOVA">MOLDOVA</option><option value="MONACO">MONACO</option><option value="MONGOLIA">MONGOLIA</option><option value="MONTENEGRO">MONTENEGRO</option><option value="MOROCCO">MOROCCO</option><option value="MOZAMBIQUE">MOZAMBIQUE</option><option value="MYANMAR">MYANMAR</option><option value="NAMIBIA">NAMIBIA</option><option value="NEPAL">NEPAL</option><option value="NETHERLANDS">NETHERLANDS</option><option value="NEW" zealand="">NEW ZEALAND</option><option value="NICARAGUA">NICARAGUA</option><option value="NIGER">NIGER</option><option value="NIGERIA">NIGERIA</option><option value="NORWAY">NORWAY</option><option value="OMAN">OMAN</option><option value="PAKISTAN">PAKISTAN</option><option value="PALAU">PALAU</option><option value="PANAMA">PANAMA</option><option value="PARAGUAY">PARAGUAY</option><option value="PERU">PERU</option><option value="PHILIPPINES">PHILIPPINES</option><option value="POLAND">POLAND</option><option value="PORTUGAL">PORTUGAL</option><option value="PUERTO" rico="">PUERTO RICO</option><option value="QATAR">QATAR</option><option value="ROMANIA">ROMANIA</option><option value="RUSSIA">RUSSIA</option><option value="RWANDA">RWANDA</option><option value="SAINT" barthÉlemy="">SAINT BARTHÉLEMY</option><option value="SAINT" kitts="" and="" nevis="">SAINT KITTS AND NEVIS</option><option value="SAMOA">SAMOA</option><option value="SAN" marino="">SAN MARINO</option><option value="SAUDI" arabia="">SAUDI ARABIA</option><option value="SENEGAL">SENEGAL</option><option value="SERBIA">SERBIA</option><option value="SEYCHELLES">SEYCHELLES</option><option value="SINGAPORE">SINGAPORE</option><option value="SLOVAKIA">SLOVAKIA</option><option value="SLOVENIA">SLOVENIA</option><option value="SOUTH" africa="">SOUTH AFRICA</option><option value="SOUTH" korea="">SOUTH KOREA</option><option value="SPAIN">SPAIN</option><option value="SRI" lanka="">SRI LANKA</option><option value="ST" lucia="">ST LUCIA</option><option value="SUDAN">SUDAN</option><option value="SWEDEN">SWEDEN</option><option value="SWITZERLAND">SWITZERLAND</option><option value="SYRIA">SYRIA</option><option value="TAIWAN">TAIWAN</option><option value="TAJIKISTAN">TAJIKISTAN</option><option value="TANZANIA">TANZANIA</option><option value="THAILAND">THAILAND</option><option value="TRINIDAD" and="" tobago="">TRINIDAD AND TOBAGO</option><option value="TUNISIA">TUNISIA</option><option value="TURKEY">TURKEY</option><option value="UGANDA">UGANDA</option><option value="UKRAINE">UKRAINE</option><option value="UNITED" arab="" emirates="">UNITED ARAB EMIRATES</option><option value="UNITED" kingdom="">UNITED KINGDOM</option><option value="UNITED" states="">UNITED STATES</option><option value="URUGUAY">URUGUAY</option><option value="UZBEKISTAN">UZBEKISTAN</option><option value="VANUATU">VANUATU</option><option value="VENEZUELA">VENEZUELA</option><option value="VIETNAM">VIETNAM</option><option value="VIRGIN" islands="" (usa)="">VIRGIN ISLANDS (USA)</option><option value="YEMEN">YEMEN</option><option value="ZAMBIA">ZAMBIA</option><option value="ZIMBABWE">ZIMBABWE</option>--%>
                    </select>
                </div>



                <%--<div class="new-row one-columns"></div>--%>
                <div class="two-columns six-columns-mobile" id="DivRdbDaysPrior">
                    <br />
                    <input type="radio" id="rdb_DaysPrior" value="Normal" checked="checked" name="Rdb" onclick="PriorFixed()" />
                    <label id="lbl_DaysPrior" for="rdb_DaysPrior" class="text-left">Days Prior</label>
                </div>
                <div class="two-columns six-columns-mobile" id="DivRdbFixedDate">
                    <br />
                    <input type="radio" id="rdb_FixedDate" value="Normal" name="Rdb" onclick="PriorFixed()" />
                    <label id="lbl_FixedDate" for="rdb_FixedDate" class="text-left">Book Before</label>
                </div>

                <div class="four-columns" id="DaysPrior">
                    <%--<span class="text-left">Days Prior   </span>--%>
                    <label for="txtDaysPrior" class="label">Days Prior <span class="red">*</span></label>
                    
                    <input type="text" class="input full-width" id="txtDaysPrior" />
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDaysPrior">
                        <b>* This field is required</b></label>
                </div>

                <div class="four-columns" id="FixedDate" style="display: none">
                    <label class="text-left">Fixed Date</label>
                    <br>
                    <input class="input full-width mySelectCalendar" type="text" id="Fixeddatepicker3" readonly="readonly" style="cursor: pointer" />
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Fixeddatepicker3">
                        <b>* This field is required</b></label>
                </div>
            </div>
                <div class="columns">
                            <%--<div class="one-columns"></div>--%>
                            <div class="four-columns" id="div_OfferType">
                                <label class="text-left">Offer Type <span class="red">*</span></label>
                                <br>
                                <select id="ddlOfferType" onchange="OfferTypeChange()" class="full-width  select OfferType">
                                    <option value="Discount">Discount</option>
                                    <option value="Deal">Deal</option>
                                    <option value="Freebi">Freebi</option>
                                </select>
                            </div>
                            <div class="two-columns" id="DivRdbDiscountPer">
                                <br>
                                <input type="radio" id="rdb_DiscountPer" value="Normal" checked="checked" name="type" onclick="DiscountChange()" />
                                 <label for="rdb_DiscountPer" class="text-left">Discount % </label>
                            </div>
                  
                            <div class="two-columns" id="DivRdbDiscountAmount">                               
                                <br>
                                <input type="radio" id="rdb_DiscountAmount" value="Normal" name="type" onclick="DiscountChange()" />
                                 <label for="rdb_DiscountAmount" class="text-left">Discount Amount</label>
                            </div>

                            <div class="four-columns" id="DivItemName" style="display:none">
                                <label class="text-left">Free Item Name <span class="red">*</span></label>
                                <br>
                                <input id="txt_ItemName" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_ItemName">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns" id="DivItemDetails" style="display:none">
                                <label class="text-left">Free Item Details <span class="red">*</span></label>
                                <br>
                                <textarea rows="1" cols="50" name="comment" id="txt_ItemDetails" class="input full-width"></textarea>
                                <%--<input id="txt_ItemDetails" class="form-control" type="text" />--%>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_ItemDetails">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns" id="DivDiscount">
                                <%--<span class="text-left">Discount %: </span>--%>
                                <label for="txtDiscountPer" class="text-left">Discount % <span class="red">*</span></label>
                                <br>
                                <input id="txtDiscountPer" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtDiscountPer">
                                    <b>* This field is required</b></label>
                            </div>
                      
                            <div class="four-columns" id="DivDiscountAmount" style="display:none">
                                <label class="text-left">Discount Amount <span class="red">*</span></label>
                                <br>
                                <input id="txt_DiscountAmount" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_DiscountAmount">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="four-columns" id="DivNewRate" style="display: none">
                                <label class="text-left">New Rate <span class="red">*</span></label>
                                <br>
                                <input id="txtNewRate" class="input full-width" type="text" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtNewRate">
                                    <b>* This field is required</b></label>
                            </div>
                    </div>
                            
                       
                             <%--<div class="new-row one-columns"></div>
                            <div class="five-columns">
                                <span class="text-left">Hotel Offer Code:</span>
                                <br>
                                <input id="txtHotelOfferCode" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtHotelOfferCode">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="five-columns">
                                <span class="text-left">Self Offer Code:</span>
                                <br>
                                <input id="txtHotelCode" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtHotelCode">
                                    <b>* This field is required</b></label>
                            </div>

                             <div class="new-row one-columns"></div>
                            <div class="five-columns" >
                                <span class="text-left">Offer Term:</span>
                                <br>
                                <textarea rows="4" name="comment" id="txtOfferTerm" class="input full-width"></textarea>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtOfferTerm">
                                    <b>* This field is required</b></label>

                            </div>
                            <div class="five-columns" >
                                <span class="text-left">Offer Note:</span>
                                <br>
                                <textarea rows="4"  name="comment" id="txtOfferNote" class="input full-width"></textarea>
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtOfferNote">
                                    <b>* This field is required</b></label>

                            </div>--%>
                <div class="columns">
                    <div class="six-columns twelve-columns-mobile">
                        <label>Hotel Offer Code</label>
                        <div class="input full-width">
                            <input type="text" id="txtHotelOfferCode" class="input-unstyled full-width">
                        </div>

                        <label class="mrgTop">Offer Term</label>
                        <div class="input full-width textarCancel">
                            <textarea name="comment" id="txtOfferTerm" cols="" rows="" class="input-unstyled full-width"></textarea>
                        </div>
                    </div>

                    <div class="six-columns twelve-columns-mobile">
                        <label>Self Offer Code</label>
                        <div class="input full-width">
                            <input type="text" id="txtHotelCode" class="input-unstyled full-width">
                        </div>
                        <label class="mrgTop">Offer Note</label>
                        <div class="input full-width textarCancel">
                            <textarea name="comment" id="txtOfferNote" cols="" rows="" class="input-unstyled full-width"></textarea>
                        </div>
                    </div>

                </div>

                <%-- <div class="new-row one-columns"></div>--%>
                           <%-- <br />--%>
                         <%--   <div class="five-columns">
                                <span class="text-left">Booking Type:</span>
                                <br>
                                <input id="txtBookingType" class="input full-width" type="text">
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txtBookingType">
                                    <b>* This field is required</b></label>
                            </div>--%>
                           <%-- <div class="new-row  eleven-columns">
                                <hr /><br />
                                <input type="button" onclick="AddUpdateOfferDetails()" class="button blue-gradient" value="Submit" id="btn_Popup" style="float: right" />
                            </div>--%>
                  <%--  </div>--%>
                    <!-- END OF TAB 1 -->
           <%-- </div>--%>
		


       
       <%-- <div class="with-padding">--%>
           
            <div class="columns" id="DivAddOffer">

                <%-- <h3 class="wrapped margin-left white" style="background-color: #006699">Create Offer</h3>--%>
                <%--  <div class="new-row twelve-columns">
                    <input id="htlname" style="background:none;border:none;color:#006699;font-size:medium;font-weight:bold;width:100%" readonly/>
                </div>--%>

                <%-- <div class="new-row three-columns">
                    <h6>Room:</h6>
                   <select id="SelectRoomCat" class="full-width  select  expandable-list" onchange="CheckOfferDuplicate()">
                       <option value="-">Select Room</option>
                       <option value="555555555">Standard Room </option>
                       <option value="555555556">Executive Room</option>
                    </select>
                </div>--%>

                <%-- <div class="three-columns">
                    <h6>From Inventory:</h6>
                    <select id="SelectInventory" class="full-width  select  expandable-list">
                                 <option value="DOTW">DOTW</option>
                                 <option value="HotelBeds">HotelBeds</option>
                                 <option value="MGH" selected="selected">MGH</option>
                    </select>
                </div>--%>

                <%--<div class="three-columns">
                   <h6>Offer Nationality:</h6>
                    <select id="select_OfferNationality" class="full-width select multiple-as-single easy-multiple-selection allow-empty check-list" multiple>
                      <option value="-">Select Country</option> <option value="AFGHANISTAN">AFGHANISTAN</option><option value="ALBANIA">ALBANIA</option><option value="ALGERIA">ALGERIA</option><option value="ANDORRA">ANDORRA</option><option value="ANGUILLA">ANGUILLA</option><option value="ARGENTINA">ARGENTINA</option><option value="AUSTRALIA">AUSTRALIA</option><option value="AUSTRIA">AUSTRIA</option><option value="BAHAMAS">BAHAMAS</option><option value="BAHRAIN">BAHRAIN</option><option value="BANGLADESH">BANGLADESH</option><option value="BARBADOS">BARBADOS</option><option value="BELARUS">BELARUS</option><option value="BELGIUM">BELGIUM</option><option value="BELIZE">BELIZE</option><option value="BENIN">BENIN</option><option value="BHUTAN">BHUTAN</option><option value="BOLIVIA">BOLIVIA</option><option value="BOSNIA" and="" herzegovina="">BOSNIA AND HERZEGOVINA</option><option value="BRAZIL">BRAZIL</option><option value="BULGARIA">BULGARIA</option><option value="CAMBODIA">CAMBODIA</option><option value="CANADA">CANADA</option><option value="CHILE">CHILE</option><option value="CHINA">CHINA</option><option value="COLOMBIA">COLOMBIA</option><option value="COSTA" rica="">COSTA RICA</option><option value="CROATIA">CROATIA</option><option value="CYPRUS">CYPRUS</option><option value="CZECH" republic="">CZECH REPUBLIC</option><option value="DENMARK">DENMARK</option><option value="ECUADOR">ECUADOR</option><option value="EGYPT">EGYPT</option><option value="ESTONIA">ESTONIA</option><option value="FIJI">FIJI</option><option value="FINLAND">FINLAND</option><option value="FRANCE">FRANCE</option><option value="GEORGIA">GEORGIA</option><option value="GERMANY">GERMANY</option><option value="GREECE">GREECE</option><option value="GREENLAND">GREENLAND</option><option value="HONG" kong="">HONG KONG</option><option value="INDIA">INDIA</option><option value="INDONESIA">INDONESIA</option><option value="IRELAND">IRELAND</option><option value="ISRAEL">ISRAEL</option><option value="ITALY">ITALY</option><option value="JAMAICA">JAMAICA</option><option value="JAPAN">JAPAN</option><option value="JORDAN">JORDAN</option><option value="KAZAKHSTAN">KAZAKHSTAN</option><option value="KENYA">KENYA</option><option value="KUWAIT">KUWAIT</option><option value="KYRGYZSTAN">KYRGYZSTAN</option><option value="LAOS">LAOS</option><option value="LATVIA">LATVIA</option><option value="LEBANON">LEBANON</option><option value="LIBERIA">LIBERIA</option><option value="LIBYA">LIBYA</option><option value="LIECHTENSTEIN">LIECHTENSTEIN</option><option value="LITHUANIA">LITHUANIA</option><option value="LUXEMBOURG">LUXEMBOURG</option><option value="MACAU">MACAU</option><option value="MACEDONIA">MACEDONIA</option><option value="MALAYSIA">MALAYSIA</option><option value="MALDIVES">MALDIVES</option><option value="MALTA">MALTA</option><option value="MARTINIQUE">MARTINIQUE</option><option value="MAURITANIA">MAURITANIA</option><option value="MAURITIUS">MAURITIUS</option><option value="MEXICO">MEXICO</option><option value="MICRONESIA">MICRONESIA</option><option value="MOLDOVA">MOLDOVA</option><option value="MONACO">MONACO</option><option value="MONGOLIA">MONGOLIA</option><option value="MONTENEGRO">MONTENEGRO</option><option value="MOROCCO">MOROCCO</option><option value="MOZAMBIQUE">MOZAMBIQUE</option><option value="MYANMAR">MYANMAR</option><option value="NAMIBIA">NAMIBIA</option><option value="NEPAL">NEPAL</option><option value="NETHERLANDS">NETHERLANDS</option><option value="NEW" zealand="">NEW ZEALAND</option><option value="NICARAGUA">NICARAGUA</option><option value="NIGER">NIGER</option><option value="NIGERIA">NIGERIA</option><option value="NORWAY">NORWAY</option><option value="OMAN">OMAN</option><option value="PAKISTAN">PAKISTAN</option><option value="PALAU">PALAU</option><option value="PANAMA">PANAMA</option><option value="PARAGUAY">PARAGUAY</option><option value="PERU">PERU</option><option value="PHILIPPINES">PHILIPPINES</option><option value="POLAND">POLAND</option><option value="PORTUGAL">PORTUGAL</option><option value="PUERTO" rico="">PUERTO RICO</option><option value="QATAR">QATAR</option><option value="ROMANIA">ROMANIA</option><option value="RUSSIA">RUSSIA</option><option value="RWANDA">RWANDA</option><option value="SAINT" barthÉlemy="">SAINT BARTHÉLEMY</option><option value="SAINT" kitts="" and="" nevis="">SAINT KITTS AND NEVIS</option><option value="SAMOA">SAMOA</option><option value="SAN" marino="">SAN MARINO</option><option value="SAUDI" arabia="">SAUDI ARABIA</option><option value="SENEGAL">SENEGAL</option><option value="SERBIA">SERBIA</option><option value="SEYCHELLES">SEYCHELLES</option><option value="SINGAPORE">SINGAPORE</option><option value="SLOVAKIA">SLOVAKIA</option><option value="SLOVENIA">SLOVENIA</option><option value="SOUTH" africa="">SOUTH AFRICA</option><option value="SOUTH" korea="">SOUTH KOREA</option><option value="SPAIN">SPAIN</option><option value="SRI" lanka="">SRI LANKA</option><option value="ST" lucia="">ST LUCIA</option><option value="SUDAN">SUDAN</option><option value="SWEDEN">SWEDEN</option><option value="SWITZERLAND">SWITZERLAND</option><option value="SYRIA">SYRIA</option><option value="TAIWAN">TAIWAN</option><option value="TAJIKISTAN">TAJIKISTAN</option><option value="TANZANIA">TANZANIA</option><option value="THAILAND">THAILAND</option><option value="TRINIDAD" and="" tobago="">TRINIDAD AND TOBAGO</option><option value="TUNISIA">TUNISIA</option><option value="TURKEY">TURKEY</option><option value="UGANDA">UGANDA</option><option value="UKRAINE">UKRAINE</option><option value="UNITED" arab="" emirates="">UNITED ARAB EMIRATES</option><option value="UNITED" kingdom="">UNITED KINGDOM</option><option value="UNITED" states="">UNITED STATES</option><option value="URUGUAY">URUGUAY</option><option value="UZBEKISTAN">UZBEKISTAN</option><option value="VANUATU">VANUATU</option><option value="VENEZUELA">VENEZUELA</option><option value="VIETNAM">VIETNAM</option><option value="VIRGIN" islands="" (usa)="">VIRGIN ISLANDS (USA)</option><option value="YEMEN">YEMEN</option><option value="ZAMBIA">ZAMBIA</option><option value="ZIMBABWE">ZIMBABWE</option>
                    </select>
                </div>--%>

                <%--  <div class="three-columns">
                    <h6>Supplier</h6>
                    <select id="SelectSupplier" class="full-width  select  expandable-list">
                        <option value="1">HotelBeds</option>
                        <option value="2">MGH</option>
                        <option value="3">Dotw</option>
                        <option value="8">Expedia</option>
                        <option value="16">ClickUrTrip</option>
                        <option value="25" selected="">From Hotel</option>
                    </select>
                </div>--%>



                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Season Dates</h3>
                </div>


                <%-- <div class="line2"></div>--%>
                <div id="SeasonUI" style="width: 100%">
                </div>

                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Block Dates</h3>
                </div>

                <%--<div class="line2"></div>--%>
                <div id="SpecialUI" style="width: 100%">
                </div>

                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Block Days</h3>
                </div>
                <%--<div class="line2"></div>--%>
                <div class="one-columns">
                    <input type="checkbox" id="Mon" class="chk_blockdays">
                    <label for="Mon" class="label">Mon</label>
                </div>

                <div class="one-columns">
                    <input type="checkbox" id="Tue" class="chk_blockdays">
                    <label for="Tue" class="label">Tue</label>
                </div>
                <div class="one-columns">
                    <input type="checkbox" id="Wed" class="chk_blockdays">
                    <label for="Wed" class="label">Wed</label>
                </div>
                <div class="one-columns">
                    <input type="checkbox" id="Thu" class="chk_blockdays">
                    <label for="Thu" class="label">Thu</label>
                </div>
                <div class="one-columns">
                    <input type="checkbox" id="Fri" class="chk_blockdays">
                    <label for="Fri" class="label">Fri</label>
                </div>
                <div class="one-columns">
                    <input type="checkbox" id="Sat" class="chk_blockdays">
                    <label for="Sat" class="label">Sat</label>
                </div>
                <div class="one-columns">
                    <input type="checkbox" id="Sun" class="chk_blockdays">
                    <label for="Sun" class="label">Sun</label>
                </div>
                <%--<div class="new-row twelve-columns">
                    <label id="lbl_BlockDays" style="color:red;display:none">* You can not block all days</label>
                </div>--%>





                <div class="new-row twelve-columns">
                    <%--<button type="button" class="button glossy blue-gradient float-right"  id="btn" onclick="AddOffer()">Add Offer</button>--%>
                    <button type="button" class="button glossy anthracite-gradient float-right" id="btn" onclick="AddOfferMaster()">Add Offer</button>
                    <button type="button" style="display: none" class="button glossy anthracite-gradient float-right" id="btn_update" onclick="UpdateOfferMaster()">Update Offer</button>
                </div>
            </div>
           

           <%-- <div class="columns" id="DivOfferList" style="min-height: 200px;display:none;">--%>
            <div class="columns" id="DivOfferList" style="min-height: 200px">
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Season List</h3>
                </div>
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbl_Hotel">
                        <thead>
                            <tr>
                                <th align="center" style="width: 5%">S.No</th>
                                <th align="center" style="width: 5%">Season Name</th>
                                <th align="center" style="width: 5%">Valid From</th>
                                <th align="center" style="width: 5%">Valid To</th>
                                <th align="center" style="width: 5%">Offer Type</th>
                                 <th align="center" style="width: 5%">Days Prior</th>
                                <th align="center" style="width: 5%">Book Before</th>
                                <th align="center" style="width: 5%">Discount Amt</th>
                                <th align="center" style="width: 5%">Discount Per</th>
                                 <th align="center" style="width: 5%">New Rate</th>
                                 <th align="center" style="width: 5%">Date Type</th>
                                <th align="center" style="width: 5%">Edit</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>


                <div class="new-row twelve-columns" style="display:none" none">

                    <div class="three-columns">
                        <span class="text-left">Check-in</span>
                        <input class="form-control mySelectCalendar" type="text" id="CheckInDate" readonly="readonly" style="cursor: pointer" />
                        <label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker4">
                            <b>* This field is required</b></label>
                    </div>
                    <div class="three-columns">
                        <span class="text-left">Check-out</span>
                        <br />
                        <input class="form-control mySelectCalendar" type="text" id="CheckOutDate" readonly="readonly" style="cursor: pointer" />
                    </div>
                    <div class="two-columns">
                        <span class="text-left">Total Nights</span>
                        <br />
                        <select id="Select_TotalNights" class="form-control">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>
            </div>
     
             
         </div>


                       
	</section>
	<!-- End main content -->


  

       <script type="text/javascript">

           $(function () {
               $("#Fixeddatepicker3").datepicker({
                   changeMonth: true,
                   changeYear: true,
                   dateFormat: "dd-mm-yy",
                   //onSelect: insertDepartureDate,
                   //dateFormat: "yy-m-d",
                   minDate: "dateToday",
                   //maxDate: "+3M +10D"
               });
           });
          </script>


	

    
</asp:Content>

