﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="OTBReporting.aspx.cs" Inherits="CutAdmin.OTBReporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/OTBReporting.js"></script>
    <script src="Scripts/OtbEmail.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $("#dteFrom").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "dd-mm-yy"
             });
             $("#dteTo").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "dd-mm-yy"
             });
             $("#dteTravel").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "dd-mm-yy"
             });
             $("#txt_RefNo").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "../Handler/OTBHandler.asmx/GetOTBByRefrence",
                         data: "{'RefrenceNo':'" + document.getElementById('txt_RefNo').value + "'}",
                         dataType: "json",
                         success: function (data) {
                             var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                             response(result);
                         },
                         error: function (result) {
                             alert("No Match");
                         }
                     });
                 },
                 minLength: 3,
                 select: function (event, ui) {
                     $('#hdnDCode').val(ui.item.id);
                 }
             });
             $("#txtFirst").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "../Handler/OTBHandler.asmx/GetOTBByNameOnAdmin",
                         data: "{'FirstName':'" + document.getElementById('txtFirst').value + "'}",
                         dataType: "json",
                         success: function (data) {
                             var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                             response(result);
                         },
                         error: function (result) {
                             alert("No Match");
                         }
                     });
                 },
                 minLength: 3,
                 select: function (event, ui) {
                     $('#hdnDCode').val(ui.item.id);
                 }
             });
             $("#txtLast").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "../Handler/OTBHandler.asmx/GetOTBByLastName",
                         data: "{'LastName':'" + document.getElementById('txtLast').value + "'}",
                         dataType: "json",
                         success: function (data) {
                             var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                             response(result);
                         },
                         error: function (result) {
                             alert("No Match");
                         }
                     });
                 },
                 minLength: 3,
                 select: function (event, ui) {
                     $('#hdnDCode').val(ui.item.id);
                 }
             });
             $("#txtPnr").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "../Handler/OTBHandler.asmx/GetOTBByPnrNo",
                         data: "{'PnrNo':'" + document.getElementById('txtPnr').value + "'}",
                         dataType: "json",
                         success: function (data) {
                             var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                             response(result);
                         },
                         error: function (result) {
                             alert("No Match");
                         }
                     });
                 },
                 minLength: 3,
                 select: function (event, ui) {
                     $('#hdnDCode').val(ui.item.id);
                 }
             });
             $("#txtAgencyName").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "../Handler/OTBHandler.asmx/GetOTBByAgencyName",
                         data: "{'AgencyName':'" + document.getElementById('txtAgencyName').value + "'}",
                         dataType: "json",
                         success: function (data) {
                             var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                             response(result);
                         },
                         error: function (result) {
                             alert("No Match");
                         }
                     });
                 },
                 minLength: 3,
                 select: function (event, ui) {
                     $('#hdn_uid').val(ui.item.id);
                 }
             });
         });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>OTBReporting</h1>
            <h2><a href="#" class="addnew"><i class="formTab fa fa-angle-left"></i></a></h2>
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Application Date From:</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="dteFrom" class="input-unstyled datepicker" value="">
                            </span>
                        </div>
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Application Date To:</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="dteTo" class="input-unstyled datepicker" value="">
                            </span>
                        </div>
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Travel Date:</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="dteTravel" class="input-unstyled datepicker" value="">
                            </span>
                        </div>

                    </div>
                    <div class="columns">

                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>First Name:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txtFirst">
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Last Name:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txtLast">
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Visa Type:</label>
                            <div class="full-width button-height">
                                <select id="selService" class="select" onchange="Notification()">
                                     <option value="-" selected="selected">--Salect Visa Type--</option>
                                    <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                    <option value="1">14 days Service VISA</option>
                                    <option value="2">30 days Tourist Single Entry</option>
                                    <option value="3">30 days Tourist Multiple Entry</option>
                                    <option value="4">90 days Tourist Single Entry</option>
                                    <option value="5">90 days Tourist Multiple Entry</option>
                                </select>
                            </div>
                        </div>
                        <%--<div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Visa Type:</label>
                            <div class="full-width button-height">
                                <select id="selService" class="form-control" onchange="Notification()">
                                    <option value="-" selected="selected">--Salect Visa Type--</option>
                                    <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                    <option value="1">14 days Service VISA</option>
                                    <option value="2">30 days Tourist Single Entry</option>
                                    <option value="3">30 days Tourist Multiple Entry</option>
                                    <option value="4">90 days Tourist Single Entry</option>
                                    <option value="5">90 days Tourist Multiple Entry</option>
                                </select>
                            </div>
                        </div>--%>


                      

                    </div>


                    <div class="columns">
                          <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Airline:</label>
                            <div class="full-width button-height">
                                <select id="Sel_AirLineIn" class="select" onchange="GetOtbCharges()">
                                    <option value="-" selected="selected">-Select AirLine-</option>
                                    <option value="G9">Air Arabia</option>
                                    <option value="AI">Air India</option>
                                    <option value="Ix">Air India Express</option>
                                    <option value="9w" id="OptJet">Jet Airways Airlines</option>
                                    <option value="6E">Indigo Airlines </option>
                                    <option value="Wy">Oman Airways</option>
                                    <option value="SG">Spicejet Airlines</option>
                                    <option value="EK" id="OptEmirates">Emirates Airlines</option>
                                    <option value="EY">Etihad Airways</option>
                                    <option value="FZ">Fly Dubai</option>
                                </select>
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Pnr No:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txtPnr">
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Agency Name:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txtAgencyName">
                                 <input type="hidden" id="hdn_uid" />
                            </div>
                        </div>


                        <div class="three-columns twelve-columns-mobile four-columns-tablet text-alignright">
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            &nbsp;
                            <button type="button" class="button anthracite-gradient" onclick="GetAllOtbDetails()">Back</button>
                        </div>

                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_OtbDetails">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Date</th>
                            <th scope="col" class="align-center">Ref. No.</th>
                            <th scope="col" class="align-center">Agent Name </th>
                            <th scope="col" class="align-center">Applicant</th>
                            <th scope="col" class="align-center">Doc.No</th>
                            <th scope="col" class="align-center">Visa Type</th>
                            <th scope="col" class="align-center">Airlines</th>
                            <th scope="col" class="align-center">Travel Date</th>
                            <th scope="col" class="align-center">Status</th>
                            <th scope="col" class="align-center">Invoice</th>
                            <th scope="col" class="align-center">Supplier</th>

                        </tr>

                    </thead>

                    <tbody>
                        <%--<tr>
                            <td class="align-center">17-08-2018</td>
                            <td class="align-center">OTB-978839276</td>
                            <td class="align-center">Arati Tour and Travels</td>
                            <td class="align-center">RAKESH PANDA x (1 Pax)</td>
                            <td class="align-center">301 / 2018 / 87 / 0276940</td>
                            <td class="align-center">30 days</td>
                            <td class="align-center">Air Arabia</td>
                            <td class="align-center">20-08-2018</td>
                            <td class="align-center"><a href="#" onclick="RecivedApp();">Recived</a></td>
                            <td class="align-center">View</td>
                            <td class="align-center">Air Arabia</td>
                        </tr>--%>
                    </tbody>

                </table>
            </div>
        </div>

    </section>
    <!-- End main content -->
    <script>
        jQuery(document).ready(function () {
            jQuery('.formTab').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
        jQuery(".formTab").toggleClass("fa-angle-right");
        jQuery(".formTab").removeClass("fa-angle-left");


    </script>

</asp:Content>
