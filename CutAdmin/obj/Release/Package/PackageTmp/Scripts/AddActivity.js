﻿var sNId;

var Country;
var City;
var activityname;
var subtitle;
var Location;
var Description;
var Attraction = '';
var longitude;
var TourNote;
var lattitude;
var id;
var Kid1StartRange;
var Kid1EndRange;
var Kid2StartRange;
var Kid2EndRange;
var Kid1Range;
var Kid2Range;
var ChildrenAllowed;
var ChildAllowed = "Yes";
var ChildAgeFrom;
var ChildAgeUpTo;
var SmallChildAgeUpTo;
var ChildMinHight;
var Infant;
var MaxNoofInfant = 0;
var ActivityType;
var PriorityType;



$(function () {

    loadcheckbox();
    GetAllActivity();
    //$('#ddlcountry').change(function () {
    //    var sndcountry = $('#ddlcountry').val();
    //    GetCity(sndcountry);
    //});

    if (location.href.indexOf('?') != -1) {

        id = GetQueryStringParams('id');
        if (id != undefined) {
            GetActivity(id);
        }
        //Success(id)
        //GetActivity(id);
    }



    $('[data-toggle="tooltip"]').tooltip()
    // AddDates();
    Getpriority();

    $('.wizard #Actdetail').on('wizardleave', function () {
        //$(".wizard-step").click(function () {
        // var fields = $("input[name='ActivityName']").serializeArray();
        var fields = $('#txt_Activity').val();
        var fields1 = $('#txt_subtitle').val();
        var fields2 = $('#ddlcountry').val();
        // var fields3 = $('#txt_Activity').val();
        var fields4 = $('#txt_location').val();
        var fields5 = $('#txt_AreaRemarks').val();

        if (fields == "") {
            Success('Please Enter Activity Name!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields1 == "") {
            Success('Please Enter Sub Title!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields2 == "") {
            Success('Please Select Country!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields4 == "") {
            Success('Please Enter Location!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields5 == "") {
            Success('Please Enter Description!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (pCities == "") {
            Success('Please Select Atleast one City!');
            $(".wizard-prev ").click();
            return false;
        }

        if (id == undefined) {
            for (var i = 0; i < ActTable.length; i++) {
                //var as = Memberemail[i].Email;
                if (ActTable[i].Act_Name == fields && ActTable[i].Sub_Title == fields1 && ActTable[i].Country == fields2 && ActTable[i].Location == fields4) {
                    Success("Activity Already Exist Please Add Another Activity");
                    return false;

                }
            }
        }


    })


    $('.wizard #tab_ChildPolicy').on('wizardleave', function () {
        if (ChildAllowed == "Yes") {
            var fields = $('#ddlChildAgeFrom').val();
            var fields1 = $('#ddlChildAgeUpTo').val();
            var fields2 = $('#ddlSmallChildAgeUpTo').val();


            if (fields == "") {
                Success('Please Select Child Age From!');
                // $(".wizard-prev ").click();
                //$(".wizard-next ").click();
                return false;
            }
            else if (fields1 == null) {
                Success('Please Select Child Age Upto!');
                //  $(".wizard-prev ").click();
                return false;
            }
            else if (fields2 == null) {
                Success('Please Select Smaill Child Age Upto!');
                // $(".wizard-prev ").click();
                return false;
            }
        }

    })


    $('.wizard #tab_Types').on('wizardleave', function () {
        var fields = $('#idAttraction').text();

        if (fields == "") {
            Success('Please Enter Atleast One Attraction!');
            //$(".wizard-prev ").click();
            return false;
        }
        if ($('#Chk_TktOnly').prop("checked") == false && $('#Chk_Sic').prop("checked") == false) {

            Success('Please Select Atleast One Activity Type!');

            // $(".wizard-prev ").click();
            return false;
        }


        var TourTypeChecked = $("input[class='chk_TourType']:checked").length;
        if (TourTypeChecked < 1) {
            Success('Please Select Atleast One Tour Type!');

            // $(".wizard-prev ").click();
            return false;
        }



    })


    $('.wizard #tab_Timing').on('wizardleave', function () {

        //var DaysChecked = $("input[name='checkbox-2']:checked").length;
        //if (DaysChecked < 1) {
        //    alert('Please Select Atleast One Operation Day!');

        //    // $(".wizard-prev ").click();
        //    return false;
        //}
        
       

        if (FullYear == "No") {
            // var fields = $("input[name='ActivityName']").serializeArray();
            var fields = $('.dt1').val();
            var fields1 = $('.dt2').val();

            if (fields == "") {
                Success('Please Select Date Operating From!');
                //$(".wizard-prev ").click();
                return false;
            }
            if (fields1 == "") {
                Success('Please Select Date Operating Till!');
                //$(".wizard-prev ").click();
                return false;
            }
        }


        if ($("#chk_All").prop("checked") || $("#Chk_TktOnly").prop("checked")) {
            var DaysCheckedTKT = $("input[name='checkbox-TKT']:checked").length;
            if (DaysCheckedTKT < 1) {
                Success('Please Select Atleast One Operation Day For TKT!');
                return false;
            }
        }
        if ($("#chk_All").prop("checked") || $("#Chk_Sic").prop("checked")) {
            var DaysCheckedSIC = $("input[name='checkbox-SIC']:checked").length;
            if (DaysCheckedSIC < 1) {
                Success('Please Select Atleast One Operation Day For SIC!');
                return false;
            }
        }

        //if ($("#chk_All").prop("checked")) {
        //    var DaysCheckedPVT = $("input[name='checkbox-PVT']:checked").length;
        //    if (DaysCheckedPVT < 1) {
        //        alert('Please Select Atleast One Operation Day For PVT!');
        //        return false;
        //    }
        //}

        if (Slot == "Yes") {
            var fields2 = $('.name').val();
            var fields3 = $('.PriorityType').val();
            var fields4 = $('.TourStartHours').val();
            var fields5 = $('.TourStartMin').val();
            var fields6 = $('.TourEndHours').val();
            var fields7 = $('.TourEndMin').val();



            if (fields2 == "") {
                Success('Please Enter Slot Name!');
                //$(".wizard-prev ").click();
                return false;
            }
            if (fields3 == "") {
                Success('Please Enter Priority!');
                // $(".wizard-prev ").click();
                return false;
            }
            if (fields4 == "") {
                Success('Please Select Tour Start Hour!');
                // $(".wizard-prev ").click();
                return false;
            }
            if (fields5 == "") {
                Success('Please Select Tour Start Minutes!');
                // $(".wizard-prev ").click();
                return false;
            }
            if (fields6 == "") {
                Success('Please Select Tour End Hour!');
                //  $(".wizard-prev ").click();
                return false;
            }
            if (fields7 == "") {
                Success('Please Select Tour End Minutes!');
                // $(".wizard-prev ").click();
                return false;
            }

            if (Sharing == true) {
                var fields8 = $('.PickUpFrom').val();
                var fields9 = $('.PickUpTimeHours').val();
                var fields10 = $('.PickUpTimeMin').val();
                var fields11 = $('.DropOffAt').val();
                var fields12 = $('.DropOffTimeHours').val();
                var fields13 = $('.DropOffTimeMin').val();

                if (fields8 == "") {
                    Success('Please Select Pick Up From Location!');
                    // $(".wizard-prev ").click();
                    return false;
                }
                if (fields9 == "") {
                    Success('Please Select Pick Up Time Hour!');
                    // $(".wizard-prev ").click();
                    return false;
                }
                if (fields10 == "") {
                    Success('Please Select Pick Up Time Minutes!');
                    // $(".wizard-prev ").click();
                    return false;
                }
                if (fields11 == "") {
                    Success('Please Select Drop Off At Location!');
                    // $(".wizard-prev ").click();
                    return false;
                }
                if (fields12 == "") {
                    Success('Please Select Drop Off Time Hour!');
                    // $(".wizard-prev ").click();
                    return false;
                }
                if (fields13 == "") {
                    Success('Please Select Drop Off Time Minutes!');
                    //  $(".wizard-prev ").click();
                    return false;
                }
            }

        }
    })


    $('.wizard #tab_Policy').on('wizardleave', function () {
        var fields = $('#txt_MinCapacity').val();

        if (fields == "") {
            Success('Please select minimum capacity!');
            //$(".wizard-prev ").click();
            return false;
        }


    })


   // $("#Actdetail .wizard-next").click(function ()
    $('#Actdetail .wizard-next').on('wizardleave', function () {
        // var fields = $("input[name='ActivityName']").serializeArray();
        var fields = $('#txt_Activity').val();
        var fields1 = $('#txt_subtitle').val();
        var fields2 = $('#ddlcountry').val();
        // var fields3 = $('#txt_Activity').val();
        var fields4 = $('#txt_location').val();
        var fields5 = $('#txt_AreaRemarks').val();

        if (fields == "") {
            Success('Please Enter Activity Name!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields1 == "") {
            Success('Please Enter Sub Title!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields2 == "") {
            Success('Please Select Country!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields4 == "") {
            Success('Please Enter Location!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (fields5 == "") {
            Success('Please Enter Description!');
            $(".wizard-prev ").click();
            return false;
        }
        else if (pCities == "") {
            Success('Please Select Atleast one City!');
            $(".wizard-prev ").click();
            return false;
        }

    })
    //var chlallw = $('#ddlChildrenAllowed option:selected').val();
    //if (chlallw == "Yes" && chlallw!=undefined)

    $("#tab_ChildPolicy .wizard-next").on('wizardleave', function () {
        // var fields = $("input[name='ActivityName']").serializeArray();
        if (ChildAllowed == "Yes") {
            var fields = $('#ddlChildAgeFrom').val();
            var fields1 = $('#ddlChildAgeUpTo').val();
            var fields2 = $('#ddlSmallChildAgeUpTo').val();


            if (fields == "") {
                Success('Please Select Child Age From!');
                $(".wizard-prev ").click();
                //$(".wizard-next ").click();
                return false;
            }
            else if (fields1 == null) {
                Success('Please Select Child Age Upto!');
                $(".wizard-prev ").click();
                return false;
            }
            else if (fields2 == null) {
                Success('Please Select Smaill Child Age Upto!');
                $(".wizard-prev ").click();
                return false;
            }
        }



    })
    //}

    $("#tab_Types .wizard-next").on('wizardleave', function () {
        // var fields = $("input['.lbl_Atraction']").serializeArray();
        var fields = $('#idAttraction').text();

        if (fields == "") {
            Success('Please Enter Atleast One Attraction!');
            $(".wizard-prev ").click();
            return false;
        }
        if ($('#Chk_TktOnly').prop("checked") == false && $('#Chk_Sic').prop("checked") == false) {

            Success('Please Select Atleast One Activity Type!');

            $(".wizard-prev ").click();
            return false;
        }


        var TourTypeChecked = $("input[class='chk_TourType']:checked").length;
        if (TourTypeChecked < 1) {
            Success('Please Select Atleast One Tour Type!');

            $(".wizard-prev ").click();
            return false;
        }
        //if ($('.chk_TourType').prop("checked")==false) {
        //    alert('Please Select Atleast One Tour Type!');

        //    $(".wizard-prev ").click();
        //    return false;
        //}



    })


    $("#tab_Timing .wizard-next").on('wizardleave', function () {


        //var DaysChecked = $("input[name='checkbox-2']:checked").length;
        //if (DaysChecked < 1) {
        //    alert('Please Select Atleast One Operation Day!');

        //    $(".wizard-prev ").click();
        //    return false;
        //}



        //if ($('.chk_OperationDays').prop("checked") == false) {

        //    alert('Please Select Atleast One Operation Day!');

        //    $(".wizard-prev ").click();
        //    return false;
        //}

        if (FullYear == "No") {
            // var fields = $("input[name='ActivityName']").serializeArray();
            var fields = $('.dt1').val();
            var fields1 = $('.dt2').val();

            if (fields == "") {
                Success('Please Select Date Operating From!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields1 == "") {
                Success('Please Select Date Operating Till!');
                $(".wizard-prev ").click();
                return false;
            }
        }


        if ($("#chk_All").prop("checked") || $("#Chk_TktOnly").prop("checked")) {
            var DaysCheckedTKT = $("input[name='checkbox-TKT']:checked").length;
            if (DaysCheckedTKT < 1) {
                Success('Please Select Atleast One Operation Day For TKT!');
                $(".wizard-prev ").click();
                return false;
            }
        }
        if ($("#chk_All").prop("checked") || $("#Chk_Sic").prop("checked")) {
            var DaysCheckedSIC = $("input[name='checkbox-SIC']:checked").length;
            if (DaysCheckedSIC < 1) {
                Success('Please Select Atleast One Operation Day For SIC!');
                $(".wizard-prev ").click();
                return false;
            }
        }

        //if ($("#chk_All").prop("checked")) {
        //    var DaysCheckedPVT = $("input[name='checkbox-PVT']:checked").length;
        //    if (DaysCheckedPVT < 1) {
        //        alert('Please Select Atleast One Operation Day For PVT!');
        //        $(".wizard-prev ").click();
        //        return false;
        //    }
        //}


        if (Slot == "Yes") {
            var fields2 = $('.name').val();
            var fields3 = $('.PriorityType').val();
            var fields4 = $('.TourStartHours').val();
            var fields5 = $('.TourStartMin').val();
            var fields6 = $('.TourEndHours').val();
            var fields7 = $('.TourEndMin').val();



            if (fields2 == "") {
                Success('Please Enter Slot Name!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields3 == "") {
                Success('Please Enter Priority!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields4 == "") {
                Success('Please Select Tour Start Hour!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields5 == "") {
                Success('Please Select Tour Start Minutes!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields6 == "") {
                Success('Please Select Tour End Hour!');
                $(".wizard-prev ").click();
                return false;
            }
            if (fields7 == "") {
                Success('Please Select Tour End Minutes!');
                $(".wizard-prev ").click();
                return false;
            }

            if (Sharing == true) {
                var fields8 = $('.PickUpFrom').val();
                var fields9 = $('.PickUpTimeHours').val();
                var fields10 = $('.PickUpTimeMin').val();
                var fields11 = $('.DropOffAt').val();
                var fields12 = $('.DropOffTimeHours').val();
                var fields13 = $('.DropOffTimeMin').val();

                if (fields8 == "") {
                    Success('Please Select Pick Up From Location!');
                    $(".wizard-prev ").click();
                    return false;
                }
                if (fields9 == "") {
                    Success('Please Select Pick Up Time Hour!');
                    $(".wizard-prev ").click();
                    return false;
                }
                if (fields10 == "") {
                    Success('Please Select Pick Up Time Minutes!');
                    $(".wizard-prev ").click();
                    return false;
                }
                if (fields11 == "") {
                    Success('Please Select Drop Off At Location!');
                    $(".wizard-prev ").click();
                    return false;
                }
                if (fields12 == "") {
                    Success('Please Select Drop Off Time Hour!');
                    $(".wizard-prev ").click();
                    return false;
                }
                if (fields13 == "") {
                    Success('Please Select Drop Off Time Minutes!');
                    $(".wizard-prev ").click();
                    return false;
                }
            }

        }

    })

    $("#tab_Policy .wizard-next").on('wizardleave', function () {
        // var fields = $("input['.lbl_Atraction']").serializeArray();
        var fields = $('#txt_MinCapacity').val();

        if (fields == "") {
            Success('Please select minimum capacity!');
            $(".wizard-prev ").click();
            return false;
        }


    })


    //$("#tab_Policy .wizard-next").click(function () {
    //    // var fields = $("input['.lbl_Atraction']").serializeArray();

    //    var fields = $('#txt_tournote').val();

    //    if (fields == "") {
    //        alert('Please Enter Tour Note!');
    //        $(".wizard-prev ").click();
    //        return false;
    //    }

    //})


});

function loadcheckbox() {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetTourTypes",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arTypeList = result.tblType;
                if (arTypeList.length > 0) {
                  
                    var trForms = '<tbody>';
                    for (i = 0; i < arTypeList.length; i = i + 6) {
                        if (i < arTypeList.length) {
                            trForms += '<tr class="spaceUnder">';
                            trForms += '<td ><input id="chk' + arTypeList[i].Sid + '" type="checkbox" class="chk_TourType" value="' + arTypeList[i].TourType + '" /><label class="" for="chk' + arTypeList[i].Sid + '">' + arTypeList[i].TourType + '</label></td>';
                            if ((i + 1) < arTypeList.length)
                                trForms += '<td ><input id="chk' + arTypeList[i + 1].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 1].TourType + '"/><label class="" for="chk' + arTypeList[i + 1].Sid + '">' + arTypeList[i + 1].TourType + '</label></td>';
                            if ((i + 2) < arTypeList.length)
                                trForms += '<td ><input id="chk' + arTypeList[i + 2].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 2].TourType + '"/><label class="" for="chk' + arTypeList[i + 2].Sid + '">' + arTypeList[i + 2].TourType + '</label></td>';
                            if ((i + 3) < arTypeList.length)
                                trForms += '<td ><input id="chk' + arTypeList[i + 3].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 3].TourType + '"/><label class="" for="chk' + arTypeList[i + 3].Sid + '">' + arTypeList[i + 3].TourType + '</label></td>';
                            if ((i + 4) < arTypeList.length)
                                trForms += '<td ><input id="chk' + arTypeList[i + 4].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 4].TourType + '"/><label class="" for="chk' + arTypeList[i + 4].Sid + '">' + arTypeList[i + 4].TourType + '</label></td>';
                            if ((i + 5) < arTypeList.length)
                                trForms += '<td ><input id="chk' + arTypeList[i + 5].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 5].TourType + '"/><label class="" for="chk' + arTypeList[i + 5].Sid + '">' + arTypeList[i + 5].TourType + '</label></td>';
                            trForms += '</tr>';

                        }
                    }
                    trForms += '</tbody>';
                    $("#tblForms").append(trForms);
                    $('input[type=checkbox]').attr("disabled", false);
                }
            }
        },

    });
}



var ActTable = "";
function GetAllActivity() {

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetAllActivity",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                ActTable = result.dtTable;


            }
            else {
                Success("An error occured !!!");

            }
        },
        error: function () {

        }
    });
}


function Save() {
    // alert("id got")
    if (id != undefined) {
        //alert(7);
        //Update_Click(id);
        UpdateActivity(id);
    }
    else {

        SaveActivity();
        //AddActivity();
    }
}

function DeleteActivity() {
    // alert(id)
    $.ajax({
        type: "POST",
        url: "../ActivityHandller.asmx/DeleteActivity",
        data: '{"id":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                // window.location.href = "login.aspx"; // Session end
                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                $("#Dialog_Deletecandidate").dialog("close");
                Success("Priority deleted successfully");
                // GetActivity(id);
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}

function Getpriority() {

    $.ajax({
        url: "ActivityHandller.asmx/Getpriority",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_MatchSummary = result.dtresult;

                if (arr_MatchSummary.length > 0) {
                    $("#ddlPriorityType").empty();
                    //var ddlRequest = '<option selected="selected" value="">Select Priority</option>';
                    var ddlRequest;
                    var ddlPriority = "";
                    for (i = 0; i < arr_MatchSummary.length; i++) {
                        ddlRequest += '<option value="' + arr_MatchSummary[i].PriorityType + '">' + arr_MatchSummary[i].PriorityType + '</option>';
                        ddlPriority += '<option value="' + arr_MatchSummary[i].PriorityType + '">' + arr_MatchSummary[i].PriorityType + '</option>';

                    }
                    $("#ddlPriorityType").append(ddlRequest);
                    $("#Select_Priority").append(ddlPriority);
                }
            }
            if (result.retCode == 0) {
                $("#ddlPriorityType").empty();
            }

        }
    })
}
//$(document).ready(function () {
//    alert("ok");
//    if (location.href.indexOf('?') != -1) {

//        id = GetQueryStringParams('id');
//         alert(id)
//        GetActivity(id);
//    }

//});

///////////////////////////////////// For New Allignment  ////////////////

function ChildPolicy() {
    ChildAllowed = $("#ddlChildrenAllowed option:selected").val();
    if (ChildAllowed == "Yes") {
        $("#ChildAgeFrom").show();
        $("#ChildAgeUpTo").show();
        $("#SmallChildAgeUpTo").show();
        //$("#ChildMinHeight").show();
        $("#Infant").show();
        $("#ChildHeight").show();
        $("#MaxChild").show();

    }
    else if (ChildAllowed == "No") {
        $("#ChildAgeFrom").hide();
        $("#ChildAgeUpTo").hide();
        $("#SmallChildAgeUpTo").hide();
        $("#ChildMinHeight").hide();
        $("#Infant").hide();
        $("#MaxNoofInfant").hide();
        $("#ChildHeight").hide();
        $("#MaxChild").hide();
        $("#MaxChildNo").hide();
    }
}


function HeightPolicy() {
    var Height = $("#ddlChildrenHeight option:selected").val();
    if (Height == "Yes") {

        $("#ChildMinHeight").show();

    }
    else if (Height == "No") {

        $("#ChildMinHeight").hide();

    }
}

function Infant() {
    //var Infant = $("#ddlInfant option:selected").val();
    //if (Infant == "Allowed") {
    //    $("#MaxNoofInfant").show();
    //}
    //else if (Infant == "Not Allowed") {
    //    $("#MaxNoofInfant").hide();
    //}
}

function MaxChildPolicy() {
    var MaxChild = $("#ddlMaxChildren option:selected").val();
    if (MaxChild == "Yes") {
        $("#MaxChildNo").show();
    }
    else if (MaxChild == "No") {
        $("#MaxChildNo").hide();
    }
}


//var AgeChild = $("#ddlChildAgeFrom option:selected").val();

//$("#ddlChildAgeFrom").change(function () {

//        var id = $(this).val();
//        $('#ddlChildAgeUpTo option[value>=' + id + ']').remove();
//    });





function ChangeAge() {
    var id = $("#ddlChildAgeFrom option:selected").val();
    var tRow = '';
    //var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
    for (var i = 1; i < id; i++) {

        tRow += '<option value="' + (id - i) + '">' + (id - i) + '</option>';

    }
    $("#ddlChildAgeUpTo").html(tRow);
}

function AgeUpTo() {
    var id = $("#ddlChildAgeUpTo option:selected").val();
    var tRow = '';
    //var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
    for (var i = 1; i < id; i++) {

        tRow += '<option value="' + (id - i) + '">' + (id - i) + '</option>';

    }
    $("#ddlSmallChildAgeUpTo").html(tRow);
}

var Slot = "No";
var Sharing;
var Private;
function Slots() {
    Slot = $("#ddlSlot option:selected").val();
    if (Slot == "Yes") {
        Sharing = $('#Chk_Sic').prop("checked");
        //Private = $('#Chk_Pvt').prop("checked");
        AddSlot();
        Getpriority();


    }
    else if (Slot == "No") {
        $("#SlotUI").empty();
    }
}

var FullYear;

function ChangeYear() {
    FullYear = $("#ddlFullYear option:selected").val();
    if (FullYear == "No") {
        AddDates();
        $("#OperationDays").show();
        // $("#Slotss").show();
        //Slots();
    }
    else if (FullYear == "Yes") {
        $("#DatesUI").empty();
        //$("#OperationDays").hide();
        // $("#SlotUI").empty();
        // $("#Slotss").hide();

    }
}

var Count = 1;
var txt = 1;
function AddDates() {
    //$("#MoreDates").hide();
    //$("#OperatingFrom1").show();
    //$("#OperatingTill1").show();
    //$("#MoreDates1").show();

    var Div = "";
    Div += '<div class="columns" id="MyDiv' + txt + '">'

    Div += '<div class="four-columns">'
    Div += '<span class="text-left">Operating From:</span>'
    Div += '<input class="input full-width mySelectCalendar dt1" type="text" id="datepicker' + Count + '" name="datepicker' + Count + '" style="cursor: pointer" value="" />'
    //Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker' + Count + '">'
    //Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="four-columns">'
    Div += '<span class="text-left">Operating Till:</span>'
    Div += '<input class="input full-width mySelectCalendar dt2" type="text" id="datepicker' + parseInt(Count + 1) + '"  style="cursor: pointer" value="" />'
    //Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker' + parseInt(Count + 1) + '">'
    //Div += '<b>* This field is required</b></label>'
    Div += '</div>'



    Div += '<br><div id="btnAddSeason' + txt + '" class="one-columns" title="Add Date">'
    Div += '<i Onclick="CheckEmpty(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-plus">More Dates</span></label></i>'

    Div += '</div>'
    if (Count != 1) {
        $("#btnAddSeason" + parseInt(txt - 1)).hide();
        Div += '<div class="one-columns" title="Delete">'
        Div += '<i Onclick="RemoveSeasonUI(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-trash"></span></label></i>'
        Div += '</div>'
    }


    Div += '</div>'
    $("#DatesUI").append(Div);
    $("#DatesUI").show();
    DateDisAuto(Count);
    Count += 2;
    txt++;

}

function CheckEmpty(dt, txt) {
    var chk = true;
    var Ndt = parseInt(parseInt(dt) + parseInt(1));

    if ($("#datepicker" + dt).val() == "") {
        // $("#lbl_datepicker" + dt).css("display", "");
        Success("Please select operating date");
        chk = false;
    }
    else {
        $("#lbl_datepicker" + dt).css("display", "none");
    }
    if ($("#datepicker" + Ndt).val() == "") {
        // $("#lbl_datepicker" + Ndt).css("display", "");
        Success("Please select operating date");
        chk = false;
    }
    else {
        $("#lbl_datepicker" + Ndt).css("display", "none");
    }
    if (chk) {
        AddDates();
    }
}

var Countt = 1;
var txtt = 1;
var ploc = "";
var dcount = "";
function AddSlot() {

    var Div = "";
    Div += '<div class="columns" id="MySlot' + txtt + '">'

    Div += '<div class="new-row three-columns"  >'
    Div += '    <h6>Slot Name :</h6>'
    Div += '    <input type="text" id="txt_SlotName' + Countt + '" class="input full-width name" />'
    Div += '<input type="hidden" class="SValue" id="lbl_SlotID' + Countt + '">'
    Div += '</div>'
    Div += '<div class="three-columns" id="PriorityType" >'
    Div += '    <h6>Priority Type  :</h6>'
    Div += '    <input type="text" name="Text[]" id="txt_PriorityType' + Countt + '" list="Select_Priority" class="input full-width PriorityType" />'
    Div += '                           <datalist id="Select_Priority"></datalist>'
    Div += '</div>'
    Div += '<div class="one-columns">'
    Div += '    <h6>Tour Start :</h6>'
    //Div += '    <input type="time" id="txt_TourStart' + Countt + '" class="input full-width TourStart" />'
    Div += ' <select id="ddl_TourStartHours' + Countt + '" style="height:30px" class="input full-width TourStartHours" onchange="CalPickTimeHr(\'' + Countt + '\')">'
    Div += '                                <option selected="" value="">Hrs</option>       '
    Div += '                                <option value="00">00</option>                 '
    Div += '                                <option value="01">01</option>                 '
    Div += '                                <option value="02">02</option>                 '
    Div += '                                <option value="03">03</option>                 '
    Div += '                                <option value="04">04</option>                 '
    Div += '                                <option value="05">05</option>                 '
    Div += '                                <option value="06">06</option>                 '
    Div += '                                <option value="07">07</option>                 '
    Div += '                                <option value="08">08</option>                 '
    Div += '                                <option value="09">09</option>                 '
    Div += '                                <option value="10">10</option>                 '
    Div += '                                <option value="11">11</option>                 '
    Div += '                                <option value="12">12</option>                 '
    Div += '                                <option value="13">13</option>                 '
    Div += '                                <option value="14">14</option>                 '
    Div += '                                <option value="15">15</option>                 '
    Div += '                                <option value="16">16</option>                 '
    Div += '                                <option value="17">17</option>                 '
    Div += '                                <option value="18">18</option>                 '
    Div += '                                <option value="19">19</option>                 '
    Div += '                                <option value="20">20</option>                 '
    Div += '                                <option value="21">21</option>                 '
    Div += '                                <option value="22">22</option>                 '
    Div += '                                <option value="23">23</option>                 '
    Div += '                                                                                           '
    Div += '                            </select>                                                      '

    Div += '</div>'

    Div += '<div class="one-columns" style="margin-top: 34px">'

    Div += '<select id="ddl_TourStartMin' + Countt + '" style="height:30px" class="input full-width TourStartMin" onchange="CalPickTimeMin(\'' + Countt + '\')">'
    Div += '                                <option selected="" value="">Min</option>       '
    Div += '                                <option value="00">00</option>                 '
    Div += '                                <option value="15">15</option>                 '
    Div += '                                <option value="30">30</option>                 '
    Div += '                                <option value="45">45</option>                 '
    Div += '                            </select>                                                      '

    Div += '</div>'

    Div += '<div class="one-columns">'
    Div += '    <h6>Tour End :</h6>'
    // Div += '    <input type="time" id="txt_TourEnd' + Countt + '" class="input full-width TourEnd" />'

    Div += ' <select id="ddl_TourEndHours' + Countt + '" style="height:30px" class="input full-width TourEndHours" onchange="CalDropTimeHr(\'' + Countt + '\')">'
    Div += '                                <option selected="" value="">Hrs</option>       '
    Div += '                                <option value="00">00</option>                 '
    Div += '                                <option value="01">01</option>                 '
    Div += '                                <option value="02">02</option>                 '
    Div += '                                <option value="03">03</option>                 '
    Div += '                                <option value="04">04</option>                 '
    Div += '                                <option value="05">05</option>                 '
    Div += '                                <option value="06">06</option>                 '
    Div += '                                <option value="07">07</option>                 '
    Div += '                                <option value="08">08</option>                 '
    Div += '                                <option value="09">09</option>                 '
    Div += '                                <option value="10">10</option>                 '
    Div += '                                <option value="11">11</option>                 '
    Div += '                                <option value="12">12</option>                 '
    Div += '                                <option value="13">13</option>                 '
    Div += '                                <option value="14">14</option>                 '
    Div += '                                <option value="15">15</option>                 '
    Div += '                                <option value="16">16</option>                 '
    Div += '                                <option value="17">17</option>                 '
    Div += '                                <option value="18">18</option>                 '
    Div += '                                <option value="19">19</option>                 '
    Div += '                                <option value="20">20</option>                 '
    Div += '                                <option value="21">21</option>                 '
    Div += '                                <option value="22">22</option>                 '
    Div += '                                <option value="23">23</option>                 '
    Div += '                                                                                           '
    Div += '                            </select>                                                      '

    Div += '</div>'

    Div += '<div class="one-columns" style="margin-top: 34px">'

    Div += '<select id="ddl_TourEndMin' + Countt + '" style="height:30px" class="input full-width TourEndMin" onchange="CalDropTimeMin(\'' + Countt + '\')">'
    Div += '                                <option selected="" value="">Min</option>       '
    Div += '                                <option value="00">00</option>                 '
    Div += '                                <option value="15">15</option>                 '
    Div += '                                <option value="30">30</option>                 '
    Div += '                                <option value="45">45</option>                 '
    Div += '                            </select>                                                      '

    Div += '</div>'


    //Div += '<div id="PvtSic" style="display:none">'

    //if (Sharing == true || Private == true) {

    //    Div += '<div class="three-columns">'
    //    Div += '    <h6>Pick-Up From :</h6>'
    //    // Div += '    <input type="text" id="txt_PickUpFrom' + Countt + '" class="input full-width PickUpFrom" />'

    //    Div += '<input type="text" name="Text[]" id="txt_PickUpFrom' + Countt + '" list="Select_Location" onchange="BinddropLoc()" class="input full-width PickUpFrom"  />'
    //    Div += '                           <datalist id="Select_Location"></datalist>'
    //    //Div += '</div>'

    //    //Div += '<select id="txt_PickUpFrom' + Countt + '" onchange="fnPicklocation()"  class="select multiple-as-single easy-multiple-selection allow-empty check-list PickUpFrom' + Countt + ' full-width" multiple />'

    //    //Div += '                            </select>                                                      '

    //    Div += '</div>'


    //    Div += '<div class="one-columns" id="PickUpTime" >'
    //    Div += '    <h6>Pick-Up Time :</h6>'
    //    // Div += '    <input type="time" id="txt_PickUpTime' + Countt + '" class="input full-width PickUpTime" />'

    //    Div += ' <select id="ddl_PickUpTimeHours' + Countt + '" style="height:30px" class="input full-width PickUpTimeHours">'
    //    Div += '                                <option selected="" value="">Hrs</option>       '
    //    //Div += '                                <option value="00">00</option>                 '
    //    //Div += '                                <option value="01">01</option>                 '
    //    //Div += '                                <option value="02">02</option>                 '
    //    //Div += '                                <option value="03">03</option>                 '
    //    //Div += '                                <option value="04">04</option>                 '
    //    //Div += '                                <option value="05">05</option>                 '
    //    //Div += '                                <option value="06">06</option>                 '
    //    //Div += '                                <option value="07">07</option>                 '
    //    //Div += '                                <option value="08">08</option>                 '
    //    //Div += '                                <option value="09">09</option>                 '
    //    //Div += '                                <option value="10">10</option>                 '
    //    //Div += '                                <option value="11">11</option>                 '
    //    //Div += '                                <option value="12">12</option>                 '
    //    //Div += '                                <option value="13">13</option>                 '
    //    //Div += '                                <option value="14">14</option>                 '
    //    //Div += '                                <option value="15">15</option>                 '
    //    //Div += '                                <option value="16">16</option>                 '
    //    //Div += '                                <option value="17">17</option>                 '
    //    //Div += '                                <option value="18">18</option>                 '
    //    //Div += '                                <option value="19">19</option>                 '
    //    //Div += '                                <option value="20">20</option>                 '
    //    //Div += '                                <option value="21">21</option>                 '
    //    //Div += '                                <option value="22">22</option>                 '
    //    //Div += '                                <option value="23">23</option>                 '
    //    //Div += '                                                                                           '
    //    Div += '                            </select>                                                      '

    //    Div += '</div>'

    //    Div += '<div class="one-columns" style="margin-top: 34px">'

    //    Div += '<select id="ddl_PickUpTimeMin' + Countt + '" style="height:30px" class="input full-width PickUpTimeMin">'
    //    Div += '                                <option selected="" value="">Min</option>       '
    //    Div += '                                <option value="00">00</option>                 '
    //    Div += '                                <option value="15">15</option>                 '
    //    Div += '                                <option value="30">30</option>                 '
    //    Div += '                                <option value="45">45</option>                 '
    //    Div += '                            </select>                                                      '

    //    Div += '</div>'



    //    Div += '<div class="three-columns" id="Div_DropOffAt' + Countt + '" >'
    //    Div += '    <h6>Drop-Off At  :</h6>'
    //    //// Div += '    <input type="text" id="txt_DropOffAt' + Countt + '" class="input full-width DropOffAt" />'
    //    Div += '<input type="text" name="Text[]" id="txt_DropOffAt' + Countt + '" list="Select_Location" class="input full-width DropOffAt"  />'
    //    Div += '                           <datalist id="Select_Location"></datalist>'

    //    //Div += '<select id="txt_DropOffAt' + Countt + '" onchange="fnDroplocation()" class="select multiple-as-single easy-multiple-selection allow-empty check-list DropOffAt' + Countt + ' full-width" multiple />'

    //    //Div += '                            </select>                                                      '

    //    Div += '</div>'

    //    Div += '<div class="one-columns" id="DropOffTime' + Countt + '" >'
    //    Div += '    <h6>Drop Time :</h6>'
    //    //Div += '    <input type="time" id="txt_DropOffTime' + Countt + '" class="input full-width DropOffTime" />'

    //    Div += ' <select id="ddl_DropOffTimeHours' + Countt + '" style="height:30px" class="input full-width DropOffTimeHours">'
    //    Div += '                                <option selected="" value="">Hrs</option>       '
    //    //Div += '                                <option value="00">00</option>                 '
    //    //Div += '                                <option value="01">01</option>                 '
    //    //Div += '                                <option value="02">02</option>                 '
    //    //Div += '                                <option value="03">03</option>                 '
    //    //Div += '                                <option value="04">04</option>                 '
    //    //Div += '                                <option value="05">05</option>                 '
    //    //Div += '                                <option value="06">06</option>                 '
    //    //Div += '                                <option value="07">07</option>                 '
    //    //Div += '                                <option value="08">08</option>                 '
    //    //Div += '                                <option value="09">09</option>                 '
    //    //Div += '                                <option value="10">10</option>                 '
    //    //Div += '                                <option value="11">11</option>                 '
    //    //Div += '                                <option value="12">12</option>                 '
    //    //Div += '                                <option value="13">13</option>                 '
    //    //Div += '                                <option value="14">14</option>                 '
    //    //Div += '                                <option value="15">15</option>                 '
    //    //Div += '                                <option value="16">16</option>                 '
    //    //Div += '                                <option value="17">17</option>                 '
    //    //Div += '                                <option value="18">18</option>                 '
    //    //Div += '                                <option value="19">19</option>                 '
    //    //Div += '                                <option value="20">20</option>                 '
    //    //Div += '                                <option value="21">21</option>                 '
    //    //Div += '                                <option value="22">22</option>                 '
    //    //Div += '                                <option value="23">23</option>                 '
    //    Div += '                                                                                           '
    //    Div += '                            </select>                                                      '

    //    Div += '</div>'
    //    Div += '<div class="one-columns" style="margin-top: 34px">'

    //    Div += '<select id="ddl_DropOffTimeMin' + Countt + '" style="height:30px" class="input full-width DropOffTimeMin">'
    //    Div += '                                <option selected="" value="">Min</option>       '
    //    Div += '                                <option value="00">00</option>                 '
    //    Div += '                                <option value="15">15</option>                 '
    //    Div += '                                <option value="30">30</option>                 '
    //    Div += '                                <option value="45">45</option>                 '
    //    Div += '                            </select>                                          '

    //    Div += '</div>'

    //}



    //Div += '</div>'

    Div += '<div id="btnAddSlot' + txtt + '" class="one-columns" title="Add Slot">'
    Div += '<i Onclick="CheckEmptyy(\'' + Countt + '\',\'' + txtt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" style="margin-top: 35px"><span class="icon-plus">More Slot</span></label></i>'



    if (Countt != 1) {
        $("#btnAddSlot" + parseInt(txtt - 1)).hide();
        Div += '<div class="one-columns" title="Remove Slot">'
        Div += '<i Onclick="RemoveSlotUI(\'' + Count + '\',\'' + txtt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-minus"></span></label></i>'
        Div += '</div>'

    }

    var tt = "abc";
    $("#lbl_SlotID" + Countt + "").val(tt);
    Div += '</div>'
    $("#SlotUI").append(Div);
    $("#SlotUI").show();
    ploc = Countt;
    // GetActLocation(ploc);
    dcount = Countt;
    Countt += 2;
    txtt++;

}

function BinddropLoc() {
    var picklocation = $('#txt_PickUpFrom' + ploc + '').val();
    $('#txt_DropOffAt' + ploc + '').val(picklocation);
}

var TstartHr = "";
function CalPickTimeHr(Countt) {
    TstartHr = $("#ddl_TourStartHours" + Countt + "").val();


    var tRow = '';
    var j = 0;
    for (var i = 0; i <= TstartHr; i++) {

        tRow += '<option value="' + (TstartHr - j) + '">' + (TstartHr - j) + '</option>';
        j++;
    }
    $("#ddl_PickUpTimeHours" + Countt + "").html(tRow);


}

var TstartMin = "";
function CalPickTimeMin(Countt) {
    TstartMin = $("#ddl_TourStartMin" + Countt + "").val();


    var tRow = '';
    var j = 0;
    for (var i = 0; i <= TstartMin; i++) {
        if ((TstartMin - j) < 0) {
            break;
        }
        tRow += '<option value="' + (TstartMin - j) + '">' + (TstartMin - j) + '</option>';

        j += 15;
    }
    $("#ddl_PickUpTimeMin" + Countt + "").html(tRow);

    //var tRow = '';
    //var j = 0;
    ////var tRow = '<tr><td><b>Name</b></td><td style="text-align:center"><b>Email | Password Manage</b></td><td><b>Mobile</b></td><td><b>Unique Code</b></td><td align="center"><b>Edit | Status | Delete</b></td></tr>';
    //for (var i = 24; i > PicktHr; i--) {

    //    tRow += '<option value="' + (parseFloat(PicktHr) + parseFloat(j)) + '">' + (parseFloat(PicktHr) + parseFloat(j)) + '</option>';
    //    j++;
    //}
    //$("#ddl_DropOffTimeHours" + Countt + "").html(tRow);

}

var TendHr = "";
function CalDropTimeHr(Countt) {
    TendHr = $("#ddl_TourEndHours" + Countt + "").val();


    var tRow = '';
    var j = 0;
    for (var i = 23; i >= TendHr; i--) {

        tRow += '<option value="' + (parseFloat(TendHr) + parseFloat(j)) + '">' + (parseFloat(TendHr) + parseFloat(j)) + '</option>';
        j++;
    }
    $("#ddl_DropOffTimeHours" + Countt + "").html(tRow);


}


var TendtMin = "";
function CalDropTimeMin(Countt) {
    TendtMin = $("#ddl_TourEndMin" + Countt + "").val();


    var tRow = '';
    var j = 0;
    for (var i = 45; i >= TendtMin; i--) {
        if ((parseFloat(TendtMin) + parseFloat(j)) > 45) {
            break;
        }
        tRow += '<option value="' + (parseFloat(TendtMin) + parseFloat(j)) + '">' + (parseFloat(TendtMin) + parseFloat(j)) + '</option>';

        j += 15;
    }
    $("#ddl_DropOffTimeMin" + Countt + "").html(tRow);


}


function CheckEmptyy(dt, txtt) {
    var chk = true;
    var Ndt = parseInt(parseInt(dt) + parseInt(1));

    if ($("#ddlOperationDays" + dt).val() == "") {
        Success("Please select opertation day.");
        chk = false;
    }

    else if ($("#txt_SlotName" + dt).val() == "") {
        Success("Please enter slot name.");
        chk = false;
    }
    else if ($("#txt_TourStart" + dt).val() == "") {
        Success("Please enter tour start time.");
        chk = false;
    }

        //else if ($("#ddl_PickUpTimeHours" + dt).val() == "") {
        //    Success("Please enter tour start time.");
        //    chk = false;
        //}

    else if ($("#ddl_TourStartMin" + dt).val() == "") {
        Success("Please enter tour start time.");
        chk = false;
    }

        //else if ($("#txt_TourEnd" + dt).val() == "") {
        //    Success("Please enter tour end time.");
        //    chk = false;
        //}

    else if ($("#ddl_TourEndHours" + dt).val() == "") {
        Success("Please enter tour end time.");
        chk = false;
    }


    else if ($("#ddl_TourEndMin" + dt).val() == "") {
        Success("Please enter tour end time.");
        chk = false;
    }

        //else if ($("#txt_PickUpFrom" + dt).val() == "") {
        //    Success("Please enter pick-up from.");
        //    chk = false;
        //}
        //else if ($("#txt_PickUpTime" + dt).val() == "") {
        //    Success("Please enter pick-up time.");
        //    chk = false;
        //}

        //else if ($("#ddl_PickUpTimeHours" + dt).val() == "") {
        //    Success("Please enter pick-up time.");
        //    chk = false;
        //}

        //else if ($("#ddl_PickUpTimeMin" + dt).val() == "") {
        //    Success("Please enter pick-up time.");
        //    chk = false;
        //}

        //else if ($("#txt_DropOffAt" + dt).val() == "") {
        //    Success("Please enter drop-off.");
        //    chk = false;
        //}
        //else if ($("#txt_DropOffTime" + dt).val() == "") {
        //    Success("Please enter drop-off time.");
        //    chk = false;
        //}

        //else if ($("#ddl_DropOffTimeHours" + dt).val() == "") {
        //    Success("Please enter drop-off time.");
        //    chk = false;
        //}

        //else if ($("#ddl_DropOffTimeMin" + dt).val() == "") {
        //    Success("Please enter drop-off time.");
        //    chk = false;
        //}

    else if ($("#txt_PriorityType" + dt).val() == "") {
        Success("Please enter priority.");
        chk = false;
    }


    if (chk) {
        AddSlot();
    }
}

function DateDisAuto(Id) {
    if (Id == 1) {
        $("#datepicker" + Id).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $("#datepicker" + parseInt(Id + 1)).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
    else {
        $("#datepicker" + Id).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: "dateToday+1",
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $('#datepicker' + parseInt(Id + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: "dateToday+1",
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
}

function RemoveSeasonUI(Cnt, Id) {
    var c = parseInt(Cnt) - 1;
    var dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
    var Ck = dateObject.selector;
    if (Ck != undefined) {
        for (var i = 0; i < 10; i++) {
            c = parseInt(c) - 2;
            dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
            Ck = dateObject.selector;
            if (Ck == undefined) {
                break;
            }
        }
    }
    var eDate = new Date();
    // var dateObject = $("#datepicker" + parseInt(parseInt(Cnt) - 1)).datepicker('getDate', '+1d');
    eDate.setDate(dateObject.getDate() + 1);
    if ($("#datepicker" + parseInt(parseInt(Cnt) + 2)).val() != undefined) {
        $("#datepicker" + parseInt(parseInt(Cnt) + 2)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Cnt) + 2)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: eDate,
            //beforeShowDay: disableDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Cnt) + 2), 1);
            }
        });
        $("#datepicker" + parseInt(parseInt(Cnt) + 3)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Cnt) + 3)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: eDate,
            //beforeShowDay: disableDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Cnt) + 3), 2);
            }
        });
    }
    $("#MyDiv" + Id).remove();
    if (txt <= parseInt(Id) + 1) {
        $("#btnAddSeason" + parseInt(parseInt(Id) - 1)).show();

    }
    if ($(".name").length == 1) {
        $("#btnAddSeason1").show();
    }
}

function RemoveSlotUI(Cnt, Id) {
    //var c = parseInt(Cnt) - 1;
    //var dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
    //var Ck = dateObject.selector;
    //if (Ck != undefined) {
    //    for (var i = 0; i < 10; i++) {
    //        c = parseInt(c) - 2;
    //        dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
    //        Ck = dateObject.selector;
    //        if (Ck == undefined) {
    //            break;
    //        }
    //    }
    //}
    //var eDate = new Date();
    // var dateObject = $("#datepicker" + parseInt(parseInt(Cnt) - 1)).datepicker('getDate', '+1d');
    //eDate.setDate(dateObject.getDate() + 1);
    //if ($("#datepicker" + parseInt(parseInt(Cnt) + 2)).val() != undefined) {
    //    $("#datepicker" + parseInt(parseInt(Cnt) + 2)).datepicker("destroy");
    //    $('#datepicker' + parseInt(parseInt(Cnt) + 2)).datepicker({
    //        dateFormat: 'dd-mm-yy',
    //        minDate: eDate,
    //        //beforeShowDay: disableDate,
    //        onSelect: function (date) {
    //            DateDisable(parseInt(parseInt(Cnt) + 2), 1);
    //        }
    //    });
    //    $("#datepicker" + parseInt(parseInt(Cnt) + 3)).datepicker("destroy");
    //    $('#datepicker' + parseInt(parseInt(Cnt) + 3)).datepicker({
    //        dateFormat: 'dd-mm-yy',
    //        minDate: eDate,
    //        //beforeShowDay: disableDate,
    //        onSelect: function (date) {
    //            DateDisable(parseInt(parseInt(Cnt) + 3), 2);
    //        }
    //    });
    //}
    $("#MySlot" + Id).remove();
    if (txt <= parseInt(Id) + 1) {
        $("#btnAddSlot" + parseInt(parseInt(Id) - 1)).show();
    }
    if ($(".name").length == 1) {
        $("#btnAddSlot").show();
    }
}

var endDate = new Date();
var endDates = new Date();

function DateDisable(Id, chk) {
    var dateObject = $("#datepicker" + Id).datepicker('getDate', '+1d');
    endDate.setDate(dateObject.getDate() + 1);

    if (chk == 1) {
        //$("#datepicker" + parseInt(parseInt(Id) + 1)).datepicker("destroy");
        $("#datepicker" + parseInt(parseInt(Id) + 1)).datepicker();
        $('#datepicker' + parseInt(parseInt(Id) + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: endDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Id) + 1), 2);
            }
        });
    }
}

///////////////////////////////////  End  /////////////////////////////////////////

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

var UTourType = "";
var UTer_ID = "";
function GetActivity(id) {
    // alert("ok")

    var Data = { id: id }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetActivity",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var dtTable = result.dtTable;
                GetNationality(dtTable[0].Country.replace("UAE", "AE"));
                //$("#ddlcountry").val(dtTable[0].Country);

                Getcity(dtTable[0].City);
                // $("#ddlcity").val(dtTable[0].City);

                $("#txt_Activity").val(dtTable[0].Act_Name);
                $("#txt_subtitle").val(dtTable[0].Sub_Title);
                $("#txt_AreaRemarks").val(dtTable[0].Description);
                $("#txt_location").val(dtTable[0].Location);
                //$("#txt_longitude").val(dtTable[0].Lon_Leng.split(',')[0]);
                //$("#txt_latitude").val(dtTable[0].Lon_Leng.split(',')[1]);
                $("#txt_tournote").val(dtTable[0].Tour_Note);
                //$("#ddlChildrenAllowed").val(dtTable[0].Allowed_Child);

                $("#ddlChildrenAllowed option").each(function () {
                    if ($(this).html() == dtTable[0].Allowed_Child) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                $("#ddlChildrenHeight option").each(function () {
                    if ($(this).html() == dtTable[0].Allow_Height) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                $("#ddlInfant option").each(function () {
                    if ($(this).html() == dtTable[0].ALlow_Infant) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                ChildAllowed = dtTable[0].Allowed_Child;
                if (dtTable[0].Allowed_Child == "Yes") {
                    GetChildPolicy(id);
                }
                else {
                    $("#ChildAgeFrom").hide();
                    $("#ChildAgeUpTo").hide();
                    $("#SmallChildAgeUpTo").hide();
                    $("#ChildMinHeight").hide();
                    $("#Infant").hide();
                    $("#MaxNoofInfant").hide();
                    $("#ChildHeight").hide();
                    $("#MaxChild").hide();
                    $("#MaxChildNo").hide();
                }

                $("#ddlPriorityType").val(dtTable[0].Priority);

                //var kid1 = dtTable[0].Kid1Range;
                //var Range1 = kid1.split('-');
                //$("#txt_Kid1StartRange").val(Range1[0]);
                //$("#txt_Kid1EndRange").val(Range1[1]);

                for (var i = 0; i < dtTable.length; i++) {

                    UTourType += dtTable[i].Act_Type + ";";
                    UTer_ID += dtTable[i].T_Id + ";";
                }

                //var kid2 = dtTable[0].Kid2Range;
                //var Range2 = kid2.split('-');
                //$("#txt_Kid2StartRange").val(Range2[0]);
                //$("#txt_Kid2EndRange").val(Range2[1]);

                // $("#txt_Attraction").val(dtTable[0].Attractions);
                //GetAddress(dtTable[0].Location);
                //stourType = dtTable[0].Tour_Type;
                settourtype(dtTable[0].Tour_Type);
                SetActivityType(UTourType);
                SetAttraction(dtTable[0].Attractions)
                for (var i = 0; i < dtTable.length; i++)

                {

                    if (dtTable[i].Act_Type == "TKT") {
                        $("#OperationDaysTKT").show();

                        SetOperationDaysTKT(dtTable[i].OperationsDays)
                    }
                    else if (dtTable[i].Act_Type == "SIC") {
                        $("#OperationDaysSIC").show();

                        SetOperationDaysSIC(dtTable[i].OperationsDays)
                    }
                    else if (dtTable[i].Act_Type == "PVT") {
                        $("#OperationDaysPVT").show();
                        SetOperationDaysPVT(dtTable[i].OperationsDays)
                    }

                }
                

                //SetOperationDays(dtTable[0].Operation_Days)
                $("#txt_MinCapacity").val(dtTable[0].Min_Capacity);
                $("#txt_MaximumCapacity").val(dtTable[0].Max_Capacity);

                $("#ddlFullYear option").each(function () {
                    if ($(this).html() == dtTable[0].Full_Year) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                FulYear = dtTable[0].Full_Year;
                //if (dtTable[0].Full_Year == "No")
                //{
                    
                    SetOperatingDate(id);
                //}
                Slot = dtTable[0].Slot_Allow;
                $("#ddlSlot option").each(function () {
                    if ($(this).html() == dtTable[0].Slot_Allow) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                if (dtTable[0].Slot_Allow == "Yes") {
                    //$("#ddlSlot").val(dtTable[0].Slot_Allow);
                    SetSlots(id);
                }


                UpdateImage("", id, dtTable[0].Act_Images)
                // Attraction = dtTable[0].Attractions;

                //if (dtTable[0].Tour_Type == "")
                //{
                //    $("#chk").prop("unchecked", Tour_Type);
                //}
                //else {
                //    $("#chk").prop("checked", Tour_Type);
                //}
                //sNId = dtTable[0].sRegisterId;
            }
            else {
                Success("An error occured !!!");
                //sNId = 0;
            }
        },
        error: function () {
            // var msg = "An error occured !!!"
            // alert("An error occured !!!");
            //sNId = 0;
        }
    });
}


function GetNationality(Country) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = Country.split(",")
        var CountryCode = $.grep(arrCountry, function (p) { return p.Country == Country; })
           .map(function (p) { return p.Country; });


        $("#drp_country .select span")[0].textContent = CountryCode;
        for (var i = 0; i < arrCountry.length; i++) {
            if (arrCountry[i].Country == CountryCode) {
                //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                $("#drp_country .select span")[0].textContent = arrCountry[i].Countryname;
                $('input[value="' + Country + '"][class="OfferType"]').prop("selected", true);

                $("#ddlcountry").val(Country);
                //  pGTA += GTA[i] + ",";
                var selected = [];
                GetCity(arrCountry[i].Countryname);
            }


        }
        GetCity(Country);
    }
    catch (ex)
    { }
}


function GetChildPolicy(id) {
    var Data = { id: id }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/GetChildPolicyDetails",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var dtTable = result.dtTable;
                //if (dtTable.length > 0) {
                //    $("#ddlChildAgeFrom").empty();
                //    $("#ddlChildAgeUpTo").empty();
                //    $("#ddlSmallChildAgeUpTo").empty();
                //    //var ddlRequest = '<option selected="selected" value="">Select Priority</option>';
                //    var ddlRequest;
                //    for (i = 0; i < dtTable.length; i++) {
                //        ddlRequest += '<option value="' + dtTable[i].Child_Age_From + '">' + dtTable[i].Child_Age_From + '</option>';
                //    }
                //    $("#ddlChildAgeFrom").append(ddlRequest);
                //}


                $("#ddlChildAgeFrom").val(dtTable[0].Child_Age_From);
                ChangeAge();
                $("#ddlChildAgeUpTo").val(dtTable[0].Child_Age_Upto);
                AgeUpTo();
                $("#ddlSmallChildAgeUpTo").val(dtTable[0].Small_Child_Age_Upto);
                $("#ChildMinHeight").show();
                $("#txt_ChildMinHight").val(dtTable[0].Child_Min_Height);

                //$("#ddlChildAgeFrom").val(dtTable[0].Child_Age_From)
                //$("#ChildAgeFrom .select-value span").text = dtTable[0].Child_Age_From;


            }
            else {
                Success("An error occured !!!");

            }
        },
        error: function () {

        }
    });
}
var Upd_OprDates = "";

function SetOperatingDate(id) {
    var Data = { id: id }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/SetOperatingDate",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1)
            {
                var dtTable = result.dtTable;
                if (FulYear == "Yes")
                {
                    Upd_OprDates = dtTable[0].P_Id;
                }
                else
                {

                    for (var i = 0; i < dtTable.length; i++) {
                        var Div = "";
                        Div += '<div class="columns" id="MyDiv' + txt + '">'

                        Div += '<div class="four-columns">'
                        Div += '<span class="text-left">Operating From:</span>'
                        Div += '<input class="input full-width mySelectCalendar dt1" type="text" id="datepicker' + Count + '" name="datepicker' + Count + '" style="cursor: pointer" value="" />'
                        //Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker' + Count + '">'
                        //Div += '<b>* This field is required</b></label>'
                        Div += '</div>'

                        Div += '<div class="four-columns">'
                        Div += '<span class="text-left">Operating Till:</span>'
                        Div += '<input class="input full-width mySelectCalendar dt2" type="text" id="datepicker' + parseInt(Count + 1) + '"  style="cursor: pointer" value="" />'
                        //Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_datepicker' + parseInt(Count + 1) + '">'
                        //Div += '<b>* This field is required</b></label>'
                        Div += '</div>'



                        Div += '<br><div id="btnAddSeason' + txt + '" class="one-columns" title="Add Date">'
                        Div += '<i Onclick="CheckEmpty(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-plus">More Dates</span></label></i>'

                        Div += '</div>'
                        if (Count != 1) {
                            $("#btnAddSeason" + parseInt(txt - 1)).hide();
                            Div += '<div class="one-columns" title="Delete">'
                            Div += '<i Onclick="RemoveSeasonUI(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-trash"></span></label></i>'
                            Div += '</div>'
                        }


                        Div += '</div>'

                        $("#DatesUI").append(Div);
                        $("#DatesUI").show();
                        DateDisAuto(Count);
                        $("#datepicker" + Count + "").val(dtTable[i].Operating_From);
                        $("#datepicker" + parseInt(Count + 1) + "").val(dtTable[i].Operating_Till);
                        Count += 2;
                        txt++;

                        Upd_OprDates += dtTable[i].P_Id + ";";
                    }

                }
                

            }
            else
            {
                Success("An error occured !!!");

            }
        },
        error: function () {

        }
    });
}

var Upd_SLots = "";
function SetSlots(id) {
    var Data = { id: id }
    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/SetSlots",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var dtTable = result.dtTable;
                Sharing = $('#Chk_Sic').prop("checked");
                //Private = $('#Chk_Pvt').prop("checked");

                for (var i = 0; i < dtTable.length; i++) {
                    var Div = "";
                    Div += '<div class="columns" id="MySlot' + txtt + '">'

                    Div += '<div class="new-row three-columns"  >'
                    Div += '    <h6>Slot Name :</h6>'
                    Div += '    <input type="text" id="txt_SlotName' + Countt + '" class="input full-width name" />'
                    Div += '<input type="hidden" class="SValue" id="lbl_SlotID' + Countt + '">'
                    Div += '</div>'
                    Div += '<div class="three-columns" id="PriorityType" >'
                    Div += '    <h6>Priority Type  :</h6>'
                    Div += '    <input type="text" id="txt_PriorityType' + Countt + '" class="input full-width PriorityType" />'
                    Div += '</div>'
                    Div += '<div class="one-columns">'
                    Div += '    <h6>Tour Start :</h6>'
                    //Div += '    <input type="time" id="txt_TourStart' + Countt + '" class="input full-width TourStart" />'
                    Div += ' <select id="ddl_TourStartHours' + Countt + '" style="height:30px" class="input full-width TourStartHours">'
                    Div += '                                <option selected="" value="">Hrs</option>       '
                    Div += '                                <option value="00">00</option>                 '
                    Div += '                                <option value="01">01</option>                 '
                    Div += '                                <option value="02">02</option>                 '
                    Div += '                                <option value="03">03</option>                 '
                    Div += '                                <option value="04">04</option>                 '
                    Div += '                                <option value="05">05</option>                 '
                    Div += '                                <option value="06">06</option>                 '
                    Div += '                                <option value="07">07</option>                 '
                    Div += '                                <option value="08">08</option>                 '
                    Div += '                                <option value="09">09</option>                 '
                    Div += '                                <option value="10">10</option>                 '
                    Div += '                                <option value="11">11</option>                 '
                    Div += '                                <option value="12">12</option>                 '
                    Div += '                                <option value="13">13</option>                 '
                    Div += '                                <option value="14">14</option>                 '
                    Div += '                                <option value="15">15</option>                 '
                    Div += '                                <option value="16">16</option>                 '
                    Div += '                                <option value="17">17</option>                 '
                    Div += '                                <option value="18">18</option>                 '
                    Div += '                                <option value="19">19</option>                 '
                    Div += '                                <option value="20">20</option>                 '
                    Div += '                                <option value="21">21</option>                 '
                    Div += '                                <option value="22">22</option>                 '
                    Div += '                                <option value="23">23</option>                 '
                    Div += '                                                                                           '
                    Div += '                            </select>                                                      '

                    Div += '</div>'

                    Div += '<div class="one-columns" style="margin-top: 34px">'

                    Div += '<select id="ddl_TourStartMin' + Countt + '" style="height:30px" class="input full-width TourStartMin">'
                    Div += '                                <option selected="" value="">Min</option>       '
                    Div += '                                <option value="00">00</option>                 '
                    Div += '                                <option value="15">15</option>                 '
                    Div += '                                <option value="30">30</option>                 '
                    Div += '                                <option value="45">45</option>                 '
                    Div += '                            </select>                                                      '

                    Div += '</div>'

                    Div += '<div class="one-columns">'
                    Div += '    <h6>Tour End :</h6>'
                    // Div += '    <input type="time" id="txt_TourEnd' + Countt + '" class="input full-width TourEnd" />'

                    Div += ' <select id="ddl_TourEndHours' + Countt + '" style="height:30px" class="input full-width TourEndHours">'
                    Div += '                                <option selected="" value="">Hrs</option>       '
                    Div += '                                <option value="00">00</option>                 '
                    Div += '                                <option value="01">01</option>                 '
                    Div += '                                <option value="02">02</option>                 '
                    Div += '                                <option value="03">03</option>                 '
                    Div += '                                <option value="04">04</option>                 '
                    Div += '                                <option value="05">05</option>                 '
                    Div += '                                <option value="06">06</option>                 '
                    Div += '                                <option value="07">07</option>                 '
                    Div += '                                <option value="08">08</option>                 '
                    Div += '                                <option value="09">09</option>                 '
                    Div += '                                <option value="10">10</option>                 '
                    Div += '                                <option value="11">11</option>                 '
                    Div += '                                <option value="12">12</option>                 '
                    Div += '                                <option value="13">13</option>                 '
                    Div += '                                <option value="14">14</option>                 '
                    Div += '                                <option value="15">15</option>                 '
                    Div += '                                <option value="16">16</option>                 '
                    Div += '                                <option value="17">17</option>                 '
                    Div += '                                <option value="18">18</option>                 '
                    Div += '                                <option value="19">19</option>                 '
                    Div += '                                <option value="20">20</option>                 '
                    Div += '                                <option value="21">21</option>                 '
                    Div += '                                <option value="22">22</option>                 '
                    Div += '                                <option value="23">23</option>                 '
                    Div += '                                                                                           '
                    Div += '                            </select>                                                      '

                    Div += '</div>'

                    Div += '<div class="one-columns" style="margin-top: 34px">'

                    Div += '<select id="ddl_TourEndMin' + Countt + '" style="height:30px" class="input full-width TourEndMin">'
                    Div += '                                <option selected="" value="">Min</option>       '
                    Div += '                                <option value="00">00</option>                 '
                    Div += '                                <option value="15">15</option>                 '
                    Div += '                                <option value="30">30</option>                 '
                    Div += '                                <option value="45">45</option>                 '
                    Div += '                            </select>                                                      '

                    Div += '</div>'


                    //Div += '<div id="PvtSic" style="display:none">'

                    //if (Sharing == true || Private == true) {

                    //    Div += '<div class="three-columns">'
                    //    Div += '    <h6>Pick-Up From :</h6>'
                    //    // Div += '    <input type="text" id="txt_PickUpFrom' + Countt + '" class="input full-width PickUpFrom" />'

                    //    Div += '<input type="text" name="Text[]" id="txt_PickUpFrom' + Countt + '" list="Select_Location" class="input full-width PickUpFrom"  />'
                    //    Div += '                           <datalist id="Select_Location"></datalist>'
                    //    Div += '</div>'

                    //    Div += '<div class="one-columns" id="PickUpTime" >'
                    //    Div += '    <h6>Pick-Up Time :</h6>'
                    //    // Div += '    <input type="time" id="txt_PickUpTime' + Countt + '" class="input full-width PickUpTime" />'

                    //    Div += ' <select id="ddl_PickUpTimeHours' + Countt + '" style="height:30px" class="input full-width PickUpTimeHours">'
                    //    Div += '                                <option selected="" value="">Hrs</option>       '
                    //    Div += '                                <option value="00">00</option>                 '
                    //    Div += '                                <option value="01">01</option>                 '
                    //    Div += '                                <option value="02">02</option>                 '
                    //    Div += '                                <option value="03">03</option>                 '
                    //    Div += '                                <option value="04">04</option>                 '
                    //    Div += '                                <option value="05">05</option>                 '
                    //    Div += '                                <option value="06">06</option>                 '
                    //    Div += '                                <option value="07">07</option>                 '
                    //    Div += '                                <option value="08">08</option>                 '
                    //    Div += '                                <option value="09">09</option>                 '
                    //    Div += '                                <option value="10">10</option>                 '
                    //    Div += '                                <option value="11">11</option>                 '
                    //    Div += '                                <option value="12">12</option>                 '
                    //    Div += '                                <option value="13">13</option>                 '
                    //    Div += '                                <option value="14">14</option>                 '
                    //    Div += '                                <option value="15">15</option>                 '
                    //    Div += '                                <option value="16">16</option>                 '
                    //    Div += '                                <option value="17">17</option>                 '
                    //    Div += '                                <option value="18">18</option>                 '
                    //    Div += '                                <option value="19">19</option>                 '
                    //    Div += '                                <option value="20">20</option>                 '
                    //    Div += '                                <option value="21">21</option>                 '
                    //    Div += '                                <option value="22">22</option>                 '
                    //    Div += '                                <option value="23">23</option>                 '
                    //    Div += '                                                                                           '
                    //    Div += '                            </select>                                                      '

                    //    Div += '</div>'

                    //    Div += '<div class="one-columns" style="margin-top: 34px">'

                    //    Div += '<select id="ddl_PickUpTimeMin' + Countt + '" style="height:30px" class="input full-width PickUpTimeMin">'
                    //    Div += '                                <option selected="" value="">Min</option>       '
                    //    Div += '                                <option value="00">00</option>                 '
                    //    Div += '                                <option value="15">15</option>                 '
                    //    Div += '                                <option value="30">30</option>                 '
                    //    Div += '                                <option value="45">45</option>                 '
                    //    Div += '                            </select>                                                      '

                    //    Div += '</div>'



                    //    Div += '<div class="three-columns" id="DropOffAt" >'
                    //    Div += '    <h6>Drop-Off At  :</h6>'
                    //    // Div += '    <input type="text" id="txt_DropOffAt' + Countt + '" class="input full-width DropOffAt" />'
                    //    Div += '<input type="text" name="Text[]" id="txt_DropOffAt' + Countt + '" list="Select_Location" class="input full-width DropOffAt"  />'
                    //    Div += '                           <datalist id="Select_Location"></datalist>'
                    //    Div += '</div>'

                    //    Div += '<div class="one-columns" id="DropOffTime' + Countt + '" >'
                    //    Div += '    <h6>Drop Time :</h6>'
                    //    //Div += '    <input type="time" id="txt_DropOffTime' + Countt + '" class="input full-width DropOffTime" />'

                    //    Div += ' <select id="ddl_DropOffTimeHours' + Countt + '" style="height:30px" class="input full-width DropOffTimeHours">'
                    //    Div += '                                <option selected="" value="">Hrs</option>       '
                    //    Div += '                                <option value="00">00</option>                 '
                    //    Div += '                                <option value="01">01</option>                 '
                    //    Div += '                                <option value="02">02</option>                 '
                    //    Div += '                                <option value="03">03</option>                 '
                    //    Div += '                                <option value="04">04</option>                 '
                    //    Div += '                                <option value="05">05</option>                 '
                    //    Div += '                                <option value="06">06</option>                 '
                    //    Div += '                                <option value="07">07</option>                 '
                    //    Div += '                                <option value="08">08</option>                 '
                    //    Div += '                                <option value="09">09</option>                 '
                    //    Div += '                                <option value="10">10</option>                 '
                    //    Div += '                                <option value="11">11</option>                 '
                    //    Div += '                                <option value="12">12</option>                 '
                    //    Div += '                                <option value="13">13</option>                 '
                    //    Div += '                                <option value="14">14</option>                 '
                    //    Div += '                                <option value="15">15</option>                 '
                    //    Div += '                                <option value="16">16</option>                 '
                    //    Div += '                                <option value="17">17</option>                 '
                    //    Div += '                                <option value="18">18</option>                 '
                    //    Div += '                                <option value="19">19</option>                 '
                    //    Div += '                                <option value="20">20</option>                 '
                    //    Div += '                                <option value="21">21</option>                 '
                    //    Div += '                                <option value="22">22</option>                 '
                    //    Div += '                                <option value="23">23</option>                 '
                    //    Div += '                                                                                           '
                    //    Div += '                            </select>                                                      '

                    //    Div += '</div>'
                    //    Div += '<div class="one-columns" style="margin-top: 34px">'

                    //    Div += '<select id="ddl_DropOffTimeMin' + Countt + '" style="height:30px" class="input full-width DropOffTimeMin">'
                    //    Div += '                                <option selected="" value="">Min</option>       '
                    //    Div += '                                <option value="00">00</option>                 '
                    //    Div += '                                <option value="15">15</option>                 '
                    //    Div += '                                <option value="30">30</option>                 '
                    //    Div += '                                <option value="45">45</option>                 '
                    //    Div += '                            </select>                                          '

                    //    Div += '</div>'
                    //    Div += '<div class="one-columns" style="margin-top: 34px" title="Delete Slot">'
                    //    Div += '<i Onclick="DeleteSlot(\'' + dtTable[i].Slot_Id + '\',\'' + id + '\',\'' + txtt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-trash"></span></label></i>'
                    //    Div += '</div>'
                    //}



                    //Div += '</div>'

                    Div += '<div id="btnAddSlot' + txtt + '" class="one-columns" title="Add Slot">'
                    Div += '<i Onclick="CheckEmptyy(\'' + Countt + '\',\'' + txtt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" style="margin-top: 35px"><span class="icon-plus">More Slot</span></label></i>'



                    if (Countt != 1) {
                        $("#btnAddSlot" + parseInt(txtt - 1)).hide();
                        Div += '<div class="one-columns" title="Remove Slot">'
                        Div += '<i Onclick="RemoveSlotUI(\'' + Count + '\',\'' + txtt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button blue-gradient" ><span class="icon-minus"></span></label></i>'
                        Div += '</div>'
                    }


                    Div += '</div>'
                    $("#SlotUI").append(Div);
                    $("#SlotUI").show();

                    $("#txt_SlotName" + Countt + "").val(dtTable[i].Slot_Name);
                    $("#lbl_SlotID" + Countt + "").val(dtTable[i].Slot_Id);
                    $("#txt_PriorityType" + Countt + "").val(dtTable[i].Priority_Type);
                    $("#ddl_TourStartHours" + Countt + "").val(dtTable[i].Tour_Start_Hours);
                    $("#ddl_TourStartMin" + Countt + "").val(dtTable[i].Tour_Start_Min);
                    $("#ddl_TourEndHours" + Countt + "").val(dtTable[i].Tour_End_Hours);
                    $("#ddl_TourEndMin" + Countt + "").val(dtTable[i].Tour_End_Min);
                    $("#txt_PickUpFrom" + Countt + "").val(dtTable[i].Pickup_From);
                    $("#ddl_PickUpTimeHours" + Countt + "").val(dtTable[i].Pickup_Time_Hours);
                    $("#ddl_PickUpTimeMin" + Countt + "").val(dtTable[i].Pickup_Time_Min);
                    $("#txt_DropOffAt" + Countt + "").val(dtTable[i].Drop_Off_At);
                    $("#ddl_DropOffTimeHours" + Countt + "").val(dtTable[i].Drop_Off_Time_Hours);
                    $("#ddl_DropOffTimeMin" + Countt + "").val(dtTable[i].Drop_Off_Time_Min);
                    $("#ddl_DropOffTimeMin" + Countt + "").val(dtTable[i].Drop_Off_Time_Min);

                    Countt += 2;
                    txtt++;

                    //$("#DatesUI").append(Div);
                    //$("#DatesUI").show();
                    //DateDisAuto(Count);
                    //$("#datepicker" + Count + "").val(dtTable[i].Operating_From);
                    //$("#datepicker" + parseInt(Count + 1) + "").val(dtTable[i].Operating_Till);
                    //Count += 2;
                    //txt++;
                    Upd_SLots += dtTable[i].Slot_Id + ";";

                }



            }
            else {
                Success("An error occured !!!");

            }
        },
        error: function () {

        }
    });
}


function DeleteSlot(Slot_Id, Act_Id, Div_Id) {
    // alert(id)
    var Data = {
        Act_Id: Act_Id,
        Slot_Id: Slot_Id
    }

    $.ajax({
        type: "POST",
        url: "../ActivityHandller.asmx/DeleteSlot",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                // window.location.href = "login.aspx"; // Session end
                Success("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                //$("#Dialog_Deletecandidate").dialog("close");
                Success("Slot deleted successfully");
                $("#MySlot" + Div_Id + "").remove();
                if (txt <= parseInt(Div_Id) + 1) {
                    $("#btnAddSlot" + parseInt(parseInt(Div_Id) - 1)).show();
                }
                if ($(".name").length == 1) {
                    $("#btnAddSlot").show();
                }
                // GetActivity(id);
            }
        },
        error: function () {
            Success("Error in deleting ");
        }
    });
}



function Getcity(City) {
   
    try {
        var checkclass = document.getElementsByClassName('check');
        var Cityy = City.split("^")

        $("#CityDiv .select span")[0].textContent = Cityy;
        for (var i = 0; i < Cityy.length - 1; i++) {
            //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
            $('input[value="' + Cityy[i] + '"][class="PPCity"]').prop("selected", true);
            $("#ddlcity").val(Cityy);
            pCities += Cityy[i] + "^";
            var selected = [];
            // $("#ddlcity").val(Cityy[i]);
            //$("#CityDiv .select span")[0].textContent = Cityy[i];
            //$('#ddlcity :selected').each(function () {
            //    selected[$(this).val(Cityy)] = $(this).text(Cityy);
            //});
            //$("#ddlcity option").each(function () {
            //    if ($(this).html() == Cityy[i]) {
            //        $(this).attr("selected", "selected");
            //        return;
            //    }
            //});
            
        }
        GetAddress(City);
    }
    catch (ex)
    { }

}


$("#ddlChildrenAllowed option").each(function () {
    if ($(this).html() == dtTable[0].Allowed_Child) {
        $(this).attr("selected", "selected");
        return;
    }
});

function GetAddress() {
   var lat = parseFloat(document.getElementById("txt_latitude").Value);
   var lng = parseFloat(document.getElementById("txt_longitude").Value);
    var latlng = new google.maps.LatLng(lat, lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                //  alert("Location: " + results[1].formatted_address);
                $("#txt_Location").val(results[0].formatted_address)
            }
        }
    });
}

function settourtype(stourType) {
   
    try {
        var Tours = stourType.split(";")
        for (var i = 0; i < Tours.length - 1; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');           

            $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}

function SetActivityType(ActType) {
   
    try {
        var Tours = ActType.split(";")
        for (var i = 0; Tours.length; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');           

            $('input[value="' + Tours[i].trim() + '"][class="chk_ActivityType"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}


function SetOperationDaysTKT(Days) {
   
    try {
        var OpDays = Days.split(";")
        for (var i = 0; OpDays.length; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');           

            $('input[value="' + OpDays[i].trim() + '"][class="chk_OperationDaysTKT"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}

function SetOperationDaysSIC(Days) {
   
    try {
        var OpDays = Days.split(";")
        for (var i = 0; OpDays.length; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');           

            $('input[value="' + OpDays[i].trim() + '"][class="chk_OperationDaysSIC"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}

function SetOperationDaysPVT(Days) {
   
    try {
        var OpDays = Days.split(";")
        for (var i = 0; OpDays.length; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');           

            $('input[value="' + OpDays[i].trim() + '"][class="chk_OperationDaysPVT"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}

function SetAttraction(sAttraction) {
   
    try {
        var noAttraction = $(".lbl_Atraction").length;
        var ListAtraction = sAttraction.split(";")
        for (var i = 0; i < ListAtraction.length; i++) {
            // $(".chk_Atraction").length;
            noAttraction = noAttraction + 1;
            if (ListAtraction[i] == "")
                continue;
            lblattraction = "";
            lblattraction = '<label class="size12 lbl_Atraction" id="lbl_Atraction' + noAttraction + '">' + ListAtraction[i] + '<i class="icon-cross-round" aria-hidden="true" style="padding-left:5px" onclick="DeleteAttraction (lbl_Atraction' + noAttraction + ')"></i></label><br>'
            //lblattraction = '<label class="size12 lbl_Atraction" id="lbl_Atraction' + noAttraction + '">' + $('#txt_Attraction').val() + '<i class="fa fa-times" aria-hidden="true" onclick="DeleteAttraction (lbl_Atraction' + noAttraction + ')"></i></label><br>'

            // lblattraction = '<input type="checkbox" value="' + ListAtraction[i] + '" checked="checked" id="chk_Atraction' + noAttraction + '" class="chk_Atraction" /> <label class="size12 lblAtraction" for="chk_Atraction' + noAttraction + '">' + ListAtraction[i] + '</label><br>'
            $("#idAttraction").append(lblattraction);
        }
    }
    catch (ex) {
    }
}
//function setlonglatiude(slonglatd) {
//    debugger
//    var Tours = slonglatd.split(";")
//    for (var i = 0; Tours.length; i++) {
//        $('input[value="' + Tours[i].replace(" ", "") + '"][class="chk_TourType"]').prop("checked", true);
//    }
//}

function AddActivity() {
   
    var TourType = "";
    for (var i = 0; i < $(".chk_TourType").length; i++) {
        if ($(".chk_TourType")[i].checked)
            TourType += $(".chk_TourType")[i].defaultValue + ";";
    }
    Attraction = "";
    for (var i = 0; i < $(".lbl_Atraction").length; i++) {

        //if ($(".chk_Atraction")[i].checked)                                                           
        //Attraction += $(".lbl_Atraction")[i].defaultValue + ";";                                    
        Attraction += $("#lbl_Atraction" + parseFloat(i + 1) + "").text() + ";";
    }
    bValid = Validate();
    if (bValid) {
        var Data = { Country: Country, City: pCities, activityname: activityname, subtitle: subtitle, Description: Description, Attraction: Attraction, longitude: longitude, lattitude: lattitude, TourNote: TourNote, TourType: TourType, Kid1Range: Kid1Range, Kid2Range: Kid2Range }
        $.ajax({
            type: "POST",
            url: "ActivityHandller.asmx/AddActivity",
            data: JSON.stringify(Data),
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    // alert("profile Inserted succesfully");


                    id = result.Sr_No;
                    sid = id;
                    $("#Button1").click();
                    // window.location.href = "AddActivityTariff.aspx?id=" + result.Sr_No
                    window.location.href = "activitylist.aspx";
                    //GetPartnerProfile();
                    //$('#txtage').val('');
                    //$('#txtage1').val('');
                    //$('#ddlmaritalstatus').val('');
                    //$('#ddlReligion').val('');
                    //$('#ddlMotherTongue').val('');
                    //$('#ddlCommunity').val('');
                }
                else {
                    Success("Something Going Wrong");
                }
            },
        });
    }
}

var lblattraction
function AddAttraction() {

    if ($('#txt_Attraction').val() != "") {
        //Attraction += $('#txt_Attraction').val() + ";";
        var noAttraction = $(".lbl_Atraction").length + 1;
        lblattraction = '<label class="size12 lbl_Atraction" id="lbl_Atraction' + noAttraction + '">' + $('#txt_Attraction').val() + '<i class="icon-cross-round" aria-hidden="true" style="padding-left:5px" onclick="DeleteAttraction (lbl_Atraction' + noAttraction + ')"></i></label><br>'

        // lblattraction = '<input type="checkbox" value="' + $('#txt_Attraction').val() + '" checked="checked"  id="chk_Atraction' + noAttraction + '" class="chk_Atraction"/> <label class="size12 lblAtraction" for="chk_Atraction' + noAttraction + '">' + $('#txt_Attraction').val() + '</label><br>'
        $("#idAttraction").append(lblattraction);
        $('#txt_Attraction').val("")
    }
    else {
        $('#txt_Attraction').focus()
        Success("Please Insert Attraction Name")
    }
}

function DeleteAttraction(lblthis) {
    $(lblthis).remove()
}

var Cities = "", selectedCities = []; var pCities = "";
function fnCities() {

    var Cities = $('.PPCity option:checked').map(function () {
        return this.value;
    }).get();

    //var Nationalities = '';
    var pCitiess = "";

    for (var i = 0; i < Cities.length; i++) {
        if (Cities[i] == "All") {
            pCitiess += '';
            pCities = pCitiess;
        }
        else {
            pCitiess += Cities[i] + "^";
            pCities = pCitiess;
        }

    }



}


var PLoc = ""; var pLocat = ""; var arrLocation = [];
function fnPicklocation() {

    PLoc = $('#txt_PickUpFrom' + ploc + ' option:checked').map(function () {
        return this.value;
    }).get();

    var pLocations = "";

    for (var i = 0; i < PLoc.length; i++) {
        if (PLoc[i] == "All") {
            pLocations += '';


        }
        else {
            pLocations += PLoc[i] + "^";
            pLocat = pLocations;

        }

    }
    arrLocation.length = 0;
    arrLocation.push(pLocat);
    SelectDropLocation(pLocat);
}


function SelectDropLocation(Location) {
    
    try {
        var checkclass = document.getElementsByClassName('check');
        var Locationn = Location.split("^")
        DLocat = "";
        //  $("#Div_DropOffAt" + dcount + " .select span")[dcount].textContent = Locationn;
        for (var i = 0; i < Locationn.length - 1; i++) {

            //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
            $('input[value="' + Locationn[i] + '"][class="DropOffAt' + dcount + '"]').prop("selected", true);
            $("#txt_DropOffAt" + dcount + "").val(Locationn);
            DLocat += Locationn[i] + "^";
            var selected = [];
            // $("#ddlcity").val(Cityy[i]);
            //$("#CityDiv .select span")[0].textContent = Cityy[i];
            //$('#ddlcity :selected').each(function () {
            //    selected[$(this).val(Cityy)] = $(this).text(Cityy);
            //});
            //$("#ddlcity option").each(function () {
            //    if ($(this).html() == Cityy[i]) {
            //        $(this).attr("selected", "selected");
            //        return;
            //    }
            //});
        }
    }
    catch (ex)
    { }

}


var DLoc = ""; var DLocat = "";
function fnDroplocation() {

    DLoc = $('#DropOffAt' + ploc + ' option:checked').map(function () {
        return this.value;
    }).get();

    var dLocations = "";

    for (var i = 0; i < DLoc.length; i++) {
        if (DLoc[i] == "All") {
            dLocations += '';
            DLocat = dLocations;
        }
        else {
            dLocations += DLoc[i] + "^";
            DLocat = dLocations;
        }

    }

}


//var DLoc = ""; var DLocat = "";
//function fnDroplocation() {

//    DLoc = $('.DropOffAt option:checked').map(function () {
//        return this.value;
//    }).get();

//    var dLocations = "";

//    for (var i = 0; i < DLoc.length; i++) {
//        if (DLoc[i] == "All") {
//            dLocations += '';
//            DLocat = dLocations;
//        }
//        else {
//            dLocations += DLoc[i] + "^";
//            DLocat = dLocations;
//        }

//    }
//}

var SeasonDate1 = "";
var SeasonDate2 = "";
var Name = "";
var TourStart = "";
var TourEnd = "";
var PickUpFrom = "";

var PickUpTime = "";
var DropOffAt = "";
var DropOffTime = "";
var PriorityType = "";



function Validate() {
    //alert("222")
    debugger
    Country = $('#ddlcountry').val();

    // City = $('#ddlcity').val();

    activityname = $('#txt_Activity').val();
    subtitle = $('#txt_subtitle').val();
    // sMotherTounge = $('#ddlMotherTongue').val();
    Description = $('#txt_AreaRemarks').val();
    // Attraction = $('#txt_Attraction').val();
    longitude = $('#txt_longitude').val();
    TourNote = $('#txt_tournote').val();
    lattitude = $('#txt_latitude').val();

    ChildrenAllowed = $('#ddlChildrenAllowed').val();

    if (activityname == '') {
        Success('Please enter activityname');
        return false;
    }
    if (Country == '') {
        Success('Please select country');
        return false;
    }
    if (subtitle == '') {
        Success('Please enter subtitle');
        return false;
    }
    if (Description == '') {
        Success('Please enter Description');
        return false;
    }
    if (longitude == '') {
        Success('Please enter longitude');
        return false;
    }
    if (Description == '') {
        Success('Please enter Description');
        return false;
    }
    if (Attraction == '') {
        Success('Please enter attraction');
        return false;
    }



    if (ChildrenAllowed == "Yes") {
        ChildAgeFrom = $('#ddlChildAgeFrom').val();

        ChildAgeUpTo = $('#ddlChildAgeUpTo').val();
        SmallChildAgeUpTo = $('#ddlSmallChildAgeUpTo').val();
        ChildMinHight = $('#txt_ChildMinHight').val();
        Infant = $('#ddlInfant').val();

        if (ddlInfant == "Allowed") {
            MaxNoofInfant = $('#txt_MaxNoofInfant').val();
        }
    }

    ActivityType = $('#ddlActivityType').val();
    PriorityType = $('#ddlPriorityType').val();


    FullYear = $('#ddlFullYear').val();
    if (FullYear == "No") {

        // Operating Dates       
        SeasonDate1 = "";
        SeasonDate2 = "";

        for (var i = 0; i < $(".dt1").length; i++) {

            var dt = $(".dt1")[i].value;
            SeasonDate1 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".dt1")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".dt1")[i].id).css("display", "none");
            }
        }
        for (var i = 0; i < $(".dt2").length; i++) {

            var dt = $(".dt2")[i].value;
            SeasonDate2 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".dt2")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".dt2")[i].id).css("display", "none");
            }
        }


        //Slots

        Name = "";
        TourStart = "";
        TourEnd = "";
        PickUpFrom = "";

        PickUpTime = "";
        DropOffAt = "";
        DropOffTime = "";
        PriorityType = "";


        for (var i = 0; i < $(".name").length - 1; i++) {

            var name = $(".name")[i].value;
            Name += name + "^";
            if (name == "") {
                $('#lbl_' + $(".name")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".name")[i].id).css("display", "none");
            }
        }
        for (var i = 0; i < $(".TourStart").length; i++) {

            var dt = $(".TourStart")[i].value;
            TourStart += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourStart")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourStart")[i].id).css("display", "none");
            }
        }
        for (var i = 0; i < $(".TourEnd").length; i++) {

            var dt = $(".TourEnd")[i].value;
            TourEnd += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourEnd")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourEnd")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".PickUpFrom").length; i++) {

            var dt = $(".PickUpFrom")[i].value;
            PickUpFrom += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".PickUpFrom")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".PickUpFrom")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".PickUpTime").length; i++) {

            var dt = $(".PickUpTime")[i].value;
            PickUpTime += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".PickUpTime")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".PickUpTime")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".DropOffAt").length; i++) {

            var dt = $(".DropOffAt")[i].value;
            SeasonDate2 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".DropOffAt")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".DropOffAt")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".DropOffTime").length; i++) {

            var dt = $(".DropOffTime")[i].value;
            SeasonDate2 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".DropOffTime")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".DropOffTime")[i].id).css("display", "none");
            }
        }


        for (var i = 0; i < $(".PriorityType").length; i++) {

            var dt = $(".PriorityType")[i].value;
            SeasonDate2 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".PriorityType")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".PriorityType")[i].id).css("display", "none");
            }
        }


    }

    MinCapacity = $('#txt_MinCapacity').val();
    MaximumCapacity = $('#txt_MaximumCapacity').val();


    return true;
}

function Update_Click(id) {

    var TourType = "";
    for (var i = 0; i < $(".chk_TourType").length; i++) {
        if ($(".chk_TourType")[i].checked)
            TourType += $(".chk_TourType")[i].defaultValue + ";";
    }
    Attraction = "";
    var terere
    for (var i = 0; i < $(".lbl_Atraction").length; i++) {
        //if ($(".chk_Atraction")[i].checked)
        //terere = $("#lbl_Atraction" + parseFloat(i + 1) + "").text();   
        //Attraction += $(".lbl_Atraction")[i].defaultValue + ";";
        Attraction += $(".lbl_Atraction")[i].outerText + ";";
        //Attraction += onclick = "Delete("+ id + ")";
    }
    Country = $('#ddlcountry').val();
    //City = $('#ddlcity').val();
    activityname = $('#txt_Activity').val();
    subtitle = $('#txt_subtitle').val();
    // sMotherTounge = $('#ddlMotherTongue').val();
    Description = $('#txt_AreaRemarks').val();
    // Attraction = $('#txt_Attraction').val();
    longitude = $('#txt_longitude').val();
    TourNote = $('#txt_tournote').val();
    lattitude = $('#txt_latitude').val();

    $.ajax({
        type: "POST",
        url: "ActivityHandller.asmx/UpdateActivityDetails",
        data: '{"Country":"' + Country + '","City":"' + pCities + '","activityname":"' + activityname + '","subtitle":"' + subtitle + '","Description":"' + Description + '","subtitle":"' + subtitle + '","longitude":"' + longitude + '","TourNote":"' + TourNote + '","lattitude":"' + lattitude + '","Attraction":"' + Attraction + '","TourType":"' + TourType + '","id":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Details updated successfully")
                // window.location.href = "Profile.aspx";
            }
            if (result.retCode == 0) {
                Success("Something went wrong!")
            }
        },
        error: function () {
            Success("An error occured while updating details");
        }
    });
}



//function loadcheckbox() {
//    //  alert("1111");
//    $.ajax({
//        type: "POST",
//        url: "../ActivityHandller.asmx/GetTourTypes",
//        data: '',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                arTypeList = result.tblType;
//                if (arTypeList.length > 0) {
//                    debugger
//                    var trForms = '';
//                    for (i = 0; i < arTypeList.length; i = i + 3)
//                    {
//                        if (i < arTypeList.length)
//                        {
//                            //trForms += ' <option value="All Selected" onchange="SelectAll()">All Selected</option>'
//                            //trForms += '<tr>';
//                            //trForms += '<td><input id="chk' + arTypeList[i].Sid + '" type="checkbox" class="chk_TourType" value="' + arTypeList[i].TourType + '"/><label class="lblAtraction" for="chk' + arTypeList[i].Sid + '">' + arTypeList[i].TourType + '</label></td>';
//                            //if ((i + 1) < arTypeList.length)
//                            //    trForms += '<td><input id="chk' + arTypeList[i + 1].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 1].TourType + '"/><label class="lblAtraction" for="chk' + arTypeList[i + 1].Sid + '">' + arTypeList[i + 1].TourType + '</label></td>';
//                            //if ((i + 2) < arTypeList.length)
//                            //    trForms += '<td><input id="chk' + arTypeList[i + 2].Sid + '" class="chk_TourType" type="checkbox" value="' + arTypeList[i + 2].TourType + '"/><label class="lblAtraction" for="chk' + arTypeList[i + 2].Sid + '">' + arTypeList[i + 2].TourType + '</label></td>';
//                            //trForms += '</tr>';

//                            trForms += '<option value="' + arTypeList[i].TourType + '">' + arTypeList[i].TourType + '</option>'
//                            if ((i + 1) < arTypeList.length)
//                                trForms += '<option value="' + arTypeList[i + 1].TourType + '">' + arTypeList[i + 1].TourType + '</option>'
//                            if ((i + 2) < arTypeList.length)
//                                trForms += '<option value="' + arTypeList[i + 2].TourType + '">' + arTypeList[i + 2].TourType + '</option>'
//                        }
//                    }

//                    $("#ddlTourType").append(trForms);

//                }
//            }
//        },
//        //error: function () {
//        //    Success("An error occured while geting form list");
//        //}
//    });
//}

function selectAll() {
    var selectVal = $('#ddlTourType').val();
    if ($('#ddlTourType option:selected').val() == "All Selected") {
        if ($('#ddlTourType option:selected').val() == "All Selected") {
            $('#ddlTourType option').prop('selected', true);
        }
        else {
            $('#ddlTourType option').prop('selected', false);
        }
    }
    else {
        $('#ddlTourType option').prop('selected', false);
    }



}

function DaysChecked() {

    if ($('#Chk_Mon').prop("checked") || $('#Chk_Tue').prop("checked") || $('#Chk_Wed').prop("checked") || $('#Chk_Thu').prop("checked") || $('#Chk_Fri').prop("checked") || $('#Chk_Sat').prop("checked") || $('#Chk_Sun').prop("checked"))
    {

        chk_Daily.checked = false;
     
    }
    if ($('#Chk_Mon').prop("checked") && $('#Chk_Tue').prop("checked") && $('#Chk_Wed').prop("checked") && $('#Chk_Thu').prop("checked") && $('#Chk_Fri').prop("checked") && $('#Chk_Sat').prop("checked") && $('#Chk_Sun').prop("checked"))
    {
        chk_Daily.checked = true;
        
    }
   

}


function DaysCheckedTKT() {

    if ($('#Chk_MonTKT').prop("checked") || $('#Chk_TueTKT').prop("checked") || $('#Chk_WedTKT').prop("checked") || $('#Chk_ThuTKT').prop("checked") || $('#Chk_FriTKT').prop("checked") || $('#Chk_SatTKT').prop("checked") || $('#Chk_SunTKT').prop("checked")) {

        chk_DailyTKT.checked = false;

    }
    if ($('#Chk_MonTKT').prop("checked") && $('#Chk_TueTKT').prop("checked") && $('#Chk_WedTKT').prop("checked") && $('#Chk_ThuTKT').prop("checked") && $('#Chk_FriTKT').prop("checked") && $('#Chk_SatTKT').prop("checked") && $('#Chk_SunTKT').prop("checked")) {
        chk_DailyTKT.checked = true;

    }


}

function DaysAllCheckedTKT() {
    if ($('#chk_DailyTKT').prop("checked")) {

        Chk_MonTKT.checked = true;
        Chk_TueTKT.checked = true;
        Chk_WedTKT.checked = true;
        Chk_ThuTKT.checked = true;
        Chk_FriTKT.checked = true;
        Chk_SatTKT.checked = true;
        Chk_SunTKT.checked = true;


    }
    else if ($('#chk_DailyTKT').prop("checked", false)) {

        Chk_MonTKT.checked = false;
        Chk_TueTKT.checked = false;
        Chk_WedTKT.checked = false;
        Chk_ThuTKT.checked = false;
        Chk_FriTKT.checked = false;
        Chk_SatTKT.checked = false;
        Chk_SunTKT.checked = false;
    }
}


function DaysCheckedSIC() {

    if ($('#Chk_MonSIC').prop("checked") || $('#Chk_TueSIC').prop("checked") || $('#Chk_WedSIC').prop("checked") || $('#Chk_ThuSIC').prop("checked") || $('#Chk_FriSIC').prop("checked") || $('#Chk_SatSIC').prop("checked") || $('#Chk_SunSIC').prop("checked")) {

        chk_DailySIC.checked = false;

    }
    if ($('#Chk_MonSIC').prop("checked") && $('#Chk_TueSIC').prop("checked") && $('#Chk_WedSIC').prop("checked") && $('#Chk_ThuSIC').prop("checked") && $('#Chk_FriSIC').prop("checked") && $('#Chk_SatSIC').prop("checked") && $('#Chk_SunSIC').prop("checked")) {
        chk_DailySIC.checked = true;

    }


}

function DaysAllCheckedSIC() {
    if ($('#chk_DailySIC').prop("checked")) {

        Chk_MonSIC.checked = true;
        Chk_TueSIC.checked = true;
        Chk_WedSIC.checked = true;
        Chk_ThuSIC.checked = true;
        Chk_FriSIC.checked = true;
        Chk_SatSIC.checked = true;
        Chk_SunSIC.checked = true;


    }
    else if ($('#chk_DailySIC').prop("checked", false)) {

        Chk_MonSIC.checked = false;
        Chk_TueSIC.checked = false;
        Chk_WedSIC.checked = false;
        Chk_ThuSIC.checked = false;
        Chk_FriSIC.checked = false;
        Chk_SatSIC.checked = false;
        Chk_SunSIC.checked = false;
    }
}


function DaysCheckedPVT() {

    if ($('#Chk_MonPVT').prop("checked") || $('#Chk_TuePVT').prop("checked") || $('#Chk_WedPVT').prop("checked") || $('#Chk_ThuPVT').prop("checked") || $('#Chk_FriPVT').prop("checked") || $('#Chk_SatPVT').prop("checked") || $('#Chk_SunPVT').prop("checked")) {

        chk_DailyPVT.checked = false;

    }
    if ($('#Chk_MonPVT').prop("checked") && $('#Chk_TuePVT').prop("checked") && $('#Chk_WedPVT').prop("checked") && $('#Chk_ThuPVT').prop("checked") && $('#Chk_FriPVT').prop("checked") && $('#Chk_SatPVT').prop("checked") && $('#Chk_SunPVT').prop("checked")) {
        chk_DailyPVT.checked = true;

    }


}

function DaysAllCheckedPVT() {
    if ($('#chk_DailyPVT').prop("checked")) {

        Chk_MonPVT.checked = true;
        Chk_TuePVT.checked = true;
        Chk_WedPVT.checked = true;
        Chk_ThuPVT.checked = true;
        Chk_FriPVT.checked = true;
        Chk_SatPVT.checked = true;
        Chk_SunPVT.checked = true;


    }
    else if ($('#chk_DailyPVT').prop("checked", false)) {

        Chk_MonPVT.checked = false;
        Chk_TuePVT.checked = false;
        Chk_WedPVT.checked = false;
        Chk_ThuPVT.checked = false;
        Chk_FriPVT.checked = false;
        Chk_SatPVT.checked = false;
        Chk_SunPVT.checked = false;
    }
}

function ActivityTypeCheck() {

    if ($('#Chk_TktOnly').prop("checked") || $('#Chk_Sic').prop("checked")) {
        chk_All.checked = false;
    }

    if ($('#Chk_TktOnly').prop("checked") && $('#Chk_Sic').prop("checked")) {
        chk_All.checked = true;
    }
    if ($('#Chk_TktOnly').prop("checked")) {
        $('#OperationDaysTKT').show();
    }
    else {
        $('#OperationDaysTKT').hide();
    }
    if ($('#Chk_Sic').prop("checked")) {
        $('#OperationDaysSIC').show();
    }
    else {
        $('#OperationDaysSIC').hide();
    }
    //if ($('#Chk_Pvt').prop("checked")) {
    //    $('#OperationDaysPVT').show();
    //}
    //else {
    //    $('#OperationDaysPVT').hide();
    //}
}

function ActivityTypeAllCheck() {
    if ($('#chk_All').prop("checked")) {
        Chk_TktOnly.checked = true;
        Chk_Sic.checked = true;
        //Chk_Pvt.checked = true;
        $('#OperationDaysTKT').show();
        $('#OperationDaysSIC').show();
        //$('#OperationDaysPVT').show();
    }
    else if ($('#chk_All').prop("checked", false)) {
        Chk_TktOnly.checked = false;
        Chk_Sic.checked = false;
        //Chk_Pvt.checked = false;
        $('#OperationDaysTKT').hide();
        $('#OperationDaysSIC').hide();
        //$('#OperationDaysPVT').hide();
    }

}

var SeasonDate1 = "";
var SeasonDate2 = "";
var Name = "";
var TourStart = "";
var TourStartH = "";
var TourStartM = "";
var TourEnd = "";
var TourEndH = "";
var TourEndM = "";
var PickUpFrom = "";

var PickUpTime = "";
var PickUpTimeH = "";
var PickUpTimeM = "";
var DropOffAt = "";
var DropOffTime = "";
var DropOffTimeH = "";
var DropOffTimeM = "";
var PriorityType = "";
var PriorityTyp = "";



function NewValidate() {

    debugger

    AgencyName = $('#txt_Agency').val();
    activityname = $('#txt_Activity').val();
    subtitle = $('#txt_subtitle').val();
    Description = $('#txt_AreaRemarks').val();
    Location = $("#txt_location").val();
    Country = $('#ddlcountry').val();
    longitude = $('#txt_longitude').val();
    TourNote = $('#txt_tournote').val();
    lattitude = $('#txt_latitude').val();
    ChildrenAllowed = $('#ddlChildrenAllowed').val();

    if (activityname == '') {
        Success('Please enter activityname');
        return false;
    }
    if (Country == '') {
        Success('Please select country');
        return false;
    }
    if (subtitle == '') {
        Success('Please enter subtitle');
        return false;
    }
    if (Description == '') {
        Success('Please enter Description');
        return false;
    }

    if (Description == '') {
        Success('Please enter Description');
        return false;
    }
    if (Attraction == '') {
        Success('Please enter attraction');
        return false;
    }

    if (AgencyName == '') {
        Success('Please enter Agency Name');
        return false;
    }

    if (ChildrenAllowed == "Yes") {
        ChildAgeFrom = $('#ddlChildAgeFrom').val();

        ChildAgeUpTo = $('#ddlChildAgeUpTo').val();
        SmallChildAgeUpTo = $('#ddlSmallChildAgeUpTo').val();



    }
    else {
        ChildAgeFrom = 0;
        ChildAgeUpTo = 0;
        SmallChildAgeUpTo = 0;



    }

    if (allowheight == "Yes") {
        ChildMinHight = $('#txt_ChildMinHight').val();
    }
    else {
        ChildMinHight = 0;
    }


    Infant = $('#ddlInfant').val();

    if (Infant == "Allowed") {
        MaxNoofInfant = $('#txt_MaxNoofInfant').val();
    }
    else {
        MaxNoofInfant = 0;
    }

    PriorityType = $('#ddlPriorityType').val();


    FullYear = $('#ddlFullYear').val();
    if (FullYear == "No") {

        // Operating Dates       
        SeasonDate1 = "";
        SeasonDate2 = "";

        for (var i = 0; i < $(".dt1").length; i++) {

            var dt = $(".dt1")[i].value;
            SeasonDate1 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".dt1")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".dt1")[i].id).css("display", "none");
            }
        }
        for (var i = 0; i < $(".dt2").length; i++) {

            var dt = $(".dt2")[i].value;
            SeasonDate2 += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".dt2")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".dt2")[i].id).css("display", "none");
            }
        }


        //Slots

        Name = "";
        TourStart = "";
        TourStartH = "";
        TourStartM = "";
        TourEnd = "";
        TourEndH = "";
        TourEndM = "";
        PickUpFrom = "";

        PickUpTime = "";
        PickUpTimeH = "";
        PickUpTimeM = "";
        DropOffAt = "";
        DropOffTime = "";
        DropOffTimeH = "";
        DropOffTimeM = "";
        PriorityTyp = "";


        //var datefrom = [];
        //for (var i = 0; i < $(".dt1").length; i++) {
        //    if ($(".dt1")[i].value != "")
        //        datefrom.push($(".dt1")[i].value);
        //}

        for (var i = 0; i < $(".name").length - 1; i++) {

            var name = $(".name")[i].value;
            Name += name + "^";
            if (name == "") {
                $('#lbl_' + $(".name")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".name")[i].id).css("display", "none");
            }
        }


        for (var i = 0; i < $(".TourStartHours").length; i++) {

            var dt = $(".TourStartHours")[i].value;
            TourStartH += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourStartHours")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourStartHours")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".TourStartMin").length; i++) {

            var dt = $(".TourStartMin")[i].value;
            TourStartM += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourStartMin")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourStartMin")[i].id).css("display", "none");
            }
        }


        for (var i = 0; i < $(".TourEndHours").length; i++) {

            var dt = $(".TourEndHours")[i].value;
            TourEndH += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourEndHours")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourEndHours")[i].id).css("display", "none");
            }
        }

        for (var i = 0; i < $(".TourEndMin").length; i++) {

            var dt = $(".TourEndMin")[i].value;
            TourEndM += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".TourEndMin")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".TourEndMin")[i].id).css("display", "none");
            }
        }




        //for (var i = 0; i < $(".PickUpFrom").length; i++) {

        //    var dt = $(".PickUpFrom")[i].value;
        //    PickUpFrom += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".PickUpFrom")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".PickUpFrom")[i].id).css("display", "none");
        //    }
        //}

        //for (var i = 0; i < $(".PickUpTimeHours").length; i++) {

        //    var dt = $(".PickUpTimeHours")[i].value;
        //    PickUpTimeH += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".PickUpTimeHours")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".PickUpTimeHours")[i].id).css("display", "none");
        //    }
        //}

        //for (var i = 0; i < $(".PickUpTimeMin").length; i++) {

        //    var dt = $(".PickUpTimeMin")[i].value;
        //    PickUpTimeM += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".PickUpTimeMin")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".PickUpTimeMin")[i].id).css("display", "none");
        //    }
        //}

        //for (var i = 0; i < $(".DropOffAt").length; i++) {

        //    var dt = $(".DropOffAt")[i].value;
        //    DropOffAt += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".DropOffAt")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".DropOffAt")[i].id).css("display", "none");
        //    }
        //}

        //for (var i = 0; i < $(".DropOffTimeHours").length; i++) {

        //    var dt = $(".DropOffTimeHours")[i].value;
        //    DropOffTimeH += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".DropOffTimeHours")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".DropOffTimeHours")[i].id).css("display", "none");
        //    }
        //}

        //for (var i = 0; i < $(".DropOffTimeMin").length; i++) {

        //    var dt = $(".DropOffTimeMin")[i].value;
        //    DropOffTimeM += dt + "^";
        //    if (dt == "") {
        //        $('#lbl_' + $(".DropOffTimeMin")[i].id).css("display", "");
        //        bValid = false;
        //    }
        //    else {
        //        $('#lbl_' + $(".DropOffTimeMin")[i].id).css("display", "none");
        //    }
        //}

        for (var i = 0; i < $(".PriorityType").length; i++) {

            var dt = $(".PriorityType")[i].value;
            PriorityTyp += dt + "^";
            if (dt == "") {
                $('#lbl_' + $(".PriorityType")[i].id).css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_' + $(".PriorityType")[i].id).css("display", "none");
            }
        }


    }

    MinCapacity = $('#txt_MinCapacity').val();
    MaximumCapacity = $('#txt_MaximumCapacity').val();
    if (MinCapacity == "") {
        Success("Select Minimum Capacity");
    }
    else if (MaximumCapacity == "") {
        MaximumCapacity = 0;
    }

    return true;
}


var allowheight;
var Infantallowed;
var AllowMaxChild = "";
var MaxChild = 0;
var FulYear = "No";
var redirtect;
var pickfrom = [];
var picktimeH = [];
var picktimeM = [];
var dropat = [];
var droptimeH = [];
var droptimeM = [];
//var OperatingTimeFrom;
//var OperatingTimeTill;
//var slotname;
//var TourStar;
//var TourEnd;

function SaveActivity() {

    $("#Loader").show();
    $("#btn_Save").prop("disabled", true);

    allowheight = $("#ddlChildrenHeight option:selected").val();
    Infantallowed = $("#ddlInfant option:selected").val();
    FulYear = $("#ddlFullYear option:selected").val();
    
    //SLOT

    var sname = [];
    for (var i = 0; i < $(".name").length - 1; i++) {
        if ($(".name")[i].value != "")
            sname.push($(".name")[i].value);
    }


 
    var tstartH = [];
    for (var i = 0; i < $(".TourStartHours").length; i++) {
        if ($(".TourStartHours")[i].value != "")
            tstartH.push($(".TourStartHours")[i].value);
    }
    var tstartM = [];
    for (var i = 0; i < $(".TourStartMin").length; i++) {
        if ($(".TourStartMin")[i].value != "")
            tstartM.push($(".TourStartMin")[i].value);
    }

    var tendH = [];
    for (var i = 0; i < $(".TourEndHours").length; i++) {
        if ($(".TourEndHours")[i].value != "")
            tendH.push($(".TourEndHours")[i].value);
    }


    var tendM = [];
    for (var i = 0; i < $(".TourEndMin").length; i++) {
        if ($(".TourEndMin")[i].value != "")
            tendM.push($(".TourEndMin")[i].value);
    }

    

    var prtype = [];
    for (var i = 0; i < $(".PriorityType").length; i++) {
        if ($(".PriorityType")[i].value != "")
            prtype.push($(".PriorityType")[i].value);
    }



    //EndSlot


    var datefrom = [];
    for (var i = 0; i < $(".dt1").length; i++) {
        if ($(".dt1")[i].value != "")
            datefrom.push($(".dt1")[i].value);
    }
    var dateTill = [];
    for (var i = 0; i < $(".dt2").length; i++) {
        if ($(".dt2")[i].value != "")
            dateTill.push($(".dt2")[i].value);
    }

    var TourType = "";
    for (var i = 0; i < $(".chk_TourType").length; i++) {
        if ($(".chk_TourType")[i].checked)
            TourType += $(".chk_TourType")[i].defaultValue + ";";
    }
    Attraction = "";
    for (var i = 0; i < $(".lbl_Atraction").length; i++) {

        //if ($(".chk_Atraction")[i].checked)                                                           
        //Attraction += $(".lbl_Atraction")[i].defaultValue + ";";                                    
        Attraction += $("#lbl_Atraction" + parseFloat(i + 1) + "").text() + ";";
    }
    var ActivityType = "";
    for (var i = 0; i < $(".chk_ActivityType").length; i++) {
        if ($(".chk_ActivityType")[i].checked)
            ActivityType += $(".chk_ActivityType")[i].defaultValue + ";";
    }

    var OperationDaysTKT = "";
    var OperationDaysSIC = "";
    //var OperationDaysPVT = "";
    var SplitActivityType = ActivityType.split(';');
    for (var i = 0; i < SplitActivityType.length; i++)
    {
        if (SplitActivityType[i]=="TKT")
        {
            for (var j = 0; j < $(".chk_OperationDaysTKT").length; j++) {
                if ($(".chk_OperationDaysTKT")[j].checked)
                    OperationDaysTKT += $(".chk_OperationDaysTKT")[j].defaultValue + ";";
            }
        }
        else if (SplitActivityType[i] == "SIC")
        {
            for (var j = 0; j < $(".chk_OperationDaysSIC").length; j++) {
                if ($(".chk_OperationDaysSIC")[j].checked)
                    OperationDaysSIC += $(".chk_OperationDaysSIC")[j].defaultValue + ";";
            }
        }
        //else if (SplitActivityType[i] == "PVT") {
        //    for (var j = 0; j < $(".chk_OperationDaysPVT").length; j++) {
        //        if ($(".chk_OperationDaysPVT")[j].checked)
        //            OperationDaysPVT += $(".chk_OperationDaysPVT")[j].defaultValue + ";";
        //    }
        //}
    }

    var OperationDays = "";
  
    bValid = NewValidate();



    if (bValid) {
        var City = pCities;
        var Data = {
            activityname: activityname,
            subtitle: subtitle,
            Country: Country,
            City: City,
            Location: Location,
            Description: Description,
            ChildrenAllowed: ChildrenAllowed,
            ChildAgeFrom: ChildAgeFrom,
            ChildAgeUpTo: ChildAgeUpTo,
            SmallChildAgeUpTo: SmallChildAgeUpTo,
            allowheight: allowheight,
            ChildMinHight: ChildMinHight,
            Infantallowed: Infantallowed,
            MaxNoofInfant: MaxNoofInfant,
            AllowMaxChild: AllowMaxChild,
            MaxChild: MaxChild,
            Attraction: Attraction,
            TourType: TourType,

            MinCapacity: MinCapacity,
            MaximumCapacity: MaximumCapacity,
            TourNote: TourNote,
            ActivityType: ActivityType,
            PriorityType: PriorityType,            //act type priority//
            OperationDays: OperationDays,

            OperationDaysTKT: OperationDaysTKT,
            OperationDaysSIC: OperationDaysSIC,
            OperationDaysPVT: "",

            datefrom: datefrom,
            dateTill: dateTill,
            FulYear: FulYear,
            Name: sname,
            //TourStart: tstart,
            TourStartH: tstartH,
            TourStartM: tstartM,
            //TourEnd: tend,
            TourEndH: tendH,
            TourEndM: tendM,
            PickUpFrom: pickfrom,
            //PickUpTime: picktime,
            PickUpTimeH: picktimeH,
            PickUpTimeM: picktimeM,
            DropOffAt: dropat,
            //DropOffTime: droptime,
            DropOffTimeH: droptimeH,
            DropOffTimeM: droptimeM,
            PriorityTyp: prtype,          // operatin time priority//
            AllowSlot: Slot

        }
        $.ajax({
            type: "POST",
            url: "ActivityHandller.asmx/SaveActivity",
            data: JSON.stringify(Data),
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Activity Inserted succesfully");


                    id = result.Sr_No;
                    sid = id;

                    redirtect = "Activityratelist.aspx?id=" + result.Sr_No + "&pCities=" + City + "&name=" + activityname + "&country=" + Country + "&location=" + Location;
                    $("#Button1").click();
                    //$("#Button1").bind("click",
                    //        { sid: "sid"},
                    //        function buttonClick(event) {
                    //            $("#displayArea").text(event.data.sid);
                    //        }
                    //        );


                    //window.location.href = "activitylist.aspx";
                    //window.location.href = "Activityratelist.aspx";
                    //GetPartnerProfile();
                    //$('#txtage').val('');
                    //$('#txtage1').val('');
                    //$('#ddlmaritalstatus').val('');
                    //$('#ddlReligion').val('');
                    //$('#ddlMotherTongue').val('');
                    //$('#ddlCommunity').val('');
                }
                else {
                    Success("Something Going Wrong");
                }
            },
        });
    }
}




function UpdateActivity(id) {

    $("#Loader").show();
    $("#btn_Save").prop("disabled", true);

    allowheight = $("#ddlChildrenHeight option:selected").val();
    Infantallowed = $("#ddlInfant option:selected").val();
    //AllowMaxChild = $("#ddlMaxChildren option:selected").val(); 
    //PriorityType = $("#ddlPriorityType option:selected").val();
    //Status = $("#ddlStatus option:selected").val();
    //Supplier = $("#txt_Agency").val();

    //if (AllowMaxChild == "Yes") {
    //    MaxChild = $('#txt_MaxChild').val();
    //}
    //else {
    //    MaxChild = 0;
    //}

    FulYear = $("#ddlFullYear option:selected").val();
    //OperatingTimeFrom = $('#datepicker1').val();
    //OperatingTimeTill = $('#datepicker2').val();
    //slotname      = $('#txt_SlotName1').val();
    //TourStar      = $('#txt_TourStart1').val();
    //TourEnd       = $('#txt_TourEnd1').val();


    //SLOT

    var sname = [];
    for (var i = 0; i < $(".name").length - 1; i++) {
        if ($(".name")[i].value != "")
            sname.push($(".name")[i].value);
    }

    var SlotValue = [];
    for (var i = 0; i < $(".SValue").length; i++) {
        //if ($(".SValue")[i].value != "")
        SlotValue.push($(".SValue")[i].value);
    }
    //var tstart = [];
    //for (var i = 0; i < $(".TourStart").length; i++) {
    //    if ($(".TourStart")[i].value != "")
    //        tstart.push($(".TourStart")[i].value);
    //}
    var tstartH = [];
    for (var i = 0; i < $(".TourStartHours").length; i++) {
        if ($(".TourStartHours")[i].value != "")
            tstartH.push($(".TourStartHours")[i].value);
    }
    var tstartM = [];
    for (var i = 0; i < $(".TourStartMin").length; i++) {
        if ($(".TourStartMin")[i].value != "")
            tstartM.push($(".TourStartMin")[i].value);
    }

    var tendH = [];
    for (var i = 0; i < $(".TourEndHours").length; i++) {
        if ($(".TourEndHours")[i].value != "")
            tendH.push($(".TourEndHours")[i].value);
    }


    var tendM = [];
    for (var i = 0; i < $(".TourEndMin").length; i++) {
        if ($(".TourEndMin")[i].value != "")
            tendM.push($(".TourEndMin")[i].value);
    }

    //var pickfrom = [];
    //for (var i = 0; i < $(".PickUpFrom").length; i++) {
    //    if ($(".PickUpFrom")[i].value != "")
    //        pickfrom.push($(".PickUpFrom")[i].value);
    //}


    //var picktimeH = [];
    //for (var i = 0; i < $(".PickUpTimeHours").length; i++) {
    //    if ($(".PickUpTimeHours")[i].value != "")
    //        picktimeH.push($(".PickUpTimeHours")[i].value);
    //}
    //var picktimeM = [];
    //for (var i = 0; i < $(".PickUpTimeMin").length; i++) {
    //    if ($(".PickUpTimeMin")[i].value != "")
    //        picktimeM.push($(".PickUpTimeMin")[i].value);
    //}

    //var dropat = [];
    //for (var i = 0; i < $(".DropOffAt").length; i++) {
    //    if ($(".DropOffAt")[i].value != "")
    //        dropat.push($(".DropOffAt")[i].value);
    //}


    //var droptimeH = [];
    //for (var i = 0; i < $(".DropOffTimeHours").length; i++) {
    //    if ($(".DropOffTimeHours")[i].value != "")
    //        droptimeH.push($(".DropOffTimeHours")[i].value);
    //}
    //var droptimeM = [];
    //for (var i = 0; i < $(".DropOffTimeMin").length; i++) {
    //    if ($(".DropOffTimeMin")[i].value != "")
    //        droptimeM.push($(".DropOffTimeMin")[i].value);
    //}

    var prtype = [];
    for (var i = 0; i < $(".PriorityType").length; i++) {
        if ($(".PriorityType")[i].value != "")
            prtype.push($(".PriorityType")[i].value);
    }



    //EndSlot


    var datefrom = [];
    for (var i = 0; i < $(".dt1").length; i++) {
        if ($(".dt1")[i].value != "")
            datefrom.push($(".dt1")[i].value);
    }
    var dateTill = [];
    for (var i = 0; i < $(".dt2").length; i++) {
        if ($(".dt2")[i].value != "")
            dateTill.push($(".dt2")[i].value);
    }

    var TourType = "";
    for (var i = 0; i < $(".chk_TourType").length; i++) {
        if ($(".chk_TourType")[i].checked)
            TourType += $(".chk_TourType")[i].defaultValue + ";";
    }
    Attraction = "";
    for (var i = 0; i < $(".lbl_Atraction").length; i++) {

        //if ($(".chk_Atraction")[i].checked)                                                           
        //Attraction += $(".lbl_Atraction")[i].defaultValue + ";";                                    
        Attraction += $("#lbl_Atraction" + parseFloat(i + 1) + "").text() + ";";
    }
    var ActivityType = "";
    for (var i = 0; i < $(".chk_ActivityType").length; i++) {
        if ($(".chk_ActivityType")[i].checked)
            ActivityType += $(".chk_ActivityType")[i].defaultValue + ";";
    }

    var OperationDaysTKT = "";
    var OperationDaysSIC = "";
    //var OperationDaysPVT = "";
    var SplitActivityType = ActivityType.split(';');
    for (var i = 0; i < SplitActivityType.length; i++) {
        if (SplitActivityType[i] == "TKT") {
            for (var j = 0; j < $(".chk_OperationDaysTKT").length; j++) {
                if ($(".chk_OperationDaysTKT")[j].checked)
                    OperationDaysTKT += $(".chk_OperationDaysTKT")[j].defaultValue + ";";
            }
        }
        else if (SplitActivityType[i] == "SIC") {
            for (var j = 0; j < $(".chk_OperationDaysSIC").length; j++) {
                if ($(".chk_OperationDaysSIC")[j].checked)
                    OperationDaysSIC += $(".chk_OperationDaysSIC")[j].defaultValue + ";";
            }
        }
        //else if (SplitActivityType[i] == "PVT") {
        //    for (var j = 0; j < $(".chk_OperationDaysPVT").length; j++) {
        //        if ($(".chk_OperationDaysPVT")[j].checked)
        //            OperationDaysPVT += $(".chk_OperationDaysPVT")[j].defaultValue + ";";
        //    }
        //}
    }

    var OperationDays = "";
    //for (var i = 0; i < $(".chk_OperationDays").length; i++) {
    //    if ($(".chk_OperationDays")[i].checked)
    //        OperationDays += $(".chk_OperationDays")[i].defaultValue + ";";
    //}
    //var Days = "";
    //if (OperationDays=="Daily;Mon;Tue;wed;Thu;Fri;Sat;Sun;")
    //{
    //    Days = "Daily";
    //}
    //else
    //{
    //    Days = OperationDays;
    //}
    //$("#Button1").click();
    bValid = NewValidate();



    if (bValid) {
        var City = pCities;
        var Data = {
            id: id,
            UTer_ID: UTer_ID,
            Upd_OprDates: Upd_OprDates,
            Upd_SLots: Upd_SLots,
            UTourType: UTourType,
            activityname: activityname,
            subtitle: subtitle,
            Country: Country,
            City: City,
            Location: Location,
            Description: Description,
            ChildrenAllowed: ChildrenAllowed,
            ChildAgeFrom: ChildAgeFrom,
            ChildAgeUpTo: ChildAgeUpTo,
            SmallChildAgeUpTo: SmallChildAgeUpTo,
            allowheight: allowheight,
            ChildMinHight: ChildMinHight,
            Infantallowed: Infantallowed,
            MaxNoofInfant: MaxNoofInfant,
            AllowMaxChild: AllowMaxChild,
            MaxChild: MaxChild,
            Attraction: Attraction,
            TourType: TourType,

            MinCapacity: MinCapacity,
            MaximumCapacity: MaximumCapacity,
            TourNote: TourNote,
            ActivityType: ActivityType,
            PriorityType: PriorityType,            //act type priority//
            OperationDays: OperationDays,

            OperationDaysTKT:OperationDaysTKT,
            OperationDaysSIC:OperationDaysSIC,
            OperationDaysPVT:OperationDaysPVT,

            datefrom: datefrom,
            dateTill: dateTill,
            FulYear: FulYear,
            Name: sname,
            SlotValue: SlotValue,
            //TourStart: tstart,
            TourStartH: tstartH,
            TourStartM: tstartM,
            //TourEnd: tend,
            TourEndH: tendH,
            TourEndM: tendM,
            PickUpFrom: pickfrom,
            //PickUpTime: picktime,
            PickUpTimeH: picktimeH,
            PickUpTimeM: picktimeM,
            DropOffAt: dropat,
            //DropOffTime: droptime,
            DropOffTimeH: droptimeH,
            DropOffTimeM: droptimeM,
            PriorityTyp: prtype,          // operatin time priority//
            AllowSlot: Slot

        }
        $.ajax({
            type: "POST",
            url: "ActivityHandller.asmx/UpdateActivity",
            data: JSON.stringify(Data),
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Activity Updates succesfully");


                    id = result.Sr_No;
                    sid = id;

                    redirtect = "Activityratelist.aspx?id=" + result.Sr_No + "&pCities=" + City + "&name=" + activityname + "&country=" + Country + "&location=" + Location;
                    $("#Button1").click();
                    //$("#Button1").bind("click",
                    //        { sid: "sid"},
                    //        function buttonClick(event) {
                    //            $("#displayArea").text(event.data.sid);
                    //        }
                    //        );


                    //window.location.href = "activitylist.aspx";
                    //window.location.href = "Activityratelist.aspx";
                    //GetPartnerProfile();
                    //$('#txtage').val('');
                    //$('#txtage1').val('');
                    //$('#ddlmaritalstatus').val('');
                    //$('#ddlReligion').val('');
                    //$('#ddlMotherTongue').val('');
                    //$('#ddlCommunity').val('');
                }
                else {
                    Success("Something Going Wrong");
                }
            },
        });
    }
}


