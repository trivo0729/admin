﻿




//var sndcity ="";

$(document).ready(function () {

    GetCountry();
    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);


    });

    $('#ddlcountry').change(function () {
        var sndcountry = $('#ddlcountry').val();
        GetCity(sndcountry);


    });
   

    $('#ddlcity').change(function ()
    {
        var sndcity = $('#ddlcity').val();
        for (var i = 0; i < sndcity.length; i++)
        {

            SetActCities(sndcity[i]);
        }
        
    });
   
    GetLocation();

});
//function getActCity()
//{
   
//}


function SetActCities(sndcity)
{

    GetActLocation(sndcity);

}

//$(function () {
//    GetCountry();
//});


var contryname;
var cityname;
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetCountry() {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    $("#ddlcountry").empty();
                   // $("#selCountryy").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                        //contryname = arrCountry[i].Countryname;
                    }
                    $("#selCountry").append(ddlRequest);
                    $("#ddlcountry").append(ddlRequest);

                   // $("#selCountryy").append(ddlRequest);
                    
                }
            }
        },
        error: function () {
        }
    });
}

function GetCity(reccountry) {
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCity1",
        data: '{"country":"' + reccountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.CityList;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    $("#ddlcity").empty();
                    
                    //var ddlRequest = '<option selected="" value="-">Select Any City</option>';
                    var ddlRequest = "";
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Description + '">' + arrCity[i].Description + '</option>';
                        //cityname = arrCity[i].Description;
                    }
                    $("#selCity").append(ddlRequest);

                    $("#ddlcity").append(ddlRequest);
                    
                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
        }
    });
}


function ValidateLocation() {

    var bValid = true;


    Location = $("#txt_Location").val();
    Country = $("#selCountry option:selected").val();
    City = $("#selCity option:selected").val();
    //Longitude = $("#txt_Longitude").val();
     //Latitude = $("#txt_Latitude").val();




    if (Location == '') {
        Success('Please enter Location');
        return false;
    }
    if (Country == '') {
        Success('Please select Country');
        return false;
    }
    if (City == '') {
        Success('Please select City');
        return false;
    }
    //if (Longitude == '') {
    //    Success('Please enter Longitude');
    //    return false;
    //}
    //if (Latitude == '') {
    //    Success('Please enter Latitude');
    //    return false;
    //}

    return bValid;
}




function SaveLocation() {




    bValid = ValidateLocation();

    if (bValid) {

        var Data = {


            Location: Location,
            Country: Country,
            City: City
            //Longitude: Longitude,
            //Latitude: Latitude
            //contryname:contryname,
            //cityname:cityname

        }
        $.ajax({
            type: "POST",
            url: "ActivityHandller.asmx/SaveLocation",
            data: JSON.stringify(Data),
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Location Inserted succesfully");

                    $("#txt_Location").val("");
                    $("#selCountry option:selected").val("");
                    $("#selCity option:selected").val("");
                    $("#modals").hide();
                    
                    //$("#txt_Longitude").val("");
                    //$("#txt_Latitude").val("");
                    //window.location.href = "AddLocation.aspx";
                    GetLocation();

                }
                else {
                    Success("This Location Already Exist");
                    GetLocation();
                }
            },
        });
    }
}




function GetLocation() {
    $("#tbl_GetLocation").dataTable().fnClearTable();
    $("#tbl_GetLocation").dataTable().fnDestroy();
    //$("#tbl_GetLocation tbody").remove();



    $.ajax({
        url: "ActivityHandller.asmx/GetLocation",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {

                var arrActLocation = result.ArrLocation;

                if (arrActLocation != 0) {
                    //$('#lblStatus').css("display", "none");

                    var tRow = '';

                    for (var i = 0; i < arrActLocation.length; i++) {

                        tRow += '<tr>';
                        tRow += '<td style="width:3%" class="align-center">' + (i + 1) + '</td>';
                        tRow += '<td style="width:11%" class="align-center">' + arrActLocation[i].LocationName + '</td>';
                        tRow += '<td style="width:11%" class="align-center">' + arrActLocation[i].City + '</td>';
                        tRow += '<td style="width:10%" class="align-center">' + arrActLocation[i].Country + '</td>';
                        //tRow += '    <td style="width:10%" align="center">' + arrActLocation[i].Longitude + '</td>';
                        //tRow += '    <td style="width:10%" align="center">' + arrActLocation[i].Latitude + '</td>';

                        tRow += '<td style="width:5%" class="align-center">';
                        tRow += '<a href="#" title="Edit" class="button" style="" onclick="GetDetails(\'' + arrActLocation[i].Lid + '\')"><span class="icon-pencil"></span></a>';
                        tRow += '<a style="display:none;" href="#" class="button" title="Delete" onclick="DeleteLocation(\'' + arrActLocation[i].Lid + '\',\'' + arrActLocation[i].LocationName + '\')"><span class="icon-trash"></span></a>';
                        tRow += '</td>';
                        //tRow += '       <td style="width:5%" align="center">';
                        //tRow += '            <a href="#" class="icon-trash icon-size2" title="Delete" onclick="DeleteLocation(\'' + arrActLocation[i].Lid + '\',\'' + arrActLocation[i].LocationName + '\')"></a>';
                     
                        //tRow += '    </td>';

                        tRow += '</tr>';

                    }
                    $("#tbl_GetLocation tbody").append(tRow);
                }
                
            }
            $("#tbl_GetLocation").dataTable(
            {
                bSort: false, sPaginationType: 'full_numbers',
            });
            $("#tbl_GetLocation").removeAttr("style");
        }
    })
}



function DeleteLocation(LocationId, LocationName) {
    if (confirm("Are you sure you want to delete " + LocationName + "?") == true) {
        $.ajax({
            url: "ActivityHandller.asmx/DeleteLocation",
            type: "post",
            data: '{"LocationId":"' + LocationId + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Location has been deleted successfully.");
                    window.location.href = "AddLocation.aspx";
                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }
}


var id;
function GetDetails(LocationID) {
    id = LocationID;
    $.ajax({
        url: "ActivityHandller.asmx/GetDetails",
        type: "post",
        data: '{"LocationID":"' + LocationID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var data = result.dttable;

                $("#txt_Location").val(data[0].LocationName);
                //$("#selCountry").val(data[0].Country);
                //$("#selCity").val(data[0].City);
                //$("#txt_Longitude").val(data[0].Longitude);
                //$("#txt_Latitude").val(data[0].Latitude);
                GetNationality(data[0].Country);
                GetCity(data[0].Country);
                setTimeout(function () {
                    AppendCity(data[0].City);
                }, 3000);
                $("#btn_Save").hide();
                $("#btn_Update").show();

            } else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function UpdateLocation() {
    var bValid = true;
    Location = $("#txt_Location").val();
    Country = $("#selCountry option:selected").val();
    City = $("#selCity option:selected").val();
    //Longitude = $("#txt_Longitude").val();
    //Latitude = $("#txt_Latitude").val();




    if (Location == '') {
        Success('Please enter Location');
        bValid = false;
    }
    if (Country == '') {
        Success('Please select Country');
        bValid = false;
    }
    if (City == '') {
        Success('Please select City');
        bValid = false;
    }
    //if (Longitude == '') {
    //    Success('Please enter Longitude');
    //    bValid = false;
    //}
    //if (Latitude == '') {
    //    Success('Please enter Latitude');
    //    bValid = false;
    //}

    if (bValid == true)
    {
        var Data = {

            id:id,
            Location: Location,
            Country: Country,
            City: City
            //Longitude: Longitude,
            //Latitude: Latitude
            //contryname:contryname,
            //cityname:cityname

        }

        $.ajax({
            type: "POST",
            url: "ActivityHandller.asmx/UpdateLocation",
            data: JSON.stringify(Data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Location Update Successfully")
                    $("#txt_Location").val("");
                    $("#selCountry option:selected").val("");
                    $("#selCity option:selected").val("");
                    //$("#txt_Longitude").val("");
                    //$("#txt_Latitude").val("");
                    $("#btn_Save").show();
                    $("#btn_Update").hide();
                    window.location.href = "AddLocation.aspx";

                    //GetLocation();
                }
            }
        })
    }

}



var arrActLocation;
function GetActLocation(sndcity) {
    //$("#tbl_GetLocation").dataTable().fnClearTable();
    //$("#tbl_GetLocation").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();
   


    $.ajax({
        url: "ActivityHandller.asmx/GetLocationByCity",
        type: "post",
        data: '{"city":"' + sndcity + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {

                arrActLocation = result.ArrLocation;
                var ddlRequest = "";
                var ddlRequestt = "";
                if (arrActLocation != 0) {
                  
                    // $("#ddlLocation").empty();
                    //  var ddlRequest = '<option selected="selected" value="-">Select Any Location</option>';
                    for (i = 0; i < arrActLocation.length; i++) {
                        ddlRequest += '<option value="' + arrActLocation[i].LocationName + '">' + arrActLocation[i].Lid + '</option>';
                        ddlRequestt += '<option value="' + arrActLocation[i].LocationName + '">' + arrActLocation[i].LocationName + '</option>';
                        
                        //if ($("#txt_location").val() == arrActLocation[i].Location_Name)
                        //{
                        //    $("#AddLocation").hide();
                        //}
                        //else
                        //{
                        //    $("#AddLocation").show();
                        //}
                        //contryname = arrCountry[i].Countryname;
                    }

                    $("#Select_Location").append(ddlRequest);
                    //$("#txt_PickUpFrom" + ploc + "").append(ddlRequestt);
                    //$("#txt_DropOffAt" + ploc + "").append(ddlRequestt);

                }
            }
        },
    });
}

//function AddLocation()
//{

//    if(arrActLocation=="")
//    {

//        $("#AddLocation").show();
        
//    }
//    else
//    {
//        $("#AddLocation").hide();
//    }
//}

function AddPopup()
{
    GetCountry();

    $.modal({
        title: 'Add Location',
        content:                  ' <div class="columns">'+
                '<div class="four-columns">' +
                    '<h6>Location Name:<h6>' +

       ' <input type="text" style="width: 150px" id="txt_Location" class="input full-width[required]" value="">' +
       '  </div>' +
       //'  &nbsp;&nbsp;&nbsp;' +
       '<div class="four-columns">' +
        '<h6>Country:<h6>' +
				'<select name="select90"  style="width: 150px" id="selCountry" class="select blue-gradient glossy mid-margin-left">' +
                    '<option selected="selected" value="-">Select Any Country</option>'+
               ' </select>' +
               '  </div>' +
                  //'  &nbsp;&nbsp;&nbsp;'+
                   '<div class="four-columns">' +
   ' <h6>City:<h6>' +
      '  <select name="select90" id="selCity"  style="width: 150px" class="select blue-gradient glossy mid-margin-right">' +
       '     <option selected="selected" value="-">Select Any City</option>'+
       ' </select>'+
      '  </div>'+


   //    ' <div class="new-row four-columns">' +
   //' Longitude:' +
   //             '    <input type="text" style="width: 150px"  id="txt_Longitude" class="input full-width[required]" value="">' +
   //             '  </div>' +
   //            // '  &nbsp;&nbsp;&nbsp;&nbsp' +
   //              '<div class="four-columns">' +
   //    'Latitude:' +
   //           '      <input type="text" style="width: 150px" id="txt_Latitude" class="input full-width[required]" value="">' +
   //           '  </div>' +
              '<div class="four-columns">' +
               
                '    <button type="button" style="margin-left: 10px;margin-top: 15px" class="button glossy blue-gradient" onclick="SaveLocation()">Save</button>' +
              '  </div>'+

         '   </div>',
    
    });
    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);

    });
};

function GetNationality(Countryname) {
    try {
        Countryname = Countryname.toUpperCase();
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#selCountry").val(Countryname);
        }
    }
    catch (ex)
    { }
}

function AppendCity(ut) {
    try {
        ut = ut.toUpperCase();
        var checkclass = document.getElementsByClassName('check');
        var CityCode = $.grep(arrCity, function (p) { return p.Description == ut; }).map(function (p) { return p.Description; });

        $("#City .select span")[0].textContent = ut;
        for (var i = 0; i <= CityCode.length; i++) {
            $('input[value="' + CityCode + '"][class="city"]').prop("selected", true);
            $("#selCity").val(CityCode);
        }
    }
    catch (ex)
    { }
}

