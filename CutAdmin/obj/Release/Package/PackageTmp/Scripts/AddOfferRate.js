﻿$(document).ready(function () {
    if (location.href.indexOf('?') != -1) {
        Sid = GetQueryStringParams('Id');
        OfferSid = GetQueryStringParams('Sid');
        HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
        GetAllOffers()
    }
    $('#htlname').val(HotelName);
});
//var Id;
var HotelName = "";
var Div = '';
var Arr = "";
var OfferDayList = "";
var Sid = 0;
var OfferOn = "Advance Booking";
var DaysPrior = "";
var FixedDate = "";
var OfferType = "Discount";
var DiscountPer = 0;
var DiscountAmount = 0;
var NewRate = 0;
var FreeItemName = "";
var FreeItemDetail = "";
var HotelOfferCode = "";
var HotelCode = "";
var OfferTerm = "";
var OfferNote = "";
var MealPlan = "";
var BookingType = "";
var OfferSid = "";
function GetAllOffers() {
    debugger
    $.ajax({
        type: "POST",
        url: "SearchUtilityHandler.asmx/GetAllOffers",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                //debugger
                Arr = obj.OfferList;
                OfferDayList = obj.OfferDayList;
                //if
                GetSeasonDetails()
                SeasonBind()
                if (OfferSid != undefined) {
                    $("#ddlSeason option").filter(function () {
                        return $(this).val() == OfferSid;
                    }).prop("selected", true);
                }
                DateDisAuto(3);
            }
        },
    });
}

function DateDisable(Id, chk) {
    var dateObject = $("#datepicker" + Id).datepicker('getDate', '+1d');
    endDate.setDate(dateObject.getDate() + 1);

    if (chk == 1) {
        $("#datepicker" + parseInt(parseInt(Id) + 1)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Id) + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: endDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Id) + 1), 2);
            }
        });
    }
}

function DateDisAuto(Id) {
    if (Id == 1) {
        $("#datepicker" + Id).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $("#datepicker" + parseInt(Id + 1)).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
    else {
        $("#datepicker" + Id).datepicker({
            dateFormat: 'dd-mm-yy',
           // minDate: endDate,
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $('#datepicker' + parseInt(Id + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            //minDate: endDate,
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
}

function AddUpdateOfferDetails() {
    debugger
    bValid = Validate();
    if (bValid) {
        Sid = $("#ddlSeason option:selected").val();
        var Data = { Sid: Sid, OfferOn: OfferOn, DaysPrior: DaysPrior, FixedDate: FixedDate, OfferType: OfferType, DiscountPer: DiscountPer, DiscountAmount: DiscountAmount, NewRate: NewRate, FreeItemName: FreeItemName, FreeItemDetail: FreeItemDetail, HotelOfferCode: HotelOfferCode, HotelCode: HotelCode, OfferTerm: OfferTerm, OfferNote: OfferNote, MealPlan: MealPlan, BookingType: BookingType };

        $.ajax({
            type: "POST",
            url: "SearchUtilityHandler.asmx/AddOfferDetails",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    Success("Offer Rate Added Successfully");
                    //$("#AddModal").modal("hide");
                    window.location.href = 'ViewOfferDetials.aspx?sHotelID=' + obj.Hotel_Id + '&HName=' + HotelName;
                }
                else {
                    Success("Error while updating offer");
                }
            },
        });
    }
}

function SeasonBind() {
    $("#ddlSeason option").filter(function () {
        return $(this).val() == 0;
    }).prop("selected", true);
    Div = '';

    var Season = $.grep(OfferDayList, function (H) { return H.Offer_Id == Sid && H.DateType == 'Season Date'; })
           .map(function (H) { return H; });
    $("#ddlSeason").empty();

    Div += '<option value="0">Select Season</option>'
    for (var i = 0; i < Season.length; i++) {

        Div += '<option value=' + Season[i].Sid + '>' + Season[i].SeasonName + ' [ ' + Season[i].ValidFrom + ' To ' + Season[i].ValidTo + ' ]</option>'
    }
    $("#ddlSeason").append(Div);
}

function GetSeasonDetails() {

    //Sid = $("#ddlSeason option:selected").val();
    ClearAll();
    var Data = $.grep(OfferDayList, function (H) { return H.Sid == OfferSid; }).map(function (H) { return H });
    if (Data.length != 0) {
        var t = Data[0].OfferOn;
        if (t == null) {

        }
        else {

            // Days Prior or Fixed Date
            setTimeout(function () {
                if (Data[0].DaysPrior != "") {
                    $("#rdb_DaysPrior").prop("checked", true);
                    $('#txtDaysPrior').val(Data[0].DaysPrior);
                    $("#DaysPrior").show();
                    $("#FixedDate").hide();
                }
                else {
                    $("#rdb_FixedDate").prop("checked", true);
                    $('#Fixeddatepicker3').val(Data[0].BookBefore);
                    
                    $("#DaysPrior").hide();
                    $("#FixedDate").show();
                }
            }, 100);
            // END Days Prior or Fixed Date

            $("#SelectMealPlan option").filter(function () {
                return $(this).text() == Data[0].MealPlan;
            }).prop("selected", true);

            // Offer Type
            $("#ddlOfferType option").filter(function () {
                return $(this).val() == Data[0].OfferType;
            }).prop("selected", true);

            if (Data[0].OfferType == "Discount") {
                if (Data[0].DiscountPer != 0) {
                    $('#rdb_DiscountPer').attr('checked', true)
                    $('#txtDiscountPer').val(Data[0].DiscountPer);
                    $("#DivDiscount").show();
                    $("#DivDiscountAmount").hide();
                }
                else {
                    $('#rdb_DiscountAmount').attr('checked', true)
                    $('#txt_DiscountAmount').val(Data[0].DiscountAmount);
                    $("#DivDiscount").hide();
                    $("#DivDiscountAmount").show();
                }
                $("#DivRdbDiscountPer").show();
                $("#DivRdbDiscountAmount").show();
                $("#DivItemName").hide();
                $("#DivItemDetails").hide();
                $("#DivNewRate").hide();
            }
            else if (Data[0].OfferType == "Deal") {
                $('#txtNewRate').val(Data[0].NewRate);
                $("#DivRdbDiscountPer").hide();
                $("#DivRdbDiscountAmount").hide();
                $("#DivDiscount").hide();
                $("#DivDiscountAmount").hide();
                $("#DivItemName").hide();
                $("#DivItemDetails").hide();
                $("#DivNewRate").show();
            }
            else if (Data[0].OfferType == "Freebi") {

                $('#txt_ItemName').val(Data[0].FreebiItem);
                $('#txt_ItemDetails').val(Data[0].FreebiItemDetail);
                $("#DivRdbDiscountPer").hide();
                $("#DivRdbDiscountAmount").hide();
                $("#DivDiscount").hide();
                $("#DivDiscountAmount").hide();
                $("#DivNewRate").hide();
                $("#DivItemName").show();
                $("#DivItemDetails").show();
            }
            //END Offer Type
            $("#txtHotelOfferCode").val(Data[0].HotelOfferCode);
            $("#txtHotelCode").val(Data[0].OfferCode);
            $("#txtOfferTerm").val(Data[0].OfferTerms);
            $("#txtOfferNote").val(Data[0].OfferNote);
            $("#txtBookingType").val(Data[0].BookingType);
        }
    }
}

function Validate() {
    bValid = true;

    HotelOfferCode = $("#txtHotelOfferCode").val();
    HotelCode = $("#txtHotelCode").val();
    OfferTerm = $("#txtOfferTerm").val();
    OfferNote = $("#txtOfferNote").val();
    BookingType = $("#txtBookingType").val();
    //Sid = $("#ddlSeason option:selected").val();
    if (Sid == 0) {
        $('#lbl_ddlSeason').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_ddlSeason').css("display", "none");
    }

    MealPlan = $('#SelectMealPlan option:selected').text();
    if (MealPlan == "Select Meal Plan") {
        $('#lbl_SelectMealPlan').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_SelectMealPlan').css("display", "none");
    }

    // Days Prior Fixed Date
    if ($('#rdb_DaysPrior').is(':checked')) {

        $('#lbl_Fixeddatepicker3').css("display", "none");
        DaysPrior = $("#txtDaysPrior").val();
        if (DaysPrior == "") {
            $('#lbl_txtDaysPrior').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txtDaysPrior').css("display", "none");
        }
    }
    else {
        FixedDate = $("#Fixeddatepicker3").val();
        $('#lbl_txtDaysPrior').css("display", "none");
        if (FixedDate == "") {
            $('#lbl_Fixeddatepicker3').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_Fixeddatepicker3').css("display", "none");
        }
    }
    // END Days Prior Fixed Date

    // Offer Type
    OfferType = $('#ddlOfferType option:selected').val();
    if (OfferType == "Discount") {
        if ($('#rdb_DiscountPer').is(':checked')) {
            DiscountAmount = 0;
            $('#lbl_txt_DiscountAmount').css("display", "none");
            DiscountPer = $("#txtDiscountPer").val();
            if (DiscountPer == "") {
                $('#lbl_txtDiscountPer').css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_txtDiscountPer').css("display", "none");
            }
        }
        else {
            DiscountPer = 0;
            $('#lbl_txtDiscountPer').css("display", "none");
            DiscountAmount = $("#txt_DiscountAmount").val();
            if (DiscountAmount == "") {
                $('#lbl_txt_DiscountAmount').css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_txt_DiscountAmount').css("display", "none");
            }
        }
    }
    else if (OfferType == "Deal") {

        NewRate = $("#txtNewRate").val();
        if (NewRate == "") {
            $('#lbl_txtNewRate').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txtNewRate').css("display", "none");
        }
    }
    else if (OfferType == "Freebi") {

        FreeItemName = $("#txt_ItemName").val();
        if (FreeItemName == "") {
            $('#lbl_txt_ItemName').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txt_ItemName').css("display", "none");
        }
        FreeItemDetail = $("#txt_ItemDetails").val();
        if (FreeItemDetail == "") {
            $('#lbl_txt_ItemDetails').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txt_ItemDetails').css("display", "none");
        }
    }
    // END Offer Type
    if (OfferTerm == "") {
        $('#lbl_txtOfferTerm').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_txtOfferTerm').css("display", "none");
    }
    return bValid;
}

function ClearAll() {

    $("#txtDaysPrior").val('');
    $("#txtDiscountPer").val('');
    $("#txt_DiscountAmount").val('');
    $("#txtNewRate").val('');
    $("#txt_ItemName").val('');
    $("#txt_ItemDetails").val('');
    $("#txtHotelOfferCode").val('');
    $("#txtHotelCode").val('');
    $("#txtOfferTerm").val('');
    $("#txtOfferNote").val('');
    $("#txtBookingType").val('');
}

function OfferTypeChange() {
    OfferType = $("#ddlOfferType option:selected").val();
    if (OfferType == "Freebi") {

        $("#DivItemName").show();
        $("#DivItemDetails").show();
        $("#DivRdbDiscountPer").hide();
        $("#DivRdbDiscountAmount").hide();
        $("#DivDiscount").hide();
        $("#DivDiscountAmount").hide();
        $("#DivNewRate").hide();
    }
    else if (OfferType == "Discount") {
        if ($('#rdb_DiscountPer').is(':checked')) {
            $("#DivDiscount").show();
            $("#DivDiscountAmount").hide();
        }
        else {
            $("#DivDiscount").hide();
            $("#DivDiscountAmount").show();
        }
        $("#DivRdbDiscountPer").show();
        $("#DivRdbDiscountAmount").show();
        $("#DivItemName").hide();
        $("#DivItemDetails").hide();
        $("#DivNewRate").hide();
    }
    else if (OfferType == "Deal") {
        $("#DivRdbDiscountPer").hide();
        $("#DivRdbDiscountAmount").hide();
        $("#DivDiscount").hide();
        $("#DivDiscountAmount").hide();
        $("#DivItemName").hide();
        $("#DivItemDetails").hide();
        $("#DivNewRate").show();
    }
}

function PriorFixed() {
    if ($('#rdb_DaysPrior').is(':checked')) {

        $("#DaysPrior").show();
        $("#FixedDate").hide();
    }
    else {
        $("#DaysPrior").hide();
        $("#FixedDate").show();
    }
}

function DiscountChange() {
    if ($('#rdb_DiscountPer').is(':checked')) {
        $("#DivDiscount").show();
        $("#DivDiscountAmount").hide();
    }
    else {
        $("#DivDiscount").hide();
        $("#DivDiscountAmount").show();
    }
}

function ChangeSeason() {

    Sid = $("#ddlSeason option:selected").val();
    ClearAll();
    var Data = $.grep(OfferDayList, function (H) { return H.Sid == Sid; }).map(function (H) { return H });
    var t = Data[0].OfferOn;
    if (t == null) {

    }
    else {

        // Days Prior or Fixed Date
        setTimeout(function () {
            if (Data[0].DaysPrior != "") {
                $("#rdb_DaysPrior").prop("checked", true);
                $('#txtDaysPrior').val(Data[0].DaysPrior);
                $("#DaysPrior").show();
                $("#FixedDate").hide();
            }
            else {
                $("#rdb_FixedDate").prop("checked", true);
                $('#Fixeddatepicker3').val(Data[0].BookBefore);
                $("#DaysPrior").hide();
                $("#FixedDate").show();
            }
        }, 100);
        // END Days Prior or Fixed Date

        $("#SelectMealPlan option").filter(function () {
            return $(this).text() == Data[0].MealPlan;
        }).prop("selected", true);

        // Offer Type
        $("#ddlOfferType option").filter(function () {
            return $(this).val() == Data[0].OfferType;
        }).prop("selected", true);

        if (Data[0].OfferType == "Discount") {
            if (Data[0].DiscountPer != 0) {
                $('#rdb_DiscountPer').attr('checked', true)
                $('#txtDiscountPer').val(Data[0].DiscountPer);
                $("#DivDiscount").show();
                $("#DivDiscountAmount").hide();
            }
            else {
                $('#rdb_DiscountAmount').attr('checked', true)
                $('#txt_DiscountAmount').val(Data[0].DiscountAmount);
                $("#DivDiscount").hide();
                $("#DivDiscountAmount").show();
            }
            $("#DivRdbDiscountPer").show();
            $("#DivRdbDiscountAmount").show();
            $("#DivItemName").hide();
            $("#DivItemDetails").hide();
            $("#DivNewRate").hide();
        }
        else if (Data[0].OfferType == "Deal") {
            $('#txtNewRate').val(Data[0].NewRate);
            $("#DivRdbDiscountPer").hide();
            $("#DivRdbDiscountAmount").hide();
            $("#DivDiscount").hide();
            $("#DivDiscountAmount").hide();
            $("#DivItemName").hide();
            $("#DivItemDetails").hide();
            $("#DivNewRate").show();
        }
        else if (Data[0].OfferType == "Freebi") {

            $('#txt_ItemName').val(Data[0].FreebiItem);
            $('#txt_ItemDetails').val(Data[0].FreebiItemDetail);
            $("#DivRdbDiscountPer").hide();
            $("#DivRdbDiscountAmount").hide();
            $("#DivDiscount").hide();
            $("#DivDiscountAmount").hide();
            $("#DivNewRate").hide();
            $("#DivItemName").show();
            $("#DivItemDetails").show();
        }
        //END Offer Type
        $("#txtHotelOfferCode").val(Data[0].HotelOfferCode);
        $("#txtHotelCode").val(Data[0].OfferCode);
        $("#txtOfferTerm").val(Data[0].OfferTerms);
        $("#txtOfferNote").val(Data[0].OfferNote);
        $("#txtBookingType").val(Data[0].BookingType);
    }
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}