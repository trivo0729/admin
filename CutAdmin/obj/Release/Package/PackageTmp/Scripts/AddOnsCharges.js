﻿var arrAddOns = new Array();
var arrAddOnsTaxces = new Array();
var arrMeals = new Array();

function addonsAdd(ID, el, RateID) {
    try {
        var id = ID + RateID;
        var html = '';

       

        html += '<div class="new-row columns TaxDetails' + RateID + '  divRow_' + id + '" id="divRow_' + id + '">'
        html += '<div class="three-columns DivAddons">'
        html += '<input type="hidden" class="addonID' + ID + '" value="' + RateID + '"/>'
        html += '<select id="sel_Addons' + id + '" name="validation-select" class=" full-width sel_Addons">'
        html += '</select>'
        html += '</div>'
        html += ''
       
        html += '<div class="three-columns">'
        html += '<input class="input full-width txt_AddOnsRate" id="txtTaxDetails' + id + '" type="number" min="0" value="0.00">'
        html += '</div>'
       

        html += '<div class="three-columns">'
        html += '<select id="" name="validation-select" class="select full-width sel_Type">'
        html += '<option value="PN" selected="selected">Per Night</option>'
       // html += '<option value="PR" selected="selected">Per Room</option>'
        html += '    </select>'
        html += '</div>';

      
        html += '<div class="one-columns" id="Action' + id + '">'
        html += '<input type="checkbox" id="chk' + ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" > '
        html += '<a id="lnk_' + id + '" onclick="addonsAdd(\'' + (parseInt(ID) + 1) + '\',\'' + el + '\',\'' + RateID + '\');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        html += ' <a id="lnkR_' + id + '" onclick="addonsDelete(' + parseInt(ID) + ',\'' + id + '\',\'' + RateID + '\');" class="button"><span class="icon-minus-round red"></span></a>'
        html += '<br>'
        html += '</div>'
        html += '</div>'
        $("#lnk_" + parseInt(ID - 1) + RateID + "").remove()
        //$("div_Add_" + parseInt(ID)).append()
        $("#" + el).append(html);
        GenrateAddOnsType('sel_Addons' + id)
        AddCount++;
        //if (RoomID != 0) {
        //    GenrateAddOns();
        //}
    }
    catch (ex) {
        alert(ex.message)
    }
}

function addonsDelete(ID, ROW, RateID) {
    $("#divRow_" + ROW).remove();
    if ($(".TaxDetails" + RateID).length == 1) {
        var html = '';
        var id = parseInt(ID - 1);
        $("#Action" + id + RateID).empty();
        html += '<a id="lnk_' + parseInt(id) + RateID + '" onclick="AddTax(' + parseInt(ID) + ');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        html += ' <a id="lnkR_' + parseInt(id) + RateID + '" onclick="DeleteTax(' + parseInt(id) + ',this);" class="button"><span class="icon-minus-round red"></span></a>'
        html += '<br>'
        $("#Action" + id + RateID).append(html);
    }
    AddCount--;
}

var RateID = 0;
function GetAddDetails(ID, RoomID, el) {
    debugger
    var HotelID = GetQueryStringParams("sHotelID");
    var ArrRate = GetQueryStringParams("ArrRateID");
    if (ArrRate != undefined) {
        var ArrRate = GetQueryStringParams("ArrRateID").split(",");
        var RateID = ArrRate[ID];
    }
    else {
        var ArrRate = GetQueryStringParams("RateID");
        if (ArrRate != undefined)
            RateID = ArrRate
        else
            RateID = 0;
    }
    $.ajax({
        url: "../handler/TaxHandler.asmx/GetAddOnsMapping",
        type: "post",
        data: '{"HotelID":"' + HotelID + '","RoomId":"' + RateID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrAddOns = result.arrAddOns
                arrTax = result.arrTax
                arrAddOnsTaxces = result.arrAddOnsRate;
                addonsAdd(ID, el, RoomID)
                arrMeals = result.arrMeals;
                GenrateExtraMeal(RoomID)

            }
            else {
                alert(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}

function GenrateAddOnsType(ID) {
    var Opt = '';
    Opt += '<option value="0" selected=selected style="color:red">Non Select</option>'
    for (var i = 0; i < arrAddOns.length; i++) {
        Opt += '<option value="' + arrAddOns[i].ID + '">' + arrAddOns[i].AddOnName + '</option>'
    }
    $('#' + ID).append(Opt)
    $('#' + ID).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: true,
        single: true,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        keepOpen: false,
    })
    return Opt;
}

function GenrateAddOnsRates(RowID, RateID, RowDivID) {
    var arrAddOnsRates = new Array();
    /*Meal Rates*/
    var arrMeal = $(".MealRate" + RowDivID );
    for (var i = 0; i < arrMeal.length; i++) {
        var Meals = $($(arrMeal[i]).find(".txt_AddOns")[0]).text()
        var ID = $.grep(arrMeals, function (p) { return p.AddOnName == Meals; })
                       .map(function (p) { return p.ID; })[0];
        var IsCumpulsury = false;
        if ($(arrMeal[i]).find(".checked").length != 0)
            IsCumpulsury = true;
        if (ID != undefined) {
            arrAddOnsRates.push({
                AddOnsID: ID,
                IsCumpulsury: IsCumpulsury,
                HotelID: GetQueryStringParams("sHotelID"),
                RateID: RateID,
                RateType: $(arrMeal[i]).find(".sel_Type")[0].outerText,
                Rate: $(arrMeal[i]).find(".txtMeal").val(),
                CWBRate: $(arrMeal[i]).find(".txtCWB").val(),
                CNBRate: $(arrMeal[i]).find(".txtCNB").val(),
            });
        }
    }
    var arrTaxDetails = $(".TaxDetails" + RowDivID);
    for (var i = 0; i < arrTaxDetails.length; i++) {
        var Addons = $($(arrTaxDetails[i]).find(".sel_Addons")[0]).val();
        var IsCumpulsury= false;
        if ($(arrTaxDetails[i]).find(".checked").length != 0)
            IsCumpulsury = true;
        
        if (Addons != null && Addons != 0) {
            arrAddOnsRates.push({
                AddOnsID: Addons,
                IsCumpulsury: IsCumpulsury,
                HotelID: GetQueryStringParams("sHotelID"),
                RateID: RateID,
                RateType: $($(arrTaxDetails[i]).find(".select-value")[0]).text(),
                Rate: $(arrTaxDetails[i]).find(".txt_AddOnsRate").val(),
            });
        }
    }
    return arrAddOnsRates;
}

function OpenAddOnsPopUp(RateTypeID, HotelID, Date, RoomNo, MealID, TypeID) {
    $.ajax({
        type: "POST",
        url: "../handler/TaxHandler.asmx/GetAddOnsByRoom",
        data: JSON.stringify({ HotelID: HotelID, RateTypeID: RateTypeID }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List = result.arrAddOnsRate;
                //if (List.length != 0)
                //{
                var html = '';


                html += '<div class="twelve-columns">'
                html += ' <div class="table-wrap"> <table class="table HotelList"  style="font-size: small;" ><br/>'
                html += '<thead>';
                html += '<tr>';
                html += '<th style="height:30px;"></th>';
                html += '<th style="height:30px;">Name</th>';
                html += '<th style="height:30px;">Type</th>';
                html += '<th style="height:30px;">Rate</th>';
                html += '<th style="height:30px;">Quantity</th>';

                html += '<tr>';
                html += '</thead>';
                html += '<tbody>'
                for (var i = 0; i < List.length; i++) {
                    html += '<tr class="AddOns">';
                    html += '<td style="background-color:white;height:32px;"><input type="checkbox" name="chk_Addons_' + i + '" id="chk_Addons_' + i + '" value="' + List[i].RateName + '" class="chk_Addons"></td>';
                    html += '<td style="background-color:white;height:32px;">' + List[i].RateName + '</td>';
                    html += '<td style="background-color:white;height:32px;" class="Type_Addons">' + List[i].Type + '</td>';
                    html += '<td style="background-color:white;height:32px;" class="Total_Addons">' + List[i].TotalRate + '</td>';

                    if (List[i].Type == "Per Person") {
                        html += '<td style="background-color:white;height:32px;"><input type="number" class="input Qty_Addons" placeholder="Quantity" style="width:75%"  min="1" value="1" id="txtQuantity_' + i + '"></td>';
                    }
                    else {
                        html += '<td style="background-color:white;height:32px;">-</td>';
                    }


                    html += '</tr>';
                }

                html += '</tbody>'
                html += '</table>';

                html += '<br>'
                html += '<br>'
                html += '<input type="button" class="button blue-gradient" style="float:right" onclick="Save(\'' + Date + '\',\'' + RoomNo + '\',\'' + TypeID + '\',\'' + MealID + '\')" value="Save" />'

                html += '</div>'
                html += ''
                html += ''
                html += ''

                $.modal({
                    content: html,
                    title: 'Addons',
                    width: 350,
                    height: 300,
                    scrolling: true,
                    actions: {
                        'No': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },

                    // buttonsLowPadding: true
                });
                // }
                //else
                //{
                //    alert("No Addons on this room");
                //}


            }
        }
    });

}

var arrAddOns = new Array();
function Save(Date, RoomNo, RateTypeID, MealID) {
    var Date = Date + "_" + RoomNo;
    var sAddOns = $(".AddOns");

    arrAddOns = arrAddOns.filter(function (obj) {
        return obj.Date != Date;
    });

    for (var i = 0; i < sAddOns.length; i++) {
        var chk_Addons = $(sAddOns[i]).find(".chk_Addons")[0]
        if (chk_Addons.checked) {
            var Qty = 1;
            if ($(sAddOns[i]).find(".Qty_Addons").length != 0)
                Qty = $(sAddOns[i]).find(".Qty_Addons")[0].value
            arrAddOns.push({
                Date: Date,
                Name: chk_Addons.value,
                Type: $(sAddOns[i]).find(".Type_Addons")[0].textContent,
                TotalRate: $(sAddOns[i]).find(".Total_Addons")[0].textContent,
                Quantity: Qty,
            });

        }

    }

    $("#AddonsList" + "_" + RoomNo).empty();

    var html = '';
    var TotalAddonsRate = 0;

    html += '<div class="columns">'
    html += '<details class="details" open>'
    html += '<summary>AddOns</summary>'
    html += ' <div> <table class="table responsive-table HotelList" id="tbl_AddonList' + '_' + RoomNo + '" style="font-size: small;">'
    html += '<thead>';
    html += '<tr>';
    html += '<th scope="col">Dates</th>';
    html += '<th scope="col" class="align-center">Name</th>';
    html += '<th scope="col" class="align-center">Type</th>';
    html += '<th scope="col" class="align-center">Quantity</th>';
    html += '<th scope="col" class="align-center">Charge</th>';
    html += '<tr>';
    html += '</thead>';
    html += '<tbody>'
    for (var i = 0; i < arrAddOns.length; i++) {
        if (arrAddOns[i].Date.split('_')[1] == RoomNo) {
            html += '<tr>';
            html += '<td class="align-center">' + arrAddOns[i].Date.split('_')[0] + '</td>';
            html += '<td class="align-center">' + arrAddOns[i].Name + '</td>';
            html += '<td class="align-center">' + arrAddOns[i].Type + '</td>';
            html += '<td class="align-center">' + arrAddOns[i].Quantity + '</td>';
            html += '<td class="align-center">' + arrAddOns[i].TotalRate + '</td>';
            html += '</tr>';
            TotalAddonsRate += parseFloat(parseFloat(arrAddOns[i].TotalRate) * parseFloat(arrAddOns[i].Quantity));
        }

    }
    $("#txt_AddOns" + RateTypeID + '_' + MealID + '_' + RoomNo).val(numberWithCommas(TotalAddonsRate));
    $("#txt_Total" + RateTypeID + '_' + MealID + '_' + RoomNo).val(numberWithCommas(parseFloat($("#txtHiddenTotal" + RateTypeID + '_' + MealID + '_' + RoomNo).val()) + parseFloat(TotalAddonsRate)));
    html += '</tbody>'
    html += '</table></div>	</details>';

    $("#tbl_AddonList" + "_" + RoomNo).append(html);
    $("#AddonsList" + "_" + RoomNo).append(html);
    // Success(TotalAddonsRate);

}

function GenrateAddOns() {
    for (var i = 0; i < arrAddOnsPrice.length; i++) {
        if (arrAddOnsPrice.IsMeal == false)
            addonsAdd(ID, el, RoomID);
        else {
            GenrateExtraMeal(RoomID)
        }

    }
}

function GenrateExtraMeal(Type) {
    var trRoomsRV = '';
    var Meal = $("#sel_MealPlan").val();
    try
    {
        for (var i = 0; i < arrMeals.length; i++) {
            var icon = '';
            trRoomsRV += '<div  class="columns MealRate' + Type + '">';
            trRoomsRV += '<div class="two-columns"> <a href="javascript:void(0)" class="button full-width txt_AddOns">'
            if (arrMeals[i].MealType == null)
                arrMeals[i].MealType = "";
            var COMPULSORY = "";
            var ListMP = arrMeals[i].MealType.split(',');

            var sMeal = $.grep(ListMP, function (p) { return p == Meal; })
                       .map(function (p) { return p });

            if (sMeal.length != 0) {
              
                COMPULSORY = "checked disabled"
            }
            //else
            //    trRoomsRV += '<span class="button-icon red-gradient"><span class="icon-cross  txt_AddOns"></span>'
            trRoomsRV +=   arrMeals[i].AddOnName + '</a></div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtMeal">'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtCWB">'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtCNB">'
            trRoomsRV += '</div>';


            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<select  name="validation-select" class="select full-width sel_Type">'
            trRoomsRV += '<option value="PN" selected="selected">Per Person</option>'
            //trRoomsRV += '<option value="PR">Per Room</option>'
            trRoomsRV += '    </select>'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns"><input type="checkbox" id="chk' + arrMeals[i].ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" ' + COMPULSORY + '></div>'
            trRoomsRV += ' </div>';

        }
        $(".AddMeal" + Type).append(trRoomsRV);
    }
    catch (ex)
    {
        Success(ex)
    }
   
}

