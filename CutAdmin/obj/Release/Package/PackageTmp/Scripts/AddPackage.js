﻿

//$(document).ready(function () {
//    $('#txt_Itenary').ckeditor();
//    $('#lst_Pricing').on("click", function () { UpdatePricingDetails(); });
//});

$(function () {
    //$(document).ready(function () {
    //    $('#txt_Desctiption').ckeditor();
    //    $('#txt_TermsCondition').ckeditor();
    //});
    $(function () {
        $('#select_Category, #select_Themes').change(function () {
            console.log($(this).val());
        }).multipleSelect({
            width: '100%'
        });
    });

    var tpj = jQuery;
    var chkinDate;
    var chkinMonth;
    var chkoutDate;
    var chkoutMonth;
    tpj(document).ready(function () {
        debugger;
        tpj("#txt_City").autocomplete({
            source: function (request, response) {
                tpj.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "../Handler/AddPackageHandler.asmx/GetDestinationCode",
                    data: "{'name':'" + document.getElementById('txt_City').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        response(result);
                    },
                    error: function (result) {
                        alert("No Match");
                    }
                });
            },
            minLength: 4,
            select: function (event, ui) {
                debugger;
                tpj('#hdnDCode').val(ui.item.id);
                var h1 = ui.item.id;
            }
        });
    });


    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#datepicker2").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "d-m-yy"
    });
    
});

function UpdatePricingDetails() {
    Success("Please first fill basic detail");
}
function setInvetoryFixed() {
    if ($("#radio_InventoryFixed").is(":checked")) {
        $("#Divtxt_Inventory").css("display", "block");
    }
    else {
        $("#Divtxt_Inventory").css("display", "none");
    }
}
var shdnDCode;
function AddPackageBasicDetails() {
    debugger;
    var bValid = true;
    $("#tbl_AddPackage tbody tr td label").css("display", "none");
    $("#select_PackageDescription").css("display", "none");

    var sPackageName = $("#txt_PackageName").val();
    var sCity = $("#txt_City").val() + " |" + $("#hdnDCode").val();
    //var HotelCode = $("#hdnDCode").val();
    var sCategory = $("#select_Category").val();
    var nDuration = $("#txt_Duration").val();
    var dvalidFrom = $("#datepicker").val(); //$(this).attr('rel');
    var dvalidupto = $("#datepicker2").val();
    var sThemes = $("#select_Themes").val();
    var sDesctiption = $("#txt_Desctiption").val();
    //var sDesctiption = $("#txt_Desctiption").Editor("getText");
    var sTax = $("#txt_Tax").val();
    var sCancelDays = $("#txt_cancelDays").val();
    var sCancelCharge = $("#txt_CancelCharge").val();
    var sTermsCondition = $("#txt_TermsCondition").val();
    //var sTermsCondition = $("#txt_TermsCondition").Editor("getText")
    shdnDCode = $("#hdnDCode").val();
    var sInventory = 0;
    if ($("#radio_InventoryRequest").is(":checked")) {
        sInventory = 0;
    }
    else if ($("#radio_InventoryFixed").is(":checked")) {
        sInventory = $("#txt_Inventory").val();
    }


    if (sPackageName == "") {
        bValid = false;
        Success("Please Enter Package Name");
        
    }
    if (sCity == "") {
        bValid = false;
        Success("Please Enter City");
       
    }
    if (sCategory == "0") {
        bValid = false;
        Success("Please Enter Last Name");
      
    }
    if (nDuration == "0") {
        bValid = false;
        Success("Please select Duration");
        
    }
    if (dvalidFrom == "") {
        bValid = false;
        Success("Please select Valid From");
       
    }
    if (dvalidupto == "") {
        bValid = false;
        Success("Please select Valid To");
        
    }
    if (process(dvalidupto) < process(dvalidFrom)) {
        //Success(dvalidupto + 'is later than ' + dvalidFrom);
        //$("#lblerr_sPackageValidupto").html("* Validate Date should be After the Validity Start Date")
        //$("#lblerr_sPackageValidupto").css("display", "");
        //$("#datepicker2").focus();
        //bValid = false;
        bValid = false;
        Success("Validate Date should be After the Validity Start Dat");
        return;
    }
    if (sThemes == "0") {
        bValid = false;
        Success("Please select Theme");
       
    }
    if (sDesctiption == "") {
        bValid = false;
        Success("Please enter Description");
       
    }
    if (sTax == "") {
        bValid = false;
        Success("Please enter Tax");
        
    }
    if (sCancelDays == "") {
        bValid = false;
        Success("Please enter Cancel Days");
       
    }
    if (sCancelCharge == "") {
        bValid = false;
        Success("Please enter Cancel Charge");
       
    }
    if (sTermsCondition == "") {
        bValid = false;
        Success("Please write Terms&Condition");
       
    }
   
    if (bValid == true) {
        
        $.ajax({
            type: "POST",
            url: "../Handler/AddPackageHandler.asmx/AddPackageBasicDetails",
            data: '{"sPackageName":"' + sPackageName + '","sCity":"' + sCity + '","sCategory":"' + sCategory + '","nDuration":"' + nDuration + '","dvalidFrom":"' + dvalidFrom + '","dvalidupto":"' + dvalidupto + '","sThemes":"' + sThemes + '","sDesctiption":"' + sDesctiption + '","dTax":"' + sTax + '","nCancelDays":"' + sCancelDays + '","dCancelCharge":"' + sCancelCharge + '","sTermsCondition":"' + sTermsCondition + '","sInventory":"' + sInventory + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Login.aspx";
                    // window.location.href = "Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    Success("Basic details added successfully, Please add Pricing details.");
                    $("#hdpackageId").val(result.nID);
                    //  $(location).attr("target", "_blank");
                    //    $(location).attr('href', '../Admin/PricingDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + shdnDCode);
                    AddPricingDetails(result.nID);
                    //window.location.href = "ViewPackage.aspx";
                    return false;
                }
                else {

                }
            },
            error: function () {
                Success("errors")
            }

        });
    }
}
function process(date) {
    debugger;
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function AddPricingDetails(nID) {
    //  $(location).attr("target", "_blank");
    $(location).attr('href', '../AddPackage.aspx?' + btoa('nID=' + nID) + '&hcd=' + shdnDCode);
}


function CheckValidationForTabs(id) {
    debugger
    if (id == "BasicInformation") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
    }
    else if (id == "Pricing") {
        var bValid = true;
        var sPackageName = $("#txt_PackageName").val();
        var sCity = $("#txt_City").val() + " |" + $("#hdnDCode").val();
       
        var sCategory = $("#select_Category").val();
        var nDuration = $("#txt_Duration").val();
        var dvalidFrom = $("#datepicker").val(); //$(this).attr('rel');
        var dvalidupto = $("#datepicker2").val();
        var sThemes = $("#select_Themes").val();
        var sDesctiption = $("#txt_Desctiption").val();
        var sTax = $("#txt_Tax").val();
        var sCancelDays = $("#txt_cancelDays").val();
        var sCancelCharge = $("#txt_CancelCharge").val();
        var sTermsCondition = $("#txt_TermsCondition").val();
        shdnDCode = $("#hdnDCode").val();
        var sInventory = 0;
        if ($("#radio_InventoryRequest").is(":checked")) {
            sInventory = 0;
        }
        else if ($("#radio_InventoryFixed").is(":checked")) {
            sInventory = $("#txt_Inventory").val();
        }


        if (sPackageName == "") {
            bValid = false;
        
        }
        if (sCity == "") {
            bValid = false;
       
        }
        if (sCategory == "0") {
            bValid = false;
      
        }
        if (nDuration == "0") {
            bValid = false;
        
        }
        if (dvalidFrom == "") {
            bValid = false;
       
        }
        if (dvalidupto == "") {
            bValid = false;
        
        }
        if (process(dvalidupto) < process(dvalidFrom)) {
            
            bValid = false;
        }
        if (sThemes == "0") {
            bValid = false;
        }
        if (sDesctiption == "") {
            bValid = false;
        }
        if (sTax == "") {
            bValid = false;
        }
        if (sCancelDays == "") {
            bValid = false;
        }
        if (sCancelCharge == "") {
            bValid = false;
        }
        if (sTermsCondition == "") {
            bValid = false;
        }
       
        if (bValid == true) {
            $('#BasicInformation').hide();
            $('#Pricing').show();
            $('#Itinerary').hide();
            $('#HotelDetails').hide();
            $('#PackageImages').hide();
        }
        else {
            Success("Please first fill basic detail");
            $('#BasicInformation').show();
            $('#Pricing').hide();
            $('#Itinerary').hide();
            $('#HotelDetails').hide();
            $('#PackageImages').hide();
        }

    }
    else if (id == "Itinerary") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
    }
    else if (id == "HotelDetails") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
    }
    else if (id == "PackageImages") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
    }

}