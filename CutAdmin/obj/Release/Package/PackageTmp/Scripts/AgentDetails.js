﻿
var arrayToSubmit = new Array();

$(document).ready(function () {
    GetAgentDetail();

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            Success("An error occured while loading countries")
        }
    });

});


function GetCountryCity() {
    var recCountry = $('#selCountry').val();
    $.ajax({
        type: "POST",
        //url: "../DefaultHandler.asmx/GetCity",
        url: "GenralHandler.asmx/GetCountryCity",
        data: '{"country":"' + recCountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.CityList;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);
                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
            Success("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}


var hiddensid;
var arrFranchisee;
var UpdateUrl = "";
var List_AgentDetails = new Array();
function GetAgentDetail() {
    $("#tbl_AgentDetails").dataTable().fnClearTable();
    $("#tbl_AgentDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "Handler/UserHanler.asmx/GetAgentDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_AgentDetails tbody").remove();
                var tRow = '<tbody>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_AgentDetails").append(tRow);

                $("#tbl_AgentDetails").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}

function UpdateMarkupModal(sid, agentCategory, UserType) {
    debugger;
    //if (agentCategory == "") {
    $("#txt_Markup").val('');
    $("#hdn_Usertype").val(UserType);
    $("#lbl_ErrMarkupDefault").css("display", "");
    //$("#GroupModal").modal('show');
    $("#MarkupModal").modal('show');
    EditMarkupModal(sid);
}

function UpdateRoles(sid) {
    debugger;
    $("#hdnDCode").val(sid);
    //$("#RolesModal").modal('show');
    // AppendFormTable();
    var Name = $.grep(List_AgentDetails, function (p) { return p.sid == sid; })
                .map(function (p) { return p.AgencyName; });
    window.open("ChannelManager.aspx?uid=" + sid + "&Name=" + Name)
    // EditMarkupModal(sid);



}

var bvalid;
var sCountry;
var sCity;
var IAgencyName; var IAgentuniquecode; var IGSTNumber;
var IdtLastAccess, IContactPerson, IsDesignation, IAddress, IDescription, ICountryname, IPinCode, Iphone, IMobile, IFax, Iemail, IWebsite, IPANNo, ICurrencyCode;



function Validate_City() {

    debugger;
    bvalid = true;
    var reg = new RegExp('[0-9]$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    if (sCountry == "-") {
        bvalid = false;
        //$("#lbl_selCountry").css("display", "");

        Success("Please select Country!");
        return bvalid;
    }
    //if (sCity == "-") {
    //    bvalid = false;
    //    //$("#lbl_selCity").css("display", "");
    //    alert("Please select City!");

    //}

    return bvalid;

}



function AgentDetailsModal(sid) {
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/UserHanler.asmx/GetAgencyDetails",
        data: '{"AgentID":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            console.log(result);
            if (result.retCode == 1) {
                var arrAgency = result.arrAgency;
                if (arrAgency.GSTNumber == null) {
                    arrAgency.GSTNumber = "";
                }
                $("#hiddenID").val(sid)
                arrAgency.dtLastAccess = moment(arrAgency.dtLastAccess).format("L LTS");
                $.modal({
                    content: '<div class="respTable">' +
                      '<table class="table responsive-table evenodd" id="sorting-advanced">' +
                      '<tr><td class="even">Agency Name:</td><td>' + arrAgency.AgencyName + '</td><td class="even">Registration Date:</td><td>' + arrAgency.dtLastAccess + '</td></tr>' +
                      '<tr><td class="even">Contact Person:</td><td>' + arrAgency.ContactPerson + '</td><td class="even">Designation:</td><td>' + arrAgency.Designation + '</td></tr>' +
                      '<tr><td class="even">Phone:</td><td>' + arrAgency.phone + '</td><td class="even">Mobile:</td><td>' + arrAgency.Mobile + '</td></tr></tr>' +
                      '<tr><td class="even">Address:</td><td>' + arrAgency.Address + '</td><td class="even">City:</td><td>' + arrAgency.Description + '</td></tr></tr>' +
                      '<tr><td class="even">Country:</td><td>' + arrAgency.Country + '</td><td class="even">Pin Code:</td><td>' + arrAgency.PinCode + '</td></tr></tr>' +
                      '<tr><td class="even">Fax:</td><td>' + arrAgency.Fax + '</td><td class="even">Email:</td><td>' + arrAgency.email + '</td></tr></tr>' +
                      '<tr><td class="even">Website:</td><td>' + arrAgency.Website + '</td><td class="even">Pan No:</td><td>' + arrAgency.PANNo + '</td></tr></tr>' +
                      '<tr><td class="even">Currency:</td><td>' + arrAgency.CurrencyCode + '</td><td class="even">Agent Code:</td><td>' + arrAgency.Agentuniquecode + '</td></tr> </tr>' +
                      //'<tr><td class="even">Agent Code:</td><td>' + arrAgency.Agentuniquecode + '</td><td class="even"></td><td></td>' +
                      '</tr>' +
                      '</table>' +
                      '</div>' +
                    '<p class="text-alignright"><a style="cursor:pointer" href="AddAgent.aspx?sid=' + sid + '&sUniqueCode=' + arrAgency.Agentuniquecode + '&bLoginFlag=' + arrAgency.LoginFlag + '&City=' + arrAgency.Description + '&CurrencyCode=' + arrAgency.CurrencyCode + '&sContactPerson=' + arrAgency.ContactPerson + '&sDesignation=' + arrAgency.Designation + '&sAgencyName=' + arrAgency.AgencyName + '&sEmail=' + arrAgency.email + '&sAddress=' + arrAgency.Address + '&sCountry=' + arrAgency.Country + '&sUsertype=' + arrAgency.UserType + '&nPinCode=' + arrAgency.PinCode + '&IATANumber=' + arrAgency.IATANumber + '&nPhone=' + arrAgency.phone + '&nMobile=' + arrAgency.Mobile + '&nFax=' + arrAgency.Fax + '&nPAN=' + arrAgency.PANNo + '&sWebsite=' + arrAgency.Website + '&GSTNumber=' + arrAgency.GSTNumber + '&sGroup=' + arrAgency.agentCategory + '" class="button compact anthracite-gradient icon-gear editpro">Edit Profile</a></p>',
                    title: 'Agent Profile',
                    width: 800,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
    });
}

function AgentDetailsModalForTransfer(AgencyName, dtLastAccess, ContactPerson, sDesignation, Address, Description, Countryname, PinCode, phone, Mobile, Fax, email, Website, PANNo, Agentuniquecode, GSTNumber, CurrencyCode) {

    IAgencyName = AgencyName;
    IAgentuniquecode = Agentuniquecode;
    if (GSTNumber == "null") {
        IGSTNumber = "";
    }
    else {
        IGSTNumber = GSTNumber;
    }


    IdtLastAccess = dtLastAccess;
    IContactPerson = ContactPerson;
    IsDesignation = sDesignation;
    IAddress = Address;
    IDescription = Description;
    ICountryname = Countryname;
    IPinCode = PinCode;
    Iphone = phone;
    IMobile = Mobile;
    IFax = Fax;
    Iemail = email;
    IWebsite = Website;
    IPANNo = PANNo;
    ICurrencyCode = CurrencyCode;

    $("#spn_ICompanyName").html(AgencyName);
    $("#spn_IValidity").html(dtLastAccess);
    $("#spn_IContactPerson").html(ContactPerson);
    $("#spn_IDesignation").html(sDesignation);
    $("#spn_IAddress").html(Address);
    $("#spn_ICity").html(Description);
    $("#spn_ICountry").html(Countryname);
    $("#spn_IPinCode").html(PinCode);
    $("#spn_IPhone").html(phone);
    $("#spn_IMobile").html(Mobile);
    $("#spn_IFax").html(Fax);
    $("#spn_IEmail").html(email);
    $("#spn_IWebsite").html(Website);
    $("#spn_IPanNo").html(PANNo);
    $("#spn_ICode").html(Agentuniquecode);
    $("#spn_IGST").html(IGSTNumber);
    $("#spn_ICurrency").html(ICurrencyCode);
}
var MyMail = "";
//function PasswordModal(sid, uid, AgencyName, password) {
//    $('#lbl_ErrPassword').css("display", "none");
//    hiddensid = sid;
//    MyMail = uid;
//    $("#txt_AgentId").val(uid);
//    $("#txt_AgencyName").val(AgencyName);
//    GetPassword(password);
//    //$("#txt_Password").val(password);
//}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $('#lbl_ErrPassword').css("display", "none");
            $.modal({
                content: '<div class="modal-body">' +
                  '<div class="scrollingDiv">' +
                  '<div class="columns">' +
                  '<div class="twelve-columns bold">Are you sure you want to change password?</div>' +
                  '</div>' +
                  '<div class="columns">' +
                  '<div class="nine-columns bold">&nbsp;</div>' +
                  '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePass()">Ok</button>' +
                  '</div></div></div>',
                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
            //Ok("Are you sure you want to change password?", "ChangePass", null)
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.alert('No Change found in Password!', {
            buttons: {
                'Cancel': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            }
        });
    }

}
function ChangePass() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to change password?</p>',
            function () {
                $.ajax({
                    type: "POST",
                    url: "HotelAdmin/Handler/GenralHandler.asmx/AgentChangePassword",
                    data: '{"sid":"' + hiddensid + '","password":"' + $('#txt_Password').val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.Session != 1) {
                            Success("Session Expired");
                        }
                        if (result.retCode == 1) {
                            Success("Password changed successfully");
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);

                        }
                        if (result.retCode == 0) {
                            Success("Something went wrong while processing your request! Please try again.");
                        }
                    },
                });
            },
            function () {
                $('#modals').remove();
            }
           );
        }
        else if ($('#txt_Password').val() == "") {
            Success("Password can not be blank");
        }
    }
    else {
        Success("No Change found in Password!");
    }

}
function SendPasswordModal(uid, password) {
    $('#lbl_ErrEmailId').css("display", "none");
    $('#lbl_ErrEmailId').html("* This field is required");
    $('#lbl_ErrsendPassword').css("display", "none");
    $("#txt_EmailId").val(uid);
    hiddensid = uid;
    GetPassword(password);
}

function SendPassword() {
    $('#lbl_ErrEmailId').css("display", "none");
    $('#lbl_ErrEmailId').html("* This field is required");
    $('#lbl_ErrsendPassword').css("display", "none");
    var emailReg = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    var bvalid = true;
    var email = $("#txt_AgentId").val();
    var password = $("#txt_SendPassword").val();
    if (email == "") {
        $('#lbl_ErrEmailId').css("display", "");
        bvalid = false;
    }
    else if (email != "" && !(emailReg.test(email))) {
        $('#lbl_ErrEmailId').css("display", "");
        $('#lbl_ErrEmailId').html("Invalid Email");
        bvalid = false;
    }
    if (bvalid == true) {
        $.modal({
            content: '<div class="modal-body">' +
              '<div class="scrollingDiv">' +
              '<div class="columns">' +
              '<div class="twelve-columns bold">Are you sure you want to send password?</div>' +
              '</div>' +
              '<div class="columns">' +
              '<div class="nine-columns bold">&nbsp;</div>' +
              '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="Send(\'' + email + '\')">Ok</button>' +
              '</div></div></div>',
            width: 300,
            scrolling: false,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient glossy displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
        //Ok("Are you sure you want to send password?", "Send", [email])
    }
}
function Send(email) {
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/MailPassword",
        data: '{"sEmail":"' + MyMail + '","sTo":"' + email + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                Success("Password Send to mail.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while sending password.");
        }
    });
    //Cancel()
}
function GetPassword(sid, uid, AgencyName, password) {
    $("#txt_Password").val('');
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/GetPassword",
        data: '{"password":"' + password + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                pass = result.password;
                $.modal({
                    content: '<div class="modal-body">' +
                      '<div class="scrollingDiv">' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">Agency Name</div>' +
                      '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="Agency-value" value="' + AgencyName + '" class="input-unstyled full-width" readonly="readonly" type="text"></div></div></div> ' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">User ID</div>' +
                      '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="' + uid + '" class="input-unstyled full-width" readonly="readonly" type="text"></div></div></div> ' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">Password</div>' +
                      '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + pass + '" class="input-unstyled full-width" type="text"></div></div> ' +
                      '</div>' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">&nbsp;</div>' +
                      '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="SendPassword()">Email</button>&nbsp;' +
                      '<button type="button" class="button anthracite-gradient" onclick="ChangePass()">Change&nbsp;Password</button></div>' +
                      '</div></div></div>',

                    title: 'Edit Password',
                    width: 500,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient glossy displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
                //$("#txt_Password").val(pass);
                $("#hddn_Password").val(pass);
                //$("#txt_SendPassword").val(pass);
            }
        },
        error: function () {
            Success("An error occured while loading password.")
        }
    });
}

function hidealert() {
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
}

function DeleteAgentID(uid, agencyname) {
    debugger;
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
    $("#spnMsg").html("Agent status has been changed successfully!");
    debugger;
    Ok("Are you sure you want to delete  <span class=\"orange\"> " + agencyname + "</span>?", "Delete", [uid])
}
function Delete(uid) {
    $.ajax({
        url: "GenralHandler.asmx/DeleteAgentID",
        type: "post",
        data: '{"uid":"' + uid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Deleted successfully!");
            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function Activate(sid, flag, agencyname, agentCategory, UserType, AgentUniqueCode) {
    var status = flag.replace("True", "0").replace("False", "1");
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + flag.replace("True", "Deactivate").replace("False", "Activate") + "<span class=\"orange\"> " + agencyname + '?</p>',
        function () {

            //    $.modal({
            //        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + flag.replace("True", "Deactivate").replace("False", "Activate") + "<span class=\"orange\"> " + agencyname + '</span>?</p>' +
            //'<div style="float:right"><button type="button" class="button anthracite-gradient" onclick="Active(\'' + sid + '\' , \'' + status + '\')">OK</button></div>',
            //        width: 500,
            //        scrolling: false,
            //        actions: {
            //            'Close': {
            //                color: 'red',
            //                click: function (win) { win.closeModal(); }
            //            }
            //        },
            //        buttons: {

            //        },
            //        buttonsLowPadding: false
            //    });

            $.ajax({
                url: "HotelAdmin/Handler/GenralHandler.asmx/ActivateLogin",
                type: "post",
                data: '{"sid":"' + sid + '","status":"' + status + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Status has been changed successfully!")
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);

                    }
                    else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);

                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);

                }
            });
        },
        function () {
            $('#modals').remove();
        }
    );
}

function Active(sid, status) {
    $.ajax({
        url: "HotelAdmin/Handler/GenralHandler.asmx/ActivateLogin",
        type: "post",
        data: '{"sid":"' + sid + '","status":"' + status + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Status has been changed successfully!")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
            setTimeout(function () {
                window.location.reload();
            }, 2000);

        }
    });
}
function AddGroup() {
    $("#lbl_ErrMarkupgroup").css("display", "none");
    var UserType = $("#hdn_Usertype").val();
    var sid = $("#hdn_sid").val();
    var GroupId = $("#selGroup option:selected").val();
    var GroupName = $("#selGroup option:selected").text();
    var flag = $("#hdn_flag").val();
    var AgentUniqueCode = $("#hdn_AgentUniqueCode").val();
    var agencyname = $("#hdn_agencyName").val();
    if (GroupId != "-") {
        $.ajax({
            url: "GenralHandler.asmx/AddGroup",
            type: "post",
            data: '{"sid":"' + sid + '","UserType":"' + UserType + '","GroupId":"' + GroupId + '","GroupName":"' + GroupName + '","AgentUniqueCode":"' + AgentUniqueCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#GroupModal").modal('hide');
                    Activate(sid, flag, agencyname, GroupName, UserType, AgentUniqueCode);

                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }
    else {
        $("#lbl_ErrMarkupgroup").css("display", "");
    }
}

function ExportAgentDetailsToExcel(Type) {
    debugger;
    window.location.href = "../HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=AgentDetails&Type=" + Type;
}

function Search() {
    $("#tbl_AgentDetails").dataTable().fnClearTable();
    $("#tbl_AgentDetails").dataTable().fnDestroy();
    var Name = $("#txt_AgencyList").val();
    var Type = $("#AgentType").val();
    var Code = $("#AgentCode").val();
    var Group = $("#selGroup option:selected").text();
    var Status = $("#selStatus").val();
    var Country = $("#selCountry option:selected").text();
    var City = $("#selCity option:selected").text();
    var MinBalance = $("#MinBalance").val();

    var data = {
        Name: Name,
        Type: Type,
        Code: Code,
        Group: Group,
        Status: Status,
        Country: Country,
        City: City,
        MinBalance: MinBalance
    }

    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/UserHanler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_AgentDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_AgentDetails").append(tRow);

                $("#tbl_AgentDetails").dataTable({
                    bSort: true, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.")
        }
    });

}

function htmlGenrator() {
    var tRow = "";
    var input = "\/Date(1458845940000)\/";
    for (var i = 0; i < List_AgentDetails.length; i++) {

        var CurrencyClass = "";
        if (List_AgentDetails[i].CurrencyCode == "AED") {
            CurrencyClass = "Currency-AED"
        }
        else if (List_AgentDetails[i].CurrencyCode == "SAR") {
            CurrencyClass = "Currency-SAR"
        }
        else if (List_AgentDetails[i].CurrencyCode == "EUR") {
            CurrencyClass = "fa fa-gbp"
        }
        else if (List_AgentDetails[i].CurrencyCode == "USD") {
            CurrencyClass = "fa fa-dollar"
        }
        else if (List_AgentDetails[i].CurrencyCode == "INR") {
            CurrencyClass = "fa fa-inr"
        }
        moment.locale('India Standard Time');
        List_AgentDetails[i].dtLastAccess = moment(List_AgentDetails[i].dtLastAccess).format("L LTS");
        tRow += '<tr>'
        tRow += '<td style="width:5%">' + (i + 1) + '</td>';
        tRow += '<td style="width:25%"><a style="cursor:pointer" data-toggle="modal" data-target="#AgencyDetailModal" onclick="AgentDetailsModal(\'' + List_AgentDetails[i].sid + '\'); return false" title="Click to view Agency Details">' + List_AgentDetails[i].AgencyName.toUpperCase() + '</a></td>';
        tRow += '<td style="width:20%"><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].uid + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].password + '\'); return false" title="Click to edit Password">' + List_AgentDetails[i].uid + ' </a></td>';
        tRow += '<td style="width:10%">' + List_AgentDetails[i].Agentuniquecode + '</td>';
        tRow += '<td style="width:10%"><i class="' + CurrencyClass + '"></i> ' + List_AgentDetails[i].AvailableCredit + '</td>';

        //if (List_AgentDetails[i].LoginFlag == "True")
        //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">No</label></span></td>';
        //else
        //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">No</label></span></td>';


        if (List_AgentDetails[i].LoginFlag == 'True') {
            tRow += '<td class="align-center"><input type="checkbox" id="chk_Off' + List_AgentDetails[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" checked onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')"></td>';
        }
        else {
            tRow += '<td class="align-center"><input type="checkbox" id="chk_Off' + List_AgentDetails[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')"></td>';
        }





        tRow += '<td tyle="width:14%" class="align-center"><span class="button-group">'
        tRow += '<a href="UpdateCredit.aspx?UniqueCode=' + List_AgentDetails[i].Agentuniquecode + '&sid=' + List_AgentDetails[i].sid + '&Name=' + List_AgentDetails[i].AgencyName + '" class="button" title="Update Credit" ><span class="fa fa-credit-card"></span></a>'
        tRow += '<a href="#" class="button" title="Update Markup Group" onclick="UpdateGroupMarkupModal(\'' + List_AgentDetails[i].sid + '\')"><span class="icon-user"></span></a>'
        //tRow += '<a href="#" onclick="UpdateCommissionModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')" class="button" title="Commission"><span class="icon-pencil"></span></a>'
        //tRow += '<a href="#" class="button" title="Billing Cycle" onclick="SetCommisionCycle(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')"><span class="icon-cycle"></span></a>'
        tRow += '<a href="#" onclick="UpdateIndiviualMarkupModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')" class="button" title="Individual Markup"><span class="icon-tag"></span></a>'
        tRow += '</span></td>'
        tRow += '</tr>';

    }
    $("#tbl_AgentDetails tbody").append(tRow);

    $(".tiny").click(function () {
        $(this).find("input:checkbox").click();
    })

    $("#tbl_AgentDetails").dataTable({
        bSort: true, sPaginationType: 'full_numbers',
    });
    $('#tbl_AgentDetails').removeAttr("style");
}

function SetCommisionCycle(Id, Category, UserType) {
    $.modal({
        content: '<div class="modal-body">' +
'<div class="scrollingDiv">' +
'<div class="columns">' +
'<div class="four-columns bold">Billing Cycle</div>' +
'<div class="eight-columns"><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Billing" value="" class="input-unstyled full-width" type="text"></div></div></div> ' +
'</div>' +
'<p class="text-alignright"><a style="cursor:pointer" href="#" class="button compact anthracite-gradient editpro" onclick="AddBillingCycle(\'' + Id + '\',\'' + Category + '\',\'' + UserType + '\')">Save</a></p>' +
'</div>',

        title: 'Add Billing Cycle',
        width: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function AddBillingCycle(Id, Category, UserType) {
    var Billing = $("#txt_Billing").val();
    var data = {
        Billing: Billing,
        Id: Id,
        Category: Category,
        UserType: UserType
    }
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/AddBillingCycle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Billing Cycle Added Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
            else {
                Success(" Error");
            }
        },

    });
}

function PasswordModal(sid, uid, AgencyName, password) {
    $('#lbl_ErrPassword').css("display", "none");
    hiddensid = sid;
    MyMail = uid;
    GetPassword(sid, uid, AgencyName, password);
};


function GetFormList(sid) {
    HiddenId = sid;
    $("#tblStaffForms").empty();
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/RoleManagementHandler.asmx/GetFormList?sid=" + sid,
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.Arr;
                if (arFormList.length > 0) {
                    var trForms = '<tbody>';
                    for (i = 0; i < arFormList.length; i = i + 4) {
                        if (i < arFormList.length) {
                            trForms += '<tr>';
                            trForms += '<td><input id="chk' + arFormList[i].sFormName + '" class="checkbox mid-margin-left chkFrm" type="checkbox" value="' + arFormList[i].nId + '"/> ' + arFormList[i].sDisplayName + '</td>';
                            if ((i + 1) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 1].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 1].nId + '"/> ' + arFormList[i + 1].sDisplayName + '</td>';
                            if ((i + 2) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 2].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 2].nId + '"/> ' + arFormList[i + 2].sDisplayName + '</td>';
                            if ((i + 3) < arFormList.length)
                                trForms += '<td><input id="chk' + arFormList[i + 3].sFormName + '" type="checkbox" class="checkbox mid-margin-left chkFrm" value="' + arFormList[i + 3].nId + '"/> ' + arFormList[i + 3].sDisplayName + '</td>';
                            trForms += '</tr>';
                        }
                    }
                    trForms += '</tbody>';
                    //$("#tblStaffForms").append(trForms);
                    //$('input[type=checkbox]').attr("disabled", true);
                    $.modal({
                        content: '<table class="table table-striped table-bordered" id="tblStaffForms" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                     '</table><br>' +
                     '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',

                        title: 'Assign Roles',
                        width: 600,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                arFormListForRole = result.arrAuthForm;
                CheckRoles();

            }
        },
        error: function () {
            Success("An error occured while geting form list");
        }
    });
}
function CheckRoles() {
    debugger;
    if (arFormListForRole.length > 0) {
        for (j = 0; j < arFormList.length; j++) {
            for (k = 0; k < arFormListForRole.length; k++) {
                if (arFormList[j].sFormName == arFormListForRole[k].sFormName) {
                    $('#chk' + arFormList[j].sFormName).click();
                }
            }
        }
    }
}

function SubmitFormsForRole() {
    var j = 0;
    var arrFrm = $(".checked")
    for (var i = 0; i < arrFrm.length; i++) {
        arrayToSubmit[j] = arrFrm[i].childNodes[1].value;
        j++;
    }
    var arrayJson = JSON.stringify(arrayToSubmit);
    if (HiddenId != 'null') {
        $.ajax({
            type: "POST",
            url: "HotelAdmin/Handler/RoleManagementHandler.asmx/SetFormsForAdminStaff?sid=" + HiddenId,
            data: '{"StaffUid":"' + HiddenId + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
                else if (result.retCode == 0) {
                    Success("Something goes wrong!");
                }
            },
            error: function () {
                Success("error occured while submitting checked forms");
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}

function UpdateGroupMarkupModal(sid) {
    HiddenId = sid;
    GetGroup(sid)
}

function GetGroup(sid) {
    $("#tblGroup").empty();
    $.ajax({
        url: "../handler/MarkUpHandler.asmx/GetGroup",
        type: "post",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                $("#btnUpdate").val("Update Markup")
                var trGroups = '<tbody>';
                aaGroupMarkupDetails = result.Arr
                for (i = 0; i < aaGroupMarkupDetails.length; i = i + 3) {
                    if (i < aaGroupMarkupDetails.length) {
                        trGroups += '<tr class="Grp">';
                        trGroups += '<td><input id="chk' + aaGroupMarkupDetails[i].sid + '" type="radio" class="radio mid-margin-left chkGrp" value="' + aaGroupMarkupDetails[i].sid + '" name="Group" /> ' + aaGroupMarkupDetails[i].GroupName + '</td>';
                        if ((i + 1) < aaGroupMarkupDetails.length)
                            trGroups += '<td><input id="chk' + aaGroupMarkupDetails[i + 1].sid + '" type="radio" class="radio mid-margin-left chkGrp" value="' + aaGroupMarkupDetails[i + 1].sid + '" name="Group"/> ' + aaGroupMarkupDetails[i + 1].GroupName + '</td>';
                        if ((i + 2) < aaGroupMarkupDetails.length)
                            trGroups += '<td><input id="chk' + aaGroupMarkupDetails[i + 2].sid + '" type="radio" class="radio mid-margin-left chkGrp" value="' + aaGroupMarkupDetails[i + 2].sid + '" name="Group"/> ' + aaGroupMarkupDetails[i + 2].GroupName + '</td>';
                        trGroups += '</tr>';
                    }
                }
                trGroups += '</tbody>';
                $.modal({
                    content: '<table class="table table-striped table-bordered" id="tblGroup" cellspacing="0" cellpadding="0" border="0">' + trGroups + '' +
                 '</table><br>' +
                 '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitAgentGroup(\'' + sid + '\')">Update Markup</button>',

                    title: 'Update Markup Group',
                    width: 600,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
                GetAgentGroup(sid)
            }
            else {
                Success('No Markup Guroup Available');
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}

function GetAgentGroup(HiddenId) {
    debugger;
    $.ajax({
        type: "POST",
        url: "handler/MarkUpHandler.asmx/GetAgentGroup",
        data: '{"AgentId":"' + HiddenId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrGroup = result.Arr;
                if (arrGroup.length > 0) {
                    $('#chk' + arrGroup[0].GroupId).click();
                }
            }
        },
        error: function () {
            Success("An error occured while geting checked form list");
        }
    });
}

function SubmitAgentGroup(HiddenId) {
    var GroupId = $('input[name="Group"]:checked').val();
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/AgentGroupAssign",
        data: '{"AgentId":"' + HiddenId + '","GroupId":"' + GroupId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Groups has been changed successfully!");
                setTimeout(function () {
                    window.location.reload();
                }, 500)
            }
            else if (result.retCode == 0) {
                Success("Something goes wrong!");
            }
        },
        error: function () {
            Success("error occured while submitting checked forms");
        }
    });
}