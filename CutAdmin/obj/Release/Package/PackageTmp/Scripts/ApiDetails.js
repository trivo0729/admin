﻿$(function () {
    //sid = GetQueryStringParams('ID');
    //if (sid != undefined) {
    //    GetData();
    //    $("#btn_Supplier").val('Update');
    //}
    //else {
    //    $("#btn_Supplier").val("ADD");
    //    GetApiList();
    //}
   
    if (location.href.indexOf('?') != -1) {
        sid = GetQueryStringParams('sid');
        GetData();
    }
    else {
        $("#btn_Supplier").text("ADD");
        GetApiList();
    }
});
var sid = 0;
var arrAPI = "";
function GetApiList() {
    $("#tbl_MasterSupplier").dataTable().fnClearTable();
    $("#tbl_MasterSupplier").dataTable().fnDestroy();
    //$("#tbl_MasterSupplier tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "../Handler/ApiHandller.asmx/GetAPI",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAPI = result.tbl_API;
                var tr = '';
                for (var i = 0; i < arrAPI.length; i++) {

                    tr += '<tr>';
                    tr += '<td>' + (i + 1) + '</td>';
                    tr += '<td>' + arrAPI[i].Supplier + '</td>';
                    if (arrAPI[i].Sup_Mobile != null) {
                        tr += '<td>' + arrAPI[i].Sup_Mobile + '</td>';
                    } else {
                        tr += '<td></td>';
                    }
                    if (arrAPI[i].Sup_Phone != null) {
                        tr += '<td>' + arrAPI[i].Sup_Phone + '</td>';
                    } else {
                        tr += '<td></td>';
                    }
                   
                    if (arrAPI[i].Hotel != true) {
                        if (arrAPI[i].Hotel != null) {
                            tr += '<td><i class="fa fa-times" aria-hidden="true"></i></td>';
                        } else {
                            tr += '<td></td>';
                        }
                    } else {
                       
                        tr += '<td><i class="fa fa-check" aria-hidden="true"></i></td>';
                    }
                    if (arrAPI[i].Active != true) {
                        if (arrAPI[i].Active != null) {
                            tr += '<td><i class="fa fa-times" aria-hidden="true"></i></td>';
                        } else {
                            tr += '<td></td>';
                        }
                    } else {

                        tr += '<td><i class="fa fa-check" aria-hidden="true"></i></td>';
                    }
                   
                    if (arrAPI[i].Flight != "True")
                        tr += '<td class="align-center"><a onclick="window.location.href=\'AddHotelSupplier.aspx?sid=' + arrAPI[i].sid + '\'" class="button compact"><span class="icon-pencil"></span></a></td>';
                    else
                        tr += '<td class="align-center"></td>';
                    tr += '</tr>';
                   
                    //tr += '<tr><td style="width: 5%">' + (i + 1) + '</td>';
                    //tr += "<td>" + arrAPI[i].Supplier + "</td>";
                    //tr += "<td>" + arrAPI[i].Sup_Mobile + "</td>";
                    //tr += "<td>" + arrAPI[i].Sup_Phone + "</td>";
                    //tr += "<td>" + arrAPI[i].Hotel.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    
                    //tr += "<td>" + arrAPI[i].Active.replace("True", '<span class="glyphicon glyphicon-ok" title="Activated"  aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-ban-circle" title="Deactivated" aria-hidden="true"></span>') + "</td>";
                  
                    //if (arrAPI[i].Flight != "True")
                    //    tr += '<td><a style="cursor:pointer" onclick="window.location.href=\'AddHotelSupplier.aspx?sid=' + arrAPI[i].sid + '\'"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a></td></tr>';
                    //else
                    //    tr += '<td></td></tr>';

                   
                }
                $("#tbl_MasterSupplier tbody").append(tr);
                $("#tbl_MasterSupplier").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_MasterSupplier").removeAttribute("style")
            }
        },
        error: function () {
            alert('An error occured while Getting Supplier Details')
        }
    });
}

var Hotel, Visa, Otb, Pakages, Supplier, sActive, Flight, HiddenId, HotelComm = 0, HotelTDS = 0, OtbComm = 0, OtbTDS, PakagesComm = 0, PackagesTDS = 0, Address;

var sCompany, sCity, sCoutry, sState, sZip, sMobile, sPhone, sEmail, sFax, oName, oEmail, oMobile, rName, rEmail, rMobile, rPhone, aName, aEmail, aMobile, aPhone;

//var VisaComm = 0, VisaTDS = 0;
var bValid = true;
function AddSupplier() {
    bValid = true;
    Supplier = $("#txt_Supplier").val()
    sMobile = $("#txt_Sup_Mobile").val()
    sPhone = $("#txt_Sup_Phone").val()
    Address = $("#txt_Address").val()
    sEmail = $("#txt_Sup_Email").val()
    if ($("#txt_HotelComm1").val() != "")
        HotelComm = $("#txt_HotelComm1").val()
    if ($("#txt_HotelTDS1").val() != "")
        HotelTDS = $("#txt_HotelTDS1").val()

    Supplier = $("#txt_Supplier").val()
    if (Supplier == "") {
        Success("Please Insert Supplier Name")
        bValid = false;
        $("#txt_Supplier").focus()
    }
    rEmail = $("#txt_Sup_ResendEmail").val()
    if (rEmail == "") {
        Success("Please Insert Supplier Reservation Email")
        bValid = false;
        $("#txt_Sup_ResendEmail").focus()
    }
    if (chkHotel.checked == true)
        Hotel = "True"
    else
        Hotel = "False"
    Visa = "False"
  
    Otb = "False"
    //if (chkPackages.checked == true)
    //    Pakages = "True"
    //else
    //    Pakages = "False"

    if (chkFlight.checked == true)
        Flight = "True"
    else
        Flight = "False"

    if (chkActive.checked == true)
        sActive = "True"
    else
        sActive = "False"
    if (bValid) {
        sCompany = $("#txt_Sup_Company").val()
        sCity = $("#txt_Sup_City").val()
        sCoutry = $("#txt_Sup_Country").val()
        sState = $("#txt_Sup_State").val()
        sZip = $("#txt_Sup_Zip").val()

        sFax = $("#txt_Sup_Fax").val()
        oName = $("#txt_Sup_OwnerName").val()
        oEmail = $("#txt_Sup_OwnerEmail").val()
        oMobile = $("#txt_Sup_OwnerMobile").val()
        rName = $("#txt_Sup_ResendName").val()

        rMobile = $("#txt_Resend_Mobile").val()
        rPhone = $("#txt_Resend_Phone").val()
        aName = $("#txt_Sup_Acc_Name").val()
        aEmail = $("#txt_Sup_Acc_Email").val()
        aMobile = $("#txt_Sup_Acc_Mobile").val()
        aPhone = $("#txt_Sup_Acc_Phone").val()

        var Data = {
            Supplier: Supplier,
            Hotel: Hotel,
            Visa: Visa,
            Otb: Otb,
            Flight: Flight,
            //Pakages: Pakages,
            Active: sActive,
            HotelComm: HotelComm,
            HotelTDS: HotelTDS,
            //VisaComm: 0,
            //VisaTDS: 0,
            //OtbComm: OtbComm,
            //OtbTDS: OtbTDS,
            //PackagesComm: PakagesComm,
            //PackagesTDS: PackagesTDS,
            sMobile: sMobile,
            sPhone: sPhone,
            Address: Address,
            sCompany: sCompany,
            sCity: sCity,
            sCoutry: sCoutry,
            sState: sState,
            sZip: sZip,
            sEmail: sEmail,
            sFax: sFax,
            oName: oName,
            oEmail: oEmail,
            oMobile: oMobile,
            rName: rName,
            rEmail: rEmail,
            rMobile: rMobile,
            rPhone: rPhone,
            aName: aName,
            aEmail: aEmail,
            aMobile: aMobile,
            aPhone: aPhone
            //Supplier: Supplier,
            //Hotel: Hotel,
            //Visa: Visa,
            //Otb: Otb,
            //Flight: Flight,
            ////Pakages: Pakages,
            //Active: sActive,
            //HotelComm: HotelComm,
            //HotelTDS: HotelTDS,
            ////VisaComm: 0,
            ////VisaTDS: 0,
            ////OtbComm: OtbComm,
            ////OtbTDS: OtbTDS,
            ////PackagesComm: PakagesComm,
            ////PackagesTDS: PackagesTDS,
            //sMobile: sMobile,
            //sPhone: sPhone,
            //Address: Address,
            //sCompany: sCompany,
            //sCity: sCity,
            //sCoutry: sCoutry,
            //sState: sState,
            //sZip: sZip,
            //sEmail: sEmail,
            //sFax: sFax,
            //oName: oName,
            //oEmail: oEmail,
            //oMobile: oMobile,
            //rName: rName,
            //rEmail: rEmail,
            //rMobile: rMobile,
            //rPhone: rPhone,
            //aName: aName,
            //aEmail: aEmail,
            //aMobile: aMobile,
            //aPhone: aPhone
        }
        var json = JSON.stringify(Data)
        $.ajax({
            type: "POST",
            url: "../Handler/ApiHandller.asmx/AddSupplier",
            data: json,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Supplier Details Added")
                    setTimeout(function () { window.location.href = "Suppliers.aspx" }, 2000)
                    
                }
            },
            error: function () {
                Success("An error occured while Adding Supplier Details")
                //alert('An error occured while Adding Supplier Details')
            }
        });
    }
}

function UpdateSupplier() {
    bValid = true;
    HotelComm = 0, HotelTDS = 0, PakagesComm = 0, PackagesTDS = 0;
    Supplier = $("#txt_Supplier").val()
    sMobile = $("#txt_Sup_Mobile").val()
    sPhone = $("#txt_Sup_Phone").val()
    Address = $("#txt_Address").val()
    sEmail = $("#txt_Sup_Email").val()
    sState = $("#txt_Sup_State").val()
    if ($("#txt_HotelComm1").val() != "")
        HotelComm = $("#txt_HotelComm1").val()
    if ($("#txt_HotelTDS1").val() != "")
        HotelTDS = $("#txt_HotelTDS1").val()

    if (Supplier == "") {
        Success("Please Insert Supplier Name")
        bValid = false;
        $("#txt_Supplier").focus()
    }
    
    rEmail = $("#txt_Sup_ResendEmail").val()
    if (rEmail == "") {
        Success("Please Insert Supplier Reservation Email")
        bValid = false;
        $("#txt_Sup_ResendEmail").focus()
    }
   
    //if ($("#txt_VisaComm1").val() != "")
    //    VisaComm = $("#txt_VisaComm1").val()
    //if ($("#txt_VisaTDS1").val() != "")
    //    VisaTDS = $("#txt_VisaTDS1").val()

    //if ($("#txt_OtbComm1").val() != "")
    //    OtbComm = $("#txt_OtbComm1").val()
    //if ($("#txt_OtbTDS1").val() != "")
    //    OtbTDS = $("#txt_OtbTDS1").val()

    //if ($("#txt_PackageComm1").val() != "")
    //    PakagesComm = $("#txt_PackageComm1").val()
    //if ($("#txt_PackageTDS1").val() != "")
    //    PackagesTDS = $("#txt_PackageTDS1").val()
    if (chkHotel.checked == true)
        Hotel = "True"
    else
        Hotel = "False"
    //if (chkVisa1.checked == true)
    //    Visa = "True"
    //else
    Visa = "False"
    //if (chkOtb1.checked == true)
    //    Otb = "True"
    //else
    Otb = "False"
    //if (chkPackages.checked == true)
    //    Pakages = "True"
    //else
    //    Pakages = "False"
    if (chkFlight.checked == true)
        Flight = "True"
    else
        Flight = "False"

    if (chkActive.checked == true)
        sActive = "True"
    else
        sActive = "False"
    if (bValid) {
        sCompany = $("#txt_Sup_Company").val()
        sCity = $("#txt_Sup_City").val()
        sCoutry = $("#txt_Sup_Country").val()
        sState = $("#txt_Sup_State").val()
        sZip = $("#txt_Sup_Zip").val()
        sFax = $("#txt_Sup_Fax").val()

        oName = $("#txt_Sup_OwnerName").val()
        oEmail = $("#txt_Sup_OwnerEmail").val()
        oMobile = $("#txt_Sup_OwnerMobile").val()


        rName = $("#txt_Sup_ResendName").val()
        rEmail = $("#txt_Sup_ResendEmail").val()
        rPhone = $("#txt_Resend_Phone").val()
        rMobile = $("#txt_Resend_Mobile").val()
       
        aName = $("#txt_Sup_Acc_Name").val()
        aEmail = $("#txt_Sup_Acc_Email").val()
        aMobile = $("#txt_Sup_Acc_Mobile").val()
        aPhone = $("#txt_Sup_Acc_Phone").val()

        var Data = {
            sid: sid,
            Supplier: Supplier,
            Hotel: Hotel,
            Visa: Visa,
            Otb: Otb,
            Flight: Flight,
            //Pakages: Pakages,
            Active: sActive,
            HotelComm: HotelComm,
            HotelTDS: HotelTDS,
            //VisaComm: 0,
            //VisaTDS: 0,
            //OtbComm: OtbComm,
            //OtbTDS: OtbTDS,
            //PackagesComm: PakagesComm,
            //PackagesTDS: PackagesTDS,
            sMobile: sMobile,
            sPhone: sPhone,
            Address: Address,
            sCompany: sCompany,
            sCity: sCity,
            sCoutry: sCoutry,
            sState: sState,
            sZip: sZip,
            sEmail: sEmail,
            sFax: sFax,
            oName: oName,
            oEmail: oEmail,
            oMobile: oMobile,
            rName: rName,
            rEmail: rEmail,
            rMobile: rMobile,
            rPhone: rPhone,
            aName: aName,
            aEmail: aEmail,
            aMobile: aMobile,
            aPhone: aPhone
        }
        var json = JSON.stringify(Data)
        $.ajax({
            type: "POST",
            url: "../Handler/ApiHandller.asmx/UpdateSupplier",
            data: json,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    // alert("Supplier Details Added")
                    //$("#frmSupplier").get(0).reset()
                    Success("Supplier Details Updated")
                    window.location.href = "Suppliers.aspx"
                }
            },
            error: function () {
                Success("An error occured while Updating Supplier Details")
                //alert('An error occured while Adding Supplier Details')
            }
        });
    }
}

function GetData() {
    var Data = { sid: sid };
    $.ajax({
        type: "POST",
        url: "../Handler/ApiHandller.asmx/GetSupplier",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                arrAPI = result.SupplierDetails;
                $("#txt_Sup_Company").val(arrAPI[0].Sup_Company);
                $("#txt_Sup_City").val(arrAPI[0].Sup_City);
                $("#txt_Sup_Country").val(arrAPI[0].Sup_Country);
                $("#txt_Sup_State").val(arrAPI[0].Sup_State);
                $("#txt_Sup_Zip").val(arrAPI[0].Sup_Zip);
                $("#txt_Sup_Email").val(arrAPI[0].Sup_Email);
                $("#txt_Sup_Fax").val(arrAPI[0].Sup_Fax)
                $("#txt_Sup_OwnerName").val(arrAPI[0].Sup_OwnerName);
                $("#txt_Sup_OwnerEmail").val(arrAPI[0].Sup_OwnerEmail);
                $("#txt_Sup_OwnerMobile").val(arrAPI[0].Sup_OwnerMobile);
                $("#txt_Sup_ResendName").val(arrAPI[0].Sup_ResName);
                $("#txt_Sup_ResendEmail").val(arrAPI[0].Sup_ResEmail);
                $("#txt_Resend_Mobile").val(arrAPI[0].Sup_ResMobile);
                $("#txt_Resend_Phone").val(arrAPI[0].Sup_ResPhone);
                $("#txt_Sup_Acc_Name").val(arrAPI[0].Sup_AccName);
                $("#txt_Sup_Acc_Email").val(arrAPI[0].Sup_AccEmail);
                $("#txt_Sup_Acc_Mobile").val(arrAPI[0].Sup_AccMobile);
                $("#txt_Sup_Acc_Phone").val(arrAPI[0].Sup_AccPhone);
                $("#txt_Supplier").val(arrAPI[0].Supplier);
                $("#txt_Sup_Mobile").val(arrAPI[0].Sup_Mobile)
                $("#txt_Sup_Phone").val(arrAPI[0].Sup_Phone)
                $("#txt_Address").val(arrAPI[0].Sup_Address)
                $("#txt_HotelComm1").val(arrAPI[0].HotelComm)
                $("#txt_HotelTDS1").val(arrAPI[0].HotelTDS)
                //$("#txt_PackageComm1").val(arrAPI[0].HotelComm)
                //$("#txt_PackageTDS1").val(arrAPI[0].HTds)
                if (arrAPI[0].Hotel)
                    $('#chkHotel').attr('checked', true)
                //$('input[id="chkHotel"][type="checkbox"][value="Hotel"]').attr("checked", true)
                //if (arrAPI[0].Visa)
                //    $('input[id="chkVisa"][type="checkbox"][value="Visa"]').attr("checked", true)
                //if (arrAPI[0].Otb)
                //    $('input[id="chkOtb"][type="checkbox"][value="Otb"]').attr("checked", true)
                if (arrAPI[0].Pakages)
                    $('input[id="chkPackages"][type="checkbox"][value="Packages"]').attr("checked", true)
                if (arrAPI[0].Flight)
                    $('input[id="chkFlight"][type="checkbox"][value="Flight"]').attr("checked", true)
                if (arrAPI[0].Active)
                    $('input[id="chkActive"][type="checkbox"][value="Active"]').attr("checked", true)
                $("#btn_Supplier").text("Update");
            }
        },
    });
}

function AddUpdate() {
    var btn = $("#btn_Supplier").text();
    if (btn == "Update") {
        UpdateSupplier();
    }
    else {
        AddSupplier();
    }
}

function OpenDilogBox() {
    $("#AddSupplierModal").modal("show")
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

/*
$("#txt_Sup_Company").val()
$("#txt_Sup_City").val()
$("#txt_Sup_Country").val()
$("#txt_Sup_State").val()
$("#txt_Sup_Zip").val()
$("#txt_Sup_Email").val()
$("#txt_Sup_Fax").val()
$("#txt_Sup_OwnerName").val()
$("#txt_Sup_OwnerEmail").val()
$("#txt_Sup_OwnerMobile").val()
$("#txt_Sup_ResendName").val()
$("#txt_Sup_ResendEmail").val()
$("#txt_Resend_Mobile").val()
$("#txt_Resend_Phone").val()
$("#txt_Sup_Acc_Name").val()
$("#txt_Sup_Acc_Email").val()
$("#txt_Sup_Acc_Mobile").val()
$("#txt_Sup_Acc_Phone").val()

function Update(Supplier, Hotel, Visa, Otb, Pakages, sid, Active, HComm, HTds, ContactPerson, ContactNo, Address)
{
    HiddenId = sid;
    HotelComm = HComm,
    HotelTDS = HTds
    $("#txt_HotelComm").val(HotelComm)
    $("#txt_HotelTDS").val(HTds)
    $("#txt_Supplier").val(Supplier);
    $("#txt_ContactPerson").val(ContactPerson)
    $("#txt_ContactNo").val(ContactNo)
    $("#txt_Address").val(Address)
    if (Hotel == "True")
        $('input[id="chkHotel"][type="checkbox"][value="Hotel"]').attr("checked", true)
    if (Visa == "True")
        $('input[id="chkVisa"][type="checkbox"][value="Visa"]').attr("checked", true)
    if (Otb == "True")
        $('input[id="chkOtb"][type="checkbox"][value="Otb"]').attr("checked", true)
    if (Pakages == "True")
        $('input[id="chkPackages"][type="checkbox"][value="Packages"]').attr("checked", true)
    if (Active == "True")
        $('input[id="chkActive"][type="checkbox"][value="Active"]').attr("checked", true)

    $("#UpdateSupplierModal").modal("show")

}
*/