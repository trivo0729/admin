﻿
$(function () {
    GetCancelationPolicies();
    DateSelect();
    GetCancellationDays();
})

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function DateSelect() {
    $("#txt_Date").datepicker($.extend({
        minDate: 0,
    }, datepickersOpt));
}

var CancelationName = "";

function checkNoShow() {
    var RefORNs = $('input[name=refund]:checked').val();
    if (RefORNs == "Refundable") { document.getElementById('divMainDateDays').style.display = ""; }
    else if (RefORNs == "NoShow") { document.getElementById('divMainDateDays').style.display = "none"; }
}

function checkRefund()
{
    var RefORNs = $('input[name=refund]:checked').val();
    if (RefORNs == "Refundable")
    {
        CancelationName = "REF/";
        var DaysORDate = $('input[name=DaysPrior]:checked').val();
        if (DaysORDate == "Date") { var date = $("#txt_Date").val(); CancelationName += "DT(" + date.substring(0, 5) + ")/"; }
        else if (DaysORDate == "DaysPrior") { var days = $("#txt_PriorDays").val(); CancelationName += "FD(" + days + ")/"; }

        var e = document.getElementById("sel_RateType");
        var RateType = e.options[e.selectedIndex].value;
        if (RateType == "Amount") { var Price = $("#txt_Amount").val(); CancelationName += "FR(" + Price + ")"; }
        else if (RateType == "Percentile") { var Price = $("#txt_Percent").val(); CancelationName += "(" + Price + "%)"; }
        else if (RateType == "Nights") { var Price = $("#txt_Nights").val(); CancelationName += "N(" + Price + ")"; }

    }
    else if (RefORNs == "NoShow")
    {
        CancelationName = "No Show/";

        var e = document.getElementById("sel_RateType");
        var RateType = e.options[e.selectedIndex].value;
        if (RateType == "Amount") { var Price = $("#txt_Amount").val(); CancelationName += "FR(" + Price + ")"; }
        else if (RateType == "Percentile") { var Price = $("#txt_Percent").val(); CancelationName += "(" + Price + "%)"; }
        else if (RateType == "Nights") { var Price = $("#txt_Nights").val(); CancelationName += "N(" + Price + ")"; }
    }

   
    

    $("#txt_CancelationPolicy").val(CancelationName)
}

function checkDaysPrerior()
{
    var DaysORDate = $('input[name=DaysPrior]:checked').val();
    if(DaysORDate == "Date")
    {
        document.getElementById("divchkDaysPrior").style.display = "none";
        document.getElementById("divchkDate").style.display = "";
    }
    else if(DaysORDate == "DaysPrior")
    {
        document.getElementById("divchkDaysPrior").style.display = "";
        document.getElementById("divchkDate").style.display = "none";
    }  
}

function fSelectRateType()
{
    var e = document.getElementById("sel_RateType");
    var RateType = e.options[e.selectedIndex].value;
    if (RateType == "Amount")
    {
        document.getElementById("divAmount").style.display = "";
        document.getElementById("divPercent").style.display = "none";
        document.getElementById("divNights").style.display = "none";
    }
    else if (RateType == "Percentile")
    {
        document.getElementById("divAmount").style.display = "none";
        document.getElementById("divPercent").style.display = "";
        document.getElementById("divNights").style.display = "none";
    }
    else if (RateType == "Nights")
    {
        document.getElementById("divAmount").style.display = "none";
        document.getElementById("divPercent").style.display = "none";
        document.getElementById("divNights").style.display = "";
        $("#txt_Nights").val("1");
        document.getElementById("txt_Nights").disabled = true;
        checkRefund();
    }
}

function GetCancellationDays() {
    $.ajax({
        type: "POST",
        url: "../CancelationHandler.asmx/GetCancellationDays",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var CancellationDays = result.CancellationDays;
                if (CancellationDays.length != 0)
                {
                    if (CancellationDays[0].DaysBefore != null && CancellationDays[0].DaysBefore != undefined) {
                        debugger
                        $('#CancellationDays').val(CancellationDays[0].DaysBefore);
                    }
                }
               
            }
        },
        error: function () {
        }
    });
}

function SaveCancellationDays() {
    var CancellationDays = $('#CancellationDays').val();

    var data = {
        CancellationDays: CancellationDays
    }

    $.ajax({
        type: "POST",
        url: "../CancelationHandler.asmx/SaveCancellationDays",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 1) {
                //Success("Bank details saved successfully.");
                //setTimeout(function () {
                //    window.location.reload();
                //}, 2000);
            }
        },
        error: function () {
            Success("An error occured while saving cancellation days.");
        }
    });
}

function SaveCancelation() {
    
    var RefundNoShow = $('input[name=refund]:checked').val();
    var IsDaysPrior = "";

    if (RefundNoShow == "Refundable")
    {
        var DaysORDate = $('input[name=DaysPrior]:checked').val();
        if (DaysORDate == "Date") { IsDaysPrior = "false"; if ($("#txt_Date").val() == "") { Success("Please Select Date"); return false } }
        else if (DaysORDate == "DaysPrior") { IsDaysPrior = "true"; if ($("#txt_PriorDays").val() == "") { Success("Please Enter Days Prior"); return false } }

        var Date = $("#txt_Date").val();
        var PriorDays = $("#txt_PriorDays").val();

        var e = document.getElementById("sel_RateType");
        var RateType = e.options[e.selectedIndex].value;
        if (RateType == "Amount") {
            if ($("#txt_Amount").val() == "") { Success("Please Enter Amount"); return false; }
        }
        else if (RateType == "Percentile") {
            if ($("#txt_Percent").val() == "") { Success("Please Enter Percent"); return false; }
        }
        else if (RateType == "Nights") {
            if ($("#txt_Nights").val() == "") { Success("Please Enter Nights"); return false; }
        }

        var Amount = $("#txt_Amount").val();
        var Percent = $("#txt_Percent").val();
        var Nights = $("#txt_Nights").val();
    }
    else if (RefundNoShow == "NoShow")
    {
        var e = document.getElementById("sel_RateType");
        var RateType = e.options[e.selectedIndex].value;
        if (RateType == "Amount") {
            if ($("#txt_Amount").val() == "") { Success("Please Enter Amount"); return false; }
        }
        else if (RateType == "Percentile") {
            if ($("#txt_Percent").val() == "") { Success("Please Enter Percent"); return false; }
        }
        else if (RateType == "Nights") {
            if ($("#txt_Nights").val() == "") { Success("Please Enter Nights"); return false; }
        }

        var Date = "";
        var PriorDays = "0";
        DaysORDate = "None";
        var Amount = $("#txt_Amount").val();
        var Percent = $("#txt_Percent").val();
        var Nights = $("#txt_Nights").val();
    } 

    var PolicyNote = $("#txtCancelNote").val();
        var PolicyName = $("#txt_CancelationPolicy").val();
        var data = {
            PolicyName: PolicyName,
            PolicyNote: PolicyNote,
            PriorDays: PriorDays,
            Date: Date,
            Amount: Amount,
            Percent: Percent,
            Nights: Nights,
            RateType: RateType,
            DaysORDate: DaysORDate,
            RefundNoShow: RefundNoShow
        }

        $.ajax({
            type: "POST",
            url: "CancelationHandler.asmx/AddCancelPolicy",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Cancelation Policy Added Successfully");
                    GetCancelationPolicies();
                }
                else if (result.retCode == 2) {
                    Success("Cancelation Already Present");
                    return false;
                }
                else
                {
                    Success("Something Went Wrong");
                    return false;
                }

            }

        });
}

function GetCancelationPolicies() {
              $("#tbl_CancelationList").dataTable().fnClearTable();
              $("#tbl_CancelationList").dataTable().fnDestroy();
                $.ajax({
                    type: "POST",
                    url: "CancelationHandler.asmx/GetCancelationPolicies",
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            CancelationPolicies = result.CancelationList;
                            $("#tbl_CancelationList tbody").empty();
                            //Hotels List
                            var trRequest = "";
                            if (CancelationPolicies.length > 0) {
                                for (i = 0; i < CancelationPolicies.length; i++) {
                                    trRequest += '<tr><td>' + CancelationPolicies[i].CancelationPolicy + '</td>';
                                    trRequest += '<td>' + CancelationPolicies[i].RefundType + '</td>';                                  
                                    if (CancelationPolicies[i].IsDaysPrior == "true")
                                    {
                                        trRequest += '<td>' + CancelationPolicies[i].DaysPrior + '</td>';
                                    }
                                    else 
                                    {
                                        trRequest += '<td>' + CancelationPolicies[i].Date + '</td>';
                                    }
                                    trRequest += '<td>' + CancelationPolicies[i].ChargesType + '</td>';
                                    if (CancelationPolicies[i].ChargesType == "Percentile")
                                    {
                                        trRequest += '<td>' + CancelationPolicies[i].PercentageToCharge + '%</td>';
                                    }
                                    else if (CancelationPolicies[i].ChargesType == "Nights") {
                                        trRequest += '<td>' + CancelationPolicies[i].NightsToCharge + ' Night</td>';
                                    }
                                    else
                                    {
                                        trRequest += '<td>' + CancelationPolicies[i].AmountToCharge + '</td>';
                                    }
                                   
                                   
                                    //trRequest += '<td><a href="javascript:void(0)" class="button icon-pencil">Edit</a></td></tr>';
                                }
                                trRequest += '</tbody>';
                                $("#tbl_CancelationList").append(trRequest);
                                $("#tbl_CancelationList").dataTable(
                                    {
                                        //"bFilter": true,
                                        //"bLengthChange": false,
                                        //"bPaginate": true,
                                         bSort: false, sPaginationType: 'full_numbers',
                                        "width": "104%"
                                    });
                                $('#tbl_CancelationList').removeAttr("style");
                            }
                        }
                        else if (result.retCode == 0) {
                            $("#tbl_CancelationList tbody").remove();
                            var trRequest = '<tbody>';
                            trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                            trRequest += '</tbody>';
                            $("#tbl_CancelationList").append(trRequest);
                        }

                    }

                });
            }
 