﻿var arrTax = new Array();
var arrMealAddon = new Array();
var arrChargeRate = new Array();
var arrAddOnsCharge = new Array();
var sHotelID = 0;
var AddCount = 0;
var arrChargeDetails = new Array();
$(function () {
    if (location.href.indexOf('?') != -1) {
        sHotelID = GetQueryStringParams("sHotelID");
    }
    GetTaxDetails();
})

function GetTaxDetails() {
    $.ajax({
        url: "../handler/TaxHandler.asmx/GetTaxMapping",
        type: "post",
        data: '{"HotelID":"' + sHotelID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrTax = result.arrTax
                arrCharges = result.arrChargeRate;
                arrMealAddon = result.arrAddOns;
                arrAddOnsCharge = result.arrAddOnsTax;
                if (location.href.indexOf('?') != -1 && arrCharges.length == 0)
                    AddTax(0, "div_UpdateTaxDetails")
                else if (location.href.indexOf('?') == -1)
                    AddTax(arrTax, 'div_TaxDetails')
                else
                    GenrateRates(arrTax);


                /*AddOns Taxtions*/
                if (location.href.indexOf('?') != -1 && arrAddOnsCharge.length == 0)
                    AddMeal(0, "div_UpdateMealDetails")
                else if (location.href.indexOf('?') == -1)
                    AddMeal(0, "div_MealDetails")
                else
                    GenrateMealRates();
            }
            else {
                // Success(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}
function GenrateRates() {
    var RatesType = [];
    for (var i = 0; i < arrCharges.length; i++) {
        RatesType.push(arrCharges[i].TaxID);
    }
    var unique = RatesType.filter(function (itm, i, RatesType) {
        return i == RatesType.indexOf(itm);
    });
    AddTax(arrTax, "div_UpdateTaxDetails");
    for (var j = 0; j < unique.length; j++) {
        var Rates = $.grep(arrCharges, function (H) { return H.TaxID == unique[j] })
           .map(function (H) { return H.TaxOnID; });
        var arrRate = [];
        arrRate.push(unique[j])
        //$("#sel_Type" + j).multipleSelect("setSelects", arrRate);
        //$("#sel_Type" + j).val(unique[j])
        $("#sel_TaxOn" + j).multipleSelect("setSelects", Rates);
    }
}

function GenrateTaxOn(ID) {
    var Opt = '';
    Opt += '<option value="0">Base Rate</option>'
    for (var i = 0; i < arrTax.length; i++) {
        if (ID == arrTax[i].ID)
            continue;
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    return Opt;
}
function GenrateChargeType(ID) {
    var Opt = '';
    Opt += '<option value="0" selected=selected style="color:red">Non Select</option>'
    Opt += '<option value="0">Base Rate</option>'
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    $('#' + ID).append(Opt)
    $('#' + ID).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: false,
        single: true,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        multiple: false, keepOpen: false,
    })
    return Opt;
}



function AddTax(arrTax, el) {
    try {
        for (var ID = 0; ID < arrTax.length; ID++) {
            var html = '';
            html += '<div class="columns TaxDetails" id="div_' + ID + '">'
            html += '<div class="five-columns twelve-columns-mobile SelBox" id="TaxType' + ID + '">'
            if (ID == 0)
                html += '<label class="input-info">Rate Type&nbsp;&nbsp;&nbsp;&nbsp;</label>';
            html += '<input type="hidden" class="sel_TaxType TaxId' + ID + '" value="' + arrTax[ID].ID + '"/>'
            html += '<input class="input full-width" value="' + arrTax[ID].Name + ' ' + '(' + arrTax[ID].Value + '%)' + '" readonly/>'
            html += '</select>'
            html += '</div>'

            html += ' <div class="five-columns twelve-columns-mobile SelBox" id="TaxOn' + ID + '">'
            if (ID == 0)
                html += '<label class="input-info">Tax On</label>';
            html += '<select id="sel_TaxOn' + ID + '" name="validation-select" multiple="multiple" class="  full-width sel_TaxOn">'
            html += ' </select>'
            html += '</div>'
            html += '<div class="ten-columns twelve-columns-mobile auto-expanding" id="TaxDetails' + ID + '" style="display:none">'
            if (ID == 0)
                html += '<label class="input-info">Tax Details</label>';
            html += '<textarea  name="autoexpanding" class="input full-width autoexpanding" id="txtTaxDetails' + ID + '"></textarea>'
            html += '</div>'
            html += '</div>'
            $("#" + el).append(html);
            $('#sel_TaxOn' + ID).append(GenrateTaxOn(arrTax[ID].ID))
            $('#sel_TaxOn' + ID).change(function () {
                console.log($(this).val());
            }).multipleSelect({
                selectAll: false,
                //single: true,
                multiple: true,
                width: '100%',
                delimiter: '+ ',
                filter: true,
                multiple: false,
                minimumCountSelected: 10,
                keepOpen: false,
            });
        }
    }
    catch (ex) {
        //  Success(ex.message)
    }

}
function GetTaxRates() {
    var arrTaxDetails = $(".TaxDetails");
    var arrRatesDetails = new Array();
    for (var i = 0; i < arrTaxDetails.length; i++) {
        var TaxOn = $(arrTaxDetails[i]).find(".sel_TaxOn")[0]
        for (var j = 0; j < TaxOn.length; j++) {
            if (TaxOn[j].selected) {
                if ($(arrTaxDetails[i]).find(".sel_TaxType").val() != "") {
                    arrRatesDetails.push({
                        TaxID: $(arrTaxDetails[i]).find(".sel_TaxType").val(),
                        TaxOnID: $(TaxOn[j]).val(),
                        IsAddOns: false,
                        HotelID: sHotelID,
                        ServiceID: 0,
                        ServiceName: "",
                        ParentID: 0
                    });
                }

            }

        }
    }
    return arrRatesDetails;
}


/*AddOns Mapping*/
function AddMeal(ID, el) {
    try {
        var arrAddons = $.grep(arrMealAddon, function (H) { return H.IsMeal != true })
           .map(function (H) { return H; });
        var html = '';
        ID = -1;
        html += '<div class="columns MealDetails">'
        html += '<div class="five-columns twelve-columns-mobile SelBox"">'
        if (ID == -1)
            html += '<label class="input-info">AddOns Type</label>';
        html += '<input type="hidden" class="sel_MealType MealId' + ID + '" value="0"/>'
        html += '<input id="sel_MealType' + ID + '" value="Meal" name="validation-select" class="input full-width " readonly/>'
        html += '</select>'
        html += '</div>'
        html += ''
        html += ' <div class="five-columns twelve-columns-mobile SelBox" >'
        if (ID == -1)
            html += '<label class="input-info">Tax On&nbsp;&nbsp;&nbsp;&nbsp;</label>';
        html += '<select id="sel_MealTaxOn' + ID + '" name="validation-select" multiple="multiple" class="  full-width sel_MealTaxOn">'
        html += ' </select>'
        html += '</div>'
        html += '<div class="three-columns"  style="display:none">'
        if (ID == -1)
            html += '<label class="input-info">Tax Details</label>';
        html += '<textarea rows="1" class="input full-width" id="txtMealDetails' + ID + '"></textarea>'
        html += '</div>'
        html += '</div>'
        $("#" + el).append(html);
        $('#sel_MealTaxOn' + ID).append(GenrateTaxOnMeal())
        $('#sel_MealTaxOn' + ID).change(function () {
            console.log($(this).val());
        }).multipleSelect({
            selectAll: false,
            //single: true,
            multiple: true,
            width: '100%',
            delimiter: '+ ',
            filter: true,
            multiple: false,
            minimumCountSelected: 10
        });
        for (var ID = 0; ID < arrAddons.length; ID++) {
            html = '';
            html += '<div class="columns MealDetails">'
            html += '<div class="five-columns twelve-columns-mobile SelBox"">'
            html += '<input type="hidden" class="sel_MealType MealId' + ID + '" value="' + arrAddons[i].ID + '"/>'
            html += '<input id="sel_MealType' + ID + '" value="' + arrAddons[i].AddOnName + '" name="validation-select" class="input full-width " readonly/>'
            html += '</select>'
            html += '</div>'
            html += ''
            html += ' <div class="five-columns twelve-columns-mobile SelBox" >'
            html += '<select id="sel_MealTaxOn' + ID + '" name="validation-select" multiple="multiple" class="  full-width sel_MealTaxOn">'
            html += ' </select>'
            html += '</div>'
            html += '<div class="three-columns"  style="display:none">'
            html += '<textarea rows="1" class="input full-width" id="txtMealDetails' + ID + '"></textarea>'
            html += '</div>'
            html += '</div>'
            $("#" + el).append(html);
            $('#sel_MealTaxOn' + ID).append(GenrateTaxOnMeal())
            $('#sel_MealTaxOn' + ID).change(function () {
                console.log($(this).val());
            }).multipleSelect({
                selectAll: false,
                //single: true,
                multiple: true,
                width: '100%',
                delimiter: '+ ',
                filter: true,
                multiple: false,
                minimumCountSelected: 10
            });
        }

    }
    catch (ex) {
        // Success(ex.message)
    }

}
function GenrateTaxOnMeal() {
    var Opt = '';
    for (var i = 0; i < arrTax.length; i++) {
        Opt += '<option value="' + arrTax[i].ID + '">' + arrTax[i].Name + '(' + arrTax[i].Value + '%)</option>'
    }
    return Opt;
}

function GenrateMealRates() {
    AddMeal(0, "div_UpdateMealDetails");
    var AddOnsType = [];
    for (var i = 0; i < arrAddOnsCharge.length; i++) {
        AddOnsType.push(arrAddOnsCharge[i].TaxID);
    }
    var unique = AddOnsType.filter(function (itm, i, AddOnsType) {
        return i == AddOnsType.indexOf(itm);
    });
    var Rates = $.grep(arrAddOnsCharge, function (H) { return H.TaxID == 0 })
           .map(function (H) { return H.TaxOnID; });
    $("#sel_MealTaxOn-1").multipleSelect("setSelects", Rates);
    for (var j = 0; j < unique.length; j++) {
        Rates = $.grep(arrAddOnsCharge, function (H) { return H.TaxID == unique[j] })
          .map(function (H) { return H.TaxOnID; });
        $("#sel_MealTaxOn" + j).multipleSelect("setSelects", Rates);
    }
}

function GetMealRates() {
    var arrMealDetails = $(".MealDetails");
    var arrMealRates = new Array();
    for (var i = 0; i < arrMealDetails.length; i++) {
        var TaxOn = $(arrMealDetails[i]).find(".sel_MealTaxOn")[0]
        for (var j = 0; j < TaxOn.length; j++) {
            if (TaxOn[j].selected) {
                if ($(arrMealDetails[i]).find(".sel_MealType").val() != "") {
                    arrMealRates.push({
                        TaxID: $(arrMealDetails[i]).find(".sel_MealType").val(),
                        TaxOnID: $(TaxOn[j]).val(),
                        IsAddOns: true,
                        HotelID: sHotelID,
                        ServiceID: 0,
                        ServiceName: "",
                        ParentID: 0
                    });
                }

            }

        }
    }
    return arrMealRates;
}
