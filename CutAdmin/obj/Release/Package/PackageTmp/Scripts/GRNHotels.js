﻿
$('.onlyNumbersText').find('input').on('keypress', function () {
    return event.charCode >= 48 && event.charCode <= 57;
})

$(function () {
    //HotelCode = getParameterByName('HotelCode');
    //HotelName = getParameterByName('HotelName');
    //$("#lblHotel").text(HotelName);
    //GetSuppliers();
    GetCountry();
    //GetCurrency();
    //GetMealPlans();
    //DateRateValidity(0);
    //getRooms();
   // document.getElementById('btn_GetGRNCode').click();
    //document.getElementById('chkAllSD0').click();
})

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}



function GetCountry() {
    var ddlRequest = '';
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option class="optCountry" value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    ddlRequest += ' <option value="Unselect" style="display:inline-block;" disabled>Unselect All</option>';
                    $("#txtCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
        }
    });
}