﻿var HB_Supplier, HB_CountryName, HB_CountryCode, HB_CityName, HB_CityCode;
var sdotwCityCode, sExpediaCityCode, sGrnCityCode;
function ManageMapping() {
    $(".citycode").change(function () {
        m_CheckBox = document.getElementsByClassName('citycode');
        m_Resevation = [];
        for (var i = 0; i < m_CheckBox.length; i++) {
            if (m_CheckBox[i].checked) {
                var data = m_CheckBox[i].value.split('_');
                if (data[1] == "HotelBeds")
                {
                    HB_CityCode = data[0];
                    HB_Supplier = data[1];
                    HB_CountryName = data[2];
                    HB_CountryCode = data[3];
                    HB_CityName = data[4];
                }
                else if (data[1] == "Dotw") {
                    sdotwCityCode = data[0];
                }
                else if (data[1] == "Expedia") {
                    sExpediaCityCode = data[0];

                }
                else if (data[1] == "Grn") {
                    sGrnCityCode = data[0];

                }
            }
        };
    })
}
var bvalid;
var sCountry;
var sCity;

function GetMappingDetails() {

    debugger;
    //var sSelect = document.getElementById("selCountry");
    //sCountry = $('#selCountry').val();
    //var sSelectcity = document.getElementById("selCity");
    //sCity = $('#selCity').val();

    var sSelect = document.getElementById("selCountry");
    sCountry = sSelect.options[sSelect.selectedIndex].text
    var sSelectcity = document.getElementById("selCity");
    sCity = sSelectcity.options[sSelectcity.selectedIndex].text
    var Data = {
        Countryname: sCountry,
        CityName: sCity
    }
    var DataToPass = JSON.stringify(Data);

    bvalid = Validate_City();

    if (bvalid == true) {
        $.ajax({
            type: "POST",
            url: "GenralHandler.asmx/GetCityCodeMapping",
            data: DataToPass,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

                $("#tbl_GetCityCodeMapping tbody").remove();
                if (result.retCode == 1) {
                    arrHotelBedsList = result.HotelBedsList;
                    arrDotwList = result.DotwList;
                    arrExpediaList = result.ExpediaList;
                    arrGrnList = result.GrnList;
                    
                    var trRequest = '<tbody><tr style="background-color:#494747;color:white"';
                    trRequest += '<td><span class="text-left"></span></td>';
                    trRequest += '<td><span class="text-left">Supplier</span></td>';
                    trRequest += '<td><span class="text-left">Country Name</span></td>';
                    trRequest += '<td><span class="text-left">Code</span></td>';
                    trRequest += '<td><span class="text-left">City Name</span></td>';
                    trRequest += '<td><span class="text-left">City Code</span></td>';
                    trRequest += '<td><span class="text-left">Area Name</span></td>';
                    trRequest += '<td><span class="text-left">Area Code</span></td>';
                    trRequest += '<td><span class="text-left">Select</span></td>';
                    trRequest += '</tr>';

                    //Hotel Beds
                    if (arrHotelBedsList.length > 0) {
                        for (i = 0; i < arrHotelBedsList.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].Supplier + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].Countryname + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].CountryCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].CityName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].CityCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].AreaName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrHotelBedsList[i].AreaCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span><input type="checkbox" class="citycode" name="checked[]" value="' + arrHotelBedsList[i].CityCode + '_' + arrHotelBedsList[i].Supplier + '_' + arrHotelBedsList[i].Countryname +'_'+ arrHotelBedsList[i].CountryCode +'_'+ arrHotelBedsList[i].CityName + '" id="selectCode"></span>';
                            trRequest += '</td></tr>';
                           
                        }
                       trRequest += '</tbody>';
                    }

                    // Dotw
                    if (arrDotwList.length > 0) {
                         for (i = 0; i < arrDotwList.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].Supplier + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].Countryname + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].CountryCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].CityName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].CityCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].AreaName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrDotwList[i].AreaCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span><input type="checkbox" class="citycode dotw" name="checked[]" value="' + arrDotwList[i].CityCode + '_' + arrDotwList[i].Supplier + '" id="selectCode"></span>';
                            trRequest += '</td></tr>';

                        }
                        trRequest += '</tbody>';
                    }

                    //Expedia
                    if (arrExpediaList.length > 0) {
                        for (i = 0; i < arrExpediaList.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].Supplier + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].Country + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].CountryCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].Destination + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].DestinationID + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].AreaName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrExpediaList[i].AreaCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span><input type="checkbox" class="citycode" name="checked[]" value="' + arrExpediaList[i].DestinationID + '_' + arrExpediaList[i].Supplier + '" id="selectCode"></span>';
                            trRequest += '</td></tr>';

                        }
                        trRequest += '</tbody>';
                    }


                    //Grn
                    if (arrGrnList.length > 0) {
                        for (i = 0; i < arrGrnList.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].Supplier + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].CountryName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].Countrycode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].Cityname + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].Citycode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].AreaName + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrGrnList[i].AreaCode + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span><input type="checkbox" class="citycode" name="checked[]" value="' + arrGrnList[i].Citycode + '_' + arrGrnList[i].Supplier + '" id="selectCode"></span>';
                            trRequest += '</td></tr>';

                         }
                         trRequest += '</tbody>';
                    }
                    $("#tbl_GetCityCodeMapping").append(trRequest);
                }
                else if (result.retCode == 0) {
                    $("#tbl_GetCityCodeMapping tbody").remove();
                    var trRequest = '<tbody>';
                    trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                    trRequest += '</tbody>';
                    $("#tbl_GetCityCodeMapping").append(trRequest);
                }
                ManageMapping()
            },
            error: function () {
            }
        });
    }
}

function MapCities()
{
    debugger
   
    //var HB_Supplier, HB_CountryName, HB_CountryCode, HB_CityName, HB_CityCode;
    //var sdotwCityCode, sExpediaCityCode, sGrnCityCode;
    //var ddlCodes = $(".citycode")
    //for (var k = 0; k < ddlCodes.length; k++)
    //{
    //    if (ddlCodes[k].checked == true)
    //    {
    //        HB_Supplier = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    //       .map(function (p) { return p.Supplier; });

    //        HB_CountryName = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    //         .map(function (p) { return p.Countryname; });

    //        HB_CountryCode = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    //          .map(function (p) { return p.CountryCode; });

    //        HB_CityName = $.grep(arrHotelBedsList, function (p) { return  p.Supplier == "HotelBeds"; })
    //         .map(function (p) { return p.CityName; });

    //        HB_CityCode = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    //         .map(function (p) { return p.CityCode; });

    //        for (var j = 0; j < arrDotwList.length; j++) {
    //            if (ddlCodes[k].checked == true) {
    //                if (ddlCodes[k].checked == true) {
    //                    sdotwCityCode = $.grep(arrDotwList, function (p) { return p.CityCode == ddlCodes[k].value && p.Supplier == "Dotw"; })
    //                   .map(function (p) { return p.CityCode; });
    //                }
    //            }
    //        }
           
    //        if (ddlCodes[k].checked == true) {
    //            sExpediaCityCode = $.grep(arrExpediaList, function (p) { return p.DestinationID == ddlCodes[k].value && p.Supplier == "Expedia"; })
    //                .map(function (p) { return p.DestinationID; });
    //        }
    //        if (ddlCodes[k].checked == true) {
    //            sGrnCityCode = $.grep(arrGrnList, function (p) { return p.Citycode == ddlCodes[k].value && p.Supplier == "Grn"; })
    //             .map(function (p) { return p.Citycode; });
    //        }
    //    }
       
       
    //}
    ////for (var k = 0; k < arrHotelBedsList.length; k++) {
    ////    if (ddlCodes[k].checked == true) {
    ////        HB_Supplier = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    ////       .map(function (p) { return p.Supplier; });

    ////        HB_CountryName = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    ////         .map(function (p) { return p.Countryname; });

    ////        HB_CountryCode = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    ////          .map(function (p) { return p.CountryCode; });

    ////        HB_CityName = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    ////         .map(function (p) { return p.CityName; });

    ////        HB_CityCode = $.grep(arrHotelBedsList, function (p) { return p.Supplier == "HotelBeds"; })
    ////         .map(function (p) { return p.CityCode; });

            
           
    ////    }
    ////}
    ////for (var k = 0; k < arrDotwList.length; k++) {
    ////    if (ddlCodes[k].checked == true) {
    ////        sdotwCityCode = $.grep(arrDotwList, function (p) { return  p.Supplier == "Dotw"; })
    ////       .map(function (p) { return p.CityCode; });
    ////    }
    ////}
    ////for (var k = 0; k < arrExpediaList.length; k++) {
    ////    if (ddlCodes[k].checked == true) {
    ////        sExpediaCityCode = $.grep(arrExpediaList, function (p) { return  p.Supplier == "Expedia"; })
    ////            .map(function (p) { return p.DestinationID; });
    ////    }
    ////}
    ////for (var k = 0; k < arrGrnList.length; k++) {
    ////    if (ddlCodes[k].checked == true) {
    ////        sGrnCityCode = $.grep(arrGrnList, function (p) { return  p.Supplier == "Grn"; })
    ////         .map(function (p) { return p.Citycode; });
    ////    }
    ////}

    var CountryName = HB_CountryName;
    var CountryCode = HB_CountryCode;
    var CityName = HB_CityName;
    var CityCode = HB_CityCode;
    var dotwCityCode = sdotwCityCode;
    var ExpediaCityCode = sExpediaCityCode;
    var GrnCityCode = sGrnCityCode;

    bvalid = true;
    if (bvalid == true) {
        var dataToPass = {
            CityName: CityName,
            CityCode: CityCode,
            CountryName: CountryName,
            CountryCode: CountryCode,
            dotwCityCode: dotwCityCode,
            ExpediaCityCode: ExpediaCityCode,
            GrnCityCode: GrnCityCode
        };

        var jsonText = JSON.stringify(dataToPass);
        $.ajax({
            type: "POST",
            url: "GenralHandler.asmx/AddMapping",
            data: jsonText,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    Success("City Mapped Successfully")
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
              
                }
                if (result.retCode == 0) {
                    Success("City Mapped Successfully");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
              
                }
            },
            error: function () {
                Success("An error occured while Adding details");
            }
        });

    }
    
}

function Validate_City() {

    debugger;
    bvalid = true;
    var reg = new RegExp('[0-9]$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    if (sCountry == "Select Any Country") {
        bvalid = false;
        //$("#lbl_selCountry").css("display", "");

        Success("Please select Country!");
        return bvalid;
    }
    if (sCity == "Select Any City") {
        bvalid = false;
        //$("#lbl_selCity").css("display", "");
        Success("Please select City!");

    }

    return bvalid;

}