﻿
$(document).ready(function () {
    GetGlobalMarkup();
});
var arrGlobalMarkup = new Array();
function GetGlobalMarkup() {

    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/GetGlobalMarkup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                arrGlobalMarkup = result.Arr;
                GenrateHtmlMarkup();
            }
        },
        error: function () {
        }

    });
}

function GenrateHtmlMarkup() {
    try {
        var html = '<div class="standard-tabs margin-bottom" id="add-tabs">';
        html += '<ul class="tabs">'
        for (var i = 0; i < arrGlobalMarkup.length; i++) {
            if (i == 0)
                html += '<li class="active"><a href="#' + arrGlobalMarkup[i].Name + '">' + arrGlobalMarkup[i].Name + '</a></li>'
            else
                html += '<li class=""><a href="#' + arrGlobalMarkup[i].Name + '">' + arrGlobalMarkup[i].Name + '</a></li>'
        }
        html += '</ul>'
        html += '<div class="tabs-content">'
        for (var i = 0; i < arrGlobalMarkup.length; i++) {
            html += '<div  class="with-padding" id="' + arrGlobalMarkup[i].Name + '"><div class="respTable">'
            html += '<table class="table responsive-table" id="sorting-advanced">'
            html += '<thead>'
            html += '<tr>                                                                                          '
            html += '<th scope="col">Supplier</th>                                                                 '
            html += '<th scope="col">MarkUp Percentage </th>                                                   '
            html += '<th scope="col" class="align-center hide-on-mobile">MarkUp Amount:</th>                   '
            html += '<th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage: </th>  '
            html += '<th scope="col" class="align-center">Commision Amount:</th>                               '
            html += '</tr>                                                                                         '
            html += '</thead><tbody>'
            var arrSupplier = arrGlobalMarkup[i].ListSupplier;
            for (var s = 0; s < arrSupplier.length; s++) {
                html += '<tr class="' + arrGlobalMarkup[i].Name + '"><td><label class="SupplierName">' + arrSupplier[s].Name + '</label></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].MarkupAmt + '" class="input-unstyled full-width MarkupPer"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].MarkupPer + '" class="input-unstyled full-width MarkupAmt"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].CommAmt + '" class="input-unstyled full-width CommPer"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].CommPer + '" class="input-unstyled full-width CommAmt"  placeholder="0%" type="text"></div></td></tr>'
            }
            html += '</tbody></table></div></div>'
        }
        html += '</div> '
        html += '</div><p class="SearchBtn"><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="SaveMarkup();" value="Save" /></p> '
        $("#div_Markup").append(html);
    }
    catch (ex) { }
}

function UpdateGlobalMarkup() {

    var MarkPercentage = $("#txt_MarkUpPercentage").val();
    if (MarkPercentage == "") {
        Success("Please enter Markup Percentage");
        return false;
    }
    var MarkUpAmount = $("#txt_MarkUpAmount").val();
    if (MarkUpAmount == "") {
        Success("Please enter MarkUp Amount");
        return false;
    }
    var CommPercentage = $("#txt_CommisionPercentage").val();
    if (CommPercentage == "") {
        Success("Please enter Commission Percentage");
        return false;
    }
    var CommAmount = $("#txt_CommisionAmount").val();
    if (CommAmount == "") {
        Success("Please enter Commission Amount");
        return false;
    }

    var data = {
        MarkPercentage: MarkPercentage,
        MarkUpAmount: MarkUpAmount,
        CommPercentage: CommPercentage,
        CommAmount: CommAmount,
    }

    $.ajax({
        type: "POST",
        url: "../MarkUpHandler.asmx/UpdateGlobalMarkupDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Global Markup Detail Update successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
               
            }
            else if (result.retCode == 0) {
                Success("Something went wrong.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
             
            }
        },
        error: function () {
        }

    });
}



function GetMarkup() {
    var arrMarkup = new Array();
    try{
        for (var i = 0; i < arrGlobalMarkup.length; i++) {
            var ndSupplier = $('.' + arrGlobalMarkup[i].Name);
            var ListSupplier = new Array();
            for (var s = 0; s < ndSupplier.length; s++) {
                var arrSupplier = $($(ndSupplier[s]).find('label')[0]).text()
                ListSupplier.push({
                    Name: arrSupplier,
                    MarkupAmt: $($(ndSupplier[s]).find('.MarkupPer')[0]).val(),
                    MarkupPer: $($(ndSupplier[s]).find('.MarkupAmt')[0]).val(),
                    CommAmt: $($(ndSupplier[s]).find('.CommAmt')[0]).val(),
                    CommPer: $($(ndSupplier[s]).find('.CommPer')[0]).val(),
                });
            }
            arrMarkup.push({ Name: arrGlobalMarkup[i].Name, ListSupplier: ListSupplier });
        }
    }
    catch(ex)
    {

    }
    return arrMarkup
}

function SaveMarkup() {
    var arrMarkup = GetMarkup();
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/SaveGlobalMarkup",
        data: JSON.stringify({ arrService: arrMarkup }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Saved")
            }
        },
        error: function () {
        }

    });
}