﻿$(document).ready(function () {
    GetGroup();
});
function GetGroup() {
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/GetGroup",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                var List = result.Arr;
                var div = "";
                for (var i = 0; i < List.length; i++) {
                    div += '<option value="' + List[i].sid + '" >' + List[i].GroupName + '</option>'
                }
                $("#SelGroup").append(div);
                $("#SelGroup").val('5');
                $("#DivGroupmark .select span")[0].textContent = "Group A";
                GetGroupMarkup();
            }
        },
        error: function () {
        }

    });
}
function GetGroupMarkup() {
    $("#div_Markup").empty();
    var GroupID = $("#SelGroup").val();
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/GetGroupMarkup",
        data:JSON.stringify({Id:GroupID}),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                arrGroupMarkup = result.arrDetails;
                GenrateHtmlMarkup();
            }
            else {
                Success("Something went wrong!");
            }
        },
        error: function () {
        }

    });
}
function GenrateHtmlMarkup() {
    try {
        var html = '<div class="standard-tabs margin-bottom" id="add-tabs">';
        html += '<ul class="tabs">'
        for (var i = 0; i < arrGroupMarkup.length; i++) {
            if (i == 0)
                html += '<li class="active"><a href="#' + arrGroupMarkup[i].Name + '">' + arrGroupMarkup[i].Name + '</a></li>'
            else
                html += '<li class=""><a href="#' + arrGroupMarkup[i].Name + '">' + arrGroupMarkup[i].Name + '</a></li>'
        }
        html += '</ul>'
        html += '<div class="tabs-content">'
        for (var i = 0; i < arrGroupMarkup.length; i++) {
            html += '<div  class="with-padding" id="' + arrGroupMarkup[i].Name + '"><div class="respTable">'
            html += '<table class="table responsive-table" id="sorting-advanced">'
            html += '<thead>'
            html += '<tr>                                                                                          '
            html += '<th scope="col">Supplier</th>                                                                 '
            html += '<th scope="col">MarkUp Percentage </th>                                                   '
            html += '<th scope="col" class="align-center hide-on-mobile">MarkUp Amount:</th>                   '
            html += '<th scope="col" class="align-center hide-on-mobile-portrait">Commision Percentage: </th>  '
            html += '<th scope="col" class="align-center">Commision Amount:</th>                               '
            html += '</tr>                                                                                         '
            html += '</thead><tbody>'
            var arrSupplier = arrGroupMarkup[i].ListSupplier;
            for (var s = 0; s < arrSupplier.length; s++) {
                html += '<tr class="' + arrGroupMarkup[i].Name + '"><td><label class="SupplierName">' + arrSupplier[s].Name + '</label></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].MarkupPer + '" class="input-unstyled full-width MarkupPer"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].MarkupAmt + '" class="input-unstyled full-width MarkupAmt"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].CommPer + '" class="input-unstyled full-width CommPer"  placeholder="0%" type="text"></div></td>'
                html += '<td><div class="input full-width"><input name="prompt-value"  value="' + arrSupplier[s].CommAmt + '" class="input-unstyled full-width CommAmt"  placeholder="0%" type="text"></div></td></tr>'
            }
            html += '</tbody></table></div></div>'
        }
        html += '</div> '
        html += '</div><p class="SearchBtn"><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="SaveMarkup();" value="Save" /></p> '
        $("#div_Markup").append(html);
    }
    catch (ex) { }
}

function GetMarkup() {
    var arrMarkup = new Array();
    try {
        for (var i = 0; i < arrGroupMarkup.length; i++) {
            var ndSupplier = $('.' + arrGroupMarkup[i].Name);
            var ListSupplier = new Array();
            for (var s = 0; s < ndSupplier.length; s++) {
                var arrSupplier = $($(ndSupplier[s]).find('label')[0]).text()
                ListSupplier.push({
                    Name: arrSupplier,
                    MarkupPer: $($(ndSupplier[s]).find('.MarkupPer')[0]).val(),
                    MarkupAmt: $($(ndSupplier[s]).find('.MarkupAmt')[0]).val(),
                    CommAmt: $($(ndSupplier[s]).find('.CommAmt')[0]).val(),
                    CommPer: $($(ndSupplier[s]).find('.CommPer')[0]).val(),
                });
            }
            arrMarkup.push({ Name: arrGroupMarkup[i].Name, ListSupplier: ListSupplier });
        }
    }
    catch (ex) {

    }
    return arrMarkup
}

function SaveMarkup() {
    var arrMarkup = GetMarkup();
    var GroupId = $("#SelGroup").val();
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/SaveGroupMarkup",
        data: JSON.stringify({ arrService: arrMarkup, GroupId: GroupId }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Group Markup Saved")
                //window.location.reload();
            }
        },
        error: function () {
        }

    });
}


//************************************ Code for PopUp **********************************************************
function AddGroupModalFunc() {
    $.modal({
        content: '<div class="columns" id="VisaModal">' +
                    '<div class="twelve-columns twelve-columns-mobile" id="divForVisa">' +
                        '<label>Group Name : </label><br/> ' +
                        '<div class="input full-width">' +
                            '<input name="prompt-value" id="txtGroupName" value="" onblur="GetGroupName()" class="input-unstyled full-width"  placeholder="Group Name" type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="twelve-columns twelve-columns-mobile">' +
                       '<div class="with-padding" id="div_AddGroupMarkup">' +
            
                        '</div>' +
                    '</div>' +
                '<p class="text-right" style="text-align:right;">' +
                    '<button type="button" class="button anthracite-gradient" onclick="AddGroup()">Add</button>' +
                '</p>',

        title: 'Add Group ',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
   
}

function GetGroupName() {
    debugger;
    var GetgName = $("#txtGroupName").val();
    $.ajax({
        type: "POST",
        url: "../handler/MarkUpHandler.asmx/GetGuropName",
        data: '{"GetgName":"' + GetgName + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrGetgnameArr = result.GetgnameArr;
                for (i = 0; i < arrGetgnameArr.length; i++) {
                    var ReName = arrGetgnameArr[i].GroupName
                    if (ReName == GetgName) {
                        alert("Group Name Already Exist !");
                        // $("#alSuccess").css("display", "");
                        $("#txtGroupName").focus();
                        // $("#lbl_GroupSid").text(arrGetgnameArr[i].sid);
                    }
                }
            }

        },
    });

}

