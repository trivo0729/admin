﻿var Count = 1;
$(function () {
    HotelCode = GetQueryStringParams('sHotelID');
    HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
    //RoomName = GetQueryStringParams('RoomType').replace(/%20/g, ' ');
    //HotelAddress = GetQueryStringParams('HotelAddress');
    $("#lbl_Hotel").text(HotelName);
    //$("#lbl_roomtype").text(RoomName);
    // $("#lbl_address").text(HotelAddress);
    AddDates();
    GetHotelAddress(HotelCode);
    GetRooms(HotelCode);
    getDates(Count);

    $("#txt_Till").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

})

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}

function getDates(Count) {
    $('#datepicker' + Count + '').datepicker($.extend({
        minDate: 0,
        onSelect: function () {
            var minDate = $(this).datepicker('getDate');
            minDate.setDate(minDate.getDate() + 1); //add One days
            $('#datepicker' + parseInt(Count + 1) + '').datepicker("option", "minDate", minDate);
        }
    }, datepickersOpt));

    $('#datepicker' + parseInt(Count + 1) + '').datepicker($.extend({
        onSelect: function () {
            var maxDate = $(this).datepicker('getDate');
            maxDate.setDate(maxDate.getDate());
            //ChangeNights();
        }
    }, datepickersOpt));


}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    //var sURLVariables = sVariables.split('%20');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}



var txt = 1;
function AddDates() {


    var Div = "";
    Div += '<div class="columns" id="MyDiv' + txt + '">'

    Div += '<div class="three-columns">'
    Div += '<small class="input-info">Valid From*:</small>'
    Div += '<input class="input ui-autocomplete-input full-width dt1"  type="text" id="datepicker' + Count + '" name="datepicker_From' + Count + '" style="cursor: pointer" value="" />'

    Div += '</div>'

    Div += '<div class="three-columns">'
    Div += '<small class="input-info">Valid To*:</small>'
    Div += '<input class="input ui-autocomplete-input full-width dt2" type="text" id="datepicker' + parseInt(Count + 1) + '"  style="cursor: pointer" value="" />'

    Div += '</div>'



    Div += '<br><div id="btnAddSeason' + txt + '" class="one-columns" title="Add Dates">'
    Div += '<i Onclick="CheckEmpty(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button grey-gradient" ><span class="icon-plus">More Dates</span></label></i>'

    Div += '</div>'
    if (Count != 1) {
        $("#btnAddSeason" + parseInt(txt - 1)).hide();
        Div += '<div class="one-columns" title="Remove Date">'
        Div += '<i Onclick="RemoveSeasonUI(\'' + Count + '\',\'' + txt + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button grey-gradient" ><span class="icon-minus"></span></label></i>'
        Div += '</div>'
    }


    Div += '</div>'
    $("#DatesUI").append(Div);
    $("#DatesUI").show();

    //DateDisAuto(Count);
    getDates(Count)
    Count += 2;
    txt++;

}

function CheckEmpty(dt, txt) {
    var chk = true;
    var Ndt = parseInt(parseInt(dt) + parseInt(1));

    if ($("#datepicker" + dt).val() == "") {
        // $("#lbl_datepicker" + dt).css("display", "");
        Success("Please select From date");
        chk = false;
    }
        //else {
        //    $("#lbl_datepicker" + dt).css("display", "none");
        //}
    else if ($("#datepicker" + Ndt).val() == "") {
        // $("#lbl_datepicker" + Ndt).css("display", "");
        Success("Please select Till date");
        chk = false;
    }
    //else {
    //    $("#lbl_datepicker" + Ndt).css("display", "none");
    //}
    if (chk) {
        AddDates();
    }
}

function RemoveSeasonUI(Cnt, Id) {
    var c = parseInt(Cnt) - 1;
    var dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
    var Ck = dateObject.selector;
    if (Ck != undefined) {
        for (var i = 0; i < 10; i++) {
            c = parseInt(c) - 2;
            dateObject = $("#datepicker" + c).datepicker('getDate', '+1d');
            Ck = dateObject.selector;
            if (Ck == undefined) {
                break;
            }
        }
    }
    var eDate = new Date();
    // var dateObject = $("#datepicker" + parseInt(parseInt(Cnt) - 1)).datepicker('getDate', '+1d');
    eDate.setDate(dateObject.getDate() + 1);
    if ($("#datepicker" + parseInt(parseInt(Cnt) + 2)).val() != undefined) {
        $("#datepicker" + parseInt(parseInt(Cnt) + 2)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Cnt) + 2)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: eDate,
            //beforeShowDay: disableDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Cnt) + 2), 1);
            }
        });
        $("#datepicker" + parseInt(parseInt(Cnt) + 3)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Cnt) + 3)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: eDate,
            //beforeShowDay: disableDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Cnt) + 3), 2);
            }
        });
    }
    $("#MyDiv" + Id).remove();
    if (txt <= parseInt(Id) + 1) {
        $("#btnAddSeason" + parseInt(parseInt(Id) - 1)).show();

    }
    if ($(".name").length == 1) {
        $("#btnAddSeason1").show();
    }
}


function DateDisAuto(Id) {
    if (Id == 1) {
        $("#datepicker" + Id).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $("#datepicker" + parseInt(Id + 1)).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: "dateToday",
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
    else {
        $("#datepicker" + Id).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: endDate,
            onSelect: function (date) {
                DateDisable(Id, 1);
            }
        });
        $('#datepicker' + parseInt(Id + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: endDate,
            onSelect: function (date) {
                DateDisable(Id + 1, 2);
            }
        });
    }
}
function DateDisable(Id, chk) {

    var dateObject = $("#datepicker" + Id).datepicker('getDate', '+1d');
    endDate.setDate(dateObject.getDate() + 1);

    if (chk == 1) {
        $("#datepicker" + parseInt(parseInt(Id) + 1)).datepicker("destroy");
        $('#datepicker' + parseInt(parseInt(Id) + 1)).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: endDate,
            onSelect: function (date) {
                DateDisable(parseInt(parseInt(Id) + 1), 2);
            }
        });

    }


}
var endDate = new Date();

function GetHotelAddress(HotelCode) {

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetHotelAddress",
        data: JSON.stringify({ HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelAddress = result.HotelAddress;
                $("#lbl_address").text("Address: " + arrHotelAddress.HotelAddress)

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}   

function GetRooms(HotelCode) {

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetRooms",
        data: JSON.stringify({ sHotelId: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                RoomList = result.RoomList;
                if (RoomList.length > 0) {
                    $("#ddlRoom").empty();

                    var ddlRequest;
                    for (i = 0; i < RoomList.length; i++) {
                        ddlRequest += '<option value="' + RoomList[i].RoomTypeID + '">' + RoomList[i].RoomType + '</option>';

                    }
                    $("#ddlRoom").append(ddlRequest);


                }

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

var RatType = "", selectedCities = []; var pRateType = "";
function fnRatType() {

    var RatType = $('.Rate option:checked').map(function () {
        return this.value;
    }).get();

    //var Nationalities = '';
    var pRateTypes = "";

    for (var i = 0; i < RatType.length; i++) {
        if (RatType[i] == "All") {
            pRateTypes += '';
            pRateType = pRateTypes;
        }
        else {
            pRateTypes += RatType[i] + "^";
            pRateType = pRateTypes;
        }

    }



}
var InvLiveOrReq = "Live";
function SaveInventory(type) {
    var Room = $("#ddlRoom").val();
    var MaxRoom = $('#txt_MaxRoomperbook').val();
    var DtTill = $('#txt_Till').val();
    var OptRoomperDate = $('#txt_MaxRoomperDate').val();
    var InType = type;


    var DateInvFr = [];
    var DateInvTo = [];
    for (var i = 0; i < $(".dt1").length; i++) {

        var dt = $(".dt1")[i].value;
        DateInvFr.push(dt);

    }
    for (var i = 0; i < $(".dt2").length; i++) {

        var dt = $(".dt2")[i].value;
        DateInvTo.push(dt);

    }

    if (Room == "-" || Room == null) {
        Success("Please Select Room");
        return false;
    }

    if (pRateType == "" || pRateType == null) {
        Success("Please Select Rate Type");
        return false;
    }
    if (DateInvFr == "") {
        Success("Please Select Valid From Date");
        return false;
    }

    if (DateInvTo == "") {
        Success("Please Select Valid To Date");
        return false;
    }
    if (MaxRoom == "" || MaxRoom == undefined) {
        MaxRoom = "";
    }
    if (DtTill == "" || DtTill == undefined) {
        DtTill = "";
    }

    if (OptRoomperDate == "" || OptRoomperDate == undefined) {
        OptRoomperDate = "";
    }

    if (InvLiveOrReq == "" || InvLiveOrReq == undefined) {
        InvLiveOrReq = "";
    }

    var data =
         {
             HotelCode: HotelCode,
             Room: Room,
             MaxRoom: MaxRoom,
             DtTill: DtTill,
             DateInvFr: DateInvFr,
             DateInvTo: DateInvTo,
             pRateType: pRateType,

             OptRoomperDate: OptRoomperDate,
             InvLiveOrReq: InvLiveOrReq,
             InType: InType
         }

    $.ajax({
        type: "POST",
        url: "InventoryHandler.asmx/SaveInventory",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Inventory Added Sucessfully");
                setTimeout(function () {
                    window.location.href = "HotelInventory.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
                }, 2000);

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

function Redirectbtn(value) {
    if (value == "Live") {
        InvLiveOrReq = "Live";
    }
    else if (value == "Request") {
        InvLiveOrReq = "Request";
    }
}

function ShowAllocation() {
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Add Allocation?</p>',
            function () {
                Allocation();
            },
            function () {
                $('#modals').remove();
            }
           );
}

function ShowAllotment() {
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to Add Allotment?</p>',
            function () {
                Allotment();
            },
            function () {
                $('#modals').remove();
            }
           );
}

function SaveAllocation(type) {
    var Room = $("#ddlRoom").val();
    var NoOfRoom = $('#txt_NoOfRoom').val();
    var DaysPrior = $('#txt_DaysPrior').val();
    var InType = type;


    var DateInvFr = [];
    var DateInvTo = [];
    for (var i = 0; i < $(".dt1").length; i++) {

        var dt = $(".dt1")[i].value;
        DateInvFr.push(dt);

    }
    for (var i = 0; i < $(".dt2").length; i++) {

        var dt = $(".dt2")[i].value;
        DateInvTo.push(dt);

    }

    if (Room == "-" || Room == null) {
        Success("Please Select Room");
        return false;
    }

    if (pRateType == "" || pRateType == null) {
        Success("Please Select Rate Type");
        return false;
    }

    if (NoOfRoom == "-" || NoOfRoom == null || NoOfRoom == "") {
        Success("Please Select No Of Room");
        return false;
    }

    if (DateInvFr == "") {
        Success("Please Select Valid From Date");
        return false;
    }

    if (DateInvTo == "") {
        Success("Please Select Valid To Date");
        return false;
    }
    if (NoOfRoom == "" || NoOfRoom == undefined) {
        NoOfRoom = "";
    }

    if (InvLiveOrReq == "" || InvLiveOrReq == undefined) {
        InvLiveOrReq = "";
    }
    if (DaysPrior == "" || DaysPrior == undefined) {
        DaysPrior = "";
    }

    var data =
         {
             HotelCode: HotelCode,
             Room: Room,
             NoOfRoom: NoOfRoom,
             DateInvFr: DateInvFr,
             DateInvTo: DateInvTo,
             pRateType: pRateType,
             InvLiveOrReq: InvLiveOrReq,
             InType: InType,
             DaysPrior: DaysPrior
         }

    $.ajax({
        type: "POST",
        url: "InventoryHandler.asmx/SaveAllocation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Inventory Added Sucessfully");
                setTimeout(function () {
                    window.location.href = "HotelInventory.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName + "";
                }, 2000);

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}

