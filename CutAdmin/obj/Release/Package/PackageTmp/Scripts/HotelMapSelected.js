﻿/// <reference path="E:\Azhar\CutAdmin\CutAdmin\CutAdmin\HotelsMapping.aspx" />
var arrFacilities = [], arrFacility = [], selectedFacilities = []; var Img = [];
var pFacilities = "", bvalid, Langitude, Latitude, HotelImage, SubImages = "", HotelBedsCode = "", DotwCode = "", MGHCode = "", GRNCode = "", ExpediaCode = "", GTACode = "",
    BaseSupplier, UserName, Date;
var sAdultMinAge = 0;
var sChildAgeFrom = 0;
var sChildAgeTo = 0;
var City = "";
var Country = "";
var ZipCode = "";
var sHotelD;
var HotelCode;
var DataSelected = [];
var CutHotelCode = '';



$(function () {
    debugger
    var sPageURL = window.location.href;
    var code1 = sPageURL.split('?');
    var code = code1[1].split('=');
    var session;
    var selectedPage = code[0];
    if (selectedPage == 'sHotelID') {
        sHotelD = GetQueryStringParams('sHotelID').replace(/%20/g, ' ')
        sHotelD = sHotelD.replace(/^,|,$/g, '');
    }
    else if (selectedPage == 'HotelCode') {
        HotelCode = GetQueryStringParams('HotelCode').replace(/%20/g, ' ')
        HotelCode = HotelCode.replace(/^,|,$/g, '');
        var session = GetQueryStringParams('session').replace(/%20/g, ' ')

    }
    getFacilities();
    City = GetQueryStringParams('City').replace(/%20/g, ' ')
    City = City.replace(/^,|,$/g, '');

    Country = GetQueryStringParams('Country').replace(/%20/g, ' ')
    Country = Country.replace(/^,|,$/g, '');

    //document.getElementById('htlCity').value = City;
    //document.getElementById('htlCountry').value = Country;


    var data =
    {
        HotelCode: HotelCode,
        session: session
    }
    debugger
    $.ajax({
        type: "POST",
        url: "HotelMappingHandler.asmx/GetMappedHotel",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var trRequest = "", trReques1 = "", trReques2 = "", trReques3 = "", trReques4 = "";

                arrSelectedHotels = result.CommonHotel;
                // var Location = arrSelectedHotels.Langitude + ',' + arrSelectedHotels.Latitude;
                Langitude = arrSelectedHotels[0].Langitude;
                Latitude = arrSelectedHotels[0].Latitude;
                //getZipcode(lang, latt);
                var address = arrSelectedHotels[0].Address;
                var arr = address.match(/(\d+)/g);
                if (arr != null)
                {
                    var zip;
                    for (var j = 0 ; j < arr.length; j++) {
                        if (arr[j].length == 6) zip = arr[j];
                    }
                    ZipCode = zip;
                }
                for (i = 0; i < arrSelectedHotels.length; i++) {

                    Img = arrSelectedHotels[i].Image;
                    if (Img.length > 0) {
                        for (var j = 0; j < Img.length; j++) {
                            SubImages += Img[j].Url + ",";
                        }
                        HotelImage = arrSelectedHotels[i].Image[0].Url;
                    }
                    if (arrSelectedHotels[i].Supplier == "HotelBeds") {
                        HotelBedsCode = arrSelectedHotels[i].HotelId;
                    }
                    else if (arrSelectedHotels[i].Supplier == "DoTW") {
                        DotwCode = arrSelectedHotels[i].HotelId;
                    }
                    else if (arrSelectedHotels[i].Supplier == "MGH") {
                        MGHCode = arrSelectedHotels[i].HotelId;
                    }
                    else if (arrSelectedHotels[i].Supplier == "Expedia") {
                        ExpediaCode = arrSelectedHotels[i].HotelId;
                    }
                    else if (arrSelectedHotels[i].Supplier == "GRN") {
                        GRNCode = arrSelectedHotels[i].HotelId;
                    }
                    else if (arrSelectedHotels[i].Supplier == "GTA") {
                        GTACode = arrSelectedHotels[i].HotelId;
                    }
                    if (arrSelectedHotels[i].Supplier == "ClickUrTrip") {
                        CutHotelCode = arrSelectedHotels[i].HotelId;
                    }

                    trRequest += '<p class="inline-label">';
                    trRequest += '<label class="label"><span><input type="checkbox" class="hname" onclick="fnHotelName(this)" name="checked[]" value="' + arrSelectedHotels[i].HotelName + '"  id="pHotelName"></span>' + arrSelectedHotels[i].Supplier + '</label>';
                    trRequest += ' <label  for="pHotelName">' + arrSelectedHotels[i].HotelName + '</label>';
                    trRequest += '</p>';

                    trReques1 += '<p class="inline-label">';
                    trReques1 += '<label class="label"><span><input type="checkbox" class="hAddress" onclick="fnHotelAddress(this)" name="checked[]" value="' + arrSelectedHotels[i].Address + '"  id="pHotelAddress"></span>' + arrSelectedHotels[i].Supplier + '</label>';
                    trReques1 += ' <label for="pHotelAddress" >' + arrSelectedHotels[i].Address + '</label>';
                    trReques1 += '</p>';

                    trReques2 += '<p class="inline-label">';
                    trReques2 += '<label  class="label"><span><input type="checkbox" class="hDescription" onclick="fnDescription(this)"  name="checked[]" value="' + arrSelectedHotels[i].Description + '"  id="pHotelDescription"></span>' + arrSelectedHotels[i].Supplier + '</label>';
                    trReques2 += ' <label for="pHotelDescription">' + arrSelectedHotels[i].Description + '</label>';
                    trReques2 += '</p>';

                    trReques3 += '<p class="inline-label">';
                    trReques3 += '<label class="label"><span><input type="checkbox" class="hRatings" onclick="fnRatings(this)"  name="checked[]" value="' + arrSelectedHotels[i].Category + '" title="' + arrSelectedHotels[i].Supplier + '"  id="pHotelRatings"></span>' + arrSelectedHotels[i].Supplier + '</label>';
                    //trReques3 += ' <label for="pHotelRatings">' + arrSelectedHotels[i].Category + '</label>';
                    if (arrSelectedHotels[i].Category == 'Other' || arrSelectedHotels[i].Category == '48055' || arrSelectedHotels[i].Category == '0')
                    {
                        trReques3 += ' <label for="pHotelRatings"><i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</label>';
                    }
                    else if (arrSelectedHotels[i].Category == '1EST' || arrSelectedHotels[i].Category == '559' || arrSelectedHotels[i].Category == '1') {
                        trReques3 += '<label for="pHotelRatings"><i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</label>';
                    }
                    else if (arrSelectedHotels[i].Category == '2EST' || arrSelectedHotels[i].Category == '560' || arrSelectedHotels[i].Category == '2') {
                        trReques3 += '<label for="pHotelRatings"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</label>';
                    }
                    else if (arrSelectedHotels[i].Category == '3EST' || arrSelectedHotels[i].Category == '561' || arrSelectedHotels[i].Category == '3') {
                        trReques3 += '<label for="pHotelRatings"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</label>';
                    }
                    else if (arrSelectedHotels[i].Category == '4EST' || arrSelectedHotels[i].Category == '562' || arrSelectedHotels[i].Category == '4') {
                        trReques3 += '<label for="pHotelRatings"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star-empty"></i>&nbsp;</label>';
                    }
                    else if (arrSelectedHotels[i].Category == '5EST' || arrSelectedHotels[i].Category == '563' || arrSelectedHotels[i].Category == '5') {
                        trReques3 += '<label for="pHotelRatings"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</label>';
                    }
                    else {
                        trReques3 += '<label for="pHotelRatings">' + arrSelectedHotels[i].Category + '</label>'
                        // trRequest += '<td width="10%" align="left"><i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;<i class="icon-star"></i>&nbsp;</td>';
                    }
                    trReques3 += '</p>';

                    trReques4 += '<p class="inline-label">';
                    //trReques4 += '<label for="input-1" class="label"><span><input type="checkbox" class="HtlFacilities" onclick="fnFacilities(this)"  name="checked[]" value="' + arrSelectedHotels[i].Facility + '"  id="pHotelName"></span>' + arrSelectedHotels[i].Supplier + '</label>';
                    if (arrSelectedHotels[i].Supplier != 'ClickUrTrip')
                    {
                        if (arrSelectedHotels[i].Facility[i] != "Unavailable") {
                            trReques4 += '<label  class="label"><span><input type="checkbox" class="HtlFacilities" onclick="fnFacilities(this)"  name="checked[]" value="' + arrSelectedHotels[i].Facility + '"  id="pHotelFacilities"></span>' + arrSelectedHotels[i].Supplier + '</label><span><label  id="pHotelFacility"></span>';
                            trReques4 += ' <label for="pHotelFacility">' + arrSelectedHotels[i].Facility + '</label>';
                        }
                    }
                    //else
                    //{
                    //    var HotelFacilities =  arrSelectedHotels[i].Facility;
                    //    for (var j = 0; j < arrFacilities.length; j++) {
                    //        for (var k = 0; k < HotelFacilities.length; k++)
                    //        {
                    //            if (arrFacilities[j].HotelFacilityID == HotelFacilities[k]) {
                    //                trReques4 += '<label  class="label"><span><input type="checkbox" class="HtlFacilities" onclick="fnFacilities(this)"  name="checked[]" value="' + arrFacilities[j].HotelFacilityName + '"  id="pHotelFacilities1"></span>' + arrSelectedHotels[i].Supplier + '</label><span><label  id="pHotelFacility"></span>';
                    //                trReques4 += ' <label for="pHotelFacility">' + arrFacilities[j].HotelFacilityName + ',</label>';
                    //            }
                    //        }
                           
                    //    }
                    //}
                    
                   
                    trReques4 += '</p>';
                }
                //getFacilities();
                $("#divHotelname").append(trRequest);
                $("#divHotelAddress").append(trReques1);
                $("#divHotelDescription").append(trReques2);
                $("#divHotelRatings").append(trReques3);
                $("#divFacilities1").append(trReques4);

            }
            //$("#htlZipcode").val(ZipCode);
            //$("#htlLangitude").val(Langitude);
            //$("#htlLatitude").val(Latitude);
        },
        error: function () {
        }
    });

})


function selectAll(value) {
    var HtlFacilities = document.getElementsByClassName('HtlFacilities');
    var SelAllFacilities = document.getElementById('SelAllFacilities');
    if (SelAllFacilities.selected == false) {
        for (var i = 0; i < HtlFacilities.length; i++) {
            SelAllFacilities.selected = true;
            HtlFacilities[i].selected = true;

        }
    }
    else if (SelAllFacilities.selected == true) {
        for (var i = 0; i < HtlFacilities.length; i++) {
            SelAllFacilities.selected = false;
            HtlFacilities[i].selected = false;
        }
    }

}

function fnFacilities() {

    var HtlFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < HtlFacilities.length; i++) {
        if (HtlFacilities[i].selected == true) {
            selectedFacilities[i] = HtlFacilities[i].value;
            pFacilities += selectedFacilities[i] + ',';
        }
    }


}

function SaveHotelMapping() {

    var sHotelName = $('#HtlName').val();
    var sHotelAddress = $('#HtlAddress').val();
    var sDescription = $('#HtlDescription').val();
    var sRatings = $('#HtlRatings').val();
    var sFacilities = $('#HtlFacilities').val();
    var sLangitude = $('#htlLangitude').val();
    var sLatitude = $('#htlLatitude').val();
    var sMainImage = HotelImage;
    var sSubImages = SubImages;
    var sHotelBedsCode = HotelBedsCode;
    var sDotwCode = DotwCode;
    var sMGHCode = MGHCode;
    var sExpediaCode = ExpediaCode;
    var sGRNCode = GRNCode;
    var sGTACode = GTACode;
    var sCountryCode = $('#htlCountry').val();
    var sCityCode = $('#htlCity').val();
    var sZipCode = $('#htlZipcode').val();
    var sPatesAllowed = $('#selPatesAllowed').val();
    var sLiquorPolicy = $('#selLiquorPolicy').val();
    var sTripAdviserLink = $('#txtTripAdviserLink').val();
    var sHotelGroup = $('#txtHotelGroup').val();

    sAdultMinAge = $('#txtAdultMinAge').val();
    sChildAgeFrom = $('#txtChildAgeFrom').val();
    sChildAgeTo = $('#txtChildAgeTo').val();
    var sCheckinTime = $('#txtCheckinTime').val();
    var sCheckoutTime = $('#txtCheckoutTime').val();

    if (sHotelName == "") {
        Success('Please Enter Hotel Name');
        return false;
    }
    if (sDescription == "") {
        Success('Please Enter Hotel Address');
        return false;
    }
    if (sRatings == "") {
        Success('Please Enter Hotel Ratings ');
        return false;
    }
    if (sAdultMinAge == "") {
        sAdultMinAge = 0;
    }
    if (sChildAgeFrom == "") {
        sChildAgeFrom = 0;
    }
    if (sChildAgeTo == "") {
        sChildAgeTo = 0;
    }


    var dataToPass = {
        sHotelName: sHotelName,
        sHotelAddress: sHotelAddress,
        sDescription: sDescription,
        sRatings: sRatings,
        sFacilities: sFacilities,
        sLangitude: sLangitude,
        sLatitude: sLatitude,
        sMainImage: sMainImage,
        sSubImages: sSubImages,
        sHotelBedsCode: sHotelBedsCode,
        sDotwCode: sDotwCode,
        sMGHCode: sMGHCode,
        sGRNCode: sGRNCode,
        sGTACode: sGTACode,
        sExpediaCode: sExpediaCode,
        sCountryCode: sCountryCode,
        sCityCode: sCityCode,
        sZipCode: sZipCode,
        sPatesAllowed: sPatesAllowed,
        sLiquorPolicy: sLiquorPolicy,
        sTripAdviserLink: sTripAdviserLink,
        sHotelGroup: sHotelGroup,
        sAdultMinAge: sAdultMinAge,
        sChildAgeFrom: sChildAgeFrom,
        sChildAgeTo: sChildAgeTo,
        sCheckinTime: sCheckinTime,
        sCheckoutTime: sCheckoutTime
    }


    $.ajax({
        type: "POST",
        url: "HotelMappingHandler.asmx/AddHotelMapping",
        data: JSON.stringify(dataToPass),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                Success("Hotel Mapped Successfully")
                setTimeout(function () {
                    window.location.href = "MappedHotelsList.aspx";
                }, 2000);
              
            }
            if (result.retCode == 0) {
                Success("Something went wrong");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
          
            }
        },
        error: function () {
            Success("An error occured while Adding details");

        }
    });


}


function fnHotelName(obj) {
    var hname = document.getElementsByClassName('hname');

    for (var i = 0; i < hname.length; i++) {
        hname[i].checked = false;
    }
    obj.checked = true;
    //document.getElementById('HtlName').value = obj.value;
    $('#lblHotelname').val(obj.value);
}
function fnHotelAddress(obj) {
    var hAddress = document.getElementsByClassName('hAddress');

    for (var i = 0; i < hAddress.length; i++) {
        hAddress[i].checked = false;
    }
    obj.checked = true;
    //document.getElementById('HtlAddress').value = obj.value;
    $('#lblHotelAddress').val(obj.value);
}
function fnDescription(obj) {
    var hDescription = document.getElementsByClassName('hDescription');

    for (var i = 0; i < hDescription.length; i++) {
        hDescription[i].checked = false;
    }
    obj.checked = true;
    //document.getElementById('HtlDescription').value = obj.value;
    $('#lblHotelDescription').val(obj.value);
}
function fnRatings(obj) {
    var hRatings = document.getElementsByClassName('hRatings');

    for (var i = 0; i < hRatings.length; i++) {
        hRatings[i].checked = false;
    }
    obj.checked = true;
    // document.getElementById('HtlRatings').value = obj.value;
    $('#lblHotelRatings').val(obj.value+'-'+obj.title);

}

var bindFacilities = '';
function fnFacilities(obj) {
    var hFacilities = document.getElementsByClassName('HtlFacilities');

    for (var i = 0; i < hFacilities.length; i++) {
        hFacilities[i].checked = false;
    }
    obj.checked = true;
    //document.getElementById('HtlFacilities').value = obj.value;
   // $('#lblHotelFacilities').val(obj.value);
    bindFacilities = obj.value;
    FacilitiesBinding();
}

function FacilitiesBinding() {
    var bFacilities = bindFacilities.split(',');
    if (bFacilities.length > 0) {
        $("#divFacilities2").empty();
        var chkRequest = '<div class="columns">';
        var j = 0;
        for (i = 0; i < bFacilities.length; i++) {
            chkRequest += '<div class="six-columns"><span>'
            chkRequest += '<input type="checkbox" id="supFacility' + i + '" onclick="SupplierFacilitiesAdd(id);" class="SupplierFacilities" value="' + bFacilities[i] + '"><label for="supFacility'+i+'">' + bFacilities[i] + '</label></span>';
            chkRequest += '</span></div>'
        }
         $("#divFacilities2").append(chkRequest);
    }
}


function SupplierFacilitiesAdd(id) {
    var addedFacilities = "";
   // $('#supFacility' + i).checked = true;
    var hFacilities = document.getElementsByClassName('SupplierFacilities');
    var facility = document.getElementById('supFacility'+i);
    for (var i = 0; i < hFacilities.length; i++) {
        if (hFacilities[i].checked) {
            addedFacilities = addedFacilities + ',' + hFacilities[i].value;
        }
    }
    addedFacilities = addedFacilities.replace(/^,|,$/g, '');
    //addedFacilitiesValues = addedFacilitiesValues.replace(/^,|,$/g, '');
    $('#lblHotelFacilities').val(addedFacilities);
    //$('#SelectedFacilities').val(addedFacilitiesValues);
}

function addFacilities() {
    var addedFacilitiesValues = "";
    var addedFacilities = "";
    var hFacilities = document.getElementsByClassName('HotelFacilities');
    var facility = document.getElementById('hFacility');
    for (var i = 0; i < hFacilities.length; i++) {
        if (hFacilities[i].checked) {
            addedFacilities = addedFacilities + ',' + hFacilities[i].title;
            addedFacilitiesValues = addedFacilitiesValues + ',' + hFacilities[i].value;
        }

    }
    addedFacilities = addedFacilities.replace(/^,|,$/g, '');
    addedFacilitiesValues = addedFacilitiesValues.replace(/^,|,$/g, '');
    $('#lblHotelFacilities').val(addedFacilities);
    $('#SelectedFacilities').val(addedFacilitiesValues);
}

function getFacilities() {
    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetFacilities",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFacilities = result.HotelFacilities;
               
            }
        },
        error: function () {
        }
    });

}

var SessionSelectedHotels;
function GetSelectedData() {
    debugger;
    var sHotelName = $('#lblHotelname').val();
    var sHotelAddress = $('#lblHotelAddress').val();
    var sHotelDescription = $('#lblHotelDescription').val();
    var sHotelRatings = $('#lblHotelRatings').val();
    var sHotelFacilitiesText = $('#lblHotelFacilities').val();
    var sHotelFacilitiesCode = $('#SelectedFacilities').val();
    var sCity = City;
    var sCountry = Country;
    var sZipCode = ZipCode;
    var sLangitude = Langitude;
    var sLatitude = Latitude;
    var sMainImage = HotelImage;
    var sSubImages = SubImages;
    var sHotelBedsCode = HotelBedsCode;
    var sDotwCode = DotwCode;
    var sMGHCode = MGHCode;
    var sExpediaCode = ExpediaCode;
    var sGRNCode = GRNCode;
    var sGTACode = GTACode;
    if (sHotelName == "")
    {
        Success("Please enter Hotel Name");
        return false;
    }
    

    // DataSelected = [sHotelName, sHotelAddress, sHotelDescription, sHotelRatings, sHotelFacilitiesText, sHotelFacilitiesCode, sCity, sCountry, sZipCode, sLangitude, sLatitude, sMainImage, sSubImages, sHotelBedsCode, sDotwCode, sMGHCode, sExpediaCode, sGRNCode];
    var data = {
        sHotelName: sHotelName,
        sHotelAddress: sHotelAddress,
        sHotelRatings: sHotelRatings,
        sHotelFacilitiesText: sHotelFacilitiesText,
        sHotelFacilitiesCode: sHotelFacilitiesCode,
        sCity: sCity,
        sCountry: sCountry,
        sZipCode: sZipCode,
        sLangitude: sLangitude,
        sLatitude: sLatitude,
        sMainImage: sMainImage,
        sHotelBedsCode: sHotelBedsCode,
        sDotwCode: sDotwCode,
        sMGHCode: sMGHCode,
        sExpediaCode: sExpediaCode,
        sGRNCode: sGRNCode,
        sGTACode: sGTACode,
        CutHotelCode: CutHotelCode
    }
    sHotelDescription = sHotelDescription.replace('&', 'and').replace('(', ' ');
    
    $.ajax({
        url: "HotelMappingHandler.asmx/GetSelectedHotels",
        type: "post",
        data: '{"sHotelDescription":"' + sHotelDescription + '","sSubImages":"' + sSubImages + '","sHotelFacilitiesText":"' + sHotelFacilitiesText + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#tbl_GetHotelMapping tbody").remove();

            if (result.retCode == 1) {
                arrHotellist = result.SelectedHotels;
                window.location.href = "AddHotelDetails.aspx?data=" + JSON.stringify(data);
            }
            else if (result.retCode == 0) {

            }

        },

    });

    
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}