﻿var globe_ItineraryCount = 0;
var globe_urlParamDecoded = 0;
var global_CategoryCount = 0;
var global_Itenerary = [];
var hcd;
$(document).ready(function () {
    debugger;
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var urlParameter = atob(url[0]).split('=');
    hcd = GetQueryStringParams('hcd');
    globe_urlParamDecoded = urlParameter[1];
    $('#lst_BasicInformation').on("click", function () { BasicDetails(globe_urlParamDecoded); });
    $('#lst_Pricing').on("click", function () { UpdatePricingDetails(globe_urlParamDecoded); });
    $('#lst_Itinerary').on("click", function () { ItineraryDetails(globe_urlParamDecoded); });
    $('#lst_HotelDetails').on("click", function () { HotelDetails(globe_urlParamDecoded); });
    $('#lst_PackageImages').on("click", function () { PackageImagesImages(globe_urlParamDecoded); });
    $('#hdnHCode').val(hcd);

    $.ajax({
        url: "PackageDetailHandler.asmx/GetItineraryDetail",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                globe_ItineraryCount = result.List_ItineraryDetail[0].nDuration;
                global_Itenerary = result.List_ItineraryDetail;
                global_CategoryCount = result.nCount;
                CreateItineraryEditors(global_Itenerary);
            }
        },
        error: function () {
        }
    });
});

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}
function GetItineraryVal(Itinerary, tabindex, index) {
    if (index == 1) {
        return Itinerary[tabindex].sItinerary_1.split('^-^');
    }
    else if (index == 2) {
        return Itinerary[tabindex].sItinerary_2.split('^-^');
    }
    else if (index == 3) {
        return Itinerary[tabindex].sItinerary_3.split('^-^');
    }
    else if (index == 4) {
        return Itinerary[tabindex].sItinerary_4.split('^-^');
    }
    else if (index == 5) {
        return Itinerary[tabindex].sItinerary_5.split('^-^');
    }
    else if (index == 6) {
        return Itinerary[tabindex].sItinerary_6.split('^-^');
    }
    else if (index == 7) {
        return Itinerary[tabindex].sItinerary_7.split('^-^');
    }
    else if (index == 8) {
        return Itinerary[tabindex].sItinerary_8.split('^-^');
    }
    else if (index == 9) {
        return Itinerary[tabindex].sItinerary_9.split('^-^');
    }
    else if (index == 10) {
        return Itinerary[tabindex].sItinerary_10.split('^-^');
    }
    else if (index == 11) {
        return Itinerary[tabindex].sItinerary_11.split('^-^');
    }
    else if (index == 12) {
        return Itinerary[tabindex].sItinerary_12.split('^-^');
    }
    else if (index == 13) {
        return Itinerary[tabindex].sItinerary_13.split('^-^');
    }
    else if (index == 14) {
        return Itinerary[tabindex].sItinerary_14.split('^-^');
    }
    else if (index == 15) {
        return Itinerary[tabindex].sItinerary_15.split('^-^');
    }
}

function CreateItineraryEditors(Itinerary) {
    var sTabWidth = 100 / global_CategoryCount;
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    var count = Itinerary[0].nDuration;
    for (var k = 0; k < global_CategoryCount; k++) {
        var sTabID = GetCategoryName(Itinerary[k].nCategoryID);
        if (k == 0) {
            sclass = "active";
            sDisplay = ";display:block";

        }
        else {
            sclass = "";
            sDisplay = ";";
        }
        sTabRow += '<li onclick="mySelectUpdate()" id="li' + sTabID + '" class="' + sclass + '" style="width:' + sTabWidth + '%' + sDisplay + '"><a data-toggle="tab" href="#Tab' + sTabID + '"><span class=""></span><span class="hidetext">' + sTabID + '</span>&nbsp;</a></li>';
        sTabRowContent += '<div class="tab-pane ' + sclass + '" id="Tab' + sTabID + '">';
        sTabRowContent += '<table id="tbl_AddPackagePrice' + sTabID + '" class="table" style="float: right">';
        for (var i = 0; i < count ; i++) {
            var sItineraryArray = GetItineraryVal(Itinerary, k, i + 1);
            if (sItineraryArray[0] == undefined) {
                sItineraryArray[0] = "";
            }
            if (sItineraryArray[1] == undefined) {
                sItineraryArray[1] = "";
            }
            if (sItineraryArray[2] == undefined) {
                sItineraryArray[2] = "";
            }
            sTabRowContent += '<tr>';
            sTabRowContent += '<td style="width:10%;padding-top:2%" ><b>Day ' + (i + 1) + '</b></td>';
            sTabRowContent += '<td><input type="text" class="form-control margtop5" id="txt_ItineraryHotelName_' + sTabID + '_' + i + '" value="' + sItineraryArray[0] + '" style="width:50%;display:inline;float:left" placeholder="Hotel Name" onfocus="AutoComplete(this.id)"/> &nbsp;&nbsp;<input type="text" id="txt_ItineraryPlace_' + sTabID + '_' + i + '" class="form-control margtop5" value="' + sItineraryArray[1] + '" style="width:40%;display:inline;float:right" placeholder="Place"/><br/><br/><br/> <textarea cols="80" rows="10" id="txt_Itinerary_' + sTabID + '_' + i + '" name="content">' + sItineraryArray[2] + '</textarea></td>';
            sTabRowContent += '</tr>';
        }
        sTabRowContent += '</table></div></div>';
    }
    $("#sItenararyTabs").html(sTabRow);
    $("#div_IteneraryTabContent").html(sTabRowContent);
    //$("#tbl_AddPackagePrice tbody").html(tRow);
    for (var k = 0; k < global_CategoryCount; k++) {
        for (var j = 0; j < count ; j++) {
            $('#txt_Itinerary_' + GetCategoryName(Itinerary[k].nCategoryID) + '_' + j).ckeditor();
        }
    }
}
function SaveItinerary() {
    debugger;
    for (var k = 0; k < global_CategoryCount; k++) {
        for (var i = 0; i < globe_ItineraryCount; i++) {
            var sHotelName = $("#txt_ItineraryHotelName_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            var sPlace = $("#txt_ItineraryPlace_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            var sItinerary = $("#txt_Itinerary_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            if (sHotelName == "") {
                $("#txt_ItineraryHotelName_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).focus();
                alert("Please fill hotel name for category " + GetCategoryName(global_Itenerary[k].nCategoryID));
                return false;
            }
            else if (sPlace == "") {
                $("#txt_ItineraryPlace_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).focus();
                alert("Please fill place for category " + GetCategoryName(global_Itenerary[k].nCategoryID));
                return false;
            }
            else if (sItinerary == "") {
                $("#txt_Itinerary_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).focus();
                alert("Please fill itinerary for category " + GetCategoryName(global_Itenerary[k].nCategoryID));
                return false;
            }
        }
    }
    var singleItinerary;
    var nSuccessCount = 0;
    for (var k = 0; k < global_CategoryCount; k++) {
        var listItinerary = [];
        for (var i = 0; i < globe_ItineraryCount; i++) {
            var sHotelName = $("#txt_ItineraryHotelName_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            var sPlace = $("#txt_ItineraryPlace_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            var sItinerary = $("#txt_Itinerary_" + GetCategoryName(global_Itenerary[k].nCategoryID) + '_' + i).val();
            value = sHotelName + '^-^' + sPlace + '^-^' + sItinerary;
            listItinerary.push(value);
        }
        var jsonText = JSON.stringify({ nID: globe_urlParamDecoded, listItinerary: listItinerary, nCategoryID: global_Itenerary[k].nCategoryID, sCategoryName: GetCategoryName(global_Itenerary[k].nCategoryID) });
        $.ajax({
            url: "AddPackageHandler.asmx/SaveItinerary",
            type: "post",
            data: jsonText,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.Session == 0) {
                    window.location.href = "index.htm";
                }
                if (result.retCode == 0) {
                    alert("No packages found");
                }
                else if (result.retCode == 1) {
                    nSuccessCount = nSuccessCount + 1;
                    if (nSuccessCount == global_CategoryCount) {
                        alert("Itinerary Added Successfully, Please add Hotel details to products");
                        HotelDetails(globe_urlParamDecoded);
                    }
                }
            },
            error: function () {
                alert('Error in saving, please try again!');
            }
        });
    }
}
function UpdatePricingDetails(nID) {
    $(location).attr('href', '../Admin/PricingDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function HotelDetails(nID) {
    $(location).attr('href', '../Admin/UpdateHotelDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function BasicDetails(nID) {
    $(location).attr('href', '../Admin/PackageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function ItineraryDetails(nID) {
    $(location).attr('href', '../Admin/ItineraryDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function PackageImagesImages(nID) {
    $(location).attr('href', '../Admin/PackageImageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

