﻿
var arrCountry = [];
var arrCity = [];

$(document).ready(function () {
    
    GetCountry();
    $('#sel_Nationality').change(function () {
        var sndcountry = $('#sel_Nationality').val();
        GetCity(sndcountry);

    });
    setTimeout(function () {
        GetUserDetail();
    }, 2000);
    
});
    function GetCountry() {
        $.ajax({
            type: "POST",
            url: "../GenralHandler.asmx/GetCountry",
            data: {},
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCountry = result.Country;
                    if (arrCountry.length > 0) {
                        $("#sel_Nationality").empty();
                        // $("#selCountryy").empty();
                        var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                        for (i = 0; i < arrCountry.length; i++) {
                            ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                            //contryname = arrCountry[i].Countryname;
                        }
                        $("#sel_Nationality").append(ddlRequest);
                        // $("#selCountryy").append(ddlRequest);

                    }
                }
            },
            error: function () {
            }
        });
    }



    function GetCity(reccountry) {
        $.ajax({
            type: "POST",
            url: "../GenralHandler.asmx/GetCity1",
            data: '{"country":"' + reccountry + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCity = result.CityList;
                    if (arrCity.length > 0) {
                        $("#sel_City").empty();
                        var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                        for (i = 0; i < arrCity.length; i++) {
                            ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                            //cityname = arrCity[i].Description;
                        }
                        $("#sel_City").append(ddlRequest);
                        //$("#selCityy").append(ddlRequest);

                    }
                }
                if (result.retCode == 0) {
                    $("#selCity").empty();
                }
            },
            error: function () {
            }
        });
    }




    function Save() {
        var FistNAme = $("#txtFirstName").val();
        var LastNAme = $("#txtLastName").val();
        var Designation = $("#txtDesignation").val();
        var Mobile = $("#txtMobile").val();

        var CompanyName = $("#txtCompanyName").val();
        var Companymail = $("#txtCompanymail").val();
        var Phone = $("#txtPhone").val();
        var Address = $("#txtAddress").val();
        var Country = $("#sel_Nationality option:selected").val();
        var City = $("#sel_City option:selected").val();

        var PinCode = $("#txtPinCode").val();
        var PanNo = $("#txtPanNo").val();
        var FaxNo = $("#txtFaxNo").val();
 
        if (FistNAme == "" || FistNAme == null) {
            Success("Please Select First Name")
            return false;
        }
        if (Designation == "" || Designation == null) {
            Success("Please Select Designation")
            return false;
        }
        if (CompanyName == "" || CompanyName == null) {
            Success("Please Select CompanyName")
            return false;
        }
        if (Country == "" || Country == null || Country == "-") {
            Success("Please Select Country")
            return false;
        }
        if (City == "" || City == null || City == "-") {
            Success("Please Select City")
            return false;
        }



        var data = {
            FistNAme: FistNAme,
            LastNAme: LastNAme,
            Designation: Designation,
            Mobile: Mobile,
            CompanyName: CompanyName,
            Companymail: Companymail,
            Phone: Phone,
            Address: Address,
            Country: Country,
            City: City,
            PinCode: PinCode,
            PanNo: PanNo,
            FaxNo: FaxNo
        }


        $.ajax({
            type: "POST",
            url: "../Profile.asmx/Save",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Updated Successfully");
                    var fileUpload = $("#Logo").get(0);
                    var files = fileUpload.files;
                   
                    if (files.length > 0) {
                        SaveLogo();
                    }
                }
                else {
                    Success("Something Went Wrong")
                    return false;
                }
            }
        })
    }

    function GetUserDetail()
    {
        $.ajax({
            type: "POST",
            url: "../Profile.asmx/GetUserDetail",
            data: {},
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var UserDetail = result.UserDetail;
                    $("#txtFirstName").val(UserDetail.ContactPerson);
                    $("#txtLastName").val(UserDetail.Last_Name);
                    $("#txtDesignation").val(UserDetail.Designation);
                    $("#txtMobile").val(UserDetail.Mobile);
                    $("#txtCompanyName").val(UserDetail.AgencyName);
                    $("#txtCompanymail").val(UserDetail.uid);
                    $("#txtPhone").val(UserDetail.phone);
                    $("#txtAddress").val(UserDetail.Address);
                    //$("#sel_Nationality option:selected").val(UserDetail.sCountry);
                    GetNationality(UserDetail.Countryname);
                    //$("#sel_City option:selected").val(UserDetail.Description);
                    setTimeout(function () {
                        GetOldCity(UserDetail.Description);
                    }, 2000);
                    
                    $("#txtPinCode").val(UserDetail.PinCode);
                    $("#txtPanNo").val(UserDetail.PANNo);
                    $("#txtFaxNo").val(UserDetail.Fax);
                }
                else {
                    Success("Something Went Wrong")
                    return false;
                }
            }
        })
    }

    function GetNationality(Country) {
        debugger;
        try {
            var checkclass = document.getElementsByClassName('check');
            var Country = Country.split(",")
            var CountryCode = $.grep(arrCountry, function (p) { return p.Countryname == Country; })
               .map(function (p) { return p.Country; });


            $("#drp_Nationality .select span")[0].textContent = CountryCode;
            for (var i = 0; i < arrCountry.length; i++) {
                if (arrCountry[i].Country == CountryCode) {
                    //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                    $("#drp_Nationality .select span")[0].textContent = arrCountry[i].Countryname;
                    $('input[value="' + Country + '"][class="OfferType"]').prop("selected", true);

                    $("#sel_Nationality").val(Country);
                    //  pGTA += GTA[i] + ",";
                    var selected = [];
                }


            }
            GetCity(Country);
        }
        catch (ex)
        { }
    }

    function GetOldCity(City) {
        debugger;
        try {
           
            var Code = $.grep(arrCity, function (p) { return p.Description == City; })
               .map(function (p) { return p.Code; });


            $("#drp_City .select span")[0].textContent = Code;
            for (var i = 0; i < arrCity.length; i++) {
                if (arrCity[i].Description == City) {
                    //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');     
                    $("#drp_City .select span")[0].textContent = arrCity[i].Description;
                    $('input[value="' + arrCity[i].Code + '"][class="OfferType"]').prop("selected", true);

                    $("#sel_City").val(arrCity[i].Code);
                    //  pGTA += GTA[i] + ",";
                    var selected = [];
                }


            }
        }
        catch (ex)
        { }
    }

    function SaveLogo() {
        var SubImages = '';
        var fileUpload = $("#Logo").get(0);
        var files = fileUpload.files;
      
        if (files.length != 0) {
            SubImages += files[0].name;
        }
    
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
       
        $.ajax({
            url: "../LogoHandler.ashx?ImagePath=" + SubImages,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (result) {
                // Success("Images Uploaded. Please wait..")
            },
        });
    }

