﻿
$(document).ready(function () {
    debugger
    //GetMasterOffer();
    GetAllSeasonsList();
    id = GetQueryStringParams('id');
    if (id != undefined) {
        $("#btn_update").show();
        $("#btn").hide();
        GetOfferByID(id);
    }

});


function AddOfferMaster() {
    bValid = Validate();
    if (bValid == true) {
        //  Supplier = $("#SelectSupplier option:selected").val();
        //  FromInventory = $("#SelectInventory option:selected").val();
        var OfferN = "";
        if (OfferNat.length == 1) {
            OfferN = OfferNat[0];
        }
        else {
            for (var i = 0; i < OfferNat.length; i++) {
                if (i == 0) {
                    OfferN = OfferNat[i];
                }
                else {
                    OfferN = OfferN + "," + OfferNat[i];
                }
            }
        }
        OfferNat = OfferN;
        HotelId = Sid;
        //if ($("#btn").val() == "Submit")
        //{
        //    Sid = 0;
        //}
        var Data = {
            OfferNat: OfferNat, SeasonName: SeasonName, SeasonDate1: SeasonDate1, SeasonDate2: SeasonDate2, BlockNames: BlockNames, SpecialDate1: SpecialDate1, SpecialDate2: SpecialDate2, BlockDays: BlockDays, HotelOfferCode: HotelOfferCode,
            CutCode: CutCode, OfferTerm: OfferTerm, OfferNote: OfferNote, DaysPrior: DaysPrior, FixedDate: FixedDate, DiscountAmount: DiscountAmount, DiscountPer: DiscountPer, NewRate: NewRate,
            FreeItemName: FreeItemName, FreeItemDetail: FreeItemDetail, OfferType: OfferType
        };

        $.ajax({
            type: "POST",
            url: "MasterHotelOfferHandler.asmx/AddOfferMaster",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Offer created.");
                    window.location.reload();
                    ClearAll();
                }
            },
        });
    }
    else
        Success("Please fill All Compulsary Details");
}
var OfferId;
function UpdateOfferMaster() {
    bValid = Validate();
    if (bValid == true) {
        var OfferN = "";

        //  Supplier = $("#SelectSupplier option:selected").val();
        //  FromInventory = $("#SelectInventory option:selected").val();
        if (OfferNat.length == 1) {
            OfferN = OfferNat[0];
        }
        else {
            for (var i = 0; i < OfferNat.length; i++) {
                if (i == 0) {
                    OfferN = OfferNat[i];
                }
                else {
                    OfferN = OfferN + "," + OfferNat[i];
                }
            }
        }
        OfferNat = OfferN;
        HotelId = Sid;
        OfferId = id;
        //if ($("#btn").val() == "Submit")
        //{
        //    Sid = 0;
        //}
        var Data = {
            OfferId: OfferId,
            OfferNat: OfferNat,
            SeasonName: SeasonName,
            SeasonDate1: SeasonDate1,
            SeasonDate2: SeasonDate2,
            BlockNames: BlockNames,
            SpecialDate1: SpecialDate1,
            SpecialDate2: SpecialDate2,
            BlockDays: BlockDays,
            HotelOfferCode: HotelOfferCode,
            CutCode: CutCode,
            OfferTerm: OfferTerm,
            OfferNote: OfferNote,
            DaysPrior: DaysPrior,
            FixedDate: FixedDate,
            DiscountAmount: DiscountAmount,
            DiscountPer: DiscountPer,
            NewRate: NewRate,
            FreeItemName: FreeItemName,
            FreeItemDetail: FreeItemDetail,
            OfferType: OfferType,
            SidOffer: SidOffer
        };

        $.ajax({
            type: "POST",
            url: "MasterHotelOfferHandler.asmx/UpdateOfferMaster",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Offer Updated.");
                    setTimeout(function () {
                        window.location.href = "MasterOffer.aspx";
                    }, 500);
                }
            },
        });
    }

}

function Validate() {
    bValid = true;

    //HotelId = Sid;
    OfferId = id;
    OfferNat = $("#select_OfferNationality").val();
    // OfferNat = $("#select_OfferNationality").val();
    HotelOfferCode = $("#txtHotelOfferCode").val();
    CutCode = $("#txtHotelCode").val();
    OfferTerm = $("#txtOfferTerm").val();
    OfferNote = $("#txtOfferNote").val();


    if (OfferNat == "" || OfferNat == null) {
        $('#lblselect_OfferNationality').css("display", "");
        bValid = false;
    }
    else {
        $('#lblselect_OfferNationality').css("display", "none");
    }
    //Season Dates
    SeasonName = "";
    SeasonDate1 = "";
    SeasonDate2 = "";
    for (var i = 0; i < $(".name").length; i++) {

        var name = $(".name")[i].value;
        SeasonName += name + "^";
        if (name == "") {
            $('#lbl_' + $(".name")[i].id).css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_' + $(".name")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".dt1").length; i++) {

        var dt = $(".dt1")[i].value;
        SeasonDate1 += dt + "^";
        if (dt == "") {
            $('#lbl_' + $(".dt1")[i].id).css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_' + $(".dt1")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".dt2").length; i++) {

        var dt = $(".dt2")[i].value;
        SeasonDate2 += dt + "^";
        if (dt == "") {
            $('#lbl_' + $(".dt2")[i].id).css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_' + $(".dt2")[i].id).css("display", "none");
        }
    }
    //END Season Dates

    //Special Dates
    BlockNames = "";
    SpecialDate1 = "";
    SpecialDate2 = "";
    for (var i = 0; i < $(".names").length; i++) {

        var name = $(".names")[i].value;
        BlockNames += name + "^";
        if (name == "") {
            //$('#lbl_' + $(".names")[i].id).css("display", "");
            //bValid = false;
        }
        else {
            $('#lbl_' + $(".names")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".dts1").length; i++) {

        var dt = $(".dts1")[i].value;
        SpecialDate1 += dt + "^";
        if (dt == "") {
            //$('#lbl_' + $(".dts1")[i].id).css("display", "");
            //bValid = false;
        }
        else {
            $('#lbl_' + $(".dts1")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".dts2").length; i++) {

        var dt = $(".dts2")[i].value;
        SpecialDate2 += dt + "^";
        if (dt == "") {
            //$('#lbl_' + $(".dts2")[i].id).css("display", "");
            //bValid = false;
        }
        else {
            $('#lbl_' + $(".dts2")[i].id).css("display", "none");
        }
    }
    //END Special Dates

    //Block Days
    BlockDays = "";
    if ($("#Mon").is(':checked'))
        BlockDays = "Monday^";

    if ($("#Tue").is(':checked'))
        BlockDays += "Tuesday^";

    if ($("#Wed").is(':checked'))
        BlockDays += "Wednesday^";

    if ($("#Thu").is(':checked'))
        BlockDays += "Thursday^";

    if ($("#Fri").is(':checked'))
        BlockDays += "Friday^";

    if ($("#Sat").is(':checked'))
        BlockDays += "Saturday^";

    if ($("#Sun").is(':checked'))
        BlockDays += "Sunday";
    if (BlockDays == "Monday^Tuesday^Wednesday^Thursday^Friday^Saturday^Sunday") {
        $('#lbl_BlockDays').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_BlockDays').css("display", "none");
    }
    //alert(BlockDays)
    //END Block Days

    //Sid = $("#ddlSeason option:selected").val();

    // Days Prior Fixed Date
    if ($('#rdb_DaysPrior').is(':checked')) {

        $('#lbl_datepicker3').css("display", "none");
        DaysPrior = $("#txtDaysPrior").val();
        if (DaysPrior == "") {
            $('#lbl_txtDaysPrior').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txtDaysPrior').css("display", "none");
        }
    }
    else {
        FixedDate = $("#Fixeddatepicker3").val();
        $('#lbl_txtDaysPrior').css("display", "none");
        if (FixedDate == "") {
            $('#Fixeddatepicker3').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_datepicker3').css("display", "none");
        }
    }
    // END Days Prior Fixed Date

    // Offer Type
    OfferType = $('#ddlOfferType option:selected').val();
    if (OfferType == "Discount") {
        if ($('#rdb_DiscountPer').is(':checked')) {
            DiscountAmount = 0;
            $('#lbl_txt_DiscountAmount').css("display", "none");
            DiscountPer = $("#txtDiscountPer").val();
            if (DiscountPer == "") {
                $('#lbl_txtDiscountPer').css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_txtDiscountPer').css("display", "none");
            }
        }
        else {
            DiscountPer = 0;
            $('#lbl_txtDiscountPer').css("display", "none");
            DiscountAmount = $("#txt_DiscountAmount").val();
            if (DiscountAmount == "") {
                $('#lbl_txt_DiscountAmount').css("display", "");
                bValid = false;
            }
            else {
                $('#lbl_txt_DiscountAmount').css("display", "none");
            }
        }
    }
    else if (OfferType == "Deal") {

        NewRate = $("#txtNewRate").val();
        if (NewRate == "") {
            $('#lbl_txtNewRate').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txtNewRate').css("display", "none");
        }
    }
    else if (OfferType == "Freebi") {

        FreeItemName = $("#txt_ItemName").val();
        if (FreeItemName == "") {
            $('#lbl_txt_ItemName').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txt_ItemName').css("display", "none");
        }
        FreeItemDetail = $("#txt_ItemDetails").val();
        if (FreeItemDetail == "") {
            $('#lbl_txt_ItemDetails').css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_txt_ItemDetails').css("display", "none");
        }
    }
    // END Offer Type
    if (OfferTerm == "") {
        $('#lbl_txtOfferTerm').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_txtOfferTerm').css("display", "none");
    }
    return bValid;
}

function GetAllSeasonsList() {
    var Divs = '';
    $("#tbl_Hotel").dataTable().fnClearTable();
    $("#tbl_Hotel").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "MasterHotelOfferHandler.asmx/GetAllSeasonsList",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var SeasonList = obj.SeasonOffer;
            if (obj.retCode == 1) {
                for (var i = 0; i < SeasonList.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td>' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td>' + SeasonList[i].SeasonName + '</td>'
                    Divs += '<td>' + SeasonList[i].ValidFrom + '</td>'
                    Divs += '<td>' + SeasonList[i].ValidTo + '</td>'
                    Divs += '<td>' + SeasonList[i].OfferType + '</td>'
                    Divs += '<td>' + SeasonList[i].DaysPrior + '</td>'
                    Divs += '<td>' + SeasonList[i].BookBefore + '</td>'
                    Divs += '<td>' + SeasonList[i].DiscountAmount + '</td>'
                    Divs += '<td>' + SeasonList[i].DiscountPercent + '</td>'
                    Divs += '<td>' + SeasonList[i].NewRate + '</td>'
                    Divs += '<td>' + SeasonList[i].DateType + '</td>'
                    //Divs += '<td align="center" style="width:10%"><i style="cursor:pointer" aria-hidden="true" title="Update Details"><label class="icon-pencil icon-size2" onclick="EditOffer(\'' + SeasonList[i].OfferID + '\')" ></label></i> | <i style="cursor:pointer" onclick="Delete(\'' + SeasonList[i].OfferID + '\',\'' + SeasonList[i].Sid + '\')" aria-hidden="true" title="Delete"><label class="icon-trash" ></label></i></td>'
                    Divs += '<td class="align-center">'
                    Divs += '<a href="#" title="Edit" class="button" style="" onclick="EditOffer(\'' + SeasonList[i].OfferID + '\')"><span class="icon-pencil"></span></a>'
                    Divs += '<a style="display:none;" href="#" class="button" title="Delete" onclick="DeleteOffer(\'' + SeasonList[i].OfferID + '\',\'' + SeasonList[i].Sid + '\')"><span class="icon-trash"></span></a>'
                    Divs += '</td>'
                    Divs += '</tr>'
                }
            }
            $("#tbl_Hotel tbody").append(Divs);
            $("#tbl_Hotel").dataTable(
                   {

                       bSort: true, sPaginationType: 'full_numbers',

                   });

        },
    });
}

function EditOffer(OfferID) {
    window.location.href = "MasterOffer.aspx?id=" + OfferID + "";
}

function DeleteOffer(Offerid, sid) {
    $.modal.confirm("Are You Sure you want to Delete this offer ", function () {
        $.ajax({
            type: "POST",
            url: "MasterHotelOfferHandler.asmx/DeleteOffer",
            data: '{"OfferId":"' + Offerid + '","sid":"' + sid + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    Success("Error in deleting ");
                    return false;
                }
                if (result.retCode == 1) {
                    Success("Offer is Deleted");
                    GetAllSeasonsList()
                }
            },
            error: function () {
                Success("Error in deleting ");
            }
        });
    }, function () {
        $('#modals').remove();
    });

}

var SidOffer = "";

function GetOfferByID(id) {
    $.ajax({
        type: "POST",
        url: "MasterHotelOfferHandler.asmx/GetOfferByID",
        data: '{"Id":"' + id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var offer = result.offer;
            if (result.Session == 0) {

                //alert("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                for (var i = 0; i < offer.length; i++) {
                    SidOffer += offer[i].Sid + "^";
                }

                $("#txtHotelOfferCode").val(offer[0].HotelOfferCode);
                $("#txtHotelCode").val(offer[0].CutOfferCode);
                $("#txtOfferTerm").val(offer[0].OfferTerm);
                $("#txtOfferNote").val(offer[0].OfferNote);
                $("#txtSeasonName1").val(offer[0].SeasonName);

                if (offer.length >= 2) {
                    if (offer[1].SeasonName != "")
                        $("#txtSpecialName1").val(offer[1].SeasonName);

                    if (offer[1].ValidFrom != "") {
                        $("#datepickers1").val(offer[1].ValidFrom);
                        $("#datepickers2").val(offer[1].ValidTo);
                    }
                }

                if (offer[0].DaysPrior != "")
                    $("#txtDaysPrior").val(offer[0].DaysPrior);
                else {
                    $("#rdb_FixedDate").prop("checked", true);
                    $("#rdb_DaysPrior").prop("checked", false);
                    $("#DaysPrior").css("display", "none");
                    $("#FixedDate").css("display", "");
                    $("#Fixeddatepicker3").val(offer[0].BookBefore);
                }
                GetOfferType(offer[0].OfferType);

                if (offer[0].OfferType == "Discount") {
                    if (offer[0].DiscountPercent != "")
                        $("#txtDiscountPer").val(offer[0].DiscountPercent);
                    else {
                        $("#rdb_DiscountAmount").prop("checked", true);
                        $("#rdb_DiscountPer").prop("checked", false);
                        $("#DivDiscount").css("display", "none");
                        $("#DivDiscountAmount").css("display", ""); //txt_DiscountAmount
                        $("#txt_DiscountAmount").val(offer[0].DiscountAmount);
                    }
                }
                else if (offer[0].OfferType == "Freebi") {
                    $("#DivDiscount").css("display", "none");
                    $("#DivRdbDiscountPer").css("display", "none");
                    $("#DivDiscountAmount").css("display", "none");
                    $("#DivRdbDiscountAmount").css("display", "none");
                    $("#DivItemName").css("display", "");
                    $("#DivItemDetails").css("display", "");
                    $("#txt_ItemName").val(offer[0].FreeItemName);
                    $("#txt_ItemDetails").val(offer[0].FreeItemDetail);
                }
                else {
                    $("#DivDiscount").css("display", "none");
                    $("#DivRdbDiscountPer").css("display", "none");
                    $("#DivDiscountAmount").css("display", "none");
                    $("#DivRdbDiscountAmount").css("display", "none");
                    $("#DivNewRate").css("display", "");
                    $("#txtNewRate").val(offer[0].NewRate);
                }
                $("#datepicker1").val(offer[0].ValidFrom);
                $("#datepicker2").val(offer[0].ValidTo);

                // $("#select_OfferNationality").val(offer[0].Countryname);


                GetOfferNationality(offer[0].Country, offer[0].Countryname);

                setBlockDays(offer[0].BlockDay);
                var BlockDays = "";
                for (var i = 0; i < $(".chk_blockdays").length; i++) {
                    if ($(".chk_blockdays")[i].checked)
                        BlockDays += $(".chk_blockdays")[i].defaultValue + ";";
                }
            }
        },
        error: function () {
            // alert("Error in deleting ");
        }
    });
}
var pNationality = "";
function GetOfferNationality(Nationality, NationalityName) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var Nationality = Nationality.split(",")

        if (NationalityName == "AllCountry")
            $("#NationalityDiv .select span")[0].textContent = "For All Nationality";
        else
            $("#NationalityDiv .select span")[0].textContent = NationalityName;
        for (var i = 0; i <= Nationality.length - 1; i++) {
            //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
            $('input[value="' + Nationality[i] + '"][class="Onataionality"]').prop("selected", true);
            $("#select_OfferNationality").val(Nationality);
            pNationality += Nationality[i] + ",";
            var selected = [];

        }
    }
    catch (ex)
    { }

}

function GetOfferType(OfferType) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var OfferType = OfferType.split(",")

        $("#div_OfferType .select span")[0].textContent = OfferType;
        for (var i = 0; i < OfferType.length; i++) {
            //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
            $('input[value="' + OfferType + '"][class="OfferType"]').prop("selected", true);
            $("#ddlOfferType").val(OfferType);
            pOfferType += OfferType[i] + ",";
            var selected = [];

        }
    }
    catch (ex)
    { }

}

function setBlockDays(BlockDays) {
    debugger;
    try {
        var BlockD = BlockDays.split("^")
        for (var i = 0; i < BlockD.length - 1; i++) {
            // $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked'); Thursday^Friday^          
            if (BlockD[i] == "Monday")
                $('input[id="Mon"]').prop("checked", true);
            if (BlockD[i] == "Tuesday")
                $('input[id="Tue"]').prop("checked", true);
            if (BlockD[i] == "Wednesday")
                $('input[id="Wed"]').prop("checked", true);
            if (BlockD[i] == "Thursday")
                $('input[id="Thu"]').prop("checked", true);
            if (BlockD[i] == "Friday")
                $('input[id="Fri"]').prop("checked", true);
            if (BlockD[i] == "Saterday")
                $('input[id="Sat"]').prop("checked", true);
            if (BlockD[i] == "Sunday")
                $('input[id="Sun"]').prop("checked", true);
        }
    }
    catch (ex)
    { }


}