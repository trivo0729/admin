﻿
var FN = '';
function sendfiles(id) {
    debugger;
    var s = id.split('UploadVisa')
    var fileUpload = $("#UploadVisa" + s[1]).get(0);
    var files = fileUpload.files;
    FN = files[0].name;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#img_UploadVisa" + s[1]).css("display", "")
    $("#lbl_UploadVisa" + s[1]).css("display", "none")
    $.ajax({
        url: "../OTBUploader.ashx?Otb_id=" + OtbRefNo + "&FileNo=" + id,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only PDF file is allowed") {
                $("#lbl_UploadVisa" + s[1]).css("display", "none")
                $("#img_UploadVisa" + s[1]).css("display", "none")
            }
            else if (result == "Only PDF file is allowed") {
                $("#img_UploadVisa" + s[1]).css("display", "none")
                $("#lbl_UploadVisa" + s[1]).html("Only PDF file is allowed")
                $("#lbl_UploadVisa" + s[1]).css("display", "")
            }
        },
        error: function (err) {
        }
    });
}
var FNIN = '';
var exIn = '';
function sendInbound() {
    debugger;
    var fileUpload = $("#UploadInbound").get(0);
    var files = fileUpload.files;
    FNIN = files[0].name;
    exIn = FNIN.split('.')

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $('#ImgInbound').empty();
    $("#img_UploadInbound").css("display", "")
    $("#lbl_UploadInbound").css("display", "none")
    debugger;
    $.ajax({
        url: "../OTBInUploader.ashx?Otb_id=" + OtbRefNo + "&FileType=" + "InBoundTicket",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only PDF/jpg file is allowed") {
                $("#lbl_UploadInbound").css("display", "none")
                $("#img_UploadInbound").css("display", "none")
                //alert("File Uploaded succesfully!");
            }
            else {
                $("#img_UploadInbound").css("display", "none")
                $("#lbl_UploadInbound").html(result)
                $("#lbl_UploadInbound").css("display", "")
                $("#UploadInbound").val("");
            }

        },

    });

}

var FNOUT = '';
var exOUT = '';
function sendOutbound() {
    debugger;
    var fileUpload = $("#UploadOutbound").get(0);
    var files = fileUpload.files;
    FNOUT = files[0].name;
    exOUT = FNOUT.split('.')

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#img_UploadOutbound").css("display", "")
    $("#lbl_UploadOutbound").css("display", "none")
    $.ajax({
        url: "../OTBOutUploader.ashx?Otb_id=" + OtbRefNo + "&FileType=" + "OutBoundTicket",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only PDF/jpg file is allowed") {
                $("#lbl_UploadOutbound").css("display", "none")
                $("#img_UploadOutbound").css("display", "none")
            }
            else {
                $("#img_UploadOutbound").css("display", "none")
                $("#lbl_UploadOutbound").html(result)
                $("#lbl_UploadOutbound").css("display", "")
                $("#UploadOutbound").val("");
            }
        },

    });

}
function imgError(image) {
    image.onerror = "";
    image.src = "../OTBDocument/icon.png";
    //var tRow = '';
    //tRow += '</tr>';
    //    tRow += '<td style="width:33.33%">Document Not Available</td>';
    //    tRow += '</tr>';
    //    $("#tbl_CurrentImages tbody").html(tRow);
    return true;
}
function Preview() {
    debugger;
    $('#preview').empty();
    var tRow;
    tRow = '<iframe src="../OTBDocument/' + FN + '" width="90%" height="600px"  />';
    $('#preview').append(tRow)
    $('#OTBPreviewModal').modal("show");
}


function InPreview() {
    debugger;
    $('#Inpreview').empty();
    var tRow;
    if (exIn[1] == 'pdf' || exIn[1] == 'PDF') {
        tRow = '<iframe src="../OTBDocument/' + FNIN + '" width="90%" height="600px"  />';
    }
    else {
        tRow = '<iframe src="../OTBDocument/' + FNIN + '" width="400px" height="400px"  />'
    }
    $('#Inpreview').append(tRow)
    $('#InPreviewModal').modal("show");
}

function OutPreview() {
    debugger;
    $('#Outpreview').empty();
    var tRow;
    if (exOUT[1] == 'pdf' || exOUT[1] == 'PDF') {
        tRow = '<iframe src="../OTBDocument/' + FNOUT + '" width="90%" height="600px"  />';
    }
    else {
        tRow = '<iframe src="../OTBDocument/' + FNOUT + '" width="400px" height="400px" />';
    }
    $('#Outpreview').append(tRow)
    $('#OutPreviewModal').modal("show");
}
function GetVisaDetailsByCode(hiddenName) {
    $.ajax({
        type: "POST",
        url: "../OTBHandler.asmx/GetVisaByCode",
        data: '{"VisaCode":"' + hiddenName + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrVisa = result.tbl_Visa;
                $('#txt_FullName').val(arrVisa[0].FirstName + " " + arrVisa[0].MiddleName + " " + arrVisa[0].MiddleName);
                $('#Sel_VisaType option:selected').text(arrVisa[0].IeService);
                $('#Sel_AirLineIn option:selected').text(arrVisa[0].ArrivalAirLine);
                $('#txt_FlightNoIn').val(arrVisa[0].ArrivalflightNo);
                $('#dt_datepickerDdateIn').val(arrVisa[0].ArrivalDate);
                $('#Sel_AirLinesOut option:selected').text(arrVisa[0].DeprtureAirLine);
                $('#txt_FlightNoOut').val(arrVisa[0].DeprtureflightNo);
                //$('#Sel_AirLinesOut').val(arrVisa[0].Deprturefrom);
                $('#td_datepickerDdateOut').val(arrVisa[0].DepartingDate);

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while loading Visa Details")
            $('#ModelMessege').modal('show')
            //alert("An error occured while loading Visa Details")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}