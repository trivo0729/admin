﻿var Ref, UserId, OldStatus;
//function UpdateStatus(RefrenceNo, Uid, OTBStatus) {
//    Ref = RefrenceNo;
//    UserId = Uid;
//    OldStatus = OTBStatus;
//    $("#selOtbStatus").val(OTBStatus);
//    $("#OtbStatusModal").modal("show")
//}
function UpdateStatus(RefrenceNo, Uid, OTBStatus) {
    Ref = RefrenceNo;
    UserId = Uid;
    OldStatus = OTBStatus;
    $.modal({
        content: '<div class="columns" id="OtbStatusModal">' +
                    '<div class="twelve-columns twelve-columns-mobile" id="divOtbStatus">' +
                        '<div class="full-width button-height OtbStatus">' +
                            '<select id="selOtbStatus" class="form-control" onchange="Notification()">' +
                                '<option value="-" selected="selected">--Salect Otb Status--</option>' +
                                '<option value="Processing">Recived</option>' +
                                '<option value="Processing-Applied">Applied</option>' +
                                '<option value="Otb Updated">Approved</option>' +
                                '<option value="Otb Not Required">Not Required</option>' +
                                '<option value="Rejected">Cancelled By Admin</option>' +
                            '</select>' +
                        '</div>' +
                    '</div>' +
                '<p class="text-right" style="text-align:right;">' +
                    '<button type="button" class="button anthracite-gradient" onclick="OtbEmailCOnformation()">Update Status</button>' +
                '</p>',

        title: 'Update Status ',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });

}
function OtbEmailCOnformation() {
    var Status = $("#selOtbStatus").val();
    if (OldStatus != Status) {
        $.ajax({
            type: "POST",
            url: "../Handler/OTBEmailHandler.asmx/UpdateConformation",
            data: '{"RefrenceNo":"' + Ref + '","UserId":"' + UserId + '","Status":"' + Status + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1 && result.MailSend && result.Session == 1) {
                    Success("OtbStatus Updated && Confornation Mail Sent to the Agent")
                }
                else if (result.retCode == 1 && result.MailSend == 0) {
                    Success("Otb Status Updated")
                }
                else if (result.retCode == 0 && result.Session == 0) {
                    Success("An Error Occured During Status Update")
                }
                $("#OtbStatusModal").modal("hide")
                GetAllOtbDetails()
            },
            error: function () {
                Success("An Error Occured During Status Update")
            }
        });
    }
    else {
        Success("Otb Status Not Changed")
    }

}
function OtbMail(OtbRef, uid) {
    $.ajax({
        type: "POST",
        url: "../Handler/OTBEmailHandler.asmx/AirlinesMails",
        data: '{"RefrenceNo":"' + OtbRef + '","UserId":"' + uid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.MailSend && result.Session == 1) {
                Success("OtbStatus Updated && Confornation Mail Sent to the Agent")
            }
            else if (result.retCode == 1 && result.MailSend == 0) {
                Success("Otb Status Updated")
            }
            else if (result.retCode == 0 && result.Session == 0) {
                Success("An Error Occured During Status Update")
            }
            $("#OtbStatusModal").modal("hide")
            GetAllOtbDetails()
        },
        error: function () {
            Success("An Error Occured During Status Update")
        }
    });
}