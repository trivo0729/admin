﻿var globe_urlParamDecoded = 0;
var ImageNameArray = '';
$(document).ready(function () {
    debugger;

    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var url = url[0].split('=%3d');
    hcd = GetQueryStringParams('hcd');
    // original var urlParameter = atob(url[0]).split('=');
    var urlParameter = atob(url[0]).split('=');
    globe_urlParamDecoded = urlParameter[1];
    $('#lst_BasicInformation').on("click", function () { BasicDetails(globe_urlParamDecoded); });
    $('#lst_Pricing').on("click", function () { UpdatePricingDetails(globe_urlParamDecoded); });
    $('#lst_Itinerary').on("click", function () { ItineraryDetails(globe_urlParamDecoded); });
    $('#lst_HotelDetails').on("click", function () { HotelDetails(globe_urlParamDecoded); });
    $('#lst_PackageImages').on("click", function () { PackageImagesImages(globe_urlParamDecoded); });
    GetCurrentImages();
    $('input[id$=hidden_Filed_ID]').val(globe_urlParamDecoded);
});

function GetCurrentImages() {
    debugger;
    $.ajax({
        url: "PackageDetailHandler.asmx/GetProductImages",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
            }
            else if (result.retCode == 1) {
                var List_ImageDetail = result.List_ImageDetail[0].ImageArray.split("^_^");
                var tRow = '';
                tRow += '<tr>';
                debugger;
                for (i = 0; i < List_ImageDetail.length; i++) {
                    if (List_ImageDetail[i] != "" && List_ImageDetail[i] != undefined && List_ImageDetail[i] != null) {
                        tRow += '<td><img src="../Admin/thumbnail.aspx?fn=' + List_ImageDetail[i] + '&w=342&h=342&productfolder=' + globe_urlParamDecoded + '&rf=" class="poductImage" style="     width: 100px; max-height: 240px;"/></td>';
                    }
                }
                tRow += '</tr>';
                $("#tbl_CurrentImages tbody").html(tRow);
            }
        },
        error: function () {
        }
    });
}


function checkFileExtension(file) {
    var flag = true;
    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'JPG':
            case 'JPEG':
            case 'PNG':
            case 'GIF':
                flag = true;
                break;
            default:
                flag = false;
        }
    }
    return flag;
}

function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}
function AddPackageImage() {
    debugger;
    $("#tbl_AddPackageImages tbody tr td label").css("display", "none");
    var nProductID = globe_urlParamDecoded;
    var bValid = true;
    var subImageIDs = "";
    var subImageIDArray = null;
    var subImageValues = "";
    var subImagesValueArray = null;
    var image_0 = getNameFromPath($("#image_0").val());
    var image_1 = getNameFromPath($("#image_1").val());
    var image_2 = getNameFromPath($("#image_2").val());
    var image_3 = getNameFromPath($("#image_3").val());
    var image_4 = getNameFromPath($("#image_4").val());
    if (image_0 == null) {
        bValid = false;
        $("#lblerr_sPackageimage_0").css("display", "");
    }
    else if (!(checkFileExtension(image_0))) {
        bValid = false;
        $("#lblerr_sPackageimage_0").css("display", "");
    }
    else if (image_1 != null) {
        if (!(checkFileExtension(image_1))) {
            bValid = false;
            $("#lblerr_sPackageimage_1").css("display", "");
        }
    }
    else if (image_2 != null) {
        if (!(checkFileExtension(image_2))) {
            bValid = false;
            $("#lblerr_sPackageimage_2").css("display", "");
        }
    }
    else if (image_3 != null) {
        if (!(checkFileExtension(image_3))) {
            bValid = false;
            $("#lblerr_sPackageimage_3").css("display", "");
        }
    }
    else if (image_4 != null) {
        if (!(checkFileExtension(image_4))) {
            bValid = false;
            $("#lblerr_sPackageimage_4").css("display", "");
        }
    }
    var imageIds = [];
    if (image_0 != null) {
        imageIds.push("image_0");
    }
    if (image_1 != null) {
        imageIds.push("image_1");
    }
    if (image_2 != null) {
        imageIds.push("image_2");
    }
    if (image_3 != null) {
        imageIds.push("image_3");
    }
    if (image_4 != null) {
        imageIds.push("image_4");
    }
    //subImageValues = subImageValues.substring(0, subImageValues.length - 1);
    //subImageIDArray = subImageIDs.split(",");
    //subImagesValueArray = subImageValues.split(",");
    //var image_1_Extention = image_1.substring(image_1.lastIndexOf(".") + 1);
    //var MainImageName = "Image_1" + "." + image_1_Extention;
    //ajaxFileUpload(nProductID, MainImageName, 'image_1', false);
    var imageCount = 1;
    ImageNameArray = '';
    if (bValid == true) {
        for (var i = 0; i < imageIds.length; i++) {
            var sProductImage = getNameFromPath($("#" + imageIds[i]).val());
            var extention = sProductImage.substring(sProductImage.lastIndexOf(".") + 1);
            var sImageName = "Image_" + i + "." + extention;
            if (imageCount == imageIds.length) {
                ImageNameArray += sImageName;
                ajaxFileUpload(nProductID, sImageName, imageIds[i], true);
            }
            else {
                ImageNameArray += sImageName + "^_^";
                ajaxFileUpload(nProductID, sImageName, imageIds[i], false);
            }
            imageCount++;
        }
    }
}

function ajaxFileUpload(FileFolder, filename, ImageID, bIsLastImage) {
    debugger;
    $.ajaxFileUpload({
        url: 'FileUpload.ashx?id=' + FileFolder + '&filename=' + filename,
        secureuri: false,
        fileElementId: ImageID,
        dataType: 'json',
        success: function (data, status) {
            if (bIsLastImage == true) {
                $("#tbl_AddPackageImages tbody tr td label").css("display", "none");
                add_ProductImagesData(ImageNameArray);
            }
            if (typeof (data.error) != 'undefined') {
                if (data.error != '') {
                    alert(data.error);
                } else {

                }
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function add_ProductImagesData(nImageNameArray) {
    debugger;
    $.ajax({
        url: "PackageDetailHandler.asmx/add_ProductImagesData",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '","arr_Image":"' + nImageNameArray + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
            }
            else if (result.retCode == 1) {
                GetCurrentImages();
                alert("Images updated Successfully.");
            }
        },
        error: function () {
        }
    });
}
function UpdatePricingDetails(nID) {
    $(location).attr('href', '../Admin/PricingDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function HotelDetails(nID) {
    $(location).attr('href', '../Admin/UpdateHotelDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function BasicDetails(nID) {
    $(location).attr('href', '../Admin/PackageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function ItineraryDetails(nID) {
    $(location).attr('href', '../Admin/ItineraryDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function PackageImagesImages(nID) {
    $(location).attr('href', '../Admin/PackageImageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}


function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}