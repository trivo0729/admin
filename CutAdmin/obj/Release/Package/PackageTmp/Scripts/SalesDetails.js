﻿
var hiddensid;



$(document).ready(function () {

    GetAllSalesPerson()

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                if (arrCountry.length > 0) {

                    $("#selTeritoryCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selTeritoryCountry").append(ddlRequest);


                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });

    $('#selTeritoryCountry').change(function () {
        var sndcountry = $('#selTeritoryCountry').val();
        GetTeritoryCity(sndcountry);
    });

});


function GetTeritoryCity(country) {

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCity",
        data: '{"country":"' + country + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.CityList;
                if (arrCity.length > 0) {
                    $("#selTeritoryCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '">' + arrCity[i].Description + '</option>';
                    }
                    $("#selTeritoryCity").append(ddlRequest);
                }
            }
            if (result.retCode == 0) {
                $("#selTeritoryCity").empty();
            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}

var TCountry = "";
var TState = "";
var TCity = "";
var TAgents = "";
var Password = "";
function GetAllSalesPerson() {
    $("#tbl_SalesDetails").dataTable().fnClearTable();
    $("#tbl_SalesDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetAllSalesPerson",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_SalesDetails = result.Arr;
                TCountry = result.TConutry;
                TState = result.Tstates;
                TCity = result.TCities;
                TAgents = result.TAgents;
                Password = result.Password;
                var Pass = Password.split(',');
                var tRow = "";

                var input = "\/Date(1458845940000)\/";
                for (var i = 0; i < List_SalesDetails.length; i++) {

                    tRow += '<tr>';

                    tRow += '<td style="width:5%">' + (i + 1) + '</td>';
                    //tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#AgencyDetailModal" onclick="AgentDetailsModal(\'' + List_SalesDetails[i].AgencyName + '\',\'' + List_SalesDetails[i].dtLastAccess + '\',\'' + List_SalesDetails[i].ContactPerson + '\',\'' + List_SalesDetails[i].Designation + '\',\'' + List_SalesDetails[i].Address + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].PinCode + '\',\'' + List_SalesDetails[i].phone + '\',\'' + List_SalesDetails[i].Mobile + '\',\'' + List_SalesDetails[i].Fax + '\',\'' + List_SalesDetails[i].email + '\',\'' + List_SalesDetails[i].Website + '\',\'' + List_SalesDetails[i].PANNo + '\',\'' + List_SalesDetails[i].Agentuniquecode + '\',\'' + List_SalesDetails[i].CurrencyCode + '\',\'' + List_SalesDetails[i].GSTNumber + '\',\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].Code + '\',\'' + List_SalesDetails[i].agentCategory + '\',\'' + List_SalesDetails[i].IATANumber + '\',\'' + List_SalesDetails[i].UserType + '\',\'' + List_SalesDetails[i].ServiceTaxNO + '\',\'' + List_SalesDetails[i].Remarks + '\',\'' + List_SalesDetails[i].password + '\',\'' + List_SalesDetails[i].dDateofJoining + '\',\'' + List_SalesDetails[i].ValidationCode + '\',\'' + List_SalesDetails[i].Validity + '\',\'' + List_SalesDetails[i].MerchantID + '\',\'' + List_SalesDetails[i].ParentID + '\',\'' + List_SalesDetails[i].LoginFlag + '\'); return false" title="Click to view Agency Details">' + List_SalesDetails[i].AgencyName + '</a></td>';
                    tRow += '<th scope="row">' + List_SalesDetails[i].Name + '</th>';
                    tRow += '<td  data-title="Email"><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].email + '\',\'' + Pass[i] + '\'); return false" title="Click to edit Password">' + List_SalesDetails[i].email + '</a></td>';
                    tRow += '<td  data-title="Territory">'
                    if (List_SalesDetails[i].Territory == "Country") {
                        tRow += '<a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].CountryID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "State") {
                        tRow += '<a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].StateID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "City") {
                        tRow += '<a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].CitiesID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "Agency") {
                        tRow += '<a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].Agents + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    //tRow += '<td>' + List_SalesDetails[i].Territory + '</td>';

                    //tRow += '<td align="center"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateSales(\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].FirstName + '\',\'' + List_SalesDetails[i].MiddleName + '\',\'' + List_SalesDetails[i].LastName + '\',\'' + List_SalesDetails[i].department + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].Address + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',)"></span></a></td>';
                    tRow += '<td data-title="Update" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateSales(\'' + List_SalesDetails[i].sid + '\')"></span></a></td>';
                    tRow += '<td data-title="Delete" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteSales(\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].ContactID + '\',\'' + List_SalesDetails[i].ContactPerson + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_SalesDetails tbody").html(tRow);
                $("#tbl_SalesDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_SalesDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_SalesDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_SalesDetails").append(tRow);

                $("#tbl_SalesDetails").dataTable({
                     bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}



function UpdateSales(ID) {

    window.location.href = "AddSalesPerson.aspx?ID=" + ID;

    //UpdateUrl = "AddSalesPerson.aspx?sid=" + sid + "&sAgencyName=" + AgencyName + "&sContactPerson=" + ContactPerson + "&sDesignation=" + sDesignation + "&sAddress=" + Address + "&sCity=" + CityCode + "&sCountry=" + Countryname + "&nPinCode=" + PinCode + "&sEmail=" + email + "&nPhone=" + phone + "&nMobile=" + Mobile + "&nFax=" + Fax + "&IATANumber=" + IATANumber + "&sUsertype=" + UserType + "&nServiceTaxNumber=" + ServiceTaxNO + "&nPAN=" + PANNo + "&sWebsite=" + Website + "&sGroup=" + Group + "&sRemarks=" + Remarks + "&sPassword=" + password + "&dDateofJoining=" + DateofJoining + "&sUniqueCode=" + Agentuniquecode + "&sValidationCode=" + ValidationCode + "&sValidity=" + Validity + "&sMerchantID=" + MerchantID + "&nParentID=" + ParentID + "&bLoginFlag=" + LoginFlag + "&sPreferredCurrency=" + CurrencyCode + "&GSTNumber=" + GSTNumber + "";
}
function DeleteSales(uid, contactId, name) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + contactId + '  ' + name + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/SalesHandler.asmx/DeleteSalesPerson",
                type: "post",
                data: '{"uid":"' + uid + '","contactId":"' + contactId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Sales Person has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}



function Search() {

    $("#tbl_SalesDetails").dataTable().fnClearTable();
    $("#tbl_SalesDetails").dataTable().fnDestroy();


    var Name = $("#txt_Name").val();
    var Territory = $("#selTerritory").val();
    //var sTerCity = $("#selTeritoryCity").val();
    //var sTerCountry = $("#selTeritoryCountry").val();

    var data = {
        Name: Name,
        Territory: Territory
        //sTerCity: sTerCity,
        //sTerCountry: sTerCountry

    }

    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/SearchSales",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {


                var List_SalesDetails = result.List_Sales;
                Password = result.Password;
                var Pass = Password.split(',');
                var tRow = "";
                //if (result.Usertype == "Admin" || result.Usertype == "AdminStaff") {
                //    $("#th_Franchisee").css("display", "")
                //    GetFranchisee()
                //}
                var input = "\/Date(1458845940000)\/";
                for (var i = 0; i < List_SalesDetails.length; i++) {

                    tRow += '<tr>';

                    tRow += '<td style="width:5%">' + (i + 1) + '</td>';
                    //tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#AgencyDetailModal" onclick="AgentDetailsModal(\'' + List_SalesDetails[i].AgencyName + '\',\'' + List_SalesDetails[i].dtLastAccess + '\',\'' + List_SalesDetails[i].ContactPerson + '\',\'' + List_SalesDetails[i].Designation + '\',\'' + List_SalesDetails[i].Address + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].PinCode + '\',\'' + List_SalesDetails[i].phone + '\',\'' + List_SalesDetails[i].Mobile + '\',\'' + List_SalesDetails[i].Fax + '\',\'' + List_SalesDetails[i].email + '\',\'' + List_SalesDetails[i].Website + '\',\'' + List_SalesDetails[i].PANNo + '\',\'' + List_SalesDetails[i].Agentuniquecode + '\',\'' + List_SalesDetails[i].CurrencyCode + '\',\'' + List_SalesDetails[i].GSTNumber + '\',\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].Code + '\',\'' + List_SalesDetails[i].agentCategory + '\',\'' + List_SalesDetails[i].IATANumber + '\',\'' + List_SalesDetails[i].UserType + '\',\'' + List_SalesDetails[i].ServiceTaxNO + '\',\'' + List_SalesDetails[i].Remarks + '\',\'' + List_SalesDetails[i].password + '\',\'' + List_SalesDetails[i].dDateofJoining + '\',\'' + List_SalesDetails[i].ValidationCode + '\',\'' + List_SalesDetails[i].Validity + '\',\'' + List_SalesDetails[i].MerchantID + '\',\'' + List_SalesDetails[i].ParentID + '\',\'' + List_SalesDetails[i].LoginFlag + '\'); return false" title="Click to view Agency Details">' + List_SalesDetails[i].AgencyName + '</a></td>';
                    tRow += '<td>' + List_SalesDetails[i].Name + '</td>';
                    tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].email + '\',\'' + Pass[i] + '\'); return false" title="Click to edit Password">' + List_SalesDetails[i].email + '</a></td>';
                    if (List_SalesDetails[i].Territory == "Country") {
                        tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].CountryID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "State") {
                        tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].StateID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "City") {
                        tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].CitiesID + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    else if (List_SalesDetails[i].Territory == "Agency") {
                        tRow += '<td><a style="cursor:pointer" data-toggle="modal" data-target="#TerritoryDetailModal" onclick="GetTerritoryDetail(\'' + List_SalesDetails[i].Agents + '\',\'' + List_SalesDetails[i].Territory + '\'); return false" title="Territory Details">' + List_SalesDetails[i].Territory + '</a></td>';
                    }
                    //tRow += '<td align="center"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateSales(\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].FirstName + '\',\'' + List_SalesDetails[i].MiddleName + '\',\'' + List_SalesDetails[i].LastName + '\',\'' + List_SalesDetails[i].department + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].Address + '\',\'' + List_SalesDetails[i].Country + '\',\'' + List_SalesDetails[i].Description + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',\'' + List_SalesDetails[i].ID + '\',)"></span></a></td>';
                    tRow += '<td align="center"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateSales(\'' + List_SalesDetails[i].sid + '\')"></span></a></td>';
                    tRow += '<td align="center"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteSales(\'' + List_SalesDetails[i].sid + '\',\'' + List_SalesDetails[i].ContactID + '\',\'' + List_SalesDetails[i].Name + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_SalesDetails tbody").html(tRow);
                $("#tbl_SalesDetails").dataTable({
                    "bSort": false,
                    "bAutoWidth": false
                });
                document.getElementById("tbl_SalesDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_SalesDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" colspan="6" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_SalesDetails").append(tRow);

                //$("#tbl_AgentDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
            //Success("An error occured while loading details.")
        }
    });

}


function PasswordModal(sid, uid, password) {
    $('#lbl_ErrPassword').css("display", "none");
    hiddensid = sid;
    MyMail = uid;
    $("#txt_SalesId").val(uid);
    $("#txt_Password").val(password);
    $("#hddn_Password").val(password);
    ///GetPassword(password);
    //$("#txt_Password").val(password);
}

function PasswordModal(sid, uid, password) {
    $('#lbl_ErrPassword').css("display", "none");
    hiddensid = sid;
    MyMail = uid;
    GetPassword(sid, uid, password);
};

function GetPassword(sid, uid, password) {
    $("#txt_Password").val('');
 
                $.modal({
                    content: '<div class="modal-body">' +
                      '<div class="scrollingDiv">' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">User ID</div>' +
                      '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_SalesId" value="' + uid + '" class="input-unstyled full-width" readonly="readonly" type="text"></div></div></div> ' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">Password</div>' +
                      '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + password + '" class="input-unstyled full-width" type="text"><input name="prompt-value" id="hddn_Password" value="' + password + '" class="input-unstyled full-width" type="hidden"></div></div> ' +
                      '</div>' +
                      '<div class="columns">' +
                      '<div class="three-columns bold">&nbsp;</div>' +
                      '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="SendPassword()">Email</button>&nbsp;' +
                      '<button type="button" class="button anthracite-gradient" onclick="ChangePassword()">Change&nbsp;Password</button></div>' +
                      '</div></div></div>',
                    title: 'Edit Password',
                    width: 450,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        //'Close': {
                        //    classes: 'huge anthracite-gradient glossy full-width',
                        //    click: function (win) { win.closeModal(); }
                        //}
                    },
                    buttonsLowPadding: true
                });
}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $('#lbl_ErrPassword').css("display", "none");
            $.modal({
                content: '<div class="modal-body">' +
                  '<div class="scrollingDiv">' +
                  '<div class="columns">' +
                  '<div class="twelve-columns bold">Are you sure you want to change password?</div>' +
                  '</div>' +
                  '<div class="columns">' +
                  '<div class="nine-columns bold">&nbsp;</div>' +
                  '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePass()">Ok</button>' +
                  '</div></div></div>',
                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
            //Ok("Are you sure you want to change password?", "ChangePass", null)
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.alert('No Change found in Password!', {
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient glossy displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
    }

}

function ChangePass() {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/SalesChangePassword",
        data: '{"sid":"' + hiddensid + '","password":"' + $('#txt_Password').val() + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session != 1) {
                alert("Session Expired");
            }
            if (result.retCode == 1) {
                $("#PasswordModal").modal('hide');
                //Success("Password changed successfully");
                alert("Password changed successfully");

                window.location.reload();

            }
            if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
        }
    });
}



function GetTerritoryDetail(Territory, TerritoryType) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetTerritoryDetail",
        data: '{"Territory":"' + Territory + '","TerritoryType":"' + TerritoryType + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            TCountry = result.TConutry;
            TState = result.Tstates;
            TCity = result.TCities;
            TAgents = result.TAgents;
            if (result.Session != 1) {
                alert("Session Expired");
            }
            if (result.retCode == 1) {
                if (TerritoryType == "Country") {
                    TerritoryDetailModal(TerritoryType, TCountry)
                }
                else if (TerritoryType == "State") {
                    TerritoryDetailModal(TerritoryType, TState)
                }
                else if (TerritoryType == "City") {
                    TerritoryDetailModal(TerritoryType, TCity)
                }
                else {
                    TerritoryDetailModal(TerritoryType, TAgents)
                }
            }
            if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
        }
    });
}

function TerritoryDetailModal(Territory, TerList) {
    var List = [];
    List = TerList.split(',');
    var tRow = "";
    for (var i = 0; i < List.length - 1; i++) {

        tRow += '<tr>';

        tRow += '<td style="width:1%">' + (i + 1) + '</td>';

        tRow += '<td>' + List[i] + '</td>';

        tRow += '</tr>';
    }
    OpenModel(tRow);
  
};

function OpenModel(tRow) {
    $.modal({
        content: '<div class="respTable">' +
           '<table class="table responsive-table evenodd" id="sorting-advanced">' +
           '<thead>' +
                '<tr>' +
                    '<th scope="col">S.No</th>' +
                    '<th scope="col">Name</th>' +
                ' </tr>' +
            '</thead>' +
            ' <tbody id="TerritoryDetailModal">' +
            '"' + tRow + '" ' +
            ' </tbody>' +
           '</table>' +
           '</div>',
        title: 'State',
        width: 600,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function SendPassword() {
    $('#lbl_ErrEmailId').css("display", "none");
    $('#lbl_ErrEmailId').html("* This field is required");
    $('#lbl_ErrsendPassword').css("display", "none");
    var emailReg = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    var bvalid = true;
    var email = $("#txt_SalesId").val();
    var password = $("#txt_Password").val();
    if (email == "") {
        $('#lbl_ErrEmailId').css("display", "");
        bvalid = false;
    }
    else if (email != "" && !(emailReg.test(email))) {
        $('#lbl_ErrEmailId').css("display", "");
        $('#lbl_ErrEmailId').html("Invalid Email");
        bvalid = false;
    }
    if (bvalid == true) {
        $.modal({
            content: '<div class="modal-body">' +
              '<div class="scrollingDiv">' +
              '<div class="columns">' +
              '<div class="twelve-columns bold">Are you sure you want to send password?</div>' +
              '</div>' +
              '<div class="columns">' +
              '<div class="nine-columns bold">&nbsp;</div>' +
              '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="Send(\'' + email + '\')">Ok</button>' +
              '</div></div></div>',
            width: 300,
            scrolling: false,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient glossy displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
        //Ok("Are you sure you want to send password?", "Send", [email])
    }
}
function Send(email) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/MailPassword",
        data: '{"sEmail":"' + MyMail + '","sTo":"' + email + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                Success("Password Send to mail.");
                //$("#SendPasswordModal").modal('hide');
                $("#PasswordModal").modal('hide');

            }
        },
        error: function () {
            Success("An error occured while sending password.");
        }
    });
    Cancel()
}
