﻿var HotelCode = ''; var HotelName = ''; var arrRoomList = []; var arrAmenities = []; var arrRoomTypes = [];

$(function () {
    HotelCode = GetQueryStringParams('sHotelID')
    HotelName = GetQueryStringParams('HName')
    HotelName = HotelName.replace(/%20/g, ' ')
    $('#HtlName').text(HotelName)
    getRooms();
})

function getRooms() {
    var sHotelId = HotelCode

    $.ajax({
        type: "POST",
        url: "RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + sHotelId + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //arrHotellist = result.MappedHotelList;
                arrRoomTypes = result.RoomTypeList;
                arrRoomList = result.RoomList;
                arrAmenities = result.RoomAmenityList;
                $("#tbl_RoomList tbody").empty();
                //Hotels List
                var trRequest = "";
                if (arrRoomList.length > 0) {
                    for (i = 0; i < arrRoomList.length; i++) {
                        trRequest += '<tr>';
                        for (j = 0; j < arrRoomTypes.length; j++) {

                            if (arrRoomList[i].RoomTypeId == arrRoomTypes[j].RoomTypeID) {
                                trRequest += '<td >' + arrRoomTypes[j].RoomType + '</td>';
                                trRequest += '<td>' + arrRoomList[i].MaxOccupacy + '</td>';
                                trRequest += '<td>';
                                trRequest += '<ul class="version-history">';
                                trRequest += '<li>Room Size:<b>' + arrRoomList[i].RoomSize + '</b></li>';
                                trRequest += '<li>Bedding Type:<b>' + arrRoomList[i].BeddingType + '</b></li>';
                                trRequest += '<li>Smoking Allowed:<b>' + arrRoomList[i].SmokingAllowed + '</b></li>';
                                trRequest += '<li>Extra Beds Allowed:<b>' + arrRoomList[i].MaxExtrabedAllowed + '</b></li>';
                                trRequest += '<li>Childs Allowed:<b>' + arrRoomList[i].MaxChildAllowed + '</b></li>';
                                trRequest += '</ul>';
                                trRequest += '</td>';
                                trRequest += '<td></td>';
                                trRequest += '<td class="low-padding" ><span class="select compact full-width" tabindex="0">';
                                trRequest += '<a href="#" class="select-value">Manage</a><span class="select-arrow"></span>'
                                trRequest += '<span class="drop-down">';
                                trRequest += '<a href="AddRooms.aspx?HotelCode=' + HotelCode + ' &HotelName=' + HotelName + '&RoomId=' + arrRoomList[i].RoomId + '">Edit</a>';
                                trRequest += '<a href="SeasonList.aspx?HotelCode=' + HotelCode + ' &HotelName=' + HotelName + '&RoomId=' + arrRoomList[i].RoomId + '">Season List</a>';
                                trRequest += '</span></span></td>';
                            }

                        }
                        trRequest += '</tr>';

                    }
                    trRequest += '</tbody>';
                    $("#tbl_RoomList").append(trRequest);
                }
                $("#tbl_RoomList").dataTable(
                    {
                        "bPaginate": true,
                    });
            }
            else if (result.retCode == 0) {
                $("#tbl_RoomList tbody").remove();
                var trRequest = '<tbody>';
                trRequest += '<tr><td align="center" style="padding-top: 2%" colspan="8"><span><b>No record found</b></span></td></tr>';
                trRequest += '</tbody>';
                $("#tbl_RoomList").append(trRequest);
            }

        }

    });
}

function NewRoom() {
    window.location.href = "AddRooms.aspx?sHotelID=" + HotelCode + "&HName=" + HotelName;
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}