﻿var globe_ItineraryCount = 0;
var globe_urlParamDecoded = 0;
var global_CategoryCount = 0;
var global_HotelItinerary = [];
var hcd;
$(document).ready(function () {
    debugger;
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var urlParameter = atob(url[0]).split('=');
    globe_urlParamDecoded = urlParameter[1];
    hcd = GetQueryStringParams('hcd');

    $('#lst_BasicInformation').on("click", function () { BasicDetails(globe_urlParamDecoded); });
    $('#lst_Pricing').on("click", function () { UpdatePricingDetails(globe_urlParamDecoded); });
    $('#lst_Itinerary').on("click", function () { ItineraryDetails(globe_urlParamDecoded); });
    $('#lst_HotelDetails').on("click", function () { HotelDetails(globe_urlParamDecoded); });
    $('#lst_PackageImages').on("click", function () { PackageImagesImages(globe_urlParamDecoded); });
    $('#hdnHCode').val(hcd);
    $.ajax({
        url: "PackageDetailHandler.asmx/GetItineraryHotelDetail",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                globe_ItineraryCount = result.List_ItineraryHotelDetail[0].nDuration;
                global_CategoryCount = result.nCount;
                global_HotelItinerary = result.List_ItineraryHotelDetail
                debugger;
                CreateHotelTabs(global_HotelItinerary);
            }
        },
        error: function () {
        }
    });
});

function UpdatePricingDetails(nID) {
    $(location).attr('href', '../Admin/PricingDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function HotelDetails(nID) {
    $(location).attr('href', '../Admin/UpdateHotelDetails.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}

function BasicDetails(nID) {
    $(location).attr('href', '../Admin/PackageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function ItineraryDetails(nID) {
    $(location).attr('href', '../Admin/ItineraryDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function PackageImagesImages(nID) {
    $(location).attr('href', '../Admin/PackageImageDetail.aspx?' + btoa('nID=' + nID) + '&hcd=' + hcd);
}
function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function checkFileExtension(file) {
    var flag = true;
    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'JPG':
            case 'JPEG':
            case 'PNG':
            case 'GIF':
                flag = true;
                break;
            default:
                flag = false;
        }
    }
    return flag;
}

function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}
//function ajaxFileUpload(FileFolder, filename, ImageID, nCategoryID, sImageNo) {
//    $.ajaxFileUpload({
//        url: 'FileUpload.ashx',
//        secureuri: false,
//        data: { id: FileFolder, filename: filename, nCategoryName: GetCategoryName(nCategoryID), imageIndex: sImageNo + 1, nCategoryID: nCategoryID },
//        fileElementId: ImageID,
//        dataType: 'json',
//        success: function (data, status) {
//            $("#img_" + nCategoryID + "_" + sImageNo).attr("width", "192px");
//            $("#img_" + nCategoryID + "_" + sImageNo).attr("height", "192px");
//            var productfolder = FileFolder + '//' + GetCategoryName(nCategoryID);
//            $("#img_" + nCategoryID + "_" + sImageNo).attr("src", "");
//            $("#img_" + nCategoryID + "_" + sImageNo).attr("src", 'HotelThumnail.aspx?fn=' + data.upfile + '&w=192&h=192&productfolder=' + productfolder + '&rf=');
//            if (typeof (data.error) != 'undefined') {
//                if (data.error != '') {
//                    alert(data.error);
//                } else {

//                }
//            }
//        },
//        error: function (data, status, e) {
//            alert(e);
//        }
//    });
//}
function ajaxFileUpload(FileFolder, filename, ImageID, CategoryID, sImageNo) {
    debugger;
    $.ajaxFileUpload({
        url: '../FileUpload.ashx',
        secureuri: false,
        data: { id: FileFolder, filename: filename, nCategoryName: GetCategoryName(CategoryID), imageIndex: sImageNo + 1, nCategoryID: CategoryID },
        fileElementId: ImageID,
        dataType: 'json',
        success: function (data, status) {
            $("#img_" + CategoryID + "_" + sImageNo).attr("width", "192px");
            $("#img_" + CategoryID + "_" + sImageNo).attr("height", "192px");
            var productfolder = FileFolder + '//' + GetCategoryName(CategoryID);
            debugger;
            $("#img_" + CategoryID + "_" + sImageNo).attr("src", '../Admin/HotelThumnail.aspx?fn=' + data.upfile + '&w=192&h=192&productfolder=' + productfolder);
            if (typeof (data.error) != 'undefined') {
                if (data.error != '') {
                    alert(data.error);
                } else {

                }
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function AddHotelImage(nProductID, nCategoryID, sImageNo, nImageID) {
    debugger;
    var sValue = $("#" + nImageID).val();
    var sImageName = getNameFromPath(sValue);
    var bValidImage = checkFileExtension(sImageName);
    var extention = sImageName.substring(sImageName.lastIndexOf(".") + 1);
    var sNewImageName = nImageID + "." + extention;
    if (sValue == "") {
        alert("Please select an Image");
        return false;
    }
    else if (bValidImage == false) {
        alert("Not a valid image.");
        return false;
    }
    ajaxFileUpload(nProductID, sNewImageName, nImageID, nCategoryID, sImageNo);
}
function GetHotelName(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelName_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelName_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelName_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelName_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelName_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelName_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelName_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelName_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelName_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelName_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelName_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelName_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelName_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelName_14;
    }
}

function GetHotelCode(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotel_Code_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotel_Code_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotel_Code_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotel_Code_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotel_Code_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotel_Code_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotel_Code_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotel_Code_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotel_Code_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotel_Code_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotel_Code_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotel_Code_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotel_Code_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotel_Code_14;
    }
}

function GetHotelDescription(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelDescrption_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelDescrption_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelDescrption_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelDescrption_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelDescrption_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelDescrption_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelDescrption_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelDescrption_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelDescrption_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelDescrption_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelDescrption_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelDescrption_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelDescrption_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelDescrption_14;
    }
}

function GetHotelImages(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelImage1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelImage2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelImage3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelImage4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelImage5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelImage6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelImage7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelImage8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelImage9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelImage10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelImage11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelImage12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelImage13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelImage14;
    }
}

function CreateHotelTabs(HotelItinerary) {
    debugger;
    var sTabWidth = 100 / (global_CategoryCount);
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    var nCount = HotelItinerary[0].nDuration;
    for (var k = 0; k < (global_CategoryCount) ; k++) {
        var sTabID = GetCategoryName(HotelItinerary[k].nCategoryID);
        if (k == 0) {
            sclass = "active";
            sDisplay = ";display:block";

        }
        else {
            sclass = "";
            sDisplay = ";";
        }
        sTabRow += '<li onclick="mySelectUpdate()" id="li' + sTabID + '" class="' + sclass + '" style="width:' + sTabWidth + '%' + sDisplay + '"><a data-toggle="tab" href="#Tab' + sTabID + '"><span class=""></span><span class="hidetext">' + sTabID + '</span>&nbsp;</a></li>';
        sTabRowContent += '<div class="tab-pane ' + sclass + '" id="Tab' + sTabID + '">';
        sTabRowContent += '<table id="tbl_AddHotelImage' + sTabID + '" class="table" style="float: right">';
        for (var i = 0; i < nCount ; i++) {
            var sHotelName = GetHotelName(k, i);
            var HotelCode = GetHotelCode(k, i);
            var sHotelDescription = GetHotelDescription(k, i)
            if (sHotelName == null || sHotelName == undefined) {
                sHotelName = "";
            }
            if (sHotelDescription == null || sHotelDescription == undefined) {
                sHotelDescription = "";
            }
            sTabRowContent += '<tr>';
            sTabRowContent += '<td style="width:10%;padding-top:2%" ><b>Day ' + (i + 1) + '</b><br /><br /><br /><img title="" alt="" src="../images/upload-image.png" id="img_' + HotelItinerary[k].nCategoryID + '_' + i + '" /><input type="file" name="uploaded" class="custom-file-input" style="width:192px;height:40px;margin-top:5px" id="' + HotelItinerary[k].nCategoryID + '_' + i + '" name="' + HotelItinerary[k].nCategoryID + '_' + i + '" onchange="AddHotelImage(' + globe_urlParamDecoded + ',' + HotelItinerary[k].nCategoryID + ',' + i + ', \'' + HotelItinerary[k].nCategoryID + '_' + i + '\');"/></td>';
            sTabRowContent += '<td><input type="text" value="' + sHotelName + '" class="form-control margtop5" id="txt_HotelName_' + sTabID + '_' + i + '"  style="width:50%;display:inline;float:left" placeholder="Hotel Name" onfocus="AutoComplete(this.id)"/> <input type="hidden" value="' + HotelCode + '" class="form-control margtop5" id="txtHidden_HotelCode_' + sTabID + '_' + i + '"  style="width:50%;display:inline;float:left"><br/><br/><br/> <textarea cols="80" rows="10" id="txt_HotelDescription_' + sTabID + '_' + i + '" name="content">' + sHotelDescription + '</textarea></td>';
            sTabRowContent += '</tr>';
        }
        sTabRowContent += '</table></div></div>';
    }
    $("#sHotelTabs").html(sTabRow);
    $("#div_HotelTabContent").html(sTabRowContent);
    //$("#tbl_AddPackagePrice tbody").html(tRow);
    for (var k = 0; k < global_CategoryCount; k++) {
        for (var j = 0; j < nCount ; j++) {
            $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("width", "192px");
            $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("height", "192px");
            var productfolder = HotelItinerary[k].nPackageID + '//' + GetCategoryName(HotelItinerary[k].nCategoryID);
            debugger;
            var sHotelImagename = GetHotelImages(k, j);
            if (sHotelImagename == "" || sHotelImagename == null || sHotelImagename == undefined) {
                $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("src", '../images/upload-image.png');
            }
            else {
                $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("src", 'HotelThumnail.aspx?fn=' + sHotelImagename + '&w=192&h=192&productfolder=' + productfolder + '&rf=');
            }
            $('#txt_HotelDescription_' + GetCategoryName(HotelItinerary[k].nCategoryID) + '_' + j).ckeditor();
        }
    }
}

function SaveHotelDetails() {

    var HotelCod = "";

    for (var k = 0; k < global_CategoryCount; k++) {
        for (var i = 0; i < globe_ItineraryCount; i++) {
            var sHotelName = $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            HotelCode = $("#txtHidden_HotelCode_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            var sHotelDesc = $("#txt_HotelDescription_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            var sImage = $('#img_' + k + '_' + i).attr("src");
            if (sHotelName == "") {
                alert("Please enter Hotel Name for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).focus();
                return false;
            }
            else if (sHotelDesc == "") {
                alert("Please enter Hotel Description for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                $("#txt_HotelDescription_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).focus();
                return false;
            }
            else if (sImage == "" || sImage == "../images/upload-image.png") {
                alert("Please enter Hotel Image for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                $('#img_' + k + '_' + i).focus();
                return false;
            }
        }
    }
    var singleItinerary;
    var nSuccessCount = 0;
    for (var k = 0; k < global_CategoryCount; k++) {
        var listHotelName = [];
        var listHotelCode = [];
        var listHotelDesc = [];
        for (var i = 0; i < globe_ItineraryCount; i++) {
            var sHotelName = $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            HotelCode = $("#txtHidden_HotelCode_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            var sHotelDesc = $("#txt_HotelDescription_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
            var sImage = $('#img_' + k + '_' + i).attr("src");
            listHotelName.push(sHotelName);
            listHotelCode.push(HotelCode);
            listHotelDesc.push(sHotelDesc);

        }
        var jsonText = JSON.stringify({ nID: globe_urlParamDecoded, listHotelName: listHotelName, listHotelCode: listHotelCode, listHotelDesc: listHotelDesc, nCategoryID: global_HotelItinerary[k].nCategoryID, sCategoryName: GetCategoryName(global_HotelItinerary[k].nCategoryID) });
        $.ajax({
            url: "AddPackageHandler.asmx/SaveHotelDetails",
            type: "post",
            data: jsonText,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.Session == 0) {
                    window.location.href = "index.htm";
                }
                if (result.retCode == 0) {
                    alert("No packages found");
                }
                else if (result.retCode == 1) {
                    nSuccessCount = nSuccessCount + 1;
                    if (nSuccessCount == global_CategoryCount) {
                        alert("Itinerary Added Successfully, Please add package images to products");
                        AddPackageImages(globe_urlParamDecoded);
                    }
                }
            },
            error: function () {
                alert('Error in saving, please try again!');
            }
        });
    }
}


var tpj = jQuery;
var HotelCode;
var chkinMonth;
var chkoutDate;
var chkoutMonth;
var id;
function AutoComplete(id) {
    debugger;
    var hdncc = tpj('#hdnHCode').val()
    tpj('#' + id).autocomplete({
        source: function (request, response) {
            tpj.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Genralhandler.asmx/GetHotel",
                data: "{'name':'" + tpj('#' + id).val() + "','destination':'" + tpj('#hdnHCode').val() + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);

                },
                error: function (result) {
                    alert("No Match");
                    alert(tpj('#hdnHCode').val());
                }
            });
        },
        minLength: 4,
        select: function (event, ui) {
            debugger;
            HotelCode = ui.item.id;
            GetDescription(id);

        }
    });

}


function GetDescription(id) {
    debugger;

    $.ajax({
        type: "POST",
        url: "Genralhandler.asmx/GetHotelDescription",
        data: '{"HCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var arrAgentList = result.ReservationDetails;

                var txtid = id.replace("txt_HotelName_", "txt_HotelDescription_");
                $('#' + id.replace("txt_HotelName_", "txtHidden_HotelCode_")).val(HotelCode)


                $('#' + txtid).val(arrAgentList[0].HotelFacilities)


            }


        },
        error: function () {
        }
    });

};



function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
