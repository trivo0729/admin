﻿function UploadDocument(id, value) {
    var fileUpload = $("#" + id).get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#loder" + value).css("display", "")
    $("#lbl_UploadOutbound").css("display", "none")
    $.ajax({
        url: "VisaAttachments.ashx?method=VisaFile&Ref_id=" + hidenId + "&FileNo=" + value,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only jpg file is allowed") {
                var image = document.getElementById(id);
                image.src = image.src.split("?")[0] + "?" + new Date().getTime();
                $("#lbl_UploadOutbound").css("display", "none")
                $("#img_UploadOutbound").css("display", "none")
                //  updateImage(value)
                Success(result);
                GetDocuments(hidenId, type)

            }
            else if (result == "Only jpg file is allowed") {
                $("#img_UploadOutbound").css("display", "none")
                $("#lbl_UploadOutbound").html("An Error Occured!!")
                $("#lbl_UploadOutbound").css("display", "")
            }
            $("#loder" + value).css("display", "none")
        },

    });
}
var newImage = new Image();


var Rfrence = []
function updateImage(id) {
    Rfrence.push(id)
    for (var i = 0; i < Rfrence.length; i++) {
        var image = document.getElementById(Rfrence[i]);
        if (image.currentSrc != "http://localhost:1653/VisaImages/logo.png")
            image.src = image.src.split("?")[0] + "?" + new Date().getTime();
        else
            //image.src = '../VisaImages/' + hidenId + "_" + id + '.jpg'
            image.src = '../Thumnail.aspx?fn=' + hidenId + '_' + id + '.jpg'
        //image.src = image.src.split("?")[0] + "?" + new Date().getTime();
    }
}
function DeleteAttachments(FileNo) {
    $("#loder" + FileNo).css("display", "")
    $.ajax({
        url: "../Handler/VisaDetails.asmx/DeleteAttachment",
        type: "POST",
        data: '{"hidenId":"' + hidenId + '","FileNo":"' + FileNo + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                updateImage(FileNo)
                alert("Image Deleted")
            }
            else {
                $("#img_UploadOutbound").css("display", "none")
            }
            $("#loder" + FileNo).css("display", "none")
        },

    });
}
