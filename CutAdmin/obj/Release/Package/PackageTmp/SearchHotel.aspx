﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SearchHotel.aspx.cs" Inherits="CutAdmin.SearchHotel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../Scripts/RoomRates.js?v=1.3"></script>
    <script src="../Scripts/HotelSearch.js?v=1.3"></script>
    <script src="../Scripts/Booking.js?v=1.6"></script>
    <script src="../Scripts/b2bRates.js?v=1.7"></script>
    <script src="../Scripts/Filter.js?v=1.3"></script>
    <script src="../js/GetMap.js?v=1.3"></script>
    <style>
        .mySlides {display:none;}
        .w3-display-left {
            position: absolute;
            top: 50%;
            left: 0%;
            transform: translate(0%,-50%);
        }
        .w3-display-right {
            position: absolute;
            top: 50%;
            right: 0%;
            transform: translate(0%,-50%);
        }
        .w3-content {
             max-width: 980px;
            margin: auto;
        }
        .w3-tooltip, .w3-display-container {
        position: relative;
        width:300px;
        height:250px
}
</style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <section role="main" id="main">

        <hgroup id="main-title" class="bold grey">
            <h1 id="SPN_HotelName">Search Hotel</h1>
             <h2 style="float: right">
                  <a class="addnew with-tooltip tooltip-bottom" id="div_Image" style="cursor:pointer" title="Hotel Details" onclick="GetHotelInfo()"></a>
                    <a  href="#" class="addnew  with-tooltip tooltip-bottom" id="lbl_Fillter" title="Filter" style="display: none;cursor:pointer" onclick="ShowFilter()"><i class="fa fa-filter"></i></a>
                    <input type="button" class="button anthracite-gradient buttonmrgTop" id="btn_mdfSearch" style="display: none" onclick="ShowSearch();" value="Modify Search">
                </h2>
            <hr>
        </hgroup>

        <div class="with-padding">

            <div id="div_Search">
                   <div class="columns">
                <div id="ButtonSearch">

                    <label for="txt_SearchType" class="label">Search Type</label><br />
                    <span class="button-group">
                        <label for="SearchType1" class="button blue-active">
                            <input type="radio" checked name="button-radio SearchType" id="SearchType1" value="General" onclick="SearchSelect(this.value);">
                            Quick Search
                        </label>
                        <label for="SearchType2" class="button blue-active">
                            <input type="radio" name="button-radio SearchType" id="SearchType2" value="Advance" onclick="SearchSelect(this.value);">
                            Advance Search
                        </label>

                    </span>


                </div>
            </div>
                <div class="columns">
                    <div class="new-row four-columns six-columns-mobile">
                        <label for="txt_Destination" class="label">Destination</label><span class="red">*</span>
                        <input type="text" id="txt_Destination" placeholder="City name" class="input  ui-autocomplete-input full-width" onchange=" GetHotelList();">
                    </div>
                    <div class="four-columns six-columns-mobile">
                        <label for="txt_HotelName" class="label">Hotel Name</label>
                        <input type="text" id="txt_HotelName" list="listHotels" placeholder="Hotel name" class="input ui-autocomplete-input full-width">
                        <datalist id="listHotels"></datalist>
                    </div>


                    <input type="hidden" id="hdnDCode" />
                    <input type="hidden" id="hdnHCode" />

                    <div class="four-columns twelve-columns-mobile" id="drp_Nationality">
                        <label for="sel_Nationality" class="label">Nationality</label>
                        <select id="sel_Nationality" onchange="CheckNationality(this.value);" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width">
                            <option value="All">All&nbsp;/&nbsp;Unselect</option>
                        </select>
                    </div>
                </div>

                <div class="columns">
                    <div class="three-columns six-columns-mobile">
                        <label for="txt_Checkin" class="label">Check In</label><br />
                        <%--  <span class="input full-width datePick">
                        <input type="text" id="txt_Checkin" class="input-unstyled  gldp-el" value="">
                    </span>--%>
                        <span class="input full-width datePick">
                            <span class="icon-calendar"></span>
                            <input type="text" id="txt_Checkin" class="input-unstyled  gldp-el" value="">
                        </span>

                    </div>

                    <div class="three-columns six-columns-mobile">
                        <label for="txt_Checkout" class="label">Check Out</label><br />
                        <span class="input full-width datePick">
                            <span class="icon-calendar"></span>

                            <input type="text" id="txt_Checkout" class="input-unstyled" value="" />
                        </span>
                    </div>

                    <div class="three-columns six-columns-mobile" id="drp_Nights">
                        <label for="sel_Nights" class="label">Nights</label><br />
                        <select id="sel_Nights" class="select full-width" onchange="drpChangeNights();">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                        </select>
                    </div>


                    <%-- <div class="new-row  three-columns">
                    <label for="sel_Nationality" class="label">Nationality</label>
                    <select id="sel_Nationality" class="select  full-width">
                        <option value="" selected="selected" disabled>Please Select</option>
                    </select>
                </div>--%>


                    <div class="three-columns six-columns-mobile">
                        <label for="sel_Rooms" class="label">Rooms</label><br />
                        <select id="Select_Rooms" class="select full-width">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                    </div>

                </div>


                <!--First room-->
                <div class="columns">
                    <div class="three-columns">
                        <label for="sel_Rooms" class="label"></label>
                        <br />
                        <input type="text" id="spn_RoomType1" placeholder="First Room" class="input  ui-autocomplete-input full-width" disabled autocomplete="off">
                    </div>
                    <div class="one-columns " id="Select_Adults1">
                        <label for="Select_Adults1" class="label">Adults</label><br />
                        <select id="Select_Adults1d" class="select full-width">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                       <div class="one-columns ">
                <label for="s" class="label">Childs</label><br />
                <select id="Select_Children1" class="select full-width">
                    <option selected="selected" value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
            <div class="one-columns " id="Select_AgeChildFirst1" style="display: none">
                <label for="Select_AgeChildFirst1" class="label">Childs Ages</label><br />
                <select id="Select_AgeChildFirst1d" class="select full-width" style="display: none">
                    <option selected="selected" value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
            <div class="one-columns test" id="Select_AgeChildSecond1" style="display: none">
                <label for="Select_AgeChildSecond1" class="label"></label>
                <br />
                <select id="Select_AgeChildSecond1d" class="select full-width">
                    <option selected="selected" value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
            <div class="one-columns test" id="Select_AgeChildThird1" style="display: none">
                <label for="Select_AgeChildThird1" class="label"></label>
                <br />
                <select id="Select_AgeChildThird1d" class="select full-width">
                    <option selected="selected" value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
            <div class="one-columns test" id="Select_AgeChildFourth1" style="display: none">
                <label for="Select_AgeChildFourth1" class="label"></label>
                <br />
                <select id="Select_AgeChildFourth1d" class="select full-width">
                    <option selected="selected" value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
                </div>

                <!--Second room-->
                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType2" style="display: none">

                        <input type="text" id="spn_RoomType2d" class="input ui-autocomplete-input full-width" disabled="disabled" value="Second Room" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults2" style="display: none">

                        <select id="Select_Adults2d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children2" style="display: none">

                        <select id="Select_Children2d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst2" style="display: none">

                        <select id="Select_AgeChildFirst2d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond2" style="display: none">

                        <select id="Select_AgeChildSecond2d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird2" style="display: none">

                        <select id="Select_AgeChildThird2d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth2" style="display: none">

                        <select id="Select_AgeChildFourth2d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Third room-->
                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType3" style="display: none">

                        <input type="text" id="spn_RoomType3d" class="input ui-autocomplete-input full-width" disabled="disabled" value="Third Room" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults3" style="display: none">

                        <select id="Select_Adults3d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children3" style="display: none">

                        <select id="Select_Children3d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst3" style="display: none">

                        <select id="Select_AgeChildFirst3d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond3" style="display: none">

                        <select id="Select_AgeChildSecond3d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird3" style="display: none">

                        <select id="Select_AgeChildThird3d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth3" style="display: none">

                        <select id="Select_AgeChildFourth3d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Forth room-->
                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType4" style="display: none">

                        <input type="text" id="spn_RoomType4d" class="input ui-autocomplete-input full-width" value="Fourth Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults4" style="display: none">

                        <select id="Select_Adults4d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children4" style="display: none">

                        <select id="Select_Children4d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst4" style="display: none">

                        <select id="Select_AgeChildFirst4d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond4" style="display: none">

                        <select id="Select_AgeChildSecond4d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird4" style="display: none">

                        <select id="Select_AgeChildThird4d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth4" style="display: none">

                        <select id="Select_AgeChildFourth4d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Fifth room-->
                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType5" style="display: none">

                        <input type="text" id="spn_RoomType5d" class="input ui-autocomplete-input full-width" value="Fifth Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults5" style="display: none">

                        <select id="Select_Adults5d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children5" style="display: none">

                        <select id="Select_Children5d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst5" style="display: none">

                        <select id="Select_AgeChildFirst5d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond5" style="display: none">

                        <select id="Select_AgeChildSecond5d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird5" style="display: none">

                        <select id="Select_AgeChildThird5d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth5" style="display: none">

                        <select id="Select_AgeChildFourth5d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Sixth room-->

                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType6" style="display: none">

                        <input type="text" id="spn_RoomType6d" class="input ui-autocomplete-input full-width" value="Sixth Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults6" style="display: none">

                        <select id="Select_Adults6d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children6" style="display: none">

                        <select id="Select_Children6d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst6" style="display: none">

                        <select id="Select_AgeChildFirst6d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond6" style="display: none">

                        <select id="Select_AgeChildSecond6d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird6" style="display: none">

                        <select id="Select_AgeChildThird6d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth6" style="display: none">

                        <select id="Select_AgeChildFourth6d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Seventh room-->

                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType7" style="display: none">

                        <input type="text" id="spn_RoomType7d" class="input ui-autocomplete-input full-width" value="Seventh Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults7" style="display: none">

                        <select id="Select_Adults7d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children7" style="display: none">

                        <select id="Select_Children7d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst7" style="display: none">

                        <select id="Select_AgeChildFirst7d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond7" style="display: none">

                        <select id="Select_AgeChildSecond7d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird7" style="display: none">

                        <select id="Select_AgeChildThird7d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth7" style="display: none">

                        <select id="Select_AgeChildFourth7d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Eight room-->
                <div class="columns">
                    <%-- <div class="new row four-columns">
            </div>--%>
                    <div class="three-columns test" id="spn_RoomType8" style="display: none">

                        <input type="text" id="spn_RoomType8d" class="input ui-autocomplete-input full-width" value="Eight Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults8" style="display: none">

                        <select id="Select_Adults8d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children8" style="display: none">

                        <select id="Select_Children8d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst8" style="display: none">

                        <select id="Select_AgeChildFirst8d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond8" style="display: none">

                        <select id="Select_AgeChildSecond8d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird8" style="display: none">

                        <select id="Select_AgeChildThird8d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth8" style="display: none">

                        <select id="Select_AgeChildFourth8d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                </div>

                <!--Ninth room-->
                <div class="columns">
                    <div class="three-columns test" id="spn_RoomType9" style="display: none">

                        <input type="text" id="spn_RoomType9d" class="input ui-autocomplete-input full-width" value="Ninth Room" disabled="disabled" style="cursor: default;">
                    </div>
                    <div class="one-columns test" id="Select_Adults9" style="display: none">

                        <select id="Select_Adults9d" class="select full-width">
                            <option value="1">1</option>
                            <option value="2" selected="selected">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_Children9" style="display: none">

                        <select id="Select_Children9d" class="select full-width">
                            <option selected="selected" value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFirst9" style="display: none">

                        <select id="Select_AgeChildFirst9d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildSecond9" style="display: none">

                        <select id="Select_AgeChildSecond9d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildThird9" style="display: none">

                        <select id="Select_AgeChildThird9d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>
                    <div class="one-columns test" id="Select_AgeChildFourth9" style="display: none">

                        <select id="Select_AgeChildFourth9d" class="select full-width">
                            <option selected="selected" value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>

                        </select>
                    </div>



                    <p class="SearchBtn">
                        <input type="button" class="button anthracite-gradient buttonmrgTop" onclick="SearchRates();" value="Search" />
                    </p>
                </div>




            </div>
            
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">

                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Room Type</label>
                            <div class="full-width button-height">
                                <select id="RoomType" onchange="FilterRate(this.value)" class="select multiple-as-single easy-multiple-selection  check-list" multiple>
                                </select>
                            </div>
                        </div>

                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Meal Type</label>
                            <div class="full-width button-height">
                                <select id="MealType" onchange="FilterRate(this.value)" class="select multiple-as-single easy-multiple-selection  check-list" multiple>
                                </select>
                            </div>
                        </div>

                       <div class="three-columns twelve-columns-mobile four-columns-tablet" >
                             <label>Price Range</label>
                                                        <div class="full-width button-height" id="Price_Filter">
                            <span class="demo-slider" data-slider-options='{"size":false,"values":[0,100],"tooltip":["top","top"],"tooltipOnHover":false,"topLabelAlign":"right","barClasses":["red-gradient","glossy"]}'></span>
                         </div>
                                                             </div>

                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div id="div_Rates">
            </div>

            <br />
            <div class="tabsystem">

                <div class="columns selcol" id="divFilters" style="display: none">

                    <div class="eight-columns eight-columns-mobile">
                        <label for="sel_ValidNationality" class="label">Valid Nationality</label>
                        <select id="sel_ValidNationality" onchange="CheckValidNationality(this.value);" class="select full-width">
                            <%-- <option value="" selected="selected" disabled>Please Select</option>--%>
                        </select>
                    </div>

                    <div class="two-columns three-columns-mobile">
                        <input type="button" class="button anthracite-gradient" onclick="Validate()" value="BooK Now" style="margin-top: 20px; cursor: pointer" />
                    </div>
                </div>
                <br />
                <div class="standard-tabs margin-bottom" id="tabs">

                    <ul class="tabs" id="ul_Supplier">
                    </ul>

                    <div class="tabs-content" id="tab_Supplier">
                    </div>

                </div>

            </div>
        </div>
    </section>
   <script>
       $(function myfunction() {
           $("#txt_Destination").autocomplete({
               source: function (request, response) {
                   jQuery.ajax({
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       url: "../HotelHandler.asmx/GetDestinationCode",
                       data: "{'name':'" + document.getElementById('txt_Destination').value + "'}",
                       dataType: "json",
                       success: function (data) {
                           var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                           response(result);
                       },
                       error: function (result) {
                           alert("No Match");
                       }
                   });
               },
               minLength: 3,
               select: function (event, ui) {
                   jQuery('#hdnDCode').val(ui.item.id);
               }
           });

       })


    </script>
    <script>
        $(document).ready(function () {
            $('.fa-filter').click(function () {
                $(' #filter').slideToggle();
                $('.searchBox li a ').on('click', function () {
                    $('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
