﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StartSale.aspx.cs" Inherits="CutAdmin.StartSale" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <script src="Scripts/HotelInventory.js?v=1.3"></script>


    <%--<script type="text/javascript">
        $(document).ready(function () {
            // GetCountry();

            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });
            $("#datepicker_Till").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });

        });


    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <!-- Main content -->
    <section role="main" id="main">

        <hgroup id="main-title" class="thin">
            <h1>Start Sale</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <span class="">
                    <h3 id="lbl_Hotel" style="margin-bottom:5px"></h3>
                    <%--<label id="lbl_roomtype" style="float: right"></label>--%>
                    <label id="lbl_address"></label>
                </span>
            <br />
            <br />
            <div class="row block table-head margin-bottom">

                <div class="with-padding start-sale" id="div_Rooms">
                    <div class="columns">
                        <div class="new-row three-columns twelve-columns-mobile" id="" style="">

                            <label class="input-info">Select Room*:</label><br />
                            <select id="ddlRoom" class="select multiple-as-single easy-multiple-selection allow-empty check-list full-widtht" onchange="" multiple>
                            </select>
                        </div>
                        <div class="three-columns twelve-columns-mobile" id="" style="">
                               <label class="input-info">Select Rate Type*:</label><br />
                            <select id="ddlRatetype" class="full-width glossy select multiple-as-single easy-multiple-selection check-list Rate" multiple onchange="fnRatType()">
                                <%--<option class="" value="" selected="selected">Rate Type</option>--%>
                                <option class="" value="Contracted">Contracted</option>
                                <option class="" value="Promo">Promo</option>
                                <option class="" value="Tactical">Tactical</option>
                            </select>
                        </div>
                        <div class="six-columns twelve-columns-mobile" id="DatesUI" style="">
                        </div>


                        <%-- <div class="new-row three-columns" id="">
                            <small class="input-info">Max Room* :</small>
                            <input type="text" name="" id="txt_MaxRoom" class="input full-width  " value="">
                        </div>--%>
                        <%--<div class="three-columns">
                            <small class="input-info">Free Sale Till*:</small>
                            <input class="input ui-autocomplete-input full-width" type="text" id="datepicker_Till" name="" style="cursor: pointer; width: 90%" value="" />

                        </div>--%>
                    </div>




                    <details class="details margin-bottom  start-sale" onclose>
                        <summary>Optional  <i class="icon-chevron-right icon-size2"></i></summary>
                        <div class="columns" style="min-height: 130px; margin-top: 20px; margin-left: 14px">
                            <div class="twelve-columns">
                                <ul>
                                    <li>
                                        <div>
                                            <span>Maximum no of Room to be sold per booking.</span>
                                            <span>
                                                <input type="text" name="" id="txt_MaxRoomperbook" class="input" value="" style="width: 5%"></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span>Free Sale Till*:</span>
                                            <span style="margin-left: 30%">
                                                <%-- <input type="text" name="" id="txt_Till" class="input" value="" style="width: 5%">--%>
                                                <input class="input ui-autocomplete-input" type="text" id="txt_Till" name="" style="width: 20%"  value=""/>
                                               
                                            </span>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span>Maximum no of Room to be sold per date.</span>
                                            <span>
                                                <input type="text" name="" id="txt_MaxRoomperDate" class="input" value="" style="width: 5%; margin-left: 4%"></span>
                                        </div>
                                    </li>

                                    <li>

                                        <div>
                                            Inventory sold live or on request.
                                        

                                                <span class="button-group" style="margin-left: 5%">
                                                    <label for="chk_Live" class="button grey-active">
                                                        <input type="radio" name="button-radio" id="chk_Live" value="Live" onclick="Redirectbtn(this.value)" checked>
                                                        Live
                                                    </label>
                                                    <label for="chk_Request" class="button red-active">
                                                        <input type="radio" name="button-radio" id="chk_Request" value="Request" onclick="Redirectbtn(this.value)">
                                                        Request
                                                    </label>

                                                </span>
                                        </div>
                                    </li>


                                </ul>

                            </div>
                        </div>
                    </details>
                    <div class="columns">
                        <div class="new-row twelve-columns" id="">
                            <button type="button" style="float: right" value="fs" class="button glossy mid-margin-right" onclick="SaveInventory(this.value);" title="Submit Details">
                                <span class="button-icon"><span class="icon-tick"></span></span>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </section>
    <!-- End main content -->




</asp:Content>
