﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="UpdateApprovedRooms.aspx.cs" Inherits="CutAdmin.HotelAdmin.UpdateApprovedRooms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    <script src="Scripts/UpdateApprovedRooms.js?v=1.0"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="../css/styles/form.css?v=1">
    <link rel="stylesheet" href="../css/styles/switches.css?v=1">
    <link rel="stylesheet" href="../css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="../js/libs/DataTables/jquery.dataTables.css?v=1">

    <style>
        .selectedImg {
            border: 3px solid #1b397b;
        }
    </style>
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section role="main" id="main" >
        <hgroup id="main-title" class="thin">
            <h1>Rooms</h1>
            <hr />
        </hgroup>
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <div id="DivAddRooms" style="display: none;margin-top:-30px" class="with-padding hotelmap">
           
                <form class="block wizard same-height"  >

                    <h3 class="block-title">Room Details</h3>

                    <fieldset class="wizard-fieldset fields-list">
                        <legend class="legend">Room Details</legend>
                      
                            <div class="columns" style="height: 300px;">

                                <div class="four-columns  twelve-columns-mobile">

                                    <label class="input-info">Room Type:</label><br />
                                    <input type="text" id="txtRoomType" class="input" style="width: 92%" list="RoomType" autocomplete="off" />
                                    <datalist id="RoomType"></datalist>
                                    <%-- <span class="right-side button icon-download blue" onclick="SaveRoomType()"></span>--%>
                                </div>
                                <%--<div class="four-columns">
                                          <small class="input-info">Number of Rooms:</small>
                                          <input type="number" id="QtyRooms" class="input full-width" value="">
                                      </div>--%>
                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Room Occupacy</label>
                                    <input type="number" id="QtyOccupacy" class="input full-width" value="0">
                                </div>

                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Bedding Type</label>
                                    <select id="BeddingType" name="validation-select" class="select full-width">
                                        <option value="-" selected="selected" disabled>Please select</option>
                                        <option value="Single Bed">Single Bed</option>
                                        <option value="Twin Bed">Twin Bed</option>
                                        <option value="Triple Bed">Triple Bed</option>
                                        <option value="Quard Bed">Quard Bed</option>
                                        <option value="Queen Bed">Queen Bed</option>
                                        <option value="6 bed">6 bed</option>
                                         <option value="King Size">King Size</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <div class="new-row four-columns  twelve-columns-mobile">
                                    <label class="input-info">Room Size:</label>
                                    <input type="text" id="RoomSize" class="input full-width" placeholder="Sqm">
                                </div>

                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Max Extra Bed Allowed:</label>
                                    <input type="number" id="MaxExtrabedAllowed" min="0" value="0" class="input full-width">
                                </div>
                                <%-- <div class="new-row four-columns">
                                          <small class="input-info">Maximum Children Allowed:</small>
                                          <input type="number" id="MaxChildsAllowed" class="input full-width" value="0">
                                      </div>--%>



                                <%--  <div class="three-columns">
                                          <small class="input-info">Adults with Childs:</small>
                                          <input type="number" id="AdultsWithChilds" class="input full-width" value="0">
                                      </div>--%>
                                <div class="four-columns twelve-columns-mobile">
                                    <label class="input-info">No Of Child Without Bed</label>
                                    <input type="number" id="AdultsWithoutChilds" min="0" value="0" class="input full-width" >
                                </div>

                                <div class="new-row twelve-columns">
                                    <label class="input-info">Room Description:</label>
                                    <textarea id="RoomDescription" class="input full-width" style="min-height: 100px;"></textarea>
                                </div>

                            </div>
                       
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset" id="tab_Amenities">
                        <legend class="legend">Amenities</legend>
                        <div class="columns" style="min-height: 280px;">
                            <div class="twelve-columns">
                                <label class="input-info">Select Room Amenities</label>
                                <div id="divAddAmenities" class="boxed" style="border-style: groove; border-width: 2px; background-color: white">
                                </div>
                            </div>
                            <div class="five-columns">
                                <label class="input-info">New Amenity</label><br />
                                <input type="text" id="txtAmenities" class="input full-width">
                            </div>
                            <div class="one-columns">
                                <br />
                                <span class="button icon-plus blue" onclick="SaveAmenities()"></span>
                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Policies</legend>
                        <div class="">
                            <div class="columns" style="height: 300px;" id="tab_Policies">
                                <%-- <div class="four-columns">
                                          <small class="input-info">Max Extra Bed Allowed:</small>
                                          <input type="number" id="MaxExtrabedAllowed" class="input full-width" value="0">
                                      </div>--%>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Bedding Type</small>
                                           <select id="BeddingType" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                              <option value="Single Bed">Single Bed</option>
                                              <option value="Twin Bed">Twin Bed</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>--%>

                                <div class="four-columns">
                                    <label class="input-info">Smoking Allowed</label>
                                    <select id="SmokingAllowed" name="validation-select" class="select full-width">
                                        <option value="-" selected="selected" disabled>Please select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Internet Connection</small>
                                           <select id="Interconnection" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                               <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>
                                --%>
                                <div class="twelve-columns">
                                    <label class="input-info">Room Notes:</label>
                                    <textarea id="RoomNotes" class="input full-width" style="min-height: 100px;"></textarea>
                                </div>

                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Images</legend>
                        <div style="min-height: 300px;">
                            <div class="columns">
                                <div class="six-columns">
                                    <input type="file" name="images[]" id="RoomImages" size="9" class="input full-width" onchange="preview_images();" multiple>
                                    <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p>
                                </div>
                                <div class="two-columns">
                                    <a class="button icon-trash">
                                        <input id="btnSubImageClearAdd" type="button" value="Clear All" onclick="ClearSubImageAdd();" style="background-color: transparent; border: none;" />
                                    </a>
                                </div>

                            </div>
                            <div class="columns" id="image_preview">
                            </div>

                        </div>
                        <hr />
                        <button type="button" class="button glossy anthracite-gradient float-right" onclick="AddRoomDetails();">Add Room</button>
                    </fieldset>

                </form>
           
        </div>
        <div id="DivUpdateRooms" style="display: none;margin-top:-300px" class="with-padding hotelmap">
           
                <form class="block wizard same-height" >

                    <h3 class="block-title">Update Room</h3>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Room Details</legend>
                        <div class="">
                            <div class="columns" style="height: 300px;">

                                <div class="four-columns">

                                    <label class="input-info">Room Type:</label><br />
                                    <input type="text" id="txtRoomType1" class="input" style="width: 92%" list="RoomType" autocomplete="off" />
                                    <datalist id="RoomType1"></datalist>
                                    <%-- <span class="right-side button icon-download blue" onclick="SaveRoomType()"></span>--%>
                                </div>
                                <%--<div class="four-columns">
                                          <small class="input-info">Number of Rooms:</small>
                                          <input type="number" id="QtyRooms" class="input full-width" value="">
                                      </div>--%>
                                <div class="four-columns">
                                    <label class="input-info">Room Occupacy</label>
                                    <input type="number" id="QtyOccupacy1" class="input full-width" value="0">
                                </div>

                                <div class="four-columns" id="div_bed">
                                    <label class="input-info">Bedding Type</label>
                                    <select id="BeddingType1" name="validation-select" class="select full-width OfferType">
                                        <option value="-" selected="selected" disabled>Please select</option>
                                        <option value="Single Bed">Single Bed</option>
                                        <option value="Twin Bed">Twin Bed</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <div class="new-row four-columns">
                                    <label class="input-info">Room Size:</label>
                                    <input type="text" id="RoomSize1" class="input full-width" placeholder="Sqm">
                                </div>

                                <div class="four-columns">
                                    <label class="input-info">Max Extra Bed Allowed:</label>
                                    <input type="number" id="MaxExtrabedAllowed1" class="input full-width">
                                </div>
                                <%-- <div class="new-row four-columns">
                                          <small class="input-info">Maximum Children Allowed:</small>
                                          <input type="number" id="MaxChildsAllowed" class="input full-width" value="0">
                                      </div>--%>



                                <%--  <div class="three-columns">
                                          <small class="input-info">Adults with Childs:</small>
                                          <input type="number" id="AdultsWithChilds" class="input full-width" value="0">
                                      </div>--%>
                                <div class="four-columns">
                                    <label class="input-info">No Of Child Without Bed</label>
                                    <input type="number" id="AdultsWithoutChilds1" class="input full-width" value="0">
                                </div>

                                <div class="new-row twelve-columns">
                                    <label class="input-info">Room Description:</label>
                                    <textarea id="RoomDescription1" class="input full-width" style="min-height: 100px;"></textarea>
                                </div>

                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset" id="tab_Amenities1">
                        <legend class="legend">Amenities</legend>
                        <div class="columns" style="min-height: 280px;">
                            <div class="twelve-columns">
                                <label class="input-info">Select Room Amenities</label>
                                <div id="divAddAmenities1" class="boxed" style="border-style: groove; border-width: 2px; background-color: white">
                                </div>
                            </div>
                            <div class="five-columns">
                                <label class="input-info">New Amenity</label><br />
                                <input type="text" id="txtAmenities1" class="input full-width">
                            </div>
                            <div class="one-columns">
                                <br />
                                <span class="button icon-plus blue" onclick="SaveAmenities()"></span>
                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Policies</legend>
                        <div class="">
                            <div class="columns" style="height: 300px;" id="tab_Policies1">
                                <%-- <div class="four-columns">
                                          <small class="input-info">Max Extra Bed Allowed:</small>
                                          <input type="number" id="MaxExtrabedAllowed" class="input full-width" value="0">
                                      </div>--%>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Bedding Type</small>
                                           <select id="BeddingType" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                              <option value="Single Bed">Single Bed</option>
                                              <option value="Twin Bed">Twin Bed</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>--%>

                                <div class="four-columns" id="div_smoke">
                                    <label class="input-info">Smoking Allowed</label>
                                    <select id="SmokingAllowed1" name="validation-select" class="select full-width OfferType">
                                        <option value="-" selected="selected" disabled>Please select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Internet Connection</small>
                                           <select id="Interconnection" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                               <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>
                                --%>
                                <div class="twelve-columns">
                                    <label class="input-info">Room Notes:</label>
                                    <textarea id="RoomNotes1" class="input full-width" style="min-height: 100px;"></textarea>
                                </div>

                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Images</legend>
                        <div style="min-height: 300px;">
                            <div class="columns">
                                <div class="six-columns">
                                    <input type="file" name="images[]" id="RoomImages1" size="9" class="input full-width" onchange="preview_images1();" multiple>
                                    <p class="red">(Please select multiple Images by pressing '<b>CTRL</b> button')</p>
                                </div>
                                <%-- <div class="two-columns">
                                            <a class="button icon-trash">
                                                <input id="btnSubImageClearAdd" type="button" value="Clear All" onclick="ClearSubImageAdd();" style="background-color: transparent; border: none;" />
                                            </a>
                                        </div> --%>
                            </div>
                            <div class="columns" id="image_preview1">
                            </div>

                        </div>
                        <hr />
                        <button type="button" class="button glossy anthracite-gradient float-right" onclick="UpdateRoomDetailsApproved();">Update & Approved Room</button>
                    </fieldset>

                </form>

        </div>
    </section>
    <!-- End main content -->



    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Scripts -->
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script src="../js/setup.js"></script>

    <!-- Template functions -->
    <script src="../js/developr.input.js"></script>
    <script src="../js/developr.navigable.js"></script>
    <script src="../js/developr.notify.js"></script>
    <script src="../js/developr.scroll.js"></script>
    <script src="../js/developr.tooltip.js"></script>
    <script src="../js/developr.table.js"></script>
    <script src="../js/developr.accordions.js"></script>
    <script src="../js/developr.wizard.js"></script>

    <!-- Plugins -->
    <script src="../js/libs/jquery.tablesorter.min.js"></script>
    <script src="../js/libs/DataTables/jquery.dataTables.min.js"></script>

    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Table sort - DataTables
        var table = $('#sorting-advanced');
        table.dataTable({
            'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [0, 5] }
            ],
            'sPaginationType': 'full_numbers',
            'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
            'fnInitComplete': function (oSettings) {
                // Style length select
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        });

        // Table sort - styled
        $('#sorting-example1').tablesorter({
            headers: {
                0: { sorter: false },
                5: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {
                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');

            // Add fake row
            $('<tr class="row-drop">' +
				'<td colspan="' + tr.children().length + '">' +
					'<div class="float-right">' +
						'<button type="submit" class="button glossy mid-margin-right">' +
							'<span class="button-icon"><span class="icon-mail"></span></span>' +
							'Send mail' +
						'</button>' +
						'<button type="submit" class="button glossy">' +
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>' +
							'Remove' +
						'</button>' +
					'</div>' +
					'<strong>Name:</strong> John Doe<br>' +
					'<strong>Account:</strong> admin<br>' +
					'<strong>Last connect:</strong> 05-07-2011<br>' +
					'<strong>Email:</strong> john@doe.com' +
				'</td>' +
			'</tr>').insertAfter(tr);

        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });

        // Table sort - simple
        $('#sorting-example2').tablesorter({
            headers: {
                5: { sorter: false }
            }
        });

    </script>

    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>
</asp:Content>
