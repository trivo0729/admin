﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewVoucher.aspx.cs" Inherits="CutAdmin.ViewVoucher" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>View Voucher</title>
    <style type="text/css">
        @page {
            size: A3 portrait;
            margin: 0.5cm;
        }

        @media print {
            .page {
                page-break-after: avoid;
            }
        }
    </style>
    <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="js/jquery.v2.0.3.js"></script>
    <script src="Scripts/Alert.js"></script>
    <script type="text/javascript" src="scripts/Invoice.js"></script>

    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="css/2x.css?v=1">

    <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1">

    <!-- jQuery Form Validation -->
    <%--	<link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">--%>

    <!-- Additional styles -->

    <link rel="stylesheet" href="css/styles/modal.css?v=1">


    <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>


    <!-- iOS web-app metas -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- iPhone ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png" sizes="57x57">
    <!-- iPad ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad.png" sizes="72x72">
    <!-- iPhone (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-retina.png" sizes="114x114">
    <!-- iPad (Retina) ICON -->
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon-ipad-retina.png" sizes="144x144">

    <!-- iPhone SPLASHSCREEN (320x460) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="(device-width: 320px)">
    <!-- iPhone (Retina) SPLASHSCREEN (640x960) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone-retina.png" media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPhone 5 SPLASHSCREEN (640×1096) -->
    <link rel="apple-touch-startup-image" href="img/splash/iphone5.png" media="(device-height: 568px) and (-webkit-device-pixel-ratio: 2)">
    <!-- iPad (portrait) SPLASHSCREEN (748x1024) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="(device-width: 768px) and (orientation: portrait)">
    <!-- iPad (landscape) SPLASHSCREEN (768x1004) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="(device-width: 768px) and (orientation: landscape)">
    <!-- iPad (Retina, portrait) SPLASHSCREEN (2048x1496) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait-retina.png" media="(device-width: 1536px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2)">
    <!-- iPad (Retina, landscape) SPLASHSCREEN (1536x2008) -->
    <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape-retina.png" media="(device-width: 1536px)  and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2)">

    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link href="css/custom.css" rel="stylesheet" />
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <script src="Scripts/moments.js"></script>



    <script>
        var Latitude;
        var Longitutude;
        var HotelCode;
        $(document).ready(function () {
            debugger;
            var ReservationID = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
            var Uid = GetQueryStringParamsForAgentRegistrationUpdate('Uid');
            var Ststs = GetQueryStringParamsForAgentRegistrationUpdate('Status');
            HotelCode = GetQueryStringParamsForAgentRegistrationUpdate('HotelCode');

            // VoucherPrint(ReservationID, Uid);

            // document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationPopup('" + ReservationID + "', '" + Ststs + "')");
            document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFVoucher('" + ReservationID + "', '" + Uid + "','" + Ststs + "')");

            $("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));



        });


        function GetQueryStringParamsForAgentRegistrationUpdate(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }



    </script>
</head>
<body>

    <div style="margin-left: 43%;" id="BtnDiv">
        <input type="button" class="button anthracite-gradient" value="Mail" style="cursor: pointer" title="Voucher" data-toggle="modal" data-target="#VoucherModal" onclick="VoucherMailModal()" />
        <input type="button" class="button anthracite-gradient" value="DownLoad & Print" id="btn_VoucherPDF" />
        <%--   <input type="button" value="Cancel" id="btn_Cancel" />--%>
    </div>
    <br />
    <div id="maincontainer" runat="server" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray">

     
    </div>
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/colors.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui.js"></script>
    <!-- Scripts -->
    <script src="js/libs/jquery-1.10.2.min.js"></script>
    <script src="js/setup.js"></script>

    <!-- Template functions -->
    <script src="js/developr.input.js"></script>
    <script src="js/developr.navigable.js"></script>
    <script src="js/developr.notify.js"></script>
    <script src="js/developr.scroll.js"></script>
    <script src="js/developr.tooltip.js"></script>

    <!-- glDatePicker -->
    <script src="js/libs/glDatePicker/glDatePicker.min.js?v=1"></script>

    <!-- jQuery Form Validation -->
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>

    <!-- Plugins -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <script src="js/libs/DataTables/jquery.dataTables.min.js"></script>
    <script src="js/developr.modal.js"></script>
</body>
</html>
