﻿function GetMap() {
    var HotelCode = $('#listHotels option').filter(function () { return this.value == HotelName; }).data('id');
    $.ajax({
        type: "POST",
        url: "../handler/GenralHandler.asmx/GetHotelInfo",
        data: JSON.stringify({ HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var objHotel = result.arrHotel;
                $.modal({
                    content: '<div id="map"></div>',
                    title: objHotel.HotelName,
                    width: 800,
                    height: 400,
                    scrolling: false,
                });
                debugger;
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: new google.maps.LatLng(objHotel.HotelLatitude, objHotel.HotelLangitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var infowindow = new google.maps.InfoWindow();
                var marker;
                var location = {};
                var Result = []
               
                location = {
                    name: objHotel.HotelName,
                    Image: objHotel.HotelImage,
                    pointlat: parseFloat(objHotel.HotelLatitude),
                    pointlng: parseFloat(objHotel.HotelLangitude),
                    Rating: objHotel.HotelCategory,
                    Description: objHotel.HotelDescription,
                };
                console.log(location);
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(location.pointlat, location.pointlng),
                    map: map,
                    zoom: 12,
                });
                //google.maps.event.addListener(marker, 'click', (function (marker, location) {
                //    return function () {
                //        infowindow.setContent('<h4>' + location.name + ' <img src="' + location.Rating + '" style="    float: right;"/> </h4><span  style="    float: right;" class="opensans orange size12"> </span><br /><table><tr> <td><img src="' + location.Image + '" width="200" height="100"/></td><td><p>' + location.Description + '</p><input type="button" class="selectbtn mt1" id="btn_Hotel' + location.id + '"  value="Details" " style="float: right;"></td></tr></tabel>');
                //        infowindow.open(map, marker);
                //    };
                //})(marker, location));
                $("#map").removeAttr('style');
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
   
}


function GetMapAll() {
    setTimeout(GetMap(), 300);
}


